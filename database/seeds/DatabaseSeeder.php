<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(Biotic_Jnr_Seeder::class);
        $this->call(Enbiotic_120s_Seeder::class);
        $this->call(Enbiotic_60s_Seeder::class);
        $this->call(MultiBiotic_120s_Seeder::class);
        $this->call(MultiBiotic_60s_Seeder::class);
        $this->call(MultiBiotic_30s_Seeder::class);
        $this->call(NRGBiotic_120s_Seeder::class);
        $this->call(NRGBiotic_60s_Seeder::class);
        $this->call(NanoCelle_B12_Seeder::class);
        $this->call(NanoCelle_D3_Seeder::class);
        $this->call(W8BioticLemonAndLime_Seeder::class);
        $this->call(W8BioticStrawberriesAndCream_Seeder::class);
        $this->call(ORSBiotic_Seeder::class);
        $this->call(GastroDaily_Seeder::class);
        $this->call(BioClean_EPA_DHA_CoQ10_120s_Seeder::class);
        $this->call(BioClean_EPA_DHA_CoQ10_60s_Seeder::class);
        $this->call(BioClean_EPA_DHA_Plant_Sterols_120s_Seeder::class);
        $this->call(BioClean_EPA_DHA_Plant_Sterols_60s_Seeder::class);
        $this->call(MgOptimaRelax_12_Seeder::class);

//        $this->call(IngredientsTableSeeder::class);
//        $this->call(IngredientProductSeeder::class);
//        $this->call(CategoriesTableSeeder::class);
//        $this->call(CategoryProductSeeder::class);
//        $this->call(DealsTableSeeder::class);
//        $this->call(OrderTableSeeder::class);

//        $this->call(CategoriesChangesTableSeeder::class);
//        $this->call(CategoryProductChangesSeeder::class);
        $this->call(ProductsSlugSeeder::class);
//        $this->call(CategoriesSlugSeeder::class);
    }
}
