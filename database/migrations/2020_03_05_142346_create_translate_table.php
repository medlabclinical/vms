<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTranslateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('translate', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('product_name');
            $table->boolean('isEnglish')->default(0);
            $table->boolean('isSaudiArabia')->default(0);
            $table->boolean('isSpain')->default(0);
            $table->boolean('isRussia')->default(0);
            $table->boolean('isFrance')->default(0);
            $table->boolean('isJapan')->default(0);
            $table->boolean('isItaly')->default(0);
            $table->boolean('isSimplifiedChinese')->default(0);
            $table->boolean('isTraditionalChinese')->default(0);
            $table->string('English')->nullable();
            $table->string('English_link')->nullable();
            $table->string('SaudiArabia')->nullable();
            $table->string('SaudiArabia_link')->nullable();
            $table->string('Spain')->nullable();
            $table->string('Spain_link')->nullable();
            $table->string('Russia')->nullable();
            $table->string('Russia_link')->nullable();
            $table->string('France')->nullable();
            $table->string('France_link')->nullable();
            $table->string('Japan')->nullable();
            $table->string('Japan_link')->nullable();
            $table->string('Italy')->nullable();
            $table->string('Italy_link')->nullable();
            $table->string('SimplifiedChinese')->nullable();
            $table->string('SimplifiedChinese_link')->nullable();
            $table->string('TraditionalChinese')->nullable();
            $table->string('TraditionalChinese_link')->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('translate');
    }
}
