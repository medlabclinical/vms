<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('position')->nullable();
            $table->string('email')->nullable();
            $table->string('picture')->nullable();
            $table->text('bio')->nullable();
            $table->boolean('executive_team')->default(0);
            $table->boolean('consulting_team')->default(0);
            $table->boolean('scientific_team')->default(0);
            $table->boolean('board_of_director')->default(0);
            $table->boolean('vms')->default(0);
            $table->boolean('pharmaceuticals')->default(0);
            $table->boolean('medlab')->default(0);
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('people');
    }
}
