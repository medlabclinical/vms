<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWebinarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('webinars', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('subtitle')->nullable();
            $table->text('synopsis');
            $table->text('content');
            $table->string('title_img')->nullable();
            $table->string('2nd_img')->nullable();
            $table->string('3rd_img')->nullable();
            $table->string('video_link')->nullable();
            $table->integer('status')->default(1);
            $table->integer('comment_amount')->default(0);
            $table->string('date')->nullable();
            $table->string('index')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('webinars');
    }
}
