<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductPageDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_page_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('xero_item_id');
            $table->string('austl_number');
            $table->string('product_name_index');
            $table->string('product_name');
            $table->string('short_description');
            $table->text('patent');
            $table->text('general_summary');
            $table->text('practitioner_summary');
            $table->text('references');
            $table->text('side_effects');
            $table->text('interactions');
            $table->text('dosage_information');
            $table->text('ingredients');
            $table->text('cmi');
            $table->text('free_from');
            $table->tinyInteger('in_stock');
            $table->decimal('price_retail');
            $table->decimal('price_wholesale');
            $table->string('image_path');
            $table->string('thumb_image_path');
            $table->timestamps();
            $table->string('slug');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_page_details');
    }
}
