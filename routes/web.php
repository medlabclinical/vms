<?php

//Sitemap Creation
use Spatie\Sitemap\SitemapGenerator;

Route::get('sitemap', function () {
    SitemapGenerator::create('https://dev-vms.medlab.co/')->writeToFile('sitemap.xml');
    return 'sitemap created';
});

Route::middleware('checkstatus')->group(function () {
    foreach (\App\Models\Redirect::where('website', config("app.url"))->get() as $item) {
        Route::get('/' . $item->url_path, function () use ($item) {
            return redirect($item->redirect, 301);
        });
    }
    //About Pages
    Route::prefix('about')->group(function () {
        Route::get('/', 'PageController@getAboutMenu')->name('about');
        Route::get('about_us', 'PageController@getAbout')->name('about_us');
        Route::get('contact', 'IndexController@getContactContent')->name('contact');
    });

    //Product Pages
//    Route::get('products_in_development/{id}', 'WWWProductsController@getProductsInDevDetail')->name('products_in_development');
    Route::get('products','ProductsController@index')->name('products');
    Route::get('products_by_category','PageController@getGroupProduct')->name('products_by_category');
    Route::get('products_by_dietary','PageController@getDietaryProduct')->name('products_by_dietary');
    Route::get('product_page/{id}','ProductsController@getProductFamily')->name('product_page');
    Route::post('updateImage','ProductsController@store')->name('updateImage');
    Route::post('articles_page','ProductsController@articlePage')->name('articles_page');

    Route::prefix('2fa')->middleware('check_login')->group(function () {
        Route::get('/', 'LoginSecurityController@show2faForm')->name('2fa');
        Route::post('/generateSecret', 'LoginSecurityController@generate2faSecret')->name('generate2faSecret');
        Route::post('/enable2fa', 'LoginSecurityController@enable2fa')->name('enable2fa');
        Route::post('/disable2fa', 'LoginSecurityController@disable2fa')->name('disable2fa');



    });
    // 2fa middleware
    Route::post('/backup_login', 'LoginSecurityController@backUpLogin')->name('backup_login');
    Route::post('/generate_backup_code', 'LoginSecurityController@generateBackupCode')->name('generate_backup_code');
    Route::post('/show_backup_code', 'LoginSecurityController@showBackupCode')->name('show_backup_code');
    Route::post('/update_recover_phone', 'LoginSecurityController@updateRecoverPhone')->name('update_recover_phone');
    Route::post('/verify_last_three_num', 'LoginSecurityController@verifyLastThreeNum')->name('verify_last_three_num');
    Route::post('/verify_verification_code', 'LoginSecurityController@verifyVerificationCode')->name('verify_verification_code');
    Route::post('/2faVerify', function () {
        return response()->json(['msg' => 'success','email'=>Cookie::get('email'),'password'=>Cookie::get('password')]);
    })->name('2faVerify')->middleware('2fa');

// test middleware
    Route::get('/2fa_verify', function () {
        return "2FA middleware work!";
    })->name('run_verify')->middleware('2fa');

    // Cart Pages
    Route::middleware('check_shopping_status')->group(function () {
        Route::get('cart', 'CartController@index')->name('cart');
        Route::get('clear-cart', 'CartController@clearCart')->name('clear-cart');
        Route::post('add-to-cart', 'CartController@addCart')->name('add-to-cart');
        Route::patch('update-cart', 'CartController@updateCart')->name('update-cart');
        Route::delete('remove-cart', 'CartController@removeCart')->name('remove-cart');
        Route::post('cart_checkout', 'CartController@checkoutCart')->name('cart_checkout');
        Route::post('process_payment', 'CartController@processPayment')->name('process_payment');
    });

    //Policy Pages
    Route::get('privacy_policy','PageController@getPrivacyPolicy')->name('privacy_policy');
    Route::get('shipping_and_delivery','PageController@getShippingAndDelivery')->name('shipping_and_delivery');
    Route::get('returns','PageController@getReturns')->name('returns');
    Route::get('sales_policy','PageController@getSalesPolicy')->name('sales_policy');
    Route::get('terms_and_conditions', 'PageController@getTermsAndConditions')->name('terms_and_conditions');

    //Home Pages
    Route::get('/locale/{locale}', 'IndexController@locale')->name('home.language');
    Route::get('/', 'IndexController@index')->name('homepage');
    Route::post('/', 'IndexController@store');
    Route::get('/log-in', 'IndexController@loginPage');

    //Search Pages
    Route::get('search_result','PageController@searchPage')->name('search');

    //Education Pages
    Route::prefix('education')->group(function () {
        Route::get('/', 'ArticlesController@getEducationMenu')->name('education');
        Route::get('articles', 'ArticlesController@index')->name('articles');
        Route::get('articles_content/{id}', 'ArticlesController@getContent')->name('articles_content');
        Route::get('flyers', 'ArticlesController@getPDF')->name('flyers');
        Route::get('flyer_details/{id}', 'ArticlesController@getPDFDetails')->name('flyer_details');
    });

    // foreach (\App\Models\Redirect::where('website', config("app.url"))->get() as $item) {
    //     Route::get('/' . $item->url_path, function () use ($item) {
    //         return redirect($item->redirect, 301);
    //     });
    // }

    Auth::routes();
    Route::post('/login', 'Auth\LoginController@login')->name('login');
    // Route::get('/login', function () {
    //     return redirect(404);
    // });

    Route::post('/get_channel', 'Auth\RegisterController@getChannel')->name('get_channel');
    Route::post('/get_association', 'Auth\RegisterController@getAssociation')->name('get_association');
    Route::post('/get_parent', 'Auth\RegisterController@getParentChannel')->name('get_parent');
    Route::get('/logout', 'Auth\LoginController@logout');
    Route::post('/email_validation', 'Auth\RegisterController@validateEmail')->name('email_validation');
    Route::post('/ahpra_number_validation', 'Auth\RegisterController@validateAHPRA')->name('ahpra_number_validation');
    Route::post('/trading_name_validation', 'Auth\RegisterController@validateTradingName')->name('trading_name_validation');
    Route::post('/referral_check', 'Auth\RegisterController@validateReferralCode')->name('referral_check');
    Route::post('/abn_check', 'Auth\RegisterController@validateABN')->name('abn_check');
    Route::post('/acn_check', 'Auth\RegisterController@validateACN')->name('acn_check');
    Route::post('/phone_check', 'Auth\RegisterController@validatePhone')->name('phone_check');
    Route::post('/address_check', 'Auth\RegisterController@validateAddress')->name('address_check');
    Route::post('/fax_check', 'Auth\RegisterController@validateFax')->name('fax_check');
    Route::get('/select_practitioner', 'Auth\LoginController@toSelectPractitioner')->name('select_practitioner');
    Route::post('/submit_practitioner', 'Auth\LoginController@submitPractitioner')->name('submit_practitioner');
    Route::post('/post_comment', 'CommentsController@store')->name('post_comment');
    Route::post('/phone_existence_validate', 'Auth\RegisterController@validatePhoneExistence')->name('phone_existence_validate');

    Route::middleware('check_profile_status')->group(function () {
//    Route::get('/patient-profile', 'UserController@getRoleInfo')->name('patient-profile');
        Route::get('/practitioner-profile', 'UserController@getRoleInfo')->name('practitioner-profile');
        Route::post('/edit_patient_detail', 'UserController@editPatientInfo')->name('edit_patient_detail');
        Route::post('/update_practitioner_detail', 'UserController@updatePractitionerInfo')->name('update_practitioner_detail');
        Route::post('/update_marketing', 'MarketingController@updateMarketing')->name('update_marketing');
        Route::get('/practitioner-marketing-page', 'MarketingController@getMarketing')->name('practitioner-marketing-page');
//    Route::get('/change-referral-code', 'WWWPageController@getReferralCode')->name('change-referral-code');
        Route::post('/update-referral-code', 'UserController@updateReferralCode')->name('update-referral-code');
//    Route::get('/patient-password-page', 'UserController@getCurrentPassword')->name('patient-password-page');
        Route::post('/update_patient_password', 'UserController@updatePatientPassword')->name('update_patient_password');
        Route::get('/practitioner-password-page', 'UserController@getCurrentPassword')->name('practitioner-password-page');
        Route::post('/update_practitioner_password', 'UserController@updatePractitionerPassword')->name('update_practitioner_password');
        Route::get('/order_list', 'OrderController@index')->name('order-list');
        Route::post('/search_order', 'OrderController@selectOrder')->name('search_order');
    });

//User Info Field Validation
    Route::post('/check_referral_code', 'Auth\RegisterController@validateUpdateReferralCode')->name('check_referral_code');
    Route::post('/phone_update_check', 'Auth\RegisterController@validateUpdatePhone')->name('phone_update_check');
    Route::post('/update_fax_check', 'Auth\RegisterController@validateUpdateFax')->name('update_fax_check');

    Route::post('/address_update_check', 'Auth\RegisterController@validateUpdateAddress')->name('address_update_check');
    Route::post('/search_company_name', 'Auth\RegisterController@searchCompanyName')->name('search_company_name');
    Route::post('/search_business', 'Auth\RegisterController@searchBusiness')->name('search_business');
    Route::post('/select_business', 'Auth\RegisterController@selectBusiness')->name('select_business');
    Route::post('/validate_password', 'Auth\RegisterController@validatePassword')->name('validate_password');


    Route::get('/invoice/{order_number}', 'OrderController@printInvoice')->name('print_invoice');

    Route::post('/reorder_cart', 'CartController@reorderCart')->name('reorder_cart');



});

