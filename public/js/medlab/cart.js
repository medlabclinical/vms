$(document).ready(function() {
    $("#card_info").hide();
    $("#stored_card_info").hide();

    $("#pay_method").change(function() {
        var methodType = $(this).val();

        // 0 = account terms, 1 = credit card, 2 = stored credit card
        if (methodType == 1) {
            $("#stored_card_info").hide();
            $('#card_info').show();
        } else if (methodType == 2) {
            $('#card_info').hide();
            $("#stored_card_info").show();
        } else{
            $("#card_info").hide();
            $("#stored_card_info").hide();
        }
    });

    /**
    * Clear the cart
    */
    $("#clear-cart").on('click', function(e) {

        let clearUrl = $('meta[name="_cclear"]').attr("content");

        Swal.fire({
            icon: 'question',
            title: 'Are you sure you want to clear your cart?',
            showDenyButton: true,
            showCancelButton: true,
            confirmButtonText: `Yes`,
            denyButtonText: `No`,
        }).then((result) => {

            if (result.isConfirmed) {

                $.ajax({
                    url: clearUrl,
                    method: "GET",
                    success: function (response) {
                        window.location.reload();
                    }
                });
            }
        });
    });

    /**
    * Clear the cart
    */
    $("#continue-shopping").on('click', function(e) {
        window.location.href = "/products";
    });

    /**
    * Update quantity of the cart
    */
    $("body").on('change', '.update-cart', function (e) {
        e.preventDefault();

        var cartId  = $("#temp_cart_id").val();
        var itemId  = $(this).data("id");
        var itemQty = $(this).val();

        Swal.showLoading();

        let updateUrl = $('meta[name="_cupdate"]').attr("content");
        let csrfToken = $('meta[name="_ctoken"]').attr("content");

        $.ajax({
            url: updateUrl,
            method: "PATCH",
            data: {_token: csrfToken, itemId: itemId, itemQty: itemQty, cartId: cartId},
            dataType: "json",
            success: function (response) {
                console.log(response);
                if (response.count==0) {
                    location.reload();
                } else {
                    $("#cart_table").html(response.html);
                    $("#current_amount").html(response.count);
                }
            }
        }).done(function() {
            Swal.close();
        });
    });

    /**
    * Remove an item
    */
    $("body").on('click', ".remove-cart", function (e) {
        e.preventDefault();

        var cartId  = $("#temp_cart_id").val();
        var thisId = $(this).data( "id" );

        let csrfToken = $('meta[name="_ctoken"]').attr("content");
        let removeUrl = $('meta[name="_cremove"]').attr("content");

        Swal.fire({
            icon: 'question',
            title: 'Are you sure to remove this item?',
            showDenyButton: true,
            showCancelButton: true,
            confirmButtonText: `Yes`,
            denyButtonText: `No`,
        }).then((result) => {

            if (result.isConfirmed) {
                $.ajax({
                    url: removeUrl,
                    method: "DELETE",
                    data: {_token: csrfToken, removeRowId: thisId, cartId: cartId},
                    dataType: "json",
                    success: function (response) {
                        if (response.count==0) {
                            location.reload();
                        } else {
                            $("#cart_table").html(response.html);
                            $("#current_amount").html(response.count);
                        }
                    }
                })
                .done(function() {
                    Swal.close();
                });
            }
        });
    });
});
function submitCart()
{

    let allAreFilled = true;
    $('#ajax-form').find(":input[required]:visible").each(function (i, requiredField) {

        if ($(requiredField).val() == '') {

            var thisTitle = $(requiredField).data('printname') + ' is a required field';
            Swal.fire({
                icon: 'error',
                title: thisTitle
            });

            allAreFilled = false;
        }
    })
    if(!document.getElementById('use_delivery').value){
        var thisTitle = $('#use_delivery').data('printname') + ' is a required field';
        Swal.fire({
            icon: 'error',
            title: thisTitle
        });
        allAreFilled = false;
    }

    if (allAreFilled)
    {
        event.preventDefault();
        let serialForm = $('#ajax-form').find("input[type='hidden'], :input:not(:hidden)").serialize();
        // console.log(serialForm);

        Swal.fire({
            title: 'Submitting Order',
            text: "Please wait as we process your order .. ",
            allowOutsideClick: false,
            allowEscapeKey: false,
            showConfirmButton: false,
            showCloseButton: false,
            showCancelButton: false,
            onOpen: () => {
                Swal.showLoading();
                $.ajax({
                    type: "POST",
                    url: $('#ajax-form').attr('action'),
                    data: $('#ajax-form').find("input[type='hidden'], :input:not(:hidden)").serialize(),
                    timeout: 60000,     // set timeout to 20 seconds
                    success: function (data) {
                        if (data.msg == 'Successfully') {
                            Swal.fire({
                                icon: 'success',
                                title: 'Payment Successfully!',
                                text: 'Thank you for your order. '+' Your cart reference is '+ data.cart_id,
                            }).then((result) => {
                                location.reload();
                            })
                        } else {
                            Swal.fire({
                                icon: 'warning',
                                title: 'Failed to process payment',
                                text: data.msg,
                            }).then((result) => {
                                // do nothing
                            })
                        }
                    },
                    error: function(data) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Timeout Error',
                            text: "The order may have processed. Click OK to proceed.",
                        }).then((result) => {
                            // do nothing
                        })
                    }
                });
            }
        })

    }
}

function camelize(str) {
  return str.replace(/(?:^\w|[A-Z]|\b\w)/g, function(word, index) {
    return index === 0 ? word.toLowerCase() : word.toUpperCase();
  }).replace(/\s+/g, '');
}

function changeDelivery()
{
    var x = $("#use_delivery").val();
    var id = x.substring(0,x.indexOf('-'));
    var country = x.substring(x.indexOf('-')+1,x.length);

    if (id==0) {
        $('#shippingForm').show();
    } else if(country != 'Australia') {
        $('#use_delivery').val(0);
        changeDelivery();
        Swal.fire({
            icon: 'error',
            title: 'Uh-oh',
            text: 'International ordering is temporarily unavailable. We apologise for the inconvenience and thank you for your understanding.​',
        })
    } else {
        $('#shippingForm').hide();
    }
}

function stopAutoFill() {
    document.getElementById('street_address').removeAttribute('autocomplete');
    document.getElementById('street_address').setAttribute('autocomplete', 'googleignoreautofill');
    document.getElementById('locality').removeAttribute('autocomplete');
    document.getElementById('locality').setAttribute('autocomplete', 'googleignoreautofill');
}

// var international = document.querySelector("input[name=international]");
google.maps.event.addDomListener(window, 'load', function () {

    var places = new google.maps.places.Autocomplete(document
        .getElementById('street_address'), {
        componentRestrictions: {
            country: "au",
        }
    });
    // international.addEventListener('change', function () {
    //     if (this.checked) {
    //         places.setComponentRestrictions({'country': ['nz', 'gb', 'us', 'ca', 'jp', 'za', 'in', 'sg', 'kr']});
    //     } else {
    //         places.setComponentRestrictions({'country': 'au'});
    //     }
    // });
    addGoogleListenerTotal(places);


});

google.maps.event.addDomListener(window, 'load', function () {
    var locality = new google.maps.places.Autocomplete((document
        .getElementById('locality')), {
        types: ['(regions)'],
        componentRestrictions: {
            country: "au",
        }
    });
    // international.addEventListener('change', function () {
    //     if (this.checked) {
    //         locality.setComponentRestrictions({'country': ['nz', 'gb', 'us', 'ca', 'jp', 'za', 'in', 'sg', 'kr']});
    //     } else {
    //         locality.setComponentRestrictions({'country': 'au'});
    //     }
    // });
    addGoogleListenerLocality(locality);


});

function addGoogleListenerLocality(locality) {
    google.maps.event.addListener(locality, 'place_changed', function () {
        var place = locality.getPlace();
        // var value = place.address_components;

        // var  value = address.split(",");
        // console.log(place.address_components)
        // console.log(place.address_components);
        var componentForm = {
            // street_number: 'short_name',
            // route: 'long_name',
            locality: 'long_name',
            administrative_area_level_1: 'short_name',
            country: 'long_name',
            postal_code: 'short_name'
        };
        // document.getElementById('street_address').value = '';

        // document.getElementById('administrative_area_level_1').value = '';
        // document.getElementById('country').value = '';
        // document.getElementById('postal_code').value = '';
        var a = 0;
        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (componentForm[addressType]) {
                var val = place.address_components[i][componentForm[addressType]];
                if (addressType == 'street_number') {
                    document.getElementById('street_address').value = val;
                } else if (addressType == 'route') {
                    document.getElementById('street_address').value += (' ' + val);
                } else if (addressType == 'locality') {
                    document.getElementById('locality').value = val;
                    a = 1;
                } else {
                    document.getElementById(addressType).value = val;
                }
            }
        }
        if (a == 0) {
            document.getElementById('locality').value = '';
        }
        if (document.getElementById('locality').value == '' ||
            document.getElementById('administrative_area_level_1').value == '' ||
            document.getElementById('country').value == '') {
        }
    });
}

function addGoogleListenerTotal(places) {

    google.maps.event.addListener(places, 'place_changed', function () {
        var place = places.getPlace();
        // var value = place.address_components;

        // var  value = address.split(",");
        // console.log(place.address_components)
        var componentForm = {
            street_number: 'short_name',
            route: 'long_name',
            locality: 'long_name',
            administrative_area_level_1: 'short_name',
            country: 'long_name',
            postal_code: 'short_name'
        };

        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (componentForm[addressType]) {
                var val = place.address_components[i][componentForm[addressType]];
                if (addressType == 'street_number') {
                    document.getElementById('street_address').value = val;
                } else if (addressType == 'route') {
                    document.getElementById('street_address').value += (' ' + val);
                } else {
                    document.getElementById(addressType).value = val;
                }
            }
        }
        if (document.getElementById('locality').value == '' ||
            document.getElementById('administrative_area_level_1').value == '' ||
            document.getElementById('country').value == '') {
        }
    });

}
