/**  Input validation for register form
 1. Validate on practitioner form
 2. for each btn add event listener on click to check
 3. for submit button add e.prevent
 */
'use-strict';

// For practitioner

// declare error message variable
const errorMessage = 'Required field';


/**
 *
 * Doctor 1st block of code
 *
 */
const personal_info_check = document.getElementById('practitioner_personal_info');
const practitioner_ahpra_number = document.getElementById('practitioner_ahpra_number');
const practitioner_email = document.getElementById('practitioner_email');
// problem 1: practitioner_titleValue is already selected
const practitioner_firstname = document.getElementById('practitioner_firstname');
const practitioner_lastname = document.getElementById('practitioner_lastname');

// these two variables are aleady defined in register page
const _practitioner_phone = document.getElementById('practitioner_phone');
const _practitioner_mobile = document.getElementById('practitioner_mobile');
const job_title = document.getElementById('job_title');


document.getElementById('personal_info_check').onclick = function () {
    let allAreFilled = true;

    personal_info_check.querySelectorAll("[required]").forEach(function (i) {
        if (i.value == '') {
            setErrorFor(i, errorMessage);
            allAreFilled = false
        } else {
            setSuccessFor(i);
        }
    })
    if (_practitioner_phone.value == '' && _practitioner_mobile.value == '') {
        allAreFilled = false;
        setErrorFor(document.getElementById('practitioner_phone').parentElement, errorMessage);
    } else if ($('#practitioner_phone').attr('data-status') == 'false') {
        allAreFilled = false
    } else if ($('#practitioner_mobile').attr('data-status') == 'false') {
        allAreFilled = false
    } else {
        setSuccessFor(document.getElementById('practitioner_phone').parentElement);
    }

    if (allAreFilled) {
        document.getElementById('personal_info_check').style.display = 'none';
        document.getElementById('practitionerBusinessInfo').style.display = 'block';
        // document.getElementById('er_practitioner_phone').style.display = 'none';
    }


    // 1st button successful


}


/**
 *
 * Doctor 2nd block of code
 *
 */

const business_info_check = document.getElementById('business_info_check');

const business_trading_name = document.getElementById('business_trading_name');
const primary_channel = document.getElementById('primary_channel');
const second_channel = document.getElementById('second_channel');
const _business_phone = document.getElementById('business_phone');


/**
 *
 * Pharmacist 1nd block of code
 *
 */



document.getElementById("pharma_business_info_check").onclick = function () {
    let allAreFilled = true;
    document.getElementById("pharmaBusinessInfo").querySelectorAll("[required]").forEach(function (i) {
        if (i.id == 'pharma_business_phone') {
            if (i.value == '') {
                setErrorFor(i.parentElement, errorMessage);
                allAreFilled = false
            } else if ($('#pharma_business_phone').attr('data-status') == 'false') {
                allAreFilled = false
            } else {
                setSuccessFor(i.parentElement);
            }
        } else {
            if (i.value == '') {
                setErrorFor(i, errorMessage);
                allAreFilled = false
            } else {
                setSuccessFor(i);
            }
        }
    })
    console.log(allAreFilled)
    if ($('#pharma_fax').attr('data-status') == 'false') {
        allAreFilled = false
    } else {
        setSuccessFor(document.getElementById('pharma_fax').parentElement);
    }
    if (allAreFilled) {
        //     Swal.fire({
        //         icon: 'error',
        //         title: 'Please fill all the fields before next step',
        //     })
        // } else {
        if (document.getElementById('pharma_country').value != 'Australia') {
            document.getElementById('if_have_number').style.display = 'none';
            document.getElementById('pharma_personal_info_detail').style.display = 'block';
            $("#pharma_ahpra_number").prop('required', false);
            $("#pharma_association_number").prop('required', true);
            $("#pharma_qualification").prop('required', true);
            $("#pharma_assocaition_name").prop('required', true);
            document.getElementById('pharma_association_area').style.display = 'block';
        }
        document.getElementById('pharma_business_info_check').style.display = 'none';
        document.getElementById('pharma_personal_info').style.display = 'block';
        if (document.getElementById('pharmaBusinessInfo').style.display == 'none') {
            document.getElementById('pharma_personal_info_check_agree').style.display = 'block';
        } else {
            document.getElementById('pharma_personal_info_check_account').style.display = 'block';
        }

        $("#pharma_password").val('');
        $("#pharma_password-confirm").val('');
        document.getElementById('pharma_firstname').focus();
    }
};

document.getElementById("pharma_personal_info_check_account").onclick = function () {
    let allAreFilled = true;
    document.getElementById("pharma_personal_info").querySelectorAll("[required]").forEach(function (i) {
        if (i.value == '') {
            setErrorFor(i, errorMessage);
            allAreFilled = false
        } else {
            setSuccessFor(i);
        }
    })
    if (document.getElementById('pharma_phone').value == '' && document.getElementById('pharma_mobile').value == '') {

        allAreFilled = false;
        setErrorFor(document.getElementById('pharma_phone').parentElement, errorMessage);
    } else if ($('#pharma_phone').attr('data-status') == 'false') {

        allAreFilled = false
    } else if ($('#pharma_mobile').attr('data-status') == 'false') {

        allAreFilled = false
    } else {
        setSuccessFor(document.getElementById('pharma_phone').parentElement);
    }
    if (allAreFilled) {
        //     Swal.fire({
        //         icon: 'error',
        //         title: 'Please fill all the fields before next step',
        //     })
        // } else {
        document.getElementById('pharma_personal_info_check_account').style.display = 'none';
        document.getElementById('pharma_accountTermsForm').style.display = 'block';
        document.getElementById('pharma_account_term_info_check').style.display = 'block';

    }
};

// declare function here'

const form = document.getElementById('ajax-form');
const practitioner_password = document.getElementById("practitioner_password");
const practitioner_password_confirm = document.getElementById("practitioner_password-confirm");
const practitioner_agree_term = document.getElementById("practitioner_agree_term");

form.addEventListener('submit', (e) => {
    e.preventDefault();
    // checkInputsC();

    let allAreFilled = true;

    if (document.getElementById('pharma_firstname').hasAttribute('required')) {
        document.getElementById("pharma_agree_to_term_info").querySelectorAll('.check_manually').forEach(function (i) {
            if (i.value == '') {
                setErrorFor(i, errorMessage);
                allAreFilled = false
            } else {
                setSuccessFor(i);
            }
        })
        if (!document.getElementById("pharma_agree_term").checked) {
            allAreFilled = false;
            document.getElementById('er_pharma_agree_term').style.display = 'block';
            document.getElementById('er_pharma_agree_term').innerText = 'Required Field';
        } else {
            document.getElementById('er_pharma_agree_term').style.display = 'none';
        }
        $('#pharma_personal_info_check_account').click();
        document.getElementById('pharma_account_term_info_check').style.display = 'none';
    } else {
        document.getElementById("agree_to_term_info").querySelectorAll('.check_manually').forEach(function (i) {
            if (i.value == '') {
                setErrorFor(i, errorMessage);
                allAreFilled = false
            } else {
                setSuccessFor(i);
            }
        })
        if (!document.getElementById("practitioner_agree_term").checked) {
            allAreFilled = false;
            document.getElementById('er_practitioner_agree_term').style.display = 'block';
            document.getElementById('er_practitioner_agree_term').innerText = 'Required Field';
        } else {
            document.getElementById('er_practitioner_agree_term').style.display = 'none';
        }
        $('#personal_info_check').click();
        document.getElementById('pharma_account_term_info_check').style.display = 'none';
    }
    if (allAreFilled) {
        submitRegisterForm();

    }


});


function setErrorFor(input, message) {
    // x refers to each row
    const x = input.parentElement;
    const small = x.querySelector('small');
    // console.log(small);
    small.innerText = message;
    small.style.display = 'block';

}

function setSuccessFor(input) {
    // x refers to each row
    const x = input.parentElement;
    const small = x.querySelector('small');
    small.style.display = 'none';

}


// function  validateInputs(){
//     alert('Here and There');
//     const practitioner_password = document.getElementById("practitioner_password");
//     const practitioner_password_confirm = document.getElementById("practitioner_password");
//     const practitioner_agree_term = document.getElementById("practitioner_agree_term");

//     let allAreFilled = true;
//     // document.getElementById("agree_to_term_info").querySelectorAll("[required]").forEach(function (i) {
//     //     if (!allAreFilled) return;
//     //     if (!i.value) allAreFilled = false;
//     // })
//     if (!allAreFilled) {
//         checkInputsC();
//     } else {
//         // document.getElementById('business_info_check').style.display = 'none';
//         // document.getElementById('agree_to_term_info').style.display = 'block';
//         // document.getElementById('er_business_trading_name').style.display = 'none';
//         // document.getElementById('er_primary_channel').style.display = 'none';
//         // document.getElementById('er_second_channel').style.display = 'none';
//         // document.getElementById('er_business_phone').style.display = 'none';
//         // console.log('test removing class sucess');
//     }

//     function checkInputsC() {
//         alert('test success 3');

//     }
// };




