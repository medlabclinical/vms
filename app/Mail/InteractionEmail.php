<?php

namespace App\Mail;

use App\Models\InteractionModel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class InteractionEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $interaction;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(InteractionModel $interaction)
    {
        $this->interaction = $interaction;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // Obtain the person details
        $this->interaction->person_id = $this->interaction->person()->first();

        // Obtain the customer details
        $this->interaction->customer_id = $this->interaction->unleashedcustomer()->first();

        // Obtain the staff details
        $this->interaction->user_id = $this->interaction->user()->first();

        $date = '-';
        $time = '-';
        if ($this->interaction->scheduled_datetime!='') {
            $split = explode(" ", $this->interaction->scheduled_datetime);
            $date = date("d-m-Y", strtotime($split[0]));
            $time = $split[1];
        }

        return $this->subject("Interaction Created")
                    ->markdown('email.interaction')
                    ->with([
                        'business'     => $this->interaction->customer_id,
                        'person'       => $this->interaction->person_id,
                        'staff'        => $this->interaction->user_id,
                        'type'         => $this->interaction->interaction_type,
                        'notes'        => $this->interaction->notes,
                        'order_placed' => ($this->interaction->order_placed==1) ? 'Yes' : 'No',
                        'date'         => $date,
                        'time'         => $time,
                        'duration'     => $this->interaction->duration,
                    ]);
    }
}
