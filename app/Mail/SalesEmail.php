<?php

namespace App\Mail;

use App\Models\ShoppingCartModel;
use App\Models\ShoppingCartPaymentModel;
use App\Models\UnleashedCustomer;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SalesEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $shopping_cart_payment;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(ShoppingCartPaymentModel $shopping_cart_payment)
    {
        $this->shopping_cart_payment = $shopping_cart_payment;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $cart = ShoppingCartModel::where('cart_id', $this->shopping_cart_payment->cart_id)->first();
        $unleashedcustomer = UnleashedCustomer::where('guid', $cart->customer_id)->first();

        return $this->subject("Sales Created")
                    ->markdown('emails.salesorder')
                    ->with([
                        'clientname'       => $unleashedcustomer->customer_name,
                        'ordernumber'      => $cart->unleashed_orderno,
                        'drug_ordernumber' => $cart->unleashed_drug_orderno,
                        'receipt_number'   => $this->shopping_cart_payment->receipt_number,
                        'total_paid'       => $this->shopping_cart_payment->total_paid,
                    ]);
    }
}
