<?php

namespace App\Mail;

use App\Models\CustomerWholesalerModel;
use App\Models\PersonModel;
use App\Models\ShoppingCartItemModel;
use App\Models\ShoppingCartModel;
use App\Models\ShoppingCartPaymentModel;
use App\Models\UnleashedCustomer;
use App\Models\User;
use App\Models\WholesalerModel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class WholesalerEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $shopping_cart_payment;
    protected $email_to;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(ShoppingCartPaymentModel $shopping_cart_payment, $email_to)
    {
        $this->shopping_cart_payment = $shopping_cart_payment;
        $this->email_to = $email_to;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $cart = ShoppingCartModel::where('cart_id', $this->shopping_cart_payment->cart_id)->first();

        // Get the customer details
        $unleashedcustomer = UnleashedCustomer::where('guid', $cart->customer_id)->first();

        // Get the person name
        $person = PersonModel::where('id', $cart->person_id)->first();

        // Get the address of shipping
        $split = explode("::", $cart->cart_delivery_address);
        $classification_name = $split[4];

        // Get the wholesalerid
        $wholesaler = WholesalerModel::where('name', $this->shopping_cart_payment->selected_wholesaler)->first();

        // Get the business wholesaler registration number
        $registration_number = CustomerWholesalerModel::where('customer_id', $cart->customer_id)->where('wholesaler_id', $wholesaler->id)->first();

        // Get the rep
        $rep = User::where('id', $cart->user_id)->first();

        // Generate the CSV and upload into s3 bucket
        $cart_items = $html_cart_items = ShoppingCartItemModel::WholesaleProducts($cart->cart_id, $wholesaler->id)->get('Product','Size', 'APICode', 'Discount', 'Qty', 'RRPInc')->toArray();

        // Add the headers
        array_unshift($cart_items, array_keys($cart_items[0]));

        // Temp create the csv file
        $filename = '../storage/app/' . time() . '.csv';
        $file_handler = fopen($filename, 'w');
        foreach ($cart_items as $row) {
            fputcsv($file_handler, $row);
        }
        fclose($file_handler);

        // Upload the file
        $s3_filename = time() . "-" . $this->shopping_cart_payment->cart_id . '.csv';
        $s3_filepath = 'sales_order/' . $s3_filename;
        $s3 = Storage::disk('s3private');
        $s3->put($s3_filepath, file_get_contents($filename), 'private');
        $url = $s3->temporaryUrl($s3_filepath, now()->addMinutes(5));

        unlink($filename);

        $receivers[] = 'wilma_smith@medlab.co';
        $receivers[] = 'ian_curtinsmith@medlab.co';

        return $this->subject("Medlab Turnover order [#SO-" . $cart->cart_id . "]")
                ->markdown('email.wholesaleorder')
                    ->attach($url, ['as' => $s3_filename, 'mime' => 'text/csv'])
                    ->cc($receivers)
                    ->from($rep->email)
                    ->with([
                        'selected_wholesaler'               => $this->shopping_cart_payment->selected_wholesaler,
                        'person_name'                       => $person->first_name . ' ' . $person->last_name,
                        'business_name'                     => $unleashedcustomer->customer_name,
                        'ws_rego_number'                    => $registration_number->registration_number,
                        'business_secondary_classification' => $classification_name,
                        'cart_items'                        => $html_cart_items,
                        'rep_email'                         => $rep->email,
                        'distributor_email'                 => $this->email_to
                    ]);
    }
}
