<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PatientAddress extends Model
{
    protected $table = 'patient_address';
    protected $fillable = [
        'patient_id', 'address_type', 'address_line_1', 'address_line_2', 'suburb', 'postcode', 'city','state','country',
        'longitude','latitude','addresssearch'
    ];

}
