<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AssociationModel extends Model
{
    use SoftDeletes;

    protected $guard_name = 'web';
    protected $table = 'associations';
    protected $primaryKey = 'id';


    /**
     * A single person can have multiple associations
     *
     * @return void
     */
    public function persons()
    {
        return $this->hasMany(PersonModel::class, 'id');
    }

    /**
     * Return all list
     *
     * @return relationship
     */
    public function scopeAssociationList()
    {
        $keypair = null;
        $query = $this->where("deleted_at", NULL)->get();
        foreach ($query as $item) {
            $keypair["{$item->id}"] = $item->name;
        }
        return $keypair;
    }
    public function primaryClassification()
    {
        return $this->belongsToMany(PrimaryClassification::class,'primary_classification_association','association_id','primary_classification_id');
    }
}
