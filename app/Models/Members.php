<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Members extends Model
{
    protected $table = 'members';

    public function member_type(){
        return $this->belongsToMany(MemberType::class,'product_multiple_relation','member_id','member_type_id');
    }
    public function article(){
        return $this->belongsToMany(Articles::class,'product_multiple_relation','member_id','article_id');
    }
    public function image(){
        return $this->hasOne('App\Models\MediaLibrary','id','picture');
    }
    public function publicationsandpresentation()
    {
        return $this->belongsToMany(Publications::class,'member_publications_and_presentations','member_id','publications_and_presentations_id');
    }
}
