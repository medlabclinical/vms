<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class OrderModel extends Model
{
    use \Awobaz\Compoships\Compoships;

    protected $guard_name = 'web';


    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table      = 'orders';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'guid';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType    = 'string';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
//    protected $casts      = [
//        'order_date'            => 'date',
//        'required_date'         => 'date',
//        'completed_date'        => 'date',
//        'payment_due_date'      => 'date',
//        'unleashed_created_on'  => 'datetime',
//        'unleashed_modified_on' => 'datetime',
//    ];

    /**
     * Order lines
     *
     * @return object
     */
    public function orderlines()
    {
        return $this->hasMany(OrderLinesModel::class, ['order_number','unleashed_id'], ['order_number','unleashed_id']);
    }

    public function business()
    {
        return $this->belongsTo(UnleashedCustomer::class,'customer_code','customer_code');
    }

    /**
     *  Search for payment type
     *  Keyword - string
     *  type - business / payment search yields different data
     */
    public function scopePaymentSearchKeyword($query, $keyword, $type)
    {
        $return = array();

        // returns exactly the same thing just different style and lesser queries
        if ($type == 'business')
        {
            $result = $query->where(function ($query) use ($keyword) {
                            $query->where('orders.customer_name', 'LIKE', '%' . $keyword . '%')
                                  ->orWhere('orders.customer_code', 'LIKE', '%' . $keyword . '%');
                        })
                        ->join('ucustomers as V', function ($join) {
                            $join->on('V.customer_code', '=', 'orders.customer_code')
                                 ->whereNull('V.deleted_at');
                        })
                        ->select(DB::raw("
                                V.customer_code as customer_code,
                                V.customer_name as customer_name,
                                '' as stored_cards
                        "))
                        ->groupBy(DB::raw("V.customer_code, V.customer_name"))
                        ->limit(200)
                        ->get()
                        ->toArray();

            foreach ($result as &$pointer)
            {
                $customer_code               = $pointer["customer_code"];
                $customer_name               = $pointer["customer_name"];
                $pointer["stored_cards"]     = ShoppingCartStoreCardModel::CardListByCustomerCode($customer_code)->get()->toArray();
                $pointer['order_display']    = '[' . $customer_code . '] ' . $customer_name;
                $pointer['customer_display'] = '[' . $customer_code . '] ' . $customer_name;
                $pointer['customer_code']    = $customer_code;
                $pointer['order_number']     = '';
                $pointer['business_brand']   = '';
                $return["items"][]           = $pointer;
            }
        }
        else
        {
            $result = $query->where('orders.order_number', 'LIKE', '%' . $keyword . '%')
                        ->leftJoin('ucustomers as V', function ($join) {
                            $join->on('V.customer_code', '=', 'orders.customer_code')
                                 ->whereNull('V.deleted_at');
                        })
                        ->leftJoin('ucustomers as P', function ($join) {
                            $join->on('P.pharma_customer_code', '=', 'orders.customer_code')
                                 ->whereNull('P.deleted_at');
                        })
                        ->select(DB::raw("
                                V.customer_code as vitamin_customer_code,
                                P.customer_code AS pharma_customer_code,
                                orders.customer_code as original_customer_code,
                                orders.unleashed_id as unleashed_id,
                                V.customer_name as vitamin_customer_name,
                                P.customer_name as pharma_customer_name,
                                orders.order_number,
                                orders.total,
                                '' as stored_cards
                        "))
                        ->groupBy(DB::raw("V.customer_code, V.customer_name, P.customer_code, P.customer_name, orders.customer_code, total, order_number, unleashed_id"))
                        ->limit(200)
                        ->get()
                        ->toArray();

            foreach ($result as &$pointer)
            {
                $customer_code               = ($pointer["vitamin_customer_code"] == '') ? $pointer["pharma_customer_code"] : $pointer["vitamin_customer_code"];
                $customer_name               = ($pointer["vitamin_customer_name"] == '') ? $pointer["pharma_customer_name"] : $pointer["vitamin_customer_name"];
                $brand                       = ($pointer["unleashed_id"] == '2bd9c34b-ad75-4a17-a1f1-83ce0fcc33d4') ? 'V' : 'P';
                $pointer["stored_cards"]     = ShoppingCartStoreCardModel::CardListByCustomerCode($customer_code)->get()->toArray();
                $pointer['order_display']    = $pointer["order_number"] . '-' . $brand . '-' . $customer_name;
                $pointer['customer_display'] = '[' . $customer_code . '] ' . $customer_name;
                $pointer['customer_code']    = $customer_code;
                $pointer['business_brand']   = $brand;
                if ($customer_name!='') {
                    $return["items"][]           = $pointer;
                }
            }
        }

        return json_encode($return);
    }

    /**
     * Sales Orders Monthly for this Financial year
     *
     * @return void
     */
    public function scopeCurrentFYData($query, $customer_code, $is_pharma=true)
    {
        $year_s = (date("n")<=6) ? date("Y")-1 : date("Y");
        $year_e = (date("n")<=6) ? date("Y") : date("Y")+1;

        $from_date = $year_s .'-07-01';
        $to_date = $year_e .'-07-01';

        if ($is_pharma)
        {
            $query = $query->where('customer_code', $customer_code)
                    ->where('order_date', '>=', $from_date)
                    ->where('order_date', '<', $to_date)
                    ->where('orders.order_status', '!=', 'Deleted')
                    ->where('unleashed_id', '=', '6c9a056b-3713-4cfc-bfdd-50be48e0c275')
                    ->select(DB::raw('sum(sub_total) AS total_spent, YEAR(order_date) AS year_value, MONTH(order_date) AS month_value'))
                    ->groupBy(DB::raw('YEAR(order_date), MONTH(order_date)'));
        }
        else
        {
            // Get indirect sales
            $indirect_sales = IndirectSales::whereNotNull('product_wholesaler.registration_number')
                                        ->join('product_wholesaler', function ($join) {
                                                $join->on('indirect_sales.product_code', '=', 'product_wholesaler.registration_number')
                                                      ->whereNull('product_wholesaler.deleted_at');
                                            })
                                        ->join('customer_wholesalers', function ($join) {
                                                $join->on('indirect_sales.customer_number', '=', 'customer_wholesalers.registration_number')
                                                      ->whereNull('customer_wholesalers.deleted_at');
                                            })
                                        ->join('products', 'products.guid', '=', 'product_wholesaler.product_guid')
                                        ->join('ucustomers', 'ucustomers.guid', '=', 'customer_wholesalers.customer_id')
                                        ->where('indirect_sales.invoice_date', '>=', $from_date)
                                        ->where('indirect_sales.invoice_date', '<', $to_date)
                                        ->where('ucustomers.customer_code', '=', $customer_code)
                                        ->where('indirect_sales.true_sales_price', '>', 0)
                                        ->groupBy('indirect_sales.invoice_date','indirect_sales.invoice_number')
                                        ->select(DB::raw("
                                                            SUM(indirect_sales.true_sales_price) AS total_spent,
                                                            YEAR(indirect_sales.invoice_date) AS year_value,
                                                            MONTH(indirect_sales.invoice_date) AS month_value
                                                        ")
                                                );

            $query = $query->where('customer_code', $customer_code)
                    ->where('order_date', '>=', $from_date)
                    ->where('order_date', '<', $to_date)
                    ->where('orders.order_status', '!=', 'Deleted')
                    ->where('unleashed_id', '=', '2bd9c34b-ad75-4a17-a1f1-83ce0fcc33d4')
                    ->select(DB::raw('sum(sub_total) AS total_spent, YEAR(order_date) AS year_value, MONTH(order_date) AS month_value'))
                    ->union($indirect_sales)
                    ->groupBy(DB::raw('YEAR(order_date), MONTH(order_date)'));
        }

        return $query;
    }

    /**
     * Sales Orders Monthly for this Financial year
     *
     * @return void
     */
    public function scopeLastFYData($query, $customer_code, $is_pharma=true)
    {
        $year_s = (date("n")<=6) ? date("Y")-2 : date("Y")-1;
        $year_e = (date("n")<=6) ? date("Y")-1 : date("Y");

        $from_date = $year_s .'-07-01';
        $to_date = $year_e .'-07-01';

        if ($is_pharma)
        {

            $query = $query->where('customer_code', $customer_code)
                    ->select(DB::raw('sum(sub_total) as total_spent, YEAR(order_date) as year_value, MONTH(order_date) as month_value'))
                    ->where('order_date', '>=', $from_date)
                    ->where('order_date', '<', $to_date)
                    ->where('orders.order_status', '!=', 'Deleted')
                    ->where('unleashed_id', '=', '6c9a056b-3713-4cfc-bfdd-50be48e0c275')
                    ->groupBy(DB::raw('YEAR(order_date), MONTH(order_date)'));
        }
        else
        {
            // Get indirect sales
            $indirect_sales = IndirectSales::whereNotNull('product_wholesaler.registration_number')
                                        ->join('product_wholesaler', function ($join) {
                                                $join->on('indirect_sales.product_code', '=', 'product_wholesaler.registration_number')
                                                      ->whereNull('product_wholesaler.deleted_at');
                                            })
                                        ->join('customer_wholesalers', function ($join) {
                                                $join->on('indirect_sales.customer_number', '=', 'customer_wholesalers.registration_number')
                                                      ->whereNull('customer_wholesalers.deleted_at');
                                            })
                                        ->join('products', 'products.guid', '=', 'product_wholesaler.product_guid')
                                        ->join('ucustomers', 'ucustomers.guid', '=', 'customer_wholesalers.customer_id')
                                        ->where('indirect_sales.invoice_date', '>=', $from_date)
                                        ->where('indirect_sales.invoice_date', '<', $to_date)
                                        ->where('ucustomers.customer_code', '=', $customer_code)
                                        ->where('indirect_sales.true_sales_price', '>', 0)
                                        ->groupBy('indirect_sales.invoice_date','indirect_sales.invoice_number')
                                        ->select(DB::raw("
                                                            SUM(indirect_sales.true_sales_price) AS total_spent,
                                                            YEAR(indirect_sales.invoice_date) AS year_value,
                                                            MONTH(indirect_sales.invoice_date) AS month_value
                                                        ")
                                                );

            $query = $query->where('customer_code', $customer_code)
                    ->select(DB::raw('sum(sub_total) as total_spent, YEAR(order_date) as year_value, MONTH(order_date) as month_value'))
                    ->where('order_date', '>=', $from_date)
                    ->where('order_date', '<', $to_date)
                    ->where('orders.order_status', '!=', 'Deleted')
                    ->where('unleashed_id', '=', '2bd9c34b-ad75-4a17-a1f1-83ce0fcc33d4')
                    ->union($indirect_sales)
                    ->groupBy(DB::raw('YEAR(order_date), MONTH(order_date)'));
        }

        return $query;
    }

    /**
     * Sales Orders Monthly for this Financial year (raw data)
     *
     * @return void
     */
    public function scopeCurrentFYRaw($query, $customer_code, $is_pharma=true)
    {
        $year_s = (date("n")<=6) ? date("Y")-1 : date("Y");
        $year_e = (date("n")<=6) ? date("Y") : date("Y")+1;

        $from_date = $year_s .'-07-01';
        $to_date = $year_e .'-07-01';

        if ($is_pharma)
        {
            $query = $query->where('customer_code', $customer_code)
                    ->select(DB::raw('sub_total as total, order_number, order_status, order_date, unleashed_id, YEAR(order_date) as year_value, MONTH(order_date) as month_value'))
                    ->where('order_date', '>=', $from_date)
                    ->where('orders.order_status', '!=', 'Deleted')
                    ->where('unleashed_id', '=', '6c9a056b-3713-4cfc-bfdd-50be48e0c275')
                    ->where('order_date', '<', $to_date);
        }
        else
        {
            // Get indirect sales
            $indirect_sales = IndirectSales::whereNotNull('product_wholesaler.registration_number')
                                        ->join('product_wholesaler', function ($join) {
                                                $join->on('indirect_sales.product_code', '=', 'product_wholesaler.registration_number')
                                                      ->whereNull('product_wholesaler.deleted_at');
                                            })
                                        ->join('customer_wholesalers', function ($join) {
                                                $join->on('indirect_sales.customer_number', '=', 'customer_wholesalers.registration_number')
                                                      ->whereNull('customer_wholesalers.deleted_at');
                                            })
                                        ->join('products', 'products.guid', '=', 'product_wholesaler.product_guid')
                                        ->join('ucustomers', 'ucustomers.guid', '=', 'customer_wholesalers.customer_id')
                                        ->where('indirect_sales.invoice_date', '>=', $from_date)
                                        ->where('indirect_sales.invoice_date', '<', $to_date)
                                        ->where('ucustomers.customer_code', '=', $customer_code)
                                        ->where('indirect_sales.true_sales_price', '>', 0)
                                        ->groupBy('indirect_sales.invoice_date','indirect_sales.invoice_number')
                                        ->select(DB::raw("
                                                            SUM(indirect_sales.true_sales_price) AS total,
                                                            indirect_sales.invoice_number AS order_number,
                                                            'Indirect' AS order_status,
                                                            indirect_sales.invoice_date AS order_date,
                                                            '2bd9c34b-ad75-4a17-a1f1-83ce0fcc33d4' AS unleashed_id,
                                                            YEAR(indirect_sales.invoice_date) AS year_value,
                                                            MONTH(indirect_sales.invoice_date) AS month_value
                                                        ")
                                                );

            $query = $query->where('customer_code', $customer_code)
                    ->select(DB::raw('sub_total as total, order_number, order_status, order_date, unleashed_id, YEAR(order_date) as year_value, MONTH(order_date) as month_value'))
                    ->where('order_date', '>=', $from_date)
                    ->where('orders.order_status', '!=', 'Deleted')
                    ->where('unleashed_id', '=', '2bd9c34b-ad75-4a17-a1f1-83ce0fcc33d4')
                    ->union($indirect_sales)
                    ->where('order_date', '<', $to_date);
        }

        return $query;
    }

    /**
     * Sales Orders Monthly for this Financial year (raw data)
     *
     * @return void
     */
    public function scopeLastFYRaw($query, $customer_code, $is_pharma=true)
    {
        $year_s = (date("n")<=6) ? date("Y")-2 : date("Y")-1;
        $year_e = (date("n")<=6) ? date("Y")-1 : date("Y");

        $from_date = $year_s .'-07-01';
        $to_date = $year_e .'-07-01';

        if ($is_pharma)
        {
            $query = $query->where('customer_code', $customer_code)
                    ->select(DB::raw('sub_total as total, order_number, order_status, order_date, unleashed_id, YEAR(order_date) as year_value, MONTH(order_date) as month_value'))
                    ->where('order_date', '>=', $from_date)
                    ->where('orders.order_status', '!=', 'Deleted')
                    ->where('unleashed_id', '=', '6c9a056b-3713-4cfc-bfdd-50be48e0c275')
                    ->where('order_date', '<', $to_date);
        }
        else
        {
            // Get indirect sales
            $indirect_sales = IndirectSales::whereNotNull('product_wholesaler.registration_number')
                                        ->join('product_wholesaler', function ($join) {
                                                $join->on('indirect_sales.product_code', '=', 'product_wholesaler.registration_number')
                                                      ->whereNull('product_wholesaler.deleted_at');
                                            })
                                        ->join('customer_wholesalers', function ($join) {
                                                $join->on('indirect_sales.customer_number', '=', 'customer_wholesalers.registration_number')
                                                      ->whereNull('customer_wholesalers.deleted_at');
                                            })
                                        ->join('products', 'products.guid', '=', 'product_wholesaler.product_guid')
                                        ->join('ucustomers', 'ucustomers.guid', '=', 'customer_wholesalers.customer_id')
                                        ->where('indirect_sales.invoice_date', '>=', $from_date)
                                        ->where('indirect_sales.invoice_date', '<', $to_date)
                                        ->where('ucustomers.customer_code', '=', $customer_code)
                                        ->where('indirect_sales.true_sales_price', '>', 0)
                                        ->groupBy('indirect_sales.invoice_date','indirect_sales.invoice_number')
                                        ->select(DB::raw("
                                                            SUM(indirect_sales.true_sales_price) AS total,
                                                            indirect_sales.invoice_number AS order_number,
                                                            'Indirect' AS order_status,
                                                            indirect_sales.invoice_date AS order_date,
                                                            '2bd9c34b-ad75-4a17-a1f1-83ce0fcc33d4' AS unleashed_id,
                                                            YEAR(indirect_sales.invoice_date) AS year_value,
                                                            MONTH(indirect_sales.invoice_date) AS month_value
                                                        ")
                                                );

            $query = $query->where('customer_code', $customer_code)
                    ->select(DB::raw('sub_total as total, order_number, order_status, order_date, unleashed_id, YEAR(order_date) as year_value, MONTH(order_date) as month_value'))
                    ->where('order_date', '>=', $from_date)
                    ->where('orders.order_status', '!=', 'Deleted')
                    ->where('unleashed_id', '=', '2bd9c34b-ad75-4a17-a1f1-83ce0fcc33d4')
                    ->union($indirect_sales)
                    ->where('order_date', '<', $to_date) ;
        }

        return $query;
    }

    /**
     * CFY Order Lines
     *
     * @return void
     */
    public function scopeCFYOrderLines($query, $customer_code, $is_pharma=true)
    {
        $year_s = (date("n")<=6) ? date("Y")-1 : date("Y");
        $year_e = (date("n")<=6) ? date("Y") : date("Y")+1;

        $from_date = $year_s .'-07-01';
        $to_date = $year_e .'-07-01';

        if ($is_pharma)
        {
            $query = OrderModel::where('customer_code', $customer_code)
                    ->leftJoin('order_lines', function($join)
                        {
                            $join->on('order_lines.order_number', '=', 'orders.order_number');
                            $join->on('order_lines.unleashed_id', '=', 'orders.unleashed_id');
                        })
                    ->select(DB::raw('
                                        MONTH(orders.order_date) AS month_value,
                                        YEAR(orders.order_date) AS year_value,
                                        orders.unleashed_id,
                                        orders.order_number,
                                        orders.order_status,
                                        order_lines.unit_price,
                                        order_lines.discounted_rate,
                                        order_lines.product_description,
                                        order_lines.product_code,
                                        order_lines.line_total,
                                        order_lines.line_tax,
                                        order_lines.avg_landed_price_on_sale,
                                        order_lines.quantity
                                    '))
                    ->where('orders.order_date', '>=', $from_date)
                    ->where('orders.order_status', '!=', 'Deleted')
                    ->where('orders.unleashed_id', '=', '6c9a056b-3713-4cfc-bfdd-50be48e0c275')
                    ->where('orders.order_date', '<', $to_date);
        }
        else
        {
            // Get indirect sales
            $indirect_sales = IndirectSales::whereNotNull('product_wholesaler.registration_number')
                                        ->join('product_wholesaler', function ($join) {
                                                $join->on('indirect_sales.product_code', '=', 'product_wholesaler.registration_number')
                                                      ->whereNull('product_wholesaler.deleted_at');
                                            })
                                        ->join('customer_wholesalers', function ($join) {
                                                $join->on('indirect_sales.customer_number', '=', 'customer_wholesalers.registration_number')
                                                      ->whereNull('customer_wholesalers.deleted_at');
                                            })
                                        ->join('products', 'products.guid', '=', 'product_wholesaler.product_guid')
                                        ->join('ucustomers', 'ucustomers.guid', '=', 'customer_wholesalers.customer_id')
                                        ->where('indirect_sales.invoice_date', '>=', $from_date)
                                        ->where('indirect_sales.invoice_date', '<', $to_date)
                                        ->where('indirect_sales.true_sales_price', '>', 0)
                                        ->where('ucustomers.customer_code', '=', $customer_code)
                                        ->select(DB::raw("
                                                            MONTH(indirect_sales.invoice_date) AS month_value,
                                                            YEAR(indirect_sales.invoice_date) AS year_value,
                                                            '2bd9c34b-ad75-4a17-a1f1-83ce0fcc33d4' AS unleashed_id,
                                                            indirect_sales.invoice_number AS order_number,
                                                            'Indirect' AS order_status,
                                                            indirect_sales.true_sales_price_per_unit AS unit_price,
                                                            0 AS discounted_rate,
                                                            products.product_description,
                                                            products.product_code,
                                                            indirect_sales.true_sales_price AS line_total,
                                                            0 AS line_tax,
                                                            (indirect_sales.medlab_total_cost/indirect_sales.units_sold) AS avg_landed_price_on_sale,
                                                            indirect_sales.units_sold AS quantity
                                                        "));

            $query = OrderModel::where('customer_code', $customer_code)
                    ->leftJoin('order_lines', function($join)
                        {
                            $join->on('order_lines.order_number', '=', 'orders.order_number');
                            $join->on('order_lines.unleashed_id', '=', 'orders.unleashed_id');
                        })
                    ->select(DB::raw('
                                        MONTH(orders.order_date) AS month_value,
                                        YEAR(orders.order_date) AS year_value,
                                        orders.unleashed_id,
                                        orders.order_number,
                                        orders.order_status,
                                        order_lines.unit_price,
                                        order_lines.discounted_rate,
                                        order_lines.product_description,
                                        order_lines.product_code,
                                        order_lines.line_total,
                                        order_lines.line_tax,
                                        order_lines.avg_landed_price_on_sale,
                                        order_lines.quantity
                                    '))
                    ->where('orders.order_date', '>=', $from_date)
                    ->where('orders.order_status', '!=', 'Deleted')
                    ->where('orders.unleashed_id', '=', '2bd9c34b-ad75-4a17-a1f1-83ce0fcc33d4')
                    ->union($indirect_sales)
                    ->where('orders.order_date', '<', $to_date);
        }

        return $query;
    }

    /**
     * LFY Order Lines
     *
     * @return void
     */
    public function scopeLFYOrderLines($query, $customer_code, $is_pharma=true)
    {
        $year_s = (date("n")<=6) ? date("Y")-2 : date("Y")-1;
        $year_e = (date("n")<=6) ? date("Y")-1 : date("Y");

        $from_date = $year_s .'-07-01';
        $to_date = $year_e .'-07-01';

        if ($is_pharma)
        {
            $query = $query->where('customer_code', $customer_code)
                    ->leftJoin('order_lines', function($join)
                        {
                            $join->on('order_lines.order_number', '=', 'orders.order_number');
                            $join->on('order_lines.unleashed_id', '=', 'orders.unleashed_id');
                        })
                    ->select(DB::raw('
                                        MONTH(orders.order_date) AS month_value,
                                        YEAR(orders.order_date) AS year_value,
                                        orders.unleashed_id,
                                        orders.order_number,
                                        orders.order_status,
                                        order_lines.unit_price,
                                        order_lines.discounted_rate,
                                        order_lines.product_description,
                                        order_lines.product_code,
                                        order_lines.line_total,
                                        order_lines.line_tax,
                                        order_lines.avg_landed_price_on_sale,
                                        order_lines.quantity
                                    '))
                    ->where('orders.order_date', '>=', $from_date)
                    ->where('orders.order_status', '!=', 'Deleted')
                    ->where('orders.unleashed_id', '=', '6c9a056b-3713-4cfc-bfdd-50be48e0c275')
                    ->where('orders.order_date', '<', $to_date);
        }
        else
        {
            // Get indirect sales
            $indirect_sales = IndirectSales::whereNotNull('product_wholesaler.registration_number')
                                        ->join('product_wholesaler', function ($join) {
                                                $join->on('indirect_sales.product_code', '=', 'product_wholesaler.registration_number')
                                                      ->whereNull('product_wholesaler.deleted_at');
                                            })
                                        ->join('customer_wholesalers', function ($join) {
                                                $join->on('indirect_sales.customer_number', '=', 'customer_wholesalers.registration_number')
                                                      ->whereNull('customer_wholesalers.deleted_at');
                                            })
                                        ->join('products', 'products.guid', '=', 'product_wholesaler.product_guid')
                                        ->join('ucustomers', 'ucustomers.guid', '=', 'customer_wholesalers.customer_id')
                                        ->where('indirect_sales.invoice_date', '>=', $from_date)
                                        ->where('indirect_sales.invoice_date', '<', $to_date)
                                        ->where('indirect_sales.true_sales_price', '>', 0)
                                        ->where('ucustomers.customer_code', '=', $customer_code)
                                        ->select(DB::raw("
                                                            MONTH(indirect_sales.invoice_date) AS month_value,
                                                            YEAR(indirect_sales.invoice_date) AS year_value,
                                                            '2bd9c34b-ad75-4a17-a1f1-83ce0fcc33d4' AS unleashed_id,
                                                            indirect_sales.invoice_number AS order_number,
                                                            'Indirect' AS order_status,
                                                            indirect_sales.true_sales_price_per_unit AS unit_price,
                                                            0 AS discounted_rate,
                                                            products.product_description,
                                                            products.product_code,
                                                            indirect_sales.true_sales_price AS line_total,
                                                            0 AS line_tax,
                                                            (indirect_sales.medlab_total_cost/indirect_sales.units_sold) AS avg_landed_price_on_sale,
                                                            indirect_sales.units_sold AS quantity
                                                        "));

            $query = $query->where('customer_code', $customer_code)
                    ->leftJoin('order_lines', function($join)
                        {
                            $join->on('order_lines.order_number', '=', 'orders.order_number');
                            $join->on('order_lines.unleashed_id', '=', 'orders.unleashed_id');
                        })
                    ->select(DB::raw('
                                        MONTH(orders.order_date) AS month_value,
                                        YEAR(orders.order_date) AS year_value,
                                        orders.unleashed_id,
                                        orders.order_number,
                                        orders.order_status,
                                        order_lines.unit_price,
                                        order_lines.discounted_rate,
                                        order_lines.product_description,
                                        order_lines.product_code,
                                        order_lines.line_total,
                                        order_lines.line_tax,
                                        order_lines.avg_landed_price_on_sale,
                                        order_lines.quantity
                                    '))
                    ->where('orders.order_date', '>=', $from_date)
                    ->where('orders.order_status', '!=', 'Deleted')
                    ->where('orders.unleashed_id', '=', '2bd9c34b-ad75-4a17-a1f1-83ce0fcc33d4')
                    ->union($indirect_sales)
                    ->where('orders.order_date', '<', $to_date);
        }

        return $query;
    }

    /**
     * CFY Order Lines
     *
     * @return void
     */
    public function scopeAjaxOrderLines($query, $customer_code, $month_year)
    {
        // July 2020
        $from_date = date("Y-m-d", strtotime("first day of " . $month_year));
        $to_date = date("Y-m-d", strtotime("last day of " . $month_year));

        // Get indirect sales
        $indirect_sales = IndirectSales::whereNotNull('product_wholesaler.registration_number')
                                    ->join('product_wholesaler', function ($join) {
                                            $join->on('indirect_sales.product_code', '=', 'product_wholesaler.registration_number')
                                                  ->whereNull('product_wholesaler.deleted_at');
                                        })
                                    ->join('customer_wholesalers', function ($join) {
                                            $join->on('indirect_sales.customer_number', '=', 'customer_wholesalers.registration_number')
                                                  ->whereNull('customer_wholesalers.deleted_at');
                                        })
                                    ->join('products', 'products.guid', '=', 'product_wholesaler.product_guid')
                                    ->join('ucustomers', 'ucustomers.guid', '=', 'customer_wholesalers.customer_id')
                                    ->where('indirect_sales.invoice_date', '>=', $from_date)
                                    ->where('indirect_sales.invoice_date', '<=', $to_date)
                                    ->where('indirect_sales.true_sales_price', '>', 0)
                                    ->where('ucustomers.customer_code', '=', $customer_code)
                                    ->select(DB::raw("
                                                        MONTH(indirect_sales.invoice_date) AS month_value,
                                                        YEAR(indirect_sales.invoice_date) AS year_value,
                                                        indirect_sales.invoice_date AS order_date,
                                                        '2bd9c34b-ad75-4a17-a1f1-83ce0fcc33d4' AS unleashed_id,
                                                        indirect_sales.invoice_number AS order_number,
                                                        'Indirect' AS order_status,
                                                        indirect_sales.true_sales_price_per_unit AS unit_price,
                                                        0 AS discounted_rate,
                                                        products.product_description,
                                                        products.product_code,
                                                        indirect_sales.true_sales_price AS line_total,
                                                        0 AS line_tax,
                                                        (indirect_sales.medlab_total_cost/indirect_sales.units_sold) AS avg_landed_price_on_sale,
                                                        indirect_sales.units_sold AS quantity
                                                    "));

        $result = OrderModel::where('customer_code', $customer_code)
                ->leftJoin('order_lines', function($join)
                    {
                        $join->on('order_lines.order_number', '=', 'orders.order_number');
                        $join->on('order_lines.unleashed_id', '=', 'orders.unleashed_id');
                    })
                ->select(DB::raw('
                                    MONTH(orders.order_date) AS month_value,
                                    YEAR(orders.order_date) AS year_value,
                                    orders.order_date,
                                    orders.unleashed_id,
                                    orders.order_number,
                                    orders.order_status,
                                    order_lines.unit_price,
                                    order_lines.discounted_rate,
                                    order_lines.product_description,
                                    order_lines.product_code,
                                    order_lines.line_total,
                                    order_lines.line_tax,
                                    order_lines.avg_landed_price_on_sale,
                                    order_lines.quantity
                                '))
                ->where('orders.order_date', '>=', $from_date)
                ->where('orders.order_date', '<=', $to_date)
                ->where('orders.order_status', '!=', 'Deleted')
                ->union($indirect_sales)
                ->get()
                ->toArray();

        // Prepare the data
        $output = array();
        foreach ($result as $item)
        {
            $so_orderno = $item["order_number"];

            $lines["{$so_orderno}"][] = $item;

            $output["{$so_orderno}"] = array(
                            "order_date"      => date("d/m/Y", strtotime($item["order_date"])),
                            "order_date_sort" => $item["order_date"],
                            "uid"             => $item["unleashed_id"],
                            "order_number"    => $item["order_number"],
                            "order_status"    => $item["order_status"],
                            "unleashed_id"    => ($item["unleashed_id"] == '6c9a056b-3713-4cfc-bfdd-50be48e0c275') ? 'Pharma' : 'VMS',
                            "total"           => 0,
                            "lines"           => $lines["{$so_orderno}"]
                        );

            foreach ($lines["{$so_orderno}"] as $row) {
                $output["{$so_orderno}"]["total"] += $row["line_total"];
            }
        }

        return $output;
    }
}
