<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class InteractionModel extends Model
{

    public $incrementing  = true;

    protected $guard_name = 'web';
    protected $table = "interactions";
    protected $primaryKey = "id";
    protected $casts = [
        'scheduled_datetime' => 'datetime'
    ];

    protected $guarded = [];
//    protected $fillable = [
//            'user_id',
//            'person_id',
//            'interaction_type',
//            'customer_id',
//            'order_person_id',
//            'notes',
//            'scheduled_datetime',
//            'duration',
//            'order_placed',
//            'send_email',
//            'email_other',
//            'file_uploaded',
//            'direction',
//            'purpose',
//            'status'
//    ];


    /**
     * An interaction belongs to a customer
     *
     * @return void
     */
    public function unleashedcustomer()
    {
        return $this->belongsTo(UnleashedCustomer::class, 'customer_id');
    }

    /**
     * An interaction is communication with a person
     *
     * @return void
     */
    public function person()
    {
        return $this->belongsTo(User::class, 'person_id');
    }

}
