<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Nova\Actions\Actionable;
use Laravel\Scout\Searchable;
use Spatie\Permission\Traits\HasRoles;

class PrimaryClassification extends Model
{
    use SoftDeletes;

    protected $guard_name = 'web';
    protected $table = 'primary_classifications';
    protected $primaryKey = 'id';


    /**
     * A parent classification can have many classifications
     *
     * @return void
     */
    public function classifications()
    {
        return $this->hasMany(SecondaryClassification::class, 'primary_classifications_id');
    }
    public function association()
    {
        return $this->belongsToMany(AssociationModel::class,'primary_classification_association','primary_classification_id','association_id');
    }
}
