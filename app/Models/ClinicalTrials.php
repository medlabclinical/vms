<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClinicalTrials extends Model
{
    protected $table = 'clinical_trials';
    public function member()
    {
        return $this->belongsToMany(Members::class,'clinical_trial_member','clinical_trial_id','member_id');
    }
    public function product()
    {
        return $this->belongsToMany(ProductModel::class,'product_multiple_relation','clinical_trial_id','product_guid');
    }
    public function partner()
    {
        return $this->belongsToMany(Partners::class,'product_multiple_relation','clinical_trial_id','partner_id');
    }
    public function brand(){
        return $this->belongsToMany(BrandModel::class,'product_multiple_relation','clinical_trial_id','brand_id');
    }
    public function product_family(){
        return $this->belongsToMany(ProductFamilyModel::class,'product_multiple_relation','clinical_trial_id','family_id');
    }
    public function article(){
        return $this->belongsToMany(Articles::class,'product_multiple_relation','clinical_trial_id','article_id');
    }
    public function product_ingredient(){
        return $this->belongsToMany(ProductIngredientModel::class,'product_multiple_relation','clinical_trial_id','ingredient_id');
    }
    public function publicationsandpresentation()
    {
        return $this->belongsToMany(Publications::class,'product_multiple_relation','clinical_trial_id','publication_id');
    }
    public function product_group(){
        return $this->belongsToMany(ProductGroupModel::class,'product_multiple_relation','clinical_trial_id','product_group_id');
    }
    public function clinical_trial_type(){
        return $this->belongsToMany(ClinicalTrialType::class,'product_multiple_relation','clinical_trial_id','clinical_trial_type_id');
    }
    public function image(){
        return $this->hasOne('App\Models\MediaLibrary','id','image');
    }
}
