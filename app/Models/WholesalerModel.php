<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class WholesalerModel extends Model
{

    protected $guard_name = 'web';
    protected $table      = 'wholesalers';
    protected $primaryKey = 'id';

    /**
     * Wholesalers has many customers
     *
     * @return void
     */
    public function customer_wholesaler_count()
    {
        return $this->belongsToMany(UnleashedCustomer::class, 'customer_wholesalers', 'wholesaler_id', 'customer_id');
    }

    /**
     * Wholesalers has many customers
     *
     * @return void
     */
    public function customer_wholesaler()
    {
        return $this->belongsToMany(UnleashedCustomer::class, 'customer_wholesalers', 'wholesaler_id', 'customer_id')->withPivot('registration_number');
    }

    /**
     * Wholesalers has many classifications
     *
     * @return void
     */
    public function wholesaler_classification_count()
    {
        return $this->belongsToMany(SecondaryClassification::class, 'wholesaler_classification', 'wholesaler_id', 'secondary_classification_id');
    }

    /**
     * Wholesalers has many classifications
     *
     * @return void
     */
    public function wholesaler_classification()
    {
        return $this->belongsToMany(SecondaryClassification::class, 'wholesaler_classification', 'wholesaler_id', 'secondary_classification_id')->withPivot('email_list');
    }

    /**
     * Wholesalers has many skus
     *
     * @return void
     */
    public function product_wholesaler()
    {
        return $this->belongsToMany(ProductModel::class, 'product_wholesaler', 'wholesaler_id', 'product_guid')->withPivot('registration_number');
    }


    /**
     * Return the list for wholesalers not associated in business (action item)
     *
     * @return relationship
     */
    public function scopeWholesalerList($query, $guid)
    {
        $keypair = null;

        $result = $query->whereNull("wholesalers.deleted_at")
                    ->leftJoin('customer_wholesalers', function ($join) use ($guid) {
                        $join->on('customer_wholesalers.wholesaler_id', '=', 'wholesalers.id');
                        $join->on('customer_wholesalers.customer_id', '=', DB::raw("'" .$guid . "'"));
                    })
                    ->select(DB::raw('wholesalers.id, wholesalers.name, customer_wholesalers.customer_id'))
                    ->get();

        foreach ($result as $item)
        {
            if (is_null($item->customer_id)) {
                $keypair["{$item->id}"] = $item->name;
            }
        }

        return $keypair;
    }
}
