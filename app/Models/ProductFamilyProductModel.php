<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductFamilyProductModel extends Model
{

    protected $guard_name = 'web';
    protected $primaryKey = 'id';
    protected $table      = 'product_family_products';
    protected $keyType    = 'integer';

    /**
     * [Description]
     *
     * @return void
     */
    public function product_family()
    {
    	return $this->belongsTo(ProductFamilyModel::class, 'product_family_id');
    }

    /**
     * [Description]
     *
     * @return void
     */
    public function product()
    {
    	return $this->belongsTo(ProductModel::class, 'product_guid', 'guid');
    }
}
