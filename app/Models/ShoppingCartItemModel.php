<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ShoppingCartItemModel extends Model
{

    public $incrementing  = true;

    protected $guard_name = 'web';
    protected $primaryKey = 'item_id';
    protected $table      = 'shopping_cart_items';
    protected $keyType    = 'integer';

    protected $fillable = [
        'cart_id',
        'line_type',
        'product_guid',
        'product_code',
        'quantity',
        'unit_price',
        'discount_rate',
        'line_total',
        'comments',
        'tax_rate',
        'line_tax',
        'batch_number',
    ];

    /**
     * Need to find the product > product family > brand_id
     */
    public function scopeCartProducts($query, $cart_id)
    {
        $vitamins_warehouse = env("VITAMINS_WAREHOUSE", "Alexandria");
        $pharma_warehouse = env("PHARMA_WAREHOUSE", "ANSPEC");

        $vitamin = WarehouseModel::where('warehouse_name', $vitamins_warehouse)->first();
        $vitamin_code = $vitamin->warehouse_code;

        $pharma = WarehouseModel::where('warehouse_name', $pharma_warehouse)->first();
        $pharma_code = $pharma->warehouse_code;

        $query->join('product_family_products', 'product_family_products.product_guid', '=', 'shopping_cart_items.product_guid')
            ->join('product_family', 'product_family.id', '=', 'product_family_products.product_family_id')
            ->leftjoin('batch_numbers as vbn', function ($join) use ($vitamin_code) {
                $join->on('vbn.batch_number', '=', 'shopping_cart_items.batch_number')
                    ->where('vbn.warehouse_code', '=', $vitamin_code);
            })
            ->leftjoin('batch_numbers as pbn', function ($join) use ($pharma_code) {
                $join->on('pbn.batch_number', '=', 'shopping_cart_items.batch_number')
                    ->where('pbn.warehouse_code', '=', $pharma_code);
            })
            ->where('shopping_cart_items.cart_id', '=', $cart_id)
            ->select(DB::raw('shopping_cart_items.*, product_family.brand_id, product_family.ship_refrigerated, vbn.batch_guid as v_batchguid, pbn.batch_guid as p_batchguid'));

        return $query;
    }

    public function shoppingcart()
    {
        return $this->belongsTo(ShoppingCartModel::class, 'cart_id');
    }

    public function skus()
    {
        return $this->belongsTo(ProductModel::class, 'product_guid', 'guid')->where('product_group', 'Finished Goods');
    }
}
