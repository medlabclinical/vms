<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApplicationAddress extends Model
{
    protected $table = 'application_address';
    protected $guarded = [];
}
