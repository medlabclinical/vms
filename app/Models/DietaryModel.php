<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DietaryModel extends Model
{

    protected $table = 'dietary';
    public function image(){
        return $this->hasOne('App\Models\MediaLibrary','id','image');
    }
    public function preview(){
        return $this->hasOne('App\Models\MediaLibrary','id','preview_image');
    }
}
