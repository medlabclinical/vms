<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StockOnHandWarehouse extends Model
{

	public $incrementing  = true;

    protected $guard_name = 'web';
    protected $table      = 'stock_on_hand_warehouse';

    public function product_guid()
    {
    	return $this->belongsTo(ProductModel::class, 'guid');
    }
}
