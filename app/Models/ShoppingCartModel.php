<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ShoppingCartModel extends Model
{

    public $incrementing  = true;

    protected $guard_name = 'web';
    protected $primaryKey = 'cart_id';
    protected $table      = 'shopping_carts';
    protected $keyType    = 'integer';
    protected $guarded = [];

    /**
     * Update the unleashed information back into the system
     * Sun Aug 16 13:07:41 2020
     */
    public function scopeUpdateUnleashedInformation($builder, $post_cartid, $post_orderno, $post_guid)
    {
        $updateCart = $this->where('cart_id', $post_cartid)->update(['unleashed_orderno' => $post_orderno, 'unleashed_guid' => $post_guid]);
        if (!$updateCart) {
            Log::warning('Vitamin Cart ' . $cart_id . ' was not updated with ' . $post_orderno);
        }
    }

    /**
     * Update the pharma unleashed information back into the system
     * Sun Aug 16 13:07:41 2020
     */
    public function scopeUpdateUnleashedInformationPharma($builder, $post_cartid, $post_orderno, $post_guid)
    {
        $updateCart = $this->where('cart_id', $post_cartid)->update(['unleashed_drug_orderno' => $post_orderno, 'unleashed_drug_guid' => $post_guid]);
        if (!$updateCart) {
            Log::warning('Pharma Cart ' . $cart_id . ' was not updated with ' . $post_orderno);
        }
    }

    /**
     * New shopping cart payment reference for securepay needs customer code and not guid
     */
    public function scopeGetSecurepayReferenceByCartId($query, $cartid)
    {
        $customer = $query->where('cart_id', $cartid)
            ->join('ucustomers', 'ucustomers.guid', '=', 'customer_id')
            ->select(DB::raw("ucustomers.customer_code,ucustomers.trading_name"))
            ->first();
        $trading_name = preg_replace('/--+/','-',preg_replace('/[^A-Za-z0-9\-]/', '-', $customer->trading_name));
        $cart_items = ShoppingCartItemModel::CartProducts($cartid)->get();

        // Peruse the items to find the num of brands
        $brands = array();
        foreach ($cart_items as $item) {
            $brands[$item->brand_id] = $item->brand_id;
        }

        // default is vitamins only
        $securepay_reference = "V-" . $customer->customer_code . "-" . $cartid . "-" . $trading_name;

        // if both then its VP
        if (count($brands)==2) {
            $securepay_reference = "VP-" . $customer->customer_code . "-" . $cartid . "-" . $trading_name;
        } else {
            if (count($brands) == 1 && isset($brands[1])) {
                $securepay_reference = "P-" . $customer->customer_code . "-" . $cartid . "-" . $trading_name;
            }
        }

        return $securepay_reference;
    }

    /**
     * A cart has a user
     *
     * @return void
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * A cart belongs to a customer
     *
     * @return void
     */
    public function unleashedcustomer()
    {
        return $this->belongsTo(UnleashedCustomer::class, 'customer_id');
    }

    /**
     * An interaction is communication with a person
     *
     * @return void
     */
    public function person()
    {
        return $this->belongsTo(PersonModel::class, 'person_id');
    }

    /**
     * A cart is communication with a person
     *
     * @return void
     */
    public function addressbook()
    {
        return $this->belongsTo(Addressbook::class, 'id');
    }

    /**
     * A cart is communication with a person
     *
     * @return void
     */
    public function customerperson()
    {
        return $this->belongsToMany(PersonModel::class, 'customer_persons', 'customer_id', 'person_id');
    }

}
