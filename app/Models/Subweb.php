<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subweb extends Model
{
    protected $table = 'subweb';
    public function image(){
        return $this->hasOne('App\Models\MediaLibrary','id','image');
    }
    public function brand(){
        return $this->belongsToMany(BrandModel::class,'subweb_brand','subweb_id','brand_id');
    }
}
