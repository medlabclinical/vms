<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class BrandModel extends Model
{

	public $incrementing  = true;

    protected $guard_name = 'web';
    protected $primaryKey = 'id';
    protected $table      = 'brand';
    protected $keyType    = 'integer';

    /**
     * A product family can only belong to a brand
     *
     * @return void
     */
    public function product_family()
    {
    	return $this->hasMany(ProductFamilyModel::class, 'brand_id');
    }
}
