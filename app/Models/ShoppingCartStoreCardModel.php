<?php

namespace App\Models;

use App\Securepay\Payment;
use Illuminate\Database\Eloquent\Model;

class ShoppingCartStoreCardModel extends Model
{

	public $incrementing  = true;

    protected $guard_name = 'web';
    protected $primaryKey = 'id';
    protected $table      = 'shopping_cart_storecard';
    protected $keyType    = 'integer';

    public static function boot()
    {
        parent::boot();
        static::deleted(function ($shoppingCartStoreCard) {
            Payment::deleteStoredCard($shoppingCartStoreCard->id);
        });
    }

    public function scopeCartGetStoredCards($query, $guid)
    {
    	$query = $query->whereNull('shopping_cart_storecard.deleted_at')
    					->join('ucustomers', 'ucustomers.customer_code', '=', 'shopping_cart_storecard.customer_code')
    					->where('ucustomers.guid', '=', $guid)
                        ->whereNotNull('shopping_cart_storecard.securepay_reference')
    					->select('shopping_cart_storecard.id','shopping_cart_storecard.card_number','shopping_cart_storecard.card_expiry','shopping_cart_storecard.card_name')
    					->get()
    					->toArray();
    	return $query;
    }

    /**
     * Business view get card list active only
     *
     * @return void
     */
    public function scopeCardList($query, $guid)
    {
        $query = $query->whereNull('shopping_cart_storecard.deleted_at')
                        ->join('ucustomers', 'ucustomers.customer_code', '=', 'shopping_cart_storecard.customer_code')
                        ->where('ucustomers.guid', '=', $guid)
                        ->whereNotNull('shopping_cart_storecard.securepay_reference')
                        ->select('shopping_cart_storecard.id','shopping_cart_storecard.card_number','shopping_cart_storecard.card_expiry','shopping_cart_storecard.card_name');
    }

    /**
     * Payment search by customer code
     *
     * @return void
     */
    public function scopeCardListByCustomerCode($query, $customer_code)
    {
        $query = $query->whereNull('shopping_cart_storecard.deleted_at')
                        ->where('shopping_cart_storecard.customer_code', '=', $customer_code)
                        ->whereNotNull('shopping_cart_storecard.securepay_reference')
                        ->select('shopping_cart_storecard.id','shopping_cart_storecard.card_number','shopping_cart_storecard.card_expiry','shopping_cart_storecard.card_name');
    }

    public function unleashed_customers()
    {
        return $this->belongsTo(UnleashedCustomer::class, 'customer_code', 'customer_code');
    }
}
