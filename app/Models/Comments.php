<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{
    protected $table = 'comments';
    protected $guarded = [];
    public function parent(){
        return $this->belongsToOne(static::class,'parent_id');
    }
    public function children(){
        return $this->hasMany(static::class,'parent_id')->orderBy('created_at','desc');
    }
}
