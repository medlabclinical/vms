<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PdfTypes extends Model
{

    protected $guard_name = 'web';
    protected $table      = 'pdf_type';
    protected $casts      = ['date' => 'date'];
    public function hasPDF(){
        return $this->hasMany(ProductPdfModel::class,'pdf_type_id', 'id');
    }
    public function brand(){
        return $this->belongsToMany(BrandModel::class,'pdf_type_brand','pdf_type_id','brand_id');
    }
    public function image(){
        return $this->hasOne('App\Models\MediaLibrary','id','rep_image');
    }
}
