<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UnleashedCustomer extends Model
{
    use SoftDeletes;

    public $incrementing  = false;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    // protected $guarded = ['price'];
    protected $fillable = [
        'customer_code','customer_name','phone_number','fax_number','email','obsolete','guid','unleashed_id','status','register_status','pre_status','channel_id','payment_term',
        'business_type','referral_code','obsolete','acn','abn_number','gstvat_number','currency_code','taxable','tax_rate','tax_code','discount_rate','stop_credit','need_credit',
        'is_business_owner','is_declared_bankrupt','is_refused_credit','list_in_web'
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table      = 'ucustomers';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'guid';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType    = 'string';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts      = [
        'unleashed_created_on'  => 'datetime',
        'unleashed_modified_on' => 'datetime'
    ];

    // protected $guard_name = 'web';

    /**
     * Get the index name for the model.
     *
     * @return string
     */
    public function searchableAs()
    {
        return 'ucustomers';
    }

    /**
     * Customer belongs to a Banner
     *
     * @return void
     */
    public function address()
    {
        return $this->hasMany(Addressbook::class, 'customer_code');
    }


    /**
     * Customer belongs to a Banner
     *
     * @return void
     */
    public function primary_channel()
    {
        return $this->belongsTo(PrimaryChannel::class, 'channel_id');
    }
    public function secondary_channel()
    {
        return $this->belongsTo(SecondaryChannel::class, 'secondary_channel_id');
    }
    /**
     * Return the the subscribed business
     */
    public function scopeGetSubscribeBusinessByCode($query, $customer_code)
    {
        return $query->where('ucustomers.customer_code', $customer_code)
            ->leftJoin('primary_classifications', 'primary_classifications.id', '=', 'ucustomers.classification_id')
            ->leftJoin('secondary_classifications', 'secondary_classifications.id', '=', 'ucustomers.secondary_classification_id')
            ->leftJoin('channel_parents', 'channel_parents.id', '=', 'ucustomers.channel_id')
            ->leftJoin('channels', 'channels.id', '=', 'ucustomers.secondary_channel_id')

            ->orderBy('ucustomers.customer_code', 'ASC')
            ->select(
                'ucustomers.customer_code',
                'ucustomers.customer_name',
                'primary_classifications.name AS primary_classifications_name',
                'secondary_classifications.name AS secondary_classifications_name',
                'channel_parents.name AS channel_parents_name',
                'channels.name AS channels_name',
                'ucustomers.guid',
                'ucustomers.tier',
                'ucustomers.drug_tier',
                'ucustomers.date_last_purchase',
                'ucustomers.drug_date_last_purchase'
            );
    }

    /**
     * Return the the subscribed business
     */
    public function scopeGetSubscribeBusiness($query, $guid)
    {
        return $query->where('ucustomers.guid', $guid)
            ->leftJoin('primary_classifications', 'primary_classifications.id', '=', 'ucustomers.classification_id')
            ->leftJoin('secondary_classifications', 'secondary_classifications.id', '=', 'ucustomers.secondary_classification_id')
            ->leftJoin('channel_parents', 'channel_parents.id', '=', 'ucustomers.channel_id')
            ->leftJoin('channels', 'channels.id', '=', 'ucustomers.secondary_channel_id')

            ->orderBy('ucustomers.customer_code', 'ASC')
            ->select(
                'ucustomers.customer_code',
                'ucustomers.customer_name',
                'primary_classifications.name AS primary_classifications_name',
                'secondary_classifications.name AS secondary_classifications_name',
                'channel_parents.name AS channel_parents_name',
                'channels.name AS channels_name',
                'ucustomers.guid',
                'ucustomers.tier',
                'ucustomers.drug_tier',
                'ucustomers.date_last_purchase',
                'ucustomers.drug_date_last_purchase'
            );
    }

    /**
     * Return the oldest business linked to a person
     */
    public function scopeGetFirstBusiness($query, $person_id)
    {
        return $query->join('customer_persons', function($join) use ($person_id) {
            $join->on('customer_persons.person_id', '=', DB::raw("'" . $person_id . "'"));
            $join->on('customer_persons.customer_id', '=', 'ucustomers.guid');
        })
            ->leftJoin('primary_classifications', 'primary_classifications.id', '=', 'ucustomers.classification_id')
            ->leftJoin('secondary_classifications', 'secondary_classifications.id', '=', 'ucustomers.secondary_classification_id')
            ->leftJoin('channel_parents', 'channel_parents.id', '=', 'ucustomers.channel_id')
            ->leftJoin('channels', 'channels.id', '=', 'ucustomers.secondary_channel_id')
            ->whereNull('customer_persons.deleted_at')
            ->orderBy('ucustomers.customer_code', 'ASC')
            ->select(
                'ucustomers.customer_code',
                'ucustomers.customer_name',
                'primary_classifications.name AS primary_classifications_name',
                'secondary_classifications.name AS secondary_classifications_name',
                'channel_parents.name AS channel_parents_name',
                'channels.name AS channels_name',
                'ucustomers.guid',
                'ucustomers.tier',
                'ucustomers.drug_tier',
                'ucustomers.date_last_purchase',
                'ucustomers.drug_date_last_purchase'
            );
    }

}
