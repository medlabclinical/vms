<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class Addressbook extends Model
{
	use SoftDeletes;

	public $incrementing  = true;

    protected $guard_name = 'web';
    protected $primaryKey = 'id';
    protected $table      = 'ucustomers_addressbook';
    protected $keyType    = 'integer';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['addresssearch', 'country', 'customer_code', 'address_type', 'address_line_1', 'address_line_2', 'suburb', 'postcode', 'state', 'longitude', 'latitude'];

    /**
     * Set the country as autralia
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        static::creating(function ($attr) {

            if (isset($attr->addresssearch)) {
                $addressJson          = json_decode($attr->addresssearch);
                $attr->address_line_1 = $addressJson->address_line_1;
                $attr->address_line_2 = (isset($addressJson->address_line_2)) ? $addressJson->address_line_2 : '';
                $attr->suburb         = $addressJson->suburb;
                $attr->postcode       = $addressJson->postcode;
                $attr->state          = $addressJson->state;
                $attr->country        = $addressJson->country;
                $attr->longitude      = $addressJson->longitude;
                $attr->latitude       = $addressJson->latitude;
            }

        });

        static::updating(function ($attr) {

            $addressJson          = json_decode($attr->addresssearch);
            $attr->address_line_1 = $addressJson->address_line_1;
            $attr->address_line_2 = (isset($addressJson->address_line_2)) ? $addressJson->address_line_2 : '';
            $attr->suburb         = $addressJson->suburb;
            $attr->postcode       = $addressJson->postcode;
            $attr->state          = $addressJson->state;
            $attr->country        = $addressJson->country;
            $attr->longitude      = $addressJson->longitude;
            $attr->latitude       = $addressJson->latitude;

        });
    }

    /**
     * A banner can have many customers
     *
     * @return void
     */
    public function unleashedcustomer()
    {
        return $this->belongsTo(UnleashedCustomer::class, 'customer_code');
    }

    /**
     * Latest sales order date
     *
     * @return void
     */
    public function scopeAddressList($query, $guid)
    {
        $query = $query->where('customer_code', $guid)
                    ->where("deleted_at", null);

        return $query;
    }
}
