<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class CustomerBrandModel extends Model
{
	use SoftDeletes;

	public $incrementing  = true;

    protected $guard_name = 'web';
    protected $primaryKey = 'id';
    protected $table      = 'customer_brands';
    protected $keyType    = 'integer';
    protected $fillable = ['customer_id','brand_id'];

    /**
     * The "booted" method of the model.
     * This event triggers after a model update
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();
    }

    /**
     * A relationship belongs to a customer
     *
     * @return void
     */
    public function unleashedcustomer()
    {
        return $this->belongsTo(UnleashedCustomer::class, 'customer_id');
    }

    /**
     * A relationship belongs to a wholersaler
     *
     * @return void
     */
    public function brand()
    {
        return $this->belongsTo(BrandModel::class, 'brand_id');
    }

    public function scopeBrandList($query, $guid)
    {
        return $query->where('customer_id', $guid)
            ->join('brand', 'brand.id', '=', 'brand_id')
            ->select(DB::raw('brand.name'));
    }
}
