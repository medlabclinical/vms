<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ProductFamilyModel extends Model
{

    protected $guard_name = 'web';
    protected $primaryKey = 'id';
    protected $table      = 'product_family';
    protected $keyType    = 'integer';

    /**
     * [Description]
     *
     * @return void
     */
    public function product_family_product_count()
    {
        return $this->hasMany(ProductFamilyProductModel::class, 'product_family_id');
    }

    /**
     * [Description]
     *
     * @return void
     */
    public function product()
    {
        // return $this->hasMany(ProductModel::class, 'product_family_id');
        return $this->belongsToMany(ProductModel::class, 'product_family_products', 'product_family_id', 'product_guid');

    }

    /**
     * [Description]
     *
     * @return void
     */
    public function product_group()
    {
        return $this->belongsToMany(ProductGroupModel::class, 'product_family_groups', 'product_family_id', 'product_group_id');
    }

    /**
     * [Description]
     *
     * @return void
     */
    public function product_ingredient()
    {
        return $this->belongsToMany(ProductIngredientModel::class, 'product_family_ingredients', 'product_family_id', 'product_ingredient_id')->withPivot('quantity','equiv_to','is_excipient');
    }

    /**
     * [Description]
     *
     * @return void
     */
    public function product_family_ingredient()
    {
        return $this->hasMany(ProductFamilyIngredientModel::class, 'product_family_id');
    }

    /**
     * [Description]
     *
     * @return void
     */
    public function product_pdf()
    {
        return $this->belongsToMany(ProductPdfModel::class, 'product_family_pdfs', 'product_family_id', 'product_pdfs_id');
    }

    /**
     * [Description]
     *
     * @return void
     */
    public function brand()
    {
        return $this->belongsTo(BrandModel::class);
    }
    public function publication(){
        return $this->belongsToMany(Publications::class,'product_multiple_relation','family_id','publication_id');
    }
    public function clinical_trial(){
        return $this->belongsToMany(ClinicalTrials::class,'product_multiple_relation','family_id','clinical_trial_id');
    }
    public function article(){
        return $this->belongsToMany(Articles::class,'product_multiple_relation','family_id','article_id');
    }
}
