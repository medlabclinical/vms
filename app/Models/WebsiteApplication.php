<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class WebsiteApplication extends Model
{
    protected $table = 'website_applications';
    protected $guarded = [];

    /**
     * Upload the image
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        static::creating(function ($website_application) {

            if ($website_application->qualification && $website_application->type != 'Account Upgrade From Lead Application')
            {
                $extension = $website_application->qualification->getClientOriginalExtension();
                $practitionername = preg_replace("/[^a-zA-Z]+/", "", trim($website_application->first_name . "_" . $website_application->last_name));
                $s3_filename = strtolower($practitionername) . "-" . time() . '.' . $extension;
                $image = $website_application->qualification;

                $s3_filepath = 'registration/' . $s3_filename;
                $s3 = Storage::disk('s3private');
                $s3->put($s3_filepath, file_get_contents($image), 'private');

                $website_application->qualification = $s3_filepath;
            }
        });
    }

    public function business()
    {
        return $this->belongsTo(UnleashedCustomer::class, 'customer_code');
    }
//    public function person()
//    {
//        return $this->belongsTo(User::class, 'person_id');
//    }
    public function association()
    {
        return $this->belongsTo(AssociationModel::class, 'association_name');
    }

    public function primary_channel()
    {
        return $this->belongsTo(PrimaryChannel::class, 'primary_channel_id');
    }

    public function secondary_channel()
    {
        return $this->belongsTo(SecondaryChannel::class, 'second_channel_id');
    }

}
