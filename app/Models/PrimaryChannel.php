<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PrimaryChannel extends Model
{
    use SoftDeletes;

    protected $guard_name = 'web';
    protected $table = 'channel_parents';
    protected $primaryKey = 'id';


    /**
     * A parent channel can have many sub channels
     *
     * @return void
     */
    public function channels()
    {
        return $this->hasMany(SecondaryChannel::class, 'parent_id');
    }
}
