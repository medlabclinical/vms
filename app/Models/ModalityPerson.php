<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModalityPerson extends Model
{

    protected $guard_name = 'web';
    protected $table      = 'modality_persons';
    protected $primaryKey = 'id';

    protected $fillable = ['modality_id', 'person_id'];

    /**
     * Set overriding
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();
    }

    /**
     * Relation has a person associated
     *
     * @return void
     */
    public function person()
    {
        return $this->belongsTo(PersonModel::class, 'person_id');
    }

    /**
     * Relationship belongs to a modality
     *
     * @return void
     */
    public function modality()
    {
        return $this->belongsTo(Modality::class, 'modality_id');
    }
}
