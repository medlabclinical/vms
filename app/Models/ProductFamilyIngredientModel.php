<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ProductFamilyIngredientModel extends Model
{

    protected $guard_name = 'web';
    protected $primaryKey = 'id';
    protected $table      = 'product_family_ingredients';
    protected $keyType    = 'integer';


    /**
     * [Description]
     *
     * @return void
     */
    public function product_family()
    {
    	return $this->belongsTo(ProductFamilyModel::class, 'product_family_id');
    }

    /**
     * [Description]
     *
     * @return void
     */
    public function product_ingredient()
    {
    	return $this->belongsTo(ProductIngredientModel::class, 'product_ingredient_id');
    }
}
