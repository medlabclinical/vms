<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Nova\Actions\Actionable;
use Spatie\Permission\Traits\HasRoles;

class SecondaryClassification extends Model
{
    use SoftDeletes;

    protected $guard_name = 'web';
    protected $table = 'secondary_classifications';
    protected $primaryKey = 'id';


    /**
     * A classification can be associated to many customers
     *
     * @return void
     */
    public function unleashedcustomer()
    {
        return $this->hasMany(UnleashedCustomer::class, 'classification_id');
    }

    /**
     * Classification belongs to a Category
     *
     * @return void
     */
    public function primary_classification()
    {
        return $this->belongsTo(PrimaryClassification::class, 'primary_classifications_id');
    }
}
