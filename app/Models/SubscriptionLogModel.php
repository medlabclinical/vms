<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class SubscriptionLogModel extends Model
{
	use SoftDeletes;

	public $incrementing  = true;

    protected $guard_name = 'web';
    protected $primaryKey = 'id';
    protected $table      = 'subscription_log';
    protected $keyType    = 'integer';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'direction',
        'email',
        'log',
    ];

}
