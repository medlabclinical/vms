<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ProductLabelModel extends Model
{

    protected $guard_name = 'web';
    protected $primaryKey = 'id';
    protected $table      = 'product_label';
    protected $keyType    = 'integer';

    /**
     * A product label belongs to a specific product
     *
     * @return void
     */
    public function product()
    {
    	return $this->belongsTo(ProductModel::class, 'product_guid');
    }
}
