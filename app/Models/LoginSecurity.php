<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LoginSecurity extends Model
{
    protected $fillable = [
        'user_id'
    ];
    protected $table = 'login_securities';
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
