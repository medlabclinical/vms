<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;

class DefaultUser extends Authenticatable
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'users';
    protected $fillable = [
        'name', 'email', 'password', 'first_name', 'last_name'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Set the user_id of the creator as the person logged in
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        static::creating(function ($user) {
            $user->name = $user->first_name . ' ' . $user->last_name;
        });

        static::updating(function ($user) {
            $user->name = $user->first_name . ' ' . $user->last_name;
        });
    }

    /**
     * isSuperAdmin
     *
     * @return void
     */
    public function isSuperAdmin()
    {
        return $this->hasRole('Root'); // ?? something like this! should return true or false
    }

    /**
     * unleashed
     *
     * @return void
     */
    public function brand()
    {
        return $this->belongsTo(BrandModel::class, 'brand_id');
    }

    /**
     * Get the list of BDM users to associate the banners too
     *
     * @return void
     */
    public function scopeBdmUsers()
    {
        $keypair = null;
        $query = $this->where("email", "<>", '')
                    ->join('model_has_roles', 'model_has_roles.model_id', '=', 'users.id')
                    ->join('roles', 'roles.id', '=', 'model_has_roles.role_id')
                    ->where('roles.id', 8)
                    ->select(DB::raw("CONCAT(first_name ,' ' ,last_name) as name, email, users.id"))
                    ->get();
        foreach ($query as $item) {
            $keypair["{$item->id}"] = $item->name;
        }
        return $keypair;
    }

    /**
     * Get the email list
     *
     * @return void
     */
    public function scopeActiveUsers()
    {
        $keypair = null;
        $query = $this->where("email", "<>", '')->select(DB::raw("CONCAT(first_name ,' ' ,last_name) as name, email"))->get();
        foreach ($query as $item) {
            $keypair["{$item->email}"] = $item->name;
        }
        return $keypair;
    }

    /**
     * Override reps field
     *
     * @return void
     */
    public function scopeRepUsers($query)
    {
        $keypair[] = array("id" => 0, "name" => "Please select a rep");
        $result = $query->orderBy('first_name', 'ASC')->get();
        foreach ($result as $item) {
            $keypair[] = array("id" => $item->id, "name" => $item->first_name . " " . $item->last_name);
        }
        return $keypair;
    }

    /**
     * Cart get staff list and email for shopping cart
     *
     * @return void
     */
    public function scopeCartActiveUsers($query, $keyword)
    {
        $result = array();
        $query = $this->where("email", "<>", '')
                    ->where("email", "LIKE", '%' . $keyword . '%')
                    ->where(function($extendquery) use ($keyword)
                        {
                            $extendquery->where("email", "LIKE", '%' . $keyword . '%')
                                    ->orWhere("first_name", "LIKE", '%' . $keyword . '%')
                                    ->orWhere("last_name", "LIKE", '%' . $keyword . '%');
                        })
                    ->select(DB::raw("CONCAT(first_name ,' ' ,last_name) as name, email"))
                    ->get();

        if (count($query) > 0) {
            foreach ($query as $item) {
                $result["staff"][] = $item;
            }
        }

        return $result;
    }
}
