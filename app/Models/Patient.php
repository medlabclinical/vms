<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Patient extends Authenticatable
{
    use Notifiable;

    protected $guard = 'patient';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'patient';
    protected $fillable = [
        'email', 'password', 'first_name', 'last_name', 'status','guid','phone','mobile','salutation','pre_status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function role()
    {
        return $this->hasOne('App\\Models\\Role', 'id', 'role_id');
    }

    public function routers()
    {
        return $this->hasMany('App\\Models\\RoleHasPermissions', 'role_id', 'role_id');
    }
    public function practitioner(){
        return $this->belongsTo('App\\Models\\UnleashedCustomer','guid');
    }
    public function patient_address()
    {
        return $this->hasMany(PatientAddress::class, 'patient_id');
    }

}
