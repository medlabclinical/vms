<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductMultipleRelation extends Model
{
    protected $table = 'product_multiple_relation';
}
