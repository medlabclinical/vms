<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ProductModel extends Model
{


    protected $guard_name = 'web';
    protected $primaryKey = 'guid';
    protected $table      = 'products';
    protected $keyType    = 'string';

    /**
     * Calculates the number of stock from stockonhand
     *
     * @return void
     */
    public function calculate_allocated_stock()
    {
        return StockOnHandWarehouse::where('product_guid', '=', $this->guid)
                    ->where('warehouse_name', '=', 'Alexandria')
                    ->groupBy('warehouse_name')
                    ->sum('available_quantity');
    }

    /**
     * A product can have multiple types of packaging
     *
     * @return void
     */
    public function product_packing_types()
    {
        return $this->belongsToMany(ProductPackingTypeModel::class, 'product_packaging','product_guid', 'product_bottle_type_id');
    }

    /**
     * A product can be distributed by many wholesalers
     *
     * @return void
     */
    public function wholesaler()
    {
        return $this->belongsToMany(WholesalerModel::class, 'product_wholesaler', 'product_guid', 'wholesaler_id')->withPivot(['registration_number']);
    }

    /**
     * A single product can have multiple individual labels that are unique to that product
     *
     * @return void
     */
    public function product_label()
    {
        return $this->hasMany(ProductLabelModel::class, 'product_guid');
    }

    /**
     * A single product can have 1 brand
     *
     * @return void
     */
    public function brand()
    {
        return $this->belongsTo(BrandModel::class);
    }

    /**
     * A product belongs to a product family
     *
     * @return void
     */
    public function product_family()
    {
        return $this->belongsToMany(ProductFamilyModel::class, 'product_family_products', 'product_guid', 'product_family_id');
        // return $this->belongsTo(ProductFamilyModel::class);
    }

    /**
     * Calculates the number of shippers per pallet of goods for display
     *
     * @return void
     */
    public function calculate_shippers_per_pallet()
    {
        return $this->wh_no_shippers_per_layer * $this->wh_no_layers_per_pallet;
    }

    /**
     * Calculates the number of products per pallet for display
     *
     * @return void
     */
    public function calculate_products_per_pallet()
    {
        return $this->wh_per_shipper * $this->wh_no_shippers_per_layer * $this->wh_no_layers_per_pallet;
    }

    /**
     * Calculates the height of the pallet based on the height of the
     * the products for display
     *
     * @return void
     */
    public function calculate_pallet_height()
    {
        return $this->wh_shipper_height_mm * $this->wh_no_layers_per_pallet;
    }

    public function scopeProductImage($query, $guid) {

        return $query->where('guid', $guid)->first();
    }


    public function scopeCartGetProduct($query, $product_id)
    {
        // Searching for the Products
        $data = $query->where('products.product_code', '=', $product_id)
                      ->leftJoin('product_family_products', 'product_family_products.product_guid', '=', 'products.guid')
                      ->leftJoin('product_family', 'product_family.id', '=', 'product_family_products.product_family_id')
                      ->leftJoin('brand', 'brand.id', '=', 'products.brand_id')
                      ->select(DB::raw("products.product_description,
                                       products.product_code,
                                       products.product_group,
                                       brand.name AS product_brand,
                                       products.sell_price_1,
                                       products.tax_rate,
                                       products.img_front_view_small,
                                       products.guid,
                                       product_family.ship_refrigerated,
                                       product_family.store_refrigerated,
                                       products.sell_price_10,
                                       products.sellable_in_unleashed,
                                       products.obsolete,
                                       products.show_on_website
                                    "))
                      ->get()
                      ->toArray();

        if (count($data)==0) {
            return false;
        }

        $item = current($data);

        $batches_list = array();
        $this_list = ProductBatchNumbersModel::GetBatchListByProduct($item["product_code"]);
        foreach ($this_list as $i) {

            $i->expiry_date_display = date("jS M Y", strtotime($i->expiry_date));
            $batches_list[] = $i;
        }

        return array(
            "product_description"  => $item["product_description"],
            "product_group"        => $item["product_group"],
            "product_brand"        => $item["product_brand"],
            "product_code"         => $item["product_code"],
            "product_cost"         => round($item["sell_price_1"], 2),
            "product_rrp"          => round($item["sell_price_10"], 2),
            "product_tax_rate"     => $item["tax_rate"],
            "product_guid"         => $item["guid"],
            "product_ship_cold"    => $item["ship_refrigerated"],
            "product_store_cold"   => $item["store_refrigerated"],
            "img_front_view_small" => $item["img_front_view_small"],
            "cart_quantity"        => 0,
            "product_batchnumber"  => (array_key_exists(0, $batches_list)) ? $batches_list[0]->batch_number : '',
            "product_expiry"       => (array_key_exists(0, $batches_list)) ? $batches_list[0]->expiry_date_display : '',
            "sellable_in_unleashed"=> $item["sellable_in_unleashed"],
            "obsolete"             => $item["obsolete"],
            "show_on_website"      => $item["show_on_website"],
        );
    }
}
