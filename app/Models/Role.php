<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'roles';

    protected $guarded = [];

    public function hasPermissions()
    {
        return $this->hasMany('App\\Models\\RoleHasPermissions', 'role_id', 'id');
    }
}
