<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pages extends Model
{
    protected $table = 'pages';
    public function titleImage(){
        return $this->hasOne('App\Models\MediaLibrary','id','title_img');
    }
}
