<?php

namespace App\Models;

use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WarehouseModel extends Model
{
    use HandlesAuthorization;


    protected $guard_name = 'web';
    protected $table = 'warehouses';
    protected $primaryKey = 'guid';
    protected $keyType = 'string';

    protected $casts = [
        'unleashed_modified_on' => 'datetime'
    ];


}
