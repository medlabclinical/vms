<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Laravel\Nova\Actions\Actionable;
use Spatie\Permission\Traits\HasRoles;

class ProductBatchNumbersModel extends Model
{

    protected $guard_name = 'web';
    protected $table      = 'batch_numbers';
    protected $primaryKey = 'id';


    protected $dates = [
        'expiry_date',
        'unleashed_created_on',
        'unleashed_modified_on'
    ];


    /**
     * Batch Numbers has many skus
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(ProductModel::class, 'product_code', 'product_code');
    }

    public function scopeGetBatchListByProduct($query, $product_code)
    {
        $vitamins_warehouse = env("VITAMINS_WAREHOUSE", "Alexandria");
        $pharma_warehouse = env("PHARMA_WAREHOUSE", "ANSPEC");

        $data = $query->where('batch_numbers.product_code', $product_code)
                    ->join('warehouses', 'warehouses.warehouse_code', '=', 'batch_numbers.warehouse_code')
                    ->where(function ($query) use ($vitamins_warehouse, $pharma_warehouse) {
                           $query->where('warehouses.warehouse_name', '=', $vitamins_warehouse)
                                 ->orWhere('warehouses.warehouse_name', '=', $pharma_warehouse);
                       })
                    ->where('warehouses.allow_on_cart', '1')
                    ->where('batch_numbers.status', 'Available')
                    ->where('batch_numbers.quantity', '>', 0)
                    ->orderBy('batch_numbers.expiry_date','ASC')
                    ->select(DB::raw("batch_number, CONVERT(expiry_date, DATE) AS expiry_date, batch_numbers.warehouse_code, batch_numbers.quantity"))
                    ->get();

        return $data;
    }

}
