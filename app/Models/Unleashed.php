<?php

namespace App\Models;

use App\Models\BrandModel;
use Illuminate\Database\Eloquent\Model;

class Unleashed extends Model
{
    /**
     * The table name
     *
     * @var string
     */
    protected $table      = 'unleashed';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType    = 'string';

    public function brand()
    {
        return $this->hasOne(BrandModel::class, 'id', 'brand_id');
    }

}
