<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SecondaryChannel extends Model
{
    use SoftDeletes;

    protected $guard_name = 'web';
    protected $table = 'channels';
    protected $primaryKey = 'id';

    protected $fillable = [
        'parent_id','name','description'
    ];

    /**
     * A channel can be linked to many customers
     *
     * @return void
     */
    public function unleashedcustomer()
    {
        return $this->hasMany(UnleashedCustomer::class, 'secondary_channel_id');
    }

    /**
     * A channel beongs to a parent channel
     *
     * @return void
     */
    public function parent()
    {
        return $this->belongsTo(PrimaryChannel::class, 'parent_id')->orderBy('id');
    }

    /**
     * Relationship counter
     *
     * @return void
     */
    public function unleashedcustomer_count()
    {
        return $this->hasMany(UnleashedCustomer::class)->count();
    }
}
