<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductGroupModel extends Model
{

    protected $guard_name = 'web';
    protected $table      = 'product_groups';
    protected $primaryKey = 'id';

    /**
     * This is to associate the products
     */
    public function product_family_group()
    {
        return $this->hasMany(ProductFamilyGroupModel::class, 'product_group_id');
    }

    /**
     * This is to associate the products
     */
    public function product_family()
    {
        return $this->belongsToMany(ProductFamilyModel::class, 'product_family_groups', 'product_group_id', 'product_family_id');
    }

    /**
     * This is to associate the products
     */
    public function product_family_group_count()
    {
        return $this->hasMany(ProductFamilyGroupModel::class, 'product_group_id');
    }
    public function image(){
        return $this->hasOne('App\Models\MediaLibrary','id','image');
    }
    public function preview(){
        return $this->hasOne('App\Models\MediaLibrary','id','preview_image');
    }
}
