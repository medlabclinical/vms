<?php

namespace App\Models;

use App\Securepay\Payment;
use Illuminate\Database\Eloquent\Model;

class ShoppingCartPaymentModel extends Model
{

    const PAYMENT_COMPLETED = 1;
    const PAYMENT_PENDING   = 0;

    public $incrementing    = true;

    protected $guard_name   = 'web';
    protected $primaryKey   = 'payment_id';
    protected $table        = 'shopping_cart_payments';
    protected $keyType      = 'integer';
    protected $guarded = [];

    public function shoppingcart()
    {
        return $this->belongsTo(ShoppingCartModel::class, 'cart_id');
    }
}
