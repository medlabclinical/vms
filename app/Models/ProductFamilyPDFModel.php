<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Nova\Actions\Actionable;
use Spatie\Permission\Traits\HasRoles;

class ProductFamilyPDFModel extends Model
{
    use HasRoles, Actionable, SoftDeletes;

    public $incrementing  = true;

    protected $guard_name = 'web';
    protected $primaryKey = 'id';
    protected $table      = 'product_family_pdfs';
    protected $keyType    = 'integer';

    /**
     * [Description]
     *
     * @return void
     */
    public function product_family()
    {
        return $this->belongsTo(ProductFamilyModel::class, 'product_family_id');
    }

    /**
     * [Description]
     *
     * @return void
     */
    public function product_pdf()
    {
        return $this->belongsTo(ProductPdfModel::class, 'product_pdf_id');
    }
}
