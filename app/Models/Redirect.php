<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Redirect extends Model
{
    public $incrementing  = true;

    protected $guard_name = 'web';
    protected $primaryKey = 'id';
    protected $table      = 'website_redirects';
    protected $keyType    = 'integer';


    /**
     * [Description]
     *
     * @return void
     */
    public function product()
    {
        return $this->belongsTo(ProductModel::class, 'product_guid', 'guid');
    }
    protected $guarded = [];
}
