<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Publications extends Model
{
    protected $table = 'publications';
    public function member(){
        return $this->belongsToMany(Members::class,'member_publications_and_presentations','publications_and_presentations_id','member_id');
    }
    public function product_family(){
        return $this->belongsToMany(ProductFamilyModel::class,'product_multiple_relation','publication_id','family_id');
    }
    public function clinical_trial(){
        return $this->belongsToMany(ClinicalTrials::class,'product_multiple_relation','publication_id','clinical_trial_id');
    }
    public function product_ingredient(){
        return $this->belongsToMany(ProductIngredientModel::class,'product_multiple_relation','publication_id','ingredient_id');
    }
    public function article(){
        return $this->belongsToMany(Articles::class,'product_multiple_relation','publication_id','article_id');
    }
    public function product_group(){
        return $this->belongsToMany(ProductGroupModel::class,'product_multiple_relation','publication_id','product_group_id');
    }
}
