<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ProductPdfModel extends Model
{

    protected $guard_name = 'web';
    protected $primaryKey = 'id';
    protected $table      = 'product_pdfs';
    protected $keyType    = 'integer';

    /**
     * [Description]
     *
     * @return void
     */

    public function pdf_type(){
        return $this->belongsTo(PdfTypes::class,'pdf_type_id');
    }


    public function product_family_pdf_count()
    {
        return $this->hasMany(ProductFamilyPDFModel::class, 'product_pdfs_id');
    }

    /**
     * 1 product family has many ingredients
     *
     * @return void
     */
    public function product_family_pdf()
    {
        return $this->hasMany(ProductFamilyPDFModel::class, 'product_pdfs_id');
    }

    /**
     * 1 product family has many ingredients
     *
     * @return void
     */
    public function product_family()
    {
        return $this->belongsToMany(ProductFamilyModel::class, 'product_family_pdfs','product_pdfs_id', 'product_family_id');
    }
    public function brand(){
        return $this->belongsToMany(BrandModel::class,'product_pdfs_brand','product_pdf_id','brand_id');
    }
    public function image(){
        return $this->hasOne('App\Models\MediaLibrary','id','preview_img');
    }
}
