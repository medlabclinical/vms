<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AphraListModel extends Model
{
    use SoftDeletes;
    // use Searchable;
    protected $guard_name = 'web';
    protected $table = 'aphra_list';
    protected $casts = ['date' => 'date'];

}
