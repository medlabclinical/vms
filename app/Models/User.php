<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;


class User extends Authenticatable
{
    use Notifiable,SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'persons';

//    protected $guard = 'practitioner';
    protected $fillable = [
        'first_name','last_name', 'email', 'password','association_number','association_id','phone','mobile','fax',
        'job_title','access_level','preffered_contact','ucustomer_id','status','comment','salutation','need_credit','is_business_owner','is_declared_bankrupt','is_refused_credit',
        'pre_status','ahpra_number'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function role()
    {
        return $this->hasOne('App\\Models\\Role', 'id', 'role_id');
    }
    public function routers()
    {
        return $this->hasMany('App\\Models\\RoleHasPermissions', 'role_id', 'role_id');
    }
    public function practitioner(){
        return $this->belongsToMany(UnleashedCustomer::class,'customer_persons','person_id','customer_id');

    }

    /**
     * Person has an association
     *
     * @return void
     */
    public function association()
    {
        return $this->belongsTo(AssociationModel::class);
    }

    /**
     * Person has many modalities
     *
     * @return void
     */
    public function modalityperson()
    {
        return $this->belongsToMany(Modality::class, 'modality_persons', 'person_id', 'modality_id');
    }
    public function loginSecurity()
    {
        return $this->hasOne('App\Models\LoginSecurity');
    }


}

