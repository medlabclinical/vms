<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CustomerPerson extends Model
{
    protected $table = 'customer_persons';
    protected $fillable = [
        'customer_id','person_id','status',
    ];
    public function business(){
        return $this->hasOne(UnleashedCustomer::class,'guid','customer_id');
    }
    public function scopeCartPersonList($query, $guid)
    {
        $result = $query->where("customer_id", $guid)
            ->join('persons', 'persons.id', '=', 'customer_persons.person_id')
            ->where('customer_persons.deleted_at', NULL)
            ->where('persons.status', '!=', '2')
            ->select(DB::raw('persons.*'));

        return $result;
    }
}
