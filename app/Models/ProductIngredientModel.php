<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductIngredientModel extends Model
{

    protected $guard_name = 'web';
    protected $primaryKey = 'id';
    protected $table      = 'product_ingredients';
    protected $keyType    = 'integer';

    /**
     * Returns the relationship count
     *
     * @return void
     */
    public function product_family_ingredient_count()
    {
        return $this->hasMany(ProductFamilyIngredientModel::class, 'product_ingredient_id');
    }

    /**
     * 1 product family has many ingredients
     *
     * @return void
     */
    public function product_family_ingredient()
    {
        return $this->hasMany(ProductFamilyIngredientModel::class, 'product_ingredient_id');
    }

    /**
     * 1 product family has many ingredients
     *
     * @return void
     */
    public function product_family()
    {
        return $this->belongsToMany(ProductFamilyModel::class, 'product_family_ingredients','product_ingredient_id', 'product_family_id')->withPivot('quantity','equiv_to','is_excipient');
    }
}
