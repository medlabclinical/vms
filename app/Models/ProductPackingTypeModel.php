<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductPackingTypeModel extends Model
{

    protected $guard_name = 'web';
    protected $primaryKey = 'id';
    protected $table      = 'product_bottle_types';
    protected $keyType    = 'integer';

    /**
     * [Description]
     *
     * @return void
     */
    public function product_packaging()
    {
    	return $this->hasMany(ProductPackagingModel::class, 'product_bottle_type_id');
    }
}
