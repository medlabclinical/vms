<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SubscriptionReportOpenModel extends Model
{

	public $incrementing  = true;

    protected $guard_name = 'web';
    protected $primaryKey = 'id';
    protected $table      = 'subscription_report_open';
    protected $keyType    = 'integer';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
		'campaign_id',
		'list_id',
		'email_id',
		'email_address',
		'open_count',
		'last_open',
		'raw_log'
    ];

}
