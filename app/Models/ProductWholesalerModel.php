<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ProductWholesalerModel extends Model
{

    protected $guard_name = 'web';
    protected $table = 'product_wholesaler';
    protected $primaryKey = 'id';

    /**
     * [Description]
     *
     * @return void
     */
    public function product()
    {
        return $this->belongsTo(ProductModel::class, 'product_guid');
    }

    /**
     * [Description]
     *
     * @return void
     */
    public function wholesaler()
    {
        return $this->belongsTo(WholesalerModel::class, 'wholesaler_id');
    }
}
