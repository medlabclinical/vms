<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SubscriptionTypeModel extends Model
{

	public $incrementing  = true;

    protected $guard_name = 'web';
    protected $primaryKey = 'id';
    protected $table      = 'subscription_type';
    protected $keyType    = 'integer';

    /**
     * Subscriptions of the given email
     *
        select
            persons.first_name, persons.last_name,
            subscription.email,
            subscription.user_type,
            subscription.person_id,
            subscription_type.name,
            subscription_type.user_type AS list_type,
            subscription_type.list_id,
            subscription_type.category_id,
            subscription_type.interest_id

        from subscription_type
        left join subscription on (subscription.type_id = subscription_type.id)
        join persons on (persons.id = subscription.person_id and persons.email = subscription.email)
        join customer_persons on (customer_persons.person_id = persons.id and customer_id = 'ea0583d2-0296-4f2f-980f-429bf148f2fd')
        where subscription.email is not null
        and subscription.deleted_at is null
        and persons.deleted_at is null
        and customer_persons.deleted_at is null
        and subscription_type.user_type = 'person'
        and subscription.user_type = 'person'

        select
            patient.first_name, patient.last_name,
            subscription.email,
            subscription.user_type,
            subscription.person_id,
            subscription_type.name,
            subscription_type.user_type AS list_type,
            subscription_type.list_id,
            subscription_type.category_id,
            subscription_type.interest_id

        from subscription_type
        left join subscription on (subscription.type_id = subscription_type.id)
        join patient on (subscription.person_id = patient.id and patient.guid = 'ea0583d2-0296-4f2f-980f-429bf148f2fd')
        where subscription.email is not null
        and subscription.deleted_at is null
        and patient.deleted_at is null
        and subscription_type.user_type = 'patient'
        and subscription.user_type = 'patient'

     * @return void
     */
    public function scopeSummaryList($query, $customer_guid)
    {
    	// get business emails
        $subscriptions = $query->whereNotNull('subscription.email')
            ->leftJoin('subscription', 'subscription.type_id', 'subscription_type.id')
            ->join('persons', 'persons.id', 'subscription.person_id')
            ->join('customer_persons', function ($join) use ($customer_guid) {
                $join->on('persons.id', 'customer_persons.person_id');
                $join->on('customer_persons.customer_id', '=', DB::raw("'" .$customer_guid . "'"));
            })
            ->whereNull('subscription.deleted_at')
            ->whereNull('customer_persons.deleted_at')
            ->whereNull('persons.deleted_at')
            ->where('subscription_type.user_type', 'person')
            ->where('subscription.user_type', 'person')
            ->select(
                        'persons.first_name',
                        'persons.last_name',

                        'subscription.email',
                        'subscription.user_type',
                        'subscription.person_id',
                        'subscription.action',

                        'subscription_type.name',
                        'subscription_type.list_id',
                        'subscription_type.category_id',
                        'subscription_type.interest_id'
                    )
            ->get();

        return $subscriptions;
    }

    public function scopeSummaryListPatient($query, $customer_guid)
    {
        // get business emails
        $subscriptions = $query->whereNotNull('subscription.email')
            ->leftJoin('subscription', 'subscription.type_id', 'subscription_type.id')
            ->join('patient', function ($join) use ($customer_guid) {
                $join->on('patient.id', 'subscription.person_id');
                $join->on('patient.guid', '=', DB::raw("'" .$customer_guid . "'"));
            })
            ->whereNull('subscription.deleted_at')
            ->whereNull('patient.deleted_at')
            ->where('subscription_type.user_type', 'patient')
            ->where('subscription.user_type', 'patient')
            ->select(
                        'patient.first_name',
                        'patient.last_name',

                        'subscription.email',
                        'subscription.user_type',
                        'subscription.person_id',
                        'subscription.action',

                        'subscription_type.name',
                        'subscription_type.list_id',
                        'subscription_type.category_id',
                        'subscription_type.interest_id'
                    )
            ->get();

        return $subscriptions;
    }

}
