<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ProductFamilyGroupModel extends Model
{


    protected $guard_name = 'web';
    protected $primaryKey = 'id';
    protected $table      = 'product_family_groups';
    protected $keyType    = 'integer';

    /**
     * [Description]
     *
     * @return void
     */
    public function product_family()
    {
    	return $this->belongsTo(ProductFamilyModel::class, 'product_family_id');
    }

    /**
     * [Description]
     *
     * @return void
     */
    public function product_group()
    {
    	return $this->belongsTo(ProductGroupModel::class, 'product_group_id');
    }
}
