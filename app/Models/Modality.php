<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Modality extends Model
{
    use SoftDeletes;

    protected $guard_name = 'web';
    protected $table      = 'modalities';
    protected $primaryKey = 'id';


    /**
     * A Person has many modalities
     *
     * @return void
     */
    public function modalityperson()
    {
        return $this->hasMany(ModalityPerson::class, 'modality_id');
    }

    /**
     * Return all active users
     *
     * @return relationship
     */
    public function scopeModalityList()
    {
        $keypair = null;
        $query = $this->where("deleted_at", NULL)->get();
        foreach ($query as $item) {
            $keypair["{$item->id}"] = $item->name;
        }
        return $keypair;
    }
}
