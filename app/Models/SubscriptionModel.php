<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class SubscriptionModel extends Model
{
	use SoftDeletes;

	public $incrementing  = true;

    protected $guard_name = 'web';
    protected $primaryKey = 'id';
    protected $table      = 'subscription';
    protected $keyType    = 'integer';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'updated_at',
        'deleted_at',
        'customer_guid',
        'person_id',
        'type_id',
        'email',
        'user_type',
        'customer_code',
        'log',
        'action'
    ];

}
