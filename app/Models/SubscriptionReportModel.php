<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SubscriptionReportModel extends Model
{

	public $incrementing  = true;

    protected $guard_name = 'web';
    protected $primaryKey = 'id';
    protected $table      = 'subscription_report';
    protected $keyType    = 'integer';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
		'campaign_id',
		'campaign_title',
		'list_name',
		'subject_line',
		'preview_text',
		'emails_sent',
		'unsubscribed',
		'send_time',
		'raw_log',
    ];
}
