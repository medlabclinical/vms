<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ArticleCategory extends Model
{

    // use Searchable;
    protected $guard_name = 'web';
    protected $table = 'article_category';
    protected $casts = ['date' => 'date'];
}
