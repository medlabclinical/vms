<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Articles extends Model
{
    protected $table = 'articles';
    public function member()
    {
        return $this->belongsToMany(Members::class,'product_multiple_relation','article_id','member_id');
    }
    public function article_category(){
        return $this->belongsToMany(ArticleCategory::class,'product_multiple_relation','article_id','article_category_id');
    }

    public function product_family(){
        return $this->belongsToMany(ProductFamilyModel::class,'product_multiple_relation','publication_id','family_id');
    }
    public function clinical_trial(){
        return $this->belongsToMany(ClinicalTrials::class,'product_multiple_relation','publication_id','clinical_trial_id');
    }
    public function product_ingredient(){
        return $this->belongsToMany(ProductIngredientModel::class,'product_multiple_relation','publication_id','ingredient_id');
    }
    public function publication(){
        return $this->belongsToMany(Publications::class,'product_multiple_relation','publication_id','article_id');
    }
    public function product_group(){
        return $this->belongsToMany(ProductGroupModel::class,'product_multiple_relation','publication_id','product_group_id');
    }
    public function brand(){
        return $this->belongsToMany(BrandModel::class,'product_multiple_relation','article_id','brand_id');
    }
}
