<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderLinesModel extends Model
{

    use \Awobaz\Compoships\Compoships;
	protected $guard_name = 'web';
	protected $table      = 'order_lines';
	protected $primaryKey = 'order_number';
	protected $keyType    = 'string';

    /**
     * [Description]
     *
     * @return void
     */
	public function order()
	{
		return $this->belongsTo(OrderModel::class, 'order_number');
	}
    public function product()
    {
        return $this->hasOne(ProductModel::class, 'product_code','product_code');
    }
}
