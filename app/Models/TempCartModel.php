<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Auth;

class TempCartModel extends Model
{

	public $incrementing  = true;

    protected $guard_name = 'web';
    protected $primaryKey = 'id';
    protected $table      = 'web_temp_cart';
    protected $keyType    = 'integer';
    protected $guarded = [];

    public function scopeGetTempCart($query, $guid, $personid)
    {
        $query = $query->where('customer_id', $guid)
                       ->where('person_id', $personid)
                       ->first();
        if (!$query) {
        	return '';
        }

        return $query;
    }

    public function scopeGetTempCartById($query, $id)
    {
        $query = $query->where('id', $id)
                       ->first();
        if (!$query) {
            return '';
        }

        return $query;
    }

    public function scopeGetTempCartMenuTotal($query)
    {
        $guid = '';
        if ($business = json_decode(Cookie::get('business'), true)) {
            $guid = $business["guid"];
        }


        $result = $query->where('customer_id', $guid)->where('person_id',Auth::guard('practitioner')->user()->id)
                        ->first();
//        dd('10');
        if (!$result) { return 0; }

        // Retrive and json decode the cart content
        $json_cart = json_decode($result->cart_content, true);

        $total_quantity_count = 0;
        foreach ($json_cart as $item) {
            $total_quantity_count += $item["cart_quantity"];
        }

        return $total_quantity_count;
    }

    public function scopeUpdateTempCart($query, $cartid, $json_cart) {

        $isSuccess = $query->where('id', $cartid)
              ->update(['cart_content' => $json_cart]);

        return array('message' => $isSuccess);
    }

    public function scopeDeleteTempCart($query, $guid) {

        $isDeleted = $query->where('customer_id', $guid)->delete();
        return json_encode(array('result' => $isDeleted));
    }
}
