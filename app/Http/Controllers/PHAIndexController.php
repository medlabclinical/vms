<?php

namespace App\Http\Controllers;

use App\Models\MediaLibrary;
use App\Models\Pages;
use App\Models\Patents;
use App\Models\Subweb;
use App\Models\Translate;
use Illuminate\Http\Request;
use Session;
use URL;
use Mail;
use Auth;
use DB;
use App\Http\Middleware\AppAzure;

class PHAIndexController extends Controller
{

    public function index(){
//        dd(Auth()->user());
//        dd(substr($_SERVER['HTTP_HOST'],0,7));
        $image_url=config("app.image_url");
        $content = Pages::where('page','home')->where('website','pharmaceuticals')->first();
        $content->title_img=MediaLibrary::where('id',$content->title_img)->first()->name;
        $content->second_img=$content->second_img?MediaLibrary::where('id',$content->second_img)->first()->name:NULL;
        $content->third_img=$content->third_img?MediaLibrary::where('id',$content->third_img)->first()->name:NULL;
        $subweb = DB::table('subweb')->join('nova_media_library','subweb.image','=','nova_media_library.id')
            ->select('subweb.*','nova_media_library.name as image')->get();
        return view('pharma/pages/home/index',compact('content','image_url','subweb'));
    }
    public function locale($locale)
    {
        if (!in_array($locale, ['cn', 'en'])) {
            $locale = 'cn';
        }

        Session::put('locale', $locale);

        return redirect(url(URL::previous()));
    }



}
