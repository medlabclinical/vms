<?php

namespace App\Http\Controllers;

use App\Models\ArticleCategory;
use App\Models\Articles;
use App\Models\MediaLibrary;
use App\Models\Pages;
use App\Models\PdfTypes;
use App\Models\ProductFamilyModel;
use App\Models\ProductMultipleRelation;
use App\Models\ProductPdfModel;
use http\Env;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Storage;

class ArticlesController extends Controller
{
    public function index(Request $request)
    {

        $role = $request->has('role')?$request->get('role'):"*";
        $article_category = $request->has('article_category')?$request->get('article_category'):"*";
        $allArticles = Articles::where('publish', 1)->whereNull('deleted_at')
//            ->when($role!="*", function ($query) use ($request) {
//                $query->where('req_login', $request->get('role'));
//            })
            ->whereHas('brand',function ($q){
                $q->where('brand.id',2);
            })->orderBy('date', 'desc')->get();

        $allData = Articles::where('publish',1)->whereNull('deleted_at')->whereHas('brand',function ($q){
            $q->where('brand.id',2);
        })->get();
        $category = array();
        $list = array();
        foreach ($allData as $article){
            if ($article->article_category()->first()) {
                $name = $article->article_category()->first()->name;
                $article->category = $name;
                if (!isset($list[$name])) {
                    array_push($category, $name);
                    $list[$name] = [];
                }
                array_push($list[$name], $article);
            } else {
                $article->category = '';
            }
        }
        if ($article_category!="*") {
            if (ArticleCategory::where('name', $article_category)->exists()) {
                $data = collect();
                $id = ArticleCategory::where('name', $article_category)->first()['id'];
                foreach ($allArticles as $item) {
                    if (in_array($id, array_column(ProductMultipleRelation::where('article_id', $item->id)->get()->toArray(), 'article_category_id'))) {
                        $data->add($item);
                    }
                }
                $allArticles = $data;
                $content = ArticleCategory::where('name',$article_category)->first();
            }else{
                return redirect(404);
            }
        }else{
            $content = Pages::where('page', 'education')->where('website', 'vms')->first();
        }
        $Filter = $allArticles;
        $Filter = array_column($Filter->toArray(),'req_login');
        $level_amount = array();
        $level_amount[0] = count($Filter)>0?1:0;
        if(!$level_amount[0]){
            return redirect(404);
        }
        $level_amount[1] = in_array(1,$Filter)?1:0;
        $level_amount[2] = in_array(0,$Filter)?1:0;
        if($role != '*'){
            foreach ($allArticles as $key=>$article) {
                if($article->req_login!=$role){
                    unset($allArticles[$key]);
                }
            }
        }
        $image_url = config("app.image_url");

        foreach ($allArticles as $article) {
            $article->title_img = $article->title_img ? MediaLibrary::where('id', $article->title_img)->first()->name : NULL;
            $article->second_img = $article->second_img ? MediaLibrary::where('id', $article->second_img)->first()->name : NULL;
            $article->third_img = $article->third_img ? MediaLibrary::where('id', $article->third_img)->first()->name : NULL;

        }
        $slice = $allArticles->slice(config('app.page-size') * ($request->get('page', 1) - 1), config('app.page-size'))->all();
        $allArticles = new LengthAwarePaginator($slice, count($allArticles), config('app.page-size'), $request->get('page', 1), ['path' => $request->url(), 'query' => $request->query()]);
        return view('vms/pages/education/articles', compact('list', 'allArticles', 'image_url', 'content', 'category', 'article_category','role','level_amount'));
    }


    public function getContent($id)
    {
        if(!strpos($id,'-')){
            abort(404);
        }
        $id = substr($id,strrpos($id,"-")+1);
        if(!Articles::where('id', $id)->whereHas('brand', function ($q) {
            $q->where('brand.id',2);
        })->whereNull('deleted_at')->exists()){
            abort(404);
        }
        $image_url = config("app.image_url");
        if(!Articles::where('id', $id)->exists()){
            return redirect(404);
        }
        $article = Articles::where('id', $id)->first();
        $article->title_img = $article->title_img ? MediaLibrary::where('id', $article->title_img)->first()->name : NULL;
        $article->second_img = $article->second_img ? MediaLibrary::where('id', $article->second_img)->first()->name : NULL;
        $article->third_img = $article->third_img ? MediaLibrary::where('id', $article->third_img)->first()->name : NULL;
        $member = $article->member()->get()->toArray();
        $related_article = [];
        if (count($member)) {
            for ($i = 0; $i < count($member); $i++) {
                if ($member[$i]['picture']) {
                    $picture = MediaLibrary::where('id', $member[$i]['picture'])->first()->name;
                    $member[$i]['picture'] = $picture;
                    foreach (array_column(ProductMultipleRelation::where('member_id', $member[$i]['id'])->get()->toArray(), 'article_id') as $item) {
                        if ($item != $id && $item != null) {
                            $related_article[] = $item;
                        }
                    }
                }
            }
        }
        $article->related_article = Articles::whereIn('id', $related_article)->whereNull('deleted_at')->where('publish', 1)->whereHas('brand',function ($q){
            $q->where('brand.id',2);
        })->orderBy('date', 'desc')->limit(3)->get();
//        foreach ($article->related_article as $k => $a) {
//            if (!in_array('1', array_column($a->product()->get()->toArray(), 'show_on_website'))) {
//                $article->related_article->forget($k);
//            } elseif (!in_array('0', array_column($a->product()->get()->toArray(), 'obsolete'))) {
//                $article->related_article->forget($k);
//            }
//        }
//        $article->related_article=array_values(array_unique($related_article));
        $article->member = $member;
        $article->product_family = $article->product_family()->where('product_family.brand_id',2)->whereNull('product_family.deleted_at')->get();
        foreach ($article->product_family as $k => $item){
            if (!$item || !in_array('1', array_column($item->product()->get()->toArray(), 'show_on_website'))
                || !in_array('0', array_column($item->product()->get()->toArray(), 'obsolete'))) {
                $article->product_family->forget($k);
            }
        }
        $article->clinical_trial = $article->clinical_trial()->whereNull('clinical_trials.deleted_at')->get()->toArray();
        $article->product_ingredient = $article->product_ingredient()->whereNull('product_ingredients.deleted_at')->get()->toArray();
        $article->publication = $article->publication()->whereNull('publications.deleted_at')->get()->toArray();
        $article->article_category = $article->article_category()->whereNull('article_category.deleted_at')->get()->toArray();
        foreach ($article->related_article as $item){
            $item->title_img = $item->title_img ? MediaLibrary::where('id', $item->title_img)->first()->name : NULL;
            $item->second_img = $item->second_img ? MediaLibrary::where('id', $item->second_img)->first()->name : NULL;
            $item->third_img = $item->third_img ? MediaLibrary::where('id', $item->third_img)->first()->name : NULL;
        }
//        $article_category = ArticleCategory::all();
//        dd($article);
//        $comment = new CommentsController();
        $comments = (new CommentsController())->index($id);
//        dd($comments);
        if ($article->article_type == 'image') {
            return view('vms/pages/education/image-articles', compact('article', 'image_url', 'comments'));
        }elseif ($article->article_type == 'pdf') {
            return view('vms/pages/education/pdf-articles', compact('article', 'image_url', 'comments'));
        } else {
            return view('vms/pages/education/video-articles', compact('article', 'image_url', 'comments'));
        }
    }

    public function getPDF(Request $request)
    {
        $role = $request->has('role') ? $request->get('role') : "*";
        $pdf_type = $request->has('pdf_type') ? $request->get('pdf_type') : "*";
        if ($pdf_type == "*") {
            $AllPDF = ProductPdfModel::where('obsolete', '!=', '1')->whereNull('deleted_at')->whereHas('brand', function ($q) {
                $q->where('brand.id', 2);
            });
            $content = Pages::where('page', 'flyers')->where('website', 'vms')->first();
        } else {
            if(!PdfTypes::where('name',$pdf_type)->exists()){
                return redirect(404);
            }
            $AllPDF = ProductPdfModel::where('obsolete', '!=', '1')->whereNull('deleted_at')->whereHas('brand', function ($q) {
                $q->where('brand.id', 2);
            })->whereHas('pdf_type', function ($q) use ($pdf_type) {
                $q->where('pdf_type.name', $pdf_type);
            });
            $content = PdfTypes::where('name',$pdf_type)->first();
        };

        $Filter = $AllPDF;
        $Filter = array_column($Filter->get()->toArray(),'access_level');
        $level_amount = array();
        $level_amount[0] = count($Filter)>0?1:0;
        if(!$level_amount[0]){
            return redirect(404);
        }
        $level_amount[1] = in_array(1,$Filter)?1:0;
        $level_amount[2] = in_array(2,$Filter)?1:0;
        $level_amount[3] = in_array(3,$Filter)?1:0;
        if($role != '*'){
            $AllPDF = $AllPDF->where('access_level',$role)->orderBy('product_pdfs.updated_at', 'desc')->get();
        }else{
            $AllPDF = $AllPDF->orderBy('product_pdfs.updated_at', 'desc')->get();
        };
        foreach ($AllPDF as $item){
            if(Storage::disk('s3private')->exists($item->file_name)){
                $item->file_name = Storage::disk('s3private')->temporaryUrl(
                    $item->file_name, now()->addMinutes(5)
                );
            }
        }

        $image_url = config("app.image_url");
        $content->title_img = $content->title_img ? MediaLibrary::where('id', $content->title_img)->first()->name : NULL;
        $content->second_img = $content->second_img ? MediaLibrary::where('id', $content->second_img)->first()->name : NULL;
        $content->third_img = $content->third_img ? MediaLibrary::where('id', $content->third_img)->first()->name : NULL;

        $slice = $AllPDF->slice(config('app.page-size') * ($request->get('page', 1) - 1), config('app.page-size'))->all();
        $AllPDF = new LengthAwarePaginator($slice, count($AllPDF), config('app.page-size'), $request->get('page', 1), ['path' => $request->url(), 'query' => $request->query()]);
//        dd($AllPDF);
        return view('vms/pages/education/flyers', compact('AllPDF', 'image_url', 'content', 'pdf_type', 'role','level_amount'));
    }

    public function getPDFDetails($id)
    {
        if(!strpos($id,'-')){
            abort(404);
        }
        $id = substr($id,strrpos($id,"-")+1);
        if(!ProductPdfModel::where('id', $id)->where('obsolete', '!=', '1')->whereHas('brand', function ($q) {
            $q->where('brand.id',2);
        })->whereNull('deleted_at')->exists()){
            abort(404);
        }
        $image_url = config("app.image_url");
        if(!ProductPdfModel::where('id', $id)->exists()){
            return redirect(404);
        }
        $pdf = ProductPdfModel::where('id', $id)->whereNull('deleted_at')->first();
        if(Storage::disk('s3private')->exists($pdf->file_name)){
            $pdf->file_name = Storage::disk('s3private')->temporaryUrl(
                $pdf->file_name, now()->addMinutes(5)
            );
        }
        $product_family = $pdf->product_family()->whereHas('brand', function ($q) {
            $q->where('brand.id',2);
        })->get();
        foreach ($product_family as $k => $a) {
            if (!in_array('1', array_column($a->product()->get()->toArray(), 'show_on_website'))) {
                $product_family->forget($k);
            } elseif (!in_array('0', array_column($a->product()->get()->toArray(), 'obsolete'))) {
                $product_family->forget($k);
            }
        }
        return view('vms/pages/education/pdf-details', compact('pdf', 'image_url','product_family'));

    }
    public function getEducationMenu()
    {
        $allData = Articles::where('publish', 1)->whereNull('deleted_at')->whereHas('brand', function ($q) {
            $q->where('brand.id', 2);
        })->get();
        $header_article_category = array();
        $list = array();
        foreach ($allData as $article) {
            if ($article->article_category()->first()) {
                $name = $article->article_category()->first()->name;
                $article->category = $name;
                if (!isset($list[$name])) {
                    array_push($header_article_category, $name);
                    $list[$name] = [];
                }
                array_push($list[$name], $article);
            } else {
                $article->category = '';
            }
        }
        $header_pdf_list = PdfTypes::whereHas('brand', function ($q) {
            $q->where('brand.id', 2);
        })->get();
        foreach ($header_pdf_list as $key => $item) {
            if (!count($item->hasPdf()->whereHas('brand', function ($q) {
                $q->where('brand.id', 2);
            })->where('product_pdfs.obsolete','!=','1')->whereNull('product_pdfs.deleted_at')->get())) {
                $header_pdf_list->forget($key);
            }
        }

        $image_url = config("app.image_url");
        $list = [];
        $list['Articles'] = ArticleCategory::whereIn('name', $header_article_category)->whereNull('deleted_at')->get();
        $list['Flyers'] = $header_pdf_list;
        $articles = Pages::where('page', 'education')->where('website','vms')->first();
        $flyers = Pages::where('page', 'flyers')->where('website','vms')->first();
        $header = 'Education';
        return view('vms/pages/education/education', compact('image_url', 'list','header','articles','flyers'));
    }
}
