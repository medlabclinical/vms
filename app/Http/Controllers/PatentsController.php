<?php

namespace App\Http\Controllers;

use App\Models\Patents;
use Illuminate\Http\Request;

class PatentsController extends Controller
{
    public function index(){
        $list = Patents::all();
        return view('patents',compact('list'));
    }
}
