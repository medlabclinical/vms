<?php

namespace App\Http\Controllers;

use App\Models\Articles;
use App\Models\AssociationModel;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Storage;
use Snowfire\Beautymail\Beautymail;
use App\Models\CustomerPerson;
use App\Models\MediaLibrary;
use App\Models\ModalityPerson;
use App\Models\Pages;
use App\Models\Patents;
use App\Models\Patient;
use App\Models\PatientAddress;
use App\Models\PrimaryChannel;
use App\Models\SecondaryChannel;
use App\Models\Modality;
use App\Models\Subweb;
use App\Models\Translate;
use App\Models\Addressbook;
use App\Models\UnleashedCustomer;
use App\Models\User;
use App\Models\WebsiteApplication;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Session;
use URL;
use Mail;
use Auth;
use DB;
use App\Http\Middleware\AppAzure;

class UserController extends Controller
{

    //User Information functions

    public function getRoleInfo()
    {
        $associations = AssociationModel::all();
        $primary_channel = PrimaryChannel::all();
        $second_channel = SecondaryChannel::all();
        $modality = Modality::all();
        try {
            if (Auth::guard('patient')->check()) {
                $role = Patient::where('id', Auth::guard('patient')->user()->id)->first();
                $patient_address = $role->patient_address()->first();
                $referral_code = $role->guid?$role->practitioner()->first()->referral_code:null;
                $referral_name = $role->guid?$role->practitioner()->first()->customer_name:null;
                return view('vms/pages/userInfo/patient-profile', compact('role', 'associations',
                    'patient_address','referral_code','referral_name'));

            } else if(Auth::guard('practitioner')->check()) {
                $role = User::where('id', Auth::guard('practitioner')->user()->id)->first();
                $business = UnleashedCustomer::where('guid',json_decode(Cookie::get('business'),true)['guid'])->first();
                $qualification=0;
                if(Storage::disk('s3private')->exists($role->qualification)){
                    $qualification = Storage::disk('s3private')->temporaryUrl(
                        $role->qualification, now()->addMinutes(5)
                    );
                }
                return view('vms/pages/userInfo/practitioner-profile', compact('role', 'associations',
                    'primary_channel', 'second_channel', 'modality','business','qualification'));
            }
        } catch (\Exception $e) {
            return redirect('/');

        }

    }

    //***Function for patient detail update***//

    public function editPatientInfo(Request $request)
    {
        Patient::where('id', $request->get('id'))
            ->update(['salutation' => $request->get('salutation'),
                'first_name' => $request->get('first_name'),
                'last_name' => $request->get('last_name'),
                'email' =>  $request->get('email'),
                'phone' => $request->get('phone'),
                'mobile' => $request->get('email_notification'),
                'email_notification' => $request->get('mobile'),
                'guid' => UnleashedCustomer::where('referral_code', $request->get('referral_code'))->first()->guid,

            ]);

        PatientAddress::where('id', $request->get('patient_address_id'))
            ->updateorInsert([
                'patient_id' => $request->get('id'),
                'address_line_1' => $request->get('address_line_1'),
                'address_line_2' => $request->get('address_line_2'),
                'suburb' => $request->get('suburb'),
                'postcode' => $request->get('postcode'),
                'city' => $request->get('city'),
                'state' => $request->get('state'),
                'country' => $request->get('country'),
            ]);
        return response()->json(['msg' => 'success']);
    }

    //***Function for practitioner detail update***//

    public function updatePractitionerInfo(Request $request)
    {
//        dd($request->all());
        $salutation = $request->get('salutation');
        $first_name = $request->get('first_name');
        $last_name = $request->get('last_name');
        $email = $request->get('email');
        $phone = $request->get('phone');
        $mobile = $request->get('mobile');
        $address_line_1 = $request->get('address_line_1');
        $address_line_2 = $request->get('address_line_2');
        $suburb = $request->get('suburb');
        $postcode = $request->get('postcode');
        $country = $request->get('country');
        $state = $request->get('state');
        $city = $request->get('city');
        $business_phone = $request->get('business_phone');
        $fax_number = $request->get('fax_number');
//        $password =$request->get('current_password');

        $association_number = $request->get('association_number');
        $association_id = $request->get('association_name');

        $job_title = $request->get('job_title');

        $primary_profession = $request->get('primary_profession');

        $primary_channel = $request->get('primary_channel');
        $second_channel = $request->get('second_channel');

        $email_notification = $request->get('email_notification');

        $ahpra_number = $request->get('ahpra_number');
        $business = UnleashedCustomer::where('guid',json_decode(Cookie::get('business'),true)['guid'])->first();
        if($request->file('qualification')){

            $extension = $request->file('qualification')->getClientOriginalExtension();
            $practitionername = preg_replace("/[^a-zA-Z]+/", "", trim($request->get('first_name') . "_" . $request->get('last_name')));
            $s3_filename = strtolower($practitionername) . "-" . time() . '.' . $extension;
            $image = $request->file('qualification');

            $s3_filepath = 'registration/' . $s3_filename;
            $s3 = Storage::disk('s3private');
            $s3->put($s3_filepath, file_get_contents($image), 'private');

            $qualification = $s3_filepath;
        }else{
            $qualification = $request->get('old_qualification')?:null;
        }


        //Persons table is updated
        User::where('id', $request->get('id'))
            ->update(['salutation' => $salutation,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'email' => $email,
                'mobile' => $mobile,
                'phone' => $phone,
                'job_title' => $job_title,
                'association_number' => $association_number,
                'association_id' => $association_id,
                'ahpra_number' => $ahpra_number,
                'email_notification' => isset($email_notification) ? 1 : 0,
                'qualification' => $qualification,

            ]);


        //Ucustomers addressbook is updated

        Addressbook::where('customer_code', $business->guid)
            ->update([
                'address_line_1' => $address_line_1,
                'address_line_2' => $address_line_2,
                'suburb' => $suburb,
                'postcode' => $postcode,
                'city' => $city,
                'state' => $state,
                'country' => $country,
            ]);
        ModalityPerson::where('person_id', Auth::guard('practitioner')->user()->id)
            ->updateOrInsert([
                'person_id' => Auth::guard('practitioner')->user()->id,
                'modality_id' => Modality::where('name', $primary_profession)->first()->id,
            ]);

        //Ucustomers table field updated

//        if (!UnleashedCustomer::where('guid', Auth::guard('practitioner')->user()->practitioner()->first()->guid)->first()->payment_term) {
//            UnleashedCustomer::where('guid', Auth::guard('practitioner')->user()->practitioner()->first()->guid)
//                ->update([
//                    'phone_number' => $business_phone,
//                    'fax_number' => $fax_number,
//                    'channel_id' => PrimaryChannel::where('name', $primary_channel)->first()->id,
//                    'secondary_channel_id' => SecondaryChannel::where('name', $second_channel)->first()->id,
//                    'trading_name' => $request->get('business_name'),
//                    'status' => 1,
//                    'payment_term' => 'COD',
//                ]);
//        } else {
        UnleashedCustomer::where('guid', $business->guid)
            ->update([
                'phone_number' => $business_phone,
                'fax_number' => $fax_number,
                'channel_id' => PrimaryChannel::where('name', $primary_channel)->first()->id,
                'secondary_channel_id' => SecondaryChannel::where('name', $second_channel)->first()->id,
                'trading_name' => $request->get('business_name'),
//                    'status' => 1,
            ]);
        $person = User::where('id',Auth::guard('practitioner')->user()->id)->first();

        if($business->status == '0'){
            $application = WebsiteApplication::create([
                'type'                 => 'Account Upgrade From Lead Application',
                'customer_code'        => $business->guid,
                'status'               => 0,
                'pre_status'           => 0,
                'salutation'           => $person->salutation,
                'first_name'           => $person->first_name,
                'last_name'            => $person->last_name,
                'email'                => $person->email,
                'password'             => $person->password,
                'phone'                => $person->phone,
                'mobile'               => $person->mobile,
                'job_title'            => $person->job_title,
                'association_number'   => $person->association_number,
                'association_name'     => $person->association_id,
                'fax'                  => $business->fax_number,
                'business_name'        => $business->customer_name,
                'business_phone'       => $business->phone_number,
                'primary_channel_id'   => $business->channel_id,
                'second_channel_id'    => $business->secondary_channel_id,
                'business_type'        => $business->business_type,
                'business_number'      => $business->gstvat_number,
                'acn'                  => $business->acn,
                'need_credit'          => $business->need_credit,
                'is_business_owner'    => $business->is_business_owner,
                'is_declared_bankrupt' => $business->is_declared_bankrupt,
                'is_refused_credit'    => $business->is_refused_credit,
                'ahpra_number'         => $person->ahpra_number,
                'list_in_web'          => $business->list_in_web,
                'trading_name'         => $business->trading_name,
                'is_certified'         => $business->is_certified,
                'classification'       => $business->classification_id,
                'modality_id'          => $person->modalityperson()->first()->id,
                'request_from'         => 'vms',
                'qualification'        => $person->qualification,
            ]);
            $sendTo = config('app.website_register_email');
            $headers = 'Account Upgrade From Lead Application';
            $time = "This email was submitted on " . date('m-d-Y');
            $beautymail = app()->make(Beautymail::class);
            $name = '';
            $url = config('app.5i_url') . "/nova/resources/website-applications/" . $application->id;
            $beautymail->send('emails.email-template-upgrade-application', ['time' => $time, 'application' => $application, 'headers' => $headers, 'name' => $name, 'url' => $url], function ($message) use ($sendTo, $name) {
                $message->to($sendTo)->subject('Account Upgrade From Lead Application');
            });
        }

        return redirect()->back();
    }




    public function updateReferralCode(Request $request)
    {

        $referral_code = $request->get('current_referral_code');
        $new_referral_code = $request->get('new_referral_code');
//        dd($new_referral_code);
//        $guid = UnleashedCustomer::where('referral_code', $request->get('referral_code'))->first()->guid;

        UnleashedCustomer::where('referral_code', $referral_code)
            ->update([
                'referral_code' => $new_referral_code,

            ]);

        return response()->json(['msg' => 'success']);

    }

    // Change Password

    public function getCurrentPassword()
    {

        try {
            if (Auth::guard('patient')->check()) {
                $role = Patient::where('id', Auth::guard('patient')->user()->id)->first();
//                (dd($role));
                $password = $role->password;

                return view('vms/pages/userInfo/patient-password-page', compact('password', 'role'));
            } else {
                $role = User::where('id', Auth::guard('practitioner')->user()->id)->first();
                $password = $role->password;
                return view('vms/pages/userInfo/practitioner-password-page', compact('password','role'));
            }

        } catch (\Exception $e) {
            return redirect('/');
        }
    }

    public function updatePatientPassword(Request $request){

        $current_password = $request->get('current_password');
        $password = $request->get('new-password');
        if(!Hash::check($current_password,Auth::guard('patient')->user()->password)){
            return response()->json(['msg' => 'wrong password']);
        }else{
            if ($request->has('current_password')) {
                Patient::where('id',Auth::guard('patient')->user()->id)
                    ->update([
                        'password' => Hash::make($password),
                    ]);
                return response()->json(['msg' => 'success']);
            }
            else{

                return response()->json(['msg' => 'error']);
            }
        }

    }

    public function updatePractitionerPassword(Request $request){

//        $current_password = Hash::make($request->get('current_password'));
        if(!Hash::check($request->get('current_password'),Auth::guard('practitioner')->user()->password)){
            return response()->json(['msg' => 'wrong password']);
        }else{
            $password= $request->get('new_password');
            User::where('id', Auth::guard('practitioner')->user()->id)
                ->update([
                    'password' => Hash::make($password),
                ]);
            return response()->json(['msg' => 'success']);
        }


    }
    public function getMarketing(Request $request)
    {
        $notification =  User::where('id', Auth::guard('practitioner')->user()->id)->first()->email_notification;

        return view('vms.pages.userInfo.practitioner-marketing',compact('notification'));

    }
    public function updateMarketing(Request $request)
    {
        if($request->has('notification')){
            User::where('id', Auth::guard('practitioner')->user()->id)
                ->update([
                    'email_notification' => $request->get('notification'),
                ]);
        }
        return response()->json(['status'=>'success','msg'=>'Update Successfully']);

    }
}















