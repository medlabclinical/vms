<?php

namespace App\Http\Controllers;

use App\Models\ClinicalTrials;
use App\Models\ClinicalTrialType;
use App\Models\MediaLibrary;
use App\Models\Members;
use App\Models\MemberType;
use App\Models\Pages;
use App\Models\ProductFamilyModel;
use App\Models\ProductMultipleRelation;
use App\Models\Publications;
use App\Models\User;
use http\Exception;
use Illuminate\Http\Request;
use Snowfire\Beautymail\Beautymail;
use Auth;


class WWWPageController extends Controller
{
    public function getPageInfo(string $page)
    {
        $content = Pages::where('page', $page)->where('website', 'medlab')->first();
        $content->title_img = $content->title_img ? MediaLibrary::where('id', $content->title_img)->first()->name : NULL;
        $content->second_img = $content->second_img ? MediaLibrary::where('id', $content->second_img)->first()->name : NULL;
        $content->third_img = $content->third_img ? MediaLibrary::where('id', $content->third_img)->first()->name : NULL;

        return $content;
    }

    public function getAboutCategory()
    {
        return Pages::whereIn('page', ['about', 'contact', 'commercial_partners', 'collaborations', 'industry_associations', 'partner opportunities', 'leadership team', 'consulting_team'])->get();
    }

    public function getPartneringCategory()
    {
        return Pages::whereIn('page', ['commercial_partners', 'collaborations', 'industry_associations', 'partner opportunities'])->get();
    }

    public function getResearchCategory()
    {
        return Pages::whereIn('page', ['pc2', 'nanocelle technology', 'publications', 'clinical trials'])->get();
    }

    public function getPipeline()
    {
        $image_url = config("app.image_url");
        $content = $this->getPageInfo('pipeline');
        return view('www/pages/products/pipeline', compact('content', 'image_url'));
    }

    public function getInDevelopment()
    {
        $image_url = config("app.image_url");
        $content = $this->getPageInfo('in development');
        $products = ProductInDev::all();
        return view('in_development', compact('content', 'image_url', 'products'));
    }

    public function getDevDetail($id)
    {
        $content = ProductInDev::where('id', $id)->first();
        $content->login_content = json_decode($content->login_content);
        $content->guest_content = json_decode($content->guest_content);
        return view('dev_detail', compact('content'));
    }

    public function getNanocelle()
    {
        $image_url = config("app.image_url");
        $content = $this->getPageInfo('nanocelle technology');
        $research_list = $this->getResearchCategory();
        return view('www/pages/research/nanocelle', compact('content', 'image_url', 'research_list'));
    }

    public function getPC2()
    {
        $image_url = config("app.image_url");
        $content = $this->getPageInfo('pc2');
        $research_list = $this->getResearchCategory();
        return view('www/pages/research/pc2', compact('content', 'image_url', 'research_list'));
    }

    public function getAbout()
    {
        $image_url = config("app.image_url");
        $content = $this->getPageInfo('about');
        $about_list = $this->getAboutCategory();
        return view('www/pages/about/about', compact('content', 'image_url', 'about_list'));
    }

    public function getPartnerOpportunities()
    {
        $image_url = config("app.image_url");
        $content = $this->getPageInfo('partner opportunities');
        $about_list = $this->getAboutCategory();
        return view('www/pages/about/partner_opportunities', compact('content', 'image_url', 'about_list'));
    }
    //Changing Referral Code Page

    public function getReferralCode()
    {
        $image_url = config("app.image_url");
        $content = $this->getPageInfo('change referral code');
        if (Auth::guard('practitioner')->check()) {
            $role = User::where('id', Auth::guard('practitioner')->user()->id)->first();
            return view('www/pages/userInfo/change-referral-code', compact('role','content','image_url'));
        }
        else{
            return redirect(404);
        }

    }

    public function store()
    {

//        $this->send();
        $send_to = "partnering@medlab.co";
        $send_subject = "Ajax form ";


        /*Be careful when editing below this line */

        $f_fname = $_POST["fname"];
        $f_lname = $_POST["lname"];
        $f_email = $_POST["email"];
        $f_phone = $_POST["phone"];
        $f_company = $_POST["company"];
        $f_country = $_POST["country"];
        $f_message = $_POST["message"];
        $from_ip = $_SERVER['REMOTE_ADDR'];
        $from_browser = $_SERVER['HTTP_USER_AGENT'];

        $time = "This email was submitted on " . date('m-d-Y');
        $name = "Name: " . $f_fname . " " . $f_lname;
        $email = "Email: " . $f_email;
        $phone = "Phone: " . $f_phone;
        $company = "Company: " . $f_company;
        $country = "Country: " . $f_country;
        $messages = "Message: " . $f_message;
        $details = "Technical Details: " . $from_ip . "\n" . $from_browser;

//        $messages = "This email was submitted on " . date('m-d-Y') .
//            "\n\nName: " . $f_name .
//            "\n\nE-Mail: " . $f_email .
//            "\n\nMessage: \n" . $f_message .
//            "\n\n\nTechnical Details:\n" . $from_ip . "\n" . $from_browser;

//        $send_subject .= " - {$f_name}";

        $headers = "Reply-To: " . $f_email;
//        $headers = "From: " . $f_email . "\r\n" .
//            "Reply-To: " . $f_email . "\r\n" .
//            "X-Mailer: PHP/" . phpversion();


        if (!$f_email) {
            return response()->json('Please input an email', 401);
        } else if (!$f_fname) {
            return response()->json('Please input your first name', 401);
        } else if (!$f_lname) {
            return response()->json('Please input your last name', 401);
        } else if (!$f_phone) {
            return response()->json('Please input your phone', 401);
        } else if (!$f_company) {
            return response()->json('Please input your company name', 401);
        } else if (!$f_country) {
            return response()->json('Please input your country', 401);
        } else if (!$f_message) {
            return response()->json('Please input your message', 401);
        } else {

            if (filter_var($f_email, FILTER_VALIDATE_EMAIL)) {
                $this->send($send_to, $headers, $time, $name, $email, $phone, $company, $country, $messages, $details);

//                dd('1');
                return response()->json(['success' => 'Thanks for contacting us! We will be in touch with you shortly.']);
            } else {
                return response()->json('Invalid email, please check the format!', 401);
            }
        }
    }


    public function send($send_to, $headers, $time, $name, $email, $phone, $company, $country, $messages, $details)
    {
//        Mail::send('email-template', ['time' => $time, 'messages' => $messages, 'headers' => $headers, 'name' => $name, 'details' => $details], function ($message) use ($send_to) {
//            $to = $send_to;
//            $message->to($to)->subject('Contact email from');
//        });
        $beautymail = app()->make(Beautymail::class);
        $beautymail->send('emails.email-partnering', ['time' => $time, 'messages' => $messages, 'headers' => $headers, 'name' => $name, 'email' => $email, 'phone' => $phone, 'company' => $company, 'country' => $country, 'details' => $details], function ($message) use ($send_to, $name) {
            $message->to($send_to)->subject('Partnering Request From');
        });

    }


    public function getPublications()
    {
        $image_url = config("app.image_url");
        $content = $this->getPageInfo('publications');
        $data_1 = Publications::where('type', 'publication')->where('deleted_at', null)->orderBy('date', 'desc')->get();
//        dd($data_1);
        $data_2 = Publications::where('type', 'book')->where('deleted_at', null)->orderBy('date', 'desc')->get();
        $data_3 = Publications::where('type', 'conference')->where('deleted_at', null)->orderBy('date', 'desc')->get();
//        $list = collect();
//        foreach ($data as $item){
//            if(in_array('3',array_column(ProductMultipleRelation::where('publication_id',$item->id)->get()->toArray(),'brand_id'))){
//                $list->add($item);
//            }
//        }
//        $data = $list;
        $publication = [];
        $book = [];
        $conference = [];
        foreach ($data_1 as $item) {
            $item->member = $item->member()->get()->toArray();
            $item->product_family = $item->product_family()->get()->toArray();
            $item->clinical_trial = $item->clinical_trial()->get()->toArray();
            $item->product_ingredient = $item->product_ingredient()->get()->toArray();
            $item->article = $item->article()->get()->toArray();
            $item->product_group = $item->product_group()->get()->toArray();
            $item->category = $item->product_group()->get()->toArray();
            $publication[substr($item['date'], 0, 4)][] = $item;
        }
        foreach ($data_2 as $item) {
            $item->member = $item->member()->get()->toArray();
            $item->product_family = $item->product_family()->get()->toArray();
            $item->clinical_trial = $item->clinical_trial()->get()->toArray();
            $item->product_ingredient = $item->product_ingredient()->get()->toArray();
            $item->article = $item->article()->get()->toArray();
            $item->product_group = $item->product_group()->get()->toArray();
            $book[substr($item['date'], 0, 4)][] = $item;
        }
        foreach ($data_3 as $item) {
            $item->member = $item->member()->get()->toArray();
            $item->product_family = $item->product_family()->get()->toArray();
            $item->clinical_trial = $item->clinical_trial()->get()->toArray();
            $item->product_ingredient = $item->product_ingredient()->get()->toArray();
            $item->article = $item->article()->get()->toArray();
            $item->product_group = $item->product_group()->get()->toArray();
            $conference[substr($item['date'], 0, 4)][] = $item;
        }
        $research_list = $this->getResearchCategory();
        return view('www/pages/research/publications', compact('content', 'image_url', 'publication', 'book', 'conference', 'research_list'));
    }

    public function getFilter($data)
    {
        $list = collect();
        foreach ($data as $item) {
            if (in_array('3', array_column(ProductMultipleRelation::where('clinical_trial_id', $item->id)->get()->toArray(), 'brand_id'))) {
                $list->add($item);
            }
        }
        return $list;
    }

    public function getPrivacyPolicy()
    {
        $image_url = config("app.image_url");
        $content = $this->getPageInfo('privacy policy');
        return view('www/pages/policies/privacy_policy', compact('content', 'image_url'));
    }

    public function getShippingAndDelivery()
    {
        $image_url = config("app.image_url");
        $content = $this->getPageInfo('shipping and delivery');
        return view('www/pages/policies/shipping_and_delivery', compact('content', 'image_url'));
    }

    public function getReturns()
    {
        $image_url = config("app.image_url");
        $content = $this->getPageInfo('returns');
        return view('www/pages/policies/returns', compact('content', 'image_url'));
    }

    public function getSalesPolicy()
    {
        $image_url = config("app.image_url");
        $content = $this->getPageInfo('sales policy');
        return view('www/pages/policies/sales_policy', compact('content', 'image_url'));
    }


    public function getClinicalTrials()
    {

        $image_url = config("app.image_url");
        $content = $this->getPageInfo('clinical trials');
        $clinicalTrialList = ClinicalTrials::where('publish', '!=', '0')->whereNull('deleted_at')->orderBy('id', 'desc')->get();
        $typeFilter = ClinicalTrialType::whereNull('deleted_at')->orderBy('id', 'desc')->get();

        //$cannabisList = ClinicalTrials::where('type','Cannabis')->where('publish', '!=', '0')->whereNull('deleted_at')->orderBy('id','desc')->get();
        //$deliveryPlatformList = ClinicalTrials::where('type','Delivery Platform')->where('publish', '!=', '0')->whereNull('deleted_at')->orderBy('id','desc')->get();
        //$depressionList = ClinicalTrials::where('type','Depression')->where('publish', '!=', '0')->whereNull('deleted_at')->orderBy('id','desc')->get();
        //$mucositisList = ClinicalTrials::where('type','Mucositis')->where('publish', '!=', '0')->whereNull('deleted_at')->orderBy('id','desc')->get();
        //$diabetesList = ClinicalTrials::where('type','Diabetes')->where('publish', '!=', '0')->whereNull('deleted_at')->orderBy('id','desc')->get();
        //$partiesList = ClinicalTrials::where('type','Contact for 3rd Parties')->where('publish', '!=', '0')->whereNull('deleted_at')->orderBy('id','desc')->get();
        //$oncologyList = ClinicalTrials::where('type','Oncology')->where('publish', '!=', '0')->whereNull('deleted_at')->orderBy('id','desc')->get();

        $research_list = $this->getResearchCategory();
        return view('www/pages/research/clinical_trials', compact('content', 'image_url', 'clinicalTrialList', 'typeFilter', 'research_list'));
    }

    public function getClinicalTrialsPage($id)
    {

        $id = substr($id, strrpos($id, "-") + 1);
        $image_url = config("app.image_url");
        try {
            $clinicaltrial = ClinicalTrials::where('id', $id)->where('publish', '!=', '0')->first();
        } catch (\Exception $ex) {
            abort(404);
        }

        $clinicaltrial->image = $clinicaltrial->image ? MediaLibrary::where('id', $clinicaltrial->image)->first()->name : NULL;

        $member = $clinicaltrial->member()->get()->toArray();
        if (count($member)) {
            for ($i = 0; $i < count($member); $i++) {
                if ($member[$i]['picture']) {
                    $picture = MediaLibrary::where('id', $member[$i]['picture'])->first()->name;
                    $member[$i]['picture'] = $picture;

                }
            }
        }
        $partner = $clinicaltrial->partner()->get()->toArray();
        if (count($partner)) {
            for ($i = 0; $i < count($partner); $i++) {
                if ($partner[$i]['picture']) {
                    $picture = MediaLibrary::where('id', $partner[$i]['picture'])->first()->name;
                    $partner[$i]['picture'] = $picture;

                }
            }
        }

        $article = $clinicaltrial->article()->get()->toArray();
        if (count($article)) {
            for ($i = 0; $i < count($article); $i++) {
                if ($article[$i]['title_img']) {
                    $picture = MediaLibrary::where('id', $article[$i]['title_img'])->first()->name;
                    $article[$i]['title_img'] = $picture;
                }
            }
        }

        $clinicaltrial->partner = $partner;
        $clinicaltrial->member = $member;
        $clinicaltrial->article = $article;

        $clinicaltrial->publicationsandpresentation = $clinicaltrial->publicationsandpresentation()->get()->toArray();
        $clinicaltrial->product_family = $clinicaltrial->product_family()->get()->toArray();
        $clinicaltrial->product_ingredient = $clinicaltrial->product_ingredient()->get()->toArray();
        $clinicaltrial->product_group = $clinicaltrial->product_group()->get()->toArray();


        $research_list = $this->getResearchCategory();
        return view('www/pages/research/clinical_trials_page', compact('clinicaltrial', 'image_url', 'research_list'));
    }


    public function getProductsInDev()
    {

        $image_url = config("app.image_url");
        $content = $this->getPageInfo('products in development');
        $productsInDev = ProductFamilyModel::where('in_development', '=', 1)->whereNull('deleted_at')->orderBy('id', 'desc')->get();


        return view('www/pages/products/products_in_development_page', compact('content', 'image_url', 'productsInDev'));
    }


}
