<?php

namespace App\Http\Controllers;

use App\Models\CustomerBrandModel;
use App\Models\ModalityPerson;
use App\Models\Patient;
use App\Models\SubscriptionLogModel;
use App\Models\SubscriptionModel;
use App\Models\SubscriptionTypeModel;
use App\Models\UnleashedCustomer;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Spatie\Newsletter\Newsletter;

class SubscriptionController extends Controller
{
    public function index()
    {
        return response('Invalid request', 400)->header('Content-Type', 'text/plain');
    }

    public function create()
    {
        return response('Tag your it!', 200)->header('Content-Type', 'text/plain');
    }

    public static function verifyIp($ip)
    {
        Log::info('verifying your ip address ' . $ip);

        return true;

        $cidrs[] = "205.201.128.0/20";
        $cidrs[] = "198.2.128.0/18";
        $cidrs[] = "148.105.0.0/16";
        $cidrs[] = "192.168.0.1/32";
        $cidrs[] = "127.0.0.1/32";

        foreach ($cidrs as $cidr)
        {
            list($subnet, $mask) = explode('/', $cidr);

            if ((ip2long($ip) & ~((1 << (32 - $mask)) - 1) ) == ip2long($subnet)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Action Patient
     * - manipulates the calls from the mailchimp webook for patients
     * - this only updates the subscribe/unsubscribe status
     */
    public function wh_action_patient(Request $request)
    {
        $result = static::verifyIp($request->ip());

        if (!$result) {
            return response('Restricted Web Space for ' . $request->ip(), 400)->header('Content-Type', 'text/plain');
        }

        $request = $request->all();
        $data = $request["data"];

        // Log::info('chanelling your patient chakra ' . serialize($data));

        // set the ids
        $subscription_type_detail = SubscriptionTypeModel::where('name', 'Vitamins and Supplements')->first();
        $is_vms_type = $subscription_type_detail->id;

        $subscription_type_detail = SubscriptionTypeModel::where('name', 'Pharmaceutical')->first();
        $is_pharma_type = $subscription_type_detail->id;

        if (isset($data["action"]) && $data['action'] == 'unsub')
        {
            $email      = $data["email"];
            $patient_id = $data["merges"]["PATIENTID"];

            // set the action to unsub
            SubscriptionModel::where('person_id', $patient_id)
                    ->where('email', $email)
                    ->where('user_type', 'patient')
                    ->update(['action' => 'unsub']);

        }
        else if (isset($data["email"]) && isset($data["merges"]["PATIENTID"]))
        {
            $email         = $data["email"];
            $patient_id    = $data["merges"]["PATIENTID"];
            $patient_fname = $data["merges"]["FNAME"];
            $patient_lname = $data["merges"]["LNAME"];

            // set default to unsubscribed
            $is_pharma = $is_vms = 0;

            // this will be need to be split by commas
            // Vitamins and Supplements, Pharmaceutical
            // Vitamins and Supplements
            $interests  = $data["merges"]["INTERESTS"];
            $split_interest = explode(", ", $interests);
            foreach ($split_interest as $interest) {
                if ($interest == "Vitamins and Supplements") { $is_pharma = 1; }
                if ($interest == "Pharmaceutical") { $is_vms = 1; }
            }

            SubscriptionLogModel::create([
                'direction' => 'inbound-patient',
                'email' => $email,
                'log' => serialize($data)
            ]);

            // $patient_detail = Patient::where('id', $patient_id)->where('email', $email)->first();

            // // email notify?
            // if (!$patient_detail) {
            //     return false;
            // }

            Patient::where('id', $patient_id)
                  ->update(['first_name' => $patient_fname, 'last_name' => $patient_lname]);


            /**** VMS PRAC ****/
            if ($is_vms>=1) {
                // if this is trashed
                SubscriptionModel::withTrashed()
                        ->where('person_id', $patient_id)
                        ->where('type_id', $is_vms_type)
                        ->where('email', $email)
                        ->where('user_type', 'patient')
                        ->restore();

                // subscribe pick the first business
                SubscriptionModel::updateOrCreate(
                    ['updated_at' => date("Y-m-d H:i:s")],
                    ['person_id' => $patient_id, 'type_id' => $is_vms_type, 'email' => $email, 'user_type' => "patient"]
                );
            } else {
                // unsubscribed from all
                SubscriptionModel::where('person_id', $patient_id)
                      ->where('type_id', $is_vms_type)
                      ->where('email', $email)
                      ->where('user_type', 'patient')
                      ->delete();
            }

            /**** PHARMA PRAC ****/
            if ($is_pharma>=1) {
                // if this is trashed
                SubscriptionModel::withTrashed()
                        ->where('person_id', $patient_id)
                        ->where('type_id', $is_pharma_type)
                        ->where('email', $email)
                        ->where('user_type', 'patient')
                        ->restore();

                // subscribe pick the first business
                SubscriptionModel::updateOrCreate(
                    ['updated_at' => date("Y-m-d H:i:s")],
                    ['person_id' => $patient_id, 'type_id' => $is_pharma_type, 'email' => $email, 'user_type' => "patient"]
                );
            } else {
                // unsubscribed from all
                SubscriptionModel::where('person_id', $patient_id)
                      ->where('type_id', $is_pharma_type)
                      ->where('email', $email)
                      ->where('user_type', 'patient')
                      ->delete();
            }

            return response('Success', 200);
        }
        else
        {
            return response('Invalid request', 400)->header('Content-Type', 'text/plain');
        }
    }

    /**
     * Action Person/Practitioner
     * - manipulates the calls from the mailchimp webook for person / practitioner
     * - this only updates the subscribe/unsubscribe status
     */
    public function wh_action(Request $request)
    {
        $result = static::verifyIp($request->ip());

        if (!$result) {
            return response('Restricted Web Space for ' . $request->ip(), 400)->header('Content-Type', 'text/plain');
        }

        $request = $request->all();
        $data = $request["data"];

        // Log::info('chanelling your chakra ' . serialize($data));

        // set the ids for each interest
        $subscription_type_detail = SubscriptionTypeModel::where('name', 'Vitamins and Supplements Newsletter')->first();
        $is_vms_type = $subscription_type_detail->id;

        $subscription_type_detail = SubscriptionTypeModel::where('name', 'Pharmaceutical Newsletter')->first();
        $is_pharma_type = $subscription_type_detail->id;

        $subscription_type_detail = SubscriptionTypeModel::where('name', 'Deals')->first();
        $is_deals_type = $subscription_type_detail->id;

        if (isset($data["action"]) && $data['action'] == 'unsub')
        {
            $email         = $data["email"];
            $person_id     = $data["merges"]["PERSONID"];

            // set the action to unsub
            SubscriptionModel::where('person_id', $person_id)
                    ->where('email', $email)
                    ->where('user_type', 'person')
                    ->update(['action' => 'unsub']);
        }
        else if (isset($data["email"]) && isset($data["merges"]["PERSONID"]))
        {
            $email         = $data["email"];
            $person_fname  = $data["merges"]["FNAME"];
            $person_lname  = $data["merges"]["LNAME"];
            $person_id     = $data["merges"]["PERSONID"];
            $customer_code = $data["merges"]["BUSINESSID"];

            // set default to be unsubscribed
            $is_deals = $is_pharma = $is_vms = 0;

            // this will be need to be split by commas
            // Vitamins and Supplements Newsletter, Pharmaceutical Newsletter, Deals
            // Vitamins and Supplements Newsletter, Deals
            $interests  = $data["merges"]["INTERESTS"];
            $split_interest = explode(", ", $interests);
            foreach ($split_interest as $interest)
            {
                if ($interest == "Vitamins and Supplements Newsletter") { $is_vms = 1; }
                if ($interest == "Pharmaceutical Newsletter") { $is_pharma = 1; }
                if ($interest == "Deals") { $is_deals = 1; }
            }

            User::where('id', $person_id)
                  ->update(['first_name' => $person_fname, 'last_name' => $person_lname]);

            SubscriptionLogModel::create([
                'direction' => 'inbound',
                'email' => $email,
                'log' => serialize($data)
            ]);

            /**** DEALS PRAC ****/
            if ($is_deals>=1) {

                // if this is trashed
                SubscriptionModel::withTrashed()
                        ->where('person_id', $person_id)
                        ->where('type_id', $is_deals_type)
                        ->where('email', $email)
                        ->where('user_type', 'person')
                        ->restore();

                // subscribe pick the first business
                SubscriptionModel::updateOrCreate(
                    ['updated_at' => date("Y-m-d H:i:s"), 'action' => NULL],
                    ['person_id' => $person_id, 'type_id' => $is_deals_type, 'email' => $email, 'user_type' => "person", 'customer_code' => $customer_code]
                );
            } else {
                // unsubscribed from all
                SubscriptionModel::where('person_id', $person_id)
                      ->where('type_id', $is_deals_type)
                      ->where('email', $email)
                      ->where('user_type', 'person')
                      ->delete();
            }

            /**** VMS PRAC ****/
            if ($is_vms>=1) {
                // if this is trashed
                SubscriptionModel::withTrashed()
                        ->where('person_id', $person_id)
                        ->where('type_id', $is_vms_type)
                        ->where('email', $email)
                        ->where('user_type', 'person')
                        ->restore();

                // subscribe pick the first business
                SubscriptionModel::updateOrCreate(
                    ['updated_at' => date("Y-m-d H:i:s"), 'action' => NULL],
                    ['person_id' => $person_id, 'type_id' => $is_vms_type, 'email' => $email, 'user_type' => "person", 'customer_code' => $customer_code]
                );
            } else {
                // unsubscribed from all
                SubscriptionModel::where('person_id', $person_id)
                      ->where('type_id', $is_vms_type)
                      ->where('email', $email)
                      ->where('user_type', 'person')
                      ->delete();
            }

            /**** PHARMA PRAC ****/
            if ($is_pharma>=1) {
                // if this is trashed
                SubscriptionModel::withTrashed()
                        ->where('person_id', $person_id)
                        ->where('type_id', $is_pharma_type)
                        ->where('email', $email)
                        ->where('user_type', 'person')
                        ->restore();

                // subscribe pick the first business
                SubscriptionModel::updateOrCreate(
                    ['updated_at' => date("Y-m-d H:i:s"), 'action' => NULL],
                    ['person_id' => $person_id, 'type_id' => $is_pharma_type, 'email' => $email, 'user_type' => "person", 'customer_code' => $customer_code]
                );
            } else {
                // unsubscribed from all
                SubscriptionModel::where('person_id', $person_id)
                      ->where('type_id', $is_pharma_type)
                      ->where('email', $email)
                      ->where('user_type', 'person')
                      ->delete();
            }
        }
        else
        {
            return response('Invalid request', 400)->header('Content-Type', 'text/plain');
        }
    }

    /**
     * Sends subscription/unsubscription to mailchimp for person
     * - patients are called via similar method below
     */
    public static function subscribe(Request $request)
    {
        $inp_email        = $request->input('email');
        $inp_interestid   = $request->input('interestid');
        $inp_personid     = $request->input('personid');
        $inp_issubscribed = $request->input('issubscribed');
        $inp_person_type  = $request->input('person_type');
        $inp_request      = serialize($request->all());

        $data = static::invoke_subscribe($inp_email, $inp_interestid, $inp_personid, $inp_issubscribed, $inp_person_type, $inp_request);

        return json_encode($data);
    }

    /**
     * Actual submission to mailchimp
     *  -- interest id is defaulted to deals for now
     */
    public static function invoke_subscribe($email, $interestid, $personid, $issubscribed, $person_type, $form_request, $is_new=false, $customer_guid='')
    {
        $listname     = config('app.mailchimp_list_name');

        SubscriptionLogModel::create([
            'direction' => 'outbound',
            'email' => $email,
            'log' => $form_request
        ]);

        $person_detail   = User::where('id', $personid)->firstOrFail();
        $person_modality = array();
        $tmp = ModalityPerson::where('person_id', $personid)
                            ->join('modalities', 'modalities.id', '=', 'modality_persons.modality_id')
                            ->get()
                            ->toArray();
        foreach ($tmp as $item) {
            $person_modality[] = $item["name"];
        }

        $list_name = SubscriptionTypeModel::where('interest_id', $interestid)->firstOrFail();

        if ($is_new==true) {
            $business_detail = UnleashedCustomer::getSubscribeBusiness($customer_guid)->firstOrFail();
        } else {
            $business_detail = UnleashedCustomer::getFirstBusiness($personid)->firstOrFail();
        }

        $tmp = CustomerBrandModel::where('customer_id', $business_detail->guid)
                            ->join('brand', 'brand.id', '=', 'customer_brands.brand_id')
                            ->get()
                            ->toArray();
        $business_brands = array();
        foreach ($tmp as $item) {
            $business_brands[] = $item["name"];
        }

        if (!$person_detail) {
            $response["message"] = 'Invalid person or subscription list, please refresh your page';
            $response["status"] = 'warning';
            return $response;
        }

        $profile["FNAME"]      = $person_detail->first_name;
        $profile["LNAME"]      = $person_detail->last_name;
        $profile["PERSONID"]   = $person_detail->id;
        $profile["AHPRA"]      = (is_null($person_detail->ahpra_number) || $person_detail->ahpra_number == 'NULL') ? '' : $person_detail->ahpra_number;
        $profile["MODALITY"]   = implode(",", $person_modality);

        $profile["BUSNAME"]    = $business_detail->customer_name;
        $profile["CHANNEL"]    = $business_detail->channel_parents_name . ' > ' . $business_detail->channels_name;
        $profile["CLASSIFY"]   = $business_detail->primary_classifications_name . ' > ' . $business_detail->secondary_classifications_name;

        $profile["BRANDS"]     = implode(",", $business_brands);
        $profile["VMSTIER"]    = $business_detail->tier;
        $profile["PHARMATIER"] = $business_detail->drug_tier;
        $profile["VMSSALE"]    = date("d-m-Y", strtotime($business_detail->date_last_purchase));
        $profile["PHARMASALE"] = date("d-m-Y", strtotime($business_detail->drug_date_last_purchase));

        $profile["BUSINESSID"] = $business_detail->customer_code;

        if ($issubscribed == 'false')
        {
            // Subscribing
            $interests['interests'][$interestid] = true;

            // New client we are subscribing them to the other newsletters vms / pharma
            if ($is_new==true)
            {
                foreach ($business_brands as $item) {
                    if ($item == "VMS") {
                        $interests['interests']['605d7525e4'] = true;
                    } else if ($item == "Pharma") {
                        $interests['interests']['78d28c19ed'] = true;
                    }
                }
            }

            $successful = \Spatie\Newsletter\NewsletterFacade::subscribeOrUpdate($email, $profile, $listname, $interests);
            if (!$successful) {
                $last_error = \Spatie\Newsletter\NewsletterFacade::getLastError();
                $response["message"] = $last_error;
                $response["status"] = 'warning';
            } else {
                $response["message"] = 'Successfully subscribed ' . $person_detail->email . ' to ' . $list_name->name;
                $response["status"] = 'success';
            }

            // if this is trashed
            SubscriptionModel::withTrashed()
                    ->where('person_id', $person_detail->id)
                    ->where('type_id', $list_name->id)
                    ->where('email', $email)
                    ->where('user_type', $person_type)
                    ->restore();

            // re-update
            SubscriptionModel::updateOrCreate(
                ['updated_at' => date("Y-m-d H:i:s"), 'type_id' => $list_name->id, 'email' => $email, 'user_type' => $person_type, 'person_id' => $person_detail->id],
                ['customer_code' => $business_detail->customer_code]
            );

            // New client we are subscribing them to the other newsletters vms / pharma
            if ($is_new==true)
            {
                foreach ($business_brands as $item) {
                    if ($item == "VMS") {
                        // re-update
                        SubscriptionModel::updateOrCreate(
                            ['updated_at' => date("Y-m-d H:i:s"), 'type_id' => 2, 'email' => $email, 'user_type' => $person_type, 'person_id' => $person_detail->id],
                            ['customer_code' => $business_detail->customer_code]
                        );
                    } else if ($item == "Pharma") {
                        // re-update
                        SubscriptionModel::updateOrCreate(
                            ['updated_at' => date("Y-m-d H:i:s"), 'type_id' => 3, 'email' => $email, 'user_type' => $person_type, 'person_id' => $person_detail->id],
                            ['customer_code' => $business_detail->customer_code]
                        );
                    }
                }
            }
        }
        else if ($issubscribed == 'true')
        {
            // Unsubscribing
            $interests['interests'][$interestid] = false;
            $successful = \Spatie\Newsletter\NewsletterFacade::subscribeOrUpdate($email, $profile, $listname, $interests);
            if (!$successful) {
                $last_error = \Spatie\Newsletter\NewsletterFacade::getLastError();
                $response["message"] = $last_error;
                $response["status"] = 'warning';
            } else {
                $response["message"] = 'Unsubscribed ' . $person_detail->email . ' from ' . $list_name->name;
                $response["status"] = 'success';
            }

            SubscriptionModel::where('person_id', $person_detail->id)
                    ->where('type_id', $list_name->id)
                    ->where('email', $email)
                    ->where('user_type', $person_type)
                    ->delete();
        }
        else
        {
            $response["message"] = 'Invalid request submitted';
            $response["status"] = 'warning';
        }

        return $response;
    }

    /**
     * Sends subscription/unsubscription to mailchimp for patients
     * - patients are called via similar method below
     */
    public static function subscribe_patient(Request $request)
    {
        $listname     = config('app.mailchimp_patient_list_name');

        $email        = $request->input('email');
        $interestid   = $request->input('interestid');
        $personid     = $request->input('personid');
        $issubscribed = $request->input('issubscribed');
        $person_type  = $request->input('person_type');

        SubscriptionLogModel::create([
            'direction' => 'outbound-patient',
            'email' => $email,
            'log' => serialize($request->all())
        ]);

        $person_detail = Patient::where('id', $personid)->where('email', $email)->firstOrFail();
        $list_name     = SubscriptionTypeModel::where('interest_id', $interestid)->firstOrFail();

        if (!$person_detail || !$list_name) {
            $response["message"] = 'Invalid person or subscription list, please refresh your page';
            $response["status"] = 'warning';
            return $response;
        }

        $profile["FNAME"]     = $person_detail->first_name;
        $profile["LNAME"]     = $person_detail->last_name;
        $profile["PATIENTID"] = $person_detail->id;

        if ($issubscribed == 'false')
        {
            // Subscribing
            $interests['interests'][$interestid] = true;
            $successful = \Spatie\Newsletter\NewsletterFacade::subscribeOrUpdate($email, $profile, $listname, $interests);
            $response["message"] = 'Successfully subscribed ' . $person_detail->email . ' to ' . $list_name->name;
            $response["status"] = 'success';

            // if this is trashed
            SubscriptionModel::withTrashed()
                    ->where('person_id', $person_detail->id)
                    ->where('type_id', $list_name->id)
                    ->where('email', $email)
                    ->where('user_type', $person_type)
                    ->restore();

            // re-update patient
            SubscriptionModel::updateOrCreate(
                ['updated_at' => date("Y-m-d H:i:s")],
                ['person_id' => $person_detail->id, 'type_id' => $list_name->id, 'email' => $email, 'user_type' => $person_type]
            );
        }
        else
        {
            $interests['interests'][$interestid] = false;
            $successful = \Spatie\Newsletter\NewsletterFacade::subscribeOrUpdate($email, $profile, $listname, $interests);
            $response["message"] = 'Unsubscribed ' . $person_detail->email . ' from ' . $list_name->name;
            $response["status"] = 'success';

            SubscriptionModel::where('person_id', $person_detail->id)
                    ->where('type_id', $list_name->id)
                    ->where('email', $email)
                    ->where('user_type', $person_type)
                    ->delete();
        }

        if (!$successful) {
            $response["message"] = 'Gateway for Mailchimp is down. Please try again later';
            $response["status"] = 'warning';
        }

        return json_encode($response);
    }

    /**
     * Please refer to Tianyi for more informtiona
     */
    public function updateCommentNotification(Request $request){
        $inp_id        = $request->input('id');
        $inp_notification   = $request->input('email_notification');

        User::where('id',$inp_id)->update([
            'email_notification' => $inp_notification?'0':'1',
        ]);

        $data = array();
        if($inp_notification){
            $data["message"] = 'Disable successfully';
        }else{
            $data["message"] = 'Enable successfully';
        }

        $data["status"] = 'success';
        return json_encode($data);
    }
}
