<?php

namespace App\Http\Controllers;

use App\Models\Pages;
use App\Models\SubscriptionModel;
use App\Models\User;
use Illuminate\Http\Request;
use Session;
use URL;
use Mail;
use Auth;
use DB;

class MarketingController extends Controller
{


    public function getMarketing(Request $request)
    {
        $image_url = config("app.image_url");
        $content = Pages::where('page', 'marketing-page')->where('website', 'vms')->first();
        $user = User::where('id', Auth::guard('practitioner')->user()->id)->first();
        $deals_preference = $this->checkSubscribe($user->id, 1);
        $vms_preference = $this->checkSubscribe($user->id, 2);
        $pharma_preference = $this->checkSubscribe($user->id, 3);
        $notification = $user->email_notification;
//        dd($content);
        return view('vms.pages.userInfo.practitioner-marketing', compact('notification', 'content', 'deals_preference', 'vms_preference', 'pharma_preference', 'image_url'));

    }

    public function updateMarketing(Request $request)
    {
        try{
            $user = User::where('id', Auth::guard('practitioner')->user()->id)->first();
            if ($request->has('notification')) {
                User::where('id', Auth::guard('practitioner')->user()->id)
                    ->update([
                        'email_notification' => $request->get('notification'),
                    ]);
            }
            if ($request->has('deals_preference')) {
                if (!($request->get('deals_preference') == $this->checkSubscribe($user->id, 1))) {
                    SubscriptionController::subscribe(new Request([
                        'email' => $user->email,
                        'interestid' => '5c55a723f5',
                        'personid' => $user->id,
                        'issubscribed' => $request->get('deals_preference') ? 'false' : 'true',
                        'person_type' => 'person',
                    ]));
                }
            }
            if ($request->has('vms_preference')) {
                if (!($request->get('vms_preference') == $this->checkSubscribe($user->id, 2))) {
                    SubscriptionController::subscribe(new Request([
                        'email' => $user->email,
                        'interestid' => '605d7525e4',
                        'personid' => $user->id,
                        'issubscribed' => $request->get('vms_preference') ? 'false' : 'true',
                        'person_type' => 'person',
                    ]));
                }
            }
            if ($request->has('pharma_preference')) {
                if (!($request->get('pharma_preference') == $this->checkSubscribe($user->id, 3))) {
                    SubscriptionController::subscribe(new Request([
                        'email' => $user->email,
                        'interestid' => '78d28c19ed',
                        'personid' => $user->id,
                        'issubscribed' => $request->get('pharma_preference') ? 'false' : 'true',
                        'person_type' => 'person',
                    ]));
                }
            }
            return response()->json(['status' => 'success', 'msg' => 'Update Successfully']);
        }catch (\Exception $e){
            return response()->json(['status' => 'error', 'msg' => 'Timeout, please try again later']);
        }


    }

    public function checkSubscribe($user, $type_id)
    {
        if (SubscriptionModel::where('person_id', $user)->where('type_id', $type_id)->exists()) {
            if (SubscriptionModel::where('person_id', $user)->where('type_id', $type_id)->whereNull('deleted_at')->first()) {
                return true;
            }
        }
        return false;
    }
}















