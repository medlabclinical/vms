<?php

namespace App\Http\Controllers;


use App\Models\OrderLinesModel;
use App\Models\OrderModel;
use App\Models\ProductFamilyModel;
use App\Models\ProductModel;
use App\Models\TempCartModel;
use App\Models\Unleashed;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Cookie;
use Barryvdh\Snappy\Facades\SnappyPdf as PDF;
use Illuminate\Database\Eloquent\Builder;

//use Knp\Snappy\Pdf;

class OrderController extends Controller
{
    public function index(Request $request)
    {

        $user = Auth::guard('practitioner')->user();
        $business = json_decode(Cookie::get('business'), true);
//        dd(OrderModel::where('customer_code',$business['customer_code'])->whereNull('deleted_at')->whereHas('orderlines', function (Builder $query){
//            $query->where('product_description', 'Mg Optima Relax, Lemon Lime - 300g');
//        })->get());
        $brand_id = Unleashed::where('brand_id',2)->first()->id;
        if(!$request->has('keyword') || $request->get('keyword') == null){
            $allOrders = OrderModel::where('customer_code',$business['customer_code'])->where('unleashed_id',$brand_id)->whereIn('order_status',['Completed','Backordered'])->whereNull('deleted_at')->orderBy('completed_date','desc')->get();
        }else{
            $search = $request->get('keyword');
//            dd($search);
            $allOrders_1 = OrderModel::where('customer_code',$business['customer_code'])->where('unleashed_id',$brand_id)->whereIn('order_status',['Completed','Backordered'])->whereNull('deleted_at')
                ->where(function($query) use ($search){
                    $query->where('order_number', 'LIKE', '%'.$search.'%');
                    $query->orWhere('customer_ref', 'LIKE', '%'.$search.'%');
                })->get();
            $allOrders_2 = OrderModel::where('customer_code',$business['customer_code'])->where('unleashed_id',$brand_id)->whereIn('order_status',['Completed','Backordered'])->whereNull('deleted_at')->whereHas('orderlines', function ($q) use ($search){
                $q->where('product_code', 'LIKE', '%'.$search.'%');
                $q->orWhere('product_description', 'LIKE', '%'.$search.'%');
            })->get();
//            dd($allOrders_2);
            $allOrders = $allOrders_2->merge($allOrders_1)->sortByDesc('completed_date')->values();
        }
        $slice = $allOrders->slice(10 * ($request->get('page', 1) - 1), 10)->all();
        $order = new LengthAwarePaginator($slice, count($allOrders), 10, $request->get('page', 1), ['path' => $request->url(), 'query' => $request->query()]);
        foreach ($order as $item) {
            $has_valid = false;
            if (count(OrderLinesModel::where('order_number', $item->order_number)->where('unleashed_id', $brand_id)->get())) {
                foreach (OrderLinesModel::where('order_number', $item->order_number)->where('unleashed_id', $brand_id)->get() as $a) {
                    $product = ProductModel::CartGetProduct($a->product_code);
                    if (!$product) {
                        continue;
                    }
                    if (in_array($product['product_group'], ['Finished goods', 'Marketing'])) {
                        if (\App\Models\StockOnHandWarehouse::where('product_guid', $product['product_guid'])->where('warehouse_name', 'Alexandria')->exists()) {
                            if (\App\Models\StockOnHandWarehouse::where('product_guid', $product['product_guid'])->where('warehouse_name', 'Alexandria')->first()->available_quantity >= 20) {
                                if ($product['sellable_in_unleashed'] && !$product['obsolete'] && $product['show_on_website']) {
                                    $has_valid = true;
                                }
                            }
                        }
                    }
                }
            }
            $item->has_valid = $has_valid;
        }
//        dd($order);
        if(!$request->has('keyword')|| $request->get('keyword') == null) {
            return view('vms/pages/userInfo/order_list', compact('order'));
        }else{
            $keyword = $request->get('keyword');
            return view('vms/pages/userInfo/order_list', compact('order','keyword'));
        }

    }
    public function printInvoice($order_number = null){
        $brand_id = Unleashed::where('brand_id',2)->first()->id;
        $order = OrderModel::where('order_number',$order_number)->where('unleashed_id',$brand_id)->first();
        $pdf = PDF::loadView('vms/pages/misc/vms_invoice_template',['order'=>$order]);
        return $pdf->download('invoice.pdf');
    }


}
