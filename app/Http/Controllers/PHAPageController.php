<?php

namespace App\Http\Controllers;

use App\Models\ClinicalTrials;
use App\Models\MediaLibrary;
use App\Models\Pages;
use App\Models\ProductInDev;
use App\Models\ProductMultipleRelation;
use App\Models\Publications;
use Illuminate\Http\Request;

class PHAPageController extends Controller
{
    public function getPageInfo(string $page){
        $content = Pages::where('page',$page)->where('website','pharmaceuticals')->first();
        $content->title_img=$content->title_img?MediaLibrary::where('id',$content->title_img)->first()->name:NULL;
        $content->second_img=$content->second_img?MediaLibrary::where('id',$content->second_img)->first()->name:NULL;
        $content->third_img=$content->third_img?MediaLibrary::where('id',$content->third_img)->first()->name:NULL;
        return $content;
    }
    public function getPrivacyPolicy(){
        $image_url=config("app.image_url");
        $content = $this->getPageInfo('privacy policy');
        return view('pharma/pages/policies/privacy_policy',compact('content','image_url'));
    }
    public function getShippingAndDelivery(){
        $image_url=config("app.image_url");
        $content = $this->getPageInfo('shipping and delivery');
        return view('pharma/pages/policies/shipping_and_delivery',compact('content','image_url'));
    }
    public function getReturns(){
        $image_url=config("app.image_url");
        $content = $this->getPageInfo('returns');
        return view('pharma/pages/policies/returns',compact('content','image_url'));
    }
    public function getSalesPolicy(){
        $image_url=config("app.image_url");
        $content = $this->getPageInfo('sales policy');
        return view('pharma/pages/policies/sales_policy',compact('content','image_url'));
    }

}
