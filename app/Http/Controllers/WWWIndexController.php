<?php

namespace App\Http\Controllers;

use App\Models\Articles;
use App\Models\AssociationModel;
use Snowfire\Beautymail\Beautymail;
use App\Models\CustomerPerson;
use App\Models\CustomerPersonModel;
use App\Models\MediaLibrary;
use App\Models\ModalityPerson;
use App\Models\Pages;
use App\Models\Patents;
use App\Models\Patient;
use App\Models\PatientAddress;
use App\Models\PrimaryChannel;
use App\Models\SecondaryChannel;
use App\Models\Modality;
use App\Models\Subweb;
use App\Models\Translate;
use App\Models\Addressbook;
use App\Models\UnleashedCustomer;
use App\Models\User;
use App\Models\WebsiteApplication;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Session;
use URL;
use Mail;
use Auth;
use DB;
use App\Http\Middleware\AppAzure;

class WWWIndexController extends Controller
{



    public function index(){
        $image_url=config("app.image_url");
        $content = Pages::where('page','home')->where('website','medlab')->first();
        $content->title_img=MediaLibrary::where('id',$content->title_img)->first()->name;
        $content->second_img=$content->second_img?MediaLibrary::where('id',$content->second_img)->first()->name:NULL;
        $content->third_img=$content->third_img?MediaLibrary::where('id',$content->third_img)->first()->name:NULL;
        $subweb = DB::table('subweb')->join('nova_media_library','subweb.image','=','nova_media_library.id')
            ->select('subweb.*','nova_media_library.name as image')->get();
        $recent_articles = Articles::where('publish',1)->whereNull('deleted_at')->whereHas('brand',function ($q){
            $q->whereIn('brand.id',[1,3]);
        })->orderBy('date','desc')->take(3)->get();
        foreach ($recent_articles as $article) {
            $article->title_img = $article->title_img ? MediaLibrary::where('id', $article->title_img)->first()->name : NULL;
            $article->second_img = $article->second_img ? MediaLibrary::where('id', $article->second_img)->first()->name : NULL;
            $article->third_img = $article->third_img ? MediaLibrary::where('id', $article->third_img)->first()->name : NULL;

        }
        $highlight_article = json_decode($content->highlight_article);
        if (!$highlight_article) {
            $highlight_article = [];
        }

        for ($i = 0; $i < count($highlight_article); $i++) {
            $highlight_article[$i] = Articles::where('id', $highlight_article[$i])->first();
        }
        foreach ($highlight_article as $article) {
            $article->title_img = $article->title_img ? MediaLibrary::where('id', $article->title_img)->first()->name : NULL;
            $article->second_img = $article->second_img ? MediaLibrary::where('id', $article->second_img)->first()->name : NULL;
            $article->third_img = $article->third_img ? MediaLibrary::where('id', $article->third_img)->first()->name : NULL;

        }
        return view('www/pages/home/index', compact('content', 'image_url', 'subweb', 'recent_articles', 'highlight_article'));
    }

    public function getAboutCategory()
    {

        return Pages::whereIn('page', ['about', 'contact', 'commercial_partners', 'collaborations', 'industry_associations', 'partner opportunities', 'leadership team', 'consulting_team'])->get();
    }

    public function locale($locale)
    {
        if (!in_array($locale, ['cn', 'en'])) {
            $locale = 'cn';
        }

        Session::put('locale', $locale);

        return redirect(url(URL::previous()));
    }

    public function logout()
    {
//        $url = URL::current().'/logout/azure';
        Auth::logout();
        return redirect('/logout/azure');
    }

    public function getInvestorCategory()
    {
        return Pages::whereIn('page', ['asx announcement', 'corporate governance', 'share price'])->get();
    }

    public function getCorporateContent()
    {
        $image_url = config("app.image_url");
        $content = Pages::where('page', 'corporate governance')->where('website', 'medlab')->first();
        $content->title_img = MediaLibrary::where('id', $content->title_img)->first()->name;
        $content->second_img = $content->second_img ? MediaLibrary::where('id', $content->second_img)->first()->name : NULL;
        $content->third_img = $content->third_img ? MediaLibrary::where('id', $content->third_img)->first()->name : NULL;
        $investor_list = $this->getInvestorCategory();
        return view('www/pages/investors/corporate_governance', compact('content', 'image_url', 'investor_list'));
    }

    public function getSharePriceContent()
    {
        $image_url = config("app.image_url");
        $content = Pages::where('page', 'share price')->where('website', 'medlab')->first();
        $content->title_img = MediaLibrary::where('id', $content->title_img)->first()->name;
        $content->second_img = $content->second_img ? MediaLibrary::where('id', $content->second_img)->first()->name : NULL;
        $content->third_img = $content->third_img ? MediaLibrary::where('id', $content->third_img)->first()->name : NULL;
        $investor_list = $this->getInvestorCategory();
        return view('www/pages/investors/share_price', compact('content', 'image_url', 'investor_list'));
    }

    public function getASXContent()
    {
        $image_url = config("app.image_url");
        $content = Pages::where('page', 'asx announcement')->where('website', 'medlab')->first();
        $content->title_img = MediaLibrary::where('id', $content->title_img)->first()->name;
        $content->second_img = $content->second_img ? MediaLibrary::where('id', $content->second_img)->first()->name : NULL;
        $content->third_img = $content->third_img ? MediaLibrary::where('id', $content->third_img)->first()->name : NULL;
        $investor_list = $this->getInvestorCategory();
        return view('www/pages/investors/asx_announcement', compact('content', 'image_url', 'investor_list'));
    }

    public function getTranslate()
    {
        $image_url = config("app.image_url");
        $allcountry = ['English', 'SaudiArabia', 'Spain', 'Russia', 'France', 'Japan', 'Italy', 'SimplifiedChinese', 'TraditionalChinese'];
        $list = [];
        $data = Translate::all();
        foreach ($data as $item) {
            foreach ($allcountry as $country) {
                $is = 'is' . $country;
                if ($item->$is) {
                    $list[$item['product_name']][$country] = $item[$country] . '& &' . $item[$country . '_link'];

                }
            }
        }
        return view('www/pages/misc/translate', compact('list', 'data', 'image_url'));
//        $list = Patents::all();
//        return view('translate',compact('list'));
    }

    public function getContactContent()
    {
        $image_url = config("app.image_url");
        $content = Pages::where('page', 'contact')->where('website', 'medlab')->first();
        $content->title_img = $content->title_img ? MediaLibrary::where('id', $content->title_img)->first()->name : NULL;
        $content->second_img = $content->second_img ? MediaLibrary::where('id', $content->second_img)->first()->name : NULL;
        $content->third_img = $content->third_img ? MediaLibrary::where('id', $content->third_img)->first()->name : NULL;
        $about_list = $this->getAboutCategory();
        return view('www/pages/about/contact', compact('content', 'image_url', 'about_list'));
    }

    public function store()
    {

//        $this->send();
        $send_to = "tianyi_jiang@medlab.co";
        $send_subject = "Ajax form ";


        /*Be careful when editing below this line */

        $f_name = $_POST["name"];
        $f_email = $_POST["email"];
        $f_message = $_POST["message"];
        $from_ip = $_SERVER['REMOTE_ADDR'];
        $from_browser = $_SERVER['HTTP_USER_AGENT'];


        $time = "This email was submitted on " . date('m-d-Y');
        $name = "Name: " . $f_name;
        $messages = "Message: " . $f_message;
        $details = "Technical Details: " . $from_ip . "\n" . $from_browser;

//        $messages = "This email was submitted on " . date('m-d-Y') .
//            "\n\nName: " . $f_name .
//            "\n\nE-Mail: " . $f_email .
//            "\n\nMessage: \n" . $f_message .
//            "\n\n\nTechnical Details:\n" . $from_ip . "\n" . $from_browser;

//        $send_subject .= " - {$f_name}";

        $headers = "Reply-To: " . $f_email;
//        $headers = "From: " . $f_email . "\r\n" .
//            "Reply-To: " . $f_email . "\r\n" .
//            "X-Mailer: PHP/" . phpversion();


        if (!$f_email) {
            return response()->json('Please input an email', 401);
        } else if (!$f_name) {
            return response()->json('Please input your name', 401);

        } else {

            if (filter_var($f_email, FILTER_VALIDATE_EMAIL)) {
                $this->send($send_to, $headers, $time, $name, $messages, $details);

//                dd('1');
                return response()->json(['success' => 'Thanks for contacting us! We will be in touch with you shortly.']);
            } else {
                return response()->json('Invalid email, please check the format!', 401);
            }
        }
    }


    public function send($send_to, $headers, $time, $name, $messages, $details)
    {
//        Mail::send('email-template', ['time' => $time, 'messages' => $messages, 'headers' => $headers, 'name' => $name, 'details' => $details], function ($message) use ($send_to) {
//            $to = $send_to;
//            $message->to($to)->subject('Contact email from');
//        });
        $beautymail = app()->make(Beautymail::class);
        $beautymail->send('emails.email-contact-us', ['time' => $time, 'messages' => $messages, 'headers' => $headers, 'name' => $name, 'details' => $details], function ($message) use ($send_to, $name) {
            $message->to($send_to)->subject('Contact email from');
        });

    }

    //User Information functions

    public function getRoleInfo()
    {
        $associations = AssociationModel::all();
        $primary_channel = PrimaryChannel::all();
        $second_channel = SecondaryChannel::all();
        $modality = Modality::all();
        try {
            if (Auth::guard('patient')->check()) {
                $role = Patient::where('id', Auth::guard('patient')->user()->id)->first();
                $patient_address = $role->patient_address()->first();
                    $referral_code = $role->guid?$role->practitioner()->first()->referral_code:null;
                    $referral_name = $role->guid?$role->practitioner()->first()->customer_name:null;
                return view('www/pages/userInfo/patient-profile', compact('role', 'associations',
                    'patient_address','referral_code','referral_name'));

            } else if(Auth::guard('practitioner')->check()) {
                $role = User::where('id', Auth::guard('practitioner')->user()->id)->first();
//                dd($role->modalityperson()->first()->name);
                return view('www/pages/userInfo/practitioner-profile', compact('role', 'associations',
                    'primary_channel', 'second_channel', 'modality'));
            }
        } catch (\Exception $e) {
            return redirect('/');

        }

    }

    //***Function for patient detail update***//

    public function editPatientInfo(Request $request)
    {
        Patient::where('id', $request->get('id'))
            ->update(['salutation' => $request->get('salutation'),
                'first_name' => $request->get('first_name'),
                'last_name' => $request->get('last_name'),
                'email' =>  $request->get('email'),
                'phone' => $request->get('phone'),
                'mobile' => $request->get('mobile'),
                'guid' => UnleashedCustomer::where('referral_code', $request->get('referral_code'))->first()->guid,

            ]);

        PatientAddress::where('id', $request->get('patient_address_id'))
            ->updateorInsert([
                'patient_id' => $request->get('id'),
                'address_line_1' => $request->get('address_line_1'),
                'address_line_2' => $request->get('address_line_2'),
                'suburb' => $request->get('suburb'),
                'postcode' => $request->get('postcode'),
                'city' => $request->get('city'),
                'state' => $request->get('state'),
                'country' => $request->get('country'),
            ]);
        return response()->json(['msg' => 'success']);
    }

    //***Function for practitioner detail update***//

    public function updatePractitionerInfo(Request $request)
    {
//        dd($request->all());
        $salutation = $request->get('salutation');
        $first_name = $request->get('first_name');
        $last_name = $request->get('last_name');
        $email = $request->get('email');
        $phone = $request->get('phone');
        $mobile = $request->get('mobile');
        $address_line_1 = $request->get('address_line_1');
        $address_line_2 = $request->get('address_line_2');
        $suburb = $request->get('suburb');
        $postcode = $request->get('postcode');
        $country = $request->get('country');
        $state = $request->get('state');
        $city = $request->get('city');
        $business_phone = $request->get('business_phone');
        $fax_number = $request->get('fax_number');
//        $password =$request->get('current_password');

        $association_number = $request->get('association_number');
        $association_id = $request->get('association_name');

        $job_title = $request->get('job_title');

        $primary_profession = $request->get('primary_profession');

        $primary_channel = $request->get('primary_channel');
        $second_channel = $request->get('second_channel');

        //Persons table is updated

        User::where('id', $request->get('id'))
            ->update(['salutation' => $salutation,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'email' => $email,
                'mobile' => $mobile,
                'phone' => $phone,
                'job_title' => $job_title,
                'association_number' => $association_number,
                'association_id' => $association_id,

            ]);

        //Ucustomers addressbook is updated

        Addressbook::where('customer_code', Auth::guard('practitioner')->user()->practitioner()->first()->guid)
            ->update([
                'address_line_1' => $address_line_1,
                'address_line_2' => $address_line_2,
                'suburb' => $suburb,
                'postcode' => $postcode,
                'city' => $city,
                'state' => $state,
                'country' => $country,
            ]);
        ModalityPerson::where('person_id', Auth::guard('practitioner')->user()->id)
            ->updateOrInsert([
                'person_id' => Auth::guard('practitioner')->user()->id,
                'modality_id' => Modality::where('name', $primary_profession)->first()->id,
            ]);

        //Ucustomers table field updated

        UnleashedCustomer::where('guid', Auth::guard('practitioner')->user()->practitioner()->first()->guid)
            ->update([
                'phone_number' => $business_phone,
                'fax_number' => $fax_number,
                'channel_id' => PrimaryChannel::where('name', $primary_channel)->first()->id,
                'secondary_channel_id' => SecondaryChannel::where('name', $second_channel)->first()->id,
            ]);


        return response()->json(['msg' => 'success']);
    }




    public function updateReferralCode(Request $request)
    {

       $referral_code = $request->get('current_referral_code');
       $new_referral_code = $request->get('new_referral_code');
//        dd($new_referral_code);
//        $guid = UnleashedCustomer::where('referral_code', $request->get('referral_code'))->first()->guid;

        UnleashedCustomer::where('referral_code', $referral_code)
            ->update([
                'referral_code' => $new_referral_code,

            ]);

        return response()->json(['msg' => 'success']);

    }

    // Change Password

    public function getCurrentPassword()
    {

        try {
            if (Auth::guard('patient')->check()) {
                $role = Patient::where('id', Auth::guard('patient')->user()->id)->first();
//                (dd($role));
                $password = $role->password;

                return view('www/pages/userInfo/patient-password-page', compact('password', 'role'));
            } else {
                $role = User::where('id', Auth::guard('practitioner')->user()->id)->first();
                $password = $role->password;
                return view('www/pages/userInfo/practitioner-password-page', compact('password','role'));
            }

        } catch (\Exception $e) {
            return redirect('/');
    }
}

    public function updatePatientPassword(Request $request){

      $current_password = $request->get('current_password');
        $password = $request->get('new-password');
        if(!Hash::check($current_password,Auth::guard('patient')->user()->password)){
            return response()->json(['msg' => 'wrong password']);
        }else{
            if ($request->has('current_password')) {
                Patient::where('id',Auth::guard('patient')->user()->id)
                    ->update([
                        'password' => Hash::make($password),
                    ]);
                return response()->json(['msg' => 'success']);
            }
            else{

                return response()->json(['msg' => 'error']);
            }
        }

    }

    public function updatePractitionerPassword(Request $request){

//        $current_password = Hash::make($request->get('current_password'));
        if(!Hash::check($request->get('current_password'),Auth::guard('practitioner')->user()->password)){
            return response()->json(['msg' => 'wrong password']);
        }else{
            $password= $request->get('new_password');
            User::where('id', Auth::guard('practitioner')->user()->id)
                ->update([
                    'password' => Hash::make($password),
                ]);
            return response()->json(['msg' => 'success']);
        }


    }
}















