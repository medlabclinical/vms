<?php

namespace App\Http\Controllers;

use App\Models\Articles;
use App\Models\ClinicalTrials;
use App\Models\DietaryModel;
use App\Models\MediaLibrary;
use App\Models\Pages;
use App\Models\ProductFamilyModel;
use App\Models\ProductGroupModel;
use App\Models\ProductInDev;
use App\Models\ProductMultipleRelation;
use App\Models\ProductPdfModel;
use App\Models\Publications;
use App\Models\User;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Cookie;

class PageController extends Controller
{
    public function getPageInfo(string $page){
        $content = Pages::where('page',$page)->where('website','vms')->first();
//        $content->title_img=$content->title_img?MediaLibrary::where('id',$content->title_img)->first()->name:NULL;
//        $content->second_img=$content->second_img?MediaLibrary::where('id',$content->second_img)->first()->name:NULL;
//        $content->third_img=$content->third_img?MediaLibrary::where('id',$content->third_img)->first()->name:NULL;
        return $content;
    }
    public function getPrivacyPolicy(){
        $image_url=config("app.image_url");
        $content = $this->getPageInfo('privacy policy');
        return view('vms/pages/policies/privacy_policy',compact('content','image_url'));
    }
    public function getShippingAndDelivery(){
        $image_url=config("app.image_url");
        $content = $this->getPageInfo('shipping and delivery');
        return view('vms/pages/policies/shipping_and_delivery',compact('content','image_url'));
    }
    public function getReturns(){
        $image_url=config("app.image_url");
        $content = $this->getPageInfo('returns');
        return view('vms/pages/policies/returns',compact('content','image_url'));
    }
    public function getSalesPolicy(){
        $image_url=config("app.image_url");
        $content = $this->getPageInfo('sales policy');
        return view('vms/pages/policies/sales_policy',compact('content','image_url'));
    }
    public function getTermsAndConditions()
    {
        $image_url = config("app.image_url");
        $content = $this->getPageInfo('terms and conditions');
        return view('vms/pages/policies/terms_and_conditions', compact('content', 'image_url'));
    }
    public function getAboutMenu()
    {
        $image_url = config("app.image_url");
        $list = [];
        $list['About'] = Pages::whereIn('page', ['about', 'contact'])->where('website','vms')->get();
        $header = 'About';
        return view('vms/pages/about/about', compact('image_url', 'list','header'));
    }
    public function getAbout()
    {
        $image_url = config("app.image_url");
        $content = $this->getPageInfo('about');
        return view('vms/pages/about/about_us', compact('content', 'image_url'));
    }
    public function getGroupProduct()
    {
        $index = ProductGroupModel::all();
        $image_url = config("app.image_url");
        $content = $this->getPageInfo('products by category');
        return view('vms/pages/products/products_by_categories', compact('index', 'image_url','content'));
    }

    public function getDietaryProduct()
    {
        $index = DietaryModel::all();
        $image_url = config("app.image_url");
        $content = $this->getPageInfo('products by dietary');
        return view('vms/pages/products/products_by_dietaries', compact('index', 'image_url','content'));
    }

    /**
     * getSearchResult
     *
     * @return array
     */
    public function searchPage(Request $request){
        $search = $request['search'];
        $image_url=config("app.image_url");
        $article_1 = Articles::where('publish','1')->whereNull('deleted_at')->whereHas('brand', function ($q) {
            $q->where('brand.id', 2);
        })->where('title', 'LIKE', '%'.$search.'%')->orderBy('date', 'desc')->get();
        $article_2 = Articles::where('publish','1')->whereNull('deleted_at')->whereHas('brand', function ($q) {
            $q->where('brand.id', 2);
        })->where('subtitle', 'LIKE', '%'.$search.'%')->orderBy('date', 'desc')->get();
        $article_3 = Articles::where('publish','1')->whereNull('deleted_at')->whereHas('brand', function ($q) {
            $q->where('brand.id', 2);
        })->where('content', 'LIKE', '%'.$search.'%')->orderBy('date', 'desc')->get();
        $article_4 = Articles::where('publish','1')->whereNull('deleted_at')->whereHas('brand', function ($q) {
            $q->where('brand.id', 2);
        })->where('category', 'LIKE', '%'.$search.'%')->orderBy('date', 'desc')->get();
        $article_5 = Articles::where('publish','1')->whereNull('deleted_at')->whereHas('brand', function ($q) {
            $q->where('brand.id', 2);
        })->where('preview_text', 'LIKE', '%'.$search.'%')->orderBy('date', 'desc')->get();
        $article = $article_1->merge($article_2)->merge($article_3)->merge($article_4)->merge($article_5);
        foreach ($article as $item){
            $item->title_img = $item->title_img ? MediaLibrary::where('id', $item->title_img)->first()->name : NULL;
            $item->second_img = $item->second_img ? MediaLibrary::where('id', $item->second_img)->first()->name : NULL;
            $item->third_img = $item->third_img ? MediaLibrary::where('id', $item->third_img)->first()->name : NULL;
        }
        $flyers = ProductPdfModel::where('obsolete', '!=', '1')->whereNull('deleted_at')->whereHas('brand', function ($q) {
            $q->where('brand.id', 2);
        })->where('name', 'LIKE', '%'.$search.'%')->orderBy('product_pdfs.updated_at', 'desc')->get();
        $product_family = ProductFamilyModel::where('brand_id','2')->whereNull('deleted_at')
            ->where(function($query) use ($search){
                $query->where('product_name', 'LIKE', '%'.$search.'%');
                $query->orWhere('product_byline', 'LIKE', '%'.$search.'%');
                $query->orWhere('product_heading', 'LIKE', '%'.$search.'%');
                $query->orWhere('directions_use', 'LIKE', '%'.$search.'%');
                $query->orWhere('product_description', 'LIKE', '%'.$search.'%');
                $query->orWhere('warnings', 'LIKE', '%'.$search.'%');
                $query->orWhere('unit_type', 'LIKE', '%'.$search.'%');
                $query->orWhere('reg_permissableindications', 'LIKE', '%'.$search.'%');
                $query->orWhere('reg_suitable_for_ages_above', 'LIKE', '%'.$search.'%');
                $query->orWhere('side_effects', 'LIKE', '%'.$search.'%');
                $query->orWhere('product_interactions', 'LIKE', '%'.$search.'%');
                $query->orWhere('austl_number', $search);
                $query->orWhere('reg_suitable_for_ages_above', 'LIKE', '%'.$search.'%');
                $query->orWhere('side_effects', 'LIKE', '%'.$search.'%');
                $query->orWhere('product_interactions', 'LIKE', '%'.$search.'%');
            })->get();
        foreach ($product_family as $k => $a) {
            if (!in_array('1', array_column($a->product()->get()->toArray(), 'show_on_website'))) {
                $product_family->forget($k);
            } elseif (!in_array('0', array_column($a->product()->get()->toArray(), 'obsolete'))) {
                $product_family->forget($k);
            }
//            elseif (!in_array('1', array_column(array_filter($a->product()->get()->toArray(), function ($var) {
//                return ($var['show_on_website'] == '1');
//            }), 'sellable_in_unleashed'))) {
//                $product_family->forget($k);
//
//            }
        }
        $product_ingredient = ProductFamilyModel::where('brand_id','2')->whereNull('deleted_at')
            ->whereHas('product_ingredient',function ($query) use ($search){
                $query->where('name', 'LIKE', '%'.$search.'%');
                $query->orWhere('scientific_name', 'LIKE', '%'.$search.'%');
                $query->orWhere('consumer_ingredient', 'LIKE', '%'.$search.'%');
                $query->orWhere('practitioner_ingredient', 'LIKE', '%'.$search.'%');
            })->get();
        foreach ($product_ingredient as $k => $a) {
            if (!in_array('1', array_column($a->product()->get()->toArray(), 'show_on_website'))) {
                $product_ingredient->forget($k);
            } elseif (!in_array('0', array_column($a->product()->get()->toArray(), 'obsolete'))) {
                $product_ingredient->forget($k);
            }
//            elseif (!in_array('1', array_column(array_filter($a->product()->get()->toArray(), function ($var) {
//                return ($var['show_on_website'] == '1');
//            }), 'sellable_in_unleashed'))) {
//                $product_ingredient->forget($k);
//
//            }
        }
        $all_products = $product_family->merge($product_ingredient);
//        foreach ($all_products as $k => $a) {
//            if (!in_array('1', array_column($a->product()->get()->toArray(), 'show_on_website'))) {
//                $all_products->forget($k);
//            } elseif (!in_array('1', array_column(array_filter($a->product()->get()->toArray(), function ($var) {
//                return ($var['show_on_website'] == '1');
//            }), 'sellable_in_unleashed'))) {
//                $all_products->forget($k);
//
//            }
//        }
        $all_results = $all_products->merge($product_ingredient)->merge($article)->merge($flyers);
        $amount = count($all_results->toArray());
        $tab_amount = (count($article)?1:0) + (count($flyers)?1:0) + (count($all_products)?1:0) + (count($product_ingredient)?1:0);
        if(json_decode(Cookie::get('business'),true)){
            $if_au =  json_decode(Cookie::get('business'),true)['classification_id']==1?1:0;
        }else{
            $if_au = 1;
        }
        return view('vms/pages/misc/search_result', compact('all_products','product_ingredient','article','image_url','all_results','amount','search','if_au','tab_amount','flyers'));

    }
    public function getReferralCode()
    {
        $image_url = config("app.image_url");
        $content = $this->getPageInfo('change referral code');
        if (Auth::guard('practitioner')->check()) {
            $role = User::where('id', Auth::guard('practitioner')->user()->id)->first();
            return view('vms/pages/userInfo/change-referral-code', compact('role','content','image_url'));
        }
        else{
            return redirect(404);
        }

    }
}
