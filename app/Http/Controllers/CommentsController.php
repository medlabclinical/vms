<?php

namespace App\Http\Controllers;

use App\Models\Articles;
use App\Models\Comments;
use App\Models\User;
use Illuminate\Http\Request;
use Mail;
use DB;
use Auth;
use Snowfire\Beautymail\Beautymail;

class CommentsController extends Controller
{
    public function index($id){
        $comments = DB::table('comments')->join('persons','comments.user_id','=','persons.id')->select('comments.*','persons.first_name as firstname','persons.last_name as lastname')->where('article_id',$id)->where('parent_id',0)->get();
//        $comments = Comments::where('article_id',$id)->where('parent_id',0)->get();
        foreach ($comments as $comment){
            $comment->children = Comments::with('children')->findOrFail($comment->id)->relationsToArray()['children'];
        }
//       dd($comments->toArray()[0]->children);
        return $comments->toArray();
    }
    public function store(Request $request){
        if(Auth::guard('practitioner')->check()){
            $user = Auth::guard('practitioner')->user();
            $comment = Comments::create([
                'user_name' => ($user->first_name && $user->last_name)?$user->first_name.' '.substr($user->last_name,0,1):null,
                'email' => $user->email,
                'content' => $request['content'],
                'user_id' => Auth::guard('practitioner')->user()->id,
                'article_id' => isset($request['article'])?$request['article']:0,
                'parent_id' => $request['parent'],
            ]);
            if($request['parent']){
                $parent = Comments::where('id',$request['parent'])->first();
                if(User::where('id',$parent['user_id'])->first()['email_notification']){
                    $sendTo = User::where('id',$parent['user_id'])->first()['email'];
                    $headers = $comment->user_name.' commented on your post';
                    $name = $parent['user_name'];
                    $messages = $request['content'];
                    $url = route('articles_content',['id' => preg_replace('/--+/','-',preg_replace('/[^A-Za-z0-9\-]/', '-', Articles::where('id',$parent['article_id'])->first()['title'])).'-'.$parent['article_id']]).'#'.$request['id'];
                    $this->send($sendTo, $headers, $messages,$url,$name);
                }
            }
        }else{
            Comments::create([
                'user_name' => (Auth::guard('patient')->user()->first_name && Auth::guard('patient')->user()->last_name)?Auth::guard('patient')->user()->first_name.' '.substr(Auth::guard('patient')->user()->last_name,0,1):null,
                'email' => Auth::guard('patient')->user()->email,
                'content' => $request['content'],
                'user_id' => Auth::guard('patient')->user()->id,
                'article_id' => isset($request['article'])?$request['article']:0,
                'parent_id' => $request['parent'],
            ]);
        }


        return response()->json(['Reply successfully!']);
    }

    public function send($send_to, $headers, $messages,$url,$name)
    {

        $beautymail = app()->make(Beautymail::class);
        $beautymail->send('emails.email-template-comment', ['messages' => $messages, 'headers' => $headers, 'name' => $name, 'url'=> $url, 'login_url'=> config('app.url').'/practitioner-marketing-page'], function($message) use ($send_to, $headers)
        {
            $message->to($send_to)->subject($headers);
        });

    }


}
