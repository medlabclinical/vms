<?php

namespace App\Http\Controllers;

use App\Models\ClinicalTrials;
use Illuminate\Http\Request;

class ClinicalController extends Controller
{
    public function index(){
        $cannabisList = ClinicalTrials::where('type','Cannabis')->get();
        $deliveryPlatformList=ClinicalTrials::where('type','Delivery Platform')->get();
        $depressionList=ClinicalTrials::where('type','Depression')->get();
        $mucositisList=ClinicalTrials::where('type','Mucositis')->get();
        $diabetesList=ClinicalTrials::where('type','Diabetes')->get();
        $partiesList=ClinicalTrials::where('type','Contract for 3rd Parties')->get();

        return view('clinical_trials',compact('cannabisList','deliveryPlatformList','depressionList','mucositisList','diabetesList','partiesList'));
    }
}
