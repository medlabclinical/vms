<?php

namespace App\Http\Controllers;

use App\Models\Articles;
use App\Models\MediaLibrary;
use App\Models\Pages;
use App\Models\Subweb;
use Session;
use URL;
use Mail;
use Auth;
use DB;

class IndexController extends Controller
{

    public function index(){
        $image_url=config("app.image_url");
        $content = Pages::where('page','home')->where('website','vms')->first();
        $content->title_img=MediaLibrary::where('id',$content->title_img)->first()->name;
        $content->second_img=$content->second_img?MediaLibrary::where('id',$content->second_img)->first()->name:NULL;
        $content->third_img=$content->third_img?MediaLibrary::where('id',$content->third_img)->first()->name:NULL;
        $subweb = Subweb::whereHas('brand',function ($q){
            $q->where('brand.id',2);
        })->whereNull('subweb.deleted_at')->where('if_on_page',1)->get();
        $recent_articles = Articles::where('publish',1)->whereNull('deleted_at')->whereHas('brand',function ($q){
            $q->where('brand.id',2);
        })->orderBy('date','desc')->take(3)->get();
        foreach ($recent_articles as $article) {
            $article->title_img = $article->title_img ? MediaLibrary::where('id', $article->title_img)->first()->name : NULL;
            $article->second_img = $article->second_img ? MediaLibrary::where('id', $article->second_img)->first()->name : NULL;
            $article->third_img = $article->third_img ? MediaLibrary::where('id', $article->third_img)->first()->name : NULL;

        }
        $highlight_article = json_decode($content->highlight_article);
        if (!$highlight_article) {
            $highlight_article = [];
        }

        for ($i = 0; $i < count($highlight_article); $i++) {
            $highlight_article[$i] = Articles::where('id', $highlight_article[$i])->first();
        }
        foreach ($highlight_article as $article) {
            $article->title_img = $article->title_img ? MediaLibrary::where('id', $article->title_img)->first()->name : NULL;
            $article->second_img = $article->second_img ? MediaLibrary::where('id', $article->second_img)->first()->name : NULL;
            $article->third_img = $article->third_img ? MediaLibrary::where('id', $article->third_img)->first()->name : NULL;

        }
        return view('vms/pages/home/index', compact('content', 'image_url', 'subweb', 'recent_articles', 'highlight_article'));
    }

    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    public function loginPage()
    {
        $image_url=config("app.image_url");
        return view('vms/pages/misc/log-in',compact('image_url'));
    }
    public function locale($locale)
    {
        if (!in_array($locale, ['cn', 'en'])) {
            $locale = 'cn';
        }

        Session::put('locale', $locale);

        return redirect(url(URL::previous()));
    }
    public function getContactContent()
    {
        $image_url = config("app.image_url");
        $content = Pages::where('page', 'contact')->where('website', 'vms')->first();
//        $content->title_img = $content->title_img ? MediaLibrary::where('id', $content->title_img)->first()->name : NULL;
//        $content->second_img = $content->second_img ? MediaLibrary::where('id', $content->second_img)->first()->name : NULL;
//        $content->third_img = $content->third_img ? MediaLibrary::where('id', $content->third_img)->first()->name : NULL;
        return view('vms/pages/about/contact', compact('content', 'image_url'));
    }



}

