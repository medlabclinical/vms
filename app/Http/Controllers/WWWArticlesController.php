<?php

namespace App\Http\Controllers;

use App\Models\ArticleCategory;
use App\Models\Articles;
use App\Models\MediaLibrary;
use App\Models\Pages;
use App\Models\ProductMultipleRelation;
use http\Env;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class WWWArticlesController extends Controller
{
    public function index(Request $request)
    {

//        dd($request->all());
        $role = $request->has('role')?$request->get('role'):"*";
        $article_category = $request->has('article_category')?$request->get('article_category'):"*";
        $allArticles = Articles::where('publish', 1)->whereNull('deleted_at')
            ->when($role!="*", function ($query) use ($request) {
            $query->where('req_login', $request->get('role'));
            })->whereHas('brand',function ($q){
                $q->whereIn('brand.id',[1,3]);
            })->orderBy('date', 'desc')->get();
        $allData = Articles::where('publish',1)->whereNull('deleted_at')->whereHas('brand',function ($q){
            $q->whereIn('brand.id',[1,3]);
        })->get();
        $category = array();
        $list = array();
        foreach ($allData as $article){
            if ($article->article_category()->first()) {
                $name = $article->article_category()->first()->name;
                $article->category = $name;
                if (!isset($list[$name])) {
                    array_push($category, $name);
                    $list[$name] = [];
                }
                array_push($list[$name], $article);
            } else {
                $article->category = '';
            }
        }
        if ($article_category!="*") {
            if (ArticleCategory::where('name', $article_category)->exists()) {
                $data = collect();
                $id = ArticleCategory::where('name', $article_category)->first()['id'];
                foreach ($allArticles as $item) {
                    if (in_array($id, array_column(ProductMultipleRelation::where('article_id', $item->id)->get()->toArray(), 'article_category_id'))) {
                        $data->add($item);
                    }
                }
                $allArticles = $data;
                $content = ArticleCategory::where('name',$article_category)->first();
            }else{
                return redirect(404);
            }
        }else{
            $content = Pages::where('page', 'education')->where('website', 'medlab')->first();
        }
        $image_url = config("app.image_url");

        foreach ($allArticles as $article) {
            $article->title_img = $article->title_img ? MediaLibrary::where('id', $article->title_img)->first()->name : NULL;
            $article->second_img = $article->second_img ? MediaLibrary::where('id', $article->second_img)->first()->name : NULL;
            $article->third_img = $article->third_img ? MediaLibrary::where('id', $article->third_img)->first()->name : NULL;

        }
        $content->title_img = $content->title_img ? MediaLibrary::where('id', $content->title_img)->first()->name : NULL;
        $content->second_img = $content->second_img ? MediaLibrary::where('id', $content->second_img)->first()->name : NULL;
        $content->third_img = $content->third_img ? MediaLibrary::where('id', $content->third_img)->first()->name : NULL;

        $slice = $allArticles->slice(config('app.page-size') * ($request->get('page', 1) - 1), config('app.page-size'))->all();
        $allArticles = new LengthAwarePaginator($slice, count($allArticles), config('app.page-size'), $request->get('page', 1), ['path' => $request->url(), 'query' => $request->query()]);
        return view('www/pages/education/education', compact('list', 'allArticles', 'image_url', 'content', 'category', 'article_category','role'));
    }


    public function getContent($id)
    {
        $id = substr($id,strrpos($id,"-")+1);
        $image_url = config("app.image_url");
        if(!Articles::where('id', $id)->exists()){
            return redirect(404);
        }
        $article = Articles::where('id', $id)->first();
        $article->title_img = $article->title_img ? MediaLibrary::where('id', $article->title_img)->first()->name : NULL;
        $article->second_img = $article->second_img ? MediaLibrary::where('id', $article->second_img)->first()->name : NULL;
        $article->third_img = $article->third_img ? MediaLibrary::where('id', $article->third_img)->first()->name : NULL;
        $member = $article->member()->get()->toArray();
        $related_article = [];
        if (count($member)) {
            for ($i = 0; $i < count($member); $i++) {
                if ($member[$i]['picture']) {
                    $picture = MediaLibrary::where('id', $member[$i]['picture'])->first()->name;
                    $member[$i]['picture'] = $picture;
                    foreach (array_column(ProductMultipleRelation::where('member_id', $member[$i]['id'])->get()->toArray(), 'article_id') as $item) {
                        if ($item != $id && $item != null) {
                            $related_article[] = $item;
                        }
                    }
                }
            }
        }
        $article->related_article = Articles::whereIn('id', $related_article)->where('publish', 1)->whereHas('brand',function ($q){
            $q->whereIn('brand.id',[1,3]);
        })->orderBy('date', 'desc')->limit(3)->get();
//        $article->related_article=array_values(array_unique($related_article));
        $article->member = $member;
        $article->product_family = $article->product_family()->get()->toArray();
        $article->clinical_trial = $article->clinical_trial()->get()->toArray();
        $article->product_ingredient = $article->product_ingredient()->get()->toArray();
        $article->publication = $article->publication()->get()->toArray();
        $article->article_category = $article->article_category()->get()->toArray();
        foreach ($article->related_article as $item){
            $item->title_img = $item->title_img ? MediaLibrary::where('id', $item->title_img)->first()->name : NULL;
            $item->second_img = $item->second_img ? MediaLibrary::where('id', $item->second_img)->first()->name : NULL;
            $item->third_img = $item->third_img ? MediaLibrary::where('id', $item->third_img)->first()->name : NULL;
        }
//        $article_category = ArticleCategory::all();
//        dd($article);
//        $comment = new CommentsController();
        $comments = (new CommentsController())->index($id);
//        dd($comments);
        if ($article->article_type == 'image') {
            return view('www/pages/education/image-articles', compact('article', 'image_url', 'comments'));
        }elseif ($article->article_type == 'pdf') {
            return view('www/pages/education/pdf-articles', compact('article', 'image_url', 'comments'));
        } else {
            return view('www/pages/education/video-articles', compact('article', 'image_url', 'comments'));
        }
    }
}
