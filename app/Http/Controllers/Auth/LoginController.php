<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\CustomerPerson;
use App\Models\Patient;
use App\Models\ProductModel;
use App\Models\ShoppingCartItemModel;
use App\Models\ShoppingCartModel;
use App\Models\TempCartModel;
use App\Models\UnleashedCustomer;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Auth;
use PragmaRX\Google2FALaravel\Support\Authenticator;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    //test
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
//    protected function redirectTo()
//    {
//        return back();
//    }
    protected function validator(array $data)
    {
        return Validator::make($data, [
//            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:customers',
            'password' => 'required|confirmed|min:8',
        ]);
    }

//    protected function validateLogin(Request $request)
//    {
//        $this->validate($request, [
//            $this->username() => [
//                'required',
//                Rule::exists('users')->where(function ($query) use ($request){
//                    $query->where('role_id', $request->all()['role_id']);
//                }),
//            ],
//            'password' => 'required|string|min:6',
//        ]);
//    }
    public function login(Request $request)
    {
//        dd($_SERVER['HTTP_REFERER']);
        session(['previous-url' => $_SERVER['HTTP_REFERER']]);
        $input = $request->all();
        Cookie::queue(Cookie::forget('email'));
        Cookie::queue(Cookie::forget('password'));

        if (Auth::guard('practitioner')->attempt(array('email' => $input['email'], 'password' => $input['password'])) ||
            Auth::guard('patient')->attempt(array('email' => $input['email'], 'password' => $input['password']))) {

            if(Auth::guard('practitioner')->check()){
                if(!Auth::guard('practitioner')->user()->loginSecurity == null && !$request->get('use_backup') == 'yes'){
                    if(!((new \App\Support\Google2FAAuthenticator($request))->checkGoogleTwoFactorExpire())){
                        Cookie::queue('email', $input['email']);
                        Cookie::queue('password', $input['password']);
                        Auth::guard('practitioner')->logout();
                        Auth::guard('patient')->logout();
                        return response()->json(['msg' => '2fa','email' => $input['email'], 'password' => $input['password']]);
                    }
                }
//            }


//            if (Auth::guard('practitioner')->check() && Auth::guard('practitioner')->user()->status != '1') {
//                Auth::guard('practitioner')->logout();
//                return response()->json(['msg' => 'invalid account']);
//            } else if (Auth::guard('patient')->check() && Auth::guard('patient')->user()->status != '1') {
//                Auth::guard('patient')->logout();
//                return response()->json(['msg' => 'invalid account']);
//            }

//            if (Auth::guard('practitioner')->check()) {

                $person_id = Auth::guard('practitioner')->user()->id;

                // Get business associated to the person
                $business_list = CustomerPerson::where('person_id', $person_id)
                    ->join('ucustomers', 'ucustomers.guid', '=', 'customer_id')
                    ->whereNull('customer_persons.deleted_at')
                    ->select('ucustomers.customer_name', 'ucustomers.guid','ucustomers.trading_name')
                    ->get()
                    ->toArray();

                // Count the businesses returned
                if (count($business_list) == 0) {

                    // No business found (its a patient)
                    return response()->json(['msg' => 'patient']);

                } else if (count($business_list) == 1) {

                    // Only 1 business was returned
                    $current_business = current($business_list);
                    $business_guid = $current_business["guid"];

                    // get business detail
                    $business_detail = UnleashedCustomer::where('guid', $business_guid)->first();

                    // not allow to login if business status is obsolete
                    if($business_detail->status == 6){
                        Auth::guard('practitioner')->logout();
                        return response()->json(['msg' => 'obsolete account' , 'name' => $business_detail->trading_name]);
                    }
                    else {
                        // setup the cookie
                        Cookie::queue('business', json_encode($business_detail));

                        // check if have something in cart before login
                        $show_notification = 0;
                        $temp_cart = TempCartModel::GetTempCart($business_detail->guid, $person_id);
                        if ($temp_cart) {
                            if (!($temp_cart->cart_content == '[]')) {
                                $show_notification = 1;
                            }
                        }
                        Cookie::queue('show_notification', $show_notification);

                        return response()->json(['msg' => 'patient']);
                    }

                } else {

                    // // Multiple business returned
                    // $business = CustomerPerson::where('person_id', $person_id)->whereNull('deleted_at')->get();

                    // for ($i = 0; $i < count($business); $i++) {
                    //     $business[$i] = $business[$i]->business()->first();
                    // }

                    return response()->json(['msg' => 'practitioner', 'practitioner' => json_encode($business_list)]);
                }

            } else {
                return response()->json(['msg' => 'patient']);
            }

        } else {
            return response()->json(['msg' => 'wrong']);
        }

    }

    public function logout(Request $request)
    {
        Auth::guard('practitioner')->logout();
        Auth::guard('patient')->logout();
        (new Authenticator(request()))->logout();
//        dd();
        if (Cookie::has('business')) {
            Cookie::queue(Cookie::forget('business'));
        }
//        session_start();
//        session(['practitioner'=>null]);
//        session(['patient'=>null]);
        return redirect()->back();
    }

    public function toSelectPractitioner()
    {
        $business = CustomerPerson::where('person_id', Auth::guard('practitioner')->user()->id)->whereNull('deleted_at')->get();
        return view('www/pages/misc/select_practitioner', compact('business'));
    }

    public function submitPractitioner(Request $request)
    {
        $practitioner = UnleashedCustomer::where('guid', $request->get('practitioner'))->first();
        if($practitioner->status == 6){
            Auth::guard('practitioner')->logout();
            return response()->json(['msg' => 'obsolete account', 'name' => $practitioner->trading_name]);
        }
        else {
            Cookie::queue('business', json_encode($practitioner));
//        $business = json_decode(Cookie::get('business'), true);
            $person = Auth::guard('practitioner')->user();
            $current_cart = ShoppingCartModel::where('customer_id', $practitioner->guid)->where('person_id', $person->id)->where('user_id', 100)->orderBy('created_at', 'desc')->first();

            $amount = 0;
            if ($current_cart) {
                $current_product = ShoppingCartItemModel::where('cart_id', $current_cart->cart_id)->get();
                if ($current_product) {
                    foreach ($current_product as $item) {
                        $amount = $this->additem($item->product_code, $item->quantity);
                    }
                }
            }
//        dd($amount);
            return response()->json(['msg' => 'success', 'amount' => $amount]);
        }
    }

    public function additem($id, $quantity)
    {
        $product = ProductModel::where('product_code', $id)->first();

        if (!$product) {

            abort(404);

        }
        $cart = json_decode(Cookie::get('cart'), true);

        // if cart is empty then this the first product
        if (!Cookie::has('cart')) {
            $cart = [
                $id => [
                    "name" => $product->product_description,
                    "code" => $product->product_code,
                    "quantity" => $quantity,
                    "price" => round($product->sell_price_10 * (1 + $product->tax_rate), 2),
                    "photo" => $product->img_front_view_small
                ]
            ];
            Cookie::queue('cart', json_encode($cart));
            $amount = $quantity;
        } else if (isset($cart[$id])) {
            // if cart not empty then check if this product exist then increment quantity
            $cart[$id]['quantity'] = $cart[$id]['quantity'] + $quantity;
            Cookie::queue('cart', json_encode($cart));
            $amount = array_sum(array_column(json_decode(Cookie::get('cart'), true), 'quantity')) + $quantity;
        } else {
            // if item not exist in cart then add to cart with quantity = 1
            $cart[$id] = [
                "name" => $product->product_description,
                "code" => $product->product_code,
                "quantity" => $quantity,
                "price" => round($product->sell_price_10 * (1 + $product->tax_rate), 2),
                "photo" => $product->img_front_view_small
            ];

            Cookie::queue('cart', json_encode($cart));

            $amount = array_sum(array_column(json_decode(Cookie::get('cart'), true), 'quantity')) + $quantity;
        }
        return $amount;
    }


}
