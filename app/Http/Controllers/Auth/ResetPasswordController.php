<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Password;
use Auth;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected string $redirectTo = '/2fa_verify';

    public function reset(Request $request)
    {
        $request->validate($this->rules(), $this->validationErrorMessages());

        // Here we will attempt to reset the user's password. If it is successful we
        // will update the password on an actual user model and persist it to the
        // database. Otherwise we will parse the error and return the response.
        $response = $this->broker()->reset(
            $this->credentials($request), function ($user, $password) {
            $this->resetPassword($user, $password);
        });
        if ($response != 'passwords.reset') {
            config(['auth.defaults.guard' => 'practitioner']);
            config(['auth.defaults.passwords' => 'practitioners']);
            $response = $this->broker()->reset(
                $this->credentials($request), function ($user, $password) {
                $this->resetPassword($user, $password);
            });
        };
        if ($response != 'passwords.reset') {
            return response()->json(['msg' => 'fail']);
        } else {

            if (!User::where('email', $request->get('email'))->first()->loginSecurity == null) {
                if (!((new \App\Support\Google2FAAuthenticator($request))->checkGoogleTwoFactorExpire())) {
                    Cookie::queue('email', $request->get('email'));
                    Cookie::queue('password', $request->get('password'));
                    Auth::guard('practitioner')->logout();
                    Auth::guard('patient')->logout();
                    return response()->json(['msg' => 'success', 'path' => '/2fa_verify']);
                }

            }
            return response()->json(['msg' => 'success', 'path' => '/', 'email' => $request->get('email'), 'password' => $request->get('password')]);
        }

        // If the password was successfully reset, we will redirect the user back to
        // the application's home authenticated view. If there is an error we can
        // redirect them back to where they came from with their error message.
//        return $response == Password::PASSWORD_RESET
//            ? $this->sendResetResponse($request, $response)
//            : $this->sendResetFailedResponse($request, $response);
    }
}
