<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;
use Auth;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset email and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */
    protected function guard($guard='patient')
    {
        return Auth::guard($guard);
    }
    public function sendResetLinkEmail(Request $request)
    {
//        dd(1);
        $this->validateEmail($request);
        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.
        $response = $this->broker()->sendResetLink(
            $this->credentials($request)
        );
        if($response!='passwords.sent'){
            config(['auth.defaults.guard' => 'practitioner']);
            config(['auth.defaults.passwords' => 'practitioners']);
            $response = $this->broker()->sendResetLink(
                $this->credentials($request)
            );
        };
        if($response!='passwords.sent'){
            return response()->json(['msg'=>'fail']);
        }
        else{
            return response()->json(['msg'=>'success']);
        }
//        return $response == Password::RESET_LINK_SENT
//            ? $this->sendResetLinkResponse($request, $response)
//            : $this->sendResetLinkFailedResponse($request, $response);
    }
    use SendsPasswordResetEmails;
}
