<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Addressbook;
use App\Models\AphraListModel;
use App\Models\ApplicationAddress;
use App\Models\AssociationModel;
use App\Models\CustomerPerson;
use App\Models\InteractionModel;
use App\Models\MediaLibrary;
use App\Models\Modality;
use App\Models\ModalityPerson;
use App\Models\Pages;
use App\Models\Patient;
use App\Models\PatientAddress;
use App\Models\PrimaryChannel;
use App\Models\PrimaryClassification;
use App\Models\SecondaryChannel;
use App\Models\UnleashedCustomer;
use App\Models\User;
use App\Models\WebsiteApplication;
use App\Rules\Nospace;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Auth;
use Illuminate\Support\Str;
use Snowfire\Beautymail\Beautymail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
//    protected function validator(array $data)
//    {
//
//        return Validator::make($data, [
//            'password' => 'required|string|min:8|pwnedpassword|confirmed',
//        ]);
//    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return \App\Models\User
     */

    public function register(Request $request)
    {
//        $this->validator($request->all())->validate();


        event(new Registered($user = $this->create($request->all())));

//        $this->guard('practitioner')->login($user);
//        $this->guard('patient')->login($user);
//        Auth::guard('practitioner')->login($user);
//        Auth::guard('patient')->login($user);

        return $this->registered($request, $user)
            ?: redirect($this->redirectPath());
    }


    protected function create(array $data)
    {
        // ***********************
        // PHARMACY REGISTRATION
        // ***********************
        if ($data['pharma_firstname'])
        {
            if (UnleashedCustomer::where('customer_name', $data['pharma_business_name'])->exists() && $data['pharma_company_name_display'])
            {
                $business = UnleashedCustomer::where('customer_name', $data['pharma_business_name'])->first();
                $application = WebsiteApplication::create([
                    'type'                 => 'Access request',
                    'first_name'           => $data['pharma_firstname'],
                    'last_name'            => $data['pharma_lastname'],
                    'salutation'           => $data['pharma_title'],
                    'email'                => $data['pharma_email'],
                    'customer_code'        => UnleashedCustomer::where('customer_name', $data['pharma_business_name'])->first()->guid,
                    'password'             => Hash::make($data['pharma_password']),
                    'phone'                => $data['pharma_phone'],
                    'mobile'               => $data['pharma_mobile'],
                    'job_title'            => $data['pharma_job_title'],
                    'ahpra_number'         => strtoupper($data['pharma_ahpra_number']),
                    'fax'                  => $business->fax_number,
                    'business_name'        => $data['pharma_business_name'],
                    'business_phone'       => $data['pharma_business_phone'],
                    'primary_channel_id'   => $business->primary_channel()->exists()?$business->primary_channel()->first()->id:null,
                    'second_channel_id'    => $business->secondary_channel()->exists()?$business->secondary_channel()->first()->id:null,
                    'association_number'   => isset($data['pharma_association_number'])?$data['pharma_association_number']:null,
                    'association_name'     => isset($data['pharma_association_name'])?
                        (AssociationModel::where('name', $data['pharma_association_name'])->exists()?AssociationModel::where('name', $data['pharma_association_name'])->first()->id:null):null,
                    'business_type'        => $business->business_type,
                    'business_number'      => $business->gstvat_number,
                    'modality_id'          => isset($data['pharma_primary_profession'])?
                        (Modality::where('name', $data['pharma_primary_profession'])->exists() ? Modality::where('name', $data['pharma_primary_profession'])->first()->id : null):null,
                    'acn'                  => $business->acn,
                    'need_credit'          => $business->need_credit,
                    'is_business_owner'    => $business->is_business_owner,
                    'is_declared_bankrupt' => $business->is_declared_bankrupt,
                    'is_refused_credit'    => $business->is_refused_credit,
                    'status'               => 0,
                    'pre_status'           => 0,
                    'list_in_web'          => $business->list_in_web,
                    'trading_name'         => $business->trading_name,
                    'qualification'        => isset($data['pharma_qualification'])?$data['pharma_qualification']:null,
                    'request_from'         => 'vms',
                    'newsletter'           => isset($data['pharma_newsletter']) ? $data['pharma_newsletter'] : 0,
                ]);

                $sendTo = config('app.website_register_email');
                $headers = 'New Access request';
                $time = "This email was submitted on " . date('m-d-Y');
                $beautymail = app()->make(Beautymail::class);
                $name = $business->customer_name;

                $url = config('app.5i_url') . "/nova/resources/website-applications/" . $application->id;

                $beautymail->send('emails.email-template-business-approve-practitioner-application', ['time' => $time, 'messages' => $application, 'headers' => $headers, 'name' => $name, 'url' => $url, 'business' => $business], function ($message) use ($sendTo, $name) {
                    $message->to($sendTo)->subject('New Access request');
                });
            }
            else
            {
                $application = WebsiteApplication::create([
                    'type'                 => 'New Account request',
                    'first_name'           => $data['pharma_firstname'],
                    'last_name'            => $data['pharma_lastname'],
                    'salutation'           => $data['pharma_title'],
                    'email'                => $data['pharma_email'],
                    'password'             => Hash::make($data['pharma_password']),
                    'phone'                => $data['pharma_phone'],
                    'mobile'               => $data['pharma_mobile'],
                    'job_title'            => $data['pharma_job_title'],
                    'ahpra_number'         => strtoupper($data['pharma_ahpra_number']),
                    'fax'                  => $data['pharma_fax'],
                    'business_name'        => $data['pharma_business_name'],
                    'business_phone'       => $data['pharma_business_phone'],
                    'primary_channel_id'   => PrimaryChannel::where('name', $data['pharma_primary_channel'])->exists() ? PrimaryChannel::where('name', $data['pharma_primary_channel'])->first()->id : null,
                    'second_channel_id'    => SecondaryChannel::where('name', $data['pharma_second_channel'])->exists() ? SecondaryChannel::where('name', $data['pharma_second_channel'])->first()->id : null,
                    'association_number'   => isset($data['pharma_association_number'])?$data['pharma_association_number']:null,
                    'association_name'     => isset($data['pharma_association_name'])?
                        (AssociationModel::where('name', $data['pharma_association_name'])->exists()?AssociationModel::where('name', $data['pharma_association_name'])->first()->id:null):null,
                    'business_type'        => $data['pharma_business_type'],
                    'business_number'      => $data['pharma_business_number'],
                    'modality_id'          => isset($data['pharma_primary_profession'])?
                        (Modality::where('name', $data['pharma_primary_profession'])->exists() ? Modality::where('name', $data['pharma_primary_profession'])->first()->id : null):null,
                    'acn'                  => $data['pharma_acn'],
                    'need_credit'          => isset($data['pharma_credit_application']) ? $data['pharma_credit_application'] : 0,
                    'is_business_owner'    => isset($data['pharma_is_register_owner']) ? $data['pharma_is_register_owner'] : 0,
                    'is_declared_bankrupt' => isset($data['pharma_is_individuals_declare']) ? $data['pharma_is_individuals_declare'] : 0,
                    'is_refused_credit'    => isset($data['pharma_is_ever_refuse']) ? $data['pharma_is_ever_refuse'] : 0,
                    'status'               => 0,
                    'pre_status'           => 0,
                    'list_in_web'          => isset($data['pharma_list_in_web']) ? $data['pharma_list_in_web'] : 0,
                    'is_certified'         => isset($data['pharma_business_certified']) ? $data['pharma_business_certified'] : 0,
                    'classification'       => isset($data['pharma_country'])?
                        (PrimaryClassification::where('name',$data['pharma_country'])->exists()?PrimaryClassification::where('name',$data['pharma_country'])->first()->id : PrimaryClassification::where('name','OTHER')->first()->id):PrimaryClassification::where('name','OTHER')->first()->id,
                    'trading_name'         => $data['pharma_business_trading_name'],
                    'qualification'        => isset($data['pharma_qualification'])?$data['pharma_qualification']:null,
                    'request_from'         => 'vms',
                    'newsletter'           => isset($data['pharma_newsletter']) ? $data['pharma_newsletter'] : 0,
                ]);
                ApplicationAddress::create([
                    'application_id' => $application->id,
                    'address_type'   => 'Physical',
                    'address_line_1' => $data['pharma_street_address'],
                    'address_line_2' => $data['pharma_second_address'],
                    'suburb'         => $data['pharma_locality'],
                    'postcode'       => $data['pharma_postal_code'],
                    'state'          => $data['pharma_administrative_area_level_1'],
                    'country'        => $data['pharma_country'],
                    'longitude'      => $data['pharma_lat'],
                    'latitude'       => $data['pharma_lng'],
                ]);
                ApplicationAddress::create([
                    'application_id' => $application->id,
                    'address_type'   => 'Delivery',
                    'address_line_1' => $data['pharma_street_address'],
                    'address_line_2' => $data['pharma_second_address'],
                    'suburb'         => $data['pharma_locality'],
                    'postcode'       => $data['pharma_postal_code'],
                    'state'          => $data['pharma_administrative_area_level_1'],
                    'country'        => $data['pharma_country'],
                    'longitude'      => $data['pharma_lat'],
                    'latitude'       => $data['pharma_lng'],
                ]);
                ApplicationAddress::create([
                    'application_id' => $application->id,
                    'address_type'   => 'Postal',
                    'address_line_1' => $data['pharma_street_address'],
                    'address_line_2' => $data['pharma_second_address'],
                    'suburb'         => $data['pharma_locality'],
                    'postcode'       => $data['pharma_postal_code'],
                    'state'          => $data['pharma_administrative_area_level_1'],
                    'country'        => $data['pharma_country'],
                    'longitude'      => $data['pharma_lat'],
                    'latitude'       => $data['pharma_lng'],
                ]);
                $sendTo = config('app.website_register_email');
                $headers = 'New Account Application';
                $time = "This email was submitted on " . date('m-d-Y');
                $beautymail = app()->make(Beautymail::class);
                $name = '';
                $url = config('app.5i_url') . "/nova/resources/website-applications/" . $application->id;
                $beautymail->send('emails.email-template-business-application', ['time' => $time, 'application' => $application, 'headers' => $headers, 'name' => $name, 'url' => $url], function ($message) use ($sendTo, $name) {
                    $message->to($sendTo)->subject('New Account Application');
                });
            }
            return true;
        }
        // ***********************
        // PRACTITIONER REGISTRATION
        // ***********************
        elseif ($data['practitioner_firstname'])
        {
            if (UnleashedCustomer::where('customer_name', $data['business_name'])->exists() && $data['practitioner_company_name_display'])
            {
                $business = UnleashedCustomer::where('customer_name', $data['business_name'])->first();
                $application = WebsiteApplication::create([
                    'type'                 => 'Access request',
                    'first_name'           => $data['practitioner_firstname'],
                    'last_name'            => $data['practitioner_lastname'],
                    'salutation'           => $data['practitioner_title'],
                    'email'                => $data['practitioner_email'],
                    'customer_code'        => UnleashedCustomer::where('customer_name', $data['business_name'])->first()->guid,
                    'password'             => Hash::make($data['practitioner_password']),
                    'phone'                => $data['practitioner_phone'],
                    'mobile'               => $data['practitioner_mobile'],
                    'job_title'            => $data['job_title'],
                    'ahpra_number'         => strtoupper($data['practitioner_ahpra_number']),
                    'fax'                  => $business->fax_number,
                    'business_name'        => $data['business_name'],
                    'business_phone'       => $data['business_phone'],
                    'primary_channel_id'   => $business->primary_channel()->exists()?$business->primary_channel()->first()->id:null,
                    'second_channel_id'    => $business->secondary_channel()->exists()?$business->secondary_channel()->first()->id:null,
                    'association_number'   => isset($data['association_number'])?$data['association_number']:null,
                    'association_name'     => isset($data['association_name'])?
                        (AssociationModel::where('name', $data['association_name'])->exists()?AssociationModel::where('name', $data['association_name'])->first()->id:null):null,
                    'business_type'        => $business->business_type,
                    'business_number'      => $business->gstvat_number,
                    'modality_id'          => isset($data['primary_profession'])?
                        (Modality::where('name', $data['primary_profession'])->exists() ? Modality::where('name', $data['primary_profession'])->first()->id : null):null,
                    'acn'                  => $business->acn,
                    'need_credit'          => $business->need_credit,
                    'is_business_owner'    => $business->is_business_owner,
                    'is_declared_bankrupt' => $business->is_declared_bankrupt,
                    'is_refused_credit'    => $business->is_refused_credit,
                    'status'               => 0,
                    'pre_status'           => 0,
                    'list_in_web'          => $business->list_in_web,
                    'trading_name'         => $business->trading_name,
                    'qualification'        => isset($data['practitioner_qualification'])?$data['practitioner_qualification']:null,
                    'request_from'         => 'vms',
                    'newsletter'           => isset($data['practitioner_newsletter']) ? $data['practitioner_newsletter'] : 0,
                ]);

                $sendTo = config('app.website_register_email');
                $headers = 'New Access request';
                $time = "This email was submitted on " . date('m-d-Y');
                $beautymail = app()->make(Beautymail::class);
                $name = $business->customer_name;
                $url = config('app.5i_url') . "/nova/resources/website-applications/" . $application->id;

                $beautymail->send('emails.email-template-business-approve-practitioner-application', ['time' => $time, 'messages' => $application, 'headers' => $headers, 'name' => $name, 'url' => $url, 'business' => $business], function ($message) use ($sendTo, $name) {
                    $message->to($sendTo)->subject('New Access request');
                });
            }
            else
            {
                // dd(1);
                $application = WebsiteApplication::create([
                    'type'                 => 'New Account request',
                    'first_name'           => $data['practitioner_firstname'],
                    'last_name'            => $data['practitioner_lastname'],
                    'salutation'           => $data['practitioner_title'],
                    'email'                => $data['practitioner_email'],
                    'password'             => Hash::make($data['practitioner_password']),
                    'phone'                => $data['practitioner_phone'],
                    'mobile'               => $data['practitioner_mobile'],
                    'job_title'            => $data['job_title'],
                    'business_name'        => $data['business_name'],
                    'business_phone'       => $data['business_phone'],
                    'primary_channel_id'   => PrimaryChannel::where('name', $data['primary_channel'])->exists() ? PrimaryChannel::where('name', $data['primary_channel'])->first()->id : null,
                    'second_channel_id'    => SecondaryChannel::where('name', $data['second_channel'])->exists() ? SecondaryChannel::where('name', $data['second_channel'])->first()->id : null,
                    'association_number'   => isset($data['association_number'])?$data['association_number']:null,
                    'association_name'     => isset($data['association_name'])?
                        (AssociationModel::where('name', $data['association_name'])->exists()?AssociationModel::where('name', $data['association_name'])->first()->id:null):null,
                    'business_type'        => $data['business_type'],
                    'business_number'      => $data['business_number'],
                    'modality_id'          => isset($data['primary_profession'])?
                        (Modality::where('name', $data['primary_profession'])->exists() ? Modality::where('name', $data['primary_profession'])->first()->id : null):null,
                    'acn'                  => $data['acn'],
                    'need_credit'          => isset($data['credit_application']) ? $data['credit_application'] : 0,
                    'is_business_owner'    => isset($data['is_register_owner']) ? $data['is_register_owner'] : 0,
                    'is_declared_bankrupt' => isset($data['is_individuals_declare']) ? $data['is_individuals_declare'] : 0,
                    'is_refused_credit'    => isset($data['is_ever_refuse']) ? $data['is_ever_refuse'] : 0,
                    'status'               => 0,
                    'pre_status'           => 0,
                    'ahpra_number'         => strtoupper($data['practitioner_ahpra_number']),
                    'list_in_web'          => isset($data['practitioner_list_in_web']) ? $data['practitioner_list_in_web'] : 0,
                    'classification'       => isset($data['practitioner_country'])?
                        (PrimaryClassification::where('name',$data['practitioner_country'])->exists()?PrimaryClassification::where('name',$data['practitioner_country'])->first()->id : PrimaryClassification::where('name','OTHER')->first()->id):PrimaryClassification::where('name','OTHER')->first()->id,
                    'qualification'        => isset($data['practitioner_qualification'])?$data['practitioner_qualification']:null,
                    'trading_name'         => $data['business_trading_name'],
                    'request_from'         => 'vms',
                    'newsletter'           => isset($data['practitioner_newsletter']) ? $data['practitioner_newsletter'] : 0,
                    // 'fax'                  => $data['fax'],
                    // 'brand'                => $data['brand'],
                ]);
                ApplicationAddress::create([
                    'application_id' => $application->id,
                    'address_type'   => 'Physical',
                    'address_line_1' => $data['practitioner_street_address'],
                    'address_line_2' => $data['practitioner_second_address'],
                    'suburb'         => $data['practitioner_locality'],
                    'postcode'       => $data['practitioner_postal_code'],
                    'state'          => $data['practitioner_administrative_area_level_1'],
                    'country'        => $data['practitioner_country'],
                    'longitude'      => $data['practitioner_lat'],
                    'latitude'       => $data['practitioner_lng'],
                ]);
                ApplicationAddress::create([
                    'application_id' => $application->id,
                    'address_type'   => 'Delivery',
                    'address_line_1' => $data['practitioner_street_address'],
                    'address_line_2' => $data['practitioner_second_address'],
                    'suburb'         => $data['practitioner_locality'],
                    'postcode'       => $data['practitioner_postal_code'],
                    'state'          => $data['practitioner_administrative_area_level_1'],
                    'country'        => $data['practitioner_country'],
                    'longitude'      => $data['practitioner_lat'],
                    'latitude'       => $data['practitioner_lng'],
                ]);
                ApplicationAddress::create([
                    'application_id' => $application->id,
                    'address_type'   => 'Postal',
                    'address_line_1' => $data['practitioner_street_address'],
                    'address_line_2' => $data['practitioner_second_address'],
                    'suburb'         => $data['practitioner_locality'],
                    'postcode'       => $data['practitioner_postal_code'],
                    'state'          => $data['practitioner_administrative_area_level_1'],
                    'country'        => $data['practitioner_country'],
                    'longitude'      => $data['practitioner_lat'],
                    'latitude'       => $data['practitioner_lng'],
                ]);

                $sendTo = config('app.website_register_email');
                $headers = 'New Account Application';
                $time = "This email was submitted on " . date('m-d-Y');
                $beautymail = app()->make(Beautymail::class);
                $name = '';
                $url = config('app.5i_url') . "/nova/resources/website-applications/" . $application->id;
                $beautymail->send('emails.email-template-business-application', ['time' => $time, 'application' => $application, 'headers' => $headers, 'name' => $name, 'url' => $url], function ($message) use ($sendTo, $name) {
                    $message->to($sendTo)->subject('New Account Application');
                });
            }
            return true;
        }
        // ***********************
        // Patient
        // ***********************
        else
        {
            $user = Patient::create([
                'salutation' => $data['patient_title'],
                'first_name' => $data['patient_firstname'],
                'last_name' => $data['patient_lastname'],
                'email' => $data['patient_email'],
                'password' => Hash::make($data['patient_password']),
                'phone' => $data['patient_phone'],
                'mobile' => $data['patient_mobile'],
                'status' => '1',
                'pre_status' => '1',
                'guid' => $data['referral_code'] ? UnleashedCustomer::where('referral_code', $data['referral_code'])->first()->guid : '6965be5b-4a95-4b76-8521-0e00b036885d',
            ]);
            PatientAddress::create([
                'patient_id' => $user->id,
                'address_line_1' => $data['patient_street_address'],
                'address_line_2' => $data['patient_second_address'],
                'suburb' => $data['patient_locality'],
                'postcode' => $data['patient_postal_code'],
                'state' => $data['patient_administrative_area_level_1'],
                'country' => $data['patient_country'],
            ]);
            InteractionModel::create([
                'customer_id' => $user->guid,
                'notes' => 'Patient ' . $user->salutation . ' ' . $user->first_name . ' ' . $user->last_name . ' linked to Practitioner',
                'interaction_type' => 'Website',
                'direction' => 'Incomming',
                'purpose' => 'Accounts'

            ]);
            Auth::guard('patient')->login($user);
            return $user;
        }

    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        $primary_channel = PrimaryChannel::all();
//        $second_channel = SecondaryChannel::all();
        $modality = Modality::all();
        $association = AssociationModel::all();
        $image_url = config("app.image_url");
        $content = Pages::where('page', 'register')->where('website', 'medlab')->first();
        return view('auth.register', compact('primary_channel', 'modality', 'association', 'content', 'image_url'));
    }

    public function getChannel(Request $request)
    {
        $parent = $request->get('parent');
        $second_channel = array_column(PrimaryChannel::where('name', $parent)->first()->channels()->get()->toArray(), 'name');

        return response()->json(['msg' => 'Successfully!', 'data' => $second_channel]);
    }

    public function getAssociation(Request $request)
    {
        $country = $request->get('country');
        if (PrimaryClassification::where('name', $country)->exists()) {
            $association = array_column(PrimaryClassification::where('name', $country)->first()->association()->get()->toArray(), 'name');
        } else {
            $association = [];
        }
        return response()->json(['msg' => 'Successfully!', 'data' => $association]);
    }

    public function validateEmail(Request $request)
    {
        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', config('app.neverbounce_api_url'), ['query' => [
            'key' => config('app.neverbounce_access_key'),
            'email' => $request->get('email'),
        ]]);
        $response = json_decode($response->getBody(), true);
        if(isset($response['status'])){
            if($response['result']!='invalid'){
                if (User::where('email', $request->get('email'))->exists() || Patient::where('email', $request->get('email'))->exists()) {
                    return response()->json(['code' => '0', 'email' => $request->get('email')]);
                } else {
                    return response()->json(['code' => '1', 'email' => $request->get('email')]);
                }
            }else{
                return response()->json(['code' => '2', 'email' => $request->get('email')]);
            }
        }else{
            return response()->json(['code' => '3', 'email' => $request->get('email')]);
        }




    }

    public function validateReferralCode(Request $request)
    {
        $code = $request->get('code');
        if ($code) {
            if (UnleashedCustomer::where('referral_code', $code)->exists()) {
                return response()->json(['code' => '1', 'top' => UnleashedCustomer::where('referral_code', $code)->first()->customer_name]);
            } else {
                return response()->json(['code' => '0']);
            }
        } else {
            return response()->json(['code' => '2']);
        }
    }

    public function validateABN(Request $request)
    {
        $abn = $request->get('abn');
        if ($abn) {
            if (UnleashedCustomer::where('gstvat_number', $abn)->exists()) {
                $business = UnleashedCustomer::where('gstvat_number', $abn)->get();
                foreach ($business as $item) {
                    $item->address = $item->address()->first();
                }
                return response()->json(['code' => '1', 'business' => json_encode($business), 'abn' => $abn]);
            }
        } else {
            return response()->json(['code' => '0']);
        }
    }
    public function validateACN(Request $request)
    {
        $acn = $request->get('acn');
        if ($acn) {
            if (UnleashedCustomer::where('acn', $acn)->exists()) {
                $business = UnleashedCustomer::where('acn', $acn)->get();
                foreach ($business as $item) {
                    $item->address = $item->address()->first();
                }
                return response()->json(['code' => '1', 'business' => json_encode($business), 'acn' => $acn]);
            }
        } else {
            return response()->json(['code' => '0']);
        }
    }

    public function validatePhone(Request $request)
    {
        $phone = $request->get('phone');
        if ($phone) {
            if (UnleashedCustomer::where('phone_number', $phone)->exists()) {
                $business = UnleashedCustomer::where('phone_number', $phone)->get();
                foreach ($business as $item) {
                    $item->address = $item->address()->first();
                }
                return response()->json(['code' => '1', 'business' => json_encode($business), 'phone' => $phone]);
            }
        } else {
            return response()->json(['code' => '0']);
        }
    }

    public function validateAddress(Request $request)
    {
        $address = $request->get('address');
        if ($address) {
            if (Addressbook::where('address_line_1', $address)->exists()) {
                $business = UnleashedCustomer::whereHas('address', function ($q) use ($address) {
                    $q->where('address_line_1', $address);
                })->get();
                if(count($business)){
                    foreach ($business as $item) {
                        $item->address = $item->address()->first();
                    }
                    return response()->json(['code' => '1', 'business' => json_encode($business), 'address' => $address]);
                }
            }
        } else {
            return response()->json(['code' => '0']);
        }
    }

    public function validateUpdateAddress(Request $request)
    {
        $address = $request->get('address');
        if ($address) {
            if (Addressbook::where('address_line_1', $address)->exists()) {
                $pre_address = json_encode(Addressbook::where('customer_code', Auth::guard('practitioner')->user()->practitioner()->first()->guid)->first());
//                $business_address = json_encode(Addressbook::where('customer_code', Addressbook::where('address_line_1', $address)->first()->customer_code)->first());
                return response()->json(['code' => '1', 'pre_address' => $pre_address, 'business' => json_encode(UnleashedCustomer::where('guid', Addressbook::where('address_line_1', $address)->first()->customer_code)->first())]);
            }
        } else {
            return response()->json(['code' => '0']);
        }
    }

    public function validateFax(Request $request)
    {
        $fax = $request->get('fax');
        if ($fax) {
            if (UnleashedCustomer::where('fax_number', $fax)->exists()) {
                $business = UnleashedCustomer::where('fax_number', $fax)->get();
                foreach ($business as $item) {
                    $item->address = $item->address()->first();
                }
                return response()->json(['code' => '1', 'business' => json_encode($business), 'fax' => $fax]);
            }
        } else {
            return response()->json(['code' => '0']);
        }
    }

    public function validateUpdateFax(Request $request)
    {
        $fax = $request->get('fax');
        if ($fax) {
            if (UnleashedCustomer::where('fax_number', $fax)->exists()) {
                $pre_fax = json_encode(UnleashedCustomer::where('guid', Auth::guard('practitioner')->user()->practitioner()->first()->guid)->first());
                return response()->json(['code' => '1', 'pre_fax' => $pre_fax, 'business' => json_encode(UnleashedCustomer::where('fax_number', $fax)->first())]);
            }
        } else {
            return response()->json(['code' => '0']);
        }
    }

    public function validateUpdatePhone(Request $request)
    {
        $phone = $request->get('phone');
        if ($phone) {
            if (UnleashedCustomer::where('phone_number', $phone)->exists()) {
                $pre_phone = json_encode(UnleashedCustomer::where('guid', Auth::guard('practitioner')->user()->practitioner()->first()->guid)->first());
                return response()->json(['code' => '1', 'pre_phone' => $pre_phone, 'business' => json_encode(UnleashedCustomer::where('phone_number', $phone)->first())]);
            }
        } else {
            return response()->json(['code' => '0']);
        }
    }

    public function validateUpdateReferralCode(Request $request)
    {
        $referral_code = $request->get('referral_code');
        if ($referral_code) {
            if (Auth::guard('practitioner')->user()->practitioner()->first()->referral_code == $referral_code) {
                return response()->json(['code' => '1']);
            } else {
                return response()->json(['code' => '0']);
            }
        }
    }

    public function getParentChannel(Request $request)
    {
        $child = $request->get('child');
        $channel = array_column(SecondaryChannel::where('name', $child)->first()->parent()->get()->toArray(), 'name');

        return response()->json(['msg' => 'Successfully!', 'data' => $channel]);
    }

    public function searchCompanyName(Request $request)
    {
        $name = $request->get('name');
        $guid = env('ABN_LOOKUP_ID', '36973b2d-8599-4f5f-8eb4-86415ba45382');
        $client = new \GuzzleHttp\Client();
        $request = $client->get('https://abr.business.gov.au/json/MatchingNames.aspx?name=' . $name . '&guid=' . $guid . '');
        $response = substr($request->getBody()->getContents(), 9, -1);
        return response()->json(['msg' => 'Successfully!', 'data' => $response]);
    }

    public function searchBusiness(Request $request)
    {
        $value = $request->get('value');
        $nospaceValue = str_replace(' ','',$value);
        $guid = env('ABN_LOOKUP_ID', '36973b2d-8599-4f5f-8eb4-86415ba45382');
        $client = new \GuzzleHttp\Client();
        if (is_numeric($nospaceValue) && strlen($nospaceValue) == 11) {
            $request = $client->get('https://abr.business.gov.au/json/AbnDetails.aspx?abn=' . $nospaceValue . '&guid=' . $guid . '');
            $response = json_decode(substr($request->getBody()->getContents(), 9, -1));
            $data = json_encode(array(
                'name' => $response->EntityName,
                'abn' => $response->Abn,
                'acn' => $response->Acn,
                'business_type' => 'Entity',
            ),true);
        } elseif (is_numeric($nospaceValue) && strlen($nospaceValue) == 9) {
            $request = $client->get('https://abr.business.gov.au/json/AcnDetails.aspx?acn=' . $nospaceValue . '&guid=' . $guid . '');
            $response = json_decode(substr($request->getBody()->getContents(), 9, -1));
            $data = json_encode(array(
                'name' => $response->EntityName,
                'abn' => $response->Abn,
                'acn' => $response->Acn,
                'business_type' => 'Entity',
            ),true);
        } elseif(strlen($value)==0){
            $data = json_encode(array(
                'name' => '',
                'abn' => '',
                'acn' => '',
                'business_type' => 'Entity',
            ),true);
        }
        else {
            $request = $client->get('https://abr.business.gov.au/json/MatchingNames.aspx?name=' . $value . '&guid=' . $guid . '');
            $data = substr($request->getBody()->getContents(), 9, -1);
        }


        return response()->json(['msg' => 'Successfully!', 'data' => $data]);
    }
    public function selectBusiness(Request $request)
    {
        if($request->get('value')!=''){
            $abn = explode(' ',$request->get('value'))[0];
            $guid = env('ABN_LOOKUP_ID', '36973b2d-8599-4f5f-8eb4-86415ba45382');
            $client = new \GuzzleHttp\Client();
            $request = $client->get('https://abr.business.gov.au/json/AbnDetails.aspx?abn=' . $abn . '&guid=' . $guid . '');
            $response = json_decode(substr($request->getBody()->getContents(), 9, -1));
            $data = json_encode(array(
                'name' => $response->EntityName,
                'abn' => $response->Abn,
                'acn' => $response->Acn,
                'business_type' => 'Entity',
            ),true);
        }else{
            $data = json_encode(array(
                'name' => '',
                'abn' => '',
                'acn' => '',
                'business_type' => 'Entity',
            ),true);
        }

        return response()->json(['msg' => 'Successfully!', 'data' => $data]);
    }
    public function validateAHPRA(Request $request)
    {
        $number = $request->get('number');
        if(preg_match('/^[A-Za-z]{3}+[0-9]{10}$/',$number)){
            if(in_array(strtoupper(substr($number,0,3)),array_column(AphraListModel::whereNull('deleted_at')->get()->toArray(),'prefix'))){
                if (User::where('ahpra_number', strtoupper($number))->exists() && $number != '') {
                    $person = User::where('ahpra_number', $number)->first();
                    $name = $person->first_name.' '.$person->last_name;
                    return response()->json(['code' => '0', 'number' => $number, 'name' => $name]);
                } else {
                    return response()->json(['code' => '1', 'number' => $number]);
                }
            }
            else{
                return response()->json(['code' => '2']);
            }
        }else{
            return response()->json(['code' => '2']);
        }


    }
    public function validateTradingName(Request $request)
    {
        $name = $request->get('name');
        if ($name) {
            if (UnleashedCustomer::where('trading_name', $name)->exists()) {
//                $pre_phone = json_encode(UnleashedCustomer::where('guid', Auth::guard('practitioner')->user()->practitioner()->first()->guid)->first());
                return response()->json(['code' => '1','name' => $name]);
            }
        } else {
            return response()->json(['code' => '0']);
        }

    }

    public function validatePhoneExistence(Request $request)
    {
        $phone = $request->get('phone');
        $type  = $request->get('type');
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', config('app.numverify_api_url'), ['query' => [
            'access_key' => config('app.numverify_access_key'),
            'number' => $phone,
        ]]);
        $response = json_decode($response->getBody(), true);
        if(($type=='landline' && $response['valid']=='true') || ($type=='mobile' && $response['valid']=='true' && $response['line_type']==$type)){
            return response()->json(['code' => '1','message' => 'valid and match']);
        }else{
            return response()->json(['code' => '2','message' => 'invalid']);
        }
    }
    public function validatePassword(Request $request)
    {
        $password = $request->get('password');
        $prefix = substr(strtoupper(sha1($password)),0,5);
        $suffix = substr(strtoupper(sha1($password)),5);
        $full_list = explode("\r\n",$this->callAPI('GET','https://api.pwnedpasswords.com/range/'.$prefix,[]));
        foreach ($full_list as $key => $value){
            $full_list[$key] = explode(':',$value);
        }
        $found_key = array_search($suffix, array_column($full_list, 0));
        if($found_key){
            $number = $full_list[$found_key][1];
        }else{
            $number = 0;
        }
            return response()->json(['number' => $number]);

    }
    function callAPI($method, $url, $data){
        $curl = curl_init();
        switch ($method){
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }
        // OPTIONS:
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
        ));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        // EXECUTE:
        $result = curl_exec($curl);
        if(!$result){die("Connection Failure");}
        curl_close($curl);
        return $result;
    }

}
