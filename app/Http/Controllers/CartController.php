<?php

namespace App\Http\Controllers;

use App\Models\Addressbook;
use App\Models\DietaryModel;
use App\Models\InteractionModel;
use App\Models\MediaLibrary;
use App\Models\OrderLinesModel;
use App\Models\Pages;
use App\Models\PdfTypes;
use App\Models\ProductFamilyGroupModel;
use App\Models\ProductFamilyModel;
use App\Models\ProductGroupModel;
use App\Models\ProductModel;
use App\Models\ProductPdfModel;
use App\Models\ShoppingCartItemModel;
use App\Models\ShoppingCartModel;
use App\Models\ShoppingCartPaymentModel;
use App\Models\ShoppingCartStoreCardModel;
use App\Models\TempCartModel;
use App\Models\UnleashedCustomer;
use App\Securepay\Payment;
use App\Unleashed\Orders;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;
use NunoMaduro\Collision\Provider;
use function GuzzleHttp\Promise\all;

class CartController extends Controller
{
    private $business_guid = NULL;
    private $business_code = NULL;
    private $user_id = NULL;
    private $user_fname = NULL;
    private $user_lname = NULL;
    private $shipping_cost = NULL;
    private $esky_cost = NULL;
    private $shipping_limit = NULL;
    private $esky_limit = NULL;
    private $ip_address = NULL;

    public function __construct()
    {
        // setup fields here
    }

    public function setupSessionVariables()
    {
        // Default setup
        $this->shipping_cost = 10;
        $this->esky_cost = 5;
        $this->shipping_limit = 165;
        $this->esky_limit = 6;

        // Get IP Address
        $this->ip_address = \Request::ip();
        $random_personid = time();

        // Setup the person
        $person = Auth::guard('practitioner')->user();

        // If no cookies founds create / setup 1
        if ($person) {
            $this->user_id = $person->id;
            $this->user_fname = $person->first_name;
            $this->user_lname = $person->last_name;

            // Obtain via cookies
            $entity = json_decode(Cookie::get('business'), true);

            // If no cookies founds create / setup 1
            if ($entity) {
                $this->business_guid = $entity['guid'];
                $this->business_code = $entity['customer_code'];
            } else {
                // redirect to the checkout becaue this cannot happen
                Redirect::to('/login')->send();
            }
        } else {
            // Obtain via cookies for guest checkout
            $temp_cookie = json_decode(Cookie::get('guest_checkout'), true);
            if ($temp_cookie) {

                // Guest checkout
                $this->business_guid = $temp_cookie["business_guid"];
                $this->business_code = $temp_cookie["business_code"];

                // Guest checkout
                $this->user_id = $temp_cookie["user_id"];
                $this->user_fname = $temp_cookie["user_fname"];
                $this->user_lname = $temp_cookie["user_lname"];

            } else {

                // Guest checkout
                $this->business_guid = (string)Str::uuid();
                $this->business_code = 'ABC123';
                $this->user_id = $random_personid;
                $this->user_fname = "Guest";
                $this->user_lname = "Checkout";

                $guest_checkout["business_guid"] = $this->business_guid;
                $guest_checkout["business_code"] = $this->business_code;
                $guest_checkout["user_id"] = $this->user_id;
                $guest_checkout["user_fname"] = $this->user_fname;
                $guest_checkout["user_lname"] = $this->user_lname;

                // Setup the cookie
                Cookie::queue('guest_checkout', json_encode($guest_checkout));
            }
        }
    }

    public function index()
    {
        $this->setupSessionVariables();

        $view_data = array('cart');
        $cart = array();
        $addressbook = null;
        $stored_cc_card = null;

        $temp_cart = $this->getCart();
        $show_notification = 0;
        if ($temp_cart) {
            // Guest checkout do not have such items
            if ($this->business_code != 'ABC123') {
                $addressbook = UnleashedCustomer::where('guid', $this->business_guid)->first()->address()->where('address_type', 'Delivery')->get();
                $stored_cc_card = ShoppingCartStoreCardModel::where('customer_code', $this->business_code)->get();
            }

            $cart = json_decode($temp_cart->cart_content, true);

            $temp_cart_id = $temp_cart->id;
            $total_exgst = $this->getCartTotalExGst($cart);
            $total = $this->getCartTotal($cart);
            $total_gst = $this->getCartGstTotal($cart);
            $total_esky_exgst = $this->getCartEskyExGst($cart);
            $total_shipping_exgst = $this->getCartShippingExGst($cart);
            $total_cold_items = $this->getCartEskyCount($cart);
            $first_name = $this->user_fname;
            $last_name = $this->user_lname;

            if (Cookie::has('show_notification')) {
                if (Cookie::get('show_notification') == 1) {
                    $show_notification = 1;
                }
                Cookie::queue(Cookie::forget('show_notification'));
            }

            $view_data = array('addressbook', 'cart', 'stored_cc_card', 'total_exgst', 'total_shipping_exgst', 'total', 'total_gst', 'total_esky_exgst', 'total_cold_items', 'temp_cart_id', 'first_name', 'last_name');
        }


        return view('vms/pages/cart/cart', compact($view_data, 'show_notification'));
    }

    public function clearCart()
    {
        $this->setupSessionVariables();

        if (Auth::guard('practitioner')->check()) {
            // CLear the cart
            $result = TempCartModel::DeleteTempCart($this->business_guid);

            // return response
            return response()->json(['msg' => 'Cart Cleared']);
        }
    }

    public function addCart(Request $request)
    {
        $this->setupSessionVariables();

        $id = $request->get('id');
        $quantity = $request->get('quantity');

        $product = ProductModel::CartGetProduct($id);

        // Is this a valid product?
        if (!$product) {
            return response()->json(['msg' => 'Invalid Product Added.', 'amount' => 0]);
        }

        // We will always have a cart now (authenticated practitioner or guest checkout)
        $cart_row = $this->getOrCreateCart();
        if ($cart_row) {
            $cart_items = array();
            if (!empty($cart_row["cart_content"])) {
                $cart_items = json_decode($cart_row["cart_content"], true);
            }

            // If item exist increment number
            if (in_array($product["product_guid"], array_column($cart_items, 'product_guid'))) {
                $cart_items[array_search($product["product_guid"], array_column($cart_items, 'product_guid'))]['cart_quantity'] += $quantity;
            } else {
                // If item exist increment number
                $cart_items[] = array(
                    'product_description' => $product["product_description"],
                    'product_group' => $product["product_group"],
                    'product_brand' => $product["product_brand"],
                    'product_code' => $product["product_code"],
                    'product_cost' => $product["product_cost"],
                    'product_rrp' => $product["product_rrp"],
                    'product_tax_rate' => $product["product_tax_rate"],
                    'product_guid' => $product["product_guid"],
                    'product_ship_cold' => $product["product_ship_cold"],
                    'product_store_cold' => $product["product_store_cold"],
                    'cart_quantity' => $quantity,
                    'product_batchnumber' => $product["product_batchnumber"],
                    'product_expiry' => $product["product_expiry"]
                );
            }

            // push it back to the database.
            TempCartModel::UpdateTempCart($cart_row["id"], json_encode($cart_items));

            return response()->json(['msg' => 'Product added to cart successfully!', 'amount' => $this->getCartTotalCountQuantity($cart_items)]);
        }

        return response()->json(['msg' => 'Unable to create a shopping cart. Please try again later', 'amount' => 0]);

    }

    public function updateCart(Request $request)
    {
        $this->setupSessionVariables();

        // get the cart id
        $cartId = $request->cartId;
        $cart_row = $this->getCart($cartId);
        $cart = json_decode($cart_row->cart_content, true);

        // Variable
        $itemId = $request->itemId;
        $itemQty = $request->itemQty;

        // Check logic
        if ($itemQty == 0) {
            // no such thing as 0 items
            unset($cart[$itemId]);
        } else {
            // update the relevent product
            $cart[$itemId]['cart_quantity'] = $itemQty;
        }

        // push back into the database
        $json_cart = json_encode($cart);
        TempCartModel::UpdateTempCart($cartId, $json_cart);

        // prepare the variables
        $total_exgst = $this->getCartTotalExGst($cart);
        $total = $this->getCartTotal($cart);
        $total_gst = $this->getCartGstTotal($cart);
        $total_esky_exgst = $this->getCartEskyExGst($cart);
        $total_shipping_exgst = $this->getCartShippingExGst($cart);
        $total_cold_items = $this->getCartEskyCount($cart);
        $total_count_item = $this->getCartTotalCountQuantity($cart);

        $view_data = array('cart', 'total_exgst', 'total', 'total_gst', 'total_esky_exgst', 'total_shipping_exgst', 'total_cold_items');
        $htmlCart = view('vms/pages/cart/cart_table', compact($view_data))->render();

        return response()->json(['html' => $htmlCart, 'count' => $total_count_item]);
    }

    public function removeCart(Request $request)
    {
        $this->setupSessionVariables();

        // get the cart id
        $cartId = $request->cartId;
        $removeRowId = $request->removeRowId;
        $cart_row = $this->getCart($cartId);
        $cart = json_decode($cart_row->cart_content, true);

        // remove the sale product
        unset($cart[$removeRowId]);

        $cart = array_values($cart);
        // push back into the database
        $json_cart = json_encode($cart);
        TempCartModel::UpdateTempCart($cartId, $json_cart);

        // prepare the variables
        $total_exgst = $this->getCartTotalExGst($cart);
        $total = $this->getCartTotal($cart);
        $total_gst = $this->getCartGstTotal($cart);
        $total_esky_exgst = $this->getCartEskyExGst($cart);
        $total_shipping_exgst = $this->getCartShippingExGst($cart);
        $total_cold_items = $this->getCartEskyCount($cart);
        $total_count_item = $this->getCartTotalCountQuantity($cart);

        $view_data = array('cart', 'total_exgst', 'total', 'total_gst', 'total_esky_exgst', 'total_shipping_exgst', 'total_cold_items');
        $htmlCart = view('vms/pages/cart/cart_table', compact($view_data))->render();

        return response()->json(['html' => $htmlCart, 'count' => $total_count_item]);
    }

    public function checkoutCart(Request $request)
    {
        $this->setupSessionVariables();

        if (Auth::guard('practitioner')->check()) {
            // Get the temp cart id (assuming if multiple contacts of the same business is logged in)
            $temp_cart_id = $request->get('temp_cart_id');

            // Get the cart contents here.
            $cart_row = $this->getCart($temp_cart_id);
            $cart_content = json_decode($cart_row->cart_content, true);

            // Has the order been placed already?
            if (!$cart_content) {
                $recentOrder = ShoppingCartModel::where('customer_id', $this->business_guid)->where('temp_cart_id', $temp_cart_id)->orderby('cart_id', 'desc')->first();
                if ($recentOrder) {
                    return response()->json(['msg' => 'Successfully', 'cart_id' => $recentOrder->cart_id]);
                }

            }

            // Obtain the totals
            $cart_shipping = $this->getCartShippingExGst($cart_content);
            $cart_line_total = $this->getCartTotalExGst($cart_content);
            $cart_esky = $this->getCartEskyExGst($cart_content);
            $cart_esky_count = $this->getCartEskyCount($cart_content);
            $cart_total_tax = $this->getCartGstTotal($cart_content);

            // Stored delivery address
            if ($request->get('use_delivery')) {
                $address = Addressbook::where('id', $request->get('use_delivery'))->first();
                $delivery_address = $address->address_line_1 . '::' . $address->address_line_2 . '::' . $address->suburb . '::' . $address->postcode . '::' . $address->state . '::' . $address->country;
            } else {
                $delivery_address = $request->get('address_line_1') . '::' . $request->get('address_line_2') . '::' . $request->get('suburb') . '::' . $request->get('postcode') . '::' . $request->get('state') . '::' . $request->get('country');

                // Store address
                Addressbook::create([
                    'customer_code' => $this->business_guid,
                    'address_type' => 'Delivery',
                    'address_line_1' => $request->get('address_line_1'),
                    'address_line_2' => $request->get('address_line_2'),
                    'suburb' => $request->get('suburb'),
                    'postcode' => $request->get('postcode'),
                    'state' => $request->get('state'),
                    'country' => $request->get('country'),
                    'delivery_notes' => $request->get('delivery_notes'),
                ]);
            }

            $cart = ShoppingCartModel::create([
                'customer_id' => $this->business_guid,
                'person_id' => $this->user_id,
                'user_id' => '1060',
                'cart_type' => 'web',
                'address_id' => $request->get('use_delivery') ? $request->get('use_delivery') : null,
                'cart_delivery_address' => $delivery_address,
                'cart_delivery_contact' => $request->get('firstname') . ' ' . $request->get('lastname'),
                'cart_line_total' => $cart_line_total,
                'cart_shipping' => $cart_shipping,
                'cart_total_tax' => $cart_total_tax,
                'status' => 1,
                'cart_esky' => $cart_esky,
                'cart_cold_items' => $cart_esky_count,
                'cart_delivery_notes' => $request->get('delivery_notes'),
                'cart_orderno' => $request->get('po_number'),
                'temp_cart_id' => $request->get('temp_cart_id'),
            ]);

            /**
             * Traverse the cart to obtain each item and store into cart row.
             * (this should use a session instead of physical table)
             */
            foreach ($cart_content as $item) {
                ShoppingCartItemModel::create([
                    'cart_id' => $cart->cart_id,
                    'product_guid' => $item["product_guid"],
                    'product_code' => $item["product_code"],
                    'quantity' => $item['cart_quantity'],
                    'unit_price' => $item['product_cost'],
                    'discount' => 0,
                    'discount_rate' => 0,
                    'batch_number' => $item['product_batchnumber'],
                    'tax_rate' => $item['product_tax_rate'],
                    'line_tax' => ($item['product_cost'] * $item['product_tax_rate']) * $item['cart_quantity'],
                    'line_total' => ($item['product_cost'] * $item['cart_quantity']),
                ]);

            }

            $payment_data = [
                'cart_id' => $cart->cart_id,
                'total_paid' => $this->getCartTotal($cart_content),
                'securepay_reference' => '',
                'selected_wholesaler' => '',
                'payment_method' => $request->pay_method,
                'stored_card_id' => $request->stored_card_id,
                'card_number' => $request->card_number,
                'card_expiry_month' => $request->card_expiry_month,
                'card_expiry_year' => $request->card_expiry_year,
                'card_cvv' => $request->card_cvv,
                'card_name' => $request->card_name,
                'save_card' => $request->save_card,
            ];

            // Process the payment
            $payment_response = $this->preparePayment($payment_data);

            $payment_response["cart_id"] = $cart->cart_id;
            $payment_response["securepay_reference"] = '';
            $payment_response["total_paid"] = $this->getCartTotal($cart_content);

            unset($payment_response["card_name"]);
            unset($payment_response["card_cvv"]);
            unset($payment_response["card_expiry_month"]);
            unset($payment_response["card_expiry_year"]);
            unset($payment_response["card_save"]);
            unset($payment_response["stored_card_id"]);
            unset($payment_response["payment_method"]);
            unset($payment_response["save_card"]);

            $newpayment = ShoppingCartPaymentModel::create($payment_response);

            if ($newpayment->status == 'Approved' || $newpayment->status == 'Account Terms') {
                TempCartModel::DeleteTempCart($this->business_guid);
                InteractionModel::create([
                    'user_id' => '62',
                    'customer_id' => $this->business_guid,
                    'interaction_type' => 'Website',
                    'person_id' => $this->user_id,
                    'notes' => 'Order from VMS Website',
                    'scheduled_datetime' => date("Y-m-d H:i:s"),
                    'purpose' => 'Order',
                    'send_email' => 0,
                    'order_placed' => 1,
                    'direction' => 'Incoming',
                    'brand_id' => '2',
                    'cart_id' => $cart->cart_id,
                ]);


                // Send the order to unleashed
                Orders::pushCart($cart->cart_id, $newpayment->status);

                // Return a response.
                return response()->json(['msg' => 'Successfully', 'cart_id' => $cart->cart_id]);
            }

            return response()->json(['msg' => 'Payment Failed ' . $newpayment->status, 'cart_id' => $cart->cart_id]);
        }

        return response()->json(['msg' => 'Your not logged in']);
    }

    public function preparePayment($payment_request)
    {
        $this->setupSessionVariables();

        $payment_request['securepay_reference'] = ShoppingCartModel::GetSecurepayReferenceByCartId($payment_request['cart_id']);

        // Setup variables
        $xml_body = '';
        $receipt_number = '0';
        $card_number = '';
        $card_expiry = '';
        $status = '';

        // Round the total paid value
        $payment_request['total_paid'] = round($payment_request['total_paid'], 2);

        if ($payment_request['payment_method'] == 2) {
            $amount = $payment_request['total_paid'] * 100;
            $reference = $payment_request['securepay_reference'];
            $saved_card_id = $payment_request['stored_card_id'];

            $process_result = Payment::processStoredCard($saved_card_id, $reference, $amount);

            $status = $process_result["status"];
            $card_number = '2222';
            $card_expiry = '22/22';
            $receipt_number = 'Stored Credit Card';
            $xml_body = 'Stored Credit Card';

        } else if ($payment_request['payment_method'] == 0) {
            $status = 'Account Terms';
            $card_number = '0000';
            $card_expiry = '00/00';
            $receipt_number = 'Account Terms';
            $xml_body = 'Account Terms';
        } else {
            $payment_results = array();

            // Has this cart been paid?
            $isPaid = ShoppingCartPaymentModel::where('cart_id', $payment_request['cart_id'])->where('status', 'Approved')->first();

            // Trigger happy people is bad for business!
            if (is_null($isPaid)) {
                $amount = $payment_request['total_paid'] * 100;
                $reference = $payment_request['securepay_reference'];
                $card_number = $payment_request['card_number'];
                $save_card_number = $payment_request['card_number'];
                $card_month = $payment_request['card_expiry_month'];
                $card_year = substr($payment_request['card_expiry_year'], -2);
                $card_cvv = $payment_request['card_cvv'];
                $card_name = $payment_request['card_name'];
                $save_card = $payment_request['save_card'];

                $payment_results = Payment::xmlPayment($amount, $reference, $card_number, $card_month, $card_year, $card_cvv);

                $xml_body = $payment_results["xml_body"];
                $receipt_number = $payment_results["receipt_number"];
                $card_number = $payment_results["card_number"];
                $card_expiry = $payment_results["card_expiry"];
                $status = $payment_results["status"];

                if ($status == "Approved" && $save_card == 1) {

                    // Get the customer-code
                    $customer = ShoppingCartModel::where('cart_id', $payment_request['cart_id'])
                        ->join('ucustomers', 'ucustomers.guid', '=', 'shopping_carts.customer_id')
                        ->select('ucustomers.customer_code')
                        ->first();

                    Payment::createStoredCard($customer->customer_code, $save_card_number, $card_month, $card_year, $card_cvv, $card_name);
                }
            } else {
                $receipt_number = $isPaid->receipt_number;
                $status = $isPaid->status;
                $xml_body = 'Duplicate payment found, no charge was applied.';
            }
        }

        // Store the value into the model to be saved.
        $payment_request['status'] = $status;
        $payment_request['card_number'] = $card_number;
        $payment_request['card_expiry'] = $card_expiry;
        $payment_request['receipt_number'] = $receipt_number;
        $payment_request['gateway_log'] = $xml_body;

        return $payment_request;
    }

    /**
     * Returns the entire row of the tempororary cart.
     * Allows to override the filter by adding the cartid in the parameter
     */
    private function getCart($id = '')
    {
        $this->setupSessionVariables();

        if ($id != '') {
            $row_entry = TempCartModel::GetTempCartById($id);
        } else {
            $row_entry = TempCartModel::GetTempCart($this->business_guid, $this->user_id);
        }

        if ($row_entry) {
            return $row_entry;
        }

        return false;
    }

    /**
     * Create or retrieve the cart via the business_guid
     */
    private function getOrCreateCart()
    {
        $this->setupSessionVariables();

        $row_entry = TempCartModel::firstOrCreate(
            ['customer_id' => $this->business_guid, 'person_id' => $this->user_id],
            ['ip_address' => $this->ip_address, 'person_id' => $this->user_id]
        )->toArray();

        if ($row_entry) {
            return $row_entry;
        }

        return false;
    }

    /**
     *   Get the cart GST cost
     */
    private function getCartGstTotal($cart)
    {
        $total = 0;

        if (!empty($cart)) {
            foreach ($cart as $id => $details) {
                $total += ($details['product_cost'] * ($details['product_tax_rate'])) * $details['cart_quantity'];
            }
        }

        $shipping = $this->getCartShippingExGst($cart);
        $total += $shipping * 0.1;

        $esky = $this->getCartEskyExGst($cart);
        $total += $esky * 0.1;

        return round($total, 2);
    }

    /**
     * Get the cart total INC gst
     */
    private function getCartTotal($cart)
    {
        $total = 0;

        if (!empty($cart)) {
            foreach ($cart as $id => $details) {
                $total += ($details['product_cost'] * (1 + $details['product_tax_rate'])) * $details['cart_quantity'];
            }
        }

        $shipping = $this->getCartShippingExGst($cart);
        $total += $shipping * 1.1;

        $esky = $this->getCartEskyExGst($cart);
        $total += $esky * 1.1;

        return round($total, 2);
    }

    /**
     * Get number of items in the cart
     */
    private function getCartTotalCount($cart)
    {
        $total = 0;

        if (!empty($cart)) {
            foreach ($cart as $id => $details) {
                $total++;
            }
        }

        return round($total, 2);
    }

    /**
     * Get number of items in the cart
     */
    private function getCartTotalCountQuantity($cart)
    {
        $total = 0;

        if (!empty($cart)) {
            foreach ($cart as $id => $details) {
                $total += $details["cart_quantity"];
            }
        }

        return round($total, 2);
    }

    /**
     * Get ESKY Ex GST
     */
    private function getCartEskyExGst($cart)
    {
        $total = 0;

        $total_items = $this->getCartEskyCount($cart);
        if ($total_items <= $this->esky_limit && $total_items > 0) {
            $total = $this->esky_cost;
        }

        return round($total, 2);
    }

    /**
     * Get ESKY INC GST
     */
    private function getCartEskyTotal($cart)
    {
        $total = 0;

        $total_items = $this->getCartEskyCount($cart);
        if ($total_items <= $this->esky_limit && $total_items > 0) {
            $total = $this->esky_cost;
        }

        return round(($total * 1.1), 2);
    }

    /**
     * Get ESKY COUNT ITEMS
     */
    private function getCartEskyCount($cart)
    {
        $total = 0;

        if (!empty($cart)) {
            foreach ($cart as $id => $details) {
                if ($details["product_ship_cold"] == 1) {
                    $total += $details['cart_quantity'];
                }
            }
        }

        return round($total, 2);
    }

    /**
     * Get Shipping Ex GST
     */
    private function getCartShippingExGst($cart)
    {
        $total = 0;

        if (!empty($cart)) {
            foreach ($cart as $id => $details) {
                $total += ($details['product_cost'] * $details['cart_quantity']);
            }
        }

        if ($total < $this->shipping_limit) {
            return round($this->shipping_cost, 2);
        } else {
            return 0;
        }
    }

    /**
     * Get Shipping INC GST
     */
    private function getCartShippingTotal($cart)
    {
        $total = 0;

        if (!empty($cart)) {
            foreach ($cart as $id => $details) {
                $total += ($details['product_cost'] * $details['cart_quantity']);
            }
        }

        if ($total < $this->shipping_limit) {
            return round(($this->shipping_cost * 1.1), 2);
        } else {
            return 0;
        }
    }

    /**
     * Get total Line items (EXGST)
     */
    private function getCartTotalExGst($cart)
    {
        $total = 0;

        if (!empty($cart)) {
            foreach ($cart as $id => $details) {
                $total += $details['product_cost'] * $details['cart_quantity'];
            }
        }

        return round($total, 2);
    }

    public function reorderCart(Request $request)
    {
        $business = json_decode(Cookie::get('business'), true);
        $order_number = $request->get('order_number');
        $discontinued = array();
        $out_of_stock = array();
        $has_invalid = false;
        $add_cart = 0;
        $cart_row = $this->getOrCreateCart();
        if($cart_row){
            $cart_items = json_decode(isset($cart_row["cart_content"])?$cart_row["cart_content"]:'[]', true);
        }else{
            $cart_items = array();
        }
        if (count(OrderLinesModel::where('order_number', $order_number)->where('unleashed_id',$business['unleashed_id'])->get())) {
            foreach (OrderLinesModel::where('order_number', $order_number)->where('unleashed_id',$business['unleashed_id'])->get() as $item) {
                $product = ProductModel::CartGetProduct($item->product_code);
                if (!$product) {
                    $has_invalid = true;
                    continue;
//                    return response()->json(['msg' => 'Invalid Product Added.', 'amount' => 0, 'has_discontinued' => $discontinued, 'has_out_of_stock' => $out_of_stock]);
                }
                if (in_array($product['product_group'], ['Finished goods', 'Marketing'])) {
                    if (\App\Models\StockOnHandWarehouse::where('product_guid', $product['product_guid'])->where('warehouse_name', 'Alexandria')->exists()) {
                        if (\App\Models\StockOnHandWarehouse::where('product_guid', $product['product_guid'])->where('warehouse_name', 'Alexandria')->first()->available_quantity >= 20) {
                            if ($product['sellable_in_unleashed'] && !$product['obsolete'] && $product['show_on_website']) {
                                // We will always have a cart now (authenticated practitioner or guest checkout)

                                $cart_row = $this->getOrCreateCart();
                                if ($cart_row) {
                                    $cart_items = array();
                                    if (!empty($cart_row["cart_content"])) {
                                        $cart_items = json_decode(isset($cart_row["cart_content"])?$cart_row["cart_content"]:'[]', true);
                                    }

                                    // If item exist increment number
                                    if (in_array($product["product_guid"], array_column($cart_items, 'product_guid'))) {
                                        $cart_items[array_search($product["product_guid"], array_column($cart_items, 'product_guid'))]['cart_quantity'] += $item->quantity;
                                    } else {
                                        // If item exist increment number
                                        $cart_items[] = array(
                                            'product_description' => $product["product_description"],
                                            'product_group' => $product["product_group"],
                                            'product_brand' => $product["product_brand"],
                                            'product_code' => $product["product_code"],
                                            'product_cost' => $product["product_cost"],
                                            'product_rrp' => $product["product_rrp"],
                                            'product_tax_rate' => $product["product_tax_rate"],
                                            'product_guid' => $product["product_guid"],
                                            'product_ship_cold' => $product["product_ship_cold"],
                                            'product_store_cold' => $product["product_store_cold"],
                                            'cart_quantity' => $item->quantity,
                                            'product_batchnumber' => $product["product_batchnumber"],
                                            'product_expiry' => $product["product_expiry"]
                                        );
                                    }

                                    // push it back to the database.
                                    TempCartModel::UpdateTempCart($cart_row["id"], json_encode($cart_items));

                                    $add_cart += 1;
//
                                } else {
                                    return response()->json(['msg' => 'Unable to create a shopping cart. Please try again later', 'amount' => 0]);
                                }
                            } else {
                                $has_invalid = true;
                                $discontinued = $product["product_code"];
                            }
                        } else {
                            $has_invalid = true;
                            $out_of_stock[] = $product["product_code"];
                        }

                    } else {
                        $has_invalid = true;
                        $out_of_stock[] = $product["product_code"];
                    }
                }
            }
            if($add_cart){
                if($has_invalid){
                    return response()->json(['msg' => 'Some products in this order are discontinued or out of stock, only current products have been added', 'amount' => $this->getCartTotalCountQuantity($cart_items), 'discontinued' => json_encode($discontinued,true), 'out_of_stock' => json_encode($out_of_stock,true),'status' => 'success','add_cart' => $add_cart]);
                }else{
                    return response()->json(['msg' => 'Product added to cart successfully!', 'amount' => $this->getCartTotalCountQuantity($cart_items), 'discontinued' => json_encode($discontinued,true), 'out_of_stock' => json_encode($out_of_stock,true),'status' => 'success','add_cart' => $add_cart]);
                }

            }else{
                return response()->json(['msg' => 'Reorder Failed, all products in this other are discontinued or out of stock','amount' => $this->getCartTotalCountQuantity($cart_items), 'discontinued' => json_encode($discontinued,true), 'out_of_stock' => json_encode($out_of_stock,true),'status' => 'error']);
            }

        }

    }
}
