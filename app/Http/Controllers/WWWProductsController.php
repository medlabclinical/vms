<?php

namespace App\Http\Controllers;

use App\Models\DietaryModel;
use App\Models\MediaLibrary;
use App\Models\Pages;
use App\Models\PdfTypes;
use App\Models\ProductFamilyModel;
use App\Models\ProductGroupModel;
use App\Models\ProductFamilyGroupModel;
use App\Models\ProductModel;
use App\Models\ProductPdfModel;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use NunoMaduro\Collision\Provider;
use function GuzzleHttp\Promise\all;
use Auth;

class WWWProductsController extends Controller
{
    public function index(Request $request)
    {
//        dd($request->all());
        $image_url = config("app.image_url");
        $condition = $request->all();
        if ($request->has('group')) {
            $index = ProductGroupModel::where('name', $request->get('group'))->first()->product_family()->where('brand_id','1')->orderBy('product_name', 'asc')->whereNull('deleted_at')->get();
            $banner = ProductGroupModel::where('name', $request->get('group'))->first();

        } else {
            $index = ProductFamilyModel::where('brand_id', 1)->whereNull('deleted_at')

                ->when($request->has('vegan_friendly'), function ($query) use ($request) {
                    $query->where('vegan_friendly', $request->get('vegan_friendly'));
                })
                ->when($request->has('vegetarian_friendly'), function ($query) use ($request) {
                    $query->where('vegetarian_friendly', $request->get('vegetarian_friendly'));
                })
                ->when($request->has('gluten_free'), function ($query) use ($request) {
                    $query->where('gluten_free', $request->get('gluten_free'));
                })
                ->when($request->has('pregnancy_safe'), function ($query) use ($request) {
                    $query->where('pregnancy_safe', $request->get('pregnancy_safe'));
                })
                ->when($request->has('dairy_free'), function ($query) use ($request) {
                    $query->where('dairy_free', $request->get('dairy_free'));
                })->orderBy('product_name', 'asc')->get();
            if ($request->has('vegan_friendly')) {
                $banner = DietaryModel::where('name', 'Vegan Friendly')->first();
            } elseif ($request->has('vegetarian_friendly')) {
                $banner = DietaryModel::where('name', 'Vegetarian Friendly')->first();
            } elseif ($request->has('gluten_free')) {
                $banner = DietaryModel::where('name', 'Gluten Free')->first();
            } elseif ($request->has('pregnancy_safe')) {
                $banner = DietaryModel::where('name', 'Pregnancy Safe')->first();
            } elseif ($request->has('dairy_free')) {
                $banner = DietaryModel::where('name', 'Dairy Free')->first();
            } else {
                $banner = Pages::where('page', 'products')->where('website', 'vms')->first();
            }
        }
        foreach ($index as $k => $a) {
            if (!in_array('1', array_column($a->product()->get()->toArray(), 'show_on_website'))) {
                $index->forget($k);
            } elseif (!in_array('1', array_column(array_filter($a->product()->get()->toArray(), function ($var) {
                return ($var['show_on_website'] == '1');
            }), 'sellable_in_unleashed'))) {
                $index->forget($k);

            }
        }

        $productsInDev = ProductFamilyModel::where('in_development', '=', 1)->whereNull('deleted_at')->orderBy('id', 'desc')->get();

        $slice = $index->slice(config('app.page-size') * ($request->get('page', 1) - 1), config('app.page-size'))->all();
        $index = new LengthAwarePaginator($slice, count($index), config('app.page-size'), $request->get('page', 1), ['path' => $request->url(), 'query' => $request->query()]);

        return view('www/pages/products/products', compact('index', 'condition', 'banner', 'image_url', 'productsInDev'));
    }


    public function getProductFamily($id)
    {
        $id = substr($id,strrpos($id,"-")+1);
        $image_url = config("app.image_url");
        $productfamily = ProductFamilyModel::where('id', $id)->first();
        if (!$productfamily||!in_array('1', array_column($productfamily->product()->get()->toArray(), 'show_on_website'))) {
            return abort(404);
        } elseif (!in_array('1', array_column(array_filter($productfamily->product()->get()->toArray(), function ($var) {
            return ($var['show_on_website'] == '1');
        }), 'sellable_in_unleashed'))) {
            return abort(404);
        }
        $products = $productfamily->product()->get();
        $this_product = $products->toArray()[0];
        $productpdfs = ProductPdfModel::where('product_family_id', $productfamily->id)->get();
        $productingredients = \DB::table('product_family_ingredients')
            ->join('product_ingredients', 'product_family_ingredients.product_ingredient_id', 'product_ingredients.id')
            ->select('product_family_ingredients.*', 'product_ingredients.*')
            ->where('product_family_id', $productfamily->id)->get();
        $product_group = $productfamily->product_group()->get();
//        $product_ingredient = $productfamily->product_ingredient()->get();
        $product_family_ingredient = $productfamily->product_family_ingredient()->get();
        $product_pdf_cmi = $productfamily->product_pdf()->where('product_pdfs.deleted_at', null)->where('obsolete', '0')->whereHas('pdf_type', function ($q) {
            $q->where('name', 'CMI');
        })->get()->toArray();
        $product_pdf_not_cmi = $productfamily->product_pdf()->where('product_pdfs.deleted_at', null)->where('obsolete', '0')->whereHas('pdf_type', function ($q) {
            $q->where('name', '!=', 'CMI');
        })->get()->toArray();
        $not_pdf = array();
        foreach ($product_pdf_not_cmi as $item) {
            if (isset($not_pdf[$item['pdf_type_id']])) {
                $not_pdf[$item['pdf_type_id']][] = $item;
            } else {
                $not_pdf[$item['pdf_type_id']][0] = $item;
                $not_pdf[$item['pdf_type_id']][0]['pdf_type_id'] = PdfTypes::where('id', $not_pdf[$item['pdf_type_id']][0]['pdf_type_id'])->first()->name;
            }
        }
        $publication = $productfamily->publication()->get();
        $clinical_trial = $productfamily->clinical_trial()->where('publish', '1')->get();
        foreach ($publication as $item) {
            $item->member = $item->member()->get()->toArray();
        }
        $publication = $publication->toArray();
        foreach ($clinical_trial as $item) {
            $item->member = $item->member()->get()->toArray();
        }
        $article = $productfamily->article()->where('publish', 1)->whereNull('articles.deleted_at')->whereHas('brand',function ($q){
            $q->whereIn('brand.id',[1,3]);
        })->get();
        $totalPage = ceil(count($article->toArray()) / 12);
        $article = $article->slice(0, 12);
        foreach ($article as $item) {
            if ($item->article_type == 'image') {
                $item->title_img = $item->title_img ? MediaLibrary::where('id', $item->title_img)->first()->name : NULL;
                $item->second_img = $item->second_img ? MediaLibrary::where('id', $item->second_img)->first()->name : NULL;
                $item->third_img = $item->third_img ? MediaLibrary::where('id', $item->third_img)->first()->name : NULL;
            }
        }
        $pdf_type = PdfTypes::where('name', '!=', 'CMI')->get();
//        foreach($pdf_type as $a){
//            dd($a->hasPDF()->whereHas('product_family', function ($q) {
//                $q->where('product_family.id', 1);
//            })->get());
//        };
//        $clinical_trial = $clinical_trial->toArray();

//        dd($not_pdf);


        return view('www/pages/products/product_page', compact('productfamily', 'products', 'productingredients', 'productpdfs', 'product_group', 'product_family_ingredient', 'this_product', 'image_url', 'product_pdf_cmi', 'not_pdf', 'publication', 'clinical_trial', 'article', 'totalPage', 'pdf_type'));

    }


    public function getProductsInDevDetail($id)
    {
        $id = substr($id,strrpos($id,"-")+1);
        $image_url = config("app.image_url");
        $productfamily = ProductFamilyModel::where('id', $id)->first();

        $products = $productfamily->product()->get();
        $productpdfs = ProductPdfModel::where('product_family_id', $productfamily->id)->get();
        $productingredients = \DB::table('product_family_ingredients')
            ->join('product_ingredients', 'product_family_ingredients.product_ingredient_id', 'product_ingredients.id')
            ->select('product_family_ingredients.*', 'product_ingredients.*')
            ->where('product_family_id', $productfamily->id)->get();
        $product_group = $productfamily->product_group()->get();
//        $product_ingredient = $productfamily->product_ingredient()->get();
        $product_family_ingredient = $productfamily->product_family_ingredient()->get();
        $product_pdf_cmi = $productfamily->product_pdf()->where('product_pdfs.deleted_at', null)->where('obsolete', '0')->whereHas('pdf_type', function ($q) {
            $q->where('name', 'CMI');
        })->get()->toArray();
        $product_pdf_not_cmi = $productfamily->product_pdf()->where('product_pdfs.deleted_at', null)->where('obsolete', '0')->whereHas('pdf_type', function ($q) {
            $q->where('name', '!=', 'CMI');
        })->get()->toArray();
        $not_pdf = array();
        foreach ($product_pdf_not_cmi as $item) {
            if (isset($not_pdf[$item['pdf_type_id']])) {
                $not_pdf[$item['pdf_type_id']][] = $item;
            } else {
                $not_pdf[$item['pdf_type_id']][0] = $item;
                $not_pdf[$item['pdf_type_id']][0]['pdf_type_id'] = PdfTypes::where('id', $not_pdf[$item['pdf_type_id']][0]['pdf_type_id'])->first()->name;
            }
        }
        $publication = $productfamily->publication()->get();
        $clinical_trial = $productfamily->clinical_trial()->get();
        foreach ($publication as $item) {
            $item->member = $item->member()->get()->toArray();
        }
        $publication = $publication->toArray();
        foreach ($clinical_trial as $item) {
            $item->member = $item->member()->get()->toArray();
        }
        $article = $productfamily->article()->where('publish', 1)->whereNull('articles.deleted_at')->whereHas('brand',function ($q){
            $q->whereIn('brand.id',[1,3]);
        })->get();
        $totalPage = ceil(count($article->toArray()) / 12);
        $article = $article->slice(0, 12);
        foreach ($article as $item) {
            if ($item->article_type == 'image') {
                $item->title_img = $item->title_img ? MediaLibrary::where('id', $item->title_img)->first()->name : NULL;
                $item->second_img = $item->second_img ? MediaLibrary::where('id', $item->second_img)->first()->name : NULL;
                $item->third_img = $item->third_img ? MediaLibrary::where('id', $item->third_img)->first()->name : NULL;
            }
        }
        $pdf_type = PdfTypes::where('name', '!=', 'CMI')->get();
//        foreach($pdf_type as $a){
//            dd($a->hasPDF()->whereHas('product_family', function ($q) {
//                $q->where('product_family.id', 1);
//            })->get());
//        };
//        $clinical_trial = $clinical_trial->toArray();

//        dd($not_pdf);


        return view('www/pages/products/products_in_development', compact('productfamily', 'products', 'productingredients', 'productpdfs', 'product_group', 'product_family_ingredient', 'image_url', 'product_pdf_cmi', 'not_pdf', 'publication', 'clinical_trial', 'article', 'totalPage', 'pdf_type'));

    }


    public function store(Request $request)
    {
        $data = ProductModel::where('product_description', $request->get('id'))->first();
        return response()->json($data);
    }

    public function articlePage(Request $request)
    {
//        $page = $request->get('page');
        $productfamily = ProductFamilyModel::where('id', $request->get('id'))->first();
        $allArticles = $productfamily->article()->where('publish', 1)->whereNull('articles.deleted_at')->whereHas('brand',function ($q){
            $q->whereIn('brand.id',[1,3]);
        })->get();
        $slice = $allArticles->slice(12 * ($request->get('page', 1) - 1), 12)->all();
        $allArticles = new LengthAwarePaginator($slice, count($allArticles), 12, $request->get('page', 1), ['path' => $request->url(), 'query' => $request->query()]);
        foreach ($allArticles as $item) {
            if ($item->article_type == 'image') {
                $item->title_img = $item->title_img ? MediaLibrary::where('id', $item->title_img)->first()->name : NULL;
                $item->second_img = $item->second_img ? MediaLibrary::where('id', $item->second_img)->first()->name : NULL;
                $item->third_img = $item->third_img ? MediaLibrary::where('id', $item->third_img)->first()->name : NULL;
            }
        }
        return response()->json($allArticles);
    }

    public function getGroupProduct()
    {
        $index = ProductGroupModel::all();
        $image_url = config("app.image_url");
        return view('www/pages/products/products_by_categories', compact('index', 'image_url'));
    }

    public function getDietaryProduct()
    {
        $index = DietaryModel::all();
        $image_url = config("app.image_url");
        return view('www/pages/products/products_by_dietaries', compact('index', 'image_url'));
    }


}
