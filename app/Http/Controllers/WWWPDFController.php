<?php

namespace App\Http\Controllers;

use App\Models\ArticleCategory;
use App\Models\Articles;
use App\Models\MediaLibrary;
use App\Models\Pages;
use App\Models\PdfTypes;
use App\Models\ProductFamilyModel;
use App\Models\ProductMultipleRelation;
use App\Models\ProductPdfModel;
use http\Env;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Auth;

class WWWPDFController extends Controller
{
    public function index(Request $request)
    {
//        dd($request->all());
//        dd(Auth::guard('practitioner')->user());
        $role = $request->has('role') ? $request->get('role') : "*";
        $pdf_type = $request->has('pdf_type') ? $request->get('pdf_type') : "*";
        $product_family = $request->has('product_family') ? $request->get('product_family') : "*";
        if ($pdf_type == "*") {
            $AllPDF = ProductPdfModel::where('obsolete', '!=', '1')->whereNull('deleted_at')->whereHas('brand', function ($q) {
                $q->whereIn('brand.id', [1,3]);
            });
            $content = Pages::where('page', 'flyers')->where('website', 'medlab')->first();
        } else {
            if(!PdfTypes::where('name',$pdf_type)->exists()){
                return redirect(404);
            }
            $AllPDF = ProductPdfModel::where('obsolete', '!=', '1')->whereNull('deleted_at')->whereHas('brand', function ($q) {
                $q->whereIn('brand.id', [1,3]);
            })->whereHas('pdf_type', function ($q) use ($pdf_type) {
                $q->where('pdf_type.name', $pdf_type);
            });
            $content = PdfTypes::where('name',$pdf_type)->first();
        };
        if($role != '*'){
            $AllPDF = $AllPDF->where('access_level',$role);
        };
        if($product_family!='*'){
            $AllPDF = $AllPDF->whereHas('product_family', function ($q) use($product_family){
                $q->where('product_family.product_name',$product_family);
            })->get();
        } else{
          $AllPDF = $AllPDF->get();

        };
//        $category = array();
//        $list = array();
//        foreach ($AllPDF as $item){
//            if ($item->product_family()->first()) {
//                $name = $item->product_family()->first()->product_name;
//                $item->category = $name;
//                if (!isset($list[$name])) {
//                    array_push($category, $name);
//                    $list[$name] = [];
//                }
//                array_push($list[$name], $item);
//            } else {
//                $item->category = '';
//            }
//        }
        $category = ProductFamilyModel::whereIn('brand_id', [1,3])->whereNull('deleted_at')->orderBy('product_name', 'asc')->get();
        foreach ($category as $k => $a) {
            if (!in_array('1', array_column($a->product()->get()->toArray(), 'show_on_website'))) {
                $category->forget($k);
            } elseif (!in_array('0', array_column(array_filter($a->product()->get()->toArray(), function ($var) {
                return ($var['show_on_website'] == '1');
            }), 'obsolete'))) {
                $category->forget($k);

            }
        }
        $image_url = config("app.image_url");

        $typeFilter = ProductFamilyModel::whereNull('deleted_at')->Where('brand_id', '=','1')->orderBy('id', 'desc')->get();


        $content->title_img = $content->title_img ? MediaLibrary::where('id', $content->title_img)->first()->name : NULL;
        $content->second_img = $content->second_img ? MediaLibrary::where('id', $content->second_img)->first()->name : NULL;
        $content->third_img = $content->third_img ? MediaLibrary::where('id', $content->third_img)->first()->name : NULL;

        $slice = $AllPDF->slice(config('app.page-size') * ($request->get('page', 1) - 1), config('app.page-size'))->all();
        $AllPDF = new LengthAwarePaginator($slice, count($AllPDF), config('app.page-size'), $request->get('page', 1), ['path' => $request->url(), 'query' => $request->query()]);
        return view('www/pages/education/flyers', compact('AllPDF', 'image_url', 'content', 'category', 'pdf_type', 'role','product_family', 'typeFilter'));
    }


    public function getContent($id)
    {
        $image_url = config("app.image_url");
        $article = Articles::where('id', $id)->first();
        $article->title_img = $article->title_img ? MediaLibrary::where('id', $article->title_img)->first()->name : NULL;
        $article->second_img = $article->second_img ? MediaLibrary::where('id', $article->second_img)->first()->name : NULL;
        $article->third_img = $article->third_img ? MediaLibrary::where('id', $article->third_img)->first()->name : NULL;
        $member = $article->member()->get()->toArray();
        $related_article = [];
        if (count($member)) {
            for ($i = 0; $i < count($member); $i++) {
                if ($member[$i]['picture']) {
                    $picture = MediaLibrary::where('id', $member[$i]['picture'])->first()->name;
                    $member[$i]['picture'] = $picture;
                    foreach (array_column(ProductMultipleRelation::where('member_id', $member[$i]['id'])->get()->toArray(), 'article_id') as $item) {
                        if ($item != $id && $item != null) {
                            $related_article[] = $item;
                        }
                    }
                }
            }
        }
        $article->related_article = Articles::whereIn('id', $related_article)->where('publish', 1)->whereHas('brand', function ($q) {
            $q->whereIn('brand.id', [1, 3]);
        })->orderBy('date', 'desc')->limit(3)->get();
//        $article->related_article=array_values(array_unique($related_article));
        $article->member = $member;
        $article->product_family = $article->product_family()->get()->toArray();
        $article->clinical_trial = $article->clinical_trial()->get()->toArray();
        $article->product_ingredient = $article->product_ingredient()->get()->toArray();
        $article->publication = $article->publication()->get()->toArray();
        $article->article_category = $article->article_category()->get()->toArray();
//        $article_category = ArticleCategory::all();
//        dd($article);
//        $comment = new CommentsController();
        $comments = (new CommentsController())->index($id);
//        dd($comments);
        if ($article->article_type == 'image') {
            return view('www/pages/education/image-articles', compact('article', 'image_url', 'comments'));
        } elseif ($article->article_type == 'link') {
            return view('www/pages/education/link-articles', compact('article', 'image_url', 'comments'));
        } elseif ($article->article_type == 'slider') {
            return view('www/pages/education/slider-articles', compact('article', 'image_url', 'comments'));
        } elseif ($article->article_type == 'pdf') {
            return view('www/pages/education/pdf-articles', compact('article', 'image_url', 'comments'));
        } else {
            return view('www/pages/education/video-articles', compact('article', 'image_url', 'comments'));
        }
    }
}
