<?php

namespace App\Http\Controllers;

use App\Models\Articles;
use App\Models\ClinicalTrials;
use App\Models\MediaLibrary;
use App\Models\Members;
use App\Models\MemberType;
use App\Models\Pages;
use App\Models\Partners;
use App\Models\ProductFamilyModel;
use App\Models\ProductMultipleRelation;
use App\Models\Publications;
use Illuminate\Http\Request;
use DB;
class PeopleController extends Controller
{
    public function getPageInfo(string $page)
    {
        $content = Pages::where('page', $page)->where('website', 'medlab')->first();
        $content->title_img = $content->title_img ? MediaLibrary::where('id', $content->title_img)->first()->name : NULL;
        $content->second_img = $content->second_img ? MediaLibrary::where('id', $content->second_img)->first()->name : NULL;
        $content->third_img = $content->third_img ? MediaLibrary::where('id', $content->third_img)->first()->name : NULL;
        return $content;
    }

    public function getAboutCategory()
    {
        return Pages::whereIn('page', ['about', 'contact', 'commercial_partners', 'collaborations','industry_associations', 'partner opportunities', 'leadership team','consulting_team'])->get();
    }
    public function getOrganisationCategory(){
        return Pages::whereIn('page',['leadership team','consulting_team'])->get();
    }
    public function getPartneringCategory(){
        return Pages::whereIn('page',['commercial_partners', 'collaborations','industry_associations', 'partner opportunities'])->get();
    }
    public function mergeMediaLibrary($table,$team,$website,$model){
        $people = DB::table($table)->join('nova_media_library',$table.'.picture','=','nova_media_library.id')
            ->select($table.'.*','nova_media_library.name as picture')->where($team,1)->get();

        if ($model=='Members'){
            $peopleNoPic1 = Members::where($team,1)->where('picture',NULL)->get();
            $peopleNoPic2 = Members::where($team,1)->where('picture','')->get();
        }else{
            $peopleNoPic1 = Partners::where($team,1)->where('picture',NULL)->get();
            $peopleNoPic2 = Partners::where($team,1)->where('picture','')->get();
        }
        $people = $people->merge($peopleNoPic1)->merge($peopleNoPic2);
//        $data = collect();
//        foreach ($people as $item){
//            if($model=='Members'){

//                if(in_array('3',array_column(ProductMultipleRelation::where('member_id',$item->id)->get()->toArray(),'brand_id'))){
//                    $data->add($item);
//                }
//            }else{
//                if(in_array('3',array_column(ProductMultipleRelation::where('partner_id',$item->id)->get()->toArray(),'brand_id'))){
//                    $data->add($item);
//                }
//            }
//
//        }

//        $people = $data;
        return($people);

    }
    public function getContent($team,$website){
        $content = Pages::where('page',$team)->where('website',$website)->first();
        $content->title_img=$content->title_img?MediaLibrary::where('id',$content->title_img)->first()->name:NULL;
        $content->second_img=$content->second_img?MediaLibrary::where('id',$content->second_img)->first()->name:NULL;
        $content->third_img=$content->third_img?MediaLibrary::where('id',$content->third_img)->first()->name:NULL;
        return($content);
    }

    public function checkPage($result,$index){
//        dd(array_column($result->toArray(),NULL,$index));
        if(array_key_exists('1',array_column($result->toArray(),NULL,$index))){
            return Pages::where('page',$index)->first();
        }else{
            return false;
        }
    }

    public function getLeadershipTeam()
    {

        $image_url = config("app.image_url");
        $content = $this->getPageInfo('leadership team');
        $leadershipTeamList = Members::where('leadership_team', '!=', '0')->whereNull('deleted_at')->orderBy('id', 'asc')->get();
        $typeFilter = MemberType::whereNull('deleted_at')->orderBy('id', 'desc')->get();

        $about_list = $this->getAboutCategory();
        return view('www/pages/about/leadership_team', compact('content', 'image_url', 'leadershipTeamList', 'typeFilter', 'about_list'));
    }

    public function getLeadershipTeamPage($id)
    {

        $id = substr($id,strrpos($id,"-")+1);
        $image_url = config("app.image_url");
        try {
            $leadershipTeam = Members::where('id', $id)->where('leadership_team', '!=', '0')->first();
        } catch (\Exception $ex) {
            abort(404);
        }

        $leadershipTeam->picture = $leadershipTeam->picture ? MediaLibrary::where('id', $leadershipTeam->picture)->first()->name : NULL;


        $article = $leadershipTeam->article()->whereNull('deleted_at')->get()->toArray();
        if (count($article)) {
            for ($i = 0; $i < count($article); $i++) {
                if ($article[$i]['title_img']) {
                    $picture = MediaLibrary::where('id', $article[$i]['title_img'])->first()->name;
                    $article[$i]['title_img'] = $picture;
                }
            }
        }

        $leadershipTeam->article = $article;

        $leadershipTeam->publicationsandpresentation = $leadershipTeam->publicationsandpresentation()->whereNull('deleted_at')->get()->toArray();



        $about_list = $this->getAboutCategory();
        return view('www/pages/about/leadership_team_page', compact('leadershipTeam', 'image_url', 'about_list'));
    }

    public function getConsultingTeam()
    {

        $image_url = config("app.image_url");
        $content = $this->getPageInfo('consulting_team');
        $consultingTeamList = Members::where('consulting_team', '!=', '0')->whereNull('deleted_at')->orderBy('id', 'asc')->get();
        $typeFilter = MemberType::whereNull('deleted_at')->orderBy('id', 'desc')->get();

        $about_list = $this->getAboutCategory();
        return view('www/pages/about/consulting_team', compact('content', 'image_url', 'consultingTeamList', 'typeFilter', 'about_list'));
    }

    public function getConsultingTeamPage($id)
    {

        $id = substr($id,strrpos($id,"-")+1);
        $image_url = config("app.image_url");
        try {
            $consultingTeam = Members::where('id', $id)->where('consulting_team', '!=', '0')->first();
        } catch (\Exception $ex) {
            abort(404);
        }

        $consultingTeam->picture = $consultingTeam->picture ? MediaLibrary::where('id', $consultingTeam->picture)->first()->name : NULL;


        $article = $consultingTeam->article()->get()->whereNull('deleted_at')->toArray();
        if (count($article)) {
            for ($i = 0; $i < count($article); $i++) {
                if ($article[$i]['title_img']) {
                    $picture = MediaLibrary::where('id', $article[$i]['title_img'])->first()->name;
                    $article[$i]['title_img'] = $picture;
                }
            }
        }

        $consultingTeam->article = $article;

        $consultingTeam->publicationsandpresentation = $consultingTeam->publicationsandpresentation()->whereNull('deleted_at')->get()->toArray();



        $about_list = $this->getAboutCategory();
        return view('www/pages/about/consulting_team_page', compact('consultingTeam', 'image_url', 'about_list'));
    }


    public function getConsultingTeamOLD(){
        $image_url=config("app.image_url");
        $people = $this->mergeMediaLibrary('members','consulting_team','medlab','Members');
        $content = $this->getContent('consulting_team','medlab');
        $about_list = $this->getAboutCategory();
        return view('www/pages/about/consulting_team', compact('people','image_url','content','about_list'));

    }
    public function getCommercialPartners(){
        $image_url=config("app.image_url");
        $people = $this->mergeMediaLibrary('partners','commercial_partners','medlab','Partners');
        $content = $this->getContent('commercial_partners','medlab');
        $about_list = $this->getAboutCategory();
        return view('www/pages/about/commercial_partners', compact('people','image_url','content','about_list'));

    }
    public function getCollaborations(){
        $image_url=config("app.image_url");
        $people = $this->mergeMediaLibrary('partners','collaboration','medlab','Partners');
        $content = $this->getContent('collaborations','medlab');
        $about_list = $this->getAboutCategory();
        return view('www/pages/about/collaborations', compact('people','image_url','content','about_list'));

    }
    public function getIndustryAss(){
        $image_url=config("app.image_url");
        $people = $this->mergeMediaLibrary('partners','industry_associations','medlab','Partners');
        $content = $this->getContent('industry_associations','medlab');
        $about_list = $this->getAboutCategory();
        return view('www/pages/about/industry_associations', compact('people','image_url','content','about_list'));

    }

    public function searchPage(Request $request){
        $search = $request['search'];
        $image_url=config("app.image_url");
        $people = DB::table('members')->join('nova_media_library','members.picture','=','nova_media_library.id')
            ->select('members.*','nova_media_library.name as picture')->where('members.name','LIKE','%'.$search.'%')->where('medlab','1')->get();
        $peopleNoPic1 = Members::where('name','LIKE','%'.$search.'%')->where('medlab','1')->where('picture',NULL)->get();
        $peopleNoPic2 = Members::where('name','LIKE','%'.$search.'%')->where('medlab','1')->where('picture','')->get();
        $name = $people->merge($peopleNoPic1)->merge($peopleNoPic2);
        $people = DB::table('members')->join('nova_media_library','members.picture','=','nova_media_library.id')
            ->select('members.*','nova_media_library.name as picture')->where('position','LIKE','%'.$search.'%')->where('medlab','1')->get();
        $peopleNoPic1 = Members::where('position','LIKE','%'.$search.'%')->where('medlab','1')->where('picture',NULL)->get();
        $peopleNoPic2 = Members::where('position','LIKE','%'.$search.'%')->where('medlab','1')->where('picture','')->get();
        $position = $people->merge($peopleNoPic1)->merge($peopleNoPic2);
//        dd($position);
        $people = DB::table('members')->join('nova_media_library','members.picture','=','nova_media_library.id')
            ->select('members.*','nova_media_library.name as picture')->where('bio','LIKE','%'.$search.'%')->where('medlab','1')->get();
        $peopleNoPic1 = Members::where('bio','LIKE','%'.$search.'%')->where('medlab','1')->where('picture',NULL)->get();
        $peopleNoPic2 = Members::where('bio','LIKE','%'.$search.'%')->where('medlab','1')->where('picture','')->get();
        $bio = $people->merge($peopleNoPic1)->merge($peopleNoPic2);

        $partner = DB::table('partners')->join('nova_media_library','partners.picture','=','nova_media_library.id')
            ->select('partners.*','nova_media_library.name as picture')->where('partners.name','LIKE','%'.$search.'%')->where('medlab','1')->get();
        $peopleNoPic1 = Partners::where('name','LIKE','%'.$search.'%')->where('medlab','1')->where('picture',NULL)->get();
        $peopleNoPic2 = Partners::where('name','LIKE','%'.$search.'%')->where('medlab','1')->where('picture','')->get();
        $partner_name = $partner->merge($peopleNoPic1)->merge($peopleNoPic2);

        $partner = DB::table('partners')->join('nova_media_library','partners.picture','=','nova_media_library.id')
            ->select('partners.*','nova_media_library.name as picture')->where('bio','LIKE','%'.$search.'%')->where('medlab','1')->get();
        $peopleNoPic1 = Partners::where('bio','LIKE','%'.$search.'%')->where('medlab','1')->where('picture',NULL)->get();
        $peopleNoPic2 = Partners::where('bio','LIKE','%'.$search.'%')->where('medlab','1')->where('picture','')->get();
        $partner_bio = $partner->merge($peopleNoPic1)->merge($peopleNoPic2);

        $partner = $partner_name->merge($partner_bio)->unique('id');

        $people = $name->merge($position)->merge($bio)->unique('id');
        $result = $people->merge($partner);
//        dd($result);
        $url = [];
        $url[]=$this->checkPage($people,'executive_team');
        $url[]=$this->checkPage($people,'consulting_team');
        $url[]=$this->checkPage($people,'scientific_team');
        $url[]=$this->checkPage($people,'board_of_directors');
        $url[]=$this->checkPage($partner,'commercial_partners');
        $url[]=$this->checkPage($partner,'industry_associations');
        foreach (Pages::where('website','medlab')->get() as $item){
//            dd($item->title);
            if(str_contains($item->content?:'',$search) || str_contains($item->title?:'',$search)){
                $url[]=$item;
            }
        }
        $url = array_unique($url);
//        dd($url);

        unset( $url[array_search( false, $url )]);
//        dd($url);
//        $article = Articles::where('publish','1')->whereNull('deleted_at')
//            ->where(function($query) use ($search){
//                $query->where('title', 'LIKE', '%'.$search.'%');
//                $query->orWhere('subtitle', 'LIKE', '%'.$search.'%');
//                $query->orWhere('category', 'LIKE', '%'.$search.'%');
//                $query->orWhere('preview_text', 'LIKE', '%'.$search.'%');
//                $query->orWhere('content', 'LIKE', '%'.$search.'%');
//            })->get();
        $article_1 = Articles::where('publish','1')->whereNull('deleted_at')->where('title', 'LIKE', '%'.$search.'%')->orderBy('date', 'desc')->get();
        $article_2 = Articles::where('publish','1')->whereNull('deleted_at')->where('subtitle', 'LIKE', '%'.$search.'%')->orderBy('date', 'desc')->get();
        $article_3 = Articles::where('publish','1')->whereNull('deleted_at')->where('content', 'LIKE', '%'.$search.'%')->orderBy('date', 'desc')->get();
        $article_4 = Articles::where('publish','1')->whereNull('deleted_at')->where('category', 'LIKE', '%'.$search.'%')->orderBy('date', 'desc')->get();
        $article_5 = Articles::where('publish','1')->whereNull('deleted_at')->where('preview_text', 'LIKE', '%'.$search.'%')->orderBy('date', 'desc')->get();
        $article = $article_1->merge($article_2)->merge($article_3)->merge($article_4)->merge($article_5);
        foreach ($article as $item){
            $item->title_img = $item->title_img ? MediaLibrary::where('id', $item->title_img)->first()->name : NULL;
            $item->second_img = $item->second_img ? MediaLibrary::where('id', $item->second_img)->first()->name : NULL;
            $item->third_img = $item->third_img ? MediaLibrary::where('id', $item->third_img)->first()->name : NULL;
        }
        $product_family = ProductFamilyModel::whereIn('brand_id',['1','3'])->whereNull('deleted_at')
            ->where(function($query) use ($search){
                $query->where('product_name', 'LIKE', '%'.$search.'%');
                $query->orWhere('product_byline', 'LIKE', '%'.$search.'%');
                $query->orWhere('product_heading', 'LIKE', '%'.$search.'%');
                $query->orWhere('directions_use', 'LIKE', '%'.$search.'%');
                $query->orWhere('product_description', 'LIKE', '%'.$search.'%');
                $query->orWhere('warnings', 'LIKE', '%'.$search.'%');
                $query->orWhere('unit_type', 'LIKE', '%'.$search.'%');
                $query->orWhere('reg_permissableindications', 'LIKE', '%'.$search.'%');
                $query->orWhere('reg_suitable_for_ages_above', 'LIKE', '%'.$search.'%');
                $query->orWhere('side_effects', 'LIKE', '%'.$search.'%');
                $query->orWhere('product_interactions', 'LIKE', '%'.$search.'%');
                $query->orWhere('austl_number', $search);
                $query->orWhere('reg_suitable_for_ages_above', 'LIKE', '%'.$search.'%');
                $query->orWhere('side_effects', 'LIKE', '%'.$search.'%');
                $query->orWhere('product_interactions', 'LIKE', '%'.$search.'%');
            })->get();
        $product_ingredient = ProductFamilyModel::whereIn('brand_id',['1','3'])->whereNull('deleted_at')
            ->whereHas('product_ingredient',function ($query) use ($search){
                $query->where('name', 'LIKE', '%'.$search.'%');
                $query->orWhere('scientific_name', 'LIKE', '%'.$search.'%');
                $query->orWhere('consumer_ingredient', 'LIKE', '%'.$search.'%');
                $query->orWhere('practitioner_ingredient', 'LIKE', '%'.$search.'%');
            })->get();
        $all_products = $product_family->merge($product_ingredient);
        foreach ($all_products as $k => $a) {
            if (!in_array('1', array_column($a->product()->get()->toArray(), 'show_on_website'))) {
                $all_products->forget($k);
            } elseif (!in_array('1', array_column(array_filter($a->product()->get()->toArray(), function ($var) {
                return ($var['show_on_website'] == '1');
            }), 'sellable_in_unleashed'))) {
                $all_products->forget($k);

            }
        }
//        dd($all_products);
        $publication_list = Publications::where('title', 'LIKE', '%'.$search.'%')->where('deleted_at', null)->orderBy('date','desc')->get();
//        dd($publication_list);
        $publications = [];
        foreach ($publication_list as $item){
            $item->member = $item->member()->get()->toArray();
            $item->product_family = $item->product_family()->get()->toArray();
            $item->clinical_trial = $item->clinical_trial()->get()->toArray();
            $item->product_ingredient = $item->product_ingredient()->get()->toArray();
            $item->article = $item->article()->get()->toArray();
            $item->product_group = $item->product_group()->get()->toArray();
            $item->category = $item->product_group()->get()->toArray();
            $publications[substr($item['date'],0,4)][] = $item;
        }
//        dd($publications);
        $amount = count($url)+count($people)+count($partner)+count($article)+count($all_products)+count($product_ingredient)+count($publication_list);
        $tab_amount = (count($url)?1:0) + (count($people)?1:0) + (count($partner)?1:0) + (count($article)?1:0) + (count($all_products)?1:0) + (count($product_ingredient)?1:0) + (count($publication_list)?1:0);
//        dd($url);
//        $url['is_exe'] = array_key_exists('1',array_column($people->toArray(),NULL,'executive_team'));
//        $url['is_con'] = array_key_exists('1',array_column($people->toArray(),NULL,'consulting_team'));
//        $url['is_sci'] = array_key_exists('1',array_column($people->toArray(),NULL,'scientific_team'));
//        $url['is_bor'] = array_key_exists('1',array_column($people->toArray(),NULL,'board_of_director'));
//        $url['is_com'] = array_key_exists('1',array_column($people->toArray(),NULL,'commercial_partners'));
//        $url['is_ind'] = array_key_exists('1',array_column($people->toArray(),NULL,'industry_associations'));
//        dd($url);
        return view('www/pages/misc/search_result', compact('people','partner','image_url','url','amount','search','article','all_products','product_ingredient','tab_amount','publications'));

    }
}
