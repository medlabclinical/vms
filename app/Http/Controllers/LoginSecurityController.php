<?php

namespace App\Http\Controllers;

use App\Models\LoginSecurity;
use App\Models\User;
use Auth;
use Carbon\Carbon;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use PragmaRX\Recovery\Recovery;
use Twilio\Rest\Client;

class LoginSecurityController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
//    public function __construct()
//    {
//        $this->middleware('auth');
//    }

    /**
     * Show 2FA Setting form
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show2faForm(Request $request){


        $user = Auth::guard('practitioner')->user();
        $google2fa_url = "";
        $secret_key = "";
        $phone = "";

        if($user->loginSecurity()->exists()){
            $google2fa = (new \PragmaRX\Google2FAQRCode\Google2FA());
            $google2fa_url = $google2fa->getQRCodeInline(
                'Medlab',
                $user->email,
                $user->loginSecurity->google2fa_secret
            );
            $secret_key = $user->loginSecurity->google2fa_secret;
            $phone = $user->mobile;
        }

        $data = array(
            'user' => $user,
            'secret' => $secret_key,
            'google2fa_url' => $google2fa_url,
            'phone' => $phone,
        );

        return view('auth.2fa_settings')->with('data', $data);
    }

    /**
     * Generate 2FA secret key
     */
    public function generate2faSecret(Request $request){
        $user = Auth::guard('practitioner')->user();
        // Initialise the 2FA class
        $google2fa = (new \PragmaRX\Google2FAQRCode\Google2FA());

        // Add the secret key to the registration data
        $login_security = LoginSecurity::firstOrNew(array('user_id' => $user->id));
        $login_security->user_id = $user->id;
        $login_security->google2fa_enable = 0;
        $login_security->google2fa_secret = $google2fa->generateSecretKey();
        $login_security->save();

        return redirect('/2fa')->with('success',"Secret key is generated.");
    }

    /**
     * Enable 2FA
     */
    public function enable2fa(Request $request){
        $user = Auth::guard('practitioner')->user();
        $google2fa = (new \PragmaRX\Google2FAQRCode\Google2FA());

        $secret = $request->input('secret');
        $valid = $google2fa->verifyKey($user->loginSecurity->google2fa_secret, $secret);

        if($valid){
            $user->loginSecurity->google2fa_enable = 1;
            $user->loginSecurity->save();
            return response()->json(['status'=>'success','msg' => '2FA is enabled successfully.']);
//            return redirect('2fa')->with('success',"2FA is enabled successfully.");
        }else{
            return response()->json(['status'=>'error','msg' => 'Invalid verification code, Please try again.']);
//            return redirect('2fa')->with('error',"Invalid verification Code, Please try again.");
        }
    }

    /**
     * Disable 2FA
     */
    public function disable2fa(Request $request){
        if (!(Hash::check($request->get('current-password'), Auth::guard('practitioner')->user()->password))) {
            // The passwords matches
            return response()->json(['status'=>'error','msg' => 'Your password does not matches with your account password. Please try again.']);
//            return redirect()->back()->with("error","Your password does not matches with your account password. Please try again.");
        }

        $validatedData = $request->validate([
            'current-password' => 'required',
        ]);
        $user = Auth::guard('practitioner')->user();
        LoginSecurity::where('user_id',$user->id)->delete();
//        $user->loginSecurity->google2fa_enable = 0;
//        $user->loginSecurity->save();
        return response()->json(['status'=>'success','msg' => '2FA is now disabled.']);
//        return redirect('/2fa')->with('success',"2FA is now disabled.");
    }

    public function generateBackupCode(Request $request){
        $this->recovery = new Recovery();
        $code = $this->recovery->toArray();
        $new_code = LoginSecurity::where('user_id',Auth::guard('practitioner')->user()->id)->update([
            'backup_code' => $code
        ]);
        if($new_code){
            return response()->json(['status'=>'success','msg' => 'Generate new backup code successfully.','code' => json_encode($code,true)]);
        }else{
            return response()->json(['status'=>'error','msg' => 'Internal Error. Please try again.']);
        }

    }
    public function showBackupCode(Request $request){
        $code = LoginSecurity::where('user_id',Auth::guard('practitioner')->user()->id)->first()->backup_code;
        if($code){
            return response()->json(['status'=>'success','msg' => 'Your backup codes:','code' => $code]);
        }else{
            return response()->json(['status'=>'error','msg' => 'You don not have backup code. Please generate first and try again.']);
        }

    }
    public function backUpLogin(Request $request){
        $code = $request->get('code');
        $backup_code = json_decode(LoginSecurity::where('user_id',User::where('email',Cookie::get('email'))->first()->id)->first()->backup_code,true);
        if(in_array($code,$backup_code)){
            unset($backup_code[array_search($code,$backup_code)]);

            // push back into the database
            $new_backup_code = json_encode(array_values($backup_code));
            LoginSecurity::where('user_id',User::where('email',Cookie::get('email'))->first()->id)->update([
                'backup_code' => $new_backup_code
            ]);
            return response()->json(['msg' => 'success','email'=>Cookie::get('email'),'password'=>Cookie::get('password')]);
        }else{
            return response()->json(['msg' => 'error']);
        }
    }
    public function updateRecoverPhone(Request $request){
        $phone = $request->get('phone');
        $user = Auth::guard('practitioner')->user();
        if($phone){
            if($user->loginSecurity()->exists()){
                User::where('id',Auth::guard('practitioner')->user()->id)->update([
                    'mobile' => $phone
                ]);
                return response()->json(['msg' => 'success','phone'=>$phone]);
            }
        }
    }
    public function verifyLastThreeNum(Request $request){
        $number = $request->get('number');
        $user = User::where('email',Cookie::get('email'))->first();
        $recovery_phone = $user->mobile;
        if(!$recovery_phone){
            return response()->json(['status' => 'error','msg' => 'You have not set recovery phone']);
        }else if(substr($recovery_phone,-3) == $number){
//            if(!$user->loginSecurity->backup_code){
//                return response()->json(['status' => 'error','msg' => 'You do not have backup code']);
//            }else{
                $verification_code = mt_rand(10000000, 99999999);
                $expire_time = Carbon::now()->addMinutes(30)->toDateTimeString();
                LoginSecurity::where('user_id',$user->id)->update([
                    'verification_code' => $verification_code,
                    'code_expire' => $expire_time,
                ]);
                $account_sid = config("app.twilio_account_sid");
                $auth_token = config("app.twilio_auth_token");
                $client = new Client($account_sid, $auth_token);
                $client->messages->create($recovery_phone,
                    ['from' => 'Medlab', 'body' => 'A request to reset your 2 factor Authentication on the Medlab website was submitted. Your reset code is '.$verification_code.'. This code is valid for the next 30 minutes'] );
                return response()->json(['status' => 'success','msg' => 'We have sent the reset code to your phone']);
//            }

        }else{
            return response()->json(['status' => 'error','msg' => 'Your input does not match our record, please check and try again']);
        }
    }

    public function verifyVerificationCode(Request $request){
        $code = $request->get('code');
        $user = User::where('email',Cookie::get('email'))->first();
        if($user->loginSecurity->verification_code != $code){
            return response()->json(['status' => 'error','msg' => 'Reset code mismatch.']);
        }elseif(!Carbon::now()->lte(Carbon::parse($user->loginSecurity->code_expire))){
            return response()->json(['status' => 'error','msg' => 'Reset code expired.']);
        }else{
            LoginSecurity::where('user_id',$user->id)->delete();
            return response()->json(['status' => 'success','msg' => 'We have disabled your 2 factor authentication','email'=>Cookie::get('email'),'password'=>Cookie::get('password')]);
        }
    }
}
