<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CheckStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        //If the status is not approved redirect to login
        if(Auth::guard('practitioner')->check() && Auth::guard('practitioner')->user()->status != '1'){
            Auth::guard('practitioner')->logout();
//            return $response;
        }else if(Auth::guard('patient')->check() && Auth::guard('patient')->user()->status != '1'){
            Auth::guard('patient')->logout();
//            return $response;
        }
        return $response;
    }
}
