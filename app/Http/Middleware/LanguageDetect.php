<?php

namespace App\Http\Middleware;

use App;
use Closure;
use Session;

class LanguageDetect
{
    /**
     * Handle an incoming request.
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Session::has('locale')) {
            $lang = Session::get('locale');
            App::setLocale($lang);
        }

        return $next($request);
    }
}
