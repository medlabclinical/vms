<?php

namespace App\Http\Middleware;

use App\Models\UnleashedCustomer;
use Closure;
use Auth;
use Illuminate\Support\Facades\Cookie;

class CheckShoppingStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        if(Auth::guard('practitioner')->check() && Cookie::has('business')){
            if(in_array(UnleashedCustomer::where('guid',json_decode(Cookie::get('business'),true)['guid'])->first()->status,[1,2])){
                return $response;
            }else{
                return redirect('/');
            }
        }else{
            return redirect('/');
        }

    }
}
