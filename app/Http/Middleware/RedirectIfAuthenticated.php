<?php

namespace App\Http\Middleware;

use App\Models\UnleashedCustomer;
use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
//        if (Auth::guard($guard)->check()) {
//            return redirect('/home');
//        }
//        abort(404);
//        if($request->has('practitioner')){
//            $practitioner = UnleashedCustomer::where('guid',$request->get('practitioner'))->first();
//            session(['practitioner' => $practitioner]);
//        }

        return $next($request);
    }
}
