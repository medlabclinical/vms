<?php

namespace App\Http\Middleware;

use App\Models\UnleashedCustomer;
use Closure;
use Auth;
use Illuminate\Support\Facades\Cookie;

class CheckLoginStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        if(Auth::guard('practitioner')->check() && Cookie::has('business')){
                return $response;

        }else{
            return redirect('/');
        }

    }
}
