<?php namespace App\Securepay;

use App\Models\ShoppingCartModel;
use App\Models\ShoppingCartStoreCardModel;
use DateTimeZone;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class Payment
{
    /**
     *
     *  Execute a securepay XML payment
     *
     * @param string $sc_amount [ eg: 12352 in cents for $123.52 ]
     * @param string $sc_reference [ max chars 60 - A - Z, 0 - 9, hyphen, underscore and hashtags
     * @param string $sc_card_number [ 4444333322221111 ]
     * @param string $sc_card_month [ 01 - 12 ]
     * @param string $sc_card_year [ 21 for the year 2021 ]
     * @param string $sc_card_cvv [ 1234 ]
     *
     * @return mixed
     * 'receipt_number' => transaction receipt number,
     * 'status'         => textual description of the response eg: approved or declined
     * 'card_number'    => masked credit card number posted
     * 'card_expiry'    => expiry date of credit card posted
     * 'xml'            => response in converted object
     * 'xml_body'       => response in XML format
     * 'status_code'    => html statuscode of the request 200, 404, 500
     *
     *
     * [Transaction Types]
     * 0 Standard Payment
     * 4 Refund
     * 10 Preauthorise
     * 11 Preauthorise Complete
     * 15 Direct Debit
     * 17 Direct Credit
     * 22 FraudGuard Only (No Transaction)
     * 40 Account Verification
     * 41 Preauthorisation Increase
     * 42 Preauthorisation Cancellation
     *
     * https://auspost.com.au/payments/docs/securepay/resources/Secure_XML_API_Integration_Guide.pdf
     *
     * [Sample returned XMLbody ]
     * <?xml version="1.0" encoding="UTF-8" standalone="no"?>
     * <SecurePayMessage>
     *   <MessageInfo>
     *       <messageID>32c07118-b81f-4430-bec0-d00cc2</messageID>
     *       <messageTimestamp>20200110232234622000+600</messageTimestamp>
     *       <apiVersion>xml-4.2</apiVersion>
     *   </MessageInfo>
     *   <RequestType>Payment</RequestType>
     *   <MerchantInfo>
     *       <merchantID>54Y0001</merchantID>
     *   </MerchantInfo>
     *   <Status>
     *       <statusCode>000</statusCode>
     *       <statusDescription>Normal</statusDescription>
     *   </Status>
     *   <Payment>
     *       <TxnList count="1">
     *           <Txn ID="1">
     *               <txnType>0</txnType>
     *               <txnSource>23</txnSource>
     *               <amount>24710</amount>
     *               <currency>AUD</currency>
     *               <purchaseOrderNo>V-pretend-6</purchaseOrderNo>
     *               <approved>Yes</approved>
     *               <responseCode>00</responseCode>
     *               <responseText>Approved</responseText>
     *               <thinlinkResponseCode>100</thinlinkResponseCode>
     *               <thinlinkResponseText>000</thinlinkResponseText>
     *               <thinlinkEventStatusCode>000</thinlinkEventStatusCode>
     *               <thinlinkEventStatusText>Normal</thinlinkEventStatusText>
     *               <settlementDate>20201002</settlementDate>
     *               <txnID>783329</txnID>
     *               <CreditCardInfo>
     *                   <pan>444433...111</pan>
     *                   <expiryDate>02/21</expiryDate>
     *                   <cardType>6</cardType>
     *                   <cardDescription>Visa</cardDescription>
     *               </CreditCardInfo>
     *           </Txn>
     *       </TxnList>
     *   </Payment>
     * </SecurePayMessage>
     *
     */
    public static function xmlPayment($sc_amount_in_cents, $sc_reference, $sc_card_number, $sc_card_month, $sc_card_year, $sc_card_cvv)
    {
        $sc_reference = substr($sc_reference, 0, 60);
        $sc_reference = preg_replace("/[^a-zA-Z0-9-_#]/", "", $sc_reference);

        $sc_amount_in_cents = preg_replace("/[^0-9]/", "", $sc_amount_in_cents);

        $client = new Client(); //GuzzleHttp\Client
        $host = config("app.securepay_host");

        $headers = array(
            'Content-Type'       => 'application/xml',
        );

        $messageid = (string) Str::uuid();
        $messageid = substr($messageid, 0, 30);

        $timestamp = date("YdmHisv") . '000+600';
        $raw_data = '<?xml version="1.0" encoding="UTF-8"?>
                        <SecurePayMessage>
                            <MessageInfo>
                                <messageID>' . $messageid . '</messageID>
                                <messageTimestamp>' . $timestamp . '</messageTimestamp>
                                <timeoutValue>60</timeoutValue>
                                <apiVersion>xml-4.2</apiVersion>
                            </MessageInfo>
                            <MerchantInfo>
                                <merchantID>' . config("app.securepay_merchantid") . '</merchantID>
                                <password>' . config("app.securepay_password")  . '</password>
                            </MerchantInfo>
                            <RequestType>Payment</RequestType>
                            <Payment>
                                <TxnList count="1">
                                    <Txn ID="1">
                                        <txnType>0</txnType>
                                        <txnSource>23</txnSource>
                                        <amount>' . $sc_amount_in_cents . '</amount>
                                        <currency>AUD</currency>
                                        <purchaseOrderNo>' . $sc_reference . '</purchaseOrderNo>
                                        <CreditCardInfo>
                                            <cardNumber>' . $sc_card_number . '</cardNumber>
                                            <expiryDate>' . $sc_card_month . '/' . $sc_card_year . '</expiryDate>
                                            <cvv>' . $sc_card_cvv . '</cvv>
                                        </CreditCardInfo>
                                    </Txn>
                                </TxnList>
                            </Payment>
                        </SecurePayMessage>';

        // Log::debug($raw_data);

        $response = $client->request('POST', $host, [
            'headers' => $headers,
            'body' => $raw_data
        ]);

        if (!$response->getStatusCode() == 200) {
            return array('status' => "Gateway is currently offline");
        }

        $statusCode    = $response->getStatusCode();
        $xmlBody       = $response->getBody()->read(3072);
        $xml           = simplexml_load_string($xmlBody);

        if (isset($xml->Payment->TxnList->Txn))
        {
            $receiptNumber = current($xml->Payment->TxnList->Txn->txnID);
            $status        = current($xml->Payment->TxnList->Txn->responseText);
            $cardNumber    = current($xml->Payment->TxnList->Txn->CreditCardInfo->pan);
            $cardExpiry    = current($xml->Payment->TxnList->Txn->CreditCardInfo->expiryDate);
        }
        else
        {
            $status = "Credit Card Declined";
            $cardNumber = '';
            $cardExpiry = '';
            $receiptNumber = '';
        }

        if ($status == "Invalid Credit Card Number") {
            $status = "Credit Card Declined";
        }

        return array(
                        'receipt_number' => $receiptNumber,
                        'status'         => $status,
                        'card_number'    => $cardNumber,
                        'card_expiry'    => $cardExpiry,
                        'xml'            => $xml,
                        'xml_body'       => $xmlBody,
                        'status_code'    => $statusCode
                    );
    }

    /**
     * StoreCard for future payments
     * https://auspost.com.au/payments/docs/securepay/resources/API_Card_Storage_and_Scheduled_Payments.pdf
     *
     *   <?xml version="1.0" encoding="UTF-8"?>
     *   <SecurePayMessage>
     *       <MessageInfo>
     *           <messageID>8af793f9af34bea0ecd7eff71b37ef</messageID>
     *           <messageTimestamp>20040710044409342000+600</messageTimestamp>
     *           <timeoutValue>60</timeoutValue>
     *           <apiVersion>spxml-3.0</apiVersion>
     *       </MessageInfo>
     *       <MerchantInfo>
     *           <merchantID>ABC0001</merchantID>
     *           <password>abc123</password>
     *       </MerchantInfo>
     *       <RequestType>Periodic</RequestType>
     *       <Periodic>
     *           <PeriodicList count="1">
     *               <PeriodicItem ID="1">
     *                   <actionType>add</actionType>
     *                   <clientID>test3</clientID>
     *                   <CreditCardInfo>
     *                       <cardNumber>4444333322221111</cardNumber>
     *                       <cvv>123</cvv>
     *                       <expiryDate>09/25</expiryDate>
     *                   </CreditCardInfo>
     *                   <amount>1100</amount>
     *                   <periodicType>4</periodicType>
     *               </PeriodicItem>
     *           </PeriodicList>
     *       </Periodic>
     *   </SecurePayMessage>
     *
     *  Response returned:
     *
     *   <?xml version="1.0" encoding="UTF-8"?>
     *   <SecurePayMessage>
     *       <MessageInfo>
     *           <messageID>8af793f9af34bea0ecd7eff71b37ef</messageID>
     *           <messageTimestamp>20040710144410220000+600</messageTimestamp>
     *           <apiVersion>spxml-3.0</apiVersion>
     *       </MessageInfo>
     *       <RequestType>Periodic</RequestType>
     *       <MerchantInfo>
     *           <merchantID>ABC0001</merchantID>
     *       </MerchantInfo>
     *       <Status>
     *           <statusCode>0</statusCode>
     *           <statusDescription>Normal</statusDescription>
     *       </Status>
     *       <Periodic>
     *           <PeriodicList count="1">
     *               <PeriodicItem ID="1">
     *                   <actionType>add</actionType>
     *                   <clientID>test3</clientID>
     *                   <responseCode>00</responseCode>
     *                   <responseText>Successful</responseText>
     *                   <successful>yes</successful>
     *                   <CreditCardInfo>
     *                       <pan>444433...111</pan>
     *                       <expiryDate>09/25</expiryDate>
     *                       <recurringFlag>no</recurringFlag>
     *                   </CreditCardInfo>
     *                   <amount>1100</amount>
     *                   <periodicType>4</periodicType>
     *               </PeriodicItem>
     *           </PeriodicList>
     *       </Periodic>
     *   </SecurePayMessage>
     *
     */
    public static function createStoredCard($customer_code, $sc_card_number, $sc_card_month, $sc_card_year, $sc_card_cvv, $sc_card_name)
    {
        $first_two_digits = substr($sc_card_number, 0, 2);
        $last_four_digits = substr($sc_card_number, -4);
        $id = DB::table('shopping_cart_storecard')->insertGetId(
            [
                'card_number'   => $first_two_digits . '...' . $last_four_digits,
                'card_name'     => $sc_card_name,
                'card_expiry'   => $sc_card_month . '/' . $sc_card_year,
                'customer_code' => $customer_code,
                'created_at'    => date("Y-m-d H:i:s")
            ]
        );

        $payee_reference = $customer_code . '-' . $id;

        $client = new Client(); //GuzzleHttp\Client
        $host = config("app.securepay_periodic_host");

        $headers = array(
            'Content-Type'       => 'application/xml',
        );

        $messageid = (string) Str::uuid();
        $messageid = substr($messageid, 0, 30);

        $timestamp = date("YdmHisv") . '000+600';
        $raw_data = '<?xml version="1.0" encoding="UTF-8"?>
                        <SecurePayMessage>
                            <MessageInfo>
                                <messageID>' . $messageid . '</messageID>
                                <messageTimestamp>' . $timestamp . '</messageTimestamp>
                                <timeoutValue>60</timeoutValue>
                                <apiVersion>spxml-3.0</apiVersion>
                            </MessageInfo>
                            <MerchantInfo>
                                <merchantID>' . config("app.securepay_merchantid") . '</merchantID>
                                <password>' . config("app.securepay_password")  . '</password>
                            </MerchantInfo>
                            <RequestType>Periodic</RequestType>
                            <Periodic>
                                <PeriodicList count="1">
                                    <PeriodicItem ID="1">
                                        <actionType>add</actionType>
                                        <clientID>' . $payee_reference . '</clientID>
                                        <CreditCardInfo>
                                            <cardNumber>' . $sc_card_number . '</cardNumber>
                                            <cvv>' . $sc_card_cvv . '</cvv>
                                            <expiryDate>' . $sc_card_month . '/' . $sc_card_year . '</expiryDate>
                                        </CreditCardInfo>
                                        <amount>1</amount>
                                        <periodicType>4</periodicType>
                                    </PeriodicItem>
                                </PeriodicList>
                            </Periodic>
                        </SecurePayMessage>';

        // Log::debug($raw_data);

        $response = $client->request('POST', $host, [
            'headers' => $headers,
            'body' => $raw_data
        ]);

        if (!$response->getStatusCode() == 200) {
            return array('status' => "Gateway is currently offline");
        }

        $statusCode    = $response->getStatusCode();
        $xmlBody       = $response->getBody()->read(3072);
        $xml           = simplexml_load_string($xmlBody);

        if (isset($xml->Periodic->PeriodicList->PeriodicItem))
        {
            $securepay_reference = current($xml->Periodic->PeriodicList->PeriodicItem->clientID);
            $status              = current($xml->Periodic->PeriodicList->PeriodicItem->responseText);
            $cc_number           = current($xml->Periodic->PeriodicList->PeriodicItem->CreditCardInfo->pan);

            if ($status == "Successful") {
                DB::table('shopping_cart_storecard')
                  ->where('id', $id)
                  ->update(['securepay_reference' => $securepay_reference, 'card_number' => $cc_number]);
            } else {
                Log::notice("Storing credit card request - failed : " . print_r($status, true));
                if ($status == "Invalid Credit Card Number") {
                    $status = "Credit Card Declined";
                }
                return array('status' => $status);
            }
        }
        else
        {
            // Log::debug(print_r($xml, true));
            return array('status' => "Gateway unsuccessful.");
        }

        return true;
    }

    /**
     *   Process Stored credit card
     *
     *   stored_card_id : refer to the shopping_cart_storecard table for id
     *   sc_reference : unique id to distinguish the payment
     *   sc_amount_in_cents : the total payable in cents.
     */
    public static function processStoredCard($stored_card_id, $sc_reference, $sc_amount_in_cents)
    {
        $sc_reference = substr($sc_reference, 0, 60);
        $sc_reference = preg_replace("/[^a-zA-Z0-9-_#]/", "", $sc_reference);

        // Get the securepay_reference
        $info = ShoppingCartStoreCardModel::where('id', $stored_card_id)
                                ->select('securepay_reference')
                                ->first();

        if (!isset($info->securepay_reference)) {
            return array('status' => "Invalid stored credit card");
        }

        $client = new Client(); //GuzzleHttp\Client
        $host = config("app.securepay_periodic_host");

        $headers = array(
            'Content-Type'       => 'application/xml',
        );

        $messageid = (string) Str::uuid();
        $messageid = substr($messageid, 0, 30);

        $timestamp = date("YdmHisv") . '000+600';
        $raw_data = '<?xml version="1.0" encoding="UTF-8"?>
                        <SecurePayMessage>
                            <MessageInfo>
                                <messageID>' . $messageid . '</messageID>
                                <messageTimestamp>' . $timestamp . '</messageTimestamp>
                                <timeoutValue>60</timeoutValue>
                                <apiVersion>spxml-3.0</apiVersion>
                            </MessageInfo>
                            <MerchantInfo>
                                <merchantID>' . config("app.securepay_merchantid") . '</merchantID>
                                <password>' . config("app.securepay_password")  . '</password>
                            </MerchantInfo>
                            <RequestType>Periodic</RequestType>
                            <Periodic>
                                <PeriodicList count="1">
                                    <PeriodicItem ID="1">
                                        <actionType>trigger</actionType>
                                        <transactionReference>' . $sc_reference . '</transactionReference>
                                        <clientID>' . $info->securepay_reference . '</clientID>
                                        <amount>' . $sc_amount_in_cents .'</amount>
                                    </PeriodicItem>
                                </PeriodicList>
                            </Periodic>
                        </SecurePayMessage>';

        $response = $client->request('POST', $host, [
            'headers' => $headers,
            'body' => $raw_data
        ]);

        if (!$response->getStatusCode() == 200) {
            return array('status' => "Gateway is currently offline");
        }

        $statusCode    = $response->getStatusCode();
        $xmlBody       = $response->getBody()->read(3072);
        $xml           = simplexml_load_string($xmlBody);

        $securepay_reference = current($xml->Periodic->PeriodicList->PeriodicItem->clientID);
        $status              = current($xml->Periodic->PeriodicList->PeriodicItem->responseText);
        $receiptNumber       = current($xml->Periodic->PeriodicList->PeriodicItem->txnID);
        $cc_number           = current($xml->Periodic->PeriodicList->PeriodicItem->CreditCardInfo->pan);

        if ($status == "Approved") {
            return array('status' => "Approved", "receipt_number" => $receiptNumber);
        } else {
            Log::notice("Trigger payment stored credit card - failed " . $sc_reference . " for " . $stored_card_id . ' with status ' . $status);
            // Log::debug(print_r($xml, true));
            return array('status' => $status);
        }
    }

    public static function deleteStoredCard($stored_card_id)
    {
        // Get the securepay_reference
        $info = ShoppingCartStoreCardModel::where('id', $stored_card_id)
                                ->select('securepay_reference')
                                ->withTrashed()
                                ->first();

        if (!isset($info->securepay_reference)) {
            return false;
        }

        $client = new Client(); //GuzzleHttp\Client
        $host = config("app.securepay_periodic_host");

        $headers = array(
            'Content-Type'       => 'application/xml',
        );

        $messageid = (string) Str::uuid();
        $messageid = substr($messageid, 0, 30);

        $timestamp = date("YdmHisv") . '000+600';
        $raw_data = '<?xml version="1.0" encoding="UTF-8"?>
                        <SecurePayMessage>
                            <MessageInfo>
                                <messageID>' . $messageid . '</messageID>
                                <messageTimestamp>' . $timestamp . '</messageTimestamp>
                                <timeoutValue>60</timeoutValue>
                                <apiVersion>spxml-3.0</apiVersion>
                            </MessageInfo>
                            <MerchantInfo>
                                <merchantID>' . config("app.securepay_merchantid") . '</merchantID>
                                <password>' . config("app.securepay_password")  . '</password>
                            </MerchantInfo>
                            <RequestType>Periodic</RequestType>
                            <Periodic>
                                <PeriodicList count="1">
                                    <PeriodicItem ID="1">
                                        <actionType>delete</actionType>
                                        <clientID>' . $info->securepay_reference . '</clientID>
                                    </PeriodicItem>
                                </PeriodicList>
                            </Periodic>
                        </SecurePayMessage>';

        // Log::debug($raw_data);

        $response = $client->request('POST', $host, [
            'headers' => $headers,
            'body' => $raw_data
        ]);

        if (!$response->getStatusCode() == 200) return false;

        $statusCode = $response->getStatusCode();
        $xmlBody    = $response->getBody()->read(3072);
        $xml        = simplexml_load_string($xmlBody);
        $status     = current($xml->Periodic->PeriodicList->PeriodicItem->responseText);

        if ($status == "Successful") {
            DB::table('shopping_cart_storecard')
              ->where('id', $stored_card_id)
              ->update(['deleted_at' => date("Y-m-d H:i:s")]);
        } else {
            Log::notice("Delete credit card request (" . $stored_card_id . ") - failed : " . print_r($status, true));
        }

        return true;
    }
}
