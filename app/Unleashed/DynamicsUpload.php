<?php
namespace App\Unleashed;

use DateTime;
use DateTimeZone;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class DynamicsUpload
{


    public function __construct()
    {
        // init
    }

    public function uploadCompaniesTest()
    {

        $file = fopen('app/Unleashed/test.csv', "r");
        while ( ($data = fgetcsv($file)) !==FALSE ){

            print_r('Success');

        }
        fclose($file);


    }

    public function uploadCompanies()
    {

        $json = File::get("database/data/api_accounts.json");
        $upload = json_decode($json, true);

        $i = 0;

        foreach($upload['data'] as $item) {

            $i = $i + 1;
            var_dump($i);

            //DYNAMICS DATA HAS UNLEASHED CODE
            if ($item['mc_unleashedcode'] != null && $item['accountid'] != null) {


                DB::table('ucustomers')->updateOrInsert(
                    ['customer_code' => $item['mc_unleashedcode']],
                    [
                        'dynamics_guid' => $item['accountid'],
                        'customer_code' => $item['mc_unleashedcode'],
                        'customer_name' => $item['name'],
                        'phone_number' => $item['telephone1'],
                        'fax_number' => $item['fax'],
                        'email' => $item['emailaddress1'],
                        'website' => $item['websiteurl'],
                        'override_reps' => '[]',
                        'custom_code_generate' => '200000',
                        'register_status' => '0',
                        'drug_tier' => '5',
                        'drug_growth_rate' => '0',
                        'status' => '1',
                        'updated_at' => date('Y-m-d H:i')
                    ]
                );

                $customer_guid = DB::table('ucustomers')->where('customer_code', '=', $item['mc_unleashedcode'])->first();

                if ($item['mc_renerwholesalernumber'] != null) {
                    DB::table('customer_wholesalers')->updateOrInsert(
                        ['customer_id' => $customer_guid->guid, 'registration_number'=> $item['mc_renerwholesalernumber']],
                    [
                        'customer_id'           => $customer_guid->guid,
                        'wholesaler_id'         => '11',
                        'registration_number'   => $item['mc_renerwholesalernumber'],
                        'created_at'            => date('Y-m-d H:i'),
                        'updated_at'            => date('Y-m-d H:i')
                    ]
                    );
                } elseif ($item['mc_apiwholesalernumber'] != null) {

                    DB::table('customer_wholesalers')->updateOrInsert(
                        ['customer_id' => $customer_guid->guid, 'registration_number'=> $item['mc_apiwholesalernumber']],
                        [
                            'customer_id'           => $customer_guid->guid,
                            'wholesaler_id'         => '13',
                            'registration_number'   => $item['mc_apiwholesalernumber'],
                            'created_at'            => date('Y-m-d H:i'),
                            'updated_at'            => date('Y-m-d H:i')
                        ]
                    );
                } elseif ($item['mc_symbionwholesalernumber'] != null) {
                    DB::table('customer_wholesalers')->updateOrInsert(
                        ['customer_id' => $customer_guid->guid, 'registration_number'=> $item['mc_symbionwholesalernumber']],
                        [
                            'customer_id'           => $customer_guid->guid,
                            'wholesaler_id'         => '14',
                            'registration_number'   => $item['mc_symbionwholesalernumber'],
                            'created_at'            => date('Y-m-d H:i'),
                            'updated_at'            => date('Y-m-d H:i')
                        ]
                    );
                } elseif ($item['mc_sigmawholesalernumber'] != null) {
                    DB::table('customer_wholesalers')->updateOrInsert(
                        ['customer_id' => $customer_guid->guid, 'registration_number'=> $item['mc_sigmawholesalernumber']],
                        [
                            'customer_id'           => $customer_guid->guid,
                            'wholesaler_id'         => '12',
                            'registration_number'   => $item['mc_sigmawholesalernumber'],
                            'created_at'            => date('Y-m-d H:i'),
                            'updated_at'            => date('Y-m-d H:i')
                        ]
                    );
                }


            } elseif ($item['mc_unleashedcode'] == null && $item['accountid'] != null) {
                //Dynamics Data has no unleashed code

                DB::table('ucustomers')->updateOrInsert(
                    ['dynamics_guid' => $item['accountid']],
                    [
                        'dynamics_guid' => $item['accountid'],
                        'customer_code' => $item['mc_unleashedcode'],
                        'customer_name' => $item['name'],
                        'guid'          => $item['accountid'],
                        'phone_number' => $item['telephone1'],
                        'fax_number' => $item['fax'],
                        'email' => $item['emailaddress1'],
                        'website' => $item['websiteurl'],
                        'override_reps' => '[]',
                        'custom_code_generate' => '200000',
                        'register_status' => '0',
                        'drug_tier' => '5',
                        'drug_growth_rate' => '0',
                        'status' => '0',
                        'updated_at' => date('Y-m-d H:i')
                    ]
                );


                if ($item['mc_renerwholesalernumber'] != null) {
                    DB::table('customer_wholesalers')->updateOrInsert(
                        ['customer_id' => $item['accountid'], 'registration_number'=> $item['mc_renerwholesalernumber']],
                        [
                            'customer_id'           => $item['accountid'],
                            'wholesaler_id'         => '11',
                            'registration_number'   => $item['mc_renerwholesalernumber'],
                            'created_at'            => date('Y-m-d H:i'),
                            'updated_at'            => date('Y-m-d H:i')
                        ]
                    );
                } elseif ($item['mc_apiwholesalernumber'] != null) {
                    DB::table('customer_wholesalers')->updateOrInsert(
                        ['customer_id' => $item['accountid'], 'registration_number'=> $item['mc_apiwholesalernumber']],
                        [
                            'customer_id'           => $item['accountid'],
                            'wholesaler_id'         => '13',
                            'registration_number'   => $item['mc_apiwholesalernumber'],
                            'created_at'            => date('Y-m-d H:i'),
                            'updated_at'            => date('Y-m-d H:i')
                        ]
                    );
                } elseif ($item['mc_symbionwholesalernumber'] != null) {
                    DB::table('customer_wholesalers')->updateOrInsert(
                        ['customer_id' => $item['accountid'], 'registration_number'=> $item['mc_symbionwholesalernumber']],
                        [
                            'customer_id'           => $item['accountid'],
                            'wholesaler_id'         => '14',
                            'registration_number'   => $item['mc_symbionwholesalernumber'],
                            'created_at'            => date('Y-m-d H:i'),
                            'updated_at'            => date('Y-m-d H:i')
                        ]
                    );
                } elseif ($item['mc_sigmawholesalernumber'] != null) {
                    DB::table('customer_wholesalers')->updateOrInsert(
                        ['customer_id' => $item['accountid'], 'registration_number'=> $item['mc_sigmawholesalernumber']],
                        [
                            'customer_id'           => $item['accountid'],
                            'wholesaler_id'         => '12',
                            'registration_number'   => $item['mc_sigmawholesalernumber'],
                            'created_at'            => date('Y-m-d H:i'),
                            'updated_at'            => date('Y-m-d H:i')
                        ]
                    );
                }
            } else {

                $test = "Nothing";
                var_dump($test);
                 DB::table('ucustomers')->Insert(
                    [
                        'dynamics_guid' => $item['accountid'],
                        'guid'          => $item['accountid'],
                        'customer_code' => $item['mc_unleashedcode'],
                        'customer_name' => $item['name'],
                        'phone_number' => $item['telephone1'],
                        //'fax_number' => $item['fax'],
                        'email' => $item['emailaddress1'],
                        //'website' => $item['websiteurl'],
                        'override_reps' => '[]',
                        'custom_code_generate' => '200000',
                        'register_status' => '0',
                        'drug_tier' => '5',
                        'drug_growth_rate' => '0',
                        'status' => '0',
                        'updated_at' => date('Y-m-d H:i')
                    ]
                );


                if ($item['mc_renerwholesalernumber'] != null) {
                    DB::table('customer_wholesalers')->updateOrInsert(
                        ['customer_id' => $item['accountid'], 'registration_number'=> $item['mc_renerwholesalernumber']],
                        [
                            'customer_id'           => $item['accountid'],
                            'wholesaler_id'         => '11',
                            'registration_number'   => $item['mc_renerwholesalernumber'],
                            'created_at'            => date('Y-m-d H:i'),
                            'updated_at'            => date('Y-m-d H:i')
                        ]
                    );
                } elseif ($item['mc_apiwholesalernumber'] != null) {
                    DB::table('customer_wholesalers')->updateOrInsert(
                        ['customer_id' => $item['accountid'], 'registration_number'=> $item['mc_apiwholesalernumber']],
                        [
                            'customer_id'           => $item['accountid'],
                            'wholesaler_id'         => '13',
                            'registration_number'   => $item['mc_apiwholesalernumber'],
                            'created_at'            => date('Y-m-d H:i'),
                            'updated_at'            => date('Y-m-d H:i')
                        ]
                    );
                } elseif ($item['mc_symbionwholesalernumber'] != null) {
                    DB::table('customer_wholesalers')->updateOrInsert(
                        ['customer_id' => $item['accountid'], 'registration_number'=> $item['mc_symbionwholesalernumber']],
                        [
                            'customer_id'           => $item['accountid'],
                            'wholesaler_id'         => '14',
                            'registration_number'   => $item['mc_symbionwholesalernumber'],
                            'created_at'            => date('Y-m-d H:i'),
                            'updated_at'            => date('Y-m-d H:i')
                        ]
                    );
                } elseif ($item['mc_sigmawholesalernumber'] != null) {
                    DB::table('customer_wholesalers')->updateOrInsert(
                        ['customer_id' => $item['accountid'], 'registration_number'=> $item['mc_sigmawholesalernumber']],
                        [
                            'customer_id'           => $item['accountid'],
                            'wholesaler_id'         => '12',
                            'registration_number'   => $item['mc_sigmawholesalernumber'],
                            'created_at'            => date('Y-m-d H:i'),
                            'updated_at'            => date('Y-m-d H:i')
                        ]
                    );
                }
            }
        }

    }

    public function uploadInteractions()
    {

        $json = File::get("database/data/accounts.json");
        $data = json_decode($json);

        foreach($data as $item) {

            DB::table('ucustomers')->updateOrInsert(
                ['customer_code' => $item->mc_unleashedcode, 'dynamics_guid' => $item->accountid],
                [
                    'customer_name'         => $item->name,
                    'phone_number'          => $item->telephone1,
                    'fax_number'            => $item->fax,
                    'email'                 => $item->emailaddress1,
                    'website'               => $item->websiteurl,
                    'override_reps'         => [],

                    'updated_at'            => date('Y-m-d H:i')

                ]
            );
        }

    }
}
