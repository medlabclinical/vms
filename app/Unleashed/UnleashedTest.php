<?php
namespace App\Unleashed;

use App\Unleashed\Helper as Helper;
use DateTime;
use DateTimeZone;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;

class UnleashedTest
{
    public function __construct()
    {
        // init
    }

    public function get($modifiedSince)
    {
        foreach ($this->api as $api) {
            $this->unleashed_id = $api['unleashed_id'];
            $this->auth_id      = $api['api_key'];
            $this->secret       = $api['api_secret'];

            $results = $this->fetch($modifiedSince);
            $paging  = $results['paging'];
            if ($paging->NumberOfPages > 1) {
                for ($page = 2; $page <= $paging->NumberOfPages; $page++) {
                    $this->fetch($modifiedSince, $page);
                }
            }
        }
    }

    /**
     * Fetch unleashed data
     *
     * @return array
     */
    public function fetch($modifiedSince = '2019-11-11', $page = 1, $pageSize = 300)
    {
        $results = array();
        $client = new Client(); //GuzzleHttp\Client
        $host = $this->host . '/' . $page;

        $query = array(
            'pageSize' => $pageSize,
            'modifiedSince' => $modifiedSince
        );

        $http_query = http_build_query($query);
        $signature = hash_hmac('sha256', $http_query, $this->secret, true);

        $headers = array(
            'Content-Type'       => 'application/json',
            'Accept'             => 'application/json',
            'api-auth-signature' => base64_encode($signature),
            'api-auth-id'        => $this->auth_id
        );

        $response = $client->get(
            $host, [
                'headers' => $headers,
                'query'   => $query
            ]
        );

        if (!$response->getStatusCode() == 200) return false;

        $body = json_decode($response->getBody());

        $results['paging'] = $body->Pagination;
        $results['items'] = $body->Items;
        $this->update($body->Items);
        return $results;
    }

    /**
     * Upsert product
     *
     * @return void
     */
    public function update($items)
    {
        foreach($items as $item) {

            $uom = isset($item->UnitOfMeasure->Name) ? $item->UnitOfMeasure->Name : null;
            $product_group = isset($item->ProductGroup->GroupName) ? $item->ProductGroup->GroupName : null;
            $supplier_code = isset($item->Supplier->SupplierCode) ? $item->Supplier->SupplierCode : null;

            DB::table('products')->updateOrInsert(
                ['product_code' => $item->ProductCode, 'unleashed_id' => $this->unleashed_id],
                [
                    'product_description' => $item->ProductDescription,
                    'barcode' => $item->Barcode,
                    'uom' => $uom,

                    // measurements
//                    'width' => $item->Width,
//                    'height' => $item->Height,
//                    'depth' => $item->Depth,
//                    'weight' => $item->Weight,

                    // Pricing
                    'last_cost' => $item->LastCost,
                    'customer_sell_price' => $item->CustomerSellPrice,
                    'default_purchase_price' => $item->DefaultPurchasePrice,
                    'default_sell_price' => $item->DefaultSellPrice,
                    'average_land_price' => $item->AverageLandPrice,
                    'sell_price_1' => $item->SellPriceTier1->Value,
                    'sell_price_2' => $item->SellPriceTier2->Value,
                    'sell_price_3' => $item->SellPriceTier3->Value,
                    'sell_price_4' => $item->SellPriceTier4->Value,
                    'sell_price_5' => $item->SellPriceTier5->Value,
                    'sell_price_6' => $item->SellPriceTier6->Value,
                    'sell_price_7' => $item->SellPriceTier7->Value,
                    'sell_price_8' => $item->SellPriceTier8->Value,
                    'sell_price_9' => $item->SellPriceTier9->Value,
                    'sell_price_10' => $item->SellPriceTier10->Value,
                    'minimum_sell_price' => $item->MinimumSellPrice,
                    'minimum_sell_quantity' => $item->MinimumSaleQuantity,
                    'minimum_order_quantity' => $item->MinimumOrderQuantity,
                    'tax_rate' => $item->XeroTaxRate,
                    'taxable_sales' => $item->TaxableSales,
                    'taxable_purchase' => $item->TaxablePurchase,

                    // categories / grouping
                    'product_group' => $product_group,
                    'supplier_name' => $supplier_code,
                    'is_component' => $item->IsComponent,
                    'is_assembled_product' => $item->IsAssembledProduct,
                    'bin_location' => $item->BinLocation,
                    'is_batch_tracked' => $item->IsBatchTracked,
                    'is_serialized' => $item->IsSerialized,

                    // Statuses
                    'obsolete' => $item->Obsolete,
                    'sellable_in_unleashed' => $item->IsSellable,

                    // Notes
                    'notes' => $item->Notes,

                    // who did it and when
                    'unleashed_created_by' => $item->CreatedBy,
                    'unleashed_created_on' => Helper::jsonToDate($item->CreatedOn, 'Y-m-d H:i:s'),
                    'unleashed_modified_by' => $item->LastModifiedBy,
                    'unleashed_modified_on' => Helper::jsonToDate($item->LastModifiedOn, 'Y-m-d H:i:s'),
                    'unleashed_id' => $this->unleashed_id,

                    'guid' => $item->Guid,
                    'created_at' => date('Y-m-d H:i'),
                    'updated_at' => date('Y-m-d H:i')

                ]
            );
        }

    }
}
