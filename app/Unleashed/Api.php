<?php

namespace App\Unleashed;

use Illuminate\Database\Eloquent\Model;

class Api extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'unleashed';

    /**
     * Unleashed config key for auth_id
     *
     * @var string
     */
    private $_unleashed_auth_id = 'unleashed.auth_id';


    /**
     * Unleashed config key for secret
     *
     * @var string
     */
    private $_unleashed_secret = 'unleashed.secret';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'name', 'api_key'];

    /**
     * Gets api secret attributes from config
     *
     * @return string
     */
    public function getApiSecretAttribute()
    {
        $auth_ids = config($this->_unleashed_auth_id);
        $secrets  = config($this->_unleashed_secret);

        foreach ($auth_ids as $key => $id) {
            if ($this->api_key == $id && array_key_exists($key, $secrets)) {
                return $secrets[$key];
            }
        }

        return '';
    }

    /**
     * Gets unleashed id, key and secrets
     *
     * @return array
     */
    public static function getApi($unleashed_id = '')
    {
        $result = array();

        foreach (static::get() as $api) {
            $result[] = [
                'unleashed_id' => $api->id,
                'api_key'      => $api->api_key,
                'api_secret'   => $api->api_secret
            ];

            if($unleashed_id <> '' && $api->id == $unleashed_id) return [
                'unleashed_id' => $api->id,
                'api_key'      => $api->api_key,
                'api_secret'   => $api->api_secret
            ];
        }

        return $result;
    }

}