<?php
namespace App\Unleashed;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;
use App\Unleashed\Api as Api;
use App\Unleashed\Helper as Helper;

class StockOnHandWarehouse
{

    protected $resource = '/StockOnHand';

    public function __construct()
    {
        $this->host = config('unleashed.host').$this->resource;
        $this->api = Api::getApi();
    }

    public function get($guid, $unleashed_id)
    {
        foreach ($this->api as $api) {
            $this->unleashed_id = $api['unleashed_id'];
            $this->auth_id      = $api['api_key'];
            $this->secret       = $api['api_secret'];
            if ($unleashed_id == $this->unleashed_id) $this->fetch($guid);
        }
    }

    /**
     * Upsert stock on hand - on warehouse
     *
     * @return array
     */
    public function fetch($guid)
    {
        $results    = array();
        $client     = new Client(); //GuzzleHttp\Client
        $host       = $this->host . '/' . $guid.'/AllWarehouses';
        $query      = array();
        $http_query = http_build_query($query);
        $signature  = hash_hmac('sha256', $http_query, $this->secret, true);

        $headers = array(
            'Content-Type'       => 'application/json',
            'Accept'             => 'application/json',
            'api-auth-signature' => base64_encode($signature),
            'api-auth-id'        => $this->auth_id
        );

        $response = $client->get(
            $host, [
                'headers' => $headers,
                'query'   => $query
            ]
        );

        if (!$response->getStatusCode() == 200) return false;

        $body = json_decode($response->getBody());
        $results['items'] = $body->Items;
        $this->update($body->Items, $guid);
        return $results;
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function update($items, $guid)
    {
        foreach ($items as $item) {

            DB::table('stock_on_hand_warehouse')->updateOrInsert(
                ['product_guid' => $guid, 'warehouse_guid'=> $item->WarehouseId, 'unleashed_id' => $this->unleashed_id],
                [
                    'product_guid' => $guid,
                    'warehouse_guid'=> $item->WarehouseId,
                    'warehouse_name'=> $item->Warehouse,
                    'available_quantity'=> $item->AvailableQty,
                    'unleashed_id' => $this->unleashed_id,
                    'created_at'   => date('Y-m-d H:i'),
                    'updated_at'   => date('Y-m-d H:i')
                ]
            );
        }

    }

}
