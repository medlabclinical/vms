<?php
namespace App\Unleashed;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;
use App\Unleashed\Api as Api;
use App\Unleashed\Helper as Helper;

class Warehouses
{

    protected $resource = '/Warehouses';

    public function __construct()
    {
        $this->host = config('unleashed.host').$this->resource;
        $this->api = Api::getApi();
    }

    public function get($modifiedSince)
    {
        foreach ($this->api as $api) {
            $this->unleashed_id = $api['unleashed_id'];
            $this->auth_id      = $api['api_key'];
            $this->secret       = $api['api_secret'];

            $results = $this->fetch($modifiedSince);
            $paging  = $results['paging'];
            if ($paging->NumberOfPages > 1) {
                for ($page = 2; $page <= $paging->NumberOfPages; $page++) {
                    $this->fetch($modifiedSince, $page);
                }
            }

        }
    }

    /**
     * Fetch unleashed data
     *
     * @return array
     */
    public function fetch($modifiedSince = '2015-01-01', $page = 1, $pageSize = 500)
    {
        $results = array();
        $client = new Client(); //GuzzleHttp\Client
        $host = $this->host . '/' . $page;

        $query = array(
            'pageSize' => $pageSize,
            'modifiedSince' => $modifiedSince
        );

        $http_query = http_build_query($query);
        $signature = hash_hmac('sha256', $http_query, $this->secret, true);

        $headers = array(
            'Content-Type'       => 'application/json',
            'Accept'             => 'application/json',
            'api-auth-signature' => base64_encode($signature),
            'api-auth-id'        => $this->auth_id
        );

        $response = $client->get(
            $host, [
                'headers' => $headers,
                'query'   => $query
            ]
        );

        if (!$response->getStatusCode() == 200) return false;

        $body = json_decode($response->getBody());

        $results['paging'] = $body->Pagination;
        $results['items'] = $body->Items;
        $this->update($body->Items);

        return $results;
    }

    /**
     * Upsert warehouse details
     *
     * @return void
     */
    public function update($items)
    {
        foreach($items as $item) {

            DB::table('warehouses')->updateOrInsert(
                ['warehouse_code' => $item->WarehouseCode, 'unleashed_id' => $this->unleashed_id],
                [
                    'guid'           => $item->Guid,
                    'warehouse_code' => $item->WarehouseCode,
                    'warehouse_name' => $item->WarehouseName,
                    'street_no'      => $item->StreetNo,
                    'address_1'      => $item->AddressLine1,
                    'address_2'      => $item->AddressLine2,
                    'city'           => $item->City,
                    'region'         => $item->Region,
                    'country'        => $item->Country,
                    'contact_name'   => $item->ContactName,
                    'ddi_number'     => $item->DDINumber,
                    'fax_number'     => $item->FaxNumber,
                    'default'        => $item->IsDefault,
                    'mobile_number'  => $item->MobileNumber,
                    'obsolete'       => $item->Obsolete,
                    'phone_number'   => $item->PhoneNumber,
                    'post_code'      => $item->PostCode,

                    'unleashed_modified_on' => date('Y-m-d H:i:s', preg_replace( '/[^0-9]/', '', ($item->LastModifiedOn) ) / 1000),
                    'unleashed_id' => $this->unleashed_id
                ]
            );
        }

    }

}
