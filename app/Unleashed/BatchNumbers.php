<?php
namespace App\Unleashed;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;
use App\Unleashed\Api as Api;
use App\Unleashed\Helper as Helper;

class BatchNumbers
{

    protected $resource = '/BatchNumbers';

    public function __construct()
    {
        $this->host = config('unleashed.host').$this->resource;
        $this->api = Api::getApi();
    }

    public function get($modifiedSince)
    {
        foreach ($this->api as $api) {
            $this->unleashed_id = $api['unleashed_id'];
            $this->auth_id      = $api['api_key'];
            $this->secret       = $api['api_secret'];

            $results = $this->fetch($modifiedSince);
            $paging  = $results['paging'];
            if ($paging->NumberOfPages > 1) {
                for ($page = 2; $page <= $paging->NumberOfPages; $page++) {
                    $this->fetch($modifiedSince, $page);
                }
            }
        }
    }

    /**
     * Fetch unleashed data
     *
     * @return array
     */
    public function fetch($modifiedSince = '2019-11-11', $page = 1, $pageSize = 300)
    {
        $results = array();
        $client = new Client(); //GuzzleHttp\Client
        $host = $this->host . '/' . $page;

        $query = array(
            'pageSize' => $pageSize,
            'modifiedSince' => $modifiedSince,
            'available' => 'false'
        );

        $http_query = http_build_query($query);
        $signature = hash_hmac('sha256', $http_query, $this->secret, true);

        $headers = array(
            'Content-Type'       => 'application/json',
            'Accept'             => 'application/json',
            'api-auth-signature' => base64_encode($signature),
            'api-auth-id'        => $this->auth_id
        );

        $response = $client->get(
            $host, [
                'headers' => $headers,
                'query'   => $query
            ]
        );

        if (!$response->getStatusCode() == 200) return false;

        $body = json_decode($response->getBody());

        $results['paging'] = $body->Pagination;
        $results['items'] = $body->Items;
        $this->update($body->Items);

        return $results;
    }

    /**
     * Upsert batch)numbers
     *
     * @return void
     */
    public function update($items)
    {
        foreach($items as $item) {

            DB::table('batch_numbers')->updateOrInsert(
                ['batch_guid' => $item->Guid, 'unleashed_id' => $this->unleashed_id],
                [
                    'batch_guid'            => $item->Guid,
                    'batch_number'          => $item->Number,
                    'expiry_date'           => Helper::jsonToDate($item->ExpiryDate, 'Y-m-d H:i:s'),
                    'quantity'              => $item->Quantity,
                    'original_quantity'     => $item->OriginalQty,
                    'product_code'          => $item->ProductCode,
                    'warehouse_code'        => $item->WarehouseCode,
                    'status'                => $item->Status,
                    'unleashed_last_modified_by'    => $item->LastModifiedBy,
                    'unelashed_created_by'  => $item->CreatedBy,
                    'unleashed_created_on'  => Helper::jsonToDate($item->CreatedOn, 'Y-m-d H:i:s'),

                    'unleashed_modified_on' => Helper::jsonToDate($item->LastModifiedOn, 'Y-m-d H:i:s'),
                    'unleashed_id' => $this->unleashed_id,
                    'created_at'   => date('Y-m-d H:i'),
                    'updated_at'   => date('Y-m-d H:i')
                ]
            );
        }

    }

}

