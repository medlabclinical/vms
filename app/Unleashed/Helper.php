<?php
namespace App\Unleashed;

use DateTimeZone;

/**
 * Unleashed class to help parse the API
 */
class Helper
{
    /**
     * Converts json date to string based date
     * example: "/Date(1470960000000)/" converted to "2016-08-12"
     *
     * @return string
     */
    public static function jsonToDate($unleashed_json_date, $type = 'Y-m-d', $timezone = 'Australia/Sydney') {

        $unleashed_json_date = preg_replace('/[^0-9]/', '', $unleashed_json_date);
        if ($unleashed_json_date == '') return null;
        $unleashed_json_date = $unleashed_json_date / 1000;

        $date = date_create();
        date_timestamp_set($date, $unleashed_json_date);
        $date->setTimeZone(new DateTimeZone($timezone));

        return date_format($date, $type);
    }
}