<?php
namespace App\Unleashed;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;
use App\Unleashed\Api as Api;
use App\Unleashed\Helper as Helper;

class Credits
{

    protected $resource = '/CreditNotes';

    public function __construct()
    {
        $this->host = config('unleashed.host').$this->resource;
        $this->api = Api::getApi();
    }

    public function get($modifiedSince)
    {
        foreach ($this->api as $api) {
            $this->unleashed_id = $api['unleashed_id'];
            $this->auth_id      = $api['api_key'];
            $this->secret       = $api['api_secret'];

            $results = $this->fetch($modifiedSince);
            $paging  = $results['paging'];
            if ($paging->NumberOfPages > 1) {
                for ($page = 2; $page <= $paging->NumberOfPages; $page++) {
                    $this->fetch($modifiedSince, $page);
                }
            }

        }
    }

    /**
     * Fetch unleashed data
     *
     * @return array
     */
    public function fetch($modifiedSince = '2019-11-11', $page = 1, $pageSize = 500)
    {
        $results = array();
        $client = new Client(); //GuzzleHttp\Client
        $host = $this->host . '/' . $page;

        $query = array(
            'pageSize' => $pageSize,
            'modifiedSince' => $modifiedSince
        );

        $http_query = http_build_query($query);
        $signature = hash_hmac('sha256', $http_query, $this->secret, true);

        $headers = array(
            'Content-Type'       => 'application/json',
            'Accept'             => 'application/json',
            'api-auth-signature' => base64_encode($signature),
            'api-auth-id'        => $this->auth_id
        );

        $response = $client->get(
            $host, [
                'headers' => $headers,
                'query'   => $query
            ]
        );

        if (!$response->getStatusCode() == 200) return false;

        $body = json_decode($response->getBody());

        $results['paging'] = $body->Pagination;
        $results['items'] = $body->Items;
        $this->update($body->Items);
        return $results;
    }

    /**
     * Update or inserts credit and credit_lines table
     *
     * @return void
     */
    public function update($items)
    {
        foreach($items as $item) {

            // deletes item lines
            DB::table('credit_lines')->where(
                [
                    ['credit_number', '=', $item->CreditNoteNumber],
                    ['unleashed_id', '=', $this->unleashed_id]
                ]
            )->delete();

            // re-inserts item lines
            foreach ($item->CreditLines as $line) {


                DB::table('credit_lines')->insert(
                    [
                        'credit_number'            => $item->CreditNoteNumber,
                        'line_number'              => $line->LineNumber,
                        'line_type'                => $line->LineType,
                        'product_code'             => $line->Product->ProductCode,
                        'product_description'      => $line->Product->ProductDescription,
                        'order_quantity'           => $line->OrderQuantity,
                        'credit_quantity'          => $line->CreditQuantity,
                        'credit_price'             => $line->CreditPrice,
                        'line_total'               => $line->LineTotal,
                        'bc_line_total'            => $line->BCLineTotal,
                        'comment'                  => $line->Comments,
                        'tax_rate'                 => $line->TaxRate,
                        'line_tax'                 => $line->LineTax,
                        'bc_line_tax'              => $line->BCLineTax,
                        'avg_landed_price_on_sale' => $line->AverageLandedPriceAtTimeOfSale,
                        'reason'                   => $line->Reason,
                        'return_to_stock'          => $line->ReturnToStock,
                        'disassemble'              => $line->Disassemble,
                        'serial_number'            => $line->SerialNumbers,
                        'unleashed_modified_on'    => Helper::jsonToDate($line->LastModifiedOn, 'Y-m-d H:i:s'),
                        'unleashed_id'             => $this->unleashed_id,
                        'created_at'               => date('Y-m-d H:i'),
                        'updated_at'               => date('Y-m-d H:i')
                    ]
                );

            }

            DB::table('credits')->updateOrInsert(
                ['credit_number' => $item->CreditNoteNumber, 'unleashed_id' => $this->unleashed_id],
                [

                    'status'                 => $item->Status,
                    'customer_code'          => $item->Customer->CustomerCode,
                    'customer_name'          => $item->Customer->CustomerName,
                    'currency_code'          => $item->Currency->CurrencyCode,
                    'credit_date'            => Helper::jsonToDate($item->CreditDate),
                    'credit_number'          => $item->CreditNoteNumber,
                    'sales_invoice_date'     => Helper::jsonToDate($item->SalesInvoiceDate),
                    'required_delivery_date' => Helper::jsonToDate($item->RequiredDeliveryDate),
                    'invoice_number'         => $item->InvoiceNumber,
                    'tax'                    => $item->Tax,
                    'tax_rate'               => $item->TaxRate,
                    'total'                  => $item->Total,
                    'bc_total'               => $item->BCTotal,
                    'comments'               => $item->Comments,
                    'subtotal'               => $item->SubTotal,
                    'bc_subtotal'            => $item->BCTotal,
                    'tax_total'              => $item->TaxTotal,
                    'bc_tax_total'           => $item->BCTaxTotal,

                    // who did it and when
                    'unleashed_created_by'   => $item->CreatedBy,
                    'unleashed_created_on'   => Helper::jsonToDate($item->CreatedOn, 'Y-m-d H:i:s'),
                    'unleashed_modified_by'  => $item->LastModifiedBy,
                    'unleashed_modified_on'  => Helper::jsonToDate($item->LastModifiedOn, 'Y-m-d H:i:s'),
                    'unleashed_id'           => $this->unleashed_id,
                    'guid'                   => $item->Guid,
                    'created_at'             => date('Y-m-d H:i'),
                    'updated_at'             => date('Y-m-d H:i')

                ]
            );

        }

    }

}
