<?php

namespace App\Unleashed;

use DateTime;
use DateTimeZone;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class indirect
{
    public function __construct()
    {
        // init
    }

    public function update()
    {

        $sigma_raw = DB::table('sigma_raw_data')->get();

        foreach ($sigma_raw as $data) {

            $unleashed_sell_price = DB::table('product_wholesaler')
                ->join('sigma_raw_data', 'product_wholesaler.registration_number', 'sigma_raw_data.sigma_item_no')
                ->join('products', 'product_wholesaler.product_guid', 'products.guid')
                ->select('products.sell_price_1')
                ->where('product_wholesaler.registration_number', '=', $data->sigma_item_no)
                ->get()->first();

            $unleashed_last_cost = DB::table('product_wholesaler')
                ->join('sigma_raw_data', 'product_wholesaler.registration_number', 'sigma_raw_data.sigma_item_no')
                ->join('products', 'product_wholesaler.product_guid', 'products.guid')
                ->select('products.last_cost')
                ->where('product_wholesaler.registration_number', '=', $data->sigma_item_no)
                ->get()->first();

            $sigma_true_sales_price_per_unit = $unleashed_sell_price->sell_price_1 - ($data->claim_value / $data->qty_sold);
            $sigma_true_sales_price = $sigma_true_sales_price_per_unit * $data->qty_sold;
            $medlab_total_cost = $unleashed_last_cost->last_cost * $data->qty_sold;
            $gross_profit_dollar = $sigma_true_sales_price - $medlab_total_cost;
            $gross_profit_percentage = ($gross_profit_dollar / $sigma_true_sales_price) * 100;




            DB::table('indirect_sales')->insert(['indirect_id' => $data->sigma_id, 'invoice_number' => $data->doc_nbr, 'invoice_line_number' => $data->doc_line_nbr, 'invoice_date' => $data->date, 'sales_order_type' => $data->ord_type, 'wholesaler_name' => "Sigma", 'customer_number' => $data->cust_nbr, 'customer_name' => $data->cust_name, 'product_code' => $data->sigma_item_no, 'product_name' => $data->item_description, 'units_sold' => $data->qty_sold,
                'true_sales_price_per_unit' => round($sigma_true_sales_price_per_unit, 2), 'true_sales_price' => round($sigma_true_sales_price, 2), 'medlab_total_cost' => round($medlab_total_cost, 2), 'gross_profit_dollar' => round($gross_profit_dollar, 2), 'gross_profit_percentage' => round($gross_profit_percentage, 2)]);
        }


    }
}
