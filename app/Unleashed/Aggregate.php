<?php
namespace App\Unleashed;

use DateTime;
use DateTimeZone;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Aggregate
{
    public function __construct()
    {
        // init
    }


    public function calculate()
    {

        $thismonth = date("n");
        $thisyear = date("Y");
        $lastmonth = date("n")-1;
        $today = date("Y-m-d");

        $year_s = ($thismonth<=6) ? date("Y")-1 : date("Y");
        $cfy_date_from = $year_s .'-07-01';
        $cfy_date_to = $year_s . date("-m-d");

        $year_s = ($thismonth<=6) ? date("Y")-2 : date("Y")-1;
        $lfy_date_from = $year_s .'-07-01';
        $lfy_date_to = $year_s . date("-m-d");




        // find all the orders for today VMS
        $sql = "
            SELECT  rs1.state, rs1.country,

                    (SELECT SUM(sub_total) FROM orders
                    WHERE orders.delivery_region = rs1.state
                    AND order_date >= '" . $cfy_date_from . "' AND order_date <= '" . $cfy_date_to . "'
                    AND order_status != 'Deleted' AND orders.unleashed_id = '2bd9c34b-ad75-4a17-a1f1-83ce0fcc33d4')
                    AS 'cfy_sales',

                    (SELECT SUM(sub_total) FROM orders
                    WHERE orders.delivery_region = rs1.state
                    AND order_date >= '" . $lfy_date_from . "' AND order_date <= '" . $lfy_date_to . "'
                    AND order_status != 'Deleted' and orders.unleashed_id = '2bd9c34b-ad75-4a17-a1f1-83ce0fcc33d4')
                    AS 'lfy_sales',

                    (SELECT SUM(sub_total) FROM orders
                    WHERE orders.delivery_region = rs1.state
                    AND order_month = '" . $thismonth . "' AND order_year = '" . $thisyear . "'
                    AND order_status != 'Deleted' AND orders.unleashed_id = '2bd9c34b-ad75-4a17-a1f1-83ce0fcc33d4')
                    AS 'month_to_date_sales',

                    (SELECT SUM(sub_total) FROM orders
                    WHERE orders.delivery_region = rs1.state
                    AND order_month = '" . $lastmonth . "' AND order_year = '" . $thisyear . "'
                    AND order_status != 'Deleted' and orders.unleashed_id = '2bd9c34b-ad75-4a17-a1f1-83ce0fcc33d4')
                    AS 'last_month_sales',

                    (SELECT SUM(sub_total) FROM orders
                    WHERE orders.delivery_region = rs1.state
                    AND order_year = '" . $thisyear . "'
                    AND order_status != 'Deleted' AND orders.unleashed_id = '2bd9c34b-ad75-4a17-a1f1-83ce0fcc33d4')
                    AS 'ytd_sales',

                    (SELECT SUM(sub_total) FROM orders
                    WHERE orders.delivery_region = rs1.state
                    AND order_date = '" . $today . "'
                    AND order_status != 'Deleted' AND orders.unleashed_id = '2bd9c34b-ad75-4a17-a1f1-83ce0fcc33d4')
                    AS 'today_sales',

                    (SELECT SUM(quantity) FROM order_lines
                    INNER JOIN orders ON order_lines.order_number = orders.order_number
                    WHERE orders.delivery_region = rs1.state
                    AND order_date >= '" . $cfy_date_from . "' AND order_date <= '" . $cfy_date_to . "'
                    AND order_status != 'Deleted' AND orders.unleashed_id = '2bd9c34b-ad75-4a17-a1f1-83ce0fcc33d4'
                    AND order_lines.unleashed_id = '2bd9c34b-ad75-4a17-a1f1-83ce0fcc33d4')
                    AS 'cfy_qty',

                    (SELECT SUM(quantity) FROM order_lines
                    INNER JOIN orders ON order_lines.order_number = orders.order_number
                    WHERE orders.delivery_region = rs1.state
                    AND order_date >= '" . $lfy_date_from . "' AND order_date <= '" . $lfy_date_to . "'
                    AND order_status != 'Deleted' and orders.unleashed_id = '2bd9c34b-ad75-4a17-a1f1-83ce0fcc33d4'
                    AND order_lines.unleashed_id = '2bd9c34b-ad75-4a17-a1f1-83ce0fcc33d4')
                    AS 'lfy_qty',

                    (SELECT SUM(quantity) FROM order_lines
                    INNER JOIN orders ON order_lines.order_number = orders.order_number
                    WHERE orders.delivery_region = rs1.state
                    AND order_month = '" . $thismonth . "' AND order_year = '" . $thisyear . "'
                    AND order_status != 'Deleted' AND orders.unleashed_id = '2bd9c34b-ad75-4a17-a1f1-83ce0fcc33d4'
                    AND order_lines.unleashed_id = '2bd9c34b-ad75-4a17-a1f1-83ce0fcc33d4')
                    AS 'this_months_qty',

                    (SELECT SUM(quantity) FROM order_lines
                    INNER JOIN orders ON order_lines.order_number = orders.order_number
					WHERE orders.delivery_region = rs1.state
                    AND order_month = '" . $lastmonth . "' AND order_year = '" . $thisyear . "'
                    AND order_status != 'Deleted' and orders.unleashed_id = '2bd9c34b-ad75-4a17-a1f1-83ce0fcc33d4'
                    AND order_lines.unleashed_id = '2bd9c34b-ad75-4a17-a1f1-83ce0fcc33d4')
                    AS 'last_months_qty',

                    (SELECT SUM(quantity) FROM order_lines
                    INNER JOIN orders ON order_lines.order_number = orders.order_number
                    WHERE orders.delivery_region = rs1.state
                    AND order_year = '" . $thisyear . "'
                    AND order_status != 'Deleted' AND orders.unleashed_id = '2bd9c34b-ad75-4a17-a1f1-83ce0fcc33d4'
                    AND order_lines.unleashed_id = '2bd9c34b-ad75-4a17-a1f1-83ce0fcc33d4')
                    AS 'ytd_qty',

                    (SELECT SUM(quantity) FROM order_lines
                    INNER JOIN orders ON order_lines.order_number = orders.order_number
                    WHERE orders.delivery_region = rs1.state
                    AND order_date = '" . $today . "'
                    AND order_status != 'Deleted' AND orders.unleashed_id = '2bd9c34b-ad75-4a17-a1f1-83ce0fcc33d4'
                    AND order_lines.unleashed_id = '2bd9c34b-ad75-4a17-a1f1-83ce0fcc33d4')
                    AS 'day_qty'

            FROM    (
                        SELECT      ifnull(orders.delivery_region, \"---\") as state, ifnull(orders.delivery_country, \"---\") as country
                        FROM        orders
                        WHERE       orders.order_status != 'Deleted' AND orders.unleashed_id = '2bd9c34b-ad75-4a17-a1f1-83ce0fcc33d4'
                        GROUP BY    state, country
                    )rs1

        ";




        $todays_orders = DB::select($sql);
        foreach ($todays_orders as $item)
        {


            //Sales Growth
            if ($item->month_to_date_sales > 0 && $item->last_month_sales > 0) {
                $monthly_growth_percentage = round(((($item->month_to_date_sales - $item->last_month_sales) / $item->last_month_sales) * 100), 0);
            } else if ($item->month_to_date_sales > 0 && $item->last_month_sales <= 0) {
                $monthly_growth_percentage = '9999';
            } else if ($item->month_to_date_sales == 0 && $item->last_month_sales > 0) {
                $monthly_growth_percentage = '-100';
            } else if ($item->month_to_date_sales == 0 && $item->last_month_sales == 0) {
                $monthly_growth_percentage = 0;
            }

            if ($item->cfy_sales > 0 && $item->lfy_sales > 0) {
                $financial_year_growth_percentage = round(((($item->cfy_sales - $item->lfy_sales) / $item->lfy_sales) * 100), 0);
            } else if ($item->cfy_sales > 0 && $item->lfy_sales <= 0) {
                $financial_year_growth_percentage = '9999';
            } else if ($item->cfy_sales == 0 && $item->lfy_sales > 0) {
                $financial_year_growth_percentage = '-100';
            } else if ($item->cfy_sales == 0 && $item->lfy_sales == 0) {
                $financial_year_growth_percentage = 0;
            }

            //Quantity Growth
            if ($item->this_months_qty > 0 && $item->last_months_qty > 0) {
                $monthly_growth_qty = round(((($item->this_months_qty - $item->last_months_qty) / $item->last_months_qty) * 100), 0);
            } else if ($item->this_months_qty > 0 && $item->last_months_qty <= 0) {
                $monthly_growth_qty = '9999';
            } else if ($item->this_months_qty == 0 && $item->last_months_qty > 0) {
                $monthly_growth_qty = '-100';
            } else if ($item->this_months_qty == 0 && $item->last_months_qty == 0) {
                $monthly_growth_qty = 0;
            }

            if ($item->cfy_qty > 0 && $item->lfy_qty > 0) {
                $financial_year_growth_qty = round(((($item->cfy_qty - $item->lfy_qty) / $item->lfy_qty) * 100), 0);
            } else if ($item->cfy_qty > 0 && $item->lfy_qty <= 0) {
                $financial_year_growth_qty = '9999';
            } else if ($item->cfy_qty == 0 && $item->lfy_qty > 0) {
                $financial_year_growth_qty = '-100';
            } else if ($item->cfy_qty == 0 && $item->lfy_qty == 0) {
                $financial_year_growth_qty = 0;
            }




                DB::table('aggregation_vms')->where(
                    [
                        ['state', '=', $item->state],
                    ]
                )->delete();

            if ($item->cfy_sales > 0 or $item->lfy_sales > 0) {


                DB::table('aggregation_vms')->insert(
                    [

                        'state'                             => $item->state,
                        'country'                           => $item->country,
                        'cfy_sales'                         => (is_null($item->cfy_sales)) ? 0 : $item->cfy_sales,
                        'lfy_sales'                         => (is_null($item->lfy_sales)) ? 0 : $item->lfy_sales,
                        'this_months_sales'                 => (is_null($item->month_to_date_sales)) ? 0 : $item->month_to_date_sales,
                        'last_months_sales'                 => (is_null($item->last_month_sales)) ? 0 : $item->last_month_sales,
                        'ytd_sales'                         => (is_null($item->ytd_sales)) ? 0 : $item->ytd_sales,
                        'day_sales'                         => (is_null($item->today_sales)) ? 0 : $item->today_sales,
                        'monthly_growth_percentage'         => $monthly_growth_percentage,
                        'financial_year_growth_percentage'  => $financial_year_growth_percentage,
                        'created_at'                        => date('Y-m-d H:i'),
                        'updated_at'                        => date('Y-m-d H:i'),
                        'cfy_qty'                         => (is_null($item->cfy_qty)) ? 0 : $item->cfy_qty,
                        'lfy_qty'                         => (is_null($item->lfy_qty)) ? 0 : $item->lfy_qty,
                        'this_months_qty'                 => (is_null($item->this_months_qty)) ? 0 : $item->this_months_qty,
                        'last_months_qty'                 => (is_null($item->last_months_qty)) ? 0 : $item->last_months_qty,
                        'ytd_qty'                         => (is_null($item->ytd_qty)) ? 0 : $item->ytd_qty,
                        'day_qty'                         => (is_null($item->day_qty)) ? 0 : $item->day_qty,
                        'monthly_growth_qty'              => $monthly_growth_qty,
                        'financial_year_growth_qty'       => $financial_year_growth_qty,

                    ]

                );

            }

        }


        // find all the orders for today Pharma
        $sql = "
            SELECT  rs1.state, rs1.state,

                    (SELECT SUM(sub_total) FROM orders
                    WHERE orders.delivery_region = rs1.state
                    AND order_date >= '" . $cfy_date_from . "' AND order_date <= '" . $cfy_date_to . "'
                    AND order_status != 'Deleted' AND orders.unleashed_id = '6c9a056b-3713-4cfc-bfdd-50be48e0c275')
                    AS 'cfy_sales',

                    (SELECT SUM(sub_total) FROM orders
                    WHERE orders.delivery_region = rs1.state
                    AND order_date >= '" . $lfy_date_from . "' AND order_date <= '" . $lfy_date_to . "'
                    AND order_status != 'Deleted' AND orders.unleashed_id = '6c9a056b-3713-4cfc-bfdd-50be48e0c275')
                    AS 'lfy_sales',

                    (SELECT SUM(sub_total) FROM orders
                    WHERE orders.delivery_region = rs1.state
                    AND order_month = '" . $thismonth . "' AND order_year = '" . $thisyear . "'
                    AND order_status != 'Deleted' AND orders.unleashed_id = '6c9a056b-3713-4cfc-bfdd-50be48e0c275')
                    AS 'month_to_date_sales',

                    (SELECT SUM(sub_total) FROM orders
                    WHERE orders.delivery_region = rs1.state
                    AND order_month = '" . $lastmonth . "' AND order_year = '" . $thisyear . "'
                    AND order_status != 'Deleted' AND orders.unleashed_id = '6c9a056b-3713-4cfc-bfdd-50be48e0c275')
                    AS 'last_month_sales',

                    (SELECT SUM(sub_total) FROM orders
                    WHERE orders.delivery_region = rs1.state
                    AND order_year = '" . $thisyear . "'
                    AND order_status != 'Deleted' AND orders.unleashed_id = '6c9a056b-3713-4cfc-bfdd-50be48e0c275')
                    AS 'ytd_sales',

                    (SELECT SUM(sub_total) FROM orders
                    WHERE orders.delivery_region = rs1.state
                    AND order_date = '" . $today . "'
                    AND order_status != 'Deleted' AND orders.unleashed_id = '6c9a056b-3713-4cfc-bfdd-50be48e0c275')
                    AS 'today_sales',

                    (SELECT SUM(quantity) FROM order_lines
                    INNER JOIN orders ON order_lines.order_number = orders.order_number
                    WHERE orders.delivery_region = rs1.state
                    AND order_date >= '" . $cfy_date_from . "' AND order_date <= '" . $cfy_date_to . "'
                    AND order_status != 'Deleted' AND orders.unleashed_id = '6c9a056b-3713-4cfc-bfdd-50be48e0c275'
                    AND order_lines.unleashed_id = '6c9a056b-3713-4cfc-bfdd-50be48e0c275')
                    AS 'cfy_qty',

                    (SELECT SUM(quantity) FROM order_lines
                    INNER JOIN orders ON order_lines.order_number = orders.order_number
                    WHERE orders.delivery_region = rs1.state
                    AND order_date >= '" . $lfy_date_from . "' AND order_date <= '" . $lfy_date_to . "'
                    AND order_status != 'Deleted' AND orders.unleashed_id = '6c9a056b-3713-4cfc-bfdd-50be48e0c275'
                    AND order_lines.unleashed_id = '6c9a056b-3713-4cfc-bfdd-50be48e0c275')
                    AS 'lfy_qty',

                    (SELECT SUM(quantity) FROM order_lines
                    INNER JOIN orders ON order_lines.order_number = orders.order_number
                    WHERE orders.delivery_region = rs1.state
                    AND order_month = '" . $thismonth . "' AND order_year = '" . $thisyear . "'
                    AND order_status != 'Deleted' AND orders.unleashed_id = '6c9a056b-3713-4cfc-bfdd-50be48e0c275'
                    AND order_lines.unleashed_id = '6c9a056b-3713-4cfc-bfdd-50be48e0c275')
                    AS 'this_months_qty',

                    (SELECT SUM(quantity) FROM order_lines
                    INNER JOIN orders ON order_lines.order_number = orders.order_number
					WHERE orders.delivery_region = rs1.state
                    AND order_month = '" . $lastmonth . "' AND order_year = '" . $thisyear . "'
                    AND order_status != 'Deleted' AND orders.unleashed_id = '6c9a056b-3713-4cfc-bfdd-50be48e0c275'
                    AND order_lines.unleashed_id = '6c9a056b-3713-4cfc-bfdd-50be48e0c275')
                    AS 'last_months_qty',

                    (SELECT SUM(quantity) FROM order_lines
                    INNER JOIN orders ON order_lines.order_number = orders.order_number
                    WHERE orders.delivery_region = rs1.state
                    AND order_year = '" . $thisyear . "'
                    AND order_status != 'Deleted' AND orders.unleashed_id = '6c9a056b-3713-4cfc-bfdd-50be48e0c275'
                    AND order_lines.unleashed_id = '6c9a056b-3713-4cfc-bfdd-50be48e0c275')
                    AS 'ytd_qty',

                    (SELECT SUM(quantity) FROM order_lines
                    INNER JOIN orders ON order_lines.order_number = orders.order_number
                    WHERE orders.delivery_region = rs1.state
                    AND order_date = '" . $today . "'
                    AND order_status != 'Deleted' AND orders.unleashed_id = '6c9a056b-3713-4cfc-bfdd-50be48e0c275'
                    AND order_lines.unleashed_id = '6c9a056b-3713-4cfc-bfdd-50be48e0c275')
                    AS 'day_qty'

            FROM    (
                        SELECT      ifnull(orders.delivery_region, \"---\") as state, ifnull(orders.delivery_country, \"---\") as country
                        FROM        orders
                        WHERE       orders.order_status != 'Deleted' AND orders.unleashed_id = '6c9a056b-3713-4cfc-bfdd-50be48e0c275'
                        GROUP BY    state, country
                    )rs1

        ";

        $todays_orders = DB::select($sql);
        foreach ($todays_orders as $item)
        {

            //Sales Growth
            if ($item->month_to_date_sales > 0 && $item->last_month_sales > 0) {
                $monthly_growth_percentage = round(((($item->month_to_date_sales - $item->last_month_sales) / $item->last_month_sales) * 100), 0);
            } else if ($item->month_to_date_sales > 0 && $item->last_month_sales <= 0) {
                $monthly_growth_percentage = '9999';
            } else if ($item->month_to_date_sales == 0 && $item->last_month_sales > 0) {
                $monthly_growth_percentage = '-100';
            } else if ($item->month_to_date_sales == 0 && $item->last_month_sales == 0) {
                $monthly_growth_percentage = 0;
            }

            if ($item->cfy_sales > 0 && $item->lfy_sales > 0) {
                $financial_year_growth_percentage = round(((($item->cfy_sales - $item->lfy_sales) / $item->lfy_sales) * 100), 0);
            } else if ($item->cfy_sales > 0 && $item->lfy_sales <= 0) {
                $financial_year_growth_percentage = '9999';
            } else if ($item->cfy_sales == 0 && $item->lfy_sales > 0) {
                $financial_year_growth_percentage = '-100';
            } else if ($item->cfy_sales == 0 && $item->lfy_sales == 0) {
                $financial_year_growth_percentage = 0;
            }

            //Quantity Growth
            if ($item->this_months_qty > 0 && $item->last_months_qty > 0) {
                $monthly_growth_qty = round(((($item->this_months_qty - $item->last_months_qty) / $item->last_months_qty) * 100), 0);
            } else if ($item->this_months_qty > 0 && $item->last_months_qty <= 0) {
                $monthly_growth_qty = '9999';
            } else if ($item->this_months_qty == 0 && $item->last_months_qty > 0) {
                $monthly_growth_qty = '-100';
            } else if ($item->this_months_qty == 0 && $item->last_months_qty == 0) {
                $monthly_growth_qty = 0;
            }

            if ($item->cfy_qty > 0 && $item->lfy_qty > 0) {
                $financial_year_growth_qty = round(((($item->cfy_qty - $item->lfy_qty) / $item->lfy_qty) * 100), 0);
            } else if ($item->cfy_qty > 0 && $item->lfy_qty <= 0) {
                $financial_year_growth_qty = '9999';
            } else if ($item->cfy_qty == 0 && $item->lfy_qty > 0) {
                $financial_year_growth_qty = '-100';
            } else if ($item->cfy_qty == 0 && $item->lfy_qty == 0) {
                $financial_year_growth_qty = 0;
            }



                DB::table('aggregation_pharma')->where(
                    [
                        ['state', '=', $item->state],
                    ]
                )->delete();

            if ($item->cfy_sales > 0 || $item->lfy_sales > 0) {


                DB::table('aggregation_pharma')->insert(
                [

                    'state'                             => $item->state,
                    'country'                           => "Australia",
                    'cfy_sales'                         => (is_null($item->cfy_sales)) ? 0 : $item->cfy_sales,
                    'lfy_sales'                         => (is_null($item->lfy_sales)) ? 0 : $item->lfy_sales,
                    'this_months_sales'                 => (is_null($item->month_to_date_sales)) ? 0 : $item->month_to_date_sales,
                    'last_months_sales'                 => (is_null($item->last_month_sales)) ? 0 : $item->last_month_sales,
                    'ytd_sales'                         => (is_null($item->ytd_sales)) ? 0 : $item->ytd_sales,
                    'day_sales'                         => (is_null($item->today_sales)) ? 0 : $item->today_sales,
                    'monthly_growth_percentage'         => $monthly_growth_percentage,
                    'financial_year_growth_percentage'  => $financial_year_growth_percentage,
                    'created_at'                        => date('Y-m-d H:i'),
                    'updated_at'                        => date('Y-m-d H:i'),
                    'cfy_qty'                         => (is_null($item->cfy_qty)) ? 0 : $item->cfy_qty,
                    'lfy_qty'                         => (is_null($item->lfy_qty)) ? 0 : $item->lfy_qty,
                    'this_months_qty'                 => (is_null($item->this_months_qty)) ? 0 : $item->this_months_qty,
                    'last_months_qty'                 => (is_null($item->last_months_qty)) ? 0 : $item->last_months_qty,
                    'ytd_qty'                         => (is_null($item->ytd_qty)) ? 0 : $item->ytd_qty,
                    'day_qty'                         => (is_null($item->day_qty)) ? 0 : $item->day_qty,
                    'monthly_growth_qty'              => $monthly_growth_qty,
                    'financial_year_growth_qty'       => $financial_year_growth_qty,

                ]

            );
            }

        }


    }
}
