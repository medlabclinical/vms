<?php
namespace App\Unleashed;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;
use App\Unleashed\StockOnHandWarehouse;
use App\Unleashed\Api as Api;
use App\Unleashed\Helper as Helper;

class StockOnHand
{

    protected $resource = '/StockOnHand';

    public function __construct()
    {
        $this->host = config('unleashed.host').$this->resource;
        $this->api = Api::getApi();
    }

    public function get($modifiedSince)
    {
        foreach ($this->api as $api) {
            $this->unleashed_id = $api['unleashed_id'];
            $this->auth_id      = $api['api_key'];
            $this->secret       = $api['api_secret'];

            $results = $this->fetch($modifiedSince);
            $paging  = $results['paging'];
            if ($paging->NumberOfPages > 1) {
                for ($page = 2; $page <= $paging->NumberOfPages; $page++) {
                    $this->fetch($modifiedSince, $page);
                }
            }
        }
    }

    /**
     * Fetch unleashed data
     *
     * @return array
     */
    public function fetch($modifiedSince = '2019-11-11', $page = 1, $pageSize = 300)
    {
        $results = array();
        $client = new Client(); //GuzzleHttp\Client
        $host = $this->host . '/' . $page;

        $query = array(
            'pageSize' => $pageSize,
            'modifiedSince' => $modifiedSince
        );

        $http_query = http_build_query($query);
        $signature = hash_hmac('sha256', $http_query, $this->secret, true);

        $headers = array(
            'Content-Type'       => 'application/json',
            'Accept'             => 'application/json',
            'api-auth-signature' => base64_encode($signature),
            'api-auth-id'        => $this->auth_id
        );

        $response = $client->get(
            $host, [
                'headers' => $headers,
                'query'   => $query
            ]
        );

        if (!$response->getStatusCode() == 200) return false;

        $body = json_decode($response->getBody());

        $results['paging'] = $body->Pagination;
        $results['items'] = $body->Items;
        $this->update($body->Items);
        return $results;
    }

    /**
     * Upsert stock_on_hand
     *
     * @return void
     */
    public function update($items)
    {
        foreach($items as $item) {

            DB::table('stock_on_hand')->updateOrInsert(
                ['product_code' => $item->ProductCode, 'unleashed_id' => $this->unleashed_id],
                [
                    'product_description' => $item->ProductDescription,
                    'product_guid'         => $item->ProductGuid,
                    'group_name'           => $item->ProductGroupName,
                    'warehouse'            => $item->Warehouse,
                    'warehouse_code'       => $item->WarehouseCode,
                    'days_since_last_sale' => $item->DaysSinceLastSale,
                    'on_purchase'          => $item->OnPurchase,
                    'allocated_qty'        => $item->AllocatedQty,
                    'available_qty'        => $item->AvailableQty,
                    'qty_on_hand'          => $item->QtyOnHand,
                    'avg_cost'             => $item->AvgCost,
                    'total_cost'           => $item->TotalCost,
                    'unleashed_modified_on' => date('Y-m-d H:i:s', preg_replace( '/[^0-9]/', '', ($item->LastModifiedOn) ) / 1000),
                    'unleashed_id' => $this->unleashed_id,
                    'created_at'   => date('Y-m-d H:i'),
                    'updated_at'   => date('Y-m-d H:i')

                ]
            );

            // update warehouse
            $stock_on_warehouse = new StockOnHandWarehouse();
            $stock_on_warehouse->get($item->ProductGuid, $this->unleashed_id);
        }

    }

}
