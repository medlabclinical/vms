<?php
namespace App\Unleashed;

use DateTime;
use DateTimeZone;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Tier
{
    public function __construct()
    {
        // init
    }

    public function update()
    {
        DB::update('update ucustomers set tier = 5 where tier IS NULL');
        DB::update('update ucustomers set drug_tier = 5 where drug_tier IS NULL');

        // Run all the tier updates here VITAMINS
        $sql = "
            SELECT      ucc.customer_code,
                        CASE
                            WHEN SUM(rs1.avg_sales_per_month) >= 3500 THEN 1
                            WHEN SUM(rs1.avg_sales_per_month) >= 2000 THEN 2
                            WHEN SUM(rs1.avg_sales_per_month) >= 1000 THEN 3
                            WHEN SUM(rs1.avg_sales_per_month) > 0 THEN 4
                            ELSE 5
                        END as tier
            FROM        (
                            SELECT      CASE
                                            WHEN (TIMESTAMPDIFF(MONTH, DATE(uc.unleashed_created_on), DATE(NOW()))) > 12 THEN ROUND(SUM(total),0)/12
                                            ELSE (ROUND(SUM(total),0) / (TIMESTAMPDIFF(MONTH, DATE(uc.unleashed_created_on), DATE(NOW()))))
                                        END AS avg_sales_per_month,
                                        oo.customer_code,
                                        TIMESTAMPDIFF(MONTH, DATE(uc.unleashed_created_on), DATE(NOW())) AS diff_month,
                                        DATE(uc.unleashed_created_on) AS date_created
                            FROM        orders oo
                            LEFT JOIN   ucustomers uc on (uc.customer_code = oo.customer_code)
                            WHERE       oo.unleashed_id = '2bd9c34b-ad75-4a17-a1f1-83ce0fcc33d4'
                            AND         oo.order_status != 'Deleted'
                            GROUP BY    oo.customer_code, uc.unleashed_created_on

                            UNION

                            SELECT      CASE
                                            WHEN (TIMESTAMPDIFF(MONTH, DATE(ucustomers.unleashed_created_on), DATE(NOW()))) > 12 THEN ROUND(SUM(indirect_sales.true_sales_price),0)/12
                                            ELSE (ROUND(SUM(indirect_sales.true_sales_price),0) / (TIMESTAMPDIFF(MONTH, DATE(ucustomers.unleashed_created_on), DATE(NOW()))))
                                        END AS avg_sales_per_month,
                                        ucustomers.customer_code,
                                        TIMESTAMPDIFF(MONTH, DATE(ucustomers.unleashed_created_on), DATE(NOW())) AS diff_month,
                                        DATE(ucustomers.unleashed_created_on) AS date_created
                            FROM        indirect_sales
                            INNER JOIN  product_wholesaler on (indirect_sales.product_code = product_wholesaler.registration_number AND product_wholesaler.deleted_at IS NULL)
                            INNER JOIN  customer_wholesalers on (customer_wholesalers.registration_number = indirect_sales.customer_number AND customer_wholesalers.deleted_at IS NULL)
                            INNER JOIN  products on (products.guid = product_wholesaler.product_guid)
                            INNER JOIN  ucustomers on (ucustomers.guid = customer_wholesalers.customer_id)
                            WHERE       indirect_sales.true_sales_price > 0
                            GROUP BY    ucustomers.customer_code, ucustomers.unleashed_created_on

                        )rs1
            INNER JOIN  ucustomers ucc on (ucc.customer_code = rs1.customer_code)
            GROUP BY    ucc.customer_code
        ";
        $tier_query = DB::select($sql);
        foreach ($tier_query as $item) {
            DB::update('update ucustomers set tier = ? where customer_code = ?', [$item->tier, $item->customer_code]);
        }

        // Run all the tier updates here DRUGGIES
        $sql = "
            SELECT      rs1.customer_code,
                        CASE
                            WHEN rs1.avg_sales_per_month >= 3500 THEN 1
                            WHEN rs1.avg_sales_per_month >= 2000 THEN 2
                            WHEN rs1.avg_sales_per_month >= 1000 THEN 3
                            WHEN rs1.avg_sales_per_month > 0 THEN 4
                            ELSE 5
                        END as tier
            FROM        (
                            SELECT      CASE
                                            WHEN (TIMESTAMPDIFF(MONTH, DATE(uc.unleashed_created_on), DATE(NOW()))) > 12 THEN ROUND(SUM(total),0)/12
                                            ELSE (ROUND(SUM(total),0) / (TIMESTAMPDIFF(MONTH, DATE(uc.unleashed_created_on), DATE(NOW()))))
                                        END AS avg_sales_per_month,
                                        oo.customer_code,
                                        TIMESTAMPDIFF(MONTH, DATE(uc.unleashed_created_on), DATE(NOW())) AS diff_month,
                                        DATE(uc.unleashed_created_on) AS date_created
                            FROM        orders oo
                            LEFT JOIN   ucustomers uc on (uc.customer_code = oo.customer_code)
                            WHERE       oo.unleashed_id = '6c9a056b-3713-4cfc-bfdd-50be48e0c275'
                            AND         oo.order_status != 'Deleted'
                            GROUP BY    oo.customer_code, uc.unleashed_created_on
                        )rs1
            INNER JOIN  ucustomers ucc on (ucc.pharma_customer_code = rs1.customer_code)
        ";
        $tier_query = DB::select($sql);
        foreach ($tier_query as $item) {
            DB::update('update ucustomers set drug_tier = ? where pharma_customer_code = ?', [$item->tier, $item->customer_code]);
        }
    }
}
