<?php
namespace App\Unleashed;

use App\Models\DefaultUser;
use App\Models\ShoppingCartItemModel;
use App\Models\ShoppingCartModel;
use App\Models\Unleashed;
use App\Models\UnleashedCustomer as UnleashedCustomer;
use App\Models\User;
use App\Unleashed\Api as Api;
use App\Unleashed\Helper as Helper;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class Orders
{

    protected $resource = '/SalesOrders';

    public function __construct()
    {
        $this->host = config('unleashed.host').$this->resource;
        $this->api = Api::getApi();
    }

    /**
     * Push new shopping cart order to unleashed
     *
     * @return mixed
     */
    public static function pushCart($cartid, $cartstatus)
    {
        $debug = false;

        // get the cart id
        $cart = ShoppingCartModel::where('cart_id', $cartid)
                    ->whereNull('unleashed_orderno')
                    ->first();

        if (!$cart)
        {
            Log::critical($cartid . ' not found in any system');
            return false;
        }
        else
        {
            $salesperson["FullName"] = 'Internet Orders';
            $salesperson["Email"]    = 'internet@medlab.co';

            $address = $cart->cart_delivery_address;
            $split = explode("::", $address);

            /**
            * Sun Sep 13 08:06:22 2020
            * Find pharma code client if available (we will only have Vitamins as default)
            */
            $customer = UnleashedCustomer::find($cart->customer_id);

            if (!$customer) {
                Log::critical($cartid . ' was unable to locate the customer guid ' . $customer->guid . ' in any system');
                return false;
            }

            $order['DeliveryInstruction']    = $cart->cart_delivery_notes;
            $order['CustomerRef']            = ($cart->cart_orderno=='') ? "Web Order # " . $cart->cart_id : $cart->cart_orderno;
            $order['SalesPerson']            = $salesperson;
            $order['DeliveryName']           = $cart->cart_delivery_contact;
            $order['DeliveryStreetAddress']  = $split[0];
            $order['DeliveryStreetAddress2'] = $split[1];
            $order['DeliverySuburb']         = $split[2];
            $order['DeliveryCity']           = '';
            $order['DeliveryRegion']         = $split[4];
            $order['DeliveryCountry']        = $split[5];
            $order['DeliveryPostCode']       = $split[3];
            $order['TaxRate']                = ($cart->cart_total_tax > 0) ? 0.1 : 0;
            $order['SalesOrderLines']        = array();

            $customer_guid                   = $customer->guid;
            $customer_pharma_guid            = ($customer->pharma_customer_guid != '') ? $customer->pharma_customer_guid : $customer_guid;
            $vitamins_extra_guids            = array('esky' => '8c064452-6008-448e-95fe-628c41e137a8', 'shipping' => '0969c3c6-2c66-44a4-8684-79030b601e14');
            $pharma_extra_guids              = array('esky' => '9621b906-65e1-4a54-803a-b7e44da1d5b5', 'shipping' => 'e282777c-29cf-4e82-b4f3-d01cc11a8e32');

            // Count the items
            $product_vitamin = array();
            $product_drug = array();
            $product_drug_cold = 0;
            $cart_items = ShoppingCartItemModel::CartProducts($cartid)->get();
            foreach ($cart_items as $item) {
                if ($item->brand_id == 2) { $product_vitamin[] = $item; }
                if ($item->brand_id == 1) {
                    $product_drug[] = $item;
                    if ($item->ship_refrigerated==1) {
                        $product_drug_cold++;
                    }
                }
            }

            // split shipping between the unleashes
            if (count($product_vitamin)>=1 && count($product_drug)>=1 && $cart->cart_shipping > 0) {
                $cart->cart_shipping = round(($cart->cart_shipping/2), 2);
            }

            // split esky between the unleashes
            if ($cart->cart_cold_items > 0 && $cart->esky > 0) {
                if ($product_drug_cold > 0) {
                    $cart->esky = round(($cart->esky/2),2);
                }
            }

            /**
             * Get Pharma Products
             */
            $count = 0;
            $pharma_lines = array();
            $cart_items = ShoppingCartItemModel::CartProducts($cartid)->get();
            foreach ($cart_items as $item)
            {
                if ($item->brand_id == 1)
                {
                    $batch_details = array(
                            "Guid"     => $item->p_batchguid,
                            "Number"   => $item->batch_number,
                            "Quantity" => $item->quantity,
                    );

                    $count++;
                    $pharma_lines[] = array(
                        'LineNumber'    => $count,
                        'BatchNumbers'  => ($item->batch_number!='') ? $batch_details : null,
                        'LineType'      => null,
                        'Product'       => array('Guid' => $item->product_guid),
                        'OrderQuantity' => $item->quantity,
                        'UnitPrice'     => $item->unit_price,
                        'DiscountRate'  => round(($item->discount_rate/100),4),
                        'LineTotal'     => $item->line_total,
                        'Comments'      => '',
                        'TaxRate'       => $item->tax_rate,
                        'LineTax'       => $item->line_tax
                    );
                }
            }

            /**
             * Get Vitamins Products
             */
            $count = 0;
            $vitamin_lines = array();
            $cart_items = ShoppingCartItemModel::CartProducts($cartid)->get();
            foreach ($cart_items as $item)
            {
                if ($item->brand_id == 2)
                {
                    $batch_details = array(
                            "Guid"     => $item->v_batchguid,
                            "Number"   => $item->batch_number,
                            "Quantity" => $item->quantity,
                    );

                    $count++;
                    $vitamin_lines[] = array(
                        'LineNumber'    => $count,
                        'BatchNumbers'  => ($item->batch_number!='') ? $batch_details : null,
                        'LineType'      => null,
                        'Product'       => array('Guid' => $item->product_guid),
                        'OrderQuantity' => $item->quantity,
                        'UnitPrice'     => $item->unit_price,
                        'DiscountRate'  => round(($item->discount_rate/100),4),
                        'LineTotal'     => $item->line_total,
                        'Comments'      => '',
                        'TaxRate'       => $item->tax_rate,
                        'LineTax'       => $item->line_tax
                    );
                }
            }

            // ----------------------------------------------------------------------------------------
            // Generate the JSONS DRUGS
            // ----------------------------------------------------------------------------------------

            if (count($pharma_lines)>0)
            {
                // Pharma goes first
                $order['SalesOrderLines']  = $pharma_lines;
                $order['Customer']['Guid'] = $customer_pharma_guid;
                $esky_guid                 = $pharma_extra_guids["esky"];
                $shipping_guid             = $pharma_extra_guids["shipping"];
                $unleashed_id              = Unleashed::where('brand_id', 1)->first()->id;

                $count = count($order['SalesOrderLines']);

                // Split shipping if there is
                if ($cart->cart_shipping > 0) {
                    $count++;
                    $order['SalesOrderLines'][] = array(
                        'LineNumber'    => $count,
                        'BatchNumbers'  => null,
                        'LineType'      => null,
                        'Product'       => array('Guid' => $shipping_guid),
                        'OrderQuantity' => 1,
                        'UnitPrice'     => $cart->cart_shipping,
                        'DiscountRate'  => 0,
                        'LineTotal'     => $cart->cart_shipping,
                        'Comments'      => '',
                        'TaxRate'       => ($cart->cart_total_tax > 0) ? 0.1 : 0,
                        'LineTax'       => ($cart->cart_total_tax > 0) ? round(($cart->cart_shipping * 0.1), 2) : 0
                    );
                }

                // Split esky count
                if ($cart->cart_cold_items > 0 && $product_drug_cold > 0) {
                    $count++;
                    $order['SalesOrderLines'][] = array(
                        'LineNumber'    => $count,
                        'BatchNumbers'  => null,
                        'LineType'      => null,
                        'Product'       => array('Guid' => $esky_guid),
                        'OrderQuantity' => 1,
                        'UnitPrice'     => $cart->cart_esky,
                        'DiscountRate'  => ($cart->cart_esky == 0) ? 1 : 0,
                        'LineTotal'     => $cart->cart_esky,
                        'Comments'      => '',
                        'TaxRate'       => ($cart->cart_total_tax > 0) ? 0.1 : 0,
                        'LineTax'       => ($cart->cart_total_tax > 0) ? round(($cart->cart_esky * 0.1), 2) : 0
                    );
                }

                // Was this paid in full?
                if ($cartstatus == "Approved")
                {
                    $count++;
                    $order['SalesOrderLines'][] = array(
                        'LineNumber'    => $count,
                        'BatchNumbers'  => null,
                        'LineType'      => null,
                        'Product'       => array('ProductCode' => 'PIF'),
                        'OrderQuantity' => 1,
                        'UnitPrice'     => 0,
                        'DiscountRate'  => 0,
                        'LineTotal'     => 0,
                        'Comments'      => '',
                        'TaxRate'       => 0,
                        'LineTax'       => 0
                    );
                }

                // generate order UUID
                $order['Guid'] = (string) Str::uuid();

                if (!array_key_exists('SalesOrderLines', $order)) {
                    Log::emergency('Order #' . $cartid . ' - Sales order lines are missing');
                    $submission_result = false;
                }

                // sets defaults
                $subtotal  = 0;
                $total     = 0;
                $tax_total = 0;

                foreach ($order['SalesOrderLines'] as $index => $row) {

                    // calculates Sub Total
                    $subtotal = $subtotal + $order['SalesOrderLines'][$index]['LineTotal'];

                    // calculates Tax Total
                    $tax_total = $tax_total + $order['SalesOrderLines'][$index]['LineTax'];

                    // calculates Total
                    $total = $total +  $order['SalesOrderLines'][$index]['LineTotal'] + $order['SalesOrderLines'][$index]['LineTax'];
                }

                $order['OrderStatus'] = 'Placed';
                $order['SubTotal']    = round($subtotal, 4);
                $order['TaxTotal']    = round($tax_total, 4);
                $order['Total']       = round($total, 4);

                $host   = config('unleashed.host'). '/SalesOrders/' . $order['Guid'];
                $api    = Api::getApi($unleashed_id);

                // generate signature
                $query      = array('serialBatch' => 'true');
                $http_query = http_build_query($query);
                $signature  = hash_hmac('sha256', $http_query, $api['api_secret'], true);

                // generate headers
                $headers = array(
                    'Content-Type'       => 'application/json',
                    'Accept'             => 'application/json',
                    'api-auth-signature' => base64_encode($signature),
                    'api-auth-id'        => $api['api_key']
                );

                if (count($api) != 3) {
                    Log::emergency('Vitamin Order #' . $cartid . ' - Unable to find matching unleashed id for ' . $unleashed_id);
                    $submission_result = false;
                }

                if ($debug)
                {
                    var_dump(json_encode($order));
                    var_dump($api['api_key']);
                    var_dump($host);
                    var_dump(base64_encode($signature));
                }
                else
                {
                    // setup for pushing to unleashed
                    $client = new Client();

                    $response = $client->post(
                        $host, [
                            'headers' => $headers,
                            'json'    => $order,
                            'query'   => $query
                        ]
                    );

                    if (!$response->getStatusCode() == 201)
                    {
                        $responseBodyAsString = $response->getBody()->getContents();
                        Log::critical('Catch failure for Pharma Order #' . $cartid . ' - ' . $responseBodyAsString);
                        $submission_result = false;
                    }

                    $body = json_decode($response->getBody());
                    ShoppingCartModel::updateUnleashedInformationPharma($cartid, $body->OrderNumber, $order['Guid']);
                    $submission_result = true;
                }
            }

            // ----------------------------------------------------------------------------------------
            // END Generate the JSONS DRUGS
            // ----------------------------------------------------------------------------------------

            if ($debug) {
                var_dump("-------------------------------------------------");
            }

            // ----------------------------------------------------------------------------------------
            // Generate the JSONS VITAMINS
            // ----------------------------------------------------------------------------------------

            if (count($vitamin_lines)>0)
            {
                // Vitamins goes second
                $order['SalesOrderLines']  = $vitamin_lines;
                $order['Customer']['Guid'] = $customer_guid;
                $esky_guid                 = $vitamins_extra_guids["esky"];
                $shipping_guid             = $vitamins_extra_guids["shipping"];
                $unleashed_id              = Unleashed::where('brand_id', 2)->first()->id;

                $count = count($order['SalesOrderLines']);

                // Split shipping if there is
                if ($cart->cart_shipping > 0) {
                    $count++;
                    $order['SalesOrderLines'][] = array(
                        'LineNumber'    => $count,
                        'BatchNumbers'  => null,
                        'LineType'      => null,
                        'Product'       => array('Guid' => $shipping_guid),
                        'OrderQuantity' => 1,
                        'UnitPrice'     => $cart->cart_shipping,
                        'DiscountRate'  => 0,
                        'LineTotal'     => $cart->cart_shipping,
                        'Comments'      => '',
                        'TaxRate'       => ($cart->cart_total_tax > 0) ? 0.1 : 0,
                        'LineTax'       => ($cart->cart_total_tax > 0) ? round(($cart->cart_shipping * 0.1), 2) : 0
                    );
                }

                // Split esky count
                if ($cart->cart_cold_items > 0) {
                    $count++;
                    $order['SalesOrderLines'][] = array(
                        'LineNumber'    => $count,
                        'BatchNumbers'  => null,
                        'LineType'      => null,
                        'Product'       => array('Guid' => $esky_guid),
                        'OrderQuantity' => 1,
                        'UnitPrice'     => $cart->cart_esky,
                        'DiscountRate'  => ($cart->cart_esky == 0) ? 1 : 0,
                        'LineTotal'     => $cart->cart_esky,
                        'Comments'      => '',
                        'TaxRate'       => ($cart->cart_total_tax > 0) ? 0.1 : 0,
                        'LineTax'       => ($cart->cart_total_tax > 0) ? round(($cart->cart_esky * 0.1), 2) : 0,
                    );
                }

                // Was this paid in full?
                if ($cartstatus == "Approved")
                {
                    $count++;
                    $order['SalesOrderLines'][] = array(
                        'LineNumber'    => $count,
                        'BatchNumbers'  => null,
                        'LineType'      => null,
                        'Product'       => array('ProductCode' => 'PIF'),
                        'OrderQuantity' => 1,
                        'UnitPrice'     => 0,
                        'DiscountRate'  => 0,
                        'LineTotal'     => 0,
                        'Comments'      => '',
                        'TaxRate'       => 0,
                        'LineTax'       => 0
                    );
                }

                // generate order UUID
                $order['Guid'] = (string) Str::uuid();

                if (!array_key_exists('SalesOrderLines', $order)) {
                    Log::emergency('Vitamin Order #' . $cartid . ' - Sales order lines are missing');
                    $submission_result = false;
                }

                // sets defaults
                $subtotal  = 0;
                $total     = 0;
                $tax_total = 0;

                foreach ($order['SalesOrderLines'] as $index => $row) {

                    // calculates Sub Total
                    $subtotal = $subtotal + $order['SalesOrderLines'][$index]['LineTotal'];

                    // calculates Tax Total
                    $tax_total = $tax_total + $order['SalesOrderLines'][$index]['LineTax'];

                    // calculates Total
                    $total = $total +  $order['SalesOrderLines'][$index]['LineTotal'] + $order['SalesOrderLines'][$index]['LineTax'];
                }

                $order['OrderStatus'] = 'Placed';
                $order['SubTotal']    = round($subtotal, 4);
                $order['TaxTotal']    = round($tax_total, 4);
                $order['Total']       = round($total, 4);

                $host   = config('unleashed.host'). '/SalesOrders/' . $order['Guid'];
                $api    = Api::getApi($unleashed_id);

                // generate signature
                $query      = array('serialBatch' => 'true');
                $http_query = http_build_query($query);
                $signature  = hash_hmac('sha256', $http_query, $api['api_secret'], true);

                // generate headers
                $headers = array(
                    'Content-Type'       => 'application/json',
                    'Accept'             => 'application/json',
                    'api-auth-signature' => base64_encode($signature),
                    'api-auth-id'        => $api['api_key']
                );

                if (count($api) != 3) {
                    Log::emergency('Vitamin Order #' . $cartid . ' - Unable to find matching unleashed id for ' . $unleashed_id);
                    $submission_result = false;
                }


                if ($debug)
                {
                    var_dump(json_encode($order));
                    var_dump($api['api_key']);
                    var_dump($host);
                    var_dump(base64_encode($signature));
                }
                else
                {
                    // setup for pushing to unleashed
                    $client = new Client();
                    $response = $client->post(
                        $host, [
                            'headers' => $headers,
                            'json'    => $order,
                            'query'   => $query
                        ]
                    );

                    if (!$response->getStatusCode() == 201)
                    {
                        $responseBodyAsString = $response->getBody()->getContents();
                        Log::critical('Catch failure for Vitamin Order #' . $cartid . ' - ' . $responseBodyAsString);
                        $submission_result = false;
                    }

                    $body = json_decode($response->getBody());
                    ShoppingCartModel::updateUnleashedInformation($cartid, $body->OrderNumber, $order['Guid']);
                    $submission_result = true;
                }
            }

            // ----------------------------------------------------------------------------------------
            // END Generate the JSONS
            // ----------------------------------------------------------------------------------------

            return true;

        }
    }
}
