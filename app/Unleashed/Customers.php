<?php
namespace App\Unleashed;

use App\Models\Addressbook;
use App\Models\CustomerPersonModel;
use App\Models\UnleashedCustomer;
use App\Unleashed\Api as Api;
use App\Unleashed\Helper as Helper;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Customers
{

    protected $resource = '/Customers';
    //static protected $host; //LACHLAN REMOVED AS NOT WORKING - ADD BACK IF REQUIRED FOR CREATION OF CUSTOMERS

    public function __construct()
    {
        $this->host = config('unleashed.host').$this->resource;
        $this->api = Api::getApi();
    }

    public function get($modifiedSince)
    {
        foreach ($this->api as $api) {
            $this->unleashed_id = $api['unleashed_id'];
            $this->auth_id      = $api['api_key'];
            $this->secret       = $api['api_secret'];

            $results = $this->fetch($modifiedSince);
            $paging  = $results['paging'];
            if ($paging->NumberOfPages > 1) {
                for ($page = 2; $page <= $paging->NumberOfPages; $page++) {
                    $this->fetch($modifiedSince, $page);
                }
            }

        }
    }

    /**
     * Fetch unleashed data
     *
     * @return array
     */
    public function fetch($modifiedSince = '2019-11-11', $page = 1, $pageSize = 500)
    {
        $results = array();
        $client = new Client(); //GuzzleHttp\Client
        $host = $this->host . '/' . $page;

        $query = array(
            'pageSize' => $pageSize,
            'modifiedSince' => $modifiedSince
        );

        $http_query = http_build_query($query);
        $signature = hash_hmac('sha256', $http_query, $this->secret, true);

        $headers = array(
            'Content-Type'       => 'application/json',
            'Accept'             => 'application/json',
            'api-auth-signature' => base64_encode($signature),
            'api-auth-id'        => $this->auth_id
        );

        $response = $client->get(
            $host, [
                'headers' => $headers,
                'query'   => $query
            ]
        );

        if (!$response->getStatusCode() == 200) return false;

        $body = json_decode($response->getBody());

        $results['paging'] = $body->Pagination;
        $results['items'] = $body->Items;
        $this->update($body->Items);

        return $results;
    }

    /**
     * Upsert customer data
     *
     * @return void
     */
    public function update($items)
    {
        foreach($items as $item) {

            $sales_rep_fullname = isset($item->SalesPerson->FullName) ? $item->SalesPerson->FullName: null;
            $sales_rep_email= isset($item->SalesPerson->Email) ? $item->SalesPerson->Email: null;
            $sales_rep_guid = isset($item->SalesPerson->Guid) ? $item->SalesPerson->Guid: null;

            if ($item->CustomerCode != 'pretend') {
                DB::table('ucustomers')->updateOrInsert(
                    ['customer_code' => $item->CustomerCode, 'unleashed_id' => $this->unleashed_id],
                    [
                        'customer_name'         => $item->CustomerName,
                        'gstvat_number'         => $item->GSTVATNumber,
                        'phone_number'          => $item->PhoneNumber,
                        'fax_number'            => $item->FaxNumber,
                        'mobile_number'         => $item->MobileNumber,
                        'toll_free_number'      => $item->TollFreeNumber,
                        'ddi_number'            => $item->DDINumber,
                        'email'                 => $item->Email,
                        'email_cc'              => $item->EmailCC,
                        'website'               => $item->Website,
                        'currency_code'         => $item->Currency->CurrencyCode,
                        'notes'                 => $item->Notes,

                        // contacts
                        'contact_firstname'     => $item->ContactFirstName,
                        'contact_lastname'      => $item->ContactLastName,

                        // accounts
                        'taxable'               => $item->Taxable,
                        'tax_rate'              => $item->TaxRate,
                        'tax_code'              => $item->TaxCode,
                        'discount_rate'         => $item->DiscountRate,
                        'payment_term'          => $item->PaymentTerm,

                        // Sales person
                        'sales_rep_fullname'    => $sales_rep_fullname,
                        'sales_rep_email'       => $sales_rep_email,
                        'sales_rep_guid'        => $sales_rep_guid,

                        // statuses
                        'print_invoice'         => $item->PrintInvoice,
                        'stop_credit'           => $item->StopCredit,
                        'obsolete'              => $item->Obsolete,

                        // categories
                        'customer_type'         => $item->CustomerType,

                        // who did it and when
                        'unleashed_created_by'  => $item->CreatedBy,
                        'unleashed_created_on'  => Helper::jsonToDate($item->CreatedOn, 'Y-m-d H:i:s'),
                        'unleashed_modified_by' => $item->LastModifiedBy,
                        'unleashed_modified_on' => Helper::jsonToDate($item->LastModifiedOn, 'Y-m-d H:i:s'),
                        'unleashed_id'          => $this->unleashed_id,

                        'guid'                  => $item->Guid,
                        'created_at'            => date('Y-m-d H:i'),
                        'updated_at'            => date('Y-m-d H:i')

                    ]
                );
            }
        }

    }

    /**
     * Push customer to unleashed for upsert
     *
     * @param string $guid
     * @return void
     */
    public static function push($guid)
    {
        Log::notice('Submitting parent guid ' . $guid . ' via addressbook model');

        // gets customer GUID
        $customer = UnleashedCustomer::find($guid);

        $contact_firstname = $contact_lastname = NULL;
        $customer_contact = CustomerPersonModel::PersonList($guid);
        if (count($customer_contact)>0) {
            $current = $customer_contact[0];
            $contact_firstname = $current->first_name;
            $contact_lastname = $current->last_name;
        }

        $addresses = array();
        $customer_addresses = Addressbook::AddressPrimaryList($guid)->get();
        if (count($customer_addresses)>0)
        {
            foreach ($customer_addresses as $item)
            {
                $addie = array();
                $addie["AddressType"]         = ($item->address_type == 'Delivery') ? 'Shipping' : $item->address_type;
                $addie["AddressName"]         = $item->address_name;
                $addie["StreetAddress"]       = $item->address_line_1;
                $addie["StreetAddress2"]      = $item->address_line_2;
                $addie["Suburb"]              = $item->suburb;
                $addie["City"]                = NULL;
                $addie["Region"]              = $item->state;
                $addie["Country"]             = $item->country;
                $addie["PostalCode"]          = $item->postcode;
                $addie["IsDefault"]           = true;
                $addie["DeliveryInstruction"] = NULL;

                $addresses[] = $addie;
            }
        }

        // gets unleashed id to get the API key
        // $api_key = $customer->unleashed->api_key;
        $unleashed_id = $customer->unleashed_id;

        // generates payload
        $payload['Guid']                             = $customer->guid;
        $payload['Addresses']                        = $addresses;
        $payload['CustomerCode']                     = $customer->customer_code;
        $payload['CustomerName']                     = $customer->customer_name;
        $payload['GSTVATNumber']                     = $customer->gstvat_number;
        $payload['BankName']                         = null;
        $payload['BankBranch']                       = null;
        $payload['BankAccount']                      = null;
        $payload['Website']                          = $customer->website;
        $payload['FaxNumber']                        = $customer->fax_number;
        $payload['MobileNumber']                     = $customer->mobile_number;
        $payload['DDINumber']                        = $customer->ddi_number;
        $payload['TollFreeNumber']                   = $customer->toll_free_number;
        $payload['Email']                            = $customer->email;
        $payload['EmailCC']                          = $customer->email_cc;
        $payload['Notes']                            = $customer->notes;

        $payload['Taxable']                          = ($customer->taxable) ? true : false;
        if ($customer->taxable) {

            $payload['TaxCode']                          = $customer->tax_code;
            $payload['TaxRate']                          = $customer->tax_rate;
        }

        $payload['SalesPerson']                      = null;
        $payload['DiscountRate']                     = $customer->discount_rate;
        $payload['PrintPackingSlipInsteadOfInvoice'] = false;
        $payload['PrintInvoice']                     = ($customer->print_invoice) ? true : false;
        $payload['StopCredit']                       = ($customer->stop_credit) ? true : false;
        $payload['Obsolete']                         = ($customer->obsolete) ? true : false;
        $payload['XeroSalesAccount']                 = null;
        $payload['XeroCostOfGoodsAccount']           = null;
        $payload['SellPriceTier']                    = "";
        $payload['SellPriceTierReference']           = null;

        $payload['Obsolete']                         = ($customer->status == 6) ? true : false;
        $payload['StopCredit']                       = ($customer->status == 3 || $customer->status == 4 || $customer->status == 5) ? true : false;

        $payload['CustomerType']                     = "Pharmacy";
        $payload['PaymentTerm']                      = $customer->payment_term;
        $payload['ContactFirstName']                 = $contact_firstname;
        $payload['ContactLastName']                  = $contact_lastname;

        $json_payload = json_encode($payload);

        // setup for pushing to unleashed
        $client = new Client(); //GuzzleHttp\Client
        $host = config('unleashed.host'). '/Customers/' . $customer->guid;
        $api = Api::getApi($unleashed_id);

        // generate signature
        $query = array();
        $http_query = http_build_query($query);
        $signature = hash_hmac('sha256', $http_query, $api['api_secret'], true);

        // generate headers
        $headers = array(
            'Content-Type'       => 'application/json',
            'Accept'             => 'application/json',
            'api-auth-signature' => base64_encode($signature),
            'api-auth-id'        => $api['api_key']
        );

        // push to unleashed for upsert
        $response = $client->post(
            $host, [
                'headers' => $headers,
                'json' => $payload,
                'query'   => $query
            ]
        );

        if (!$response->getStatusCode() == 200) return false;
        return $response;
    }

    /**
     * Push customer to unleashed for upsert - customer code is needed to identify unleashes
     * New clients are easy with X & X
     * Existing C will push C & C
     * Existing MC will push MC & MC
     * Existing C will push C & MC -- this needs customer_code
     *
     * @param string $guid
     * @return void
     */
    public static function pushSpecificUnleashed($guid, $unleashed_id, $customer_code)
    {
        // gets customer GUID
        $customer = UnleashedCustomer::find($guid);

        // Set the parent customer variable
        $parent_customer = $customer;

        // Validate customer is from VMS
        if (!$customer)
        {
            // Are we pushing pharma
            $customer = UnleashedCustomer::where('pharma_customer_guid', $guid)->first();
            if (!$customer) {
                Log::critical($guid . ' not found in any system');
                return false;
            }
        }

        Log::notice('Submitting guid ' . $guid . '(' . $customer_code . ') to unleashedId ' . $unleashed_id . ' with addresses of parent guid ' . $parent_customer->guid);

        // generates payload
        $payload['Guid']                             = $guid;
        $payload['CustomerCode']                     = $customer_code;

        // Generating addresses (always use primary guid)
        $addresses = array();
        $customer_addresses = Addressbook::AddressPrimaryList($parent_customer->guid)->get();
        if (count($customer_addresses)>0)
        {
            foreach ($customer_addresses as $item)
            {
                $addie = array();
                $addie["AddressType"]         = ($item->address_type == 'Delivery') ? 'Shipping' : $item->address_type;
                $addie["AddressName"]         = $item->address_name;
                $addie["StreetAddress"]       = $item->address_line_1;
                $addie["StreetAddress2"]      = $item->address_line_2;
                $addie["Suburb"]              = $item->suburb;
                $addie["City"]                = NULL;
                $addie["Region"]              = $item->state;
                $addie["Country"]             = $item->country;
                $addie["PostalCode"]          = $item->postcode;
                $addie["IsDefault"]           = true;
                $addie["DeliveryInstruction"] = NULL;

                $addresses[] = $addie;
            }
        }

        $payload['Addresses']                        = $addresses;

        $payload['Taxable']                          = ($customer->taxable) ? true : false;
        if ($customer->taxable) {

            $payload['TaxCode']                          = $customer->tax_code;
            $payload['TaxRate']                          = $customer->tax_rate;
        }

        $payload['CustomerName']                     = $customer->customer_name;
        $payload['GSTVATNumber']                     = $customer->gstvat_number;
        $payload['BankName']                         = null;
        $payload['BankBranch']                       = null;
        $payload['BankAccount']                      = null;
        $payload['Website']                          = $customer->website;
        $payload['FaxNumber']                        = $customer->fax_number;
        $payload['MobileNumber']                     = $customer->mobile_number;
        $payload['DDINumber']                        = $customer->ddi_number;
        $payload['TollFreeNumber']                   = $customer->toll_free_number;
        $payload['Email']                            = $customer->email;
        $payload['EmailCC']                          = $customer->email_cc;
        $payload['Notes']                            = $customer->notes;
        $payload['SalesPerson']                      = null;
        $payload['DiscountRate']                     = $customer->discount_rate;
        $payload['PrintPackingSlipInsteadOfInvoice'] = false;
        $payload['PrintInvoice']                     = ($customer->print_invoice) ? true : false;
        $payload['StopCredit']                       = ($customer->stop_credit) ? true : false;
        $payload['Obsolete']                         = ($customer->obsolete) ? true : false;
        $payload['XeroSalesAccount']                 = null;
        $payload['XeroCostOfGoodsAccount']           = null;
        $payload['SellPriceTier']                    = "";
        $payload['SellPriceTierReference']           = null;

        $payload['Obsolete']                         = ($customer->status == 6) ? true : false;
        $payload['StopCredit']                       = ($customer->status == 3 || $customer->status == 4 || $customer->status == 5) ? true : false;

        $payload['CustomerType']                     = "Pharmacy";
        $payload['PaymentTerm']                      = $customer->payment_term;

        // Generating people (always use primary guid)
        $contact_firstname = $contact_lastname = NULL;
        $customer_contact = CustomerPersonModel::PersonList($customer->guid);
        if (count($customer_contact)>0) {
            $current = $customer_contact[0];
            $contact_firstname = $current->first_name;
            $contact_lastname = $current->last_name;
        }

        $payload['ContactFirstName']                 = $contact_firstname;
        $payload['ContactLastName']                  = $contact_lastname;

        $json_payload = json_encode($payload);

        // setup for pushing to unleashed
        $client = new Client(); //GuzzleHttp\Client
        $host = config('unleashed.host'). '/Customers/' . $guid;
        $api = Api::getApi($unleashed_id);

        // generate signature
        $query = array();
        $http_query = http_build_query($query);
        $signature = hash_hmac('sha256', $http_query, $api['api_secret'], true);

        // generate headers
        $headers = array(
            'Content-Type'       => 'application/json',
            'Accept'             => 'application/json',
            'api-auth-signature' => base64_encode($signature),
            'api-auth-id'        => $api['api_key']
        );

        // push to unleashed for upsert
        $response = $client->post(
            $host, [
                'headers' => $headers,
                'json'    => $payload,
                'query'   => $query
            ]
        );

        if (!$response->getStatusCode() == 200) return false;
        return $response;
    }

}
