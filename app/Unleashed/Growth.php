<?php
namespace App\Unleashed;

use DateTime;
use DateTimeZone;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Growth
{
    public function __construct()
    {
        // init
    }

    public function run_all()
    {
        $clients = array();
        $thismonth = date("n");

        $year_s = ($thismonth<=6) ? date("Y")-1 : date("Y");
        $cfy_date_from = $year_s .'-07-01';
        $cfy_date_to = $year_s . date("-m-d");

        $year_s = ($thismonth<=6) ? date("Y")-2 : date("Y")-1;
        $lfy_date_from = $year_s .'-07-01';
        $lfy_date_to = $year_s . date("-m-d");

        // Update everyone to 0
        DB::update('update ucustomers set growth_rate = ? where growth_rate IS NULL', [0]);
        DB::update('update ucustomers set drug_growth_rate = ? where drug_growth_rate IS NULL', [0]);

        // find all the orders for today VITAMINS ONLY
        $sql = "
            SELECT  rs1.customer_code,
                    (
                        SELECT SUM(sub_total)
                        FROM orders
                        WHERE order_date >= '" . $cfy_date_from . "'
                        AND order_date <= '" . $cfy_date_to . "'
                        AND customer_code = rs1.customer_code
                        AND order_status != 'Deleted'
                        AND unleashed_id = '2bd9c34b-ad75-4a17-a1f1-83ce0fcc33d4'
                    ) AS 'cfy_sales',
                    (
                        SELECT SUM(sub_total)
                        FROM orders
                        WHERE order_date >= '" . $lfy_date_from . "'
                        AND order_date <= '" . $lfy_date_to . "'
                        AND customer_code = rs1.customer_code
                        AND order_status != 'Deleted'
                        AND unleashed_id = '2bd9c34b-ad75-4a17-a1f1-83ce0fcc33d4'
                    ) AS 'lfy_sales',
                    (
                        SELECT SUM(indirect_sales.true_sales_price)
                        FROM indirect_sales
                        INNER JOIN product_wholesaler on (indirect_sales.product_code = product_wholesaler.registration_number AND product_wholesaler.deleted_at IS NULL)
                        INNER JOIN customer_wholesalers on (customer_wholesalers.registration_number = indirect_sales.customer_number AND customer_wholesalers.deleted_at IS NULL)
                        INNER JOIN products on (products.guid = product_wholesaler.product_guid)
                        INNER JOIN ucustomers on (ucustomers.guid = customer_wholesalers.customer_id)
                        WHERE ucustomers.customer_code = rs1.customer_code
                        AND indirect_sales.invoice_date >= '" . $cfy_date_from . "'
                        AND indirect_sales.invoice_date <= '" . $cfy_date_to . "'
                        AND indirect_sales.true_sales_price > 0

                    ) as cfy_indirect_sales,
                    (
                        SELECT SUM(indirect_sales.true_sales_price)
                        FROM indirect_sales
                        INNER JOIN product_wholesaler on (indirect_sales.product_code = product_wholesaler.registration_number AND product_wholesaler.deleted_at IS NULL)
                        INNER JOIN customer_wholesalers on (customer_wholesalers.registration_number = indirect_sales.customer_number AND customer_wholesalers.deleted_at IS NULL)
                        INNER JOIN products on (products.guid = product_wholesaler.product_guid)
                        INNER JOIN ucustomers on (ucustomers.guid = customer_wholesalers.customer_id)
                        WHERE ucustomers.customer_code = rs1.customer_code
                        AND indirect_sales.invoice_date >= '" . $lfy_date_from . "'
                        AND indirect_sales.invoice_date <= '" . $lfy_date_to . "'
                        AND indirect_sales.true_sales_price > 0
                    ) as lfy_indirect_sales,
                    (SELECT order_date FROM orders WHERE customer_code = rs1.customer_code and order_status != 'Deleted' and unleashed_id = '2bd9c34b-ad75-4a17-a1f1-83ce0fcc33d4' ORDER BY order_date DESC LIMIT 1) AS last_purchase_date
            FROM    (
                        SELECT      oo.customer_code
                        FROM        orders oo
                        WHERE       oo.unleashed_id = '2bd9c34b-ad75-4a17-a1f1-83ce0fcc33d4'
                        AND         oo.order_status != 'Deleted'
                        GROUP BY    oo.customer_code
                    )rs1
        ";

        $all_orders = DB::select($sql);
        foreach ($all_orders as $item)
        {
            $growth = 0;
            $item->cfy_sales = $item->cfy_sales + $item->cfy_indirect_sales;
            $item->lfy_sales = $item->lfy_sales + $item->lfy_indirect_sales;

            if ($item->cfy_sales > 0 && $item->lfy_sales > 0) {
                $growth = round(((($item->cfy_sales - $item->lfy_sales) / $item->lfy_sales) * 100), 2);
            } else if ($item->cfy_sales > 0 && $item->lfy_sales <= 0) {
                $growth = '99999999';
            } else if ($item->cfy_sales == 0 && $item->lfy_sales > 0) {
                $growth = '-100';
            } else if ($item->cfy_sales == 0 && $item->lfy_sales == 0) {
                $growth = 0;
            }

            DB::update('update ucustomers set growth_rate = ?, date_last_purchase = ?, updated_at = ? where customer_code = ?', [$growth, $item->last_purchase_date, date("Y-m-d H:i:s"), $item->customer_code]);
        }


        // find all the orders for today for DRUGS
        $sql = "
            SELECT  rs1.customer_code,
                    (SELECT SUM(sub_total) FROM orders WHERE order_date >= '" . $cfy_date_from . "' AND order_date <= '" . $cfy_date_to . "' AND customer_code = rs1.customer_code and order_status != 'Deleted' and unleashed_id = '6c9a056b-3713-4cfc-bfdd-50be48e0c275' ) AS 'cfy_sales',
                    (SELECT SUM(sub_total) FROM orders WHERE order_date >= '" . $lfy_date_from . "' AND order_date <= '" . $lfy_date_to . "' AND customer_code = rs1.customer_code and order_status != 'Deleted' and unleashed_id = '6c9a056b-3713-4cfc-bfdd-50be48e0c275' ) AS 'lfy_sales',
                    (SELECT order_date FROM orders WHERE customer_code = rs1.customer_code and order_status != 'Deleted' and unleashed_id = '6c9a056b-3713-4cfc-bfdd-50be48e0c275' ORDER BY order_date DESC LIMIT 1) AS last_purchase_date
            FROM    (
                        SELECT      oo.customer_code
                        FROM        orders oo
                        WHERE       oo.unleashed_id = '6c9a056b-3713-4cfc-bfdd-50be48e0c275'
                        AND         oo.order_status != 'Deleted'
                        GROUP BY    oo.customer_code
                    )rs1
        ";

        $all_orders = DB::select($sql);
        foreach ($all_orders as $item)
        {
            $growth = 0;

            if ($item->cfy_sales > 0 && $item->lfy_sales > 0) {
                $growth = round(((($item->cfy_sales - $item->lfy_sales) / $item->lfy_sales) * 100), 2);
            } else if ($item->cfy_sales > 0 && $item->lfy_sales <= 0) {
                $growth = '99999999';
            } else if ($item->cfy_sales == 0 && $item->lfy_sales > 0) {
                $growth = '-100';
            } else if ($item->cfy_sales == 0 && $item->lfy_sales == 0) {
                $growth = 0;
            }

            DB::update('update ucustomers set drug_growth_rate = ?, drug_date_last_purchase = ?, updated_at = ? where pharma_customer_code = ?', [$growth, $item->last_purchase_date, date("Y-m-d H:i:s"), $item->customer_code]);
        }
    }

    public function calculate()
    {
        $clients = array();

        $thismonth = date("n");

        $year_s = ($thismonth<=6) ? date("Y")-1 : date("Y");
        $cfy_date_from = $year_s .'-07-01';
        $cfy_date_to = $year_s . date("-m-d");

        $year_s = ($thismonth<=6) ? date("Y")-2 : date("Y")-1;
        $lfy_date_from = $year_s .'-07-01';
        $lfy_date_to = $year_s . date("-m-d");

        // find all the orders for today VITAMINS
        $sql = "
            SELECT      oo.customer_code,
                        (
                            SELECT SUM(sub_total)
                            FROM orders
                            WHERE order_date >= '" . $cfy_date_from . "'
                            AND order_date <= '" . $cfy_date_to . "'
                            AND unleashed_id = '2bd9c34b-ad75-4a17-a1f1-83ce0fcc33d4'
                            AND order_status != 'Deleted'
                            AND customer_code = oo.customer_code

                        ) AS 'cfy_sales',
                        (
                            SELECT SUM(sub_total)
                            FROM orders
                            WHERE order_date >= '" . $lfy_date_from . "'
                            AND order_date <= '" . $lfy_date_to . "'
                            AND unleashed_id = '2bd9c34b-ad75-4a17-a1f1-83ce0fcc33d4'
                            AND order_status != 'Deleted'
                            AND customer_code = oo.customer_code

                        ) AS 'lfy_sales',
                        (
                            SELECT SUM(indirect_sales.true_sales_price)
                            FROM indirect_sales
                            INNER JOIN product_wholesaler on (indirect_sales.product_code = product_wholesaler.registration_number AND product_wholesaler.deleted_at IS NULL)
                            INNER JOIN customer_wholesalers on (customer_wholesalers.registration_number = indirect_sales.customer_number AND customer_wholesalers.deleted_at IS NULL)
                            INNER JOIN products on (products.guid = product_wholesaler.product_guid)
                            INNER JOIN ucustomers on (ucustomers.guid = customer_wholesalers.customer_id)
                            WHERE ucustomers.customer_code = oo.customer_code
                            AND indirect_sales.invoice_date >= '" . $cfy_date_from . "'
                            AND indirect_sales.invoice_date <= '" . $cfy_date_to . "'
                            AND indirect_sales.true_sales_price > 0

                        ) as cfy_indirect_sales,
                        (
                            SELECT SUM(indirect_sales.true_sales_price)
                            FROM indirect_sales
                            INNER JOIN product_wholesaler on (indirect_sales.product_code = product_wholesaler.registration_number AND product_wholesaler.deleted_at IS NULL)
                            INNER JOIN customer_wholesalers on (customer_wholesalers.registration_number = indirect_sales.customer_number AND customer_wholesalers.deleted_at IS NULL)
                            INNER JOIN products on (products.guid = product_wholesaler.product_guid)
                            INNER JOIN ucustomers on (ucustomers.guid = customer_wholesalers.customer_id)
                            WHERE ucustomers.customer_code = oo.customer_code
                            AND indirect_sales.invoice_date >= '" . $lfy_date_from . "'
                            AND indirect_sales.invoice_date <= '" . $lfy_date_to . "'
                            AND indirect_sales.true_sales_price > 0
                        ) as lfy_indirect_sales
            FROM        orders oo
            WHERE       oo.order_date = '" . date("Y-m-d") . "'
            AND         oo.unleashed_id = '2bd9c34b-ad75-4a17-a1f1-83ce0fcc33d4'
            AND         oo.order_status != 'Deleted'
        ";

        $todays_orders = DB::select($sql);
        foreach ($todays_orders as $item)
        {
            $growth = 0;
            $item->cfy_sales = $item->cfy_sales + $item->cfy_indirect_sales;
            $item->lfy_sales = $item->lfy_sales + $item->lfy_indirect_sales;

            $item->lfy_sales = (is_null($item->lfy_sales)) ? 0 : $item->lfy_sales;
            $item->cfy_sales = (is_null($item->cfy_sales)) ? 0 : $item->cfy_sales;

            if ($item->cfy_sales > 0 && $item->lfy_sales > 0) {
                $growth = round(((($item->cfy_sales - $item->lfy_sales) / $item->lfy_sales) * 100), 2);
            } else if ($item->cfy_sales > 0 && $item->lfy_sales <= 0) {
                $growth = '99999999';
            } else if ($item->cfy_sales == 0 && $item->lfy_sales > 0) {
                $growth = '-100';
            } else if ($item->cfy_sales == 0 && $item->lfy_sales == 0) {
                $growth = 0;
            }

            DB::update('update ucustomers set growth_rate = ?, date_last_purchase = ?, updated_at = ? where customer_code = ?', [$growth, date("Y-m-d"), date("Y-m-d H:i:s"), $item->customer_code]);
        }


        // find all the orders for today DRUGS
        $sql = "
            SELECT      oo.customer_code,
                        (
                            SELECT SUM(sub_total)
                            FROM orders
                            WHERE order_date >= '" . $cfy_date_from . "'
                            AND order_date <= '" . $cfy_date_to . "'
                            AND unleashed_id = '6c9a056b-3713-4cfc-bfdd-50be48e0c275'
                            AND order_status != 'Deleted'
                            AND customer_code = oo.customer_code

                        ) AS 'cfy_sales',
                        (
                            SELECT SUM(sub_total)
                            FROM orders
                            WHERE order_date >= '" . $lfy_date_from . "'
                            AND order_date <= '" . $lfy_date_to . "'
                            AND unleashed_id = '6c9a056b-3713-4cfc-bfdd-50be48e0c275'
                            AND order_status != 'Deleted'
                            AND customer_code = oo.customer_code

                        ) AS 'lfy_sales'
            FROM        orders oo
            WHERE       oo.order_date = '" . date("Y-m-d") . "'
            AND         oo.unleashed_id = '6c9a056b-3713-4cfc-bfdd-50be48e0c275'
            AND         oo.order_status != 'Deleted'
        ";

        $todays_orders = DB::select($sql);
        foreach ($todays_orders as $item)
        {
            $growth = 0;

            $item->lfy_sales = (is_null($item->lfy_sales)) ? 0 : $item->lfy_sales;
            $item->cfy_sales = (is_null($item->cfy_sales)) ? 0 : $item->cfy_sales;

            if ($item->cfy_sales > 0 && $item->lfy_sales > 0) {
                $growth = round(((($item->cfy_sales - $item->lfy_sales) / $item->lfy_sales) * 100), 2);
            } else if ($item->cfy_sales > 0 && $item->lfy_sales <= 0) {
                $growth = '99999999';
            } else if ($item->cfy_sales == 0 && $item->lfy_sales > 0) {
                $growth = '-100';
            } else if ($item->cfy_sales == 0 && $item->lfy_sales == 0) {
                $growth = 0;
            }

            DB::update('update ucustomers set drug_growth_rate = ?, drug_date_last_purchase = ?, updated_at = ? where pharma_customer_code = ?', [$growth, date("Y-m-d"), date("Y-m-d H:i:s"), $item->customer_code]);
        }
    }
}
