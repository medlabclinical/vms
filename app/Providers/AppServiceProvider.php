<?php

namespace App\Providers;

use App\Models\Addressbook;
use App\Models\Articles;
use App\Models\InteractionModel;
use App\Models\PdfTypes;
use App\Models\ProductPdfModel;
use App\Models\ShoppingCartModel;
use App\Models\ShoppingCartPaymentModel;
use App\Observers\AddressbookObserver;
use App\Observers\InteractionObserver;
use App\Observers\ShoppingCartObserver;
use App\Observers\ShoppingCartPaymentObserver;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (config('app.env') === 'production') {
            \URL::forceScheme('https');
        }
        $allData = Articles::where('publish', 1)->whereNull('deleted_at')->whereHas('brand', function ($q) {
            $q->where('brand.id', 2);
        })->get();
        $header_article_category = array();
        $list = array();
        foreach ($allData as $article) {
            if ($article->article_category()->first()) {
                $name = $article->article_category()->first()->name;
                $article->category = $name;
                if (!isset($list[$name])) {
                    array_push($header_article_category, $name);
                    $list[$name] = [];
                }
                array_push($list[$name], $article);
            } else {
                $article->category = '';
            }
        }
        $header_pdf_list = PdfTypes::whereHas('brand', function ($q) {
            $q->where('brand.id', 2);
        })->get();
        foreach ($header_pdf_list as $key => $item) {

            if (!count($item->hasPdf()->whereHas('brand', function ($q) {
                $q->where('brand.id', 2);
            })->where('product_pdfs.obsolete','!=','1')->whereNull('product_pdfs.deleted_at')->get())) {
                $header_pdf_list->forget($key);
            }
        }
        View::share('header_article_category', $header_article_category);
        View::share('header_pdf_list', $header_pdf_list);

        Schema::defaultStringLength(191);

        InteractionModel::observe(InteractionObserver::class);
        ShoppingCartPaymentModel::observe(ShoppingCartPaymentObserver::class);
        Addressbook::observe(AddressbookObserver::class);

    }
}
