<?php

namespace App\Observers;

use App\Mail\SalesEmail;
use App\Mail\WholesalerEmail;
use App\Models\SecondaryClassification;
use App\Models\ShoppingCartModel;
use App\Models\ShoppingCartPaymentModel;
use App\Models\User;
use App\Models\WholesalerModel;
use App\Unleashed\Orders;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class ShoppingCartPaymentObserver
{
    /**
     * Handle the ShoppingCartPaymentModel "created" event.
     *
     * @param  \App\ShoppingCartPaymentModel  $shopping_cart_payment
     * @return void
     */
    public function created(ShoppingCartPaymentModel $shopping_cart_payment)
    {
        $receivers[] = 'wilma_smith@medlab.co';

        if ($shopping_cart_payment->status == 'Approved' || $shopping_cart_payment->status == 'Account Terms')
        {
            $cart = ShoppingCartModel::where('cart_id', $shopping_cart_payment->cart_id)->first();
            Mail::to($receivers)->queue(new SalesEmail($shopping_cart_payment));
        }
    }

    /**
     * Handle the ShoppingCartPaymentModel "updated" event.
     *
     * @param  \App\ShoppingCartPaymentModel  $shopping_cart_payment
     * @return void
     */
    public function updated(ShoppingCartPaymentModel $shopping_cart_payment)
    {
        //
    }

    /**
     * Handle the ShoppingCartPaymentModel "deleted" event.
     *
     * @param  \App\ShoppingCartPaymentModel  $shopping_cart_payment
     * @return void
     */
    public function deleted(ShoppingCartPaymentModel $shopping_cart_payment)
    {
        //
    }

    /**
     * Handle the ShoppingCartPaymentModel "forceDeleted" event.
     *
     * @param  \App\ShoppingCartPaymentModel  $shopping_cart_payment
     * @return void
     */
    public function forceDeleted(ShoppingCartPaymentModel $shopping_cart_payment)
    {
        //
    }
}
