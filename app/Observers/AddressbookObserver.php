<?php

namespace App\Observers;

use App\Models\Addressbook;
use App\Models\UnleashedCustomer;
use Illuminate\Support\Facades\Log;

class AddressbookObserver
{
    /**
     * Handle the addressbook "updated" event.
     *
     * @param  \App\Addressbook  $addressbook
     * @return void
     */
    public function created(Addressbook $addressbook)
    {
        if ($addressbook->address_type == 'Physical' && $addressbook->is_primary == 1) {
            UnleashedCustomer::updateSecondaryClassification($addressbook->customer_code, $addressbook->state);
        }
    }

    /**
     * Handle the addressbook "updated" event.
     *
     * @param  \App\Addressbook  $addressbook
     * @return void
     */
    public function updated(Addressbook $addressbook)
    {
        if ($addressbook->address_type == 'Physical' && $addressbook->is_primary == 1) {
            UnleashedCustomer::updateSecondaryClassification($addressbook->customer_code, $addressbook->state);
        }
    }
}
