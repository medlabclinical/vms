<?php

namespace App\Observers;

use App\Mail\InteractionEmail;
use App\Models\InteractionModel;
use Illuminate\Support\Facades\Mail;

class InteractionObserver
{
    /**
     * Handle the interaction "created" event.
     *
     * @param  \App\Interaction  $interaction
     * @return void
     */
    public function created(InteractionModel $interaction)
    {
    	// Only send the email if the option is ticked
    	if ($interaction->send_email == 1) {

    		if ($interaction->email_other != '') {
                $interaction->email_other = json_decode($interaction->email_other);
        		Mail::to($interaction->email_other)->queue(new InteractionEmail($interaction));
        	}
    	}
    }
}
