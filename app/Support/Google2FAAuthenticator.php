<?php

namespace App\Support;

use App\Models\User;
use Illuminate\Support\Facades\Cookie;
use PragmaRX\Google2FALaravel\Support\Authenticator;
use Auth;

class Google2FAAuthenticator extends Authenticator
{
    protected function canPassWithoutCheckingOTP()
    {

        return false;
//        if(Auth::guard('practitioner')->user()->loginSecurity == null){
//            return true;
//        }else{
//            return false;
//            return
//                !Auth::guard('practitioner')->user()->loginSecurity->google2fa_enable ||
//                !$this->isEnabled() ||
//                $this->noUserIsAuthenticated() ||
//                $this->twoFactorAuthStillValid();
//        }
    }

    protected function getGoogle2FASecretKey()
    {
        $user = User::where('email',Cookie::get('email'))->first();
        $secret = $user->loginSecurity->{$this->config('otp_secret_column')};

        if (is_null($secret) || empty($secret)) {
            throw new InvalidSecretKey('Secret key cannot be empty.');
        }

        return $secret;
    }
    public function checkGoogleTwoFactorExpire(){
        return
            !Auth::guard('practitioner')->user()->loginSecurity->google2fa_enable ||
            !$this->isEnabled()
        ||
//            $this->noUserIsAuthenticated() ||
            $this->twoFactorAuthStillValid();
    }

}
