<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Unleashed API URL
    |--------------------------------------------------------------------------
    |
    | Unleashed API Host URL
    |
    */

    'host' => env('UNLEASHED_API_HOST', 'https://api.unleashedsoftware.com'),

    /*
    |--------------------------------------------------------------------------
    | Unleashed API Auth ID
    |--------------------------------------------------------------------------
    |
    | Unleashed API Auth ID
    |
    */

    'auth_id' => explode(',', env('UNLEASHED_API_AUTH_ID', '')),

    /*
    |--------------------------------------------------------------------------
    | Unleashed API Auth ID
    |--------------------------------------------------------------------------
    |
    | Unleashed API Auth ID
    |
    */

    'secret' => explode(',', env('UNLEASHED_API_AUTH_KEY', '')),




];
