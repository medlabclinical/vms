<?php

return [

    // These CSS rules will be applied after the regular template CSS

    /*
        'css' => [
            '.button-content .button { background: red }',
        ],
    */

    'colors' => [

        'highlight' => '#004a88',
        'button'    => '#004a88',

    ],

    'view' => [
        'senderName'  => 'Medlab',
        'reminder'    => null,
        'unsubscribe' => null,
        'address'     => null,

        'logo'        => [
            'path'   => 'https://cdn.medlab.co/www/Images/logo-white.png',
            'width'  => '50%',
            'height' => '',
        ],

        'twitter'  => 'https://twitter.com/medlabclinical',
        'facebook' => 'https://twitter.com/medlabclinical',
        'flickr'   => null,
    ],

];
