@extends('www.layouts.main')
@section('title')
    {{ $content->title }}
@endsection
@section('description')
    @if($content->meta_description)
        {{ $content->meta_description }}
    @elseif(json_decode($content->content))
        @foreach(json_decode($content->content) as $item)
            @if($item->layout=='text')
                {{ substr(str_replace('&nbsp;',' ',strip_tags($item->attributes->content)),0,160) }}
            @endif
        @endforeach
    @endif
@endsection
@section('breadcrumbs')
    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset;background-color:#7AA43F;">
            <ol class="breadcrumb medlab_breadcrumbs" style="margin-bottom: unset;background-color:#7AA43F;">
            </ol>
        </div>
    </div>

    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset">
            <ol class="breadcrumb medlab_breadcrumbs">
                <li><a class="medlab_breadcrumbs_link" href="/">@lang('trs.home')</a></li>
                <li class="active medlab_breadcrumbs_text">@lang('trs.clinical trials')</li>
            </ol>
        </div>
    </div>
@endsection

@section('content')
    <main class="cd-main-content" id="main" style="margin-top:90px">

        <!-- SLIDER IMAGE
================================================== -->

        @if(json_decode($content->slider))
            <section class="home">
                <div class="slider-container">
                    <div class="tp-banner-container">
                        <div class="tp-banner">
                            <ul>
                                @foreach(json_decode($content->slider) as $slider)
                                    @if($slider->layout=='video')
                                        <li data-transition="fade" data-slotamount="1" data-masterspeed="500"
                                            data-saveperformance="on" data-title="Intro Slide">
                                            <iframe
                                                src="https://player.vimeo.com/video/{{ $slider->attributes->video_link }}"
                                                width="100%" height="100%" align="center" frameborder="0"
                                                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                                allowfullscreen></iframe>
                                        </li>
                                    @elseif($slider->layout=='image')
                                        <li data-transition="fade" data-slotamount="1" data-masterspeed="500"
                                            data-saveperformance="on" data-title="Intro Slide">
                                            <img
                                                src="{{ $image_url }}/www/Images/{{ \App\Models\MediaLibrary::where('id',$slider->attributes->slider_image)->first()->name }}"
                                                alt="{{ $slider->attributes->slider_alt }}"
                                                data-lazyload="{{ $image_url }}/www/Images/{{ \App\Models\MediaLibrary::where('id',$slider->attributes->slider_image)->first()->name }}"
                                                data-bgposition="center top" data-bgfit="cover"
                                                data-bgrepeat="no-repeat">
                                            <a href="#">
                                                <div class="black-heavy-3">
                                                    <div class="black-heavy-3-heading">
                                                        <h1>{{ $slider->attributes->title }}</h1></div>
                                                    <div
                                                        class="black-heavy-3-subheading">{{ $slider->attributes->subtitle }}</div>
                                                </div>
                                            </a>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
        @else
            <br>
        @endif

    <!-- CONTENT SECTION
    ================================================== -->

        @if(json_decode($content->content))
            <section class="section white-section section-padding-top" id="scroll-link">
                <div class="container">
                    <div class="sixteen columns remove-bottom">
                        <div class="full-image">
                            <div class="articleClass padding">
                                @foreach(json_decode($content->content) as $item)
                                    @if($item->layout=='text')
                                        <div class="sixteen columns">
                                            {!! $item->attributes->content !!}
                                        </div>
                                    @elseif($item->layout=='image')
                                        <div class="sixteen columns">
                                            <img src="{{ $image_url }}/www/Images/{{ \App\Models\MediaLibrary::where('id',$item->attributes->image)->first()->name }}"
                                                 alt="{{ $item->attributes->image_alt }}" style = "width: {{ $item->attributes->width }}%; float: {{ $item->attributes->float }}">
                                        </div>
                                    @else
                                        <div class="sixteen columns">
                                            <iframe src="{{ $item->attributes->content }}?transparent=false"
                                                    width="100%" height="500"
                                                    frameborder=”0″ webkitallowfullscreen mozallowfullscreen
                                                    allowfullscreen></iframe>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        @endif


    <!-- CLINICAL TRIALS SECTION
  ================================================== -->

        <section class="section white-section section-padding-top-bottom">
            <div class="container">
                <div class="sixteen columns">
                    <div id="portfolio-filter">
                        <ul id="filter">
                            <li><a href="#" class="current" data-filter="*" title="">Show All</a></li>
                            @foreach($typeFilter as $filter)
                                <li><a href="#" data-filter=".{{ $filter->id }}" title="">{{ $filter->name }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="clear"></div>
                <div id="shop-grid">
                    @foreach($clinicalTrialList as $item)
                        @if(isset( $item->clinical_trial_type()->first()->name))
                            <div class="one-third column
                            @if(count($item->clinical_trial_type))
                            @foreach($item->clinical_trial_type as $filter)
                            {{ $filter->id }}
                            @endforeach
                            @endif
                                ">

                                <div class="blog-box-1 link-post">
                                    <a href="{{ route('clinical_trials_page',['id' => preg_replace('/--+/','-',preg_replace('/[^A-Za-z0-9\-]/', '-', $item->name)).'-'.$item->id]) }}"
                                       class="animsition-link">
                                        @if(isset( $item->image()->first()->name))
                                            <div
                                                style="max-height: 270px; position:relative; padding-bottom: 56.25%; height: 0;">
                                                <img
                                                    src="{{ $image_url }}/www/Images/{{ $item->image()->first()->name }}"
                                                    alt=" " style="margin-left: auto;
                                                 margin-right: auto; position: absolute; top: 0; left: 0; width: 100%; height: 100%;">
                                            </div>
                                        @endif
                                        <h4 class="two-row-max"
                                            style="min-height: 96px;">{{ $item->name }}</h4>
                                        <p class="four-row-max"
                                           style="padding-bottom: unset;min-height: 92px;">{{ $item->clinical_trial_summary }}</p>
                                        <br>
                                        <div style="min-height: 64px;">
                                            <div class="skills-name">Progression - {{ $item->progression }}</div>
                                        </div>
                                        <div class="pro-bar-container pro-bar-margin">
                                            @if($item->progression_percentage != null )
                                                <div class="pro-bar bar-{{$item->progression_percentage}}"
                                                     data-pro-bar-percent="{{$item->progression_percentage}}"></div>
                                                <div class="text-in-bar"><span
                                                        class="counter-skills">{{$item->progression_percentage}}</span>%
                                                </div>
                                                <div class="arrow-skills"></div>
                                            @elseif($item->progression =='Discovery - Not Recruiting')
                                                <div class="pro-bar bar-1" data-pro-bar-percent="1"></div>
                                                <div class="text-in-bar"><span class="counter-skills">1</span>%
                                                </div>
                                                <div class="arrow-skills"></div>
                                            @elseif($item->progression =='Discovery - Recruiting')
                                                <div class="pro-bar bar-2" data-pro-bar-percent="2"></div>
                                                <div class="text-in-bar"><span class="counter-skills">2</span>%
                                                </div>
                                                <div class="arrow-skills"></div>
                                            @elseif($item->progression =='Discovery - Recruiting Closed')
                                                <div class="pro-bar bar-5" data-pro-bar-percent="5"></div>
                                                <div class="text-in-bar"><span class="counter-skills">5</span>%
                                                </div>
                                                <div class="arrow-skills"></div>
                                            @elseif($item->progression =='Discovery - Completed')
                                                <div class="pro-bar bar-10" data-pro-bar-percent="10"></div>
                                                <div class="text-in-bar"><span class="counter-skills">10</span>%
                                                </div>
                                                <div class="arrow-skills"></div>
                                            @elseif($item->progression =='Pre-Clinical - Not Recruiting')
                                                <div class="pro-bar bar-11" data-pro-bar-percent="11"></div>
                                                <div class="text-in-bar"><span class="counter-skills">11</span>%
                                                </div>
                                                <div class="arrow-skills"></div>
                                            @elseif($item->progression =='Pre-Clinical - Recruiting')
                                                <div class="pro-bar bar-12" data-pro-bar-percent="12"></div>
                                                <div class="text-in-bar"><span class="counter-skills">12</span>%
                                                </div>
                                                <div class="arrow-skills"></div>
                                            @elseif($item->progression =='Pre-Clinical - Recruiting Closed')
                                                <div class="pro-bar bar-15" data-pro-bar-percent="15"></div>
                                                <div class="text-in-bar"><span class="counter-skills">15</span>%
                                                </div>
                                                <div class="arrow-skills"></div>
                                            @elseif($item->progression =='Pre-Clinical - Completed')
                                                <div class="pro-bar bar-20" data-pro-bar-percent="20"></div>
                                                <div class="text-in-bar"><span class="counter-skills">20</span>%
                                                </div>
                                                <div class="arrow-skills"></div>
                                            @elseif($item->progression =='FIH Safety & Efficacy - Not Recruiting')
                                                <div class="pro-bar bar-21" data-pro-bar-percent="21"></div>
                                                <div class="text-in-bar"><span class="counter-skills">21</span>%
                                                </div>
                                                <div class="arrow-skills"></div>
                                            @elseif($item->progression =='FIH Safety & Efficacy - Recruiting')
                                                <div class="pro-bar bar-22" data-pro-bar-percent="22"></div>
                                                <div class="text-in-bar"><span class="counter-skills">22</span>%
                                                </div>
                                                <div class="arrow-skills"></div>
                                            @elseif($item->progression =='FIH Safety & Efficacy - Recruiting Closed')
                                                <div class="pro-bar bar-25" data-pro-bar-percent="25"></div>
                                                <div class="text-in-bar"><span class="counter-skills">25</span>%
                                                </div>
                                                <div class="arrow-skills"></div>
                                            @elseif($item->progression =='FIH Safety & Efficacy - Completed')
                                                <div class="pro-bar bar-30" data-pro-bar-percent="30"></div>
                                                <div class="text-in-bar"><span class="counter-skills">30</span>%
                                                </div>
                                                <div class="arrow-skills"></div>
                                            @elseif($item->progression =='Late Stage-Large Population Trials. Safety & Efficacy - Not Recruiting')
                                                <div class="pro-bar bar-50" data-pro-bar-percent="50"></div>
                                                <div class="text-in-bar"><span class="counter-skills">50</span>%
                                                </div>
                                                <div class="arrow-skills"></div>
                                            @elseif($item->progression =='Late Stage-Large Population Trials. Safety & Efficacy - Recruiting')
                                                <div class="pro-bar bar-55" data-pro-bar-percent="55"></div>
                                                <div class="text-in-bar"><span class="counter-skills">55</span>%
                                                </div>
                                                <div class="arrow-skills"></div>
                                            @elseif($item->progression =='Late Stage-Large Population Trials. Safety & Efficacy - Recruiting Closed')
                                                <div class="pro-bar bar-65" data-pro-bar-percent="65"></div>
                                                <div class="text-in-bar"><span class="counter-skills">65</span>%
                                                </div>
                                                <div class="arrow-skills"></div>
                                            @elseif($item->progression =='Late Stage-Large Population Trials. Safety & Efficacy - Completed')
                                                <div class="pro-bar bar-75" data-pro-bar-percent="75"></div>
                                                <div class="text-in-bar"><span class="counter-skills">75</span>%
                                                </div>
                                                <div class="arrow-skills"></div>
                                            @elseif($item->progression =='Regulatory Submission - Submitted')
                                                <div class="pro-bar bar-90" data-pro-bar-percent="90"></div>
                                                <div class="text-in-bar"><span class="counter-skills">90</span>%
                                                </div>
                                                <div class="arrow-skills"></div>
                                            @elseif($item->progression =='Regulatory Submission - Approved')
                                                <div class="pro-bar bar-100" data-pro-bar-percent="100"></div>
                                                <div class="text-in-bar"><span class="counter-skills">100</span>%
                                                </div>
                                                <div class="arrow-skills"></div>
                                            @else
                                                <div class="pro-bar bar-0" data-pro-bar-percent="0"></div>
                                                <div class="text-in-bar"><span class="counter-skills">0</span>%
                                                </div>
                                                <div class="arrow-skills"></div>
                                            @endif
                                        </div>
                                    </a>
                                    @if($item->open_for_partnering==1)
                                        <div class="button-shortcodes text-size-1 text-padding-1 version-1"
                                             onClick="location.href = '/partner_opportunities';"
                                             style="float:unset;width: 100%;">
                                            <span>&#xf18e;</span>Open to partnering
                                        </div>
                                    @else
                                        <br><br>
                                    @endif
                                </div>

                            </div>
                        @else
                            <div class="one-third column">

                                <div class="blog-box-1 link-post">
                                    <a href="{{ route('clinical_trials_page',['id' => preg_replace('/--+/','-',preg_replace('/[^A-Za-z0-9\-]/', '-', $item->name)).'-'.$item->id]) }}"
                                       class="animsition-link">
                                        @if(isset( $item->image()->first()->name))
                                            <div
                                                style="max-height: 270px; position:relative; padding-bottom: 56.25%; height: 0;">
                                                <img
                                                    src="{{ $image_url }}/www/Images/{{ $item->image()->first()->name }}"
                                                    alt=" " style="margin-left: auto;
                                                 margin-right: auto; position: absolute; top: 0; left: 0; width: 100%; height: 100%;">
                                            </div>
                                        @endif
                                        <h4 class="two-row-max"
                                            style="min-height: 96px;">{{ $item->name }}</h4>
                                        <p class="four-row-max"
                                           style="padding-bottom: unset;min-height: 92px;">{{ $item->clinical_trial_summary }}</p>
                                        <br>
                                        <div style="min-height: 64px;">
                                            <div class="skills-name">Progression - {{ $item->progression }}</div>
                                        </div>
                                        <div class="pro-bar-container pro-bar-margin">
                                            @if($item->progression_percentage != null )
                                                <div class="pro-bar bar-{{$item->progression_percentage}}"
                                                     data-pro-bar-percent="{{$item->progression_percentage}}"></div>
                                                <div class="text-in-bar"><span
                                                        class="counter-skills">{{$item->progression_percentage}}</span>%
                                                </div>
                                                <div class="arrow-skills"></div>
                                            @elseif($item->progression =='Indication - Not Recruiting')
                                                <div class="pro-bar bar-5" data-pro-bar-percent="5"></div>
                                                <div class="text-in-bar"><span class="counter-skills">5</span>%</div>
                                                <div class="arrow-skills"></div>
                                            @elseif($item->progression =='Indication - Recruiting')
                                                <div class="pro-bar bar-10" data-pro-bar-percent="10"></div>
                                                <div class="text-in-bar"><span class="counter-skills">10</span>%</div>
                                                <div class="arrow-skills"></div>
                                            @elseif($item->progression =='Indication - Recruiting Closed')
                                                <div class="pro-bar bar-15" data-pro-bar-percent="15"></div>
                                                <div class="text-in-bar"><span class="counter-skills">15</span>%</div>
                                                <div class="arrow-skills"></div>
                                            @elseif($item->progression =='Indication - Completed')
                                                <div class="pro-bar bar-20" data-pro-bar-percent="20"></div>
                                                <div class="text-in-bar"><span class="counter-skills">20</span>%</div>
                                                <div class="arrow-skills"></div>
                                            @elseif($item->progression =='Discovery - Not Recruiting')
                                                <div class="pro-bar bar-25" data-pro-bar-percent="25"></div>
                                                <div class="text-in-bar"><span class="counter-skills">25</span>%</div>
                                                <div class="arrow-skills"></div>
                                            @elseif($item->progression =='Discovery - Recruiting')
                                                <div class="pro-bar bar-30" data-pro-bar-percent="30"></div>
                                                <div class="text-in-bar"><span class="counter-skills">30</span>%</div>
                                                <div class="arrow-skills"></div>
                                            @elseif($item->progression =='Discovery - Recruiting Closed')
                                                <div class="pro-bar bar-35" data-pro-bar-percent="35"></div>
                                                <div class="text-in-bar"><span class="counter-skills">35</span>%</div>
                                                <div class="arrow-skills"></div>
                                            @elseif($item->progression =='Discovery - Completed')
                                                <div class="pro-bar bar-40" data-pro-bar-percent="40"></div>
                                                <div class="text-in-bar"><span class="counter-skills">40</span>%</div>
                                                <div class="arrow-skills"></div>
                                            @elseif($item->progression =='Pre-Clinical - Not Recruiting')
                                                <div class="pro-bar bar-45" data-pro-bar-percent="45"></div>
                                                <div class="text-in-bar"><span class="counter-skills">45</span>%</div>
                                                <div class="arrow-skills"></div>
                                            @elseif($item->progression =='Pre-Clinical - Recruiting')
                                                <div class="pro-bar bar-50" data-pro-bar-percent="50"></div>
                                                <div class="text-in-bar"><span class="counter-skills">50</span>%</div>
                                                <div class="arrow-skills"></div>
                                            @elseif($item->progression =='Pre-Clinical - Recruiting Closed')
                                                <div class="pro-bar bar-55" data-pro-bar-percent="55"></div>
                                                <div class="text-in-bar"><span class="counter-skills">55</span>%</div>
                                                <div class="arrow-skills"></div>
                                            @elseif($item->progression =='Pre-Clinical - Completed')
                                                <div class="pro-bar bar-60" data-pro-bar-percent="60"></div>
                                                <div class="text-in-bar"><span class="counter-skills">60</span>%</div>
                                                <div class="arrow-skills"></div>
                                            @elseif($item->progression =='FIH Safety & Efficacy - Not Recruiting')
                                                <div class="pro-bar bar-65" data-pro-bar-percent="65"></div>
                                                <div class="text-in-bar"><span class="counter-skills">65</span>%</div>
                                                <div class="arrow-skills"></div>
                                            @elseif($item->progression =='FIH Safety & Efficacy - Recruiting')
                                                <div class="pro-bar bar-70" data-pro-bar-percent="70"></div>
                                                <div class="text-in-bar"><span class="counter-skills">70</span>%</div>
                                                <div class="arrow-skills"></div>
                                            @elseif($item->progression =='FIH Safety & Efficacy - Recruiting Closed')
                                                <div class="pro-bar bar-75" data-pro-bar-percent="75"></div>
                                                <div class="text-in-bar"><span class="counter-skills">75</span>%</div>
                                                <div class="arrow-skills"></div>
                                            @elseif($item->progression =='FIH Safety & Efficacy - Completed')
                                                <div class="pro-bar bar-80" data-pro-bar-percent="80"></div>
                                                <div class="text-in-bar"><span class="counter-skills">80</span>%</div>
                                                <div class="arrow-skills"></div>
                                            @elseif($item->progression =='Regulatory Submission - Submitted')
                                                <div class="pro-bar bar-85" data-pro-bar-percent="90"></div>
                                                <div class="text-in-bar"><span class="counter-skills">75</span>%</div>
                                                <div class="arrow-skills"></div>
                                            @elseif($item->progression =='Regulatory Submission - Approved')
                                                <div class="pro-bar bar-90" data-pro-bar-percent="100"></div>
                                                <div class="text-in-bar"><span class="counter-skills">90</span>%</div>
                                                <div class="arrow-skills"></div>
                                            @else
                                                <div class="pro-bar bar-0" data-pro-bar-percent="0"></div>
                                                <div class="text-in-bar"><span class="counter-skills">0</span>%</div>
                                                <div class="arrow-skills"></div>
                                            @endif
                                        </div>
                                    </a>
                                    @if($item->open_for_partnering==1)
                                        <div class="button-shortcodes text-size-1 text-padding-1 version-1"
                                             onClick="location.href = '/partner_opportunities';"
                                             style="float:unset;width: 100%;">
                                            <span>&#xf18e;</span>Open to partnering
                                        </div>
                                    @else
                                        <br><br>
                                    @endif
                                </div>

                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </section>


        <!-- MORE PAGES SECTION RESEARCH
                    ================================================== -->
        <section class="section white-section section-padding-top-bottom">
            <div class="container">
                <div class="sixteen columns">
                    <h2>More</h2>
                </div>
                <div class="container">

                    <div class="clear"></div>
                    @foreach($research_list as $item)
                        <div class="four columns">
                            <a href="{{ $item->page_url }}">
                                <div class="portfolio-box-2 grey-section">
                                    @if(json_decode($item->slider))
                                        @foreach(json_decode($item->slider) as $slider)
                                            @if($slider->layout=='image')
                                                <img
                                                    src="{{ $image_url }}/www/Images/{{ \App\Models\MediaLibrary::where('id',$slider->attributes->slider_image)->first()->name }}"
                                                    style="display: inline-block;margin-top: unset; width: 100%;"/>
                                            @endif
                                        @endforeach
                                    @endif
                                    <div>
                                        <p class="two-row-max"
                                           style="height: 50px; text-align: center">{{ $item->title }}</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>

        @include('www.layouts.footer')
    </main>

    <div class="scroll-to-top">&#xf106;</div>






    <!-- JAVASCRIPT
    ================================================== -->
    <script type="text/javascript" src="{{ asset('/js/jquery-2.1.1.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/modernizr.custom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.mobile.custom.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/retina-1.1.0.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.animsition.min.js') }}"></script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            $(document).ready(function () {
                @if (session('login_error'))
                open_login();
                @endif
                $(".animsition").animsition({

                    inClass: 'zoom-in-sm',
                    outClass: 'zoom-out-sm',
                    inDuration: 1500,
                    outDuration: 800,
                    linkElement: '.animsition-link',
                    // e.g. linkElement   :   'a:not([target="_blank"]):not([href^=#])'
                    loading: true,
                    loadingParentElement: 'body', //animsition wrapper element
                    loadingClass: 'animsition-loading',
                    unSupportCss: ['animation-duration',
                        '-webkit-animation-duration',
                        '-o-animation-duration'
                    ],
                    //"unSupportCss" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
                    //The default setting is to disable the "animsition" in a browser that does not support "animation-duration".

                    overlay: false,

                    overlayClass: 'animsition-overlay-slide',
                    overlayParentElement: 'body'
                });
            });
        })(jQuery);
    </script>


    <script type="text/javascript" src="{{ asset('/js/jquery.easing.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.hidescroll.min.js') }}"></script>
    <script type="text/javascript">
        $('.header-top').hidescroll();
    </script>
    <script type="text/javascript" src="{{ asset('/js/smoothScroll.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.parallax-1.1.3.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/owl.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/masonry.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/isotope.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/scrollReveal.js') }}"></script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            window.scrollReveal = new scrollReveal();
        })(jQuery);
    </script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            jQuery(document).ready(function () {
                var offset = 450;
                var duration = 500;
                jQuery(window).scroll(function () {
                    if (jQuery(this).scrollTop() > offset) {
                        jQuery('.scroll-to-top').fadeIn(duration);
                    } else {
                        jQuery('.scroll-to-top').fadeOut(duration);
                    }
                });

                jQuery('.scroll-to-top').click(function (event) {
                    event.preventDefault();
                    jQuery('html, body').animate({scrollTop: 0}, duration);
                    return false;
                })
            });
        })(jQuery);
    </script>



    <script type="text/javascript" src="{{ asset('js/pro-bars.js') }}"></script>


    <script type="text/javascript" src="{{ asset('/js/jquery.themepunch.tools.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.themepunch.revolution.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/visible.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.counterup.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/waypoints.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/custom-portfolio-home-1.js') }}"></script>
    {{--    <script type="text/javascript" src="{{ asset('/js/custom-about-1.js') }}"></script>--}}
    <script type="text/javascript" src="{{ asset('/js/custom-shop-home-2.js') }}"></script>
    {{--    <script type="text/javascript" src="{{ asset('/js/custom-tabs.js') }}"></script>--}}

    <link rel="stylesheet" type="text/css" href="{{ asset('/DataTables/datatables.min.css') }}"/>

    <script type="text/javascript" src="{{ asset('/DataTables/datatables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/custom-corporate-home-1.js') }}"></script>







    <!-- End Document
    ================================================== -->
@endsection

