﻿@extends('www.layouts.main')
@section('title')
    {{ $content->title }}
@endsection
@section('description')
    @if($content->meta_description)
        {{ $content->meta_description }}
    @elseif(json_decode($content->content))
        @foreach(json_decode($content->content) as $item)
            @if($item->layout=='text')
                {{ substr(str_replace('&nbsp;',' ',strip_tags($item->attributes->content)),0,160) }}
            @endif
        @endforeach
    @endif
@endsection
@section('breadcrumbs')
    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset;background-color:#7AA43F;">
            <ol class="breadcrumb medlab_breadcrumbs" style="margin-bottom: unset;background-color:#7AA43F;">
            </ol>
        </div>
    </div>

    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset">
            <ol class="breadcrumb medlab_breadcrumbs">
                <li><a class="medlab_breadcrumbs_link" href="/">@lang('trs.home')</a></li>
                <li class="active medlab_breadcrumbs_text">@lang('trs.publications')</li>
            </ol>
        </div>
    </div>
@endsection


@section('content')
    <main class="cd-main-content" id="main" style="margin-top:90px">

        <!-- SLIDER IMAGE
================================================== -->

        @if(json_decode($content->slider))
            <section class="home">
                <div class="slider-container">
                    <div class="tp-banner-container">
                        <div class="tp-banner">
                            <ul>
                                @foreach(json_decode($content->slider) as $slider)
                                    @if($slider->layout=='video')
                                        <li data-transition="fade" data-slotamount="1" data-masterspeed="500"
                                            data-saveperformance="on" data-title="Intro Slide">
                                            <iframe
                                                src="https://player.vimeo.com/video/{{ $slider->attributes->video_link }}"
                                                width="100%" height="100%" align="center" frameborder="0"
                                                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                                allowfullscreen></iframe>
                                        </li>
                                    @elseif($slider->layout=='image')
                                        <li data-transition="fade" data-slotamount="1" data-masterspeed="500"
                                            data-saveperformance="on" data-title="Intro Slide">
                                            <img
                                                src="{{ $image_url }}/www/Images/{{ \App\Models\MediaLibrary::where('id',$slider->attributes->slider_image)->first()->name }}"
                                                alt="{{ $slider->attributes->slider_alt }}"
                                                data-lazyload="{{ $image_url }}/www/Images/{{ \App\Models\MediaLibrary::where('id',$slider->attributes->slider_image)->first()->name }}"
                                                data-bgposition="center top" data-bgfit="cover"
                                                data-bgrepeat="no-repeat">
                                            <a href="#">
                                                <div class="black-heavy-3">
                                                    <div class="black-heavy-3-heading">
                                                        <h1>{{ $slider->attributes->title }}</h1></div>
                                                    <div
                                                        class="black-heavy-3-subheading">{{ $slider->attributes->subtitle }}</div>
                                                </div>
                                            </a>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
        @else
            <br>
        @endif

    <!-- CONTENT SECTION
    ================================================== -->

        @if(json_decode($content->content))
            <section class="section white-section section-padding-top" id="scroll-link">
                <div class="container">
                    <div class="sixteen columns remove-bottom"
                    >
                        <div class="full-image">
                            <div class="articleClass padding">
                                @foreach(json_decode($content->content) as $item)
                                    @if($item->layout=='text')
                                        <div class="sixteen columns">
                                            {!! $item->attributes->content !!}
                                        </div>
                                    @elseif($item->layout=='image')
                                        <div class="sixteen columns">
                                            <img
                                                src="{{ $image_url }}/www/Images/{{ \App\Models\MediaLibrary::where('id',$item->attributes->image)->first()->name }}"
                                                alt="{{ $item->attributes->image_alt }}"
                                                style="width: {{ $item->attributes->width }}%; float: {{ $item->attributes->float }}">
                                        </div>
                                    @else
                                        <div class="sixteen columns">
                                            <iframe src="{{ $item->attributes->content }}?transparent=false"
                                                    width="100%" height="500"
                                                    frameborder=”0″ webkitallowfullscreen mozallowfullscreen
                                                    allowfullscreen></iframe>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        @endif

    <!-- PUBLICATIONS SECTION
        ================================================== -->
        <section class="section grey-section section-padding-top-bottom">

            <div class="container">
                <div class="sixteen columns">
                    <div class="shortcodes-carousel">

                        <div id="sync-sortcodes-1" class="owl-carousel">

                            <div class="item grey-section" id="publications">

                                <br>
                                @foreach($publication as $year)
                                    @foreach($year as $item)
                                        <div class="accordion">
                                            <div class="accordion_in"
                                                 data-scroll-reveal="enter left move 200px over 1s after 0.3s">
                                                <div class="acc_head white-section"
                                                     style="text-transform: unset;">{{$item->title}}</div>
                                                <div class="acc_content white-section">
                                                    @if($item->publication_location!='' && $item->publication_location!='Null')
                                                        <p>{{ $item->publication_location }}</p><br>
                                                    @endif
                                                    @if($item->date!='' && $item->date!='Null')
                                                        <p><strong>Date:</strong>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp{{ substr($item->date,0,10) }}
                                                        </p><br>
                                                    @endif

                                                    @if(count($item->member))
                                                        <p><strong>Authors:</strong>&nbsp&nbsp&nbsp
                                                            @if(count($item->member))
                                                                {{$item->member[0]['name']}}
                                                                @if(count($item->member)!=1)
                                                                    @for($i=1;$i<count($item->member);$i++)
                                                                        ,&nbsp{{ $item->member[$i]['name'] }}
                                                                    @endfor
                                                                @endif
                                                            @endif
                                                        </p><br>
                                                    @endif
                                                    @if($item->link!='' && $item->link!='Null')
                                                        <p><strong>Link:</strong>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<a
                                                                style="color: #6ba53a;word-break: break-word;"
                                                                href="{{ $item->link }}"
                                                                target="_blank">{{ $item->link }}</a></p>
                                                    @endif
                                                    @if(count($item->article) || count($item->product_family) || count($item->clinical_trial) || count($item->product_ingredient) || count($item->product_group))
                                                        <br><br>
                                                        <hr style="margin:unset;"/>
                                                        <br><br>
                                                        @if(count($item->article))
                                                            <p><strong>Related Articles</strong></p><br>
                                                            @foreach($item->article as $a)
                                                                <a style="color: #6ba53a;word-break: break-word;"
                                                                   href={{ route('www.articles_content',['id' => $a['id']]) }} target="_blank">{{ $a['title'] }}</a>
                                                                <br><br>
                                                            @endforeach
                                                        @endif
                                                        @if(count($item->product_family))
                                                            <p><strong>Related Products</strong></p><br>
                                                            @foreach($item->product_family as $a)
                                                                <a>{{ $a['product_name'] }}</a>
                                                                <br><br>
                                                            @endforeach
                                                        @endif
                                                        @if(count($item->clinical_trial))
                                                            <p><strong>Related Clinical Trials</strong></p><br>
                                                            @foreach($item->clinical_trial as $a)
                                                                <a>{{ $a['name'] }}</a>
                                                                <br><br>
                                                            @endforeach
                                                        @endif
                                                        @if(count($item->product_ingredient))
                                                            <p><strong>Related Ingredients</strong></p><br>
                                                            @foreach($item->product_ingredient as $a)
                                                                <a>{{ $a['name'] }}</a>
                                                                <br><br>
                                                            @endforeach
                                                        @endif
                                                        @if(count($item->product_group))
                                                            <p><strong>Related Product Groups</strong></p><br>
                                                            @foreach($item->product_group as $a)
                                                                <a>{{ $a['name'] }}</a>
                                                                <br><br>
                                                            @endforeach
                                                        @endif
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endforeach
                            </div>
                            <div class="item grey-section" id="books">

                                <br>
                                @foreach($book as $year)
                                    @foreach($year as $item)
                                        <div class="accordion">
                                            <div class="accordion_in"
                                                 data-scroll-reveal="enter left move 200px over 1s after 0.3s">
                                                <div class="acc_head white-section" style="text-transform: unset"
                                                     ;>{{$item->title}}</div>
                                                <div class="acc_content white-section">
                                                    @if($item->date!='' && $item->date!='Null')
                                                        <p><strong>Date:</strong>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp{{ substr($item->date,0,10) }}
                                                        </p><br>
                                                    @endif
                                                    @if($item->publication_location!='' && $item->publication_location!='Null')
                                                        <p>{{ $item->publication_location }}</p><br>
                                                    @endif
                                                    @if(count($item->member))
                                                        <p><strong>Authors:</strong>&nbsp&nbsp&nbsp
                                                            @if(count($item->member))
                                                                {{$item->member[0]['name']}}
                                                                @if(count($item->member)!=1)
                                                                    @for($i=1;$i<count($item->member);$i++)
                                                                        ,&nbsp{{ $item->member[$i]['name'] }}
                                                                    @endfor
                                                                @endif
                                                            @endif
                                                        </p><br>
                                                    @endif
                                                    @if($item->link!='' && $item->link!='Null')
                                                        <p><strong>Link:</strong>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<a
                                                                href="{{ $item->link }}">{{ $item->link }}</a></p>
                                                    @endif
                                                    @if(count($item->article) || count($item->product_family) || count($item->clinical_trial) || count($item->product_ingredient) || count($item->product_group))
                                                        <br><br>
                                                        <hr style="border : 1px dashed;margin:unset;"/>
                                                        <br><br>
                                                        @if(count($item->article))
                                                            <p><strong>Related Articles</strong></p><br>
                                                            @foreach($item->article as $a)
                                                                <a href={{ route('www.articles_content',['id' => $a['id']]) }}>{{ $a['title'] }}</a>
                                                                <br><br>
                                                            @endforeach
                                                        @endif
                                                        @if(count($item->product_family))
                                                            <p><strong>Related Products</strong></p><br>
                                                        @endif
                                                        @if(count($item->clinical_trial))
                                                            <p><strong>Related Clinical Trials</strong></p><br>
                                                        @endif
                                                        @if(count($item->product_ingredient))
                                                            <p><strong>Related Ingredients</strong></p><br>
                                                        @endif
                                                        @if(count($item->product_group))
                                                            <p><strong>Related Product Groups</strong></p><br>
                                                        @endif
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endforeach
                            </div>
                            <div class="item grey-section" id="conferences">

                                <br>
                                @foreach($conference as $year)
                                    @foreach($year as $item)
                                        <div class="accordion">
                                            <div class="accordion_in"
                                                 data-scroll-reveal="enter left move 200px over 1s after 0.3s">
                                                <div class="acc_head white-section" style="text-transform: unset"
                                                     ;>{{$item->title}}</div>
                                                <div class="acc_content white-section">
                                                    @if($item->date!='' && $item->date!='Null')
                                                        <p><strong>Date:</strong>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp{{ substr($item->date,0,10) }}
                                                        </p><br>
                                                    @endif
                                                    @if($item->publication_location!='' && $item->publication_location!='Null')
                                                        <p>{{ $item->publication_location }}</p><br>
                                                    @endif
                                                    @if(count($item->member))
                                                        <p><strong>Authors:</strong>&nbsp&nbsp&nbsp
                                                            @if(count($item->member))
                                                                {{$item->member[0]['name']}}
                                                                @if(count($item->member)!=1)
                                                                    @for($i=1;$i<count($item->member);$i++)
                                                                        ,&nbsp{{ $item->member[$i]['name'] }}
                                                                    @endfor
                                                                @endif
                                                            @endif
                                                        </p><br>
                                                    @endif
                                                    @if($item->link!='' && $item->link!='Null')
                                                        <p><strong>Link:</strong>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<a
                                                                href="{{ $item->link }}">{{ $item->link }}</a></p>
                                                    @endif
                                                    @if(count($item->article) || count($item->product_family) || count($item->clinical_trial) || count($item->product_ingredient) || count($item->product_group))
                                                        <br><br>
                                                        <hr style="border : 1px dashed;margin:unset;"/>
                                                        <br><br>
                                                        @if(count($item->article))
                                                            <p><strong>Related Articles</strong></p><br>
                                                            @foreach($item->article as $a)
                                                                <a href={{ route('www.articles_content',['id' => $a['id']]) }}>{{ $a['title'] }}</a>
                                                                <br><br>
                                                            @endforeach
                                                        @endif
                                                        @if(count($item->product_family))
                                                            <p><strong>Related Products</strong></p><br>
                                                        @endif
                                                        @if(count($item->clinical_trial))
                                                            <p><strong>Related Clinical Trials</strong></p><br>
                                                        @endif
                                                        @if(count($item->product_ingredient))
                                                            <p><strong>Related Ingredients</strong></p><br>
                                                        @endif
                                                        @if(count($item->product_group))
                                                            <p><strong>Related Product Groups</strong></p><br>
                                                        @endif
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endforeach
                            </div>
                        </div>

                        <div id="sync-sortcodes-2" class="owl-carousel">
                            <div class="item" onclick="setPub()">
                                <h3 style="font-size: smaller">PUBLICATION</h3>
                            </div>
                            <div class="item" onclick="setBook()">
                                <h3 style="font-size: smaller">BOOK</h3>
                            </div>
                            <div class="item" onclick="setConf()">
                                <h3 style="font-size: smaller">CONFERENCE</h3>
                            </div>

                        </div>

                    </div>
                </div>
            </div>

        </section>


        <!-- MORE PAGES SECTION RESEARCH
                    ================================================== -->
        <section class="section white-section section-padding-top-bottom">
            <div class="container">
                <div class="sixteen columns">
                    <h2>More</h2>
                </div>
                <div class="container">

                    <div class="clear"></div>
                    @foreach($research_list as $item)
                        <div class="four columns">
                            <a href="{{ $item->page_url }}">
                                <div class="portfolio-box-2 grey-section">
                                    @if(json_decode($item->slider))
                                        @foreach(json_decode($item->slider) as $slider)
                                            @if($slider->layout=='image')
                                                <img
                                                    src="{{ $image_url }}/www/Images/{{ \App\Models\MediaLibrary::where('id',$slider->attributes->slider_image)->first()->name }}"
                                                    style="display: inline-block;margin-top: unset; width: 100%;"/>
                                            @endif
                                        @endforeach
                                    @endif
                                    <div>
                                        <p class="two-row-max"
                                           style="height: 50px; text-align: center">{{ $item->title }}</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>

        @include('www.layouts.footer')

    </main>

    <div class="scroll-to-top">&#xf106;</div>





    <!-- JAVASCRIPT
    ================================================== -->
    <script type="text/javascript" src="{{ asset('/js/jquery-2.1.1.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/modernizr.custom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.mobile.custom.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/retina-1.1.0.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.animsition.min.js') }}"></script>
    <script type="text/javascript">
        (function ($) {
            "use strict";

            jQuery(document).ready(function ($) {
                $(".accordion").smk_Accordion({
                    closeAble: true, //boolean
                });
            });


            $(document).ready(function () {
                @if (session('login_error'))
                open_login();
                @endif
                $(".animsition").animsition({

                    inClass: 'zoom-in-sm',
                    outClass: 'zoom-out-sm',
                    inDuration: 1500,
                    outDuration: 800,
                    linkElement: '.animsition-link',
                    // e.g. linkElement   :   'a:not([target="_blank"]):not([href^=#])'
                    loading: true,
                    loadingParentElement: 'body', //animsition wrapper element
                    loadingClass: 'animsition-loading',
                    unSupportCss: ['animation-duration',
                        '-webkit-animation-duration',
                        '-o-animation-duration'
                    ],
                    //"unSupportCss" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
                    //The default setting is to disable the "animsition" in a browser that does not support "animation-duration".

                    overlay: false,

                    overlayClass: 'animsition-overlay-slide',
                    overlayParentElement: 'body'
                });
            });
        })(jQuery);
    </script>
    <script type="text/javascript" src="{{ asset('/js/jquery.themepunch.tools.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.themepunch.revolution.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.easing.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.hidescroll.min.js') }}"></script>
    <script type="text/javascript">
        $('.header-top').hidescroll();
    </script>
    <script type="text/javascript" src="{{ asset('/js/smoothScroll.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/isotope.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.counterup.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/waypoints.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/scrollReveal.js') }}"></script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            window.scrollReveal = new scrollReveal();
        })(jQuery);
    </script>
    <script type="text/javascript" src="{{ asset('/js/jquery.colorbox.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/owl.carousel.min.js') }}"></script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            jQuery(document).ready(function () {
                var offset = 450;
                var duration = 500;
                jQuery(window).scroll(function () {
                    if (jQuery(this).scrollTop() > offset) {
                        jQuery('.scroll-to-top').fadeIn(duration);
                    } else {
                        jQuery('.scroll-to-top').fadeOut(duration);
                    }
                });

                jQuery('.scroll-to-top').click(function (event) {
                    event.preventDefault();
                    jQuery('html, body').animate({scrollTop: 0}, duration);
                    return false;
                })
            });
        })(jQuery);
    </script>
    <script>
        //Tabs 1

        $(document).ready(function () {

            var sync1 = $("#sync-sortcodes-1");
            var sync2 = $("#sync-sortcodes-2");

            sync1.owlCarousel({
                singleItem: true,
                transitionStyle: "backSlide",
                slideSpeed: 1500,
                navigation: false,
                pagination: false,
                afterAction: syncPosition,
                responsiveRefreshRate: 200
            });


            sync2.owlCarousel({
                items: 3,
                itemsDesktop: [1199, 3],
                itemsDesktopSmall: [979, 3],
                itemsTablet: [768, 3],
                itemsMobile: [479, 3],
                pagination: false,
                responsiveRefreshRate: 100,
                afterInit: function (el) {
                    el.find(".owl-item").eq(0).addClass("synced");
                }
            });

            function syncPosition(el) {
                var current = this.currentItem;
                $("#sync-sortcodes-2")
                    .find(".owl-item")
                    .removeClass("synced")
                    .eq(current)
                    .addClass("synced")
                if ($("#sync-sortcodes-2").data("owlCarousel") !== undefined) {
                    center(current)
                }
            }

            $("#sync-sortcodes-2").on("click", ".owl-item", function (e) {
                e.preventDefault();
                var number = $(this).data("owlItem");
                sync1.trigger("owl.goTo", number);
            });

            function center(number) {
                var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
                var num = number;
                var found = false;
                for (var i in sync2visible) {
                    if (num === sync2visible[i]) {
                        var found = true;
                    }
                }

                if (found === false) {
                    if (num > sync2visible[sync2visible.length - 1]) {
                        sync2.trigger("owl.goTo", num - sync2visible.length + 2)
                    } else {
                        if (num - 1 === -1) {
                            num = 0;
                        }
                        sync2.trigger("owl.goTo", num);
                    }
                } else if (num === sync2visible[sync2visible.length - 1]) {
                    sync2.trigger("owl.goTo", sync2visible[1])
                } else if (num === sync2visible[0]) {
                    sync2.trigger("owl.goTo", num - 1)
                }

            }
        });

        function setPub() {
            var height = document.getElementById('publications').offsetHeight + 65;
            document.getElementById('sync-sortcodes-1').style.minHeight = null;
            document.getElementById('sync-sortcodes-1').style.height = height + 'px';
        }

        function setBook() {
            var height = document.getElementById('books').offsetHeight + 65;
            document.getElementById('sync-sortcodes-1').style.minHeight = null;
            document.getElementById('sync-sortcodes-1').style.height = height + 'px';
        }

        function setConf() {
            var height = document.getElementById('conferences').offsetHeight + 65;
            document.getElementById('sync-sortcodes-1').style.minHeight = null;
            document.getElementById('sync-sortcodes-1').style.height = height + 'px';
        }
    </script>

    <script type="text/javascript" src="{{ asset('/js/styleswitcher.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/custom-shop-home-1.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/visible.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/smk-accordion.js') }}"></script>
    <!-- End Document
================================================== -->
@endsection
