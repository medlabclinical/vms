@extends('www.layouts.main')
@section('title')
    {{ $clinicaltrial->name }}
@endsection
@section('description')
    {{ substr($clinicaltrial->name.' - '.$clinicaltrial->progression,0,160) }}
@endsection
@section('breadcrumbs')
    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset;background-color:#7AA43F;">
            <ol class="breadcrumb medlab_breadcrumbs" style="margin-bottom: unset;background-color:#7AA43F;">
            </ol>
        </div>
    </div>

    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset">
            <ol class="breadcrumb medlab_breadcrumbs">
                <li><a class="medlab_breadcrumbs_link" href="/">@lang('trs.home')</a></li>
                <li><a class="medlab_breadcrumbs_link" href="/clinical_trials">@lang('trs.clinical trials')</a></li>
                <li class="active medlab_breadcrumbs_text">{{$clinicaltrial->name}}</li>
            </ol>
        </div>
    </div>
@endsection

@section('content')
    <main class="cd-main-content" id="main" style="margin-top:90px">


        <!-- TOP SECTION
        ================================================== -->
        <section class="section white-section section-padding-top-bottom">
            <div class="container">
                <div class="twelve columns">
                    <div class="section-title left">
                        <h1>{{ $clinicaltrial->name }}</h1>
                        <div class="subtitle left big">{{ $clinicaltrial->type }}</div>
                        <a href="{{ $clinicaltrial->anzctr_link }}">
                            <div class="subtitle left big">
                                ANZCTR: {{ $clinicaltrial->anzctr }}
                            </div>
                        </a>
                        @if(isset($clinicaltrial->date_started))
                            <div class="subtitle left">
                                Date Started: {{ substr($clinicaltrial->date_started,0,10) }}</div>
                        @endif
                        @if(count($clinicaltrial->member))
                            <div class="subtitle left">Members Associated With Trail:
                                @foreach($clinicaltrial->member as $people)
                                    {{ $people['name'] }}.
                                @endforeach
                            </div>
                        @endif
                            <div class="subtitle left">
                                Progress - {{ $clinicaltrial->progression }}
                            </div>
                    </div>
                    <hr>
                    <div class="twelve columns">
                        <div class="blog-big-wrapper white-section"
                             data-scroll-reveal="enter bottom move 200px over 1s after 0.3s" style="color:white;">
                            <img src="{{ $image_url }}/www/Images/{{ $clinicaltrial->image }}" alt="test">
                        </div>
                        <div class="grey-section" style="margin-top: 20px;">
                            <div class="articleClass padding">
                                <h3>Study Site: </h3>{!! $clinicaltrial->study_site !!}
                            </div>
                        </div>
                        <div class="grey-section" style="margin-top: 20px;">
                            <div class="articleClass padding">
                                <h3>Formulation: </h3>{!! $clinicaltrial->formulation !!}
                            </div>
                        </div>
                        <div class="grey-section" style="margin-top: 20px;">
                            <div class="articleClass padding">
                                <h3>About Trial: </h3>{!! $clinicaltrial->about !!}
                            </div>
                        </div>
                        @if(isset($clinicaltrial->date_hrec))
                        <div class="grey-section" style="margin-top: 20px;">
                            <div class="articleClass padding">
                                <h3>HREC Date:</h3>{{ substr($clinicaltrial->date_hrec,0,10) }}
                            </div>
                        </div>
                        @endif
                        @if(isset($clinicaltrial->hrec_name))
                            <div class="grey-section" style="margin-top: 20px;">
                                <div class="articleClass padding">
                                    <h3>HREC Date:</h3>{{ substr($clinicaltrial->date_hrec,0,10) }}

                                </div>
                            </div>
                        @endif
                        @if(isset($clinicaltrial->hrec_id))
                            <div class="grey-section" style="margin-top: 20px;">
                                <div class="articleClass padding">
                                    <h3>HREC ID:</h3>{{ $clinicaltrial->hrec_id }}
                                </div>
                            </div>
                        @endif
                        @if(isset($clinicaltrial->ctn))
                            <div class="grey-section" style="margin-top: 20px;">
                                <div class="articleClass padding">
                                    <h3>CTN:</h3>{{ $clinicaltrial->ctn }}
                                </div>
                            </div>
                        @endif
                    </div>

                </div>
                <div class="four columns" data-scroll-reveal="enter bottom move 200px over 1s after 0.3s">
                    <div class="post-sidebar">
                        <div class="post-sidebar">

                            <div></div>
                            <div class="skills-name">Progression - {{ $clinicaltrial->progression }}</div>
                            <div class="pro-bar-container pro-bar-margin">
                                @if($clinicaltrial->progression_percentage != null )
                                    <div class="pro-bar bar-{{$clinicaltrial->progression_percentage}}"
                                         data-pro-bar-percent="{{$clinicaltrial->progression_percentage}}"></div>
                                    <div class="text-in-bar"><span
                                            class="counter-skills">{{$clinicaltrial->progression_percentage}}</span>%
                                    </div>
                                    <div class="arrow-skills"></div>
                                @elseif($clinicaltrial->progression =='Indication - Not Recruiting')
                                    <div class="pro-bar bar-1" data-pro-bar-percent="1"></div>
                                    <div class="text-in-bar"><span class="counter-skills">1</span>%</div>
                                    <div class="arrow-skills"></div>
                                @elseif($clinicaltrial->progression =='Discovery - Recruiting')
                                    <div class="pro-bar bar-2" data-pro-bar-percent="2"></div>
                                    <div class="text-in-bar"><span class="counter-skills">2</span>%</div>
                                    <div class="arrow-skills"></div>
                                @elseif($clinicaltrial->progression =='Discovery - Recruiting Closed')
                                    <div class="pro-bar bar-5" data-pro-bar-percent="5"></div>
                                    <div class="text-in-bar"><span class="counter-skills">5</span>%</div>
                                    <div class="arrow-skills"></div>
                                @elseif($clinicaltrial->progression =='Discovery - Completed')
                                    <div class="pro-bar bar-10" data-pro-bar-percent="10"></div>
                                    <div class="text-in-bar"><span class="counter-skills">10</span>%</div>
                                    <div class="arrow-skills"></div>
                                @elseif($clinicaltrial->progression =='Pre-Clinical - Not Recruiting')
                                    <div class="pro-bar bar-11" data-pro-bar-percent="11"></div>
                                    <div class="text-in-bar"><span class="counter-skills">11</span>%</div>
                                    <div class="arrow-skills"></div>
                                @elseif($clinicaltrial->progression =='Pre-Clinical - Recruiting')
                                    <div class="pro-bar bar-12" data-pro-bar-percent="12"></div>
                                    <div class="text-in-bar"><span class="counter-skills">12</span>%</div>
                                    <div class="arrow-skills"></div>
                                @elseif($clinicaltrial->progression =='Pre-Clinical - Recruiting Closed')
                                    <div class="pro-bar bar-15" data-pro-bar-percent="15"></div>
                                    <div class="text-in-bar"><span class="counter-skills">15</span>%</div>
                                    <div class="arrow-skills"></div>
                                @elseif($clinicaltrial->progression =='Pre-Clinical - Completed')
                                    <div class="pro-bar bar-20" data-pro-bar-percent="20"></div>
                                    <div class="text-in-bar"><span class="counter-skills">20</span>%</div>
                                    <div class="arrow-skills"></div>
                                @elseif($clinicaltrial->progression =='FIH Safety & Efficacy - Not Recruiting')
                                    <div class="pro-bar bar-21" data-pro-bar-percent="21"></div>
                                    <div class="text-in-bar"><span class="counter-skills">21</span>%</div>
                                    <div class="arrow-skills"></div>
                                @elseif($clinicaltrial->progression =='FIH Safety & Efficacy - Recruiting')
                                    <div class="pro-bar bar-22" data-pro-bar-percent="22"></div>
                                    <div class="text-in-bar"><span class="counter-skills">22</span>%</div>
                                    <div class="arrow-skills"></div>
                                @elseif($clinicaltrial->progression =='FIH Safety & Efficacy - Recruiting Closed')
                                    <div class="pro-bar bar-25" data-pro-bar-percent="25"></div>
                                    <div class="text-in-bar"><span class="counter-skills">25</span>%</div>
                                    <div class="arrow-skills"></div>
                                @elseif($clinicaltrial->progression =='FIH Safety & Efficacy - Completed')
                                    <div class="pro-bar bar-30" data-pro-bar-percent="30"></div>
                                    <div class="text-in-bar"><span class="counter-skills">30</span>%</div>
                                    <div class="arrow-skills"></div>
                                @elseif($clinicaltrial->progression =='Late Stage-Large Population Trials. Safety & Efficacy - Not Recruiting')
                                    <div class="pro-bar bar-50" data-pro-bar-percent="50"></div>
                                    <div class="text-in-bar"><span class="counter-skills">50</span>%</div>
                                    <div class="arrow-skills"></div>
                                @elseif($clinicaltrial->progression =='Late Stage-Large Population Trials. Safety & Efficacy - Recruiting')
                                    <div class="pro-bar bar-55" data-pro-bar-percent="55"></div>
                                    <div class="text-in-bar"><span class="counter-skills">55</span>%</div>
                                    <div class="arrow-skills"></div>
                                @elseif($clinicaltrial->progression =='Late Stage-Large Population Trials. Safety & Efficacy - Recruiting Closed')
                                    <div class="pro-bar bar-65" data-pro-bar-percent="65"></div>
                                    <div class="text-in-bar"><span class="counter-skills">65</span>%</div>
                                    <div class="arrow-skills"></div>
                                @elseif($clinicaltrial->progression =='Late Stage-Large Population Trials. Safety & Efficacy - Completed')
                                    <div class="pro-bar bar-75" data-pro-bar-percent="75"></div>
                                    <div class="text-in-bar"><span class="counter-skills">75</span>%</div>
                                    <div class="arrow-skills"></div>
                                @elseif($clinicaltrial->progression =='Regulatory Submission - Submitted')
                                    <div class="pro-bar bar-90" data-pro-bar-percent="90"></div>
                                    <div class="text-in-bar"><span class="counter-skills">90</span>%</div>
                                    <div class="arrow-skills"></div>
                                @elseif($clinicaltrial->progression =='Regulatory Submission - Approved')
                                    <div class="pro-bar bar-100" data-pro-bar-percent="100"></div>
                                    <div class="text-in-bar"><span class="counter-skills">100</span>%</div>
                                    <div class="arrow-skills"></div>
                                @else
                                    <div class="pro-bar bar-0" data-pro-bar-percent="0"></div>
                                    <div class="text-in-bar"><span class="counter-skills">0</span>%</div>
                                    <div class="arrow-skills"></div>
                                @endif
                            </div>
                            @if($clinicaltrial->open_for_partnering==1)
                                <div class="button-shortcodes text-size-1 text-padding-1 version-1"
                                     onClick="location.href = '/partner_opportunities';"
                                     style="float:unset;width: 100%;text-align: center;">
                                    <span>&#xf18e;</span>Open to partnering
                                </div>
                                <br><br>
                            @endif
                            @if(isset($clinicaltrial->anzctr_link))
                            <div id="button-con" style="float:unset;width: 100%;" class="button-shortcodes text-size-1 text-padding-1 version-1" onClick="location.href = '{{ $clinicaltrial->anzctr_link }}';">
                                    <span>&#xf18e;</span>Click Here For ANZCTR Page
                            </div>
                            @endif
                            @if(count($clinicaltrial->article))
                                <div class="separator-sidebar"></div>
                                <h3>Related Articles</h3>
                                <ul class="link-recents">
                                    @foreach($clinicaltrial->article as $a)
                                        <li>
                                            <a href={{ route('www.articles_content',['id' => preg_replace('/--+/','-',preg_replace('/[^A-Za-z0-9\-]/', '-', $a['title'])).'-'.$a['id']]) }}>{{$a['title']}}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            @endif
                            @if(count($clinicaltrial->product_family))
                                <div class="separator-sidebar"></div>
                                <h3>Related Products</h3>
                                <ul class="link-recents">
                                    @foreach($clinicaltrial->brand as $brands)
                                        @if($brands['id'] == 1)
                                            @foreach($clinicaltrial->product_family as $a)
                                                @if($a['in_development'] == 1)
                                                    <li><a href="{{ route('products_in_development',['id' => preg_replace('/--+/','-',preg_replace('/[^A-Za-z0-9\-]/', '-', $a['product_name'])).'-'.$a['id']]) }}">{{ $a['product_name'] }}</a>
                                                    </li>
                                                @else
                                                    <li><a href="{{ route('www.product_page',['id' => preg_replace('/--+/','-',preg_replace('/[^A-Za-z0-9\-]/', '-', $a['product_name'])).'-'.$a['id']]) }}">{{ $a['product_name'] }}</a>
                                                    </li>
                                                @endif
                                            @endforeach
                                        @else
                                            @foreach($clinicaltrial->product_family as $a)
                                                <li><a href="#">{{ $a['product_name'] }}</a></li>
                                            @endforeach
                                        @endif

                                    @endforeach

                                </ul>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <!-- EXTRAS SECTION
        ================================================== -->
        <section class="section white-section section-home-padding-top">
            <div class="container">

                <!-- PARTNER SECTION
                ================================================== -->

                @if(count($clinicaltrial->publicationsandpresentation))
                    <div class="sixteen columns">
                        <div class="section-title">
                            <h2 style="text-align: center">PUBLICATIONS OR CONFERENCES ASSOCIATED WITH THE PROGRAM</h2>
                        </div>
                    </div>
                    @foreach($clinicaltrial->publicationsandpresentation as $a)
                        <div class="sixteen columns">
                            <div class="accordion">
                                <div class="accordion_in"
                                     data-scroll-reveal="enter left move 200px over 1s after 0.3s">
                                    <div class="acc_head white-section"
                                         style="text-transform: unset;">{{ $a['title'] }}</div>
                                    <div class="acc_content white-section">
                                        @if($a['publication_location']!='' && $a['publication_location']!='Null')
                                            <p>{{ $a['publication_location'] }}</p>
                                        @endif
                                        @if($a['date']!='' && $a['date']!='Null')
                                            <p>
                                                <strong>Date:</strong>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp{{ substr($a['date'],0,10) }}
                                            </p>
                                        @endif
                                        @if($a['authors']!='' && $a['authors']!='Null')
                                            <p><strong>Authors:</strong>&nbsp&nbsp&nbsp{{$a['authors']}}</p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <hr>
                @endif

            <!-- PARTNER SECTION
            ================================================== -->

                @if(count($clinicaltrial->partner))
                    <div class="sixteen columns">
                        <div class="section-title">
                            <h2 style="text-align: center">PARTNER'S ASSOCIATED WITH THE PROGRAM</h2>
                        </div>
                    </div>
                    @foreach($clinicaltrial->partner as $partners)
                        <section class="section white-section section-padding-top-bottom">
                            <div class="sixteen columns">
                                <div class="blockquotes-box-1 grey-section blockquotes-float-content"
                                     style="max-width: 300px;padding: 10px;border: unset;">
                                    <img src="{{ $image_url }}/www/Images/{{ $partners['picture'] }}"
                                         alt="{{ $partners['alt'] }}">
                                </div>
                                <h4 style="margin: 20px;font-size: x-large;text-align: unset;margin-left: unset;">{{ $partners['name'] }}</h4>
                                {!! $partners['bio'] !!}
                            </div>

                        </section>
                    @endforeach
                    <hr>
                @endif

            <!-- AUTHER SECTION
            ================================================== -->

                @if(count($clinicaltrial->member))
                    <div class="twelve columns">
                        <div class="section-title">
                            <h2 style="text-align: center">MEMBER'S ASSOCIATED WITH THE PROGRAM</h2>
                        </div>
                    </div>
                    @foreach($clinicaltrial->member as $people)
                        <section class="section white-section section-padding-top-bottom">
                            <div class="sixteen columns">
                                <div class="blockquotes-box-1 grey-section blockquotes-float-content"
                                     style="max-width: 300px;padding: 10px;border: unset;">
                                    <img src="{{ $image_url }}/www/Images/{{ $people['picture'] }}"
                                         alt="{{ $people['alt'] }}">
                                </div>
                                <div class="team-name-top" style="inline-block;">{{ $people['position'] }}</div>
                                <h4 style="margin: 20px;font-size: x-large;text-align: unset;margin-left: unset;">{{ $people['name'] }}</h4>
                                {!! $people['bio'] !!}
                            </div>

                        </section>
                    @endforeach
                    <hr>
                @endif
            </div>
        </section>


        <!-- MORE PAGES SECTION RESEARCH
                    ================================================== -->
        <section class="section white-section section-padding-top-bottom">
            <div class="container">
                <div class="sixteen columns">
                    <h2>More</h2>
                </div>
                <div class="container">

                    <div class="clear"></div>
                    @foreach($research_list as $item)
                        <div class="four columns">
                            <a href="{{ $item->page_url }}">
                                <div class="portfolio-box-2 grey-section">
                                    @if(json_decode($item->slider))
                                        @foreach(json_decode($item->slider) as $slider)
                                            @if($slider->layout=='image')
                                                <img
                                                    src="{{ $image_url }}/www/Images/{{ \App\Models\MediaLibrary::where('id',$slider->attributes->slider_image)->first()->name }}"
                                                    style="display: inline-block;margin-top: unset; width: 100%;"/>
                                            @endif
                                        @endforeach
                                    @endif
                                    <div>
                                        <p class="two-row-max"
                                           style="height: 50px; text-align: center">{{ $item->title }}</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>

        @include('www.layouts.footer')
    </main>

    <div class="scroll-to-top">&#xf106;</div>





    <!-- JAVASCRIPT
    ================================================== -->
    <script type="text/javascript" src="{{ asset('/js/jquery-2.1.1.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/modernizr.custom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.mobile.custom.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/retina-1.1.0.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.animsition.min.js') }}"></script>
    <script type="text/javascript">
        (function ($) {
            "use strict";

            jQuery(document).ready(function ($) {
                $(".accordion").smk_Accordion({
                    closeAble: true, //boolean
                });
            });

            $(document).ready(function () {
                @if (session('login_error'))
                open_login();
                @endif
                $(".animsition").animsition({

                    inClass: 'zoom-in-sm',
                    outClass: 'zoom-out-sm',
                    inDuration: 1500,
                    outDuration: 800,
                    linkElement: '.animsition-link',
                    // e.g. linkElement   :   'a:not([target="_blank"]):not([href^=#])'
                    loading: true,
                    loadingParentElement: 'body', //animsition wrapper element
                    loadingClass: 'animsition-loading',
                    unSupportCss: ['animation-duration',
                        '-webkit-animation-duration',
                        '-o-animation-duration'
                    ],
                    //"unSupportCss" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
                    //The default setting is to disable the "animsition" in a browser that does not support "animation-duration".

                    overlay: false,

                    overlayClass: 'animsition-overlay-slide',
                    overlayParentElement: 'body'
                });
            });
        })(jQuery);
    </script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            jQuery(document).ready(function () {
                var offset = 450;
                var duration = 500;
                jQuery(window).scroll(function () {
                    if (jQuery(this).scrollTop() > offset) {
                        jQuery('.scroll-to-top').fadeIn(duration);
                    } else {
                        jQuery('.scroll-to-top').fadeOut(duration);
                    }
                });

                jQuery('.scroll-to-top').click(function (event) {
                    event.preventDefault();
                    jQuery('html, body').animate({scrollTop: 0}, duration);
                    return false;
                })
            });
        })(jQuery);
    </script>
    <script type="text/javascript" src="{{ asset('/js/smk-accordion.js') }}"></script>


    <script type="text/javascript" src="{{ asset('/js/jquery.easing.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.hidescroll.min.js') }}"></script>
    <script type="text/javascript">
        $('.header-top').hidescroll();
    </script>
    <script type="text/javascript" src="{{ asset('/js/jquery.parallax-1.1.3.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/owl.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/masonry.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/isotope.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/scrollReveal.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/styleswitcher.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.themepunch.tools.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.themepunch.revolution.min.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('/DataTables/datatables.min.css') }}"/>

    <script type="text/javascript" src="{{ asset('/DataTables/datatables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/custom-corporate-home-1.js') }}"></script>





    <!-- End Document
    ================================================== -->
@endsection

