@extends('www.layouts.main')
@section('title')
    {{ $content->title }}
@endsection
@section('description')
    @if($content->meta_description)
        {{ $content->meta_description }}
    @elseif(json_decode($content->content))
        @foreach(json_decode($content->content) as $item)
            @if($item->layout=='text')
                {{ substr(str_replace('&nbsp;',' ',strip_tags($item->attributes->content)),0,160) }}
            @endif
        @endforeach
    @endif
@endsection
@section('breadcrumbs')
    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset;background-color:#7AA43F;">
            <ol class="breadcrumb medlab_breadcrumbs" style="margin-bottom: unset;background-color:#7AA43F;">
            </ol>
        </div>
    </div>

    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset">
            <ol class="breadcrumb medlab_breadcrumbs">
                <li><a class="medlab_breadcrumbs_link" href="/">@lang('trs.home')</a></li>
                <li class="active medlab_breadcrumbs_text">Leadership Team</li>
            </ol>
        </div>
    </div>
@endsection



@section('content')

    <main class="cd-main-content" id="main" style="margin-top:90px">

        <!-- SLIDER IMAGE
================================================== -->

        @if(json_decode($content->slider))
            <section class="home">
                <div class="slider-container">
                    <div class="tp-banner-container">
                        <div class="tp-banner">
                            <ul>
                                @foreach(json_decode($content->slider) as $slider)
                                    @if($slider->layout=='video')
                                        <li data-transition="fade" data-slotamount="1" data-masterspeed="500"
                                            data-saveperformance="on" data-title="Intro Slide">
                                            <iframe
                                                src="https://player.vimeo.com/video/{{ $slider->attributes->video_link }}"
                                                width="100%" height="100%" align="center" frameborder="0"
                                                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                                allowfullscreen></iframe>
                                        </li>
                                    @elseif($slider->layout=='image')
                                        <li data-transition="fade" data-slotamount="1" data-masterspeed="500"
                                            data-saveperformance="on" data-title="Intro Slide">
                                            <img
                                                src="{{ $image_url }}/www/Images/{{ \App\Models\MediaLibrary::where('id',$slider->attributes->slider_image)->first()->name }}"
                                                alt="{{ $slider->attributes->slider_alt }}"
                                                data-lazyload="{{ $image_url }}/www/Images/{{ \App\Models\MediaLibrary::where('id',$slider->attributes->slider_image)->first()->name }}"
                                                data-bgposition="center top" data-bgfit="cover"
                                                data-bgrepeat="no-repeat">
                                            <a href="#">
                                                <div class="black-heavy-3">
                                                    <div class="black-heavy-3-heading">
                                                        <h1>{{ $slider->attributes->title }}</h1></div>
                                                    <div
                                                        class="black-heavy-3-subheading">{{ $slider->attributes->subtitle }}</div>
                                                </div>
                                            </a>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
        @else
            <br>
        @endif


        <!-- SECTION
        ================================================== -->


        <section class="section grey-section section-padding-top-bottom">
            <div class="container">
                <div id="portfolio-filter">
                    <ul id="filter">
                        <li><a href="#" class="current" data-filter="*" title="">Show All</a></li>
                        @foreach($typeFilter as $filter)
                            <li><a href="#" data-filter=".{{ $filter->id }}" title="">{{ $filter->name }}</a></li>
                        @endforeach
                    </ul>
                </div>

                <div class="clear"></div>
                <div id="shop-grid">
                    @foreach($leadershipTeamList as $item)
                        @if(isset( $item->member_type()->first()->name))
                            <div class="one-third column
                            @if(count($item->member_type))
                            @foreach($item->member_type as $filter)
                            {{ $filter->id }}
                            @endforeach
                            @endif
                                ">
                                <div class="blog-box-1 grey-section" id="{{ $item->id }}"
                                     data-scroll-reveal="enter left move 200px over 1s after 0.3s"
                                     onclick="showDetail({{ $item }},{{ $item->image()->first() }})"
                                     style="cursor: pointer">
                                    @if(isset( $item->image()->first()->name))
                                        <img src="{{ $image_url }}/www/Images/{{ $item->image()->first()->name }}"
                                             alt="Test">
                                    @endif
                                    <div class="one-row-max"
                                         style="padding: 5px;height: 30px;font-size:20px">{{ $item->name }}</div>
                                    <h6 class="two-row-max"
                                        style="padding: 5px;min-height: 50px;">{{ $item->position }}</h6>
                                    <div class="four-row-max"
                                         style="padding: 5px;max-height: 120px; text-align: left">
                                        <p>{!! $item->bio !!}</p></div>
                                    <div class="link">&#xf178;</div>
                                </div>
                            </div>

                        @endif
                    @endforeach
                </div>

            </div>
        </section>

        <!-- MORE PAGES SECTION ABOUT US
                ================================================== -->
        <section class="section white-section section-padding-top-bottom">
            <div class="container">
                <div class="sixteen columns">
                    <h2>More</h2>
                </div>
                <div class="container">

                <div class="clear"></div>
                        @foreach($about_list as $item)
                            <div class="four columns">
                                <a href="{{ $item->page_url }}">
                                    <div class="portfolio-box-2 grey-section">
                                        @if(json_decode($item->slider))
                                            @foreach(json_decode($item->slider) as $slider)
                                                @if($slider->layout=='image')
                                                    <img
                                                        src="{{ $image_url }}/www/Images/{{ \App\Models\MediaLibrary::where('id',$slider->attributes->slider_image)->first()->name }}"
                                                        style="display: inline-block;margin-top: unset; width: 100%;"/>
                                                @endif
                                            @endforeach
                                        @endif
                                        <div>
                                            <p class="two-row-max"
                                               style="height: 30px; text-align: center">{{ $item->title }}</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                </div>
            </div>
        </section>

        @include('www.layouts.footer')

    </main>

    <div class="scroll-to-top">&#xf106;</div>





    <!-- JAVASCRIPT
   ================================================== -->
    <script type="text/javascript" src="{{ asset('/js/jquery-2.1.1.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/modernizr.custom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.mobile.custom.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/retina-1.1.0.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.animsition.min.js') }}"></script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            $(document).ready(function () {
                @if (session('login_error'))
                open_login();
                @endif
                $(".animsition").animsition({

                    inClass: 'zoom-in-sm',
                    outClass: 'zoom-out-sm',
                    inDuration: 1500,
                    outDuration: 800,
                    linkElement: '.animsition-link',
                    // e.g. linkElement   :   'a:not([target="_blank"]):not([href^=#])'
                    loading: true,
                    loadingParentElement: 'body', //animsition wrapper element
                    loadingClass: 'animsition-loading',
                    unSupportCss: ['animation-duration',
                        '-webkit-animation-duration',
                        '-o-animation-duration'
                    ],
                    //"unSupportCss" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
                    //The default setting is to disable the "animsition" in a browser that does not support "animation-duration".

                    overlay: false,

                    overlayClass: 'animsition-overlay-slide',
                    overlayParentElement: 'body'
                });
            });
        })(jQuery);

        function showDetail(item, image) {
            Swal.fire({
                title: item['name'],
                text: item['position'],
                html: '<p style="font-family: \'Playball\',crusive;">' + item['position'] + '</p>' + '<br>' + '<div style="text-align: left">' + item['bio'] + '</div>',
                width: '90%',
                imageUrl: '{{ $image_url }}/www/Images/' + image['name'],
                imageWidth: 350,
                imageHeight: 'auto',
                // showCloseButton: true,
                imageAlt: 'Custom image',
                showCancelButton: true,
                cancelButtonText: 'Close',
                confirmButtonColor: '#7AA43F',
                cancelButtonColor: '#7AA43F',
                confirmButtonText: 'More',
                reverseButtons: true,
            }).then((result) => {
                if (result.value) {
                    let url = '{{ route('homepage') }}' + '/leadership_team/' + item['name'].replace('/[^A-Za-z0-9\-]/', '-').replace('/--+/', '-') + '-' + item['id']
                    document.location.href = url;
                }
            })
        }
    </script>


    <script type="text/javascript" src="{{ asset('/js/jquery.easing.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.hidescroll.min.js') }}"></script>
    <script type="text/javascript">
        $('.header-top').hidescroll();
    </script>
    <script type="text/javascript" src="{{ asset('/js/jquery.parallax-1.1.3.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/owl.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/masonry.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/isotope.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/scrollReveal.js') }}"></script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            window.scrollReveal = new scrollReveal();
        })(jQuery);
    </script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            jQuery(document).ready(function () {
                var offset = 450;
                var duration = 500;
                jQuery(window).scroll(function () {
                    if (jQuery(this).scrollTop() > offset) {
                        jQuery('.scroll-to-top').fadeIn(duration);
                    } else {
                        jQuery('.scroll-to-top').fadeOut(duration);
                    }
                });

                jQuery('.scroll-to-top').click(function (event) {
                    event.preventDefault();
                    jQuery('html, body').animate({scrollTop: 0}, duration);
                    return false;
                })
            });
        })(jQuery);
    </script>

    <script type="text/javascript" src="{{ asset('/js/styleswitcher.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/smk-accordion.js') }}"></script>

    <script type="text/javascript" src="{{ asset('/js/jquery.themepunch.tools.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.themepunch.revolution.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/visible.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.counterup.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/waypoints.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/custom-portfolio-home-1.js') }}"></script>

    <script type="text/javascript" src="{{ asset('/js/custom-shop-home-2.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('/DataTables/datatables.min.css') }}"/>

    <script type="text/javascript" src="{{ asset('/DataTables/datatables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/custom-team.js') }}"></script>



    <!-- End Document
================================================== -->

@endsection
