@extends('www.layouts.main')
@section('title')
    {{ $leadershipTeam->name }}
@endsection
@section('description')
    {{ substr(str_replace('&nbsp;',' ',strip_tags($leadershipTeam->bio)),0,160) }}
@endsection
@section('breadcrumbs')
    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset;background-color:#7AA43F;">
            <ol class="breadcrumb medlab_breadcrumbs" style="margin-bottom: unset;background-color:#7AA43F;">
            </ol>
        </div>
    </div>

    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset">
            <ol class="breadcrumb medlab_breadcrumbs">
                <li><a class="medlab_breadcrumbs_link" href="/">@lang('trs.home')</a></li>
                <li><a class="medlab_breadcrumbs_link" href="/leadership_team">Leadership Team</a></li>
                <li class="active medlab_breadcrumbs_text">{{$leadershipTeam->name}}</li>
            </ol>
        </div>
    </div>
@endsection

@section('content')
    <main class="cd-main-content" id="main" style="margin-top:90px">


        <!-- TOP SECTION
        ================================================== -->
        <section class="section white-section section-padding-top-bottom">
            <div class="container">
                <div class="twelve columns">
                    <div class="section-title left">
                        <h1>{{ $leadershipTeam->name }}</h1>
                        <div class="subtitle left big">{{ $leadershipTeam->position }}</div>
                    </div>
                    <hr>
                    <div class="twelve columns">
                        <div class="blog-big-wrapper white-section"
                             data-scroll-reveal="enter bottom move 200px over 1s after 0.3s" style="color:white;">
                            <img src="{{ $image_url }}/www/Images/{{ $leadershipTeam->picture }}" alt="test">
                        </div>
                        <div class="grey-section" style="margin-top: 20px;">
                            <div class="articleClass padding">
                                <h3>Bio: </h3>{!! $leadershipTeam->bio !!}
                            </div>
                        </div>
                    </div>


                    <!-- PUBLICATIONS SECTION
                    ================================================== -->

                    @if(count($leadershipTeam->publicationsandpresentation))
                        <div class="twelve columns">
                            <div class="section-title">
                                <h2 style="text-align: center">PUBLICATIONS OR CONFERENCES ASSOCIATED WITH THIS
                                    MEMBER</h2>
                            </div>
                        </div>
                        @foreach($leadershipTeam->publicationsandpresentation as $a)
                            <div class="twelve columns">
                                <div class="accordion">
                                    <div class="accordion_in"
                                         data-scroll-reveal="enter left move 200px over 1s after 0.3s">
                                        <div class="acc_head white-section"
                                             style="text-transform: unset;">{{ $a['title'] }}</div>
                                        <div class="acc_content white-section">
                                            @if($a['publication_location']!='' && $a['publication_location']!='Null')
                                                <p>{{ $a['publication_location'] }}</p>
                                            @endif
                                            @if($a['date']!='' && $a['date']!='Null')
                                                <p>
                                                    <strong>Date:</strong>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp{{ substr($a['date'],0,10) }}
                                                </p>
                                            @endif
                                            @if($a['authors']!='' && $a['authors']!='Null')
                                                <p><strong>Authors:</strong>&nbsp&nbsp&nbsp{{$a['authors']}}</p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        <hr>
                    @endif

                </div>

                <!-- ARTICLES SECTION
                    ================================================== -->

                <div class="four columns" data-scroll-reveal="enter bottom move 200px over 1s after 0.3s">
                    <div class="post-sidebar">
                        <div class="post-sidebar">
                            <div></div>
                            @if(count($leadershipTeam->article))
                                <div class="separator-sidebar"></div>
                                <h3>Related Articles</h3>
                                <ul class="link-recents">
                                    @foreach($leadershipTeam->article as $a)
                                        <li>
                                            <a href={{ route('www.articles_content',['id' => preg_replace('/--+/','-',preg_replace('/[^A-Za-z0-9\-]/', '-', $a['title'])).'-'.$a['id']]) }}>{{$a['title']}}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            @endif
                        </div>
                    </div>
                </div>

            </div>
        </section>


        <!-- MORE PAGES SECTION ABOUT US
            ================================================== -->
        <section class="section white-section section-padding-top-bottom">
            <div class="container">
                <div class="sixteen columns">
                    <h2>More</h2>
                </div>
                <div class="container">

                    <div class="clear"></div>
                    @foreach($about_list as $item)
                        <div class="four columns">
                            <a href="{{ $item->page_url }}">
                                <div class="portfolio-box-2 grey-section">
                                    @if(json_decode($item->slider))
                                        @foreach(json_decode($item->slider) as $slider)
                                            @if($slider->layout=='image')
                                                <img
                                                    src="{{ $image_url }}/www/Images/{{ \App\Models\MediaLibrary::where('id',$slider->attributes->slider_image)->first()->name }}"
                                                    style="display: inline-block;margin-top: unset; width: 100%;"/>
                                            @endif
                                        @endforeach
                                    @endif
                                    <div>
                                        <p class="two-row-max"
                                           style="height: 30px; text-align: center">{{ $item->title }}</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>

        @include('www.layouts.footer')
    </main>

    <div class="scroll-to-top">&#xf106;</div>





    <!-- JAVASCRIPT
    ================================================== -->
    <script type="text/javascript" src="{{ asset('/js/jquery-2.1.1.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/modernizr.custom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.mobile.custom.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/retina-1.1.0.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.animsition.min.js') }}"></script>
    <script type="text/javascript">
        (function ($) {
            "use strict";

            jQuery(document).ready(function ($) {
                $(".accordion").smk_Accordion({
                    closeAble: true, //boolean
                });
            });

            $(document).ready(function () {
                @if (session('login_error'))
                open_login();
                @endif
                $(".animsition").animsition({

                    inClass: 'zoom-in-sm',
                    outClass: 'zoom-out-sm',
                    inDuration: 1500,
                    outDuration: 800,
                    linkElement: '.animsition-link',
                    // e.g. linkElement   :   'a:not([target="_blank"]):not([href^=#])'
                    loading: true,
                    loadingParentElement: 'body', //animsition wrapper element
                    loadingClass: 'animsition-loading',
                    unSupportCss: ['animation-duration',
                        '-webkit-animation-duration',
                        '-o-animation-duration'
                    ],
                    //"unSupportCss" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
                    //The default setting is to disable the "animsition" in a browser that does not support "animation-duration".

                    overlay: false,

                    overlayClass: 'animsition-overlay-slide',
                    overlayParentElement: 'body'
                });
            });
        })(jQuery);
    </script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            jQuery(document).ready(function () {
                var offset = 450;
                var duration = 500;
                jQuery(window).scroll(function () {
                    if (jQuery(this).scrollTop() > offset) {
                        jQuery('.scroll-to-top').fadeIn(duration);
                    } else {
                        jQuery('.scroll-to-top').fadeOut(duration);
                    }
                });

                jQuery('.scroll-to-top').click(function (event) {
                    event.preventDefault();
                    jQuery('html, body').animate({scrollTop: 0}, duration);
                    return false;
                })
            });
        })(jQuery);
    </script>

    <script type="text/javascript" src="{{ asset('/js/smk-accordion.js') }}"></script>


    <script type="text/javascript" src="{{ asset('/js/jquery.easing.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.hidescroll.min.js') }}"></script>
    <script type="text/javascript">
        $('.header-top').hidescroll();
    </script>
    <script type="text/javascript" src="{{ asset('/js/jquery.parallax-1.1.3.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/owl.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/masonry.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/isotope.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/scrollReveal.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/styleswitcher.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.themepunch.tools.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.themepunch.revolution.min.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('/DataTables/datatables.min.css') }}"/>

    <script type="text/javascript" src="{{ asset('/DataTables/datatables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/custom-corporate-home-1.js') }}"></script>





    <!-- End Document
    ================================================== -->
@endsection

