﻿@extends('www.layouts.main')
@section('title')
    {{ $content->title }}
@endsection
@section('description')
    @if($content->meta_description)
        {{ $content->meta_description }}
    @elseif(json_decode($content->content))
        @foreach(json_decode($content->content) as $item)
            @if($item->layout=='text')
                {{ substr(str_replace('&nbsp;',' ',strip_tags($item->attributes->content)),0,160) }}
            @endif
        @endforeach
    @endif
@endsection
@section('breadcrumbs')
    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset;background-color:#7AA43F;">
            <ol class="breadcrumb medlab_breadcrumbs" style="margin-bottom: unset;background-color:#7AA43F;">
            </ol>
        </div>
    </div>

    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset">
            <ol class="breadcrumb medlab_breadcrumbs">
                <li><a class="medlab_breadcrumbs_link" href="/">@lang('trs.home')</a></li>
                <li class="active medlab_breadcrumbs_text">@lang('partner opportunities')</li>
            </ol>
        </div>
    </div>
@endsection




@section('content')


    <main class="cd-main-content" id="main" style="margin-top:90px">

        <!-- SLIDER IMAGE
================================================== -->

        @if(json_decode($content->slider))
            <section class="home">
                <div class="slider-container">
                    <div class="tp-banner-container">
                        <div class="tp-banner">
                            <ul>
                                @foreach(json_decode($content->slider) as $slider)
                                    @if($slider->layout=='video')
                                        <li data-transition="fade" data-slotamount="1" data-masterspeed="500"
                                            data-saveperformance="on" data-title="Intro Slide">
                                            <iframe
                                                src="https://player.vimeo.com/video/{{ $slider->attributes->video_link }}"
                                                width="100%" height="100%" align="center" frameborder="0"
                                                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                                allowfullscreen></iframe>
                                        </li>
                                    @elseif($slider->layout=='image')
                                        <li data-transition="fade" data-slotamount="1" data-masterspeed="500"
                                            data-saveperformance="on" data-title="Intro Slide">
                                            <img
                                                src="{{ $image_url }}/www/Images/{{ \App\Models\MediaLibrary::where('id',$slider->attributes->slider_image)->first()->name }}"
                                                alt="{{ $slider->attributes->slider_alt }}"
                                                data-lazyload="{{ $image_url }}/www/Images/{{ \App\Models\MediaLibrary::where('id',$slider->attributes->slider_image)->first()->name }}"
                                                data-bgposition="center top" data-bgfit="cover"
                                                data-bgrepeat="no-repeat">
                                            <a href="#">
                                                <div class="black-heavy-3">
                                                    <div class="black-heavy-3-heading">
                                                        <h1>{{ $slider->attributes->title }}</h1></div>
                                                    <div
                                                        class="black-heavy-3-subheading">{{ $slider->attributes->subtitle }}</div>
                                                </div>
                                            </a>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
        @else
            <br>
        @endif

    <!-- CONTENT SECTION
    ================================================== -->

        @if(json_decode($content->content))
            <section class="section white-section section-padding-top" id="scroll-link">
                <div class="container">
                    <div class="sixteen columns remove-bottom"
                    >
                        <div class="full-image">
                            <div class="articleClass padding">
                                @foreach(json_decode($content->content) as $item)
                                    @if($item->layout=='text')
                                        <div class="sixteen columns">
                                            {!! $item->attributes->content !!}
                                        </div>
                                    @elseif($item->layout=='image')
                                        <div class="sixteen columns">
                                            <img src="{{ $image_url }}/www/Images/{{ \App\Models\MediaLibrary::where('id',$item->attributes->image)->first()->name }}"
                                                 alt="{{ $item->attributes->image_alt }}" style = "width: {{ $item->attributes->width }}%; float: {{ $item->attributes->float }}">
                                        </div>
                                    @else
                                        <div class="sixteen columns">
                                            <iframe src="{{ $item->attributes->content }}?transparent=false"
                                                    width="100%" height="500"
                                                    frameborder=”0″ webkitallowfullscreen mozallowfullscreen
                                                    allowfullscreen></iframe>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        @endif

        <section class="section grey-section section-padding-top-bottom" id="scroll-link-6">

            <div class="container">
                <div class="sixteen columns">
                    <div class="section-title">
                        <h2>Partnering Request</h2>
                    </div>
                </div>
                <div class="alert alert-red  error" id="err-form">There was a problem validating the form please check!</div>
                <div class="alert alert-red  error" id="err-timedout">The connection to the server timed out!</div>
                <div class="alert alert-red error" id="err-state"></div>
                <div class="clear"></div>

                <form name="ajax-form" id="ajax-form" action="{{ url('/contact') }}" method="post">
                    {!! csrf_field() !!}
                    <div class="eight columns">
                        <label for="fname" class="uname required"> First Name
                            <span class="error" id="err-fname" style="color: red">PLEASE ENTER YOUR FIRST NAME</span>
                        </label>
                        <input name="fname" id="fname" type="text" placeholder="First Name" required/>
                    </div>
                    <div class="eight columns">
                        <label for="lname" class="uname required"> Last Name
                            <span class="error" id="err-lname" style="color: red">PLEASE ENTER YOUR LAST NAME</span>
                        </label>
                        <input name="lname" id="lname" type="text" placeholder="Last Name" required/>
                    </div>
                    <div class="eight columns">
                        <label for="email" class="uname required"> E-Mail Address
                            <span class="error" id="err-email" style="color: red">PLEASE ENTER YOUR E-MAIL</span>
                            <span class="error" id="err-emailvld" style="color: red">E-MAIL IS NOT IN A VALID FORMAT</span>
                        </label>
                        <input name="email" id="email" type="text" placeholder="E-Mail" required/>
                    </div>
                    <div class="eight columns">
                        <label for="phone" class="uname required">Phone Number
                            <span class="error" id="err-phone" style="color: red">PLEASE ENTER YOUR PHONE NUMBER</span>
                            <span class="error" id="err-phonevld" style="color: red">PHONE NUMBER IS NOT A VALID NUMBER</span>
                        </label>
                        <input name="phone" id="phone" type="text" placeholder="Phone Number" required/>
                    </div>
                    <div class="eight columns">
                        <label for="company" class="uname required"> Company
                            <span class="error" id="err-company" style="color: red">PLEASE ENTER YOUR COMPANY NAME</span>
                        </label>
                        <input name="company" id="company" type="text" placeholder="Company Name" required/>
                    </div>
                    <div class="eight columns">
                        <label for="country" class="uname required"> Country
                            <span class="error" id="err-country" style="color: red">PLEASE ENTER A COUNTRY</span>
                        </label>
                        <input name="country" id="country" type="text" placeholder="Country" required/>
                    </div>
                    <div class="sixteen columns">
                        <label for="message"> Message</label>
                        <textarea name="message" id="message" ></textarea>
                    </div>
                    <div class="sixteen columns">
                        <div id="button-con"><button class="send_message" id="send">submit</button></div>
                    </div>
                    <div class="clear"></div>

                </form>

                <div class="clear"></div>

                <div id="ajaxsuccess"></div>

            </div>

            <div class="clear"></div>

        </section>


        <!-- MORE PAGES SECTION ABOUT US
            ================================================== -->
        <section class="section white-section section-padding-top-bottom">
            <div class="container">
                <div class="sixteen columns">
                    <h2>More</h2>
                </div>
                <div class="container">

                    <div class="clear"></div>
                    @foreach($about_list as $item)
                        <div class="four columns">
                            <a href="{{ $item->page_url }}">
                                <div class="portfolio-box-2 grey-section">
                                    @if(json_decode($item->slider))
                                        @foreach(json_decode($item->slider) as $slider)
                                            @if($slider->layout=='image')
                                                <img
                                                    src="{{ $image_url }}/www/Images/{{ \App\Models\MediaLibrary::where('id',$slider->attributes->slider_image)->first()->name }}"
                                                    style="display: inline-block;margin-top: unset; width: 100%;"/>
                                            @endif
                                        @endforeach
                                    @endif
                                    <div>
                                        <p class="two-row-max"
                                           style="height: 30px; text-align: center">{{ $item->title }}</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>


        @include('www.layouts.footer')


    </main>

    <div class="scroll-to-top">&#xf106;</div>





    <!-- JAVASCRIPT
    ================================================== -->
    <script type="text/javascript" src="{{ asset('/js/jquery-2.1.1.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/modernizr.custom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.mobile.custom.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/retina-1.1.0.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.animsition.min.js') }}"></script>
    <script type="text/javascript">
        (function($) { "use strict";
            $(document).ready(function() {
                @if (session('login_error'))
                open_login();
                    @endif



                $(".animsition").animsition({

                    inClass               :   'zoom-in-sm',
                    outClass              :   'zoom-out-sm',
                    inDuration            :    1500,
                    outDuration           :    800,
                    linkElement           :   '.animsition-link',
                    // e.g. linkElement   :   'a:not([target="_blank"]):not([href^=#])'
                    loading               :    true,
                    loadingParentElement  :   'body', //animsition wrapper element
                    loadingClass          :   'animsition-loading',
                    unSupportCss          : [ 'animation-duration',
                        '-webkit-animation-duration',
                        '-o-animation-duration'
                    ],
                    //"unSupportCss" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
                    //The default setting is to disable the "animsition" in a browser that does not support "animation-duration".

                    overlay               :   false,

                    overlayClass          :   'animsition-overlay-slide',
                    overlayParentElement  :   'body'
                });
            });
        })(jQuery);
        jQuery(document).ready(function ($) { // wait until the document is ready
            $('#send').click(function(){ // when the button is clicked the code executes
                $('.error').fadeOut('slow'); // reset the error messages (hides them)

                var error = false; // we will set this true if the form isn't valid

                var fname = $('input#fname').val(); // get the value of the input field
                if(fname == "" || fname == " ") {
                    $('#err-fname').fadeIn('slow'); // show the error message
                    error = true; // change the error state to true
                }

                var lname = $('input#lname').val(); // get the value of the input field
                if(lname == "" || lname == " ") {
                    $('#err-lname').fadeIn('slow'); // show the error message
                    error = true; // change the error state to true
                }

                var email_compare = /^([a-z0-9_.-]+)@([da-z.-]+).([a-z.]{2,6})$/; // Syntax to compare against input
                var email = $('input#email').val(); // get the value of the input field
                if (email == "" || email == " ") { // check if the field is empty
                    $('#err-email').fadeIn('slow'); // error - empty
                    error = true;
                }else if (!email_compare.test(email)) { // if it's not empty check the format against our email_compare variable
                    $('#err-emailvld').fadeIn('slow'); // error - not right format
                    error = true;
                }

                var phone_compare = /^0[0-8]\d{8}$/; // Syntax to compare against input
                var phone = $('input#phone').val(); // get the value of the input field
                if (phone == "" || phone == " ") { // check if the field is empty
                    $('#err-phone').fadeIn('slow'); // error - empty
                    error = true;
                }else if (!phone_compare.test(phone)) { // if it's not empty check the format against our email_compare variable
                    $('#err-phonevld').fadeIn('slow'); // error - not right format
                    error = true;
                }

                var company = $('input#company').val(); // get the value of the input field
                if(company == "" || company == " ") {
                    $('#err-company').fadeIn('slow'); // show the error message
                    error = true; // change the error state to true
                }

                var country = $('input#country').val(); // get the value of the input field
                if(country == "" || country == " ") {
                    $('#err-country').fadeIn('slow'); // show the error message
                    error = true; // change the error state to true
                }

                if(error == true) {
                    $('#err-form').slideDown('slow');
                    return false;
                }


                var data_string = $('#ajax-form').serialize(); // Collect data from form

                console.log(data_string);
                $.ajax({
                    type: "POST",
                    url: "/partner_opportunities",
                    data: data_string,
                    // timeout: 6000,
                    error: function(data) {
                        console.log(data);
                        if (error == "timeout") {
                            $('#err-timedout').slideDown('slow');
                        }
                        else {
                            $('#err-state').slideDown('slow');
                            $("#err-state").html('An error occurred: ' + data['responseJSON'] + '');
                        }
                    },
                    success: function(data) {
                        console.log(data);
                        $('#ajax-form').slideUp('slow');
                        document.getElementById('ajaxsuccess').innerHTML=data['success'];
                        $('#ajaxsuccess').slideDown('slow');
                    }
                });

                return false; // stops user browser being directed to the php file
            }); // end click function


        });
    </script>
    <script type="text/javascript" src="{{ asset('/js/jquery.themepunch.tools.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.themepunch.revolution.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.easing.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.hidescroll.min.js') }}"></script>
    <script type="text/javascript">
        $('.header-top').hidescroll();
    </script>
    <script type="text/javascript" src="{{ asset('/js/smoothScroll.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/isotope.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.counterup.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/waypoints.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/scrollReveal.js') }}"></script>
    <script type="text/javascript">
        (function($) { "use strict";
            window.scrollReveal = new scrollReveal();
        })(jQuery);
    </script>
    <script type="text/javascript" src="{{ asset('/js/jquery.colorbox.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/owl.carousel.min.js') }}"></script>
    <script type="text/javascript">
        (function($) { "use strict";
            jQuery(document).ready(function() {
                var offset = 450;
                var duration = 500;
                jQuery(window).scroll(function() {
                    if (jQuery(this).scrollTop() > offset) {
                        jQuery('.scroll-to-top').fadeIn(duration);
                    } else {
                        jQuery('.scroll-to-top').fadeOut(duration);
                    }
                });

                jQuery('.scroll-to-top').click(function(event) {
                    event.preventDefault();
                    jQuery('html, body').animate({scrollTop: 0}, duration);
                    return false;
                })
            });
        })(jQuery);
    </script>


    <script type="text/javascript" src="{{ asset('/js/styleswitcher.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/custom-shop-home-1.js') }}"></script>
    <!-- End Document
================================================== -->
@endsection
