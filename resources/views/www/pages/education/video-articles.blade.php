@extends('www.layouts.main')
@section('title')
    {{ $article->title }}
@endsection
@section('description')
    @if(($article->req_login && Auth::guard('practitioner')->check())|| !$article->req_login)
        @if(json_decode($article->content))
            @foreach(json_decode($article->content) as $item)
                @if($item->layout=='text')
                    {{ substr(str_replace('&nbsp;',' ',$article->subtitle.' - '.strip_tags($item->attributes->content)),0,160) }}
                @endif
            @endforeach
        @endif
    @else
        {{substr($article->subtitle.' - '.$article->preview_text,0,160)}}
    @endif
@endsection
@section('breadcrumbs')
    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset;background-color:#7AA43F;">
            <ol class="breadcrumb medlab_breadcrumbs" style="margin-bottom: unset;background-color:#7AA43F;">
            </ol>
        </div>
    </div>

    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset">
            <ol class="breadcrumb medlab_breadcrumbs">
                <li><a class="medlab_breadcrumbs_link" href="/">@lang('trs.home')</a></li>
                <li><a class="medlab_breadcrumbs_link" href="{{ route('www.articles') }}">@lang('trs.articles')</a></li>
                <li class="active medlab_breadcrumbs_text">{{ $article->title }}</li>
            </ol>
        </div>
    </div>
@endsection
@section('content')

    <main class="cd-main-content" id="main">


        <section class="section white-section section-home-padding-top">
            <div class="container">
                <div class="sixteen columns">
                    <div class="section-title left">
                        <h1>{{ $article->title }}</h1>
                        <div class="subtitle left big">{{ $article->subtitle }}</div>
                        @if(isset($article->date))
                            <div class="subtitle left">Date:&nbsp&nbsp{{ substr($article->date,0,10) }}</div>@endif
                        @if(count($article->member))
                            <div class="subtitle left">Author:&nbsp&nbsp
                                @if(count($article->member))
                                    {{$article->member[0]['name']}}
                                    @if(count($article->member)!=1)
                                        @for($i=1;$i<count($article->member);$i++)
                                            ,&nbsp{{ $article->member[$i]['name'] }}
                                        @endfor
                                    @endif
                                @endif
                            </div>@endif
                    </div>
                </div>
            </div>
        </section>

        <!-- SECTION
        ================================================== -->

        <section class="section white-section section-padding-bottom">

            <div class="container">
                <div class="twelve columns">
                    <div class="blog-big-wrapper white-section"
                         data-scroll-reveal="enter bottom move 200px over 1s after 0.3s"
                         style="color:white;padding:unset;">
                        @if(($article->req_login && !(Auth::guard('practitioner')->check())))
                            <div style="opacity: 0.85;background: black;">
                                <iframe
                                    srcdoc="<p style='text-align:center;margin-top:25%;font-size:xxx-large;color:white'>Please login to view this video</p>"
                                    src="{{ $article->video_link }}" width="100%" height="560"
                                    allowfullscreen></iframe>
                            </div>
                        @else
                            <iframe src="{{ $article->video_link }}" width="100%" height="560"
                                    allowfullscreen></iframe>
                        @endif
                    </div>
                    @if(($article->req_login && Auth::guard('practitioner')->check())|| !$article->req_login)
                        @if(json_decode($article->content))
                        <div class="grey-section" style="margin-top: 20px;">
                            <div class="articleClass padding">
                                @foreach(json_decode($article->content) as $item)
                                    @if($item->layout=='text')
                                        <div class="sixteen columns">
                                            {!! $item->attributes->content !!}
                                        </div>
                                    @elseif($item->layout=='image')
                                        <div class="sixteen columns">
                                            <img src="{{ $image_url }}/www/Images/{{ \App\Models\MediaLibrary::where('id',$item->attributes->image)->first()->name }}"
                                                 alt="{{ $item->attributes->image_alt }}" style = "width: {{ $item->attributes->width }}%; float: {{ $item->attributes->float }}">
                                        </div>
                                    @else
                                        <div class="sixteen columns">
                                            <iframe src="{{ $item->attributes->content }}" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                        @endif

                        @if(!$article->disable_comments)
                            <div class="post-content-com-top grey-section"
                                 data-scroll-reveal="enter bottom move 200px over 1s after 0.3s">
                                <p>COMMENTS <span>{{ count($comments) }}</span></p>
                            </div>
                            @if(Auth::guard('practitioner')->check()||Auth::guard('patient')->check())

                                @foreach($comments as $comment)
                                    <div id="comment_{{$comment->id}}">
                                        <div class="post-content-comment grey-section"
                                             data-scroll-reveal="enter bottom move 200px over 1s after 0.3s">
                                            <h4>{{ $comment->user_name }}</h4>
                                            <p>{{ $comment->content }}</p>
                                            <a class="reply" id="reply_{{$comment->id}}"
                                               onclick="addComment({{$comment->id}})">reply</a>
                                        </div>
                                    </div>
                                    @if(count($comment->children))
                                        @foreach($comment->children as $children)
                                            <div class="post-content-comment reply-in grey-section"
                                                 data-scroll-reveal="enter bottom move 200px over 1s after 0.3s"
                                                 id="comment_{{$children['id']}}">
                                                <h4>{{ $children['user_name'] }}</h4>
                                                <p>{{ $children['content'] }}</p>
                                            </div>
                                        @endforeach
                                    @endif
                                @endforeach
                                <form action="{{ url('post_comment') }}" method="post" id="comment_form"
                                      style="width: 100%">
                                    {!! csrf_field() !!}
                                    <div class="leave-reply grey-section"
                                         data-scroll-reveal="enter bottom move 200px over 1s after 0.3s">
                                        <h4>LEAVE A COMMENT</h4>

                                        <input name="article" type="hidden" value="{{ $article->id }}"/>
                                        <input name="parent" type="hidden" value='0'/>
                                        <textarea name="content" placeholder="COMMENT"></textarea>
                                        <button class="post-comment sendForm" id="send">post comment</button>

                                    </div>
                                </form>
                            @else
                                <div class="leave-reply grey-section"
                                     data-scroll-reveal="enter bottom move 200px over 1s after 0.3s">
                                    <h4 style="padding-bottom: unset;">Tell us what you think <a onClick="open_login()" style="cursor: pointer;color: #6ba53a;">login</a> to share your thoughts.</h4>
                                </div>
                            @endif
                        @endif
                    @else
                        <div class="grey-section" style="margin-top: 20px;">
                            <div class="articleClass padding">
                                {{$article->preview_text}}
                            </div>
                            <div style="height: 100px;float: right;">
                                <div style="margin-right: unset;margin-top: 10%;"
                                     class="button-shortcodes text-size-1 text-padding-1 version-1"
                                     onClick="open_login()"><span>&#xf18e;</span> Practitioner login to view
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="four columns" data-scroll-reveal="enter bottom move 200px over 1s after 0.3s">
                    <div class="post-sidebar">
                        @if(count($article->product_family))
                            <div class="separator-sidebar"></div>
                            <h3>Related Products</h3>
                            <ul class="link-recents">
                                @foreach($article->product_family as $a)
                                    <li><a href="#">{{ $a['product_name'] }}</a></li>
                                @endforeach
                            </ul>
                        @endif
                        @if(count($article->clinical_trial))
                            <div class="separator-sidebar"></div>
                            <h3>Related Clinical Trials</h3>
                            <ul class="link-recents">
                                @foreach($article->clinical_trial as $a)
                                    <li><a style="color: #6ba53a;word-break: break-word;"
                                           href="https://pharma.medlab.co/clinical_trials">{{ $a['name'] }}</a></li>
                                @endforeach
                            </ul>
                        @endif
                        @if(count($article->publication))
                            <div class="separator-sidebar"></div>
                            <h3>Related Publications And Presentations</h3>
                            <ul class="link-recents">
                                @foreach($article->publication as $a)
                                    <li><a style="color: #6ba53a;word-break: break-word;"
                                           href="https://pharma.medlab.co/publications">{{ $a['title'] }}</a></li>
                                @endforeach
                            </ul>
                        @endif
                        @if($article->article_category)
                            <div class="separator-sidebar"></div>
                            <h3>Categories</h3>
                            <ul class="link-recents">
                                @foreach($article->article_category as $a)
                                    <li><a style="color: #6ba53a;word-break: break-word;"
                                           href="https://dev-www.medlab.co/education?article_category={{ $a['name'] }}">{{ str_replace('_',' ',$a['name']) }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        @endif
                    </div>
                </div>
            </div>

        </section>


        <!-- AUTHOR SECTION
        ================================================== -->

        @if(count($article->member))
            <div class="container">
                <div class="sixteen columns">
                    <div class="section-title">
                        <h4>ABOUT THE AUTHOR</h4>
                    </div>
                </div>
                @foreach($article->member as $people)
                    <section class="section white-section section-padding-top-bottom">


                        <div class="sixteen columns">
                            <div class="blockquotes-box-1 grey-section blockquotes-float-content"
                                 style="max-width: 300px;padding: 10px;border: unset;">
                                <img src="{{ $image_url }}/www/Images/{{ $people['picture'] }}"
                                     alt="{{ $people['alt'] }}">
                            </div>
                            <div class="team-name-top mobile-inline-block"
                                 style="font-family: 'Playball',crusive;">{{ $people['position'] }}</div>
                            <h5 style="margin: 20px;font-size: x-large;text-align: unset;margin-left: unset;">{{ $people['name'] }}</h5>
                            {!! $people['bio'] !!}
                        </div>
                    </section>
                @endforeach
            </div>
        @endif
        @if(count($article->related_article))
            <section class="section grey-section section-padding-top-bottom" style="padding-top: unset;">
                <div class="container">
                    <div class="sixteen columns">
                        <div class="section-title">
                            <h4>MORE BY THIS AUTHOR</h4>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="blog-wrapper">
                        <div id="blog-grid-masonry">
                            @foreach($article->related_article as $item)
                                @if($item->article_type=='link')
                                    <a href={{ route('www.articles_content',['id' => preg_replace('/--+/','-',preg_replace('/[^A-Za-z0-9\-]/', '-', $item->title)).'-'.$item->id]) }}>
                                        <div class="blog-box-3">
                                            <div class="blog-box-1 link-post grey-section">
                                                <h4 class="two-row-max"
                                                    style="min-height: 96px;">{{ $item->title }}</h4>
                                                <p class="four-row-max"
                                                   style="padding-bottom: unset;min-height: 92px;">{{ $item->preview_text }}</p>
                                                <br>
                                                <p>{{ substr($item->date,0,10) }}</p>
                                                @if(isset($item->author))<p>Author: {{ $item->author }}</p>@endif
                                                <div class="link">&#xf178;</div>
                                            </div>
                                        </div>
                                    </a>
                                @elseif($item->article_type=='video')
                                    <a href={{ route('www.articles_content',['id' => preg_replace('/--+/','-',preg_replace('/[^A-Za-z0-9\-]/', '-', $item->title)).'-'.$item->id]) }}>
                                        <div class="blog-box-3">
                                            <div class="blog-box-1 grey-section">
                                                @if(($item->req_login && !(Auth::guard('practitioner')->check()||Auth::guard('patient')->check())))
                                                    <div
                                                        style="max-height: 270px; position:relative; padding-bottom: 56.25%; height: 0; opacity: 0.85;background: black; ">
                                                        <iframe
                                                            srcdoc="<p style='text-align:center;margin-top:25%;font-size:xx-large;color:white'>Please login to view this video</p>"
                                                            width="560" height="349" src="{{ $item->video_link }}"
                                                            style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"
                                                            allowfullscreen></iframe>
                                                    </div>
                                                @else
                                                    <div
                                                        style="max-height: 270px; position:relative; padding-bottom: 56.25%; height: 0;">
                                                        <iframe width="560" height="349" src="{{ $item->video_link }}"
                                                                style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"
                                                                allowfullscreen></iframe>
                                                    </div>
                                                @endif
                                                <h4 class="two-row-max"
                                                    style="min-height: 96px;">{{ $item->title }}</h4>
                                                <p class="four-row-max"
                                                   style="padding-bottom: unset;min-height: 92px;">{{ $item->preview_text }}</p>
                                                <br>
                                                <p>{{ substr($item->date,0,10) }}</p>
                                                @if(isset($item->author))<p>Author: {{ $item->author }}</p>@endif
                                                <div class="link">&#xf178;</div>
                                            </div>
                                        </div>
                                    </a>
                                @else
                                    <a href={{ route('www.articles_content',['id' => preg_replace('/--+/','-',preg_replace('/[^A-Za-z0-9\-]/', '-', $item->title)).'-'.$item->id]) }}>
                                        <div class="blog-box-3">
                                            <div class="blog-box-1 grey-section">
                                                <div
                                                    style="max-height: 270px; position:relative; padding-bottom: 56.25%; height: 0;">
                                                    <img src="{{ $image_url }}/www/Images/{{ $item->title_img }}"
                                                         alt="{{ $item->title_alt }}" style="margin-left: auto;
                                                 margin-right: auto; position: absolute; top: 0; left: 0; width: 100%; height: 100%;">
                                                </div>
                                                <h4 class="two-row-max"
                                                    style="min-height: 96px;">{{ $item->title }}</h4>
                                                <p class="four-row-max"
                                                   style="padding-bottom: unset;min-height: 92px;">{{ $item->preview_text }}</p>
                                                <br>
                                                <p>{{ substr($item->date,0,10) }}</p>
                                                @if(isset($item->author))<p>Author: {{ $item->author }}</p>@endif
                                                <div class="link">&#xf178;</div>
                                            </div>
                                        </div>
                                    </a>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </section>
        @endif


        @include('www.layouts.footer')


    </main>

    <div class="scroll-to-top">&#xf106;</div>

    <!-- JAVASCRIPT
    ================================================== -->
    <script type="text/javascript" src="{{ asset('/js/jquery-2.1.1.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/modernizr.custom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.mobile.custom.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/retina-1.1.0.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.animsition.min.js') }}"></script>
    <script type="text/javascript">
        (function ($) {

            "use strict";
            $(document).ready(function () {
                @if (session('login_error'))
                open_login();
                @endif
                $(".animsition").animsition({

                    inClass: 'zoom-in-sm',
                    outClass: 'zoom-out-sm',
                    inDuration: 1500,
                    outDuration: 800,
                    linkElement: '.animsition-link',
                    // e.g. linkElement   :   'a:not([target="_blank"]):not([href^=#])'
                    loading: true,
                    loadingParentElement: 'body', //animsition wrapper element
                    loadingClass: 'animsition-loading',
                    unSupportCss: ['animation-duration',
                        '-webkit-animation-duration',
                        '-o-animation-duration'
                    ],
                    //"unSupportCss" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
                    //The default setting is to disable the "animsition" in a browser that does not support "animation-duration".

                    overlay: false,

                    overlayClass: 'animsition-overlay-slide',
                    overlayParentElement: 'body'
                });
            });
        })(jQuery);
    </script>
    <script type="text/javascript" src="{{ asset('/js/jquery.easing.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.hidescroll.min.js') }}"></script>
    <script type="text/javascript">
        $('.header-top').hidescroll();
    </script>
    <script type="text/javascript" src="{{ asset('/js/smoothScroll.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.counterup.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/waypoints.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/scrollReveal.js') }}"></script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            window.scrollReveal = new scrollReveal();
        })(jQuery);
    </script>
    <script type="text/javascript" src="{{ asset('/js/jquery.colorbox.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/owl.carousel.min.js') }}"></script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            jQuery(document).ready(function () {
                var offset = 450;
                var duration = 500;
                jQuery(window).scroll(function () {
                    if (jQuery(this).scrollTop() > offset) {
                        jQuery('.scroll-to-top').fadeIn(duration);
                    } else {
                        jQuery('.scroll-to-top').fadeOut(duration);
                    }
                });

                jQuery('.scroll-to-top').click(function (event) {
                    event.preventDefault();
                    jQuery('html, body').animate({scrollTop: 0}, duration);
                    return false;
                })
            });
        })(jQuery);
        var form = "<form class='form'></form>";

        function addComment(id) {
            console.log(document.getElementById('reply_' + id).innerText);
            if (document.getElementById('reply_' + id).innerText == 'CANCEL') {
                document.getElementById('comment_form_' + id).remove();
                document.getElementById('reply_' + id).innerText = 'reply';
            } else {
                document.getElementById('reply_' + id).innerText = 'cancel';
                $('#comment_' + id).append("<form action='{{ url('post_comment')}}' method='post' id='comment_form_" + id + "' style='width: 100%' target='_blank'>" + "<input type='hidden' name='_token' value=" + document.getElementsByName('_token')[0].value + ">" + "<div class='leave-reply grey-section post-content-comment'><input name='parent' type='hidden' value=" + id + "><textarea name='content'  placeholder='COMMENT'></textarea><button class='post-comment sendForm' id=" + id + ">reply</button></div></form>");
            }
        }

        $(document).on("click", ".sendForm", function () {
            var fileId = $(this).attr("id");
            console.log(fileId)
            if (fileId == 'send') {
                var data_string = $('#comment_form').serialize();
                $.ajax({
                    type: "POST",
                    url: $('#comment_form').attr('action'),
                    data: data_string,
                    error: function (data) {

                    },
                    success: function (data) {
                        location.reload();

                    }
                });
                return false;
            } else {
                var id = $(this).attr("id");
                var data_string = $('#comment_form_' + id).serialize();
                $.ajax({
                    type: "POST",
                    url: $('#comment_form_' + id).attr('action'),
                    data: data_string,
                    error: function (data) {

                    },
                    success: function (data) {
                        location.reload();

                    }
                });

                return false;
            }
        });
    </script>
{{--    <script type="text/javascript" src="{{ asset('/js/jquery.fitvids.js') }}"></script>--}}
    <script type="text/javascript" src="{{ asset('/js/styleswitcher.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/custom-video-project.js') }}"></script>


    <!-- End Document
    ================================================== -->
@endsection
