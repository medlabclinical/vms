@extends('www.layouts.main')
@section('title')
    {{ $article_category=='*'?$content->title:$content->name }}
@endsection
@section('description')
    @if($content->meta_description)
        {{ $content->meta_description }}
    @elseif($article_category=='*')
        @if(json_decode($content->content))
            @foreach(json_decode($content->content) as $item)
                @if($item->layout=='text')
                    {{ substr(str_replace('&nbsp;',' ',strip_tags($item->attributes->content)),0,160) }}
                @endif
            @endforeach
        @endif

    @else
        {{ substr(str_replace('&nbsp;',' ',strip_tags($content->content)),0,160) }}
    @endif
@endsection
@section('breadcrumbs')
    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset;background-color:#7AA43F;">
            <ol class="breadcrumb medlab_breadcrumbs" style="margin-bottom: unset;background-color:#7AA43F;">
            </ol>
        </div>
    </div>

    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset">
            <ol class="breadcrumb medlab_breadcrumbs">
                <li><a class="medlab_breadcrumbs_link" href="/">@lang('trs.home')</a></li>
                @if($article_category=='*')
                    <li class="active medlab_breadcrumbs_text">@lang('trs.education')</li>
                @else
                    <li><a class="medlab_breadcrumbs_link" href="/education">@lang('trs.education')</a></li>
                    <li class="active medlab_breadcrumbs_text">{{ $article_category }}</li>
                @endif
            </ol>
        </div>
    </div>
@endsection
@section('content')

    <main class="cd-main-content" id="main" style="margin-top:90px">

        <!-- TOP SECTION - SLIDER IMAGE
================================================== -->

        <section class="home">

            <div class="slider-container">
                <div class="tp-banner-container">
                    <div class="tp-banner">
                        <ul>
                            <li data-transition="fade" data-slotamount="1" data-masterspeed="500"
                                data-saveperformance="on" data-title="Intro Slide">
                                <img src="{{ $image_url }}/www/Images/{{ $content->title_img }}"
                                     alt="{{ $content->title_alt }}"
                                     data-lazyload="{{ $image_url }}/www/Images/{{ $content->title_img }}"
                                     data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                                <a href="#">
                                    <div class="black-heavy-3">
                                        <div class="black-heavy-3-heading">
                                            <h1>{{ $article_category=='*'?$content->title:$content->name }}</h1></div>
                                        @if(isset($content->subtitle))
                                            <div class="black-heavy-3-subheading">{{ $content->subtitle }}</div>
                                        @endif
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

        </section>

        <!-- CONTENT SECTION
    ================================================== -->
        <br>
        @if(json_decode($content->content))
            <section class="section white-section section-padding-top" id="scroll-link">
                <div class="container">
                    <div class="sixteen columns remove-bottom">
                        <div class="full-image">
                            <div class="articleClass">
                                @foreach(json_decode($content->content) as $item)
                                    @if($item->layout=='text')
                                        {!! $item->attributes->content !!}
                                    @else
                                        <div style="width: 100%;">
                                            <iframe src="{{ $item->attributes->content }}?transparent=false"
                                                    width="100%" height="500"
                                                    frameborder=”0″ webkitallowfullscreen mozallowfullscreen
                                                    allowfullscreen></iframe>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        @endif

    <!-- ARTICLE SECTION
        ================================================== -->

        <section class="section white-section section-padding-top-bottom" style="padding-top: unset;">

            <div class="container">
                <div class="sixteen columns">
                    <div>
                        <ul id="filter">
                            <li><a href="{{ route('www.articles') }}?article_category={{ $article_category }}&role=*"
                                   @if(!isset($role) || $role == '*') class="current" @endif>Show All</a></li>
                            <li><a href="{{ route('www.articles') }}?article_category={{ $article_category }}&role=1"
                                   @if($role == '1') class="current" @endif>Practitioner</a></li>
                            <li><a href="{{ route('www.articles') }}?article_category={{ $article_category }}&role=0"
                                   @if($role == '0') class="current" @endif>Consumer</a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="clear"></div>
            <div class="blog-wrapper">
                <div id="blog-grid-masonry">
                    @foreach($allArticles as $item)
                        @if($item->article_type=='link')
                            <a href={{ route('www.articles_content',['id' => preg_replace('/--+/','-',preg_replace('/[^A-Za-z0-9\-]/', '-', $item->title)).'-'.$item->id]) }}>
                                <div class="blog-box-3">
                                    <div class="blog-box-1 link-post grey-section">
                                        <h4 class="two-row-max"
                                            style="min-height: 96px;">{{ $item->title }}</h4>
                                        <p class="four-row-max"
                                           style="padding-bottom: unset;min-height: 92px;">{{ $item->preview_text }}</p>
                                        <br>
                                        <p>{{ substr($item->date,0,10) }}</p>
                                        @if(isset($item->author))<p>Author: {{ $item->author }}</p>@endif
                                        <div class="link">&#xf178;</div>
                                    </div>
                                </div>
                            </a>
                        @elseif($item->article_type=='video')
                            <a href={{ route('www.articles_content',['id' => preg_replace('/--+/','-',preg_replace('/[^A-Za-z0-9\-]/', '-', $item->title)).'-'.$item->id]) }}>
                                <div class="blog-box-3">
                                    <div class="blog-box-1 grey-section">
                                        @if(($item->req_login && !(Auth::guard('practitioner')->check()||Auth::guard('patient')->check())))
                                            <div
                                                style="max-height: 270px; position:relative; padding-bottom: 56.25%; height: 0; opacity: 0.85;background: black; ">
                                                <iframe
                                                    srcdoc="<p style='text-align:center;margin-top:25%;font-size:xx-large;color:white'>Please login to view this video</p>"
                                                    width="560" height="349" src="{{ $item->video_link }}"
                                                    style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"
                                                    allowfullscreen></iframe>
                                            </div>
                                        @else
                                            <div
                                                style="max-height: 270px; position:relative; padding-bottom: 56.25%; height: 0;">
                                                <iframe width="560" height="349" src="{{ $item->video_link }}"
                                                        style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"
                                                        allowfullscreen></iframe>
                                            </div>
                                        @endif
                                        <h4 class="two-row-max"
                                            style="min-height: 96px;">{{ $item->title }}</h4>
                                        <p class="four-row-max"
                                           style="padding-bottom: unset;min-height: 92px;">{{ $item->preview_text }}</p>
                                        <br>
                                        <p>{{ substr($item->date,0,10) }}</p>
                                        @if(isset($item->author))<p>Author: {{ $item->author }}</p>@endif
                                        <div class="link">&#xf178;</div>
                                    </div>
                                </div>
                            </a>
                        @else
                            <a href={{ route('www.articles_content',['id' => preg_replace('/--+/','-',preg_replace('/[^A-Za-z0-9\-]/', '-', $item->title)).'-'.$item->id]) }}>
                                <div class="blog-box-3">
                                    <div class="blog-box-1 grey-section">
                                        <div
                                            style="max-height: 270px; position:relative; padding-bottom: 56.25%; height: 0;">
                                            <img src="{{ $image_url }}/www/Images/{{ $item->title_img }}"
                                                 alt="{{ $item->title_alt }}" style="margin-left: auto;
                                                 margin-right: auto; position: absolute; top: 0; left: 0; width: 100%; height: 100%;">
                                        </div>
                                        <h4 class="two-row-max"
                                            style="min-height: 96px;">{{ $item->title }}</h4>
                                        <p class="four-row-max"
                                           style="padding-bottom: unset;min-height: 92px;">{{ $item->preview_text }}</p>
                                        <br>
                                        <p>{{ substr($item->date,0,10) }}</p>
                                        @if(isset($item->author))<p>Author: {{ $item->author }}</p>@endif
                                        <div class="link">&#xf178;</div>
                                    </div>
                                </div>
                            </a>
                        @endif
                    @endforeach
                </div>
            </div>
        </section>


        <!-- SECTION
        ================================================== -->

        <section class="section grey-section section-padding-top-bottom">
            <div class="container">
                <div class="sixteen columns">
                    <div class="blog-left-right-links pagination">
                        @if($allArticles->currentPage()-1!=0)
                            <a href="{{ route('www.articles') }}?role={{ $role }}&article_category={{ $article_category }}&page={{ $allArticles->currentPage()-1 }}">
                                <div class="blog-left-link"><p>PREVIOUS 24</p></div>
                            </a>
                        @endif
                        @if($allArticles->currentPage()!=$allArticles->lastPage())
                            <a href="{{ route('www.articles') }}?role={{ $role }}&article_category={{ $article_category }}&page={{ $allArticles->currentPage()+1 }}">
                                <div class="blog-right-link"><p>NEXT 24</p></div>
                            </a>
                        @endif
                        <div style="padding: 6px">
                            <select onchange="changePage()" id="page" style="float: right;margin-right: 5%;">
                                @for($i=1;$i<$allArticles->lastPage()+1;$i++)
                                    <option
                                        value={{ $i }} @if($allArticles->currentPage() == $i) selected @endif >{{ $i }}</option>
                                @endfor
                            </select>
                            <p style="float: right">Page No.</p>
                        </div>
                    </div>
                </div>
            </div>

        </section>

        @include('www.layouts.footer')
    </main>

    <div class="scroll-to-top">&#xf106;</div>



    <!-- JAVASCRIPT
    ================================================== -->
    <script type="text/javascript" src="{{ asset('/js/jquery-2.1.1.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/modernizr.custom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.mobile.custom.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/retina-1.1.0.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.animsition.min.js') }}"></script>
    <script type="text/javascript">
        function submitForm() {
            var form = document.getElementById("filterForm");
            form.submit();
        }

        function changePage() {
            var page = document.getElementById("page").value;

            location.href = "{{ route('www.articles') }}?role={{ $role }}&article_category={{ $article_category }}&page=" + page;

        }


        (function ($) {
            "use strict";
            $(document).ready(function () {
                @if (session('login_error'))
                open_login();
                @endif

                $(".animsition").animsition({

                    inClass: 'zoom-in-sm',
                    outClass: 'zoom-out-sm',
                    inDuration: 1500,
                    outDuration: 800,
                    linkElement: '.animsition-link',
                    // e.g. linkElement   :   'a:not([target="_blank"]):not([href^=#])'
                    loading: true,
                    loadingParentElement: 'body', //animsition wrapper element
                    loadingClass: 'animsition-loading',
                    unSupportCss: ['animation-duration',
                        '-webkit-animation-duration',
                        '-o-animation-duration'
                    ],
                    //"unSupportCss" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
                    //The default setting is to disable the "animsition" in a browser that does not support "animation-duration".

                    overlay: false,

                    overlayClass: 'animsition-overlay-slide',
                    overlayParentElement: 'body'
                });
            });

            $(document).ready(function () {
                $('#example').DataTable({
                    'searching': false,
                    'lengthChange': false,
                });

            });
        })(jQuery);
    </script>
    <script type="text/javascript" src="{{ asset('/js/jquery.easing.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.hidescroll.min.js') }}"></script>
    <script type="text/javascript">
        $('.header-top').hidescroll();
    </script>
    {{--    <script type="text/javascript" src="{{ asset('/js/smoothScroll.js') }}"></script>--}}
    <script type="text/javascript" src="{{ asset('/js/jquery.parallax-1.1.3.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/owl.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/scrollReveal.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/masonry.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/isotope.js') }}"></script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            window.scrollReveal = new scrollReveal();
        })(jQuery);
    </script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            jQuery(document).ready(function () {
                var offset = 450;
                var duration = 500;
                jQuery(window).scroll(function () {
                    if (jQuery(this).scrollTop() > offset) {
                        jQuery('.scroll-to-top').fadeIn(duration);
                    } else {
                        jQuery('.scroll-to-top').fadeOut(duration);
                    }
                });

                jQuery('.scroll-to-top').click(function (event) {
                    event.preventDefault();
                    jQuery('html, body').animate({scrollTop: 0}, duration);
                    return false;
                })
            });
        })(jQuery);

    </script>

    <script type="text/javascript" src="{{ asset('/js/styleswitcher.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.themepunch.tools.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.themepunch.revolution.min.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('/DataTables/datatables.min.css') }}"/>

    <script type="text/javascript" src="{{ asset('/DataTables/datatables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/custom-corporate-home-1.js') }}"></script>


    <!-- End Document
================================================== -->
@endsection
