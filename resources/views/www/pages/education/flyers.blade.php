@extends('www.layouts.main')
@section('title')
    {{ $pdf_type=='*'?$content->title:$content->name }}
@endsection
@section('description')
    @if($content->meta_description)
        {{ $content->meta_description }}
    @elseif($pdf_type=='*')
        @if(json_decode($content->content))
            @foreach(json_decode($content->content) as $item)
                @if($item->layout=='text')
                    {{ substr(str_replace('&nbsp;',' ',strip_tags($item->attributes->content)),0,160) }}
                @endif
            @endforeach
        @endif
    @else
        {{ substr(str_replace('&nbsp;',' ',strip_tags($content->content)),0,160) }}
    @endif
@endsection
@section('breadcrumbs')
    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset;background-color:#7AA43F;">
            <ol class="breadcrumb medlab_breadcrumbs" style="margin-bottom: unset;background-color:#7AA43F;">
            </ol>
        </div>
    </div>

    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset">
            <ol class="breadcrumb medlab_breadcrumbs">
                <li><a class="medlab_breadcrumbs_link" href="/">@lang('trs.home')</a></li>
                @if($pdf_type=='*')
                    <li class="active medlab_breadcrumbs_text">Flyers</li>
                @else
                    <li><a class="medlab_breadcrumbs_link" href="/flyers">Flyers</a></li>
                    <li class="active medlab_breadcrumbs_text">{{ $pdf_type }}</li>
                @endif
            </ol>
        </div>
    </div>
@endsection
@section('content')

    <main class="cd-main-content" id="main" style="margin-top:90px">

        <!-- TOP SECTION - SLIDER IMAGE
================================================== -->

        <section class="home">

            <div class="slider-container">
                <div class="tp-banner-container">
                    <div class="tp-banner">
                        <ul>
                            <li data-transition="fade" data-slotamount="1" data-masterspeed="500"
                                data-saveperformance="on" data-title="Intro Slide">
                                <img src="{{ $image_url }}/www/Images/{{ $content->title_img }}"
                                     alt="{{ $content->title_alt }}"
                                     data-lazyload="{{ $image_url }}/www/Images/{{ $content->title_img }}"
                                     data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                                <a href="#">
                                    <div class="black-heavy-3">
                                        <div
                                            class="black-heavy-3-heading">
                                            <h1>{{ $pdf_type=='*'?$content->title:$content->name }}</h1></div>
                                        @if(isset($content->subtitle))
                                            <div class="black-heavy-3-subheading">{{ $content->subtitle }}</div>
                                        @endif
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

        </section>

        <!-- CONTENT SECTION
    ================================================== -->
        <br>
        @if($pdf_type=='*')
            @if(json_decode($content->content))
                <section class="section white-section section-padding-top" id="scroll-link">
                    <div class="container">
                        <div class="sixteen columns remove-bottom">
                            <div class="full-image">
                                <div class="articleClass">
                                    @foreach(json_decode($content->content) as $item)
                                        @if($item->layout=='text')
                                            {!! $item->attributes->content !!}
                                        @else
                                            <div style="width: 100%;">
                                                <iframe src="{{ $item->attributes->content }}?transparent=false"
                                                        width="100%" height="500"
                                                        frameborder=”0″ webkitallowfullscreen mozallowfullscreen
                                                        allowfullscreen></iframe>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            @endif
        @else
            <section class="section white-section section-padding-top" id="scroll-link">
                <div class="container">
                    <div class="sixteen columns remove-bottom">
                        <div class="full-image">
                            <div class="articleClass">
                                {!! $content->content !!}
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        @endif
        <section class="section white-section section-padding-top-bottom" style="padding-top: unset;">
            <div class="container">
                <div class="sixteen columns">
                    <div>
                        <ul id="filter">
                            <li><a href="{{ route('www.flyers') }}?role=*&pdf_type={{ $pdf_type }}"
                                   @if(!isset($role) || $role == '*') class="current" @endif>Show All</a></li>
                            <li><a href="{{ route('www.flyers') }}?role=1&pdf_type={{ $pdf_type }}"
                                   @if($role == '1') class="current" @endif>Practitioner Only</a></li>
                            <li><a href="{{ route('www.flyers') }}?role=2&pdf_type={{ $pdf_type }}"
                                   @if($role == '2') class="current" @endif>Practitioner and Patient Only</a></li>
                            <li><a href="{{ route('www.flyers') }}?role=3&pdf_type={{ $pdf_type }}"
                                   @if($role == '3') class="current" @endif>Guest</a></li>
                        </ul>
                    </div>
                </div>
            </div>


            <div class="clear"></div>
            <div class="blog-wrapper">
                <div id="blog-grid-masonry">
                    @foreach($AllPDF as $item)
                        @if(($item->access_level=='1'||$item->access_level=='2')&&!(Auth::guard('practitioner')->check()||Auth::guard('patient')->check()))

                            <div class="blog-box-3">
                                <a href='#' onClick='open_login()'>
                                    <div class="blog-box-1 grey-section">
                                        <div
                                            style="max-height: 270px; position:relative; padding-bottom: 56.25%; height: 0; opacity: 0.85;background: black; ">
                                            <p style='text-align:center;font-family: initial;font-size:45px;color:white;padding-top: 20%'>
                                                Please login to view</p>
                                            {{--                                            <iframe--}}
                                            {{--                                                srcdoc="<p style='text-align:center;margin-top:23%;font-size:45px;color:white'>Please login to view</p>"--}}
                                            {{--                                                src=""--}}
                                            {{--                                                style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"--}}
                                            {{--                                                allowfullscreen></iframe>--}}
                                        </div>
                                        <h4 class="two-row-max"
                                            style="min-height: 96px;">{{ $item->name }}</h4>
                                        <p class="one-row-max"
                                           style="min-height: 30px;">
                                            Product:&nbsp{{ $item->product_family()->first()->product_name }}</p>
                                        <p class="one-row-max"
                                           style="min-height: 30px;">
                                            Type:&nbsp{{ $item->pdf_type()->first()->name }}</p>
                                        <p class="one-row-max"
                                           style="min-height: 30px;">Language:&nbsp{{ $item->language }}</p>
                                        <p class="one-row-max"
                                           style="min-height: 30px;">Version #&nbsp{{ $item->version_number }}</p>
                                        <br>
                                        <div class="link">&#xf178;</div>
                                    </div>
                                </a>
                            </div>


                        @elseif($item->access_level=='1'&&!Auth::guard('practitioner')->check())

                            <div class="blog-box-3">
                                <a href='#' onClick='open_login()'>
                                    <div class="blog-box-1 link-post grey-section">
                                        <div
                                            style="max-height: 270px; position:relative; padding-bottom: 56.25%; height: 0; opacity: 0.85;background: black; ">
                                            <p style='text-align:center;font-family: initial;font-size:45px;color:white;padding-top: 20%'>
                                                Please login to view</p>
                                        </div>
                                        <h4 class="two-row-max"
                                            style="min-height: 96px;">{{ $item->name }}</h4>
                                        <p class="one-row-max"
                                           style="min-height: 30px;">
                                            Product:&nbsp{{ $item->product_family()->first()->product_name }}</p>
                                        <p class="one-row-max"
                                           style="min-height: 30px;">
                                            Type:&nbsp{{ $item->pdf_type()->first()->name }}</p>
                                        <p class="one-row-max"
                                           style="min-height: 30px;">Language:&nbsp{{ $item->language }}</p>
                                        <p class="one-row-max"
                                           style="min-height: 30px;">Version #&nbsp{{ $item->version_number }}</p>
                                        <br>
                                        <div class="link">&#xf178;</div>
                                    </div>
                                </a>
                            </div>

                        @else
                            <a href="{{ $image_url }}/{{$item->file_name}}">
                                <div class="blog-box-3">
                                    <div class="blog-box-1 link-post grey-section">
                                        @if($item->preview_img)
                                            <img
                                                src="{{ $image_url }}/www/Images/{{ $item->image()->first()->name }}">
                                        @elseif($item->pdf_type()->first()->image()->first())
                                            <img
                                                src="{{ $image_url }}/www/Images/{{ $item->pdf_type()->first()->image()->first()->name }}">
                                        @else
                                        @endif
                                        <h4 class="two-row-max"
                                            style="min-height: 96px;">{{ $item->name }}</h4>
                                        <p class="one-row-max"
                                           style="min-height: 30px;">
                                            Product:&nbsp{{ $item->product_family()->first()->product_name }}</p>
                                        <p class="one-row-max"
                                           style="min-height: 30px;">
                                            Type:&nbsp{{ $item->pdf_type()->first()->name }}</p>
                                        <p class="one-row-max"
                                           style="min-height: 30px;">Language:&nbsp{{ $item->language }}</p>
                                        <p class="one-row-max"
                                           style="min-height: 30px;">Version #&nbsp{{ $item->version_number }}</p>
                                        <br>
                                        <div class="link">&#xf178;</div>
                                    </div>
                                </div>
                            </a>
                        @endif
                    @endforeach
                </div>
            </div>


        </section>

        <!-- SELECT PAGE SECTION
        ================================================== -->

        <section class="section grey-section section-padding-top-bottom">
            <div class="container">
                <div class="sixteen columns">
                    <div class="blog-left-right-links pagination">
                        @if($AllPDF->currentPage()-1!=0)
                            <a href="{{ route('www.flyers') }}?role={{ $role }}&pdf_type={{ $pdf_type }}&product_family={{ $product_family }}&page={{ $AllPDF->currentPage()-1 }}">
                                <div class="blog-left-link"><p>PREVIOUS 24</p></div>
                            </a>
                        @endif

                        @if($AllPDF->currentPage()!=$AllPDF->lastPage())
                            <a href="{{ route('www.flyers') }}?role={{ $role }}&pdf_type={{ $pdf_type }}&product_family={{ $product_family }}&page={{ $AllPDF->currentPage()+1 }}">
                                <div class="blog-right-link"><p>NEXT 24</p></div>
                            </a>
                        @endif
                        <a>
                            <div style="padding: 6px">
                                <select onchange="changePage()" id="page" style="float: right;margin-right: 5%;">
                                    @for($i=1;$i<$AllPDF->lastPage()+1;$i++)
                                        <option
                                            value={{ $i }} @if($AllPDF->currentPage() == $i) selected @endif >{{ $i }}</option>
                                    @endfor
                                </select>
                                <p style="float: right">Page No.</p>
                            </div>
                        </a>


                    </div>
                </div>
            </div>

        </section>

        @include('www.layouts.footer')
    </main>

    <div class="scroll-to-top">&#xf106;</div>



    <!-- JAVASCRIPT
    ================================================== -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdf.js/2.2.228/pdf.min.js"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery-2.1.1.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/modernizr.custom.js') }}"></script>
    {{--    <script type="text/javascript" src="{{ asset('/js/jquery.mobile.custom.min.js') }}"></script>--}}
    {{--    <script type="text/javascript" src="{{ asset('/js/retina-1.1.0.min.js') }}"></script>--}}
    <script type="text/javascript" src="{{ asset('/js/jquery.animsition.min.js') }}"></script>
    <script type="text/javascript">
        function changePage() {
            var page = document.getElementById("page").value;

            location.href = "{{ route('www.flyers') }}?role={{ $role }}&pdf_type={{ $pdf_type }}&product_family={{ $product_family }}&page=" + page;

        }

        (function ($) {
            "use strict";
            $(document).ready(function () {
                $(".animsition").animsition({

                    inClass: 'zoom-in-sm',
                    outClass: 'zoom-out-sm',
                    inDuration: 1500,
                    outDuration: 800,
                    linkElement: '.animsition-link',
                    // e.g. linkElement   :   'a:not([target="_blank"]):not([href^=#])'
                    loading: true,
                    loadingParentElement: 'body', //animsition wrapper element
                    loadingClass: 'animsition-loading',
                    unSupportCss: ['animation-duration',
                        '-webkit-animation-duration',
                        '-o-animation-duration'
                    ],
                    //"unSupportCss" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
                    //The default setting is to disable the "animsition" in a browser that does not support "animation-duration".

                    overlay: false,

                    overlayClass: 'animsition-overlay-slide',
                    overlayParentElement: 'body'
                });

                {{--                @foreach($AllPDF as $item)--}}
                {{--                showPDF('{{ $image_url }}/{{ $item->file_name }}',{{ $item->id }});--}}
                {{--                @endforeach--}}
                {{--                var _PDF_DOC,--}}
                {{--                    _CURRENT_PAGE,--}}
                {{--                    _TOTAL_PAGES,--}}
                {{--                    _PAGE_RENDERING_IN_PROGRESS = 0;--}}
                {{--                // _CANVAS = document.querySelector('#pdf-canvas');--}}

                {{--                // initialize and load the PDF--}}
                {{--                async function showPDF(pdf_url, id) {--}}
                {{--                    // document.querySelector("#pdf-loader").style.display = 'block';--}}

                {{--                    // get handle of pdf document--}}
                {{--                    try {--}}
                {{--                        _PDF_DOC = await pdfjsLib.getDocument({url: pdf_url});--}}
                {{--                    } catch (error) {--}}
                {{--                    }--}}

                {{--                    // total pages in pdf--}}
                {{--                    _TOTAL_PAGES = 1;--}}

                {{--                    // Hide the pdf loader and show pdf container--}}
                {{--                    // document.querySelector("#pdf-loader").style.display = 'none';--}}
                {{--                    // document.querySelector("#pdf-contents_"+id).style.display = 'block';--}}
                {{--                    // document.querySelector("#pdf-total-pages").innerHTML = _TOTAL_PAGES;--}}

                {{--                    // show the first page--}}
                {{--                    showPage(1, id);--}}
                {{--                }--}}

                {{--                // load and render specific page of the PDF--}}
                {{--                async function showPage(page_no, id) {--}}
                {{--                    // _CANVAS = document.querySelector('#pdf-canvas_'+id);--}}
                {{--                    _PAGE_RENDERING_IN_PROGRESS = 1;--}}
                {{--                    _CURRENT_PAGE = page_no;--}}

                {{--                    // disable Previous & Next buttons while page is being loaded--}}
                {{--                    // document.querySelector("#pdf-next").disabled = true;--}}
                {{--                    // document.querySelector("#pdf-prev").disabled = true;--}}

                {{--                    // while page is being rendered hide the canvas and show a loading message--}}
                {{--                    // document.querySelector("#pdf-canvas").style.display = 'none';--}}
                {{--                    // document.querySelector("#page-loader").style.display = 'block';--}}

                {{--                    // update current page--}}
                {{--                    // document.querySelector("#pdf-current-page").innerHTML = page_no;--}}

                {{--                    // get handle of page--}}
                {{--                    try {--}}
                {{--                        var page = await _PDF_DOC.getPage(page_no);--}}
                {{--                    } catch (error) {--}}
                {{--                    }--}}

                {{--                    // original width of the pdf page at scale 1--}}
                {{--                    var pdf_original_width = page.getViewport(1).width;--}}

                {{--                    // as the canvas is of a fixed width we need to adjust the scale of the viewport where page is rendered--}}
                {{--                    var scale_required = document.querySelector('#pdf-canvas_' + id).width / pdf_original_width;--}}

                {{--                    // get viewport to render the page at required scale--}}
                {{--                    var viewport = page.getViewport(scale_required);--}}

                {{--                    // set canvas height same as viewport height--}}
                {{--                    document.querySelector('#pdf-canvas_' + id).height = viewport.height;--}}
                {{--                    // document.querySelector('#pdf-canvas_'+id).width = document.getElementById(id).width;--}}

                {{--                    // setting page loader height for smooth experience--}}
                {{--                    // document.querySelector("#page-loader").style.height =  document.querySelector('#pdf-canvas_'+id).height + 'px';--}}
                {{--                    // document.querySelector("#page-loader").style.lineHeight = document.querySelector('#pdf-canvas_'+id).height + 'px';--}}

                {{--                    // page is rendered on <canvas> element--}}
                {{--                    var render_context = {--}}
                {{--                        canvasContext: document.querySelector('#pdf-canvas_' + id).getContext('2d'),--}}
                {{--                        viewport: viewport--}}
                {{--                    };--}}

                {{--                    // render the page contents in the canvas--}}
                {{--                    try {--}}
                {{--                        await page.render(render_context);--}}
                {{--                    } catch (error) {--}}
                {{--                        alert(error.message);--}}
                {{--                    }--}}

                {{--                    _PAGE_RENDERING_IN_PROGRESS = 0;--}}

                {{--                    // re-enable Previous & Next buttons--}}
                {{--                    // document.querySelector("#pdf-next").disabled = false;--}}
                {{--                    // document.querySelector("#pdf-prev").disabled = false;--}}

                {{--                    // show the canvas and hide the page loader--}}
                {{--                    // document.querySelector("#pdf-canvas").style.display = 'block';--}}
                {{--                    // document.querySelector("#page-loader").style.display = 'none';--}}
                {{--                }--}}
            });


        })(jQuery);
    </script>
    <script type="text/javascript" src="{{ asset('/js/jquery.parallax-1.1.3.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/owl.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/scrollReveal.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/masonry.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/isotope.js') }}"></script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            window.scrollReveal = new scrollReveal();
        })(jQuery);
    </script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            jQuery(document).ready(function () {
                var offset = 450;
                var duration = 500;
                jQuery(window).scroll(function () {
                    if (jQuery(this).scrollTop() > offset) {
                        jQuery('.scroll-to-top').fadeIn(duration);
                    } else {
                        jQuery('.scroll-to-top').fadeOut(duration);
                    }
                });

                jQuery('.scroll-to-top').click(function (event) {
                    event.preventDefault();
                    jQuery('html, body').animate({scrollTop: 0}, duration);
                    return false;
                })
            });
        })(jQuery);

    </script>

    {{--    <script type="text/javascript" src="{{ asset('/js/styleswitcher.js') }}"></script>--}}
    <script type="text/javascript" src="{{ asset('/js/jquery.themepunch.tools.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.themepunch.revolution.min.js') }}"></script>
    {{--    <link rel="stylesheet" type="text/css" href="{{ asset('/DataTables/datatables.min.css') }}"/>--}}

    {{--    <script type="text/javascript" src="{{ asset('/DataTables/datatables.min.js') }}"></script>--}}
    <script type="text/javascript" src="{{ asset('/js/custom-corporate-home-1.js') }}"></script>



    <!-- End Document
================================================== -->
@endsection
