@extends(substr($_SERVER['HTTP_HOST'],0,7)=='dev-vms'?'vms.layouts.main':(substr($_SERVER['HTTP_HOST'],0,7)=='dev-www'?'www.layouts.main':'pharma.layouts.main'))

@section('content')
    <main class="cd-main-content" id="main" style="margin-top:90px">

        <section class="section white-section section-padding-top-bottom" id="scroll-link-6">
            <div class="container">

                <div class="sixteen columns">
                    <div class="section-title">
                        <h2>Reset your password</h2>
                    </div>
                </div>
                <form name="ajax-form" id="ajax-form" action="{{ route('update_patient_password') }}"
                      method="post">
                    @csrf
                    <div class="sixteen columns">
                        <div class="row">
                            <label for="patient_password" class="youpasswd required"> Your current password</label>
                            <input id="patient_password" type="password" class="form-control"
                                   name="current_password" required autofocus style="text-transform: unset;">
                        </div>

                        <div class="row">
                            <label for="new-password" class="youpasswd required"> Enter new password </label>
                            <input id="new-password" type="password" class="form-control"
                                   name="new-password"
                                   value="{{$role->new_password}}" required
                                   autofocus style="text-transform: unset;">
                        </div>

                        <div class="alert alert-blue">
                            <p style="text-transform: unset;">Password needs to be 6 to 20 characters,contain at least
                                one numeric digit,
                                one uppercase and one lowercase letter.</p>
                        </div>
                        <br>

                        <div class="row">
                            <label for="password-confirm" class="youpasswd required"> Confirm your password </label>
                            <input id="password-confirm" type="password"
                                   class="form-control"
                                   name="password-confirm" required
                                   autofocus style="text-transform: unset;">
                        </div>

                        <div class="sixteen columns">
                            <div id="button-con">
                                <button class="send_message" type="submit">update</button>
                            </div>
                        </div>


                        {{--                </div>--}}
                    </div>
                </form>
            </div>
        </section>
    </main>
    @include('www.layouts.footer')

    <!-- JAVASCRIPT
    ================================================== -->
    <script type="text/javascript" src="{{ asset('/js/jquery-2.1.1.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/modernizr.custom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.mobile.custom.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/retina-1.1.0.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.animsition.min.js') }}"></script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            $(document).ready(function () {
                $("#ajax-form").submit(function (e) {

                    e.preventDefault(); // avoid to execute the actual submit of the form.
                        {{--                    @if(Auth::guard('patient')->check())--}}
                    var passw = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/;
                    var currentPassword = document.getElementById("patient_password").value;
                    var newPassword = document.getElementById("new-password").value;
                    var confirmPassword = document.getElementById("password-confirm").value;
                    // console.log(newPassword);
                    // console.log(confirmPassword);
                    if (newPassword !== confirmPassword) {
                        Swal.fire({
                            title: 'Error',
                            text: "Password not match,please try again",
                            icon: 'error',
                        })
                    } else if (!newPassword.match(passw)) {
                        Swal.fire({
                            title: 'Password too weak',
                            text: "Password needs 6 to 20 characters which contain at least one numeric digit, one uppercase and one lowercase letter",
                            icon: 'error',
                        })
                    } else {
                        var form = $(this);
                        var url = form.attr('action');

                        $.ajax({
                            type: "POST",
                            url: url,
                            data: form.serialize(), // serializes the form's elements.
                            success: function (data) {
                                // console.log(data);
                                if (data.msg == 'success') {
                                    Swal.fire({
                                        icon: 'success',
                                        title: 'Password updated successfully ',
                                        confirmButtonText: 'OK',
                                        allowOutsideClick: false
                                    }).then((result) => {
                                        if (result.value) {
                                            window.location.href = "/";
                                        }
                                    })
                                } else if (data.msg == 'wrong password') {
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Wrong Password',
                                        confirmButtonText: 'OK',
                                        allowOutsideClick: false
                                    })
                                } else {
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Something is wrong. Please try again',
                                        // title: data.msg,
                                    })
                                }
                            }
                        });
                    }
                    {{--                @endif--}}

                });

                $(".animsition").animsition({

                    inClass: 'zoom-in-sm',
                    outClass: 'zoom-out-sm',
                    inDuration: 1500,
                    outDuration: 800,
                    linkElement: '.animsition-link',
                    // e.g. linkElement   :   'a:not([target="_blank"]):not([href^=#])'
                    loading: true,
                    loadingParentElement: 'body', //animsition wrapper element
                    loadingClass: 'animsition-loading',
                    unSupportCss: ['animation-duration',
                        '-webkit-animation-duration',
                        '-o-animation-duration'
                    ],
                    //"unSupportCss" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
                    //The default setting is to disable the "animsition" in a browser that does not support "animation-duration".

                    overlay: false,

                    overlayClass: 'animsition-overlay-slide',
                    overlayParentElement: 'body'
                });
            });
        })(jQuery);


        $(document).ready(function () {
            $('#example').DataTable({
                "lengthMenu": [25, 50, 75, 100],
            });

        });
    </script>

    <script type="text/javascript" src="{{ asset('/js/jquery.easing.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.hidescroll.min.js') }}"></script>
    <script type="text/javascript">
        $('.header-top').hidescroll();
    </script>
    <script type="text/javascript" src="{{ asset('/js/smoothScroll.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.parallax-1.1.3.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/owl.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/scrollReveal.js') }}"></script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            window.scrollReveal = new scrollReveal();
        })(jQuery);
    </script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            jQuery(document).ready(function () {
                var offset = 450;
                var duration = 500;
                jQuery(window).scroll(function () {
                    if (jQuery(this).scrollTop() > offset) {
                        jQuery('.scroll-to-top').fadeIn(duration);
                    } else {
                        jQuery('.scroll-to-top').fadeOut(duration);
                    }
                });

                jQuery('.scroll-to-top').click(function (event) {
                    event.preventDefault();
                    jQuery('html, body').animate({scrollTop: 0}, duration);
                    return false;
                })
            });
        })(jQuery);
    </script>
    <script type="text/javascript" src="{{ asset('/js/visible.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/pro-bars.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.counterup.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/waypoints.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/smk-accordion.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/styleswitcher.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/custom-about-1.js') }}"></script>

    <link rel="stylesheet" type="text/css" href="{{ asset('/DataTables/datatables.min.css') }}"/>

    <script type="text/javascript" src="{{ asset('/DataTables/datatables.min.js') }}"></script>

@endsection
