@extends('www.layouts.main')
@section('breadcrumbs')
    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset;background-color:#7AA43F;">
            <ol class="breadcrumb medlab_breadcrumbs" style="margin-bottom: unset;background-color:#7AA43F;">
            </ol>
        </div>
    </div>

    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset">
            <ol class="breadcrumb medlab_breadcrumbs">
                <li><a class="medlab_breadcrumbs_link" href="/">@lang('trs.home')</a></li>
                <li class="active medlab_breadcrumbs_text">@lang('trs.about')</li>
            </ol>
        </div>
    </div>
@endsection




@section('content')

    <main class="cd-main-content" id="main" style="margin-top:90px">

        @if(json_decode($content->slider))
            <section class="home">
                <div class="slider-container">
                    <div class="tp-banner-container">
                        <div class="tp-banner">
                            <ul>
                                @foreach(json_decode($content->slider) as $slider)
                                    @if($slider->layout=='video')
                                        <li data-transition="fade" data-slotamount="1" data-masterspeed="500"
                                            data-saveperformance="on" data-title="Intro Slide">
                                            <iframe
                                                src="https://player.vimeo.com/video/{{ $slider->attributes->video_link }}"
                                                width="100%" height="100%" align="center" frameborder="0"
                                                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                                allowfullscreen></iframe>
                                        </li>
                                    @elseif($slider->layout=='image')
                                        <li data-transition="fade" data-slotamount="1" data-masterspeed="500"
                                            data-saveperformance="on" data-title="Intro Slide">
                                            <img
                                                src="{{ $image_url }}/www/Images/{{ \App\Models\MediaLibrary::where('id',$slider->attributes->slider_image)->first()->name }}"
                                                alt="{{ $slider->attributes->slider_alt }}"
                                                data-lazyload="{{ $image_url }}/www/Images/{{ \App\Models\MediaLibrary::where('id',$slider->attributes->slider_image)->first()->name }}"
                                                data-bgposition="center top" data-bgfit="cover"
                                                data-bgrepeat="no-repeat">
                                            <a href="#">
                                                <div class="black-heavy-3">
                                                    <div class="black-heavy-3-heading">
                                                        <h1>{{ $slider->attributes->title }}</h1></div>
                                                    <div
                                                        class="black-heavy-3-subheading">{{ $slider->attributes->subtitle }}</div>
                                                </div>
                                            </a>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
        @else
            <br>
        @endif

    <!-- CONTENT SECTION
    ================================================== -->

        @if(json_decode($content->content))
            <section class="section white-section section-padding-top" id="scroll-link">
                <div class="container">
                    <div class="sixteen columns remove-bottom"
                    >
                        <div class="full-image">
                            <div class="articleClass padding">
                                @foreach(json_decode($content->content) as $item)
                                    @if($item->layout=='text')
                                        {!! $item->attributes->content !!}
                                    @else
                                        <div style="width: 100%;">
                                            <iframe src="{{ $item->attributes->content }}?transparent=false"
                                                    width="100%" height="500"
                                                    frameborder=”0″ webkitallowfullscreen mozallowfullscreen
                                                    allowfullscreen></iframe>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        @endif


        <section class="section white-section section-padding-top-bottom" id="scroll-link-6">
            <div class="container">

                <div class="sixteen columns">
                    <div class="section-title">
                        <h2>CHANGE REFERRAL CODE</h2>
                        {{--                        <div class="subtitle big">Medlab</div>--}}
                    </div>
                </div>

                <form name="ajax-form" id="ajax-form" action="{{ route('update-referral-code') }}"
                      method="post">
                    @csrf
                    <div class="sixteen columns" data-scroll-reveal="enter right move 200px over 1s after 0.3s">
                    </div>
                <div class="row">
                    <label for="referral_code" class="uname"  style="width: 50%;float:left;margin-right: 1%">
                        Current Referral Code</label>
                    <input id="referral_code" type="text"
                           class="form-control"
                           name="current_referral_code" readonly value="{{ $role->practitioner()->first()->referral_code }}"
                           style="text-transform: unset;width: 50%;float:left;"/>
                </div>
                <div class="row">

                    <label for="new_referral_code" class="uname" style="width: 50%;float:left;margin-right: 1%">
                        New Referral Code</label>
                    <input id="new_referral_code" type="text"
                           class="form-control"
                           name="new_referral_code" required
                           style="text-transform: unset;width: 50%;float:left;" />
                </div>
                    <div class="sixteen columns">
                        <div id="button-con">
                            <button class="send_message" type="submit">update</button>
                        </div>
                    </div>
                </form>
                    </div>
                </section>
{{--    @endif--}}

        @include('www.layouts.footer')

    </main>

    <div class="scroll-to-top">&#xf106;</div>

    <!-- JAVASCRIPT
    ================================================== -->
    <script type="text/javascript" src="{{ asset('/js/jquery-2.1.1.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/modernizr.custom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.mobile.custom.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/retina-1.1.0.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.animsition.min.js') }}"></script>
    <script type="text/javascript">
        (function($) { "use strict";
            $(document).ready(function() {
                @if (session('login_error'))
                open_login();
                @endif
                $(".animsition").animsition({

                    inClass               :   'zoom-in-sm',
                    outClass              :   'zoom-out-sm',
                    inDuration            :    1500,
                    outDuration           :    800,
                    linkElement           :   '.animsition-link',
                    // e.g. linkElement   :   'a:not([target="_blank"]):not([href^=#])'
                    loading               :    true,
                    loadingParentElement  :   'body', //animsition wrapper element
                    loadingClass          :   'animsition-loading',
                    unSupportCss          : [ 'animation-duration',
                        '-webkit-animation-duration',
                        '-o-animation-duration'
                    ],
                    //"unSupportCss" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
                    //The default setting is to disable the "animsition" in a browser that does not support "animation-duration".

                    overlay               :   false,

                    overlayClass          :   'animsition-overlay-slide',
                    overlayParentElement  :   'body'
                });
            });
        })(jQuery);
    </script>
    <script type="text/javascript" src="{{ asset('/js/jquery.themepunch.tools.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.themepunch.revolution.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.easing.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.hidescroll.min.js') }}"></script>
    <script type="text/javascript">
        $('.header-top').hidescroll();
    </script>
    <script type="text/javascript" src="{{ asset('/js/smoothScroll.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/isotope.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.counterup.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/waypoints.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/scrollReveal.js') }}"></script>
    <script type="text/javascript">
        (function($) { "use strict";
            window.scrollReveal = new scrollReveal();
        })(jQuery);
    </script>
    <script type="text/javascript" src="{{ asset('/js/jquery.colorbox.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/owl.carousel.min.js') }}"></script>
    <script type="text/javascript">
        (function($) { "use strict";
            jQuery(document).ready(function() {
                var offset = 450;
                var duration = 500;
                jQuery(window).scroll(function() {
                    if (jQuery(this).scrollTop() > offset) {
                        jQuery('.scroll-to-top').fadeIn(duration);
                    } else {
                        jQuery('.scroll-to-top').fadeOut(duration);
                    }
                });

                jQuery('.scroll-to-top').click(function(event) {
                    event.preventDefault();
                    jQuery('html, body').animate({scrollTop: 0}, duration);
                    return false;
                })
            });
        })(jQuery);

        document.getElementById('new_referral_code').addEventListener('change', function () {
            $.ajax({
                type: "POST",
                url: '{{ url('referral_check') }}',
                data: {
                    _token: '{{ csrf_token() }}',
                    code: this.value,

                },
                timeout: 6000,
                success: function(data) {
                    if(data.code=='1'){
                        Swal.fire({
                            icon: 'error',
                            title: 'The code you have entered already exists.' ,
                        })
                        document.getElementById('new_referral_code').value = "";
                    }
                }

            });
        });
    </script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            $(document).ready(function () {
                $('#ajax-form').submit(function (e) {
                    e.preventDefault(); // avoid to execute the actual submit of the form.
                    var form = $(this);
                    var url = form.attr('action');
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: form.serialize(), // serializes the form's elements.
                        success: function (data) {
                            // console.log(data);
                            if (data.msg=='success') {
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Your update was successful!',
                                    confirmButtonText: 'OK',
                                    allowOutsideClick: false
                                }).then((result) => {
                                    if (result.value) {
                                        document.getElementById("ajax-form").submit();
                                        window.location.reload();
                                    }
                                })
                            } else {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Something is wrong, please try again!',
                                    // title: data.msg,
                                })
                            }
                        }
                    });


                })
                $(".animsition").animsition({

                    inClass: 'zoom-in-sm',
                    outClass: 'zoom-out-sm',
                    inDuration: 1500,
                    outDuration: 800,
                    linkElement: '.animsition-link',
                    // e.g. linkElement   :   'a:not([target="_blank"]):not([href^=#])'
                    loading: true,
                    loadingParentElement: 'body', //animsition wrapper element
                    loadingClass: 'animsition-loading',
                    unSupportCss: ['animation-duration',
                        '-webkit-animation-duration',
                        '-o-animation-duration'
                    ],
                    //"unSupportCss" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
                    //The default setting is to disable the "animsition" in a browser that does not support "animation-duration".

                    overlay: false,

                    overlayClass: 'animsition-overlay-slide',
                    overlayParentElement: 'body'
                });
            });
        })(jQuery);

    </script>
    <script type="text/javascript" src="{{ asset('/js/styleswitcher.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/custom-shop-home-1.js') }}"></script>
    <!-- End Document
================================================== -->
@endsection
