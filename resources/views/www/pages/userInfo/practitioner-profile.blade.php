@extends(substr($_SERVER['HTTP_HOST'],0,7)=='dev-vms'?'vms.layouts.main':(substr($_SERVER['HTTP_HOST'],0,7)=='dev-www'?'www.layouts.main':'pharma.layouts.main'))
{{--@section('breadcrumbs')--}}
{{--    <div class="medlab_breadcrumbs_wrapper">--}}
{{--        <div class="container" style="width: unset;background-color:#7AA43F;">--}}
{{--            <ol class="breadcrumb medlab_breadcrumbs" style="margin-bottom: unset;background-color:#7AA43F;">--}}
{{--            </ol>--}}
{{--        </div>--}}
{{--    </div>--}}

{{--    <div class="medlab_breadcrumbs_wrapper">--}}
{{--        <div class="container" style="width: unset">--}}
{{--            <ol class="breadcrumb medlab_breadcrumbs">--}}
{{--                <li><a class="medlab_breadcrumbs_link" href="/">@lang('trs.home')</a></li>--}}
{{--                <li><a class="medlab_breadcrumbs_link" href="/research">@lang('trs.research')</a></li>--}}
{{--                <li class="active medlab_breadcrumbs_text">@lang('trs.patents')</li>--}}
{{--            </ol>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--@endsection--}}
@section('content')


    <main class="cd-main-content" id="main" style="margin-top:90px">

        <section class="section grey-section section-padding-top">
            <div class="container">
                <div class="sixteen columns" data-scroll-reveal="enter left move 200px over 1s after 0.3s">

                </div>
                    <form name="ajax-form" id="ajax-form" action="{{ route('update_practitioner_detail') }}"
                          method="post">
                        @csrf
                        <div class="sixteen columns"
                             data-scroll-reveal="enter right move 200px over 1s after 0.3s">
                            <div class="services-boxes-1" style="cursor: pointer; width:100%">
                                <div class="icon-box">&#xf007;</div>
                                <h6>PRACTITIONER</h6>
                                <br>
                            </div>
                        </div>
                        <h4 style="text-align: left;">My Business Information</h4>
                        <div class="row">

                            <label for="practitioner_international" class="uname"
                                   style="display:inline-block;">
                                Show International</label>
                            <input id="practitioner_international" type="checkbox"
                                   class="form-control"
                                   name="practitioner_international"
                                   style="display:inline-block;width: unset"/>
                        </div>

                        <div class="row">
{{--                            <input type="text" name="id" value={{ Auth::guard('practitioner')->user()->customer_code }} hidden/>--}}
                            <label for="practitioner_street_address" class="uname required"
                                   style="width: 100%;">
                                Street Address</label>
                            <input id="practitioner_street_address" type="text"
                                   class="form-control"
                                   name="address_line_1" value="{{$role->practitioner()->first()->address()->first()->address_line_1 }}"
                                   onFocus="stopAutoFillPractitioner()" required/>

                        </div>
                        <div class="row">
                            <label for="practitioner_second_address" class="uname" style="width: 100%;">
                                A second line if you need (optional)</label>
                            <input id="practitioner_second_address" type="text"
                                   class="form-control"
                                   name="address_line_2" value="{{ $role->practitioner()->first()->address()->first()->address_line_2}}"
                                   autocomplete="googleignoreautofill"/>
                        </div>
                        <div class="row" style="margin-bottom: unset;">
                            <label for="practitioner_locality" class="uname required"
                                   style="width: 33.3%;float: left">Suburb/Postcode</label>
                            <label for="practitioner_administrative_area_level_1" class="uname"
                                   style="width: 33.3%;float: left">
                                &nbsp;</label>
                            <label for="practitioner_postal_code" class="uname"
                                   style="width: 33.3%;float: left">
                                &nbsp;</label>
                        </div>

                        <div class="row" style="position:relative" id="locality_row">

                            <input id="practitioner_locality" type="text"
                                   class="form-control"
                                   name="suburb" value="{{$role->practitioner()->first()->address()->first()->suburb}}"
                                   style="width:100%;float: left;position: absolute" />

                            <input id="practitioner_administrative_area_level_1" type="text"
                                   class="form-control"
                                   name="state"
                                   value="{{$role->practitioner()->first()->address()->first()->state}}" autocomplete="googleignoreautofill"
                                   style="width: 33.3%;float: left;margin-left: 33.3%" readonly>

                            <input id="practitioner_postal_code" type="number"
                                   class="form-control"
                                   name="postcode" style="width: 33.3%;float: left"
                                   value="{{$role->practitioner()->first()->address()->first()->postcode}}"
                                   autocomplete="googleignoreautofill" readonly>

                        </div>
                        <div class="row">

                            <label for="practitioner_country" class="uname" style="width: 100%;">
                                Country</label>
                            <input id="practitioner_country" type="text"
                                   class="form-control"
                                   name="country" value="{{$role->practitioner()->first()->address()->first()->country}}"
                                   autocomplete="googleignoreautofill" readonly/>
                        </div>
                        <br>
                        <div class="alert alert-yelow">
                            <p><span>To change business name or ABN please contact</span><a href="mailto:accounts@medlab.co">accounts@medlab.co</a></p>
                        </div>
                        <div class="row">
                            <input type="text" name="id" value={{ Auth::guard('practitioner')->user()->id }} hidden/>
                            <label for="business_name" class="uname" style="width: 100%">
                                Clinic/Business Name</label>
                            <input id="business_name" type="text"
                                   class="form-control"
                                   name="business_name"
                                   value="{{$role->practitioner()->first()->customer_name}}" readonly
                                   style="text-transform: unset;width: 100%"
                                   onFocus="stopAutoFillPractitioner()"/>

                        </div>

                        <div id="practitionerBusinessInfo">
                            <div class="row">
                                <label for="business_type" class="uname" id="business_type_label"
                                       style="width: 100%"> Business
                                    Type</label>
                                <select id="business_type" type="text"
                                        class="form-control"
                                        name="business_type"
                                        style="text-transform: unset;width: 100%">
                                    <option @if( $role->practitioner()->first()->business_type=='Company') selected @endif>Company</option>
                                    <option @if( $role->practitioner()->first()->business_type=='Partnership') selected @endif>Partnership</option>
                                    <option @if( $role->practitioner()->first()->business_type=='Sole Enterprise/Trade') selected @endif>Sole Enterprise/Trade</option>
                                readonly </select>
                            </div>

                            <div class="row">
                                <label for="business_number" class="uname"
                                       style="width: 50%;float:left;margin-right: 1%"> ABN
                                </label>

                                <label for="acn" class="uname" style="width: 49%;float:right;">ACN</label>
                                <input id="business_number" type="number"
                                       class="form-control"
                                       name="abn_number"
                                       value="{{$role->practitioner()->first()->gstvat_number}}"
                                       style="text-transform: unset;width: 50%;float:left;" readonly/>
                                <input id="acn" type="text"
                                       class="form-control"
                                       name="acn"
                                       value="{{ $role->practitioner()->first()->acn}}"
                                       style="text-transform: unset;width: 49%;float:right;" readonly/>

                            </div>
                            <div class="sixteen columns">

                            </div>
                            <br>
                            <br>
                        <div class="row">
                            <label for="primary_channel" class="uname" style="width: 50%;float:left;margin-right: 1%">Business
                                Primary Channel</label>
                            <label for="second_channel" class="uname"
                                   style="width: 49%;float:right;"> Business Second Channel</label>
                            <select id="primary_channel" type="text"
                                    class="form-control"
                                    name="primary_channel"
                                    style="text-transform: unset;width: 50%;float:left;">
                               @foreach($primary_channel as $item)
                                    <option @if($role->practitioner()->first()->channel_id==$item->id) selected @endif> {{ $item->name }}</option>
                               @endforeach
                            </select>
                            <select id="second_channel" type="text"
                                    class="form-control"
                                    name="second_channel"
                                    style="text-transform: unset;width: 49%;float:right;">
                               @foreach($second_channel as $item)
                                   <option @if($role->practitioner()->first()->secondary_channel_id==$item->id) selected @endif>{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>

                            <br>
                            <br>


                            <br>
                            <br>
                            <h4>My Personal Details</h4>
                            <br>

                            <input type="text" name="id" value={{ Auth::guard('practitioner')->user()->id }} hidden/>
                            <div class="row">
                                <label for="practitioner_title" class="uname required">
                                    Title</label>
                                <select id="practitioner_title" type="text"
                                        class="form-control"
                                        name="salutation" required
                                        style="text-transform: unset;width: 49%;">
                                    <option></option>
                                    <option @if($role->salutation=='Mr') selected @endif>Mr</option>
                                    <option @if($role->salutation=='Ms') selected @endif>Ms</option>
                                    <option @if($role->salutation=='Miss') selected @endif>Miss</option>
                                    <option @if($role->salutation=='Dr.') selected @endif>Dr.</option>
                                    <option @if($role->salutation=='Prof.') selected @endif>Prof.</option>
                                </select>
                            </div>
                            <div class="row">
                                <label for="practitioner_firstname" class="uname required"> First
                                    Name</label>
                                <input id="practitioner_firstname" type="text"
                                       class="form-control"
                                       name="first_name" required
                                       value="{{$role->first_name}}"
                                       style="text-transform: unset;" onFocus="stopAutoFillPractitioner()">
                            </div>
                            <div class="row">
                                <label for="practitioner_lastname" class="uname required"> Last Name</label>
                                <input id="practitioner_lastname" type="text"
                                       class="form-control"
                                       name="last_name" required
                                       value="{{$role->last_name}}"
                                       style="text-transform: unset;">
                            </div>
                            <div class="row">
                                <label for="practitioner_email" class="uname required"> Your email</label>
                                <input id="practitioner_email" type="email"
                                       class="form-control"
                                       name="email" required
                                       value="{{$role->email}}"
                                       autocomplete="email" autofocus style="text-transform: unset;">
                            </div>
                        <div class="row">

                            <label for="practitioner_phone" class="uname required" style="width: 100%;">Telephone(Enter at least one number)</label>
                            <input id="practitioner_phone" type="number" name="phone" value="{{$role->phone }}"/>
                        </div>

                        <div class="row">

                            <label for="practitioner_mobile" class="uname" style="width: 100%;">Mobile</label>
                            <input id="practitioner_mobile" type="number" name="mobile" value="{{$role->mobile }}"/>
                        </div>
                            <br>

                            <h4>My Professional Details</h4>
                            <br>

                            <div class="row">
                                <label for="primary_profession" class="uname required"
                                       style="width: 50%;float:left;margin-right: 1%"> Primary
                                    Profession</label>
                                <label for="job_title" class="uname required"
                                       style="width: 49%;float:right;"> Job Title</label>
                                <select id="primary_profession" type="text"
                                        class="form-control"
                                        name="primary_profession"
                                        style="text-transform: unset;width: 50%;float:left;" required>
                                    @foreach($modality as $item)
                                        <option @if($role->modalityperson()->exists()) @if ($role->modalityperson()->first()->name==$item->name) selected @endif @endif> {{$item->name}}</option>
                                    @endforeach
                                </select>
                                <input id="job_title" type="text"
                                       class="form-control"
                                       name="job_title"
                                       value="{{$role->job_title}}" required
                                       style="text-transform: unset;width: 49%;float:right;">

                            </div>
                            <div class="row">
                                <label for="association_name" class="uname required" style="width: 50%;float:left;margin-right: 1%"> Association
                                    Name</label>
                                <label for="association_number" class="uname required"
                                       style="width: 49%;float:right;"> Association Number</label>
                                <select id="association_name" type="text"
                                        class="form-control"
                                        name="association_name"
                                        style="text-transform: unset;width: 49%;float:left;">
                                    @foreach($associations as $item)
                                        <option @if ($role->association_id == $item->id) selected @endif value="{{ $item->id }}">{{ $item->name }}</option>
                                    @endforeach
                                </select>
                                <input id="association_number" type="number"
                                       class="form-control"
                                       name="association_number"
                                       value="{{$role->association_number}}" required
                                       style="text-transform: unset;width: 49%;float:right;">
                            </div>

                        <div class="row">
                            <label for="business_phone" class="uname required" style="width: 100%">
                                Telephone</label>

                            <input id="business_phone" type="tel"
                                   class="form-control"
                                   name="business_phone"
                                   value="{{$role->practitioner()->first()->phone_number }}"
                                   style="text-transform: unset;" required >
                        </div>

                        <div class="row">

                                <label for="fax" class="uname" style="width: 100%"> Fax</label>

                                <input id="fax" type="tel"
                                       class="form-control"
                                       name="fax_number"
                                       value="{{$role->practitioner()->first()->fax_number}}"
                                       style="text-transform: unset;">

                            </div>


                        <div class="sixteen columns">
                            <div id="button-con">
                                <button class="send_message" name="practitioner_information"
                                        type="submit">update
                                </button>

                            </div>

                        </div>
            </div>
                    </form>
            </div>
        </section>
        {{--<div class="container">--}}
        {{--    <div class="row justify-content-center">--}}
        {{--        <div class="col-md-8">--}}
        {{--            <div class="card">--}}
        {{--                <div class="card-header">{{ __('Reset Password') }}</div>--}}

        {{--                <div class="card-body">--}}
        {{--                    @if (session('status'))--}}
        {{--                        <div class="alert alert-success" role="alert">--}}
        {{--                            {{ session('status') }}--}}
        {{--                        </div>--}}
        {{--                    @endif--}}

        {{--                    <form method="POST" action="{{ route('password.email') }}">--}}
        {{--                        @csrf--}}

        {{--                        <div class="form-group row">--}}
        {{--                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>--}}

        {{--                            <div class="col-md-6">--}}
        {{--                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>--}}

        {{--                                @error('email')--}}
        {{--                                    <span class="invalid-feedback" role="alert">--}}
        {{--                                        <strong>{{ $message }}</strong>--}}
        {{--                                    </span>--}}
        {{--                                @enderror--}}
        {{--                            </div>--}}
        {{--                        </div>--}}

        {{--                        <div class="form-group row mb-0">--}}
        {{--                            <div class="col-md-6 offset-md-4">--}}
        {{--                                <button type="submit" class="btn btn-primary">--}}
        {{--                                    {{ __('Send Password Reset Link') }}--}}
        {{--                                </button>--}}
        {{--                            </div>--}}
        {{--                        </div>--}}
        {{--                    </form>--}}
        {{--                </div>--}}
        {{--            </div>--}}
        {{--        </div>--}}
        {{--    </div>--}}
        {{--</div>--}}
    </main>
    @include(substr($_SERVER['HTTP_HOST'],0,7)=='dev-vms'?'vms.layouts.footer':(substr($_SERVER['HTTP_HOST'],0,7)=='dev-www'?'www.layouts.footer':'pharma.layouts.footer'))

    <div class="scroll-to-top">&#xf106;</div>

    <!-- JAVASCRIPT
    ================================================== -->
    <script type="text/javascript" src="{{ asset('/build/js/intlTelInput.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery-2.1.1.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/modernizr.custom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.mobile.custom.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/retina-1.1.0.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.animsition.min.js') }}"></script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            $(document).ready(function () {
                $('#ajax-form').submit(function (e) {
                     e.preventDefault(); // avoid to execute the actual submit of the form.
                    var form = $(this);
                    var url = form.attr('action');
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: form.serialize(), // serializes the form's elements.
                        success: function (data) {
                            // console.log(data);
                            if (data.msg=='success') {
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Your update was successful!',
                                    confirmButtonText: 'OK',
                                    allowOutsideClick: false
                                }).then((result) => {
                                    if (result.value) {
                                        document.getElementById("ajax-form").submit();
                                        window.location.reload();
                                    }
                                })
                            } else {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Something is wrong, please try again!',
                                    // title: data.msg,
                                })
                            }
                        }
                    });


                })
                $(".animsition").animsition({

                    inClass: 'zoom-in-sm',
                    outClass: 'zoom-out-sm',
                    inDuration: 1500,
                    outDuration: 800,
                    linkElement: '.animsition-link',
                    // e.g. linkElement   :   'a:not([target="_blank"]):not([href^=#])'
                    loading: true,
                    loadingParentElement: 'body', //animsition wrapper element
                    loadingClass: 'animsition-loading',
                    unSupportCss: ['animation-duration',
                        '-webkit-animation-duration',
                        '-o-animation-duration'
                    ],
                    //"unSupportCss" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
                    //The default setting is to disable the "animsition" in a browser that does not support "animation-duration".

                    overlay: false,

                    overlayClass: 'animsition-overlay-slide',
                    overlayParentElement: 'body'
                });
            });
        })(jQuery);

    </script>

    <script type="text/javascript" src="{{ asset('/js/jquery.easing.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.hidescroll.min.js') }}"></script>
    <script type="text/javascript">
        $('.header-top').hidescroll();
    </script>
    <script type="text/javascript" src="{{ asset('/js/smoothScroll.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.parallax-1.1.3.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/owl.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/scrollReveal.js') }}"></script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            window.scrollReveal = new scrollReveal();
        })(jQuery);
    </script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            jQuery(document).ready(function () {
                var offset = 450;
                var duration = 500;
                jQuery(window).scroll(function () {
                    if (jQuery(this).scrollTop() > offset) {
                        jQuery('.scroll-to-top').fadeIn(duration);
                    } else {
                        jQuery('.scroll-to-top').fadeOut(duration);
                    }
                });

                jQuery('.scroll-to-top').click(function (event) {
                    event.preventDefault();
                    jQuery('html, body').animate({scrollTop: 0}, duration);
                    return false;
                })
            });
        })(jQuery);
    </script>
    <script type="text/javascript">
        document.getElementById('primary_channel').addEventListener('change', function () {

            $.ajax({
                type: "POST",
                url: '{{ url('get_channel') }}',
                data: {
                    _token: '{{ csrf_token() }}',
                    parent: this.value,
                },
                timeout: 6000,
                error: function(request,error) {
                    if (error == "timeout") {
                        $('#err-timedout').slideDown('slow');
                    }
                    else {
                        $('#err-state').slideDown('slow');
                        $("#err-state").html('An error occurred: ' + error + '');
                    }
                },
                success: function(data) {
                    $('#second_channel').empty();
                    $.each(data.data, function () {
                        $("#second_channel").append('<option value="' + this + '">' + this + '</option>')
                    })
                }

            });
        });
        document.getElementById('practitioner_email').addEventListener('change', function () {
            $.ajax({
                type: "POST",
                url: '{{ url('email_validation') }}',
                data: {
                    _token: '{{ csrf_token() }}',
                    role: 'practitioner',
                    email: this.value,
                },
                timeout: 6000,
                success: function(data) {
                    if(data.code=='0'){
                        document.getElementById('practitioner_email').value = "";
                        Swal.fire({
                            icon: 'info',
                            html: data.email+ ' already exists. Please <a href="#" onClick="open_login()" color="#6ba53a">login</a> or <a href="/password/reset" color="#6ba53a">reset your password.</a>'
                        })

                    }
                }

            });
        });
        {{--document.getElementById('referral_code').addEventListener('change', function () {--}}
        {{--    $.ajax({--}}
        {{--        type: "POST",--}}
        {{--        url: '{{ url('referral_check') }}',--}}
        {{--        data: {--}}
        {{--            _token: '{{ csrf_token() }}',--}}
        {{--            code: this.value,--}}
        {{--        },--}}
        {{--        timeout: 6000,--}}
        {{--        success: function(data) {--}}
        {{--            if(data.code=='1'){--}}
        {{--                Swal.fire({--}}
        {{--                    icon: 'question',--}}
        {{--                    text: 'Please confirm that your Practitioner is "'+data.top+'"?',--}}
        {{--                    showCancelButton: true,--}}
        {{--                    allowOutsideClick: false,--}}
        {{--                    confirmButtonText: 'Yes',--}}
        {{--                    reverseButtons: true,--}}

        {{--                }).then((result)=>{--}}
        {{--                    if(!result.value){--}}
        {{--                        document.getElementById('referral_code').value = "";--}}
        {{--                    }--}}
        {{--                })--}}
        {{--            }else if(data.code=='0'){--}}
        {{--                Swal.fire({--}}
        {{--                    icon: 'error',--}}
        {{--                    title: 'The code you have entered does not match our records. Please contact your Practitioner to confirm' ,--}}
        {{--                })--}}
        {{--                document.getElementById('referral_code').value = "";--}}
        {{--            }--}}
        {{--        }--}}

        {{--    });--}}
        {{--});--}}

        document.getElementById('business_phone').addEventListener('change', function () {
            $.ajax({
                type: "POST",
                url: '{{ url('phone_update_check') }}',
                data: {
                    _token: '{{ csrf_token() }}',
                    phone: this.value,
                },
                timeout: 6000,
                success: function(data) {
                    var business = JSON.parse(data.business);
                    var pre_phone = JSON.parse(data.pre_phone);
                    if(data.code=='1'){
                        Swal.fire({
                            icon: 'info',
                            text: 'The phone entered has an existing business "'+business['customer_name']+'" in our database. Continue with this update? "',
                            showCancelButton: true,
                            allowOutsideClick: false,
                            // confirmButtonText: 'Link to existing',
                            // cancelButtonText: 'New Account',
                            reverseButtons: true

                        }).then((result)=>{
                            if(!result.value){

                                document.getElementById('business_phone').value = pre_phone['phone_number'];

                            }
                        })
                    }
                }

            });
        });

        document.getElementById('fax').addEventListener('change', function () {
           // console.log(this.value);
            $.ajax({
                type: "POST",
                url: '{{ url('update_fax_check') }}',

                data: {
                    _token: '{{ csrf_token() }}',
                    fax: this.value,
                },
                timeout: 6000,
                success: function(data) {
                    var business = JSON.parse(data.business);
                    var pre_fax = JSON.parse(data.pre_fax);
                    console.log(pre_fax);
                    if(data.code=='1'){
                        Swal.fire({
                            icon: 'info',
                            text: 'The fax entered has an existing business "'+business['customer_name']+'" in our database. Continue with this update?"',
                            showCancelButton: true,
                            allowOutsideClick: false,
                            // confirmButtonText: 'Link to existing',
                            // cancelButtonText: 'New Account',
                            reverseButtons: true

                        }).then((result)=>{
                            if(!result.value){
                                document.getElementById('fax').value = pre_fax['fax_number'];
                            }
                        })
                    }
                }

            });
        });

        function stopAutoFillPractitioner() {
            document.getElementById('practitioner_street_address').removeAttribute('autocomplete');
            document.getElementById('practitioner_street_address').setAttribute('autocomplete', 'googleignoreautofill');
            document.getElementById('practitioner_locality').removeAttribute('autocomplete');
            document.getElementById('practitioner_locality').setAttribute('autocomplete', 'googleignoreautofill');
        }

        document.getElementById('practitioner_street_address').addEventListener("beforeinput", function () {
            $("#practitioner_locality_error").remove();
            // document.getElementById('street_address').value = '';
            document.getElementById('practitioner_locality').value = '';
            document.getElementById('practitioner_administrative_area_level_1').value = '';
            document.getElementById('practitioner_country').value = '';
            document.getElementById('practitioner_postal_code').value = '';
        })
        document.getElementById('practitioner_locality').addEventListener("input", function () {
            $("#practitioner_locality_error").remove();
            document.getElementById('practitioner_administrative_area_level_1').value = '';
            document.getElementById('practitioner_country').value = '';
            document.getElementById('practitioner_postal_code').value = '';
        })

        var practitioner_international = document.querySelector("input[name=practitioner_international]");

        google.maps.event.addDomListener(window, 'load', function () {

            var places = new google.maps.places.Autocomplete(document
                .getElementById('practitioner_street_address'), {
                componentRestrictions: {
                    country: "au",
                }
            });
            practitioner_international.addEventListener('change', function () {
                if (this.checked) {
                    places.setComponentRestrictions({'country': []});
                } else {
                    places.setComponentRestrictions({'country': 'au'});
                }
            });
            addGoogleListenerTotal('practitioner', places);


        });
        google.maps.event.addDomListener(window, 'load', function () {

            var locality = new google.maps.places.Autocomplete((document
                .getElementById('practitioner_locality')), {
                types: ['(regions)'],
                componentRestrictions: {
                    country: "au",
                }
            });
            practitioner_international.addEventListener('change', function () {
                if (this.checked) {
                    locality.setComponentRestrictions({'country': []});
                } else {
                    locality.setComponentRestrictions({'country': 'au'});
                }
            });
            addGoogleListenerLocality('practitioner', locality);


        });

        function addGoogleListenerLocality(role, locality) {
            google.maps.event.addListener(locality, 'place_changed', function () {
                var place = locality.getPlace();
                // var value = place.address_components;

                // var  value = address.split(",");
                // console.log(place.address_components)
                console.log(place.address_components);
                var componentForm = {
                    // street_number: 'short_name',
                    // route: 'long_name',
                    locality: 'long_name',
                    administrative_area_level_1: 'short_name',
                    country: 'long_name',
                    postal_code: 'short_name'
                };
                // document.getElementById('street_address').value = '';

                // document.getElementById('administrative_area_level_1').value = '';
                // document.getElementById('country').value = '';
                // document.getElementById('postal_code').value = '';
                var a=0;
                for (var i = 0; i < place.address_components.length; i++) {
                    var addressType = place.address_components[i].types[0];
                    if (componentForm[addressType]) {
                        var val = place.address_components[i][componentForm[addressType]];
                        if (addressType == 'street_number') {
                            document.getElementById(role + '_' + 'street_address').value = val;
                        } else if (addressType == 'route') {
                            document.getElementById(role + '_' + 'street_address').value += (' ' + val);
                        } else if (addressType == 'locality') {
                            document.getElementById(role + '_' + 'locality').value =val;
                            a=1;
                        } else {
                            document.getElementById(role + '_' + addressType).value = val;
                        }
                    }
                }
                if(a==0){
                    document.getElementById(role + '_' + 'locality').value ='';
                }
                if(document.getElementById(role + '_' + 'locality').value=='' ||
                    document.getElementById(role + '_' + 'administrative_area_level_1').value=='' ||
                    document.getElementById(role + '_' + 'country').value==''){
                    $('#'+role+'_locality_row').append('<div class="alert alert-red" style="margin-top: 40px" id='+role+'_locality_error><p>PLEASE SELECT ANOTHER SUBURB WITH A POSTCODE</p></div>')
                }
            });
        }

        function addGoogleListenerTotal(role, places) {

            google.maps.event.addListener(places, 'place_changed', function () {
                console.log(111);
                var place = places.getPlace();
                // var value = place.address_components;

                // var  value = address.split(",");
                // console.log(place.address_components)
                var componentForm = {
                    street_number: 'short_name',
                    route: 'long_name',
                    locality: 'long_name',
                    administrative_area_level_1: 'short_name',
                    country: 'long_name',
                    postal_code: 'short_name'
                };

                for (var i = 0; i < place.address_components.length; i++) {
                    var addressType = place.address_components[i].types[0];
                    if (componentForm[addressType]) {
                        var val = place.address_components[i][componentForm[addressType]];
                        if (addressType == 'street_number') {
                            document.getElementById(role + '_' + 'street_address').value = val;
                        } else if (addressType == 'route') {
                            document.getElementById(role + '_' + 'street_address').value += (' ' + val);
                        } else {
                            document.getElementById(role + '_' + addressType).value = val;
                        }
                    }
                }
                if(document.getElementById(role + '_' + 'locality').value=='' ||
                    document.getElementById(role + '_' + 'administrative_area_level_1').value=='' ||
                    document.getElementById(role + '_' + 'country').value==''){
                    $('#'+role+'_locality_row').append('<div class="alert alert-red" style="margin-top: 40px" id='+role+'_locality_error><p>PLEASE SELECT ANOTHER SUBURB WITH A POSTCODE</p></div>')
                }
                if(role=='practitioner'){
                    console.log(document.getElementById(role + '_' + 'street_address').value);
                    $.ajax({
                        type: "POST",
                        url: '{{ url('address_update_check') }}',
                        data: {
                            _token: '{{ csrf_token() }}',
                            address: document.getElementById(role + '_' + 'street_address').value,
                        },
                        timeout: 6000,

                        success: function(data) {
                            var business = JSON.parse(data.business);
                            var pre_address = JSON.parse(data.pre_address);
                            if(data.code=='1'){
                                Swal.fire({
                                    icon: 'info',
                                    text: 'The address entered has an existing business "'+business['customer_name']+'" in our database. Continue with this update? "',
                                    showCancelButton: true,
                                    allowOutsideClick: false,
                                    // confirmButtonText: 'Cancel',
                                    // cancelButtonText: 'Yes',
                                    reverseButtons: true

                                }).then((result)=>{
                                    if(!result.value){
                                        document.getElementById('practitioner_street_address').value = pre_address['address_line_1'];
                                        document.getElementById('practitioner_second_address').value = pre_address['address_line_2'];
                                        document.getElementById('practitioner_locality').value = pre_address['suburb'];
                                        document.getElementById('practitioner_administrative_area_level_1').value = pre_address['state'];
                                        document.getElementById('practitioner_postal_code').value = pre_address['postcode'];
                                        document.getElementById('practitioner_country').value = pre_address['country'];

                                    }
                                })
                            }
                        }

                    });
                }
            });

        }

        function submitPractitionerForm() {
            document.getElementById('practitioner_phone').value = int_practitioner_phone.getNumber();
            document.getElementById('fax').value = int_fax.getNumber();
            return true;

        }

        var practitioner_phone = document.querySelector("#practitioner_phone");
        var practitioner_mobile = document.querySelector("#practitioner_mobile");
        var business_phone = document.querySelector("#business_phone");
        var fax = document.querySelector("#fax");
        var int_practitioner_phone = window.intlTelInput(practitioner_phone, {
            // allowDropdown: true,
            // autoHideDialCode: false,
            // autoPlaceholder: "off",
            // dropdownContainer: document.body,
            // excludeCountries: ["us"],
            // formatOnDisplay: false,
            // geoIpLookup: function(callback) {
            //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
            //     var countryCode = (resp && resp.country) ? resp.country : "";
            //     callback(countryCode);
            //   });
            // },
            // hiddenInput: "full_number",
            initialCountry: "au",
            // localizedCountries: { 'de': 'Deutschland' },
            // nationalMode: false,
            // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
            // placeholderNumberType: "MOBILE",
            // preferredCountries: ['cn', 'jp'],
            separateDialCode: true,
            utilsScript: "{{ asset('build/js/utils.js') }}",
        });
        var int_practitioner_mobile = window.intlTelInput(practitioner_mobile, {
            initialCountry: "au",
            separateDialCode: true,
            utilsScript: "{{ asset('build/js/utils.js') }}",
        });
        var int_business_phone = window.intlTelInput(business_phone, {
            initialCountry: "au",
            separateDialCode: true,
            utilsScript: "{{ asset('build/js/utils.js') }}",
        });
        var int_fax = window.intlTelInput(fax, {
            initialCountry: "au",
            separateDialCode: true,
            utilsScript: "{{ asset('build/js/utils.js') }}",
        });
    </script>

    <script type="text/javascript" src="{{ asset('/js/visible.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/pro-bars.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.counterup.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/waypoints.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/smk-accordion.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/styleswitcher.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/custom-about-1.js') }}"></script>

    <link rel="stylesheet" type="text/css" href="{{ asset('/DataTables/datatables.min.css') }}"/>

    <script type="text/javascript" src="{{ asset('/DataTables/datatables.min.js') }}"></script>

    <!-- End Document
    ================================================== -->
@endsection


