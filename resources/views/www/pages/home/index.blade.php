﻿@extends('www.layouts.main')
@section('title')
    Medlab
@endsection
@section('description')
    @if($content->meta_description)
        {{ $content->meta_description }}
    @elseif(json_decode($content->content))
        @foreach(json_decode($content->content) as $item)
            @if($item->layout=='text')
                {{ substr(str_replace('&nbsp;',' ',strip_tags($item->attributes->content)),0,160) }}
            @endif
        @endforeach
    @endif
@endsection
@section('content')
    <!-- HOME SECTION
    ================================================== -->

    <main class="cd-main-content" id="main" style="margin-top:45px">


        <!-- SLIDER IMAGE
================================================== -->
        @if(json_decode($content->slider))
            @foreach(json_decode($content->slider) as $slider)
                @if($slider->layout=='video')
                    <div class="containerHomepage">
                        <div class="textHomepage">
                            <iframe width=100% height=100%
                                    src="https://player.vimeo.com/video/{{ $slider->attributes->video_link }}?muted=1&autoplay=1&loop=1&autopause=0"
                                    allow="autoplay; fullscreen" allowfullscreen></iframe>
                        </div>
                    </div>

                @elseif($slider->layout=='image')

                    <section class="home">
                        <div class="slider-container">
                            <div class="tp-banner-container">
                                <div class="tp-banner">
                                    <ul>
                                        <li data-transition="fade" data-slotamount="1" data-masterspeed="500"
                                            data-saveperformance="on" data-title="Intro Slide">
                                            <img
                                                src="{{ $image_url }}/www/Images/{{ \App\Models\MediaLibrary::where('id',$slider->attributes->slider_image)->first()->name }}"
                                                alt="{{ $slider->attributes->slider_alt }}"
                                                data-lazyload="{{ $image_url }}/www/Images/{{ \App\Models\MediaLibrary::where('id',$slider->attributes->slider_image)->first()->name }}"
                                                data-bgposition="center top" data-bgfit="cover"
                                                data-bgrepeat="no-repeat">
                                            <a href="#">
                                                <div class="black-heavy-3">
                                                    <div class="black-heavy-3-heading">
                                                        <h1>{{ $slider->attributes->title }}</h1></div>
                                                    <div
                                                        class="black-heavy-3-subheading">{{ $slider->attributes->subtitle }}</div>
                                                </div>
                                            </a>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </section>
                @endif
            @endforeach
        @else
            <br>
        @endif

    <!-- CONTENT SECTION
    ================================================== -->
        @if(json_decode($content->content))
            <section class="section white-section section-padding-top" id="scroll-link">
                <div class="container">
                    <div class="sixteen columns remove-bottom">
                        <div class="full-image">
                            <div class="articleClass padding">
                                @foreach(json_decode($content->content) as $item)
                                    @if($item->layout=='text')
                                        {!! $item->attributes->content !!}
                                    @else
                                        <div style="width: 100%;">
                                            <iframe src="{{ $item->attributes->content }}?transparent=false"
                                                    width="100%" height="500"
                                                    frameborder=”0″ webkitallowfullscreen mozallowfullscreen
                                                    allowfullscreen></iframe>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        @endif


    <!-- SUBWEBS
        ================================================== -->

        <section class="section grey-section section-padding-top-bottom">
            <div class="container">

                <div class="clear"></div>
                <div id="projects-grid">

                @foreach($subweb as $item)

                        <div class="eight columns">
                            <a target="_blank" href="{{ $item->link }}" class="animsition-link">
                            <div class="portfolio-box-2 grey-section">
                                <a href="{{ $item->link }}" class="animsition-link"><div class="mask-left">&#xf0c1;</div></a>
                                <a class="group1" href="{{ $item->link }}"><div class="mask-right">&#xf0b2;</div></a>
                                <img src="{{ $image_url }}/www/Images/{{ $item->image }}" alt="{{ $item->alt }}" width = "100%">
                                <h3>{{ $item->title }}</h3>
                                <p>{{ $item->introduction }}</p>
                            </div>
                            </a>
                        </div>

                @endforeach
                </div>

            </div>
        </section>

        <!-- RECENT ARTICLES
            ================================================== -->

        @if(count($recent_articles))


            <section class="section white-section section-padding-top-bottom">
                <div class="container">

                    <h2 style="text-align: left;">Recent Articles</h2>
                    <div class="blog-wrapper">
                        <div id="blog-grid-masonry">
                            @foreach($recent_articles as $item)
                                @if($item->article_type=='link')
                                    <a href={{ route('www.articles_content',['id' => preg_replace('/--+/','-',preg_replace('/[^A-Za-z0-9\-]/', '-', $item->title)).'-'.$item->id]) }}>
                                        <div class="blog-box-3">
                                            <div class="blog-box-1 link-post grey-section">
                                                <h4 class="two-row-max"
                                                    style="min-height: 96px;">{{ $item->title }}</h4>
                                                <p class="four-row-max"
                                                   style="padding-bottom: unset;min-height: 92px;">{{ $item->preview_text }}</p>
                                                <br>
                                                <p>{{ substr($item->date,0,10) }}</p>
                                                @if(isset($item->author))<p>Author: {{ $item->author }}</p>@endif
                                                <div class="link">&#xf178;</div>
                                            </div>
                                        </div>
                                    </a>
                                @elseif($item->article_type=='video')
                                    <a href={{ route('www.articles_content',['id' => preg_replace('/--+/','-',preg_replace('/[^A-Za-z0-9\-]/', '-', $item->title)).'-'.$item->id]) }}>
                                        <div class="blog-box-3">
                                            <div class="blog-box-1 grey-section">
                                                @if(($item->req_login && !(Auth::guard('practitioner')->check()||Auth::guard('patient')->check())))
                                                    <div style="max-height: 270px; position:relative; padding-bottom: 56.25%; height: 0; opacity: 0.85;background: black; ">
                                                        <iframe
                                                            srcdoc="<p style='text-align:center;margin-top:25%;font-size:xx-large;color:white'>Please login to view this video</p>"
                                                            width="560" height="349" src="{{ $item->video_link }}" style = "position: absolute; top: 0; left: 0; width: 100%; height: 100%;"
                                                            allowfullscreen></iframe>
                                                    </div>
                                                @else
                                                    <div style="max-height: 270px; position:relative; padding-bottom: 56.25%; height: 0;">
                                                        <iframe width="560" height="349" src="{{ $item->video_link }}" style = "position: absolute; top: 0; left: 0; width: 100%; height: 100%;"
                                                                allowfullscreen></iframe>
                                                    </div>
                                                @endif
                                                <h4 class="two-row-max"
                                                    style="min-height: 96px;">{{ $item->title }}</h4>
                                                <p class="four-row-max"
                                                   style="padding-bottom: unset;min-height: 92px;">{{ $item->preview_text }}</p>
                                                <br>
                                                <p>{{ substr($item->date,0,10) }}</p>
                                                @if(isset($item->author))<p>Author: {{ $item->author }}</p>@endif
                                                <div class="link">&#xf178;</div>
                                            </div>
                                        </div>
                                    </a>
                                @else
                                    <a href={{ route('www.articles_content',['id' => preg_replace('/--+/','-',preg_replace('/[^A-Za-z0-9\-]/', '-', $item->title)).'-'.$item->id]) }}>
                                        <div class="blog-box-3">
                                            <div class="blog-box-1 grey-section">
                                                <div style="max-height: 270px; position:relative; padding-bottom: 56.25%; height: 0;">
                                                    <img src="{{ $image_url }}/www/Images/{{ $item->title_img }}"
                                                         alt="{{ $item->title_alt }}" style="margin-left: auto;
                                                 margin-right: auto; position: absolute; top: 0; left: 0; width: 100%; height: 100%;">
                                                </div>
                                                <h4 class="two-row-max"
                                                    style="min-height: 96px;">{{ $item->title }}</h4>
                                                <p class="four-row-max"
                                                   style="padding-bottom: unset;min-height: 92px;">{{ $item->preview_text }}</p>
                                                <br>
                                                <p>{{ substr($item->date,0,10) }}</p>
                                                @if(isset($item->author))<p>Author: {{ $item->author }}</p>@endif
                                                <div class="link">&#xf178;</div>
                                            </div>
                                        </div>
                                    </a>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </section>

        @endif
        @if(count($highlight_article))


            <section class="section grey-section section-padding-top-bottom">
                <div class="container">

                    <h2 style="text-align: left;">Highlight Articles</h2>
                    <div class="blog-wrapper">
                        <div id="blog-grid">
                            @foreach($highlight_article as $item)
                                @if($item->article_type=='link')
                                    <a href={{ route('www.articles_content',['id' => preg_replace('/--+/','-',preg_replace('/[^A-Za-z0-9\-]/', '-', $item->title)).'-'.$item->id]) }}>
                                        <div class="blog-box-3">
                                            <div class="blog-box-1 link-post grey-section">
                                                <h4 class="two-row-max"
                                                    style="min-height: 96px;">{{ $item->title }}</h4>
                                                <p class="four-row-max"
                                                   style="padding-bottom: unset;min-height: 92px;">{{ $item->preview_text }}</p>
                                                <br>
                                                <p>{{ substr($item->date,0,10) }}</p>
                                                @if(isset($item->author))<p>Author: {{ $item->author }}</p>@endif
                                                <div class="link">&#xf178;</div>
                                            </div>
                                        </div>
                                    </a>
                                @elseif($item->article_type=='video')
                                    <a href={{ route('www.articles_content',['id' => preg_replace('/--+/','-',preg_replace('/[^A-Za-z0-9\-]/', '-', $item->title)).'-'.$item->id]) }}>
                                        <div class="blog-box-3">
                                            <div class="blog-box-1 grey-section">
                                                @if(($item->req_login && !(Auth::guard('practitioner')->check()||Auth::guard('patient')->check())))
                                                    <div style="max-height: 270px; position:relative; padding-bottom: 56.25%; height: 0; opacity: 0.85;background: black; ">
                                                        <iframe
                                                            srcdoc="<p style='text-align:center;margin-top:25%;font-size:xx-large;color:white'>Please login to view this video</p>"
                                                            width="560" height="349" src="{{ $item->video_link }}" style = "position: absolute; top: 0; left: 0; width: 100%; height: 100%;"
                                                            allowfullscreen></iframe>
                                                    </div>
                                                @else
                                                    <div style="max-height: 270px; position:relative; padding-bottom: 56.25%; height: 0;">
                                                        <iframe width="560" height="349" src="{{ $item->video_link }}" style = "position: absolute; top: 0; left: 0; width: 100%; height: 100%;"
                                                                allowfullscreen></iframe>
                                                    </div>
                                                @endif
                                                <h4 class="two-row-max"
                                                    style="min-height: 96px;">{{ $item->title }}</h4>
                                                <p class="four-row-max"
                                                   style="padding-bottom: unset;min-height: 92px;">{{ $item->preview_text }}</p>
                                                <br>
                                                <p>{{ substr($item->date,0,10) }}</p>
                                                @if(isset($item->author))<p>Author: {{ $item->author }}</p>@endif
                                                <div class="link">&#xf178;</div>
                                            </div>
                                        </div>
                                    </a>
                                @else
                                    <a href={{ route('www.articles_content',['id' => preg_replace('/--+/','-',preg_replace('/[^A-Za-z0-9\-]/', '-', $item->title)).'-'.$item->id]) }}>
                                        <div class="blog-box-3">
                                            <div class="blog-box-1 grey-section">
                                                <div style="max-height: 270px; position:relative; padding-bottom: 56.25%; height: 0;">
                                                    <img src="{{ $image_url }}/www/Images/{{ $item->title_img }}"
                                                         alt="{{ $item->title_alt }}" style="margin-left: auto;
                                                 margin-right: auto; position: absolute; top: 0; left: 0; width: 100%; height: 100%;">
                                                </div>
                                                <h4 class="two-row-max"
                                                    style="min-height: 96px;">{{ $item->title }}</h4>
                                                <p class="four-row-max"
                                                   style="padding-bottom: unset;min-height: 92px;">{{ $item->preview_text }}</p>
                                                <br>
                                                <p>{{ substr($item->date,0,10) }}</p>
                                                @if(isset($item->author))<p>Author: {{ $item->author }}</p>@endif
                                                <div class="link">&#xf178;</div>
                                            </div>
                                        </div>
                                    </a>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </section>

        @endif
        @include('www.layouts.footer')
    </main>

    <div class="scroll-to-top">&#xf106;</div>

    <!-- JAVASCRIPT
    ================================================== -->
    <script type="text/javascript" src="{{ asset('/js/jquery-2.1.1.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/modernizr.custom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.mobile.custom.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/retina-1.1.0.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.animsition.min.js') }}"></script>
    <script type="text/javascript">

        (function ($) {
            "use strict";
            $(document).ready(function () {
                @if (session('login_error'))
                open_login();
                @endif
                // $(".animsition").animsition({
                //
                //     inClass               :   'zoom-in-sm',
                //     outClass              :   'zoom-out-sm',
                //     inDuration            :    1500,
                //     outDuration           :    800,
                //     linkElement           :   '.animsition-link',
                //     // e.g. linkElement   :   'a:not([target="_blank"]):not([href^=#])'
                //     loading               :    true,
                //     loadingParentElement  :   'body', //animsition wrapper element
                //     loadingClass          :   'animsition-loading',
                //     unSupportCss          : [ 'animation-duration',
                //         '-webkit-animation-duration',
                //         '-o-animation-duration'
                //     ],
                //     // "unSupportCss" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
                //     // The default setting is to disable the "animsition" in a browser that does not support "animation-duration".
                //
                //     overlay               :   false,
                //
                //     overlayClass          :   'animsition-overlay-slide',
                //     overlayParentElement  :   'body'
                // });
            });
        })(jQuery);
    </script>

    <script type="text/javascript" src="{{ asset('/js/jquery.themepunch.tools.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.themepunch.revolution.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.easing.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.hidescroll.min.js') }}"></script>
    <script type="text/javascript">
        $('.header-top').hidescroll();
    </script>
    <script type="text/javascript" src="{{ asset('/js/smoothScroll.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.parallax-1.1.3.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.counterup.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/waypoints.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/scrollReveal.js') }}"></script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            window.scrollReveal = new scrollReveal();
        })(jQuery);
    </script>
    <script type="text/javascript" src="{{ asset('/js/owl.carousel.min.js') }}"></script>
    {{--    <script type="text/javascript">--}}
    {{--        (function($) { "use strict";--}}
    {{--            jQuery(document).ready(function() {--}}
    {{--                var offset = 450;--}}
    {{--                var duration = 500;--}}
    {{--                jQuery(window).scroll(function() {--}}
    {{--                    if (jQuery(this).scrollTop() > offset) {--}}
    {{--                        jQuery('.scroll-to-top').fadeIn(duration);--}}
    {{--                    } else {--}}
    {{--                        jQuery('.scroll-to-top').fadeOut(duration);--}}
    {{--                    }--}}
    {{--                });--}}

    {{--                jQuery('.scroll-to-top').click(function(event) {--}}
    {{--                    event.preventDefault();--}}
    {{--                    jQuery('html, body').animate({scrollTop: 0}, duration);--}}
    {{--                    return false;--}}
    {{--                })--}}
    {{--            });--}}
    {{--        })(jQuery);--}}
    {{--    </script>--}}


    <script type="text/javascript" src="{{ asset('/js/styleswitcher.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/custom-shop-home-1.js') }}"></script>


    <!-- End Document
    ================================================== -->
@endsection


