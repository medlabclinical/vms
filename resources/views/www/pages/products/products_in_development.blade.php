@extends('www.layouts.main')
@section('title')
    {{ $productfamily->product_name }}
@endsection
@section('description')
    {{ substr(str_replace('&nbsp;',' ',strip_tags($productfamily->product_description)),0,160) }}
@endsection
@section('breadcrumbs')
    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset;background-color:#7AA43F;">
            <ol class="breadcrumb medlab_breadcrumbs" style="margin-bottom: unset;background-color:#7AA43F;">
            </ol>
        </div>
    </div>

    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset">
            <ol class="breadcrumb medlab_breadcrumbs">
                <li><a class="medlab_breadcrumbs_link" href="/">Home</a></li>
                <li><a class="medlab_breadcrumbs_link" href="{{ route('www.products') }}">Products</a></li>
                <li class="active medlab_breadcrumbs_text">{{ $productfamily->product_name }}</li>
            </ol>
        </div>
    </div>
@endsection
@section('content')




    <main class="cd-main-content" id="main">

        @if($productfamily['in_development']=='1' && !(Auth::guard('practitioner')->check()))

            <section class="section white-section section-padding-bottom" style="margin-top: 120px;padding-bottom: unset">
                <div class="container">
                    <div class="grey-section" style="margin-top: 20px;">
                        <div class="articleClass padding">
                            <h1 style="text-align: left;">{{ $productfamily->product_name }}</h1><br>
                            <p>{{ $productfamily->product_heading }}</p><br>
                            <p>{{ $productfamily->product_byline }}</p>
                            {!! $productfamily->unlisted_drug_description !!}
                        </div>
                        <div
                            class="button-shortcodes text-size-1 text-padding-1 version-1"
                            onClick="open_login()"><span>&#xf18e;</span>
                            Practitioner login to view
                        </div>
                    </div>

                </div>
            </section>
        @else

            <section class="section white-section section-padding-bottom" style="margin-top: 120px;padding-bottom: unset">
                <div class="container">
                    <!-- Product Images -->
                    <div class="twelve columns white-section">
                        <div class="shop-carousel-wrap grey-section" style="position: relative">


                        </div>
                        <div class="grey-section" style="margin-top: 20px;">
                            <div class="articleClass padding">
                                <h2 style="text-align: left;">{{ $productfamily->product_name }}</h2><br>
                                <p>{{ $productfamily->product_heading }}</p><br>
                                <p>{{ $productfamily->product_byline }}</p>
                                {!! $productfamily->product_description !!}
                            </div>
                        </div>
                        @if($productfamily->directions_use)
                            <div class="grey-section" style="margin-top: 20px;">
                                <div class="articleClass padding">
                                    <h2 style="text-align: left;">Directions of Use</h2><br>
                                    {!! $productfamily->directions_use !!}
                                </div>
                            </div>
                        @endif
                        @if($productfamily->reg_permissableindications)
                            <div class="grey-section" style="margin-top: 20px;">
                                <div class="articleClass padding">
                                    <h2 style="text-align: left;">Indications</h2><br>
                                    {!! $productfamily->reg_permissableindications !!}
                                </div>
                            </div>
                        @endif
                        @if($productfamily->warnings)
                            <div class="grey-section" style="margin-top: 20px;">
                                <div class="articleClass padding">
                                    <h2 style="text-align: left;">Warning</h2><br>
                                    {!! $productfamily->warnings !!}
                                </div>
                            </div>
                        @endif
                        @if($productfamily->side_effects)
                            <div class="grey-section" style="margin-top: 20px;">
                                <div class="articleClass padding">
                                    <h2 style="text-align: left;">Side Effects</h2><br>
                                    {!! $productfamily->side_effects !!}
                                </div>
                            </div>
                        @endif
                        @if($productfamily->product_interactions)
                            <div class="grey-section" style="margin-top: 20px;">
                                <div class="articleClass padding">
                                    <h2 style="text-align: left;">Interactions</h2><br>
                                    {!! $productfamily->product_interactions !!}
                                </div>
                            </div>
                        @endif
                    </div>

                    <div class="four columns">
                        <div class="post-sidebar">
                            <h3>Size</h3>
                            <hr style="margin: unset;margin-bottom: 20px;margin-top: 20px;">
                            <h3>Facts</h3>
                            @if($productfamily->ing_patent_information)
                                <div class="alert alert-green">
                                    <p><span>&#xf087;</span>{!! $productfamily->ing_patent_information !!}</p>
                                </div><br>
                            @endif
                            @if($productfamily->store_refrigerated)
                                <div class="alert alert-blue">
                                    <p><span>&#xf129;</span>PRODUCT REQUIRES REFRIGERATION</p>
                                </div><br>
                            @endif
                            <ul class="link-recents" style="line-height: 2em;padding: unset;">
                                Age Suitable: {{ $productfamily->reg_suitable_for_ages_above }}<br>
                                Serving Type: {{ $productfamily->unit_type }}<br>
                            </ul>
                            <hr style="margin: unset;margin-bottom: 20px;margin-top: 20px;">

                            <hr style="margin: unset;margin-bottom: 20px;margin-top: 20px;">
                            <h3>Categories</h3>
                            <ul class="link-recents" style="padding: unset">
                                @foreach($product_group as $a)
                                    <li><a style="color: #6ba53a;word-break: break-word;"
                                           href={{ route('www.products') }}?product_group={{ $a->name }}>{{ $a->name }}</a></li>
                                @endforeach
                            </ul>

                        </div>
                    </div>
                </div>
            </section>


            <section class="section white-section">
                <div class="container">
                    <div class="grey-section" style="margin-top: 20px;">
                        <div class="articleClass padding">
                            <h2 style="text-align: left;">Ingredients</h2><br>
                            {!! $productfamily->ing_first_msg !!}<br>
                            @php
                                session_start();
                                $_SESSION['a']=0;
                                $_SESSION['b']=0;
                            @endphp
                            @foreach($product_family_ingredient as $a)
                                @if(!$a->is_excipient)
                                    @if(($a->product_ingredient()->first()->practitioner_ingredient==''||$a->product_ingredient()->first()->practitioner_ingredient==null)
                                      &&($a->product_ingredient()->first()->consumer_ingredient==''||$a->product_ingredient()->first()->consumer_ingredient==null))
                                        @if($_SESSION['a']==0)
                                            <div class="accordion_in"
                                                 data-scroll-reveal="enter left move 200px over 1s after 0.3s">
                                                @else
                                                    <div class="accordion_in"
                                                         data-scroll-reveal="enter right move 200px over 1s after 0.3s">
                                                        @endif
                                                        <div class="acc_head white-section"
                                                             style="font-weight: unset;padding: 20px;padding-right: 20%">
                                                            {!! $a->product_ingredient()->first()->scientific_name !!}<p
                                                                class="hover-white">
                                                                ({{ $a->product_ingredient()->first()->name }})</p>
                                                            <p class="float-move"
                                                               style="display: block;margin: 0px">{{ $a->quantity }}</p>
                                                            @if($a->equiv_to1)
                                                                <br/><p class="mobile-indent">
                                                                    Equiv. {{ $a->equiv_to1 }}</p>
                                                                <p class="float-move hover-white"
                                                                   style="display: block;margin: 0px">{{ $a->equiv_to1_size }}</p>
                                                            @endif
                                                            @if($a->equiv_to2)
                                                                <br/><p class="mobile-indent">
                                                                    Equiv. {{ $a->equiv_to2 }}</p>
                                                                <p class="float-move hover-white"
                                                                   style="display: block;margin: 0px">{{ $a->equiv_to2_size }}</p>
                                                            @endif
                                                            @if($a->equiv_to3)
                                                                <br/><p class="mobile-indent">
                                                                    Equiv. {{ $a->equiv_to3 }}</p>
                                                                <p class="float-move hover-white"
                                                                   style="display: block;margin: 0px">{{ $a->equiv_to2_size }}</p>
                                                            @endif

                                                        </div>
                                                    </div><br>
                                                    @else
                                                        <div class="accordion">
                                                            @if($_SESSION['a']==0)
                                                                <div class="accordion_in"
                                                                     data-scroll-reveal="enter left move 200px over 1s after 0.3s">
                                                                    @else
                                                                        <div class="accordion_in"
                                                                             data-scroll-reveal="enter right move 200px over 1s after 0.3s">
                                                                            @endif
                                                                            <div class="acc_head white-section hover-white"
                                                                                 style="font-weight: unset;padding-right: 20%">
                                                                                {!! $a->product_ingredient()->first()->scientific_name !!}
                                                                                <p>
                                                                                    ({{ $a->product_ingredient()->first()->name }}
                                                                                    )</p>
                                                                                <p class="float-move"
                                                                                   style="display: block;margin: 0px">{{ $a->quantity }}</p>
                                                                                @if($a->equiv_to1)
                                                                                    <br/><p
                                                                                        class="mobile-indent">
                                                                                        Equiv. {{ $a->equiv_to1 }}</p>
                                                                                    <p class="float-move hover-white"
                                                                                       style="display: block;margin: 0px">{{ $a->equiv_to1_size }}</p>
                                                                                @endif
                                                                                @if($a->equiv_to2)
                                                                                    <br/><p
                                                                                        class="mobile-indent">
                                                                                        Equiv. {{ $a->equiv_to2 }}</p>
                                                                                    <p class="float-move hover-white"
                                                                                       style="display: block;margin: 0px">{{ $a->equiv_to2_size }}</p>
                                                                                @endif
                                                                                @if($a->equiv_to3)
                                                                                    <br/><p
                                                                                        class="mobile-indent">
                                                                                        Equiv. {{ $a->equiv_to3 }}</p>
                                                                                    <p class="float-move hover-white"
                                                                                       style="display: block;margin: 0px">{{ $a->equiv_to2_size }}</p>
                                                                                @endif
                                                                            </div>
                                                                            <div class="acc_content white-section"
                                                                                 style="overflow: hidden">
                                                                                <h4 style="text-align: left;">Consumer
                                                                                    Information</h4>
                                                                                {!! $a->product_ingredient()->first()->consumer_ingredient !!}
                                                                                <br>
                                                                                <h4 style="text-align:left;padding-top: 20px;">
                                                                                    Practitioner Information</h4>
                                                                                @if(Auth::check()))
                                                                                {!! $a->product_ingredient()->first()->practitioner_ingredient !!}
                                                                                @else
                                                                                    <div
                                                                                        class="button-shortcodes text-size-1 text-padding-1 version-1"
                                                                                        onClick="open_login()"><span>&#xf18e;</span>
                                                                                        Please login to view
                                                                                    </div>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                </div>
                                                            @endif
                                                            @endif
                                                            @php
                                                                if($_SESSION['a']==0){$_SESSION['a']=1;} else{$_SESSION['a']=0;};@endphp
                                                            @endforeach

                                                            <br>{!! $productfamily->ing_last_msg !!}<br>
                                                        </div>
                                            </div>

                                            <div class="grey-section" style="margin-top: 20px;">
                                                <div class="articleClass padding">
                                                    <h2 style="text-align: left;">Excipents</h2><br>


                                                    @foreach($product_family_ingredient as $a)
                                                        @if($a->is_excipient)
                                                            @if(($a->product_ingredient()->first()->practitioner_ingredient==''||$a->product_ingredient()->first()->practitioner_ingredient==null)
                                                              &&($a->product_ingredient()->first()->consumer_ingredient==''||$a->product_ingredient()->first()->consumer_ingredient==null))
                                                                @if($_SESSION['b']==0)
                                                                    <div class="accordion_in"
                                                                         data-scroll-reveal="enter left move 200px over 1s after 0.3s">
                                                                        @else
                                                                            <div class="accordion_in"
                                                                                 data-scroll-reveal="enter right move 200px over 1s after 0.3s">
                                                                                @endif
                                                                                <div class="acc_head white-section"
                                                                                     style="font-weight: unset;padding: 20px;padding-right: 20%">
                                                                                    {!! $a->product_ingredient()->first()->scientific_name !!}
                                                                                    <p>
                                                                                        ({{ $a->product_ingredient()->first()->name }}
                                                                                        )</p>
                                                                                    <p class="float-move"
                                                                                       style="display: block;margin: 0px;">{{ $a->quantity }}</p>
                                                                                    @if($a->equiv_to1)
                                                                                        <br/><p class="mobile-indent">
                                                                                            Equiv. {{ $a->equiv_to1 }}</p>
                                                                                        <p class="float-move hover-white"
                                                                                           style="display: block;margin: 0px">{{ $a->equiv_to1_size }}</p>
                                                                                    @endif
                                                                                    @if($a->equiv_to2)
                                                                                        <br/><p class="mobile-indent">
                                                                                            Equiv. {{ $a->equiv_to2 }}</p>
                                                                                        <p class="float-move hover-white"
                                                                                           style="display: block;margin: 0px">{{ $a->equiv_to2_size }}</p>
                                                                                    @endif
                                                                                    @if($a->equiv_to3)
                                                                                        <br/><p class="mobile-indent">
                                                                                            Equiv. {{ $a->equiv_to3 }}</p>
                                                                                        <p class="float-move hover-white"
                                                                                           style="display: block;margin: 0px">{{ $a->equiv_to2_size }}</p>
                                                                                    @endif
                                                                                </div>
                                                                            </div><br>
                                                                            @else
                                                                                <div class="accordion">
                                                                                    @if($_SESSION['b']==0)
                                                                                        <div class="accordion_in"
                                                                                             data-scroll-reveal="enter left move 200px over 1s after 0.3s">
                                                                                            @else
                                                                                                <div class="accordion_in"
                                                                                                     data-scroll-reveal="enter right move 200px over 1s after 0.3s">
                                                                                                    @endif
                                                                                                    <div class="acc_head white-section hover-white"
                                                                                                         style="font-weight: unset;padding-right: 20%">
                                                                                                        {!! $a->product_ingredient()->first()->scientific_name !!}
                                                                                                        <p>
                                                                                                            ({{ $a->product_ingredient()->first()->name }}
                                                                                                            )</p>
                                                                                                        <p class="float-move"
                                                                                                           style="display: block;margin: 0px">{{ $a->quantity }}</p>
                                                                                                        @if($a->equiv_to1)
                                                                                                            <br/><p class="mobile-indent">
                                                                                                                Equiv. {{ $a->equiv_to1 }}</p>
                                                                                                            <p class="float-move"
                                                                                                               style="display: block;margin: 0px">{{ $a->equiv_to1_size }}</p>
                                                                                                        @endif
                                                                                                        @if($a->equiv_to2)
                                                                                                            <br/><p class="mobile-indent">
                                                                                                                Equiv. {{ $a->equiv_to2 }}</p>
                                                                                                            <p class="float-move"
                                                                                                               style="display: block;margin: 0px">{{ $a->equiv_to2_size }}</p>
                                                                                                        @endif
                                                                                                        @if($a->equiv_to3)
                                                                                                            <br/><p class="mobile-indent">
                                                                                                                Equiv. {{ $a->equiv_to3 }}</p>
                                                                                                            <p class="float-move"
                                                                                                               style="display: block;margin: 0px">{{ $a->equiv_to2_size }}</p>
                                                                                                        @endif
                                                                                                    </div>
                                                                                                    <div class="acc_content white-section"
                                                                                                         style="overflow: hidden">
                                                                                                        <h4 style="text-align: left;">Consumer
                                                                                                            Information</h4>
                                                                                                        {!! $a->product_ingredient()->first()->consumer_ingredient !!}
                                                                                                        <br>
                                                                                                        <h4 style="text-align:left;padding-top: 20px;">
                                                                                                            Practitioner Information</h4>
                                                                                                        @if(Auth::check()))
                                                                                                        {!! $a->product_ingredient()->first()->practitioner_ingredient !!}
                                                                                                        @else
                                                                                                            <div
                                                                                                                class="button-shortcodes text-size-1 text-padding-1 version-1"
                                                                                                                onClick="open_login()">
                                                                                                                <span>&#xf18e;</span> Please login to
                                                                                                                view
                                                                                                            </div>
                                                                                                        @endif
                                                                                                    </div>
                                                                                                </div>
                                                                                        </div>
                                                                                    @endif
                                                                                    @endif
                                                                                    @php
                                                                                        if($_SESSION['b']==0){$_SESSION['b']=1;} else{$_SESSION['b']=0;};@endphp
                                                                                    @endforeach
                                                                                </div>
                                                                    </div>

                                                </div>
            </section>
            @if($productfamily->consumer_video)
                <div class="container">
                    <hr>
                </div>
                <section class="section grey-section">
                    <div class="office-1">
                        <div class="box-1">
                            <iframe width="100%" height="350" frameborder="0" scrolling="no" marginheight="0"
                                    marginwidth="0" allowfullscreen
                                    src="https://player.vimeo.com/video/{{ $productfamily->consumer_video }}"></iframe>
                        </div>
                        <div class="box-1">
                            <div class="text-in">
                                <div class="section-title left office-text">
                                    <div class="subtitle left">
                                        About
                                    </div>
                                    <h3>{{ $productfamily->product_name }}</h3>
                                    <div class="subtitle left">
                                        Consumer Video
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            @endif

            @if($productfamily->practitioner_video)
                <div class="container">
                    <hr>
                </div>
                <section class="section grey-section">

                    <div class="office-1">

                        <div class="box-1">
                            <div class="text-in">
                                <div class="section-title right office-text">
                                    <div class="subtitle right">
                                        About
                                    </div>
                                    <h3>{{ $productfamily->product_name }}</h3>
                                    <div class="subtitle right">
                                        Practitioner Video
                                    </div>
                                    @if(!(Auth::check()))
                                        <div style="margin-right: unset;margin-bottom: 3%;"
                                             class="button-shortcodes text-size-1 text-padding-1 version-1"
                                             onClick="open_login()"><span>&#xf18e;</span> Please login to view
                                        </div>
                                    @endif

                                </div>
                            </div>
                        </div>
                        @if(Auth::check()?Auth::user()->role==1:false)
                            <div class="box-1">
                                <iframe width="100%" height="350" frameborder="0" scrolling="no" marginheight="0"
                                        marginwidth="0" allowfullscreen
                                        src="https://player.vimeo.com/video/{{ $productfamily->practitioner_video }}"></iframe>
                            </div>
                        @elseif(Auth::check()?Auth::user()->role==2:false)
                            <div class="box-1">
                                <iframe
                                    srcdoc="<p style='text-align:center;font-size:xxx-large;color:white;padding:5%;margin-top:15%;'>Restricted to Practitioner accounts</p>"
                                    width="100%" height="350"
                                    allowfullscreen style="background-color:black;"></iframe>
                            </div>
                        @else
                            <div class="box-1">
                                <iframe
                                    srcdoc="<p style='text-align:center;font-size:xxx-large;color:white;padding:5%;margin-top:15%;'>Please login to view this video</p>"
                                    width="100%" height="350"
                                    allowfullscreen style="background-color:black;"></iframe>
                            </div>
                        @endif

                    </div>
                </section>
            @endif

            @if(count($product_pdf_cmi))
                <div class="container">
                    <hr>
                </div>
                <section class="section white-section section-padding-top-bottom">
                    <div class="container">
                        <h2 style="text-align: left;">Consumer Medicines Information (CMI)</h2><br>
                        @foreach($product_pdf_cmi as $a)
                            <div class="one-third column" data-scroll-reveal="enter left move 200px over 1s after 0.3s">
                                <a href="{{ $image_url }}/{{ $a['file_name'] }}" class="animsition-link">
                                    <div class="blog-box-1 white-section" style="padding: unset;margin:unset;">
                                        <img src="{{ $image_url }}/www/Images/large-flag/{{ $a['language'] }}.jpg"
                                             alt="{{ $a['language'] }}">
                                        <div class="link"
                                             style="color: #6ba53a;text-align: center">{{ $a['language'] }}</div>
                                    </div>
                                </a>
                            </div>
                        @endforeach

                    </div>
                </section>
            @endif

            @if(count($publication))
                <div class="container">
                    <hr>
                </div>
                <section class="section white-section section-padding-top-bottom">
                    <div class="container">
                        <h2 style="text-align: left;">Related Publications</h2><br>
                        @for($i=0;$i<count($publication);$i++)
                            {{--                @foreach($publication as $a)--}}
                            @if($i%2==0)
                                <div class="accordion">
                                    <div class="accordion_in"
                                         data-scroll-reveal="enter left move 200px over 1s after 0.3s">
                                        <div class="acc_head grey-section">{{$publication[$i]['title']}}</div>
                                        <div class="acc_content grey-section" style="padding: 5px;">
                                            <table id="publication_table_{{ $publication[$i]['id'] }}"
                                                   class="display compact"
                                                   style="width:100%">
                                                <thead>
                                                <tr style="background-color: lightgrey;">
                                                    <th style="width: 35%">Title</th>
                                                    <th style="width: 65%">Content</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @if($publication[$i]['publication_location'])
                                                    <tr>
                                                        <td>Location</td>
                                                        <td>{{ $publication[$i]['publication_location'] }}</td>
                                                    </tr>
                                                @endif
                                                @if($publication[$i]['date'])
                                                    <tr>
                                                        <td>Date</td>
                                                        <td>{{ substr($publication[$i]['date'],0,10) }}</td>
                                                    </tr>
                                                @endif

                                                @if(count($publication[$i]['member']))

                                                    <tr>
                                                        <td>Authors</td>
                                                        <td>
                                                            @if(count($publication[$i]['member']))
                                                                {{$publication[$i]['member'][0]['name']}}
                                                                @if(count($publication[$i]['member'])!=1)
                                                                    @for($j=1;$j<count($publication[$i]['member']);$j++)
                                                                        ,&nbsp{{ $publication[$i]['member'][$j]['name'] }}
                                                                    @endfor
                                                                @endif
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endif
                                                @if($publication[$i]['link'])
                                                    <tr>
                                                        <td>Link</td>
                                                        <td>
                                                            <a style="color: #6ba53a;word-break: break-word;"
                                                               href="{{ $publication[$i]['link'] }}"
                                                               target="_blank">{{ $publication[$i]['link'] }}</a>
                                                        </td>
                                                    </tr>
                                                @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            @else
                                <div class="accordion">
                                    <div class="accordion_in"
                                         data-scroll-reveal="enter right move 200px over 1s after 0.3s">
                                        <div class="acc_head grey-section">{{$publication[$i]['title']}}</div>
                                        <div class="acc_content grey-section" style="padding: 5px;">
                                            <table id="publication_table_{{ $publication[$i]['id'] }}"
                                                   class="display compact"
                                                   style="width:100%">
                                                <thead>
                                                <tr style="background-color: lightgrey;">
                                                    <th style="width: 35%">Title</th>
                                                    <th style="width: 65%">Content</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @if($publication[$i]['publication_location'])
                                                    <tr>
                                                        <td>Location</td>
                                                        <td>{{ $publication[$i]['publication_location'] }}</td>
                                                    </tr>
                                                @endif
                                                @if($publication[$i]['date'])
                                                    <tr>
                                                        <td>Date</td>
                                                        <td>{{ substr($publication[$i]['date'],0,10) }}</td>
                                                    </tr>
                                                @endif

                                                @if(count($publication[$i]['member']))

                                                    <tr>
                                                        <td>Authors</td>
                                                        <td>
                                                            @if(count($publication[$i]['member']))
                                                                {{$publication[$i]['member'][0]['name']}}
                                                                @if(count($publication[$i]['member'])!=1)
                                                                    @for($j=1;$j<count($publication[$i]['member']);$j++)
                                                                        ,&nbsp{{ $publication[$i]['member'][$j]['name'] }}
                                                                    @endfor
                                                                @endif
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endif
                                                @if($publication[$i]['link'])
                                                    <tr>
                                                        <td>Link</td>
                                                        <td>
                                                            <a style="color: #6ba53a;word-break: break-word;"
                                                               href="{{ $publication[$i]['link'] }}"
                                                               target="_blank">{{ $publication[$i]['link'] }}</a>
                                                        </td>
                                                    </tr>
                                                @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endfor
                    </div>
                </section>
            @endif


            @if(count($clinical_trial->toArray()))
                <div class="container">
                    <hr>
                </div>
                <section class="section white-section section-padding-top-bottom">
                    <div class="container">
                        <h2 style="text-align: left;">Related Clinical Trials</h2><br>

                        @php
                            $_SESSION['i']=0;
                        @endphp
                        @foreach($clinical_trial as $a)
                            {{--                @foreach($publication as $a)--}}
                            @if($_SESSION['i']==0)
                                <div class="accordion">
                                    <div class="accordion_in"
                                         data-scroll-reveal="enter left move 200px over 1s after 0.3s">
                                        @else
                                            <div class="accordion">
                                                <div class="accordion_in"
                                                     data-scroll-reveal="enter right move 200px over 1s after 0.3s">
                                                    @endif
                                                    <div class="acc_head grey-section">{{$a->name}}</div>
                                                    <div class="acc_content grey-section" style="padding: 5px;">
                                                        <table id="clinical_table_{{ $a->id }}" class="display compact"
                                                               style="width:100%">
                                                            <thead>
                                                            <tr style="background-color: lightgrey;">
                                                                <th style="width: 35%">Title</th>
                                                                <th style="width: 65%">Content</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            @if($a->name)
                                                                <tr>
                                                                    <td>Name</td>
                                                                    <td>{{ $a->name }}</td>
                                                                </tr>
                                                            @endif
                                                            @if($a->type)
                                                                <tr>
                                                                    <td>Type</td>
                                                                    <td>{{ $a->type }}</td>
                                                                </tr>
                                                            @endif
                                                            @if($a->about)
                                                                <tr>
                                                                    <td>About</td>
                                                                    <td>{!! $a->about !!}</td>
                                                                </tr>
                                                            @endif
                                                            @if($a->formulation)
                                                                <tr>
                                                                    <td>Formulation</td>
                                                                    <td>{!! $a->formulation !!}</td>
                                                                </tr>
                                                            @endif
                                                            @if($a->date_started)
                                                                <tr>
                                                                    <td>Date started</td>
                                                                    <td>{{ substr($a->date_started,0,10) }}</td>
                                                                </tr>
                                                            @endif
                                                            @if($a->date_hrec)
                                                                <tr>
                                                                    <td>Date of HREC approval</td>
                                                                    <td>{{ substr($a->date_hrec,0,10) }}</td>
                                                                </tr>
                                                            @endif
                                                            @if($a->hrec_name)
                                                                <tr>
                                                                    <td>HREC name</td>
                                                                    <td>{{ $a->hrec_name }}</td>
                                                                </tr>
                                                            @endif
                                                            @if($a->hrec_id)
                                                                <tr>
                                                                    <td>HREC approval ID</td>
                                                                    <td>{{ $a->hrec_id }}</td>
                                                                </tr>
                                                            @endif
                                                            @if($a->ctn)
                                                                <tr>
                                                                    <td>CTN</td>
                                                                    <td>{{ $a->ctn }}</td>
                                                                </tr>
                                                            @endif
                                                            @if($a->anzctr)
                                                                <tr>
                                                                    <td>ANZCTR</td>
                                                                    <td>{{ $a->anzctr }}</td>
                                                                </tr>
                                                            @endif
                                                            @if($a->anzctr_link)
                                                                <tr>
                                                                    <td>ANZCTR URL Link</td>
                                                                    <td><a href="{{ $a->anzctr_link }}"
                                                                           style="color: #6ba53a;word-break: break-word;">{{ $a->anzctr_link }}</a>
                                                                    </td>
                                                                </tr>
                                                            @endif
                                                            @if($a->study_site)
                                                                <tr>
                                                                    <td>Study Site</td>
                                                                    <td>{{ $a->study_site }}</td>
                                                                </tr>
                                                            @endif
                                                            @if($a->status)
                                                                <tr>
                                                                    <td>Status</td>
                                                                    <td>{{ $a->status }}</td>
                                                                </tr>
                                                            @endif
                                                            @if($a->progression)
                                                                <tr>
                                                                    <td>Progression</td>
                                                                    <td>{{ $a->progression }}</td>
                                                                </tr>
                                                            @endif
                                                            @if(count($a->brand()->get()->toArray()))
                                                                <tr>
                                                                    <td>Brand</td>
                                                                    <td>
                                                                        @for($i=0;$i<count($a->brand()->get()->toArray());$i++)
                                                                            @if($i==count($a->brand()->get()->toArray())-1)
                                                                                {{ $a->brand()->get()->toArray()[$i]['name'] }}
                                                                            @else
                                                                                {{ $a->brand()->get()->toArray()[$i]['name'] }}
                                                                                ,
                                                                            @endif
                                                                        @endfor
                                                                    </td>
                                                                </tr>
                                                            @endif
                                                            @if(count($a->product_family()->get()->toArray()))
                                                                <tr>
                                                                    <td>Product Family</td>
                                                                    <td>
                                                                        @for($i=0;$i<count($a->product_family()->get()->toArray());$i++)
                                                                            @if($i==count($a->product_family()->get()->toArray())-1)
                                                                                {{ $a->product_family()->get()->toArray()[$i]['product_name'] }}
                                                                            @else
                                                                                {{ $a->product_family()->get()->toArray()[$i]['product_name'] }}
                                                                                ,
                                                                            @endif
                                                                        @endfor
                                                                    </td>
                                                                </tr>
                                                            @endif
                                                            @if(count($a->member()->get()->toArray()))
                                                                <tr>
                                                                    <td>Member</td>
                                                                    <td>@for($i=0;$i<count($a->member()->get()->toArray());$i++)
                                                                            @if($i==count($a->member()->get()->toArray())-1)
                                                                                {{ $a->member()->get()->toArray()[$i]['name'] }}
                                                                            @else
                                                                                {{ $a->member()->get()->toArray()[$i]['name'] }}
                                                                                ,
                                                                            @endif
                                                                        @endfor
                                                                    </td>
                                                                </tr>
                                                            @endif
                                                            @if(count($a->partner()->get()->toArray()))
                                                                <tr>
                                                                    <td>Partner</td>
                                                                    <td>
                                                                        @for($i=0;$i<count($a->partner()->get()->toArray());$i++)
                                                                            @if($i==count($a->partner()->get()->toArray())-1)
                                                                                {{ $a->partner()->get()->toArray()[$i]['name'] }}
                                                                            @else
                                                                                {{ $a->partner()->get()->toArray()[$i]['name'] }}
                                                                                ,
                                                                            @endif
                                                                        @endfor
                                                                    </td>
                                                                </tr>
                                                            @endif
                                                            @if(count($a->product_ingredient()->get()->toArray()))
                                                                <tr>
                                                                    <td>Product Ingredient</td>
                                                                    <td>
                                                                        @for($i=0;$i<count($a->product_ingredient()->get()->toArray());$i++)
                                                                            @if($i==count($a->product_ingredient()->get()->toArray())-1)
                                                                                {!! $a->product_ingredient()->get()->toArray()[$i]['scientific_name'] !!}
                                                                            @else
                                                                                {!! $a->product_ingredient()->get()->toArray()[$i]['scientific_name'] !!}
                                                                                ,
                                                                            @endif
                                                                        @endfor
                                                                    </td>
                                                                </tr>
                                                            @endif
                                                            @if(count($a->product_group()->get()->toArray()))
                                                                <tr>
                                                                    <td>Product Group</td>
                                                                    <td>
                                                                        @for($i=0;$i<count($a->product_group()->get()->toArray());$i++)
                                                                            @if($i==count($a->product_group()->get()->toArray())-1)
                                                                                {{ $a->product_group()->get()->toArray()[$i]['name'] }}
                                                                            @else
                                                                                {{ $a->product_group()->get()->toArray()[$i]['name'] }}
                                                                                ,
                                                                            @endif
                                                                        @endfor
                                                                    </td>
                                                                </tr>
                                                            @endif
                                                            @if(count($a->article()->get()->toArray()))
                                                                <tr>
                                                                    <td>Article</td>
                                                                    <td>
                                                                        @for($i=0;$i<count($a->article()->get()->toArray());$i++)
                                                                            @if($i==count($a->article()->get()->toArray())-1)
                                                                                {{ $a->article()->get()->toArray()[$i]['title'] }}
                                                                            @else
                                                                                {{ $a->article()->get()->toArray()[$i]['title'] }}
                                                                                ,
                                                                            @endif
                                                                        @endfor
                                                                    </td>
                                                                </tr>
                                                            @endif
                                                            @if(count($a->publicationsandpresentation()->get()->toArray()))
                                                                <tr>
                                                                    <td>Publications and presentations</td>
                                                                    <td>
                                                                        @for($i=0;$i<count($a->publicationsandpresentation()->get()->toArray());$i++)
                                                                            @if($i==count($a->publicationsandpresentation()->get()->toArray())-1)
                                                                                {{ $a->publicationsandpresentation()->get()->toArray()[$i]['title'] }}
                                                                            @else
                                                                                {{ $a->publicationsandpresentation()->get()->toArray()[$i]['title'] }}
                                                                                ,
                                                                            @endif
                                                                        @endfor
                                                                    </td>
                                                                </tr>
                                                            @endif
                                                            @php
                                                                if($_SESSION['i']==0){$_SESSION['i']=1;} else{$_SESSION['i']=0;};@endphp
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                        <a href = "{{ route('clinical_trials_page',['id' => preg_replace('/--+/','-',preg_replace('/[^A-Za-z0-9\-]/', '-', $a->name)).'-'.$a->id]) }}">
                                                            <button style = "width: 100%; background-color: #7AA43F; color: white; text-align: center; font-size: 16px; cursor: pointer; padding: 15px 32px; border: none;">Show More</button>
                                                        </a>
                                                </div>
                                            </div>
                                            @endforeach

                                    </div>
                                </div>
                    </div>
                </section>
            @endif
            @if(count($not_pdf))
                <div class="container">
                    <hr>
                </div>
                <section class="section white-section section-padding-top-bottom">
                    <div class="container">
                        <h2 style="text-align: left;">Practitioner Downloads</h2>

                        @if(Auth::check()?Auth::user()->role==1:false)
                            @foreach($not_pdf as $type)
                                <h2>{{ $type[0]['pdf_type_id'] }}</h2>

                                @foreach($type as $a)
                                    <div class="grey-section" style="margin-bottom: 20px;min-height: 80px;">
                                        <div class="articleClass padding">
                                            {{--                                            <h4 style="float: left;width: 90%;color:#6ba53a;" href="{{ $image_url }}/{{ $a['file_name']}}">{{ $a['name'] }}</h4>--}}
                                            <p style="margin: 0px;float: left;width: 94%;"><a
                                                    href="{{ $image_url }}/{{ $a['file_name']}}"
                                                    style="color:#6ba53a;font-size: larger;font-weight: 900;">{{ $a['name'] }}</a>
                                            </p>
                                            <img src="{{ $image_url }}/www/Images/large-flag/{{ $a['language'] }}.jpg"
                                                 alt="{{ $a['language'] }}" style="width: 6%;float: right;">

                                            @if($a['description'])
                                                <p style="margin: 0px;">{!! $a['description'] !!}</p>
                                            @endif

                                        </div>
                                    </div>
                                @endforeach

                            @endforeach
                        @elseif(Auth::check()?Auth::user()->role==2:false)
                            <div class="grey-section" style="margin-top: 20px;">
                                <div class="articleClass padding">
                                    <p style="margin: 0px;">Practitioner Only!</p>
                                </div>
                            </div>
                        @else
                            <div class="grey-section" style="margin-top: 20px;">
                                <div class="button-shortcodes text-size-1 text-padding-1 version-1"
                                     onClick="open_login()"><span>&#xf18e;</span> Please login to view
                                </div>
                            </div>
                        @endif

                    </div>
                </section>
            @endif

            @if(count($article->toArray()))
                <div class="container">
                    <hr>
                </div>
                <section class="section white-section section-padding-top-bottom">
                    <div class="container">

                        <h2 style="text-align: left;">Related Articles</h2>
                        <div class="blog-wrapper">
                            <div id="blog-grid-masonry">
                                @foreach($article as $item)
                                    @if($item->article_type=='link')
                                        <a href="{{ route('www.articles_content',['id' => preg_replace('/--+/','-',preg_replace('/[^A-Za-z0-9\-]/', '-', $item->title)).'-'.$item->id]) }}"

                                           class="animsition-link">
                                            <div class="blog-box-3">
                                                <div class="blog-box-1 link-post grey-section">
                                                    <h4>{{ $item->title }}</h4>
                                                    <p>{{ $item->preview_text }}</p>
                                                    <p>{{ substr($item->date,0,10) }}</p>
                                                    @if(isset($item->author))<p>Author: {{ $item->author }}</p>@endif
                                                    <div class="link">&#xf178;</div>
                                                </div>
                                            </div>
                                        </a>
                                    @elseif($item->article_type=='video')
                                        <a href="{{ route('www.articles_content',['id' => preg_replace('/--+/','-',preg_replace('/[^A-Za-z0-9\-]/', '-', $item->title)).'-'.$item->id]) }}"
                                           class="animsition-link">
                                            <div class="blog-box-3">
                                                <div class="blog-box-1 grey-section">

                                                    <iframe src="{{ $item->video_link }}" width="100%" height="auto"
                                                            allowfullscreen></iframe>
                                                    <h4>{{ $item->title }}</h4>
                                                    <p>{{ $item->preview_text }}</p>
                                                    <p>{{ substr($item->date,0,10) }}</p>
                                                    @if(isset($item->author))<p>Author: {{ $item->author }}</p>@endif
                                                    <div class="link">&#xf178;</div>
                                                </div>
                                            </div>
                                        </a>
                                    @else
                                        <a href="{{ route('www.articles_content',['id' => preg_replace('/--+/','-',preg_replace('/[^A-Za-z0-9\-]/', '-', $item->title)).'-'.$item->id]) }}"
                                           class="animsition-link">
                                            <div class="blog-box-3">
                                                <div class="blog-box-1 grey-section">
                                                    <img src="{{ $image_url }}/www/Images/{{ $item->title_img }}"
                                                         alt="{{ $item->title_alt }}">
                                                    <h4>{{ $item->title }}</h4>
                                                    <p>{{ $item->preview_text }}</p>
                                                    <p>{{ substr($item->date,0,10) }}</p>
                                                    @if(isset($item->author))<p>Author: {{ $item->author }}</p>@endif
                                                    <div class="link">&#xf178;</div>
                                                </div>
                                            </div>
                                        </a>
                                    @endif

                                @endforeach
                            </div>
                        </div>
                    </div>
                </section>
                <section class="section white-section section-padding-top-bottom">
                    <div class="container">
                        <div class="sixteen columns">
                            <div class="blog-left-right-links pagination">
                                <a onclick="changePage(0)" id="pre" style="display: none;cursor:pointer">
                                    <div class="blog-left-link"><p>PREVIOUS 12</p></div>
                                </a>
                                <a onclick="changePage(1)" id="next" style="display: unset;cursor:pointer">
                                    <div class="blog-right-link"><p>NEXT 12</p></div>
                                </a>
                                {{--                                    <a>--}}
                                {{--                            <div style="padding: 6px">--}}
                                {{--                                <select onchange="changePage()" id="page" style="float: right;margin-right: 5%;">--}}
                                {{--                                    @for($i=1;$i<$allArticles->lastPage()+1;$i++)--}}
                                {{--                                        <option value={{ $i }} @if($allArticles->currentPage() == $i) selected @endif >{{ $i }}</option>--}}
                                {{--                                    @endfor--}}
                                {{--                                </select><p style="float: right">Page No.</p>--}}
                                {{--                            </div>--}}
                                {{--                                    </a>--}}


                            </div>
                        </div>
                    </div>

                </section>
            @endif


        @endif



    <!-- SECTION
        ================================================== -->


        @include('www.layouts.footer')


    </main>




    <div class="scroll-to-top">&#xf106;</div>





    <!-- JAVASCRIPT
    ================================================== -->
    <script type="text/javascript" src="{{ asset('/js/jquery-2.1.1.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/modernizr.custom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.mobile.custom.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/retina-1.1.0.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.animsition.min.js') }}"></script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            $(document).ready(function () {
                @if (session('login_error'))
                open_login();
                @endif
                @if(!$productfamily['in_development']=='1')
                sessionStorage.setItem('current_page', 1);
                sessionStorage.setItem('last_page',{{ $totalPage }});
                if (sessionStorage.getItem('last_page') == '1') {
                    document.getElementById('next').style.display = 'none';
                }
                @endif

                $(".animsition").animsition({

                    inClass: 'zoom-in-sm',
                    outClass: 'zoom-out-sm',
                    inDuration: 1500,
                    outDuration: 800,
                    linkElement: '.animsition-link',
                    // e.g. linkElement   :   'a:not([target="_blank"]):not([href^=#])'
                    loading: true,
                    loadingParentElement: 'body', //animsition wrapper element
                    loadingClass: 'animsition-loading',
                    unSupportCss: ['animation-duration',
                        '-webkit-animation-duration',
                        '-o-animation-duration'
                    ],
                    //"unSupportCss" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
                    //The default setting is to disable the "animsition" in a browser that does not support "animation-duration".

                    overlay: false,

                    overlayClass: 'animsition-overlay-slide',
                    overlayParentElement: 'body'
                });
            });
        })(jQuery);
        // var cw = $('#sync2').width();
        // $('#sync2').css({'height': cw + 'px'});

        function updateImage(id) {
            var token = document.getElementById(id).getAttribute('data-token');
            var data = 'id=' + id;
            $.ajax({
                type: "POST",
                url: '{{ url('www.updateImage')}}',
                data: {
                    _token: token,
                    'id': id,
                },
                dataType: 'json',
                success: function (data) {
                    console.log(document.getElementsByClassName('item'));
                    var result_1 = '';
                    var result_2 = '';
                    var maxwidth = document.getElementsByClassName('owl-item')[0].style.width;
                    var minwidth = document.getElementsByClassName('owl-item')[document.getElementsByClassName('owl-item').length - 1].style.width;
                    if (data['img_front_view_small'] == '' || data['img_front_view_small'] == 'Null') {
                        document.getElementsByClassName('item')[0].innerHTML = "<img src='{{ asset('/images/default_image.png') }}' alt=''/>";
                        document.getElementsByClassName('item')[3].innerHTML = "<img src='{{ asset('/images/default_small_image.png') }}' alt=''/>";
                    } else {
                        // result_1+=("<div class='owl-item' style='width:"+maxwidth+"'>"+"<div class='item'>"+
                        //     "<img src='https://cdn.medlab.co/"+ data['img_front_view']+"' alt=''/>"+
                        //     "</div>"+"</div>");
                        // result_2+=("<div class='owl-item synced' style='width:"+minwidth+"'>"+"<div class='item'>"+
                        //     "<img src='https://cdn.medlab.co/"+ data['img_front_view']+"' alt=''/>"+
                        //     "</div>"+"</div>");
                        document.getElementsByClassName('item')[0].innerHTML = "<img src='https://cdn.medlab.co/" + data['img_front_view'] + "' alt=''/>";
                        document.getElementsByClassName('item')[3].innerHTML = "<img src='https://cdn.medlab.co/" + data['img_front_view_small'] + "' alt=''/>";
                    }
                    if (data['img_left_view_small'] == '' || data['img_left_view_small'] == 'Null') {
                        document.getElementsByClassName('item')[1].innerHTML = "<img src='{{ asset('/images/default_image.png') }}' alt=''/>";
                        document.getElementsByClassName('item')[4].innerHTML = "<img src='{{ asset('/images/default_small_image.png') }}' alt=''/>";
                    } else {
                        // result_1+=("<div class='owl-item' style='width:"+maxwidth+"'>"+"<div class='item'>"+
                        //     "<img src='https://cdn.medlab.co/"+ data['img_left_view']+"' alt=''/>"+
                        //     "</div>"+"</div>");
                        // result_2+=("<div class='owl-item' style='width:"+minwidth+"'>"+"<div class='item'>"+
                        //     "<img src='https://cdn.medlab.co/"+ data['img_left_view']+"' alt=''/>"+
                        //     "</div>"+"</div>");
                        document.getElementsByClassName('item')[1].innerHTML = "<img src='https://cdn.medlab.co/" + data['img_left_view'] + "' alt=''/>";
                        document.getElementsByClassName('item')[4].innerHTML = "<img src='https://cdn.medlab.co/" + data['img_left_view_small'] + "' alt=''/>";
                    }
                    if (data['img_right_view_small'] == '' || data['img_right_view_small'] == 'Null') {
                        document.getElementsByClassName('item')[2].innerHTML = "<img src='{{ asset('/images/default_image.png') }}' alt=''/>";
                        document.getElementsByClassName('item')[5].innerHTML = "<img src='{{ asset('/images/default_small_image.png') }}' alt=''/>";
                    } else {
                        // result_1+=("<div class='owl-item' style='width:"+maxwidth+"'>"+"<div class='item'>"+
                        //     "<img src='https://cdn.medlab.co/"+ data['img_right_view']+"' alt=''/>"+
                        //     "</div>"+"</div>");
                        // result_2+=("<div class='owl-item' style='width:"+minwidth+"'>"+"<div class='item'>"+
                        //     "<img src='https://cdn.medlab.co/"+ data['img_right_view']+"' alt=''/>"+
                        //     "</div>"+"</div>");
                        document.getElementsByClassName('item')[2].innerHTML = "<img src='https://cdn.medlab.co/" + data['img_right_view'] + "' alt=''/>";
                        document.getElementsByClassName('item')[5].innerHTML = "<img src='https://cdn.medlab.co/" + data['img_right_view_small'] + "' alt=''/>";
                    }
                    // document.getElementsByClassName('owl-wrapper')[0].innerHTML=result_1;
                    // document.getElementsByClassName('owl-wrapper')[1].innerHTML=result_2;
                    // location.reload();

                },
                error: function (data) {
                    console.log(data);

                },
            })

        }

        function addone() {
            document.getElementById('amount').innerHTML = (parseInt(document.getElementById('amount').innerHTML) + 1 >= 0) ? parseInt(document.getElementById('amount').innerHTML) + 1 : 0;
        }

        function minusone() {
            document.getElementById('amount').innerHTML = (parseInt(document.getElementById('amount').innerHTML) - 1 >= 0) ? parseInt(document.getElementById('amount').innerHTML) - 1 : 0;
        }

        $("#amount").keypress(function (e) {
            if (isNaN(String.fromCharCode(e.which))) e.preventDefault();
        });
        $(document).ready(function () {
            @foreach($clinical_trial as $item)

            $('#clinical_table_{{$item['id']}}').DataTable({
                "lengthMenu": [150],
                "bFilter": false,
                "bInfo": false,
                "bPaginate": false,
                "ordering": false,
            });
            @endforeach

            @foreach($publication as $item)

            $('#publication_table_{{$item['id']}}').DataTable({
                "lengthMenu": [150],
                "bFilter": false,
                "bInfo": false,
                "bPaginate": false,
                "ordering": false,
            });
            @endforeach

        });

        function changePage(tag) {
            console.log(sessionStorage.getItem('current_page'));
            if (tag == '0') {
                var page = sessionStorage.getItem('current_page') - 1;
            } else {
                var page = Number(sessionStorage.getItem('current_page')) + 1;
            }
            console.log(page);
            $.ajax({
                headers: {
                    'X-CSRF-Token': '{{ csrf_token() }}',
                },
                url: "{{ url('www.articles_page') }}",
                data: {
                    page: page,
                    id: '{{ $productfamily->id }}',
                },
                type: "POST",
                dataType: "JSON",
                success: function (data) {

                    var str = '';
                    for (var k in data['data']) {
                        if (data['data'][k]['article_type'] == 'video') {
                            str += "<a href = 'https://dev-www.medlab.co/articles_content/" + data['data'][k]['id'] +
                                "' class='animsition-link'>" + "<div class='blog-box-3'>" +
                                "<div class='blog-box-1 grey-section'>" +
                                "<iframe src='" + data['data'][k]['video_link'] + "' width='100%' height='auto' allowfullscreen></iframe>" +
                                "<h4>" + data['data'][k]['title'] + "</h4>" +
                                "<p>" + data['data'][k]['preview_text'] + "</p>" +
                                "<p>" + data['data'][k]['date'] + "</p>" +
                                "<div class='link'>&#xf178;</div>" +
                                "</div>" +
                                "</div>" +
                                "</a>";


                        } else {
                            str += "<a href = 'https://dev-www.medlab.co/articles_content/" + data['data'][k]['id'] +
                                "' class='animsition-link'>" +
                                "<div class='blog-box-3'>" +
                                "<div class='blog-box-1 grey-section'>" +
                                "<img src='https://cdn.medlab.co/www/Images/" + data['data'][k]['title_img'] + "'" + ' ' +
                                "alt=" + data['data'][k]['title_alt'] + ">" +
                                "<h4>" + data['data'][k]['title'] + "</h4>" +
                                "<p>" + data['data'][k]['preview_text'] + "</p>" +
                                "<p>" + data['data'][k]['date'] + "</p>" +
                                "<div class='link'>&#xf178;</div>" +
                                "</div>" +
                                "</div>" +
                                "</a>";
                        }
                        $('#blog-grid-masonry').empty().html(str);

                    }
                    sessionStorage.setItem('current_page', data['current_page'])
                    sessionStorage.setItem('last_page', data['last_page'])
                    if (data['current_page'] == 1) {
                        document.getElementById('pre').style.display = 'none';
                    } else {
                        document.getElementById('pre').style.display = 'unset';
                    }
                    if (data['current_page'] == data['last_page']) {
                        document.getElementById('next').style.display = 'none';
                    } else {
                        document.getElementById('next').style.display = 'unset';
                    }
                }
            })
        }

    </script>
    <script type="text/javascript" src="{{ asset('/js/jquery.themepunch.tools.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.themepunch.revolution.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.easing.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.hidescroll.min.js') }}"></script>
    <script type="text/javascript">
        $('.header-top').hidescroll();
    </script>
    <script type="text/javascript" src="{{ asset('/js/smoothScroll.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/isotope.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.counterup.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/waypoints.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/scrollReveal.js') }}"></script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            window.scrollReveal = new scrollReveal();
        })(jQuery);
    </script>
    <script type="text/javascript" src="{{ asset('/js/jquery.colorbox.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/owl.carousel.min.js') }}"></script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            jQuery(document).ready(function () {
                var offset = 450;
                var duration = 500;
                jQuery(window).scroll(function () {
                    if (jQuery(this).scrollTop() > offset) {
                        jQuery('.scroll-to-top').fadeIn(duration);
                    } else {
                        jQuery('.scroll-to-top').fadeOut(duration);
                    }
                });

                jQuery('.scroll-to-top').click(function (event) {
                    event.preventDefault();
                    jQuery('html, body').animate({scrollTop: 0}, duration);
                    return false;
                })
            });
        })(jQuery);
    </script>
    <script type="text/javascript" src="{{ asset('/js/styleswitcher.js') }}"></script>
    {{--    <script type="text/javascript" src="{{ asset('/js/custom-shop-home-1.js') }}"></script>--}}
    <script type="text/javascript" src="{{ asset('js/custom-shop-item.js') }}"></script>
    {{--    <script type="text/javascript" src="{{ asset('js/custom-tabs.js') }}"></script>--}}
    <script type="text/javascript" src="{{ asset('js/smk-accordion.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('/DataTables/datatables.min.css') }}"/>

    <script type="text/javascript" src="{{ asset('/DataTables/datatables.min.js') }}"></script>
    {{--    <script type="text/javascript" src="{{ asset('/js/custom-blog-grid-left.js') }}"></script>--}}






    <!-- End Document
================================================== -->
@endsection
