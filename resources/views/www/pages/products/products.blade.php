@extends('www.layouts.main')
@section('title')
    {{ $banner->title }}
@endsection
@section('description')
    @if($banner->meta_description)
        {{ $banner->meta_description }}
    @elseif(json_decode($banner->content))
        @foreach(json_decode($banner->content) as $item)
            @if($item->layout=='text')
                {{ substr(str_replace('&nbsp;',' ',strip_tags($item->attributes->content)),0,160) }}
            @endif
        @endforeach
    @endif
@endsection
@section('breadcrumbs')
    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset;background-color:#7AA43F;">
            <ol class="breadcrumb medlab_breadcrumbs" style="margin-bottom: unset;background-color:#7AA43F;">
            </ol>
        </div>
    </div>

    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset">
            <ol class="breadcrumb medlab_breadcrumbs">
                <li><a class="medlab_breadcrumbs_link" href="/">Home</a></li>
                @if(isset($condition['vegan_friendly']))
                    <li><a class="medlab_breadcrumbs_link" href="/products">Products</a></li>
                    <li class="active medlab_breadcrumbs_text">@lang('Vegan Friendly')</li>
                @elseif(isset($condition['vegetarian_friendly']))
                    <li><a class="medlab_breadcrumbs_link" href="/products">Products</a></li>
                    <li class="active medlab_breadcrumbs_text">@lang('Vegetarian Friendly')</li>
                @elseif(isset($condition['gluten_free']))
                    <li><a class="medlab_breadcrumbs_link" href="/products">Products</a></li>
                    <li class="active medlab_breadcrumbs_text">@lang('Gluten Free')</li>
                @elseif(isset($condition['pregnancy_safe']))
                    <li><a class="medlab_breadcrumbs_link" href="/products">Products</a></li>
                    <li class="active medlab_breadcrumbs_text">@lang('Pregnancy Safe')</li>
                @elseif(isset($condition['dairy_free']))
                    <li><a class="medlab_breadcrumbs_link" href="/products">Products</a></li>
                    <li class="active medlab_breadcrumbs_text">@lang('Dairy Free')</li>
                @elseif(isset($condition['group']))
                    <li><a class="medlab_breadcrumbs_link" href="/products">Products</a></li>
                    <li class="active medlab_breadcrumbs_text">{{$condition['group']}}</li>
                @else
                    <li class="active medlab_breadcrumbs_text">Products</li>
                @endif
            </ol>
        </div>
    </div>
@endsection

@section('content')



    <main class="cd-main-content" id="main" style="margin-top: 90px">

        <!-- HOME SECTION
        ================================================== -->

        @if(isset($banner->image)&&$banner->image!=''&&$banner->image!='Null')
            <section class="home">
                <div class="slider-container">
                    <div class="tp-banner-container">
                        <div class="tp-banner">
                            <ul>
                                <li data-transition="fade" data-slotamount="1" data-masterspeed="500"
                                    data-saveperformance="on" data-title="Intro Slide">
                                    <img src="{{ $image_url }}/www/Images/{{ $banner->image()->first()->name }}" alt=""
                                         data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
        @endif
        @if(isset($banner->page))
            <section class="section grey-section" style="padding-top: 40px">

                <div class="container">
                    <div class="sixteen columns">
                        <div class="section-title left">
                            <h1>{{ $banner->title }}</h1>
                            @if(isset($banner->subtitle))
                                <div class="subtitle big left">{{ $banner->subtitle }}</div>
                            @endif
                        </div>
                    </div>
                </div>

            </section>
        @else
            @if($banner->image!=''&&$banner->image!='Null')
                <section class="section grey-section">

                    <div class="container">
                        <div class="sixteen columns">
                            <div class="section-title left">
                                <h1>{{ $banner->name }}</h1>
                            </div>
                        </div>
                    </div>

                </section>
            @else
                <section class="section grey-section" style="padding-top: 40px">

                    <div class="container">
                        <div class="sixteen columns">
                            <div class="section-title left">
                                <h1>{{ $banner->name }}</h1>
                            </div>
                        </div>
                    </div>

                </section>
            @endif
        @endif
        @if(json_decode($banner->content))
            <section class="section white-section">
                <div class="container">
                    <div class="sixteen columns remove-bottom">
                        <div class="full-image">
                            <div class="articleClass padding">
                                @foreach(json_decode($banner->content) as $item)
                                    @if($item->layout=='text')
                                        {!! $item->attributes->content !!}
                                    @else
                                        <div style="width: 100%;">
                                            <iframe src="{{ $item->attributes->content }}?transparent=false"
                                                    width="100%" height="500"
                                                    frameborder=”0″ webkitallowfullscreen mozallowfullscreen
                                                    allowfullscreen></iframe>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        @endif

    <!-- SECTION
        ================================================== -->


        <section class="section white-section section-padding-top-bottom">
            <div class="container">
                <div id="portfolio-filter">
                    <ul id="filter">
                        <li><a href="#" class="current" data-filter="*" title="">Show All</a></li>
                        <li><a href="#" data-filter=".inDev" title="">In Development</a></li>
                    </ul>
                </div>
                <div class="clear"></div>
                <div id="shop-grid">
                    @foreach($index as $item)
                        <div class="one-third column">
                            <a href={{ route('www.product_page', ['id' => preg_replace('/--+/','-',preg_replace('/[^A-Za-z0-9\-]/', '-', $item->product_name)).'-'.$item->id]) }}>
                                <div class="blog-box-1 grey-section"
                                     style="text-align: center">

                                    @if($item->product()->first()->unapproved_drug=='1' && !(Auth::guard('practitioner')->check()))
                                        <img
                                            src="https://cdn.medlab.co/www/Images/demo_drug.png"
                                            style="display: inline-block" alt=""/>
                                    @else
                                        <img
                                            src="https://cdn.medlab.co/{{ $item->product()->first()->img_front_view_small }}"
                                            style="display: inline-block" alt=""/>
                                    @endif
                                    <div>
                                        <h2 class="one-row-max"
                                            style="padding: 5px;height: 40px;text-align: center">{{ $item->product_name }}</h2>
                                        <br>
                                        <div class="two-row-max"
                                             style="padding: 5px;height: 30px;">
                                            <p>{!! $item->product_heading !!}</p></div>
                                        <br>
                                        <div class="two-row-max"
                                             style="padding: 5px;height: 50px;">
                                            <p>{!! $item->product_byline !!}</p></div>
                                    </div>
                                </div>
                            </a>

                        </div>
                    @endforeach
                    @foreach($productsInDev as $item)
                        <div class="one-third column inDev">
                            <a href="{{ route('products_in_development', ['id' => preg_replace('/--+/','-',preg_replace('/[^A-Za-z0-9\-]/', '-', $item->product_name)).'-'.$item->id]) }}">
                                <div class="blog-box-1 grey-section"
                                     style="text-align: center">
                                    <img
                                        src="https://cdn.medlab.co/www/Images/demo_drug.png"
                                        style="display: inline-block" alt=""/>

                                    <div>
                                        <h2 class="one-row-max"
                                            style="padding: 5px;height: 40px;text-align: center">{{ $item->product_name }}</h2>
                                        <br>
                                        <div class="two-row-max"
                                             style="padding: 5px;height: 30px;">
                                            <p>{!! $item->product_heading !!}</p></div>
                                        <br>
                                        <div class="two-row-max"
                                             style="padding: 5px;height: 50px;">
                                            <p>{!! $item->product_byline !!}</p></div>
                                    </div>

                                </div>
                            </a>

                        </div>
                    @endforeach
                </div>

            </div>
        </section>
        <!-- SECTION
        ================================================== -->


        <section class="section grey-section section-padding-top-bottom">
            <div class="container">
                <div class="sixteen columns">
                    <div class="blog-left-right-links pagination">
                        @if($index->currentPage()-1!=0)
                            @if(isset($condition['vegan_friendly']))
                                <a href="{{ route('www.products') }}?vegan_friendly={{ $condition['vegan_friendly'] }}&page={{ $index->currentPage()-1 }}">
                                    <div class="blog-left-link"><p>PREVIOUS 25</p></div>
                                </a>
                            @elseif(isset($condition['vegetarian_friendly']))
                                <a href="{{ route('www.products') }}?vegetarian_friendly={{ $condition['vegetarian_friendly'] }}&page={{ $index->currentPage()-1 }}">
                                    <div class="blog-left-link"><p>PREVIOUS 25</p></div>
                                </a>
                            @elseif(isset($condition['gluten_free']))
                                <a href="{{ route('www.products') }}?gluten_free={{ $condition['gluten_free'] }}&page={{ $index->currentPage()-1 }}">
                                    <div class="blog-left-link"><p>PREVIOUS 25</p></div>
                                </a>
                            @elseif(isset($condition['pregnancy_safe']))
                                <a href="{{ route('www.products') }}?pregnancy_safe={{ $condition['pregnancy_safe'] }}&page={{ $index->currentPage()-1 }}">
                                    <div class="blog-left-link"><p>PREVIOUS 25</p></div>
                                </a>
                            @elseif(isset($condition['dairy_free']))
                                <a href="{{ route('www.products') }}?dairy_free={{ $condition['dairy_free'] }}&page={{ $index->currentPage()-1 }}">
                                    <div class="blog-left-link"><p>PREVIOUS 25</p></div>
                                </a>
                            @elseif(isset($condition['group']))
                                <a href="{{ route('www.products') }}?group={{ $condition['group'] }}&page={{ $index->currentPage()-1 }}">
                                    <div class="blog-left-link"><p>PREVIOUS 25</p></div>
                                </a>
                            @else
                                <a href="{{ route('www.products') }}?page={{ $index->currentPage()-1 }}">
                                    <div class="blog-left-link"><p>PREVIOUS 25</p></div>
                                </a>
                            @endif
                        @endif
                        {{--                                @for($i=1;$i<$allArticles->total()+1;$i++)--}}
                        {{--                                        <a href="{{ route('www.articles') }}?role={{ $role }}&article_category={{ $article_category }}&page={{ $i }}">--}}
                        {{--                                            <div class="blog-left-link"><p>{{ $i }}</p></div>--}}
                        {{--                                        </a>--}}
                        {{--                                    @endfor--}}

                        @if($index->currentPage()!=$index->lastPage())
                            @if(isset($condition['vegan_friendly']))
                                <a href="{{ route('www.products') }}?vegan_friendly={{ $condition['vegan_friendly'] }}&page={{ $index->currentPage()+1 }}">
                                    <div class="blog-right-link"><p>NEXT 25</p></div>
                                </a>
                            @elseif(isset($condition['vegetarian_friendly']))
                                <a href="{{ route('www.products') }}?vegetarian_friendly={{ $condition['vegetarian_friendly'] }}&page={{ $index->currentPage()+1 }}">
                                    <div class="blog-right-link"><p>NEXT 25</p></div>
                                </a>
                            @elseif(isset($condition['gluten_free']))
                                <a href="{{ route('www.products') }}?gluten_free={{ $condition['gluten_free'] }}&page={{ $index->currentPage()+1 }}">
                                    <div class="blog-right-link"><p>NEXT 25</p></div>
                                </a>
                            @elseif(isset($condition['pregnancy_safe']))
                                <a href="{{ route('www.products') }}?pregnancy_safe={{ $condition['pregnancy_safe'] }}&page={{ $index->currentPage()+1 }}">
                                    <div class="blog-right-link"><p>NEXT 25</p></div>
                                </a>
                            @elseif(isset($condition['dairy_free']))
                                <a href="{{ route('www.products') }}?dairy_free={{ $condition['dairy_free'] }}&page={{ $index->currentPage()+1 }}">
                                    <div class="blog-right-link"><p>NEXT 25</p></div>
                                </a>
                            @elseif(isset($condition['group']))
                                <a href="{{ route('www.products') }}?group={{ $condition['group'] }}&page={{ $index->currentPage()+1 }}">
                                    <div class="blog-right-link"><p>NEXT 25</p></div>
                                </a>
                            @else
                                <a href="{{ route('www.products') }}?page={{ $index->currentPage()+1 }}">
                                    <div class="blog-right-link"><p>NEXT 25</p></div>
                                </a>
                            @endif
                        @endif
                        {{--                                    <a>--}}
                        <div style="padding: 6px">
                            <select onchange="changePage()" id="page" style="float: right;margin-right: 5%;">
                                @for($i=1;$i<$index->lastPage()+1;$i++)
                                    <option
                                        value={{ $i }} @if($index->currentPage() == $i) selected @endif >{{ $i }}</option>
                                @endfor
                            </select>
                            <p style="float: right">Page No.</p>
                        </div>
                        {{--                                    </a>--}}


                    </div>
                </div>
            </div>

        </section>

        @include('www.layouts.footer')
    </main>

    <div class="scroll-to-top">&#xf106;</div>





    <!-- JAVASCRIPT
    ================================================== -->
    <script type="text/javascript" src="{{ asset('/js/jquery-2.1.1.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/modernizr.custom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.mobile.custom.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/retina-1.1.0.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.animsition.min.js') }}"></script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            $(document).ready(function () {
                @if (session('login_error'))
                open_login();
                @endif
                $(".animsition").animsition({

                    inClass: 'zoom-in-sm',
                    outClass: 'zoom-out-sm',
                    inDuration: 1500,
                    outDuration: 800,
                    linkElement: '.animsition-link',
                    // e.g. linkElement   :   'a:not([target="_blank"]):not([href^=#])'
                    loading: true,
                    loadingParentElement: 'body', //animsition wrapper element
                    loadingClass: 'animsition-loading',
                    unSupportCss: ['animation-duration',
                        '-webkit-animation-duration',
                        '-o-animation-duration'
                    ],
                    //"unSupportCss" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
                    //The default setting is to disable the "animsition" in a browser that does not support "animation-duration".

                    overlay: false,

                    overlayClass: 'animsition-overlay-slide',
                    overlayParentElement: 'body'
                });
            });
        })(jQuery);

        function addone(id) {
            document.getElementById('amount_' + id).innerHTML = (parseInt(document.getElementById('amount_' + id).innerHTML) + 1 >= 0) ? parseInt(document.getElementById('amount_' + id).innerHTML) + 1 : 0;
        }

        function minusone(id) {
            document.getElementById('amount_' + id).innerHTML = (parseInt(document.getElementById('amount_' + id).innerHTML) - 1 >= 0) ? parseInt(document.getElementById('amount_' + id).innerHTML) - 1 : 0;
        }

        $(".num-itm").keypress(function (e) {
            if (isNaN(String.fromCharCode(e.which))) e.preventDefault();
        });

        function changePage() {
            var page = document.getElementById("page").value;
            console.log(page);

                @if(isset($condition['vegan_friendly'])){
                location.href = "{{ route('www.products') }}?vegan_friendly={{$condition['vegan_friendly']}}&page=" + page;
            }
                @elseif(isset($condition['vegetarian_friendly'])){
                location.href = "{{ route('www.products') }}?vegetarian_friendly={{$condition['vegetarian_friendly']}}&page=" + page;
            }
                @elseif(isset($condition['gluten_free'])){
                location.href = "{{ route('www.products') }}?gluten_free={{$condition['gluten_free']}}&page=" + page;
            }
                @elseif(isset($condition['pregnancy_safe'])){
                location.href = "{{ route('www.products') }}?pregnancy_safe={{$condition['pregnancy_safe']}}&page=" + page;
            }
                @elseif(isset($condition['dairy_free'])){
                location.href = "{{ route('www.products') }}?dairy_free={{$condition['dairy_free']}}&page=" + page;
            }
                @elseif(isset($condition['group'])){
                location.href = "{{ route('www.products') }}?group={{$condition['group']}}&page=" + page;
            }
                @else{
                location.href = "{{ route('www.products') }}?&page=" + page;
            }
            @endif



        }

        function isset(ref) {
            return typeof ref !== 'undefined'
        }
    </script>
    <script type="text/javascript" src="{{ asset('/js/jquery.themepunch.tools.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.themepunch.revolution.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.easing.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.hidescroll.min.js') }}"></script>
    <script type="text/javascript">
        $('.header-top').hidescroll();
    </script>
    <script type="text/javascript" src="{{ asset('/js/smoothScroll.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/isotope.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.counterup.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/waypoints.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/scrollReveal.js') }}"></script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            window.scrollReveal = new scrollReveal();
        })(jQuery);
    </script>
    <script type="text/javascript" src="{{ asset('/js/jquery.colorbox.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/owl.carousel.min.js') }}"></script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            jQuery(document).ready(function () {
                var offset = 450;
                var duration = 500;
                jQuery(window).scroll(function () {
                    if (jQuery(this).scrollTop() > offset) {
                        jQuery('.scroll-to-top').fadeIn(duration);
                    } else {
                        jQuery('.scroll-to-top').fadeOut(duration);
                    }
                });

                jQuery('.scroll-to-top').click(function (event) {
                    event.preventDefault();
                    jQuery('html, body').animate({scrollTop: 0}, duration);
                    return false;
                })
            });
        })(jQuery);
    </script>
    <script type="text/javascript" src="{{ asset('/js/styleswitcher.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/custom-shop-home-2.js') }}"></script>

    <!-- End Document
    ================================================== -->
@endsection
