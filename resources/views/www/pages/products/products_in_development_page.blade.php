@extends('www.layouts.main')
@section('title')
    {{ $content->title }}
@endsection
@section('description')
    @if($content->meta_description)
        {{ $content->meta_description }}
    @elseif($content->content)
        {{ substr(str_replace('&nbsp;',' ',strip_tags($content->content)),0,160) }}
    @endif
@endsection
@section('breadcrumbs')
    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset;background-color:#7AA43F;">
            <ol class="breadcrumb medlab_breadcrumbs" style="margin-bottom: unset;background-color:#7AA43F;">
            </ol>
        </div>
    </div>

    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset">
            <ol class="breadcrumb medlab_breadcrumbs">
                <li><a class="medlab_breadcrumbs_link" href="/">@lang('trs.home')</a></li>
                <li class="active medlab_breadcrumbs_text">Products In Development</li>
            </ol>
        </div>
    </div>
@endsection

@section('content')
    <main class="cd-main-content" id="main" style="margin-top:90px">

        <!-- TOP SECTION - SLIDER IMAGE
================================================== -->

        <section class="home">

            <div class="slider-container">
                <div class="tp-banner-container">
                    <div class="tp-banner">
                        <ul>
                            <li data-transition="fade" data-slotamount="1" data-masterspeed="500"
                                data-saveperformance="on" data-title="Intro Slide">
                                <img src="{{ $image_url }}/www/Images/{{ $content->title_img }}"
                                     alt="{{ $content->title_alt }}"
                                     data-lazyload="{{ $image_url }}/www/Images/{{ $content->title_img }}"
                                     data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                                <a href="#">
                                    <div class="black-heavy-3">
                                        <div class="black-heavy-3-heading"><h1>{{ $content->title }}</h1></div>
                                        <div class="black-heavy-3-subheading">{{ $content->subtitle }}</div>
                                    </div>
                                </a>
                            </li>
                            @if(isset($content->second_img))
                                <li data-transition="fade" data-slotamount="2" data-masterspeed="500"
                                    data-saveperformance="on" data-title="Intro Slide">
                                    <img src="{{ $image_url }}/www/Images/{{ $content->second_img }}"
                                         alt="{{ $content->second_alt }}"
                                         data-lazyload="{{ $image_url }}/www/Images/{{ $content->second_img }}"
                                         data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                                    <a href="#">
                                        <div class="black-heavy-3">
                                            <div class="black-heavy-3-heading">{{ $content->title2 }}</div>
                                            @if(isset($content->subtitle3))
                                                <div class="black-heavy-3-subheading">{{ $content->subtitle2 }}</div>
                                            @endif
                                        </div>
                                    </a>
                                </li>
                            @endif
                            @if(isset($content->third_img))
                                <li data-transition="fade" data-slotamount="3" data-masterspeed="500"
                                    data-saveperformance="on" data-title="Intro Slide">
                                    <img src="{{ $image_url }}/www/Images/{{ $content->third_img }}"
                                         alt="{{ $content->third_img }}"
                                         data-lazyload="{{ $image_url }}/www/Images/{{ $content->third_img }}"
                                         data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                                    <a href="#">
                                        <div class="black-heavy-3">
                                            <div class="black-heavy-3-heading">{{ $content->title3 }}</div>
                                            @if(isset($content->subtitle3))
                                                <div class="black-heavy-3-subheading">{{ $content->subtitle3 }}</div>
                                            @endif
                                        </div>
                                    </a>
                                </li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>

        </section>

        <!-- CONTENT SECTION
    ================================================== -->

        @if($content->content)
            <section class="section white-section section-padding-top" id="scroll-link">
                <div class="container">
                    <div class="sixteen columns remove-bottom">
                        <div class="full-image">
                            <div class="articleClass padding">
                                {!! $content->content !!}
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        @endif


    <!-- CLINICAL TRIALS SECTION
  ================================================== -->

        <section class="section white-section section-padding-top-bottom">
            <div class="container">
                <div class="sixteen columns">

                </div>
                <div class="clear"></div>
                <div id="shop-grid">
                    @foreach($productsInDev as $item)
                        <div class="one-third column">
                            <a href="{{ route('products_in_development', ['id' => $item->id]) }}"
                               class="animsition-link">
                                <div class="blog-box-1 link-post">
                                    <h3 class="two-row-max" style="min-height: 50px;">{{ $item->product_name }}</h3>
                                    <h4 class="two-row-max" style="min-height: 50px;">{{ $item->product_heading }}</h4>
                                    <p class="four-row-max"
                                       style="padding-bottom: unset;min-height: 92px;">{{ $item->product_byline }}</p>
                                </div>
                            </a>
                        </div>

                    @endforeach
                </div>
            </div>
        </section>

        @include('www.layouts.footer')
    </main>

    <div class="scroll-to-top">&#xf106;</div>





    <!-- JAVASCRIPT
    ================================================== -->
    <script type="text/javascript" src="{{ asset('/js/jquery-2.1.1.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/modernizr.custom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.mobile.custom.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/retina-1.1.0.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.animsition.min.js') }}"></script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            $(document).ready(function () {
                @if (session('login_error'))
                open_login();
                @endif
                $(".animsition").animsition({

                    inClass: 'zoom-in-sm',
                    outClass: 'zoom-out-sm',
                    inDuration: 1500,
                    outDuration: 800,
                    linkElement: '.animsition-link',
                    // e.g. linkElement   :   'a:not([target="_blank"]):not([href^=#])'
                    loading: true,
                    loadingParentElement: 'body', //animsition wrapper element
                    loadingClass: 'animsition-loading',
                    unSupportCss: ['animation-duration',
                        '-webkit-animation-duration',
                        '-o-animation-duration'
                    ],
                    //"unSupportCss" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
                    //The default setting is to disable the "animsition" in a browser that does not support "animation-duration".

                    overlay: false,

                    overlayClass: 'animsition-overlay-slide',
                    overlayParentElement: 'body'
                });
            });
        })(jQuery);
    </script>


    <script type="text/javascript" src="{{ asset('/js/jquery.easing.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.hidescroll.min.js') }}"></script>
    <script type="text/javascript">
        $('.header-top').hidescroll();
    </script>
    <script type="text/javascript" src="{{ asset('/js/smoothScroll.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.parallax-1.1.3.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/owl.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/masonry.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/isotope.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/scrollReveal.js') }}"></script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            window.scrollReveal = new scrollReveal();
        })(jQuery);
    </script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            jQuery(document).ready(function () {
                var offset = 450;
                var duration = 500;
                jQuery(window).scroll(function () {
                    if (jQuery(this).scrollTop() > offset) {
                        jQuery('.scroll-to-top').fadeIn(duration);
                    } else {
                        jQuery('.scroll-to-top').fadeOut(duration);
                    }
                });

                jQuery('.scroll-to-top').click(function (event) {
                    event.preventDefault();
                    jQuery('html, body').animate({scrollTop: 0}, duration);
                    return false;
                })
            });
        })(jQuery);
    </script>

    <!--
    <script>
        (function($) { "use strict";
        var container = $('#blog-grid-masonry');


        function getNumbColumns() {
            var winWidth = $(window).width(),
                columnNumb = 1;


            if (winWidth > 1500) {
                columnNumb = 3;
            } else if (winWidth > 1200) {
                columnNumb = 3;
            } else if (winWidth > 900) {
                columnNumb = 2;
            } else if (winWidth > 600) {
                columnNumb = 2;
            } else if (winWidth > 300) {
                columnNumb = 1;
            }

            return columnNumb;
        }


        function setColumnWidth() {
            var winWidth = $(window).width(),
                columnNumb = getNumbColumns(),
                postWidth = Math.floor(winWidth / columnNumb);

        }

        $('#portfolio-filter #filter a').click(function () {
            var selector = $(this).attr('data-filter');

            $(this).parent().parent().find('a').removeClass('current');
            $(this).addClass('current');

            container.isotope( {
                filter : selector
            });

            setTimeout(function () {
                reArrangeProjects();
            }, 300);


            return false;
        });

        function reArrangeProjects() {
            setColumnWidth();
            container.isotope('reLayout');
        }


        container.imagesLoaded(function () {
            setColumnWidth();


            container.isotope( {
                itemSelector : '.one-third column',
                layoutMode : 'masonry',
                resizable : false
            } );
        } );





        $(window).on('debouncedresize', function () {
            reArrangeProjects();

        } );
        })(jQuery);
    </script>
    -->
    <script type="text/javascript" src="{{ asset('/js/styleswitcher.js') }}"></script>


    <script type="text/javascript" src="{{ asset('js/pro-bars.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/smk-accordion.js') }}"></script>


    <script type="text/javascript" src="{{ asset('/js/jquery.themepunch.tools.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.themepunch.revolution.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/visible.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.counterup.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/waypoints.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/custom-portfolio-home-1.js') }}"></script>

    {{--    <script type="text/javascript" src="{{ asset('/js/custom-about-1.js') }}"></script>--}}
    <script type="text/javascript" src="{{ asset('/js/custom-shop-home-2.js') }}"></script>
    {{--    <script type="text/javascript" src="{{ asset('/js/custom-tabs.js') }}"></script>--}}

    <link rel="stylesheet" type="text/css" href="{{ asset('/DataTables/datatables.min.css') }}"/>
    <script type="text/javascript" src="{{ asset('/DataTables/datatables.min.js') }}"></script>






    <!-- End Document
    ================================================== -->
@endsection

