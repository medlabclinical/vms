﻿@extends('www.layouts.main')
@section('title')
    Translate
@endsection

@section('breadcrumbs')
<div class="medlab_breadcrumbs_wrapper">
	<div class="container" style="width: unset;background-color:#7AA43F;">
		<ol class="breadcrumb medlab_breadcrumbs" style="margin-bottom: unset;background-color:#7AA43F;">
		</ol>
	</div>
</div>

<div class="medlab_breadcrumbs_wrapper">
	<div class="container" style="width: unset">
		<ol class="breadcrumb medlab_breadcrumbs">
			<li><a class="medlab_breadcrumbs_link" href="/">@lang('trs.home')</a></li>
			<li class="active medlab_breadcrumbs_text">@lang('trs.translate')</li>
		</ol>
	</div>
</div>
@endsection
@section('content')




	<main class="cd-main-content" id="main" style="margin-top:90px">

		<!-- TOP SECTION
    ================================================== -->

		<section class="section parallax-section parallax-section-padding-top-bottom-home">

			<div class="parallax-about-me"></div>

			<div class="container">
				<div class="sixteen columns">
					<div class="section-title left">
						<h1>@lang('trs.translate')</h1>
						<div class="subtitle left big">Translate

						</div>
					</div>
				</div>
			</div>

		</section>

		<!-- SECTION
        ================================================== -->

        <section class="section white-section section-padding-top-bottom">
            <div class="container">
{{--                <div class="sixteen columns">--}}
{{--                    <div class="section-title">--}}
{{--                        <div class="subtitle" style="text-align: left;font-weight: bold;text-transform: unset">Medlab has multiple patents pending on our products:(Patent Portfolio as at 16/10/2019)</div>--}}
{{--                    </div>--}}
{{--                </div>--}}
                <table id="example" class="display compact" style="width:100%">
                    <thead>
                    <tr style="background-color: lightgrey;">
                        <th style="width: 35%">Product</th>
                        <th style="width: 30%">Country</th>
                        <th>Link</th>
                    </tr>
                    </thead>
                    <tbody>

                    @for($i=0;$i<count($list);$i++)
                        @for($j=0;$j<count($list[$data[$i]->product_name]);$j++)
                        <tr>
                            <td>{{ $data[$i]->product_name }}</td>
                            <td style="height: 30px;"><img style="vertical-align:middle;" src="{{ $image_url.'/www/Images/flag/'.array_keys($list[$data[$i]->product_name])[$j].'.png'}}" />  {{ array_keys($list[$data[$i]->product_name])[$j] }}</td>
                            <td><a href="{{ explode('& &',$list[$data[$i]->product_name][array_keys($list[$data[$i]->product_name])[$j]])[1] }}" target="_blank">
                                    {{ explode('& &',$list[$data[$i]->product_name][array_keys($list[$data[$i]->product_name])[$j]])[0] }}</a></td>
                        </tr>
                        @endfor
                    @endfor
                    </tbody>
                    <tfoot>
                    <tr>
                        <th >Product</th>
                        <th >Country</th>
                        <th >Link</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </section>





        @include('www.layouts.footer')


	</main>

	<div class="scroll-to-top">&#xf106;</div>





	<!-- JAVASCRIPT
    ================================================== -->
    <script type="text/javascript" src="{{ asset('/js/jquery-2.1.1.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/modernizr.custom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.mobile.custom.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/retina-1.1.0.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.animsition.min.js') }}"></script>
    <script type="text/javascript">
        (function($) { "use strict";
            $(document).ready(function() {
                @if (session('login_error'))
                open_login();
                @endif
                $(".animsition").animsition({

                    inClass               :   'zoom-in-sm',
                    outClass              :   'zoom-out-sm',
                    inDuration            :    1500,
                    outDuration           :    800,
                    linkElement           :   '.animsition-link',
                    // e.g. linkElement   :   'a:not([target="_blank"]):not([href^=#])'
                    loading               :    true,
                    loadingParentElement  :   'body', //animsition wrapper element
                    loadingClass          :   'animsition-loading',
                    unSupportCss          : [ 'animation-duration',
                        '-webkit-animation-duration',
                        '-o-animation-duration'
                    ],
                    //"unSupportCss" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
                    //The default setting is to disable the "animsition" in a browser that does not support "animation-duration".

                    overlay               :   false,

                    overlayClass          :   'animsition-overlay-slide',
                    overlayParentElement  :   'body'
                });
            });
        })(jQuery);



        $(document).ready(function() {
            $('#example').DataTable({
                "lengthMenu": [150],
            });
            MergeGridCells();

        } );
        function MergeGridCells() {
            var dimension_cells = new Array();
            var dimension_col = null;
            var columnCount = $("#example tr:first th").length;
            for (dimension_col = 0; dimension_col < columnCount; dimension_col++) {
                // first_instance holds the first instance of identical td
                var first_instance = null;
                var rowspan = 1;
                // iterate through rows
                $("#example").find('tr').each(function () {

                    // find the td of the correct column (determined by the dimension_col set above)
                    var dimension_td = $(this).find('td:nth-child(' + dimension_col + ')');

                    if (first_instance == null) {
                        // must be the first row
                        first_instance = dimension_td;
                    } else if (dimension_td.text() == first_instance.text() && first_instance.text()!='  English') {
                        console.log(first_instance.text())
                        // the current td is identical to the previous
                        // remove the current td
                        dimension_td.remove();
                        ++rowspan;
                        // increment the rowspan attribute of the first instance
                        first_instance.attr('rowspan', rowspan);
                    } else {
                        // this cell is different from the last
                        first_instance = dimension_td;
                        rowspan = 1;
                    }
                });
            }
        }
    </script>
    <script type="text/javascript" src="{{ asset('/js/jquery.easing.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.hidescroll.min.js') }}"></script>
    <script type="text/javascript">
        $('.header-top').hidescroll();
    </script>
    <script type="text/javascript" src="{{ asset('/js/smoothScroll.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.parallax-1.1.3.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/owl.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/scrollReveal.js') }}"></script>
    <script type="text/javascript">
        (function($) { "use strict";
            window.scrollReveal = new scrollReveal();
        })(jQuery);
    </script>
    <script type="text/javascript">
        (function($) { "use strict";
            jQuery(document).ready(function() {
                var offset = 450;
                var duration = 500;
                jQuery(window).scroll(function() {
                    if (jQuery(this).scrollTop() > offset) {
                        jQuery('.scroll-to-top').fadeIn(duration);
                    } else {
                        jQuery('.scroll-to-top').fadeOut(duration);
                    }
                });

                jQuery('.scroll-to-top').click(function(event) {
                    event.preventDefault();
                    jQuery('html, body').animate({scrollTop: 0}, duration);
                    return false;
                })
            });
        })(jQuery);
    </script>


    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5K6GL6T"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->



    <script type="text/javascript" src="{{ asset('/js/visible.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/pro-bars.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.counterup.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/waypoints.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/smk-accordion.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/styleswitcher.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/custom-about-1.js') }}"></script>

    <link rel="stylesheet" type="text/css" href="{{ asset('/DataTables/datatables.min.css') }}"/>

    <script type="text/javascript" src="{{ asset('/DataTables/datatables.min.js') }}"></script>
	<!-- End Document
================================================== -->
	@endsection
