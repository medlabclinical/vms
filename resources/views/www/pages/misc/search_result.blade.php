@extends('www.layouts.main')
@section('title')
    Search Result
@endsection
@section('breadcrumbs')
    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset;background-color:#7AA43F;">
            <ol class="breadcrumb medlab_breadcrumbs" style="margin-bottom: unset;background-color:#7AA43F;">
            </ol>
        </div>
    </div>

    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset">
            <ol class="breadcrumb medlab_breadcrumbs">
                <li><a class="medlab_breadcrumbs_link" href="/">@lang('trs.home')</a></li>
                <li class="active medlab_breadcrumbs_text">@lang('trs.search result')</li>
            </ol>
        </div>
    </div>
@endsection



@section('content')

    <main class="cd-main-content" id="main" style="margin-top:130px">


        <!-- TOP SECTION
        ================================================== -->

        <section class="section parallax-section" style="padding-bottom: unset;">
            <div class="container">
                <div class="sixteen columns">
                    <div class="section-title left">
                        <h1>Search Result</h1>
                        <div class="subtitle left big" style="text-transform: unset;font-family: unset;">
                            Found {{ $amount }} results for "{{ $search }}" across all content in all brand
                        </div>
                    </div>
                </div>
            </div>

        </section>


        <!-- SECTION
  ================================================== -->

        <section class="section white-section section-padding-top-bottom">
            <div class="shortcodes-carousel">

                <div id="sync-sortcodes-1" class="owl-carousel">
                    @if(count($url)!=0)
                        <div class="item white-section" id="explore">
                            @foreach($url as $item)
                                @if($item)
                                    <section class="section white-section section-padding-top-bottom"
                                             style="padding-bottom: unset;padding-top: 50px;">
                                        <div class="container">
                                            <div class="sixteen columns hidepart">
                                                <h4 style="text-align: left;">{{ $item->title }}&nbsp -
                                                    &nbsp{{ $item->subtitle }}</h4><br>
                                                <div class="hidepart one-row-max">{!! $item->content !!}</div>
                                                <br>
                                                <a href="{{ $item->page_url }}" style="text-decoration: underline;"
                                                   target="_blank">{{ $item->page_url }}</a>
                                            </div>
                                        </div>
                                    </section>
                                @endif
                            @endforeach


                        </div>
                    @endif
                    @if(count($people)!=0)
                        <div class="item white-section" id="people">
                            @foreach($people as $item)
                                <section class="section white-section section-padding-top-bottom">
                                    <div class="container">
                                        <div class="sixteen columns">
                                            <div
                                                class="blockquotes-box-1 grey-section blockquotes-float-content"
                                                style="max-width: 300px;padding: 10px;border: unset;">
                                                <img src="{{ $image_url }}/www/Images/{{ $item->picture }}"
                                                     alt="{{ $item->alt }}">
                                            </div>
                                            <div class="team-name-top mobile-inline-block"
                                                 style="font-family: 'Playball',crusive;">{{ $item->position }}</div>
                                            <h4 style="margin: 20px;font-size: x-large;text-align: unset;margin-left: unset;">{{ $item->name }}</h4>
                                            {!! $item->bio !!}
                                        </div>
                                    </div>
                                </section>

                            @endforeach
                        </div>
                    @endif
                    @if(count($partner)!=0)

                        <div class="item white-section" id="partner">

                            @foreach($partner as $item)
                                <section class="section white-section section-padding-top-bottom">
                                    <div class="container">
                                        <div class="sixteen columns">
                                            <div
                                                class="blockquotes-box-1 grey-section blockquotes-float-content"
                                                style="max-width: 300px;padding: 10px;border: unset;">
                                                <img src="{{ $image_url }}/www/Images/{{ $item->picture }}"
                                                     alt="{{ $item->alt }}">
                                            </div>
                                            <h4 style="margin: 20px;font-size: x-large;text-align: unset;margin-left: unset;display: inline-block;">{{ $item->name }}</h4>
                                            {!! $item->bio !!}
                                        </div>
                                    </div>
                                </section>

                            @endforeach


                        </div>
                    @endif
                    @if(count($article)!=0)

                        <div class="item white-section" id="article">
                            <div class="blog-wrapper">
                                <div id="blog-grid-masonry">
                                    @foreach($article as $item)
                                        @if($item->article_type=='link')
                                            <a href={{ route('www.articles_content',['id' => preg_replace('/--+/','-',preg_replace('/[^A-Za-z0-9\-]/', '-', $item->title)).'-'.$item->id]) }}>
                                                <div
                                                    class="blog-box-3 {{ str_replace(' ','_',$item['category']) }} {{ $item['article_type'] }} {{ $item['req_login'] }}">
                                                    <div class="blog-box-1 link-post">
                                                        <h4>{{ $item->title }}</h4>
                                                        <p>{{ $item->preview_text }}</p>
                                                        <p>{{ substr($item->date,0,10) }}</p>
                                                        @if(isset($item->author))<p>
                                                            Author: {{ $item->author }}</p>@endif
                                                        <div class="link">&#xf178;</div>
                                                    </div>
                                                </div>
                                            </a>
                                        @elseif($item->article_type=='video')
                                            <a href={{ route('www.articles_content',['id' => preg_replace('/--+/','-',preg_replace('/[^A-Za-z0-9\-]/', '-', $item->title)).'-'.$item->id]) }}>
                                                <div
                                                    class="blog-box-3  {{ str_replace(' ','_',$item['category']) }} {{ $item['article_type'] }} {{ $item['req_login'] }}">
                                                    <div class="blog-box-1 grey-section">
                                                        @if(($item->req_login && !(Auth::guard('practitioner')->check()||Auth::guard('patient')->check())))
                                                            <div style="opacity: 0.85;background: black;">
                                                                <iframe
                                                                    srcdoc="<p style='text-align:center;margin-top:23%;font-size:xxx-large;color:white'>Please login to view this video</p>"
                                                                    src="{{ $item->video_link }}" width="100%"
                                                                    height="270"
                                                                    allowfullscreen></iframe>
                                                            </div>
                                                        @else
                                                            <iframe src="{{ $item->video_link }}" width="100%"
                                                                    height="220"
                                                                    allowfullscreen></iframe>
                                                        @endif
                                                        {{--                                                <img src="{{ $image_url }}/www/Images/{{ $item->title_img }}"--}}
                                                        {{--                                                     alt="{{ $item->title_alt }}">--}}
                                                        <h4>{{ $item->title }}</h4>
                                                        <p>{{ $item->preview_text }}</p>
                                                        <p>{{ substr($item->date,0,10) }}</p>
                                                        @if(isset($item->author))<p>
                                                            Author: {{ $item->author }}</p>@endif
                                                        <div class="link">&#xf178;</div>
                                                    </div>
                                                </div>
                                            </a>
                                        @else
                                            <a href={{ route('www.articles_content',['id' => preg_replace('/--+/','-',preg_replace('/[^A-Za-z0-9\-]/', '-', $item->title)).'-'.$item->id]) }}>
                                                <div
                                                    class="blog-box-3  {{ str_replace(' ','_',$item['category']) }} {{ $item['article_type'] }} {{ $item['req_login'] }}">
                                                    <div class="blog-box-1 grey-section">
                                                        <img src="{{ $image_url }}/www/Images/{{ $item->title_img }}"
                                                             alt="{{ $item->title_alt }}">
                                                        <h4>{{ $item->title }}</h4>
                                                        <p>{{ $item->preview_text }}</p>
                                                        <p>{{ substr($item->date,0,10) }}</p>
                                                        @if(isset($item->author))<p>
                                                            Author: {{ $item->author }}</p>@endif
                                                        <div class="link">&#xf178;</div>
                                                    </div>
                                                </div>
                                            </a>
                                        @endif
                                    @endforeach
                                </div>
                            </div>


                        </div>
                    @endif
                    @if(count($all_products)!=0)
                        <div class="item white-section" id="all_products">
                            <section class="section white-section">
                                <div class="container">
                                    <div id="shop-grid">
                                        @foreach($all_products as $item)
                                            <div class="one-quarter column" style="height: 500px;">
                                                <a href={{ route('www.product_page', ['id' => $item->id]) }}>
                                                    <div class="shop-box grey-section"
                                                         style="height: 500px;width: 380px;text-align: center">
                                                        @if($item->product()->first()->unapproved_drug=='1' && !(Auth::guard('practitioner')->check()))
                                                            <img
                                                                src="https://cdn.medlab.co/www/Images/demo_drug.png"
                                                                style="display: inline-block" alt=""/>
                                                        @else
                                                            <img
                                                                src="https://cdn.medlab.co/{{ $item->product()->first()->img_front_view_small }}"
                                                                style="display: inline-block" alt=""/>
                                                        @endif
                                                        <div>
                                                            <h2 style="text-align: center">{{ $item->product_name }}</h2>
                                                            <br>
                                                            <div class="subtitle -align-center"
                                                                 style="font-size: 12px;padding: unset">{{ $item->product_heading }}</div>
                                                            <br>
                                                            <div class="subtitle -align-center"
                                                                 style="font-size: 12px;padding: unset;text-transform: unset">{{ $item->product_byline }}</div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </section>
                        </div>
                    @endif
                    @if(count($product_ingredient)!=0)

                        <div class="item white-section" id="product_ingredient">
                            <section class="section white-section">
                                <div class="container">
                                    <div class="clear"></div>
                                    <div id="shop-grid">
                                        @foreach($product_ingredient as $item)
                                            <div class="one-quarter column" style="height: 500px;">
                                                <a href={{ route('www.product_page', ['id' => $item->id]) }}>
                                                    <div class="shop-box grey-section"
                                                         style="height: 500px;width: 380px;text-align: center">

                                                        @if($item->product()->first()->unapproved_drug=='1' && !(Auth::guard('practitioner')->check()||Auth::guard('patient')->check()))
                                                            <img
                                                                src="https://cdn.medlab.co/www/Images/demo_drug.png"
                                                                style="display: inline-block" alt=""/>
                                                        @else
                                                            <img
                                                                src="https://cdn.medlab.co/{{ $item->product()->first()->img_front_view_small }}"
                                                                style="display: inline-block" alt=""/>
                                                        @endif
                                                        <div>
                                                            <h2 style="text-align: center">{{ $item->product_name }}</h2>
                                                            <br>
                                                            <div class="subtitle -align-center"
                                                                 style="font-size: 12px;padding: unset">{{ $item->product_heading }}</div>
                                                            <br>
                                                            <div class="subtitle -align-center"
                                                                 style="font-size: 12px;padding: unset;text-transform: unset">{{ $item->product_byline }}</div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </section>


                        </div>
                    @endif
                    @if(count($publications)!=0)
                        <div class="item white-section" id="publications">
                            <section class="section white-section">
                                <div class="container">

{{--                                    <div class="item grey-section">--}}

                                        <br>
                                        @foreach($publications as $year)
                                            @foreach($year as $item)
                                                <div class="accordion">
                                                    <div class="accordion_in"
                                                         data-scroll-reveal="enter left move 200px over 1s after 0.3s">
                                                        <div class="acc_head white-section"
                                                             style="text-transform: unset;">{{$item->title}}</div>
                                                        <div class="acc_content white-section">
                                                            @if($item->publication_location!='' && $item->publication_location!='Null')
                                                                <p>{{ $item->publication_location }}</p><br>
                                                            @endif
                                                            @if($item->date!='' && $item->date!='Null')
                                                                <p><strong>Date:</strong>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp{{ substr($item->date,0,10) }}
                                                                </p><br>
                                                            @endif

                                                            @if(count($item->member))
                                                                <p><strong>Authors:</strong>&nbsp&nbsp&nbsp
                                                                    @if(count($item->member))
                                                                        {{$item->member[0]['name']}}
                                                                        @if(count($item->member)!=1)
                                                                            @for($i=1;$i<count($item->member);$i++)
                                                                                ,&nbsp{{ $item->member[$i]['name'] }}
                                                                            @endfor
                                                                        @endif
                                                                    @endif
                                                                </p><br>
                                                            @endif
                                                            @if($item->link!='' && $item->link!='Null')
                                                                <p><strong>Link:</strong>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<a
                                                                        style="color: #6ba53a;word-break: break-word;"
                                                                        href="{{ $item->link }}"
                                                                        target="_blank">{{ $item->link }}</a>
                                                                </p>
                                                            @endif
                                                            @if(count($item->article) || count($item->product_family) || count($item->clinical_trial) || count($item->product_ingredient) || count($item->product_group))
                                                                <br><br>
                                                                <hr style="margin:unset;"/>
                                                                <br><br>
                                                                @if(count($item->article))
                                                                    <p><strong>Related Articles</strong></p><br>
                                                                    @foreach($item->article as $a)
                                                                        <a style="color: #6ba53a;word-break: break-word;"
                                                                           href={{ route('www.articles_content',['id' => $a['id']]) }} target="_blank">{{ $a['title'] }}</a>
                                                                        <br><br>
                                                                    @endforeach
                                                                @endif
                                                                @if(count($item->product_family))
                                                                    <p><strong>Related Products</strong></p><br>
                                                                    @foreach($item->product_family as $a)
                                                                        <a>{{ $a['product_name'] }}</a>
                                                                        <br><br>
                                                                    @endforeach
                                                                @endif
                                                                @if(count($item->clinical_trial))
                                                                    <p><strong>Related Clinical Trials</strong>
                                                                    </p><br>
                                                                    @foreach($item->clinical_trial as $a)
                                                                        <a>{{ $a['name'] }}</a>
                                                                        <br><br>
                                                                    @endforeach
                                                                @endif
                                                                @if(count($item->product_ingredient))
                                                                    <p><strong>Related Ingredients</strong></p>
                                                                    <br>
                                                                    @foreach($item->product_ingredient as $a)
                                                                        <a>{{ $a['name'] }}</a>
                                                                        <br><br>
                                                                    @endforeach
                                                                @endif
                                                                @if(count($item->product_group))
                                                                    <p><strong>Related Product Groups</strong>
                                                                    </p><br>
                                                                    @foreach($item->product_group as $a)
                                                                        <a>{{ $a['name'] }}</a>
                                                                        <br><br>
                                                                    @endforeach
                                                                @endif
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endforeach
                                    </div>
{{--                                </div>--}}
                            </section>
                        </div>

                    @endif


                </div>

                {{--                        <div class="container">--}}
                <div id="sync-sortcodes-2" class="owl-carousel">
                    @if(count($url)!=0)
                        <div class="item" onclick="setExplore()">
                            <h3>Explore</h3>
                        </div>
                    @endif
                    @if(count($people)!=0)
                        <div class="item" onclick="setPeople()">
                            <h3>People</h3>
                        </div>
                    @endif
                    @if(count($partner)!=0)
                        <div class="item" onclick="setPartner()">
                            <h3>Partners</h3>
                        </div>
                    @endif
                    @if(count($article)!=0)
                        <div class="item" onclick="setArticle()">
                            <h3>Articles</h3>
                        </div>
                    @endif
                    @if(count($all_products)!=0)
                        <div class="item" onclick="setResult()">
                            <h3>Products</h3>
                        </div>
                    @endif
                    @if(count($product_ingredient)!=0)
                        <div class="item" onclick="setIngredient()">
                            <h3>Ingredients</h3>
                        </div>
                    @endif
                    @if(count($publications)!=0)
                        <div class="item" onclick="setPublications()">
                            <h3>Publications</h3>
                        </div>
                    @endif
                </div>
            </div>
            {{--                    </div>--}}

            {{--                </div>--}}
            {{--            </div>--}}

        </section>
        @include('www.layouts.footer')


    </main>

    <div class="scroll-to-top">&#xf106;</div>





    <!-- JAVASCRIPT
   ================================================== -->
    <script type="text/javascript" src="{{ asset('/js/jquery-2.1.1.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/modernizr.custom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.mobile.custom.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/retina-1.1.0.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.animsition.min.js') }}"></script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            $(document).ready(function () {
                jQuery(document).ready(function ($) {
                    $(".accordion").smk_Accordion({
                        closeAble: true, //boolean
                    });
                });

                @if (session('login_error'))
                open_login();
                @endif
                $(".animsition").animsition({

                    inClass: 'zoom-in-sm',
                    outClass: 'zoom-out-sm',
                    inDuration: 1500,
                    outDuration: 800,
                    linkElement: '.animsition-link',
                    // e.g. linkElement   :   'a:not([target="_blank"]):not([href^=#])'
                    loading: true,
                    loadingParentElement: 'body', //animsition wrapper element
                    loadingClass: 'animsition-loading',
                    unSupportCss: ['animation-duration',
                        '-webkit-animation-duration',
                        '-o-animation-duration'
                    ],
                    //"unSupportCss" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
                    //The default setting is to disable the "animsition" in a browser that does not support "animation-duration".

                    overlay: false,

                    overlayClass: 'animsition-overlay-slide',
                    overlayParentElement: 'body'
                });
            });
        })(jQuery);

        //Tabs 1

        $(document).ready(function () {

            var sync1 = $("#sync-sortcodes-1");
            var sync2 = $("#sync-sortcodes-2");

            sync1.owlCarousel({
                singleItem: true,
                transitionStyle: "backSlide",
                slideSpeed: 1500,
                navigation: false,
                pagination: false,
                afterAction: syncPosition,
                responsiveRefreshRate: 200
            });


            sync2.owlCarousel({
                items: '{{ $tab_amount }}',
                itemsDesktop: [1199, {{ $tab_amount }}],
                itemsDesktopSmall: [979, {{ $tab_amount }}],
                itemsTablet: [768, {{ $tab_amount }}],
                itemsMobile: [479, {{ $tab_amount }}],
                pagination: false,
                responsiveRefreshRate: 100,
                afterInit: function (el) {
                    el.find(".owl-item").eq(0).addClass("synced");
                }
            });

            function syncPosition(el) {
                var current = this.currentItem;
                $("#sync-sortcodes-2")
                    .find(".owl-item")
                    .removeClass("synced")
                    .eq(current)
                    .addClass("synced")
                if ($("#sync-sortcodes-2").data("owlCarousel") !== undefined) {
                    center(current)
                }
            }

            $("#sync-sortcodes-2").on("click", ".owl-item", function (e) {
                e.preventDefault();
                var number = $(this).data("owlItem");
                sync1.trigger("owl.goTo", number);
            });

            function center(number) {
                var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
                var num = number;
                var found = false;
                for (var i in sync2visible) {
                    if (num === sync2visible[i]) {
                        var found = true;
                    }
                }

                if (found === false) {
                    if (num > sync2visible[sync2visible.length - 1]) {
                        sync2.trigger("owl.goTo", num - sync2visible.length + 2)
                    } else {
                        if (num - 1 === -1) {
                            num = 0;
                        }
                        sync2.trigger("owl.goTo", num);
                    }
                } else if (num === sync2visible[sync2visible.length - 1]) {
                    sync2.trigger("owl.goTo", sync2visible[1])
                } else if (num === sync2visible[0]) {
                    sync2.trigger("owl.goTo", num - 1)
                }

            }

                @if(count($url)!=0)
            var height = document.getElementById('explore').offsetHeight + 60;
            document.getElementById('sync-sortcodes-1').style.height = height + 'px';
                @elseif(count($people)!=0)
            var height = document.getElementById('people').offsetHeight + 60;
            document.getElementById('sync-sortcodes-1').style.height = height + 'px';
                @elseif(count($partner)!=0)
            var height = document.getElementById('partner').offsetHeight + 60;
            document.getElementById('sync-sortcodes-1').style.height = height + 'px';
                @elseif(count($article)!=0)
            var height = document.getElementById('article').offsetHeight + 60;
            document.getElementById('sync-sortcodes-1').style.minHight = height + 'px';
                @elseif(count($all_products)!=0)
            var height = document.getElementById('all_products').offsetHeight + 60;
            document.getElementById('sync-sortcodes-1').style.height = height + 'px';
                @elseif(count($product_ingredient)!=0)
            var height = document.getElementById('product_ingredient').offsetHeight + 60;
            document.getElementById('sync-sortcodes-1').style.height = height + 'px';
                @elseif(count($publications)!=0)
            var height = document.getElementById('publications').offsetHeight + 60;
            document.getElementById('sync-sortcodes-1').style.minHight = height + 'px';
            @else
            var height = 60;
            document.getElementById('sync-sortcodes-1').style.minHight = height + 'px';
            @endif

        });


        //Tabs 2

        $(document).ready(function () {

            var sync1 = $("#sync-sortcodes-3");
            var sync2 = $("#sync-sortcodes-4");

            sync1.owlCarousel({
                singleItem: true,
                autoPlay: 3000,
                slideSpeed: 1500,
                navigation: false,
                pagination: false,
                afterAction: syncPosition,
                responsiveRefreshRate: 200
            });


            sync2.owlCarousel({
                items: 6,
                itemsDesktop: [1199, 6],
                itemsDesktopSmall: [979, 6],
                itemsTablet: [768, 6],
                itemsMobile: [479, 6],
                pagination: false,
                responsiveRefreshRate: 100,
                afterInit: function (el) {
                    el.find(".owl-item").eq(0).addClass("synced");
                }
            });

            function syncPosition(el) {
                var current = this.currentItem;
                $("#sync-sortcodes-4")
                    .find(".owl-item")
                    .removeClass("synced")
                    .eq(current)
                    .addClass("synced")
                if ($("#sync-sortcodes-4").data("owlCarousel") !== undefined) {
                    center(current)
                }
            }

            $("#sync-sortcodes-4").on("click", ".owl-item", function (e) {
                e.preventDefault();
                var number = $(this).data("owlItem");
                sync1.trigger("owl.goTo", number);
            });

            function center(number) {
                var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
                var num = number;
                var found = false;
                for (var i in sync2visible) {
                    if (num === sync2visible[i]) {
                        var found = true;
                    }
                }

                if (found === false) {
                    if (num > sync2visible[sync2visible.length - 1]) {
                        sync2.trigger("owl.goTo", num - sync2visible.length + 2)
                    } else {
                        if (num - 1 === -1) {
                            num = 0;
                        }
                        sync2.trigger("owl.goTo", num);
                    }
                } else if (num === sync2visible[sync2visible.length - 1]) {
                    sync2.trigger("owl.goTo", sync2visible[1])
                } else if (num === sync2visible[0]) {
                    sync2.trigger("owl.goTo", num - 1)
                }

            }

        });
        function setExplore() {
            var height = document.getElementById('explore').offsetHeight + 60;
            document.getElementById('sync-sortcodes-1').style.minHeight = null;
            document.getElementById('sync-sortcodes-1').style.height = height + 'px';
        }

        function setPeople() {
            var height = document.getElementById('people').offsetHeight + 60;
            document.getElementById('sync-sortcodes-1').style.minHeight = null;
            document.getElementById('sync-sortcodes-1').style.height = height + 'px';
        }

        function setPartner() {
            var height = document.getElementById('partner').offsetHeight + 60;
            document.getElementById('sync-sortcodes-1').style.minHeight = null;
            document.getElementById('sync-sortcodes-1').style.height = height + 'px';
        }

        function setArticle() {
            var height = document.getElementById('article').offsetHeight + 60;
            document.getElementById('sync-sortcodes-1').style.height = null;
            document.getElementById('sync-sortcodes-1').style.minHeight = height + 'px';

        }

        function setResult() {
            var height = document.getElementById('all_products').offsetHeight + 60;
            document.getElementById('sync-sortcodes-1').style.minHeight = null;
            document.getElementById('sync-sortcodes-1').style.height = height + 'px';
        }

        function setIngredient() {
            var height = document.getElementById('product_ingredient').offsetHeight + 60;
            document.getElementById('sync-sortcodes-1').style.minHeight = null;
            document.getElementById('sync-sortcodes-1').style.height = height + 'px';
        }

        function setPublications() {
            var height = document.getElementById('publications').offsetHeight + 60;
            document.getElementById('sync-sortcodes-1').style.height = null;
            document.getElementById('sync-sortcodes-1').style.minHeight = height + 'px';
        }
    </script>
    <script type="text/javascript" src="{{ asset('/js/jquery.easing.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.hidescroll.min.js') }}"></script>
    <script type="text/javascript">
        $('.header-top').hidescroll();
    </script>
    {{--    <script type="text/javascript" src="{{ asset('/js/smoothScroll.js') }}"></script>--}}
    <script type="text/javascript" src="{{ asset('/js/jquery.parallax-1.1.3.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/owl.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/scrollReveal.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/masonry.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/isotope.js') }}"></script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            window.scrollReveal = new scrollReveal();
        })(jQuery);
    </script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            jQuery(document).ready(function () {
                var offset = 450;
                var duration = 500;
                jQuery(window).scroll(function () {
                    if (jQuery(this).scrollTop() > offset) {
                        jQuery('.scroll-to-top').fadeIn(duration);
                    } else {
                        jQuery('.scroll-to-top').fadeOut(duration);
                    }
                });

                jQuery('.scroll-to-top').click(function (event) {
                    event.preventDefault();
                    jQuery('html, body').animate({scrollTop: 0}, duration);
                    return false;
                })
            });
        })(jQuery);
    </script>

    <script type="text/javascript" src="{{ asset('/js/styleswitcher.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.themepunch.tools.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.themepunch.revolution.min.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('/DataTables/datatables.min.css') }}"/>
    <script type="text/javascript" src="{{ asset('/js/smk-accordion.js') }}"></script>

    <script type="text/javascript" src="{{ asset('/DataTables/datatables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/custom-corporate-home-1.js') }}"></script>



    <!-- End Document
================================================== -->

@endsection
