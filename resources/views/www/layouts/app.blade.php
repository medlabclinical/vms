<!DOCTYPE html>
<!--[if IE 8]>
<html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]>
<!-->
<html class="no-js" lang="en"><!--<![endif]-->
{{--<head>--}}

<meta name="google-site-verification" content="E6Z2w_wPne5Z223-U298s1K1BEHyKl6oqq40JLYoxzU"/>
<!-- Google Tag Manager -->
{{--<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':--}}
{{--            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],--}}
{{--        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=--}}
{{--        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);--}}
{{--    })(window,document,'script','dataLayer','GTM-TP3N8DM');</script>--}}
<!-- End Google Tag Manager -->

<!-- Basic Page Needs
  ================================================== -->
<meta charset="utf-8">
<title>@yield('title')</title>
<meta name="description" content="@yield('description')">
<meta name="keywords" content="@yield('keywords')">

<!-- Mobile Specific Metas
================================================== -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">


<!-- CSS
================================================== -->
<link rel="stylesheet" href="{{ asset('/css/base.css') }}"/>
<link rel="stylesheet" href="{{ asset('/css/skeleton.css') }}"/>
<link rel="stylesheet" href="{{ asset('/css/layout.css') }}"/>
<link rel="stylesheet" href="{{ asset('/css/settings.css') }}"/>
<link rel="stylesheet" href="{{ asset('/css/font-awesome.css') }}"/>
<link rel="stylesheet" href="{{ asset('/css/owl.carousel.css') }}"/>
<link rel="stylesheet" href="{{ asset('/css/retina.css') }}"/>
{{--<link rel="stylesheet" href="{{ asset('/css/animsition.min.css') }}"/>--}}

<link rel="alternate stylesheet" type="text/css" href="{{ asset('/css/colors/color-gold.css') }}" title="1">
{{--<link rel="alternate stylesheet" type="text/css" href="{{ asset('/css/colors/color-2.css') }}" title="2">--}}
{{--<link rel="alternate stylesheet" type="text/css" href="{{ asset('/css/colors/color-3.css') }}" title="3">--}}
{{--<link rel="alternate stylesheet" type="text/css" href="{{ asset('/css/colors/color-4.css') }}" title="4">--}}
{{--<link rel="alternate stylesheet" type="text/css" href="{{ asset('/css/colors/color-5.css') }}" title="5">--}}
{{--<link rel="alternate stylesheet" type="text/css" href="{{ asset('/css/colors/color-6.css') }}" title="6">--}}
{{--<link rel="alternate stylesheet" type="text/css" href="{{ asset('/css/colors/color-7.css') }}" title="7">--}}


<!-- Favicons
================================================== -->
<link rel="shortcut icon" href="{{ asset('/images/favicon.png')}}">
<link rel="apple-touch-icon" href="{{ asset('/images/apple-touch-icon.png')}}">
<link rel="apple-touch-icon" sizes="72x72" href="{{ asset('/images/apple-touch-icon-72x72.png')}}">
<link rel="apple-touch-icon" sizes="114x114" href="{{ asset('/images/apple-touch-icon-114x114.png')}}">

<link rel="stylesheet" type="text/css" href="{{ asset('/css/demo.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('/css/style2.css')}}"/>
{{--<link rel="stylesheet" type="text/css" href="{{ asset('/css/animate-custom.css')}}" />--}}

{{--<link rel="stylesheet" type="text/css" href="{{ asset('/vendor/bootstrap/css/bootstrap.min.css')}}">--}}
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="{{ asset('/fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
<!--===============================================================================================-->
{{--<link rel="stylesheet" type="text/css" href="{{ asset('/vendor/animate/animate.css')}}">--}}
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="{{ asset('/vendor/css-hamburgers/hamburgers.min.css')}}">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="{{ asset('/vendor/select2/select2.min.css')}}">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="{{ asset('/css/main.css')}}">
<script type="text/javascript"
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA806a5QISlttwcTojqi6lfSp1OuBk8UWU&libraries=places"></script>
<link rel="stylesheet" href="{{ asset('/build/css/intlTelInput.css') }}">

<script>
    dataLayer = [];
</script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-TP3N8DM');</script>
<!-- End Google Tag Manager -->
