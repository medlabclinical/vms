<!-- FOOTER
    ================================================== -->


<!-- SOCIAL MEDIA ICONS
================================================== -->

<section class="section white-section section-padding-top-bottom" style="height: 160px;padding-top: 50px;padding-bottom: 50px;z-index: -1">

    <div class="container">
        <div class="sixteen columns">
            <div class="social-contact">
                <ul class="list-contact" style="padding: 0px;">
                    <li class="contact-soc">
                        <a class="tooltip-shop" href="https://www.facebook.com/medlabAUS" target="view_window">&#xf09a;<span class="tooltip-content-shop"><span class="tooltip-text-shop"><span class="tooltip-inner-shop">follow us</span></span></span></a>
                    </li>
                    <li class="contact-soc">
                        <a class="tooltip-shop" href="https://twitter.com/medlabclinical" target="view_window">&#xf099;<span class="tooltip-content-shop"><span class="tooltip-text-shop"><span class="tooltip-inner-shop">like us</span></span></span></a>
                    </li>
                    <li class="contact-soc">
                        <a class="tooltip-shop" href="https://www.instagram.com/medlab_clinical/" target="view_window">&#xf16d;<span class="tooltip-content-shop"><span class="tooltip-text-shop"><span class="tooltip-inner-shop">stay up to date</span></span></span></a>
                    </li>
                    <li class="contact-soc">
                        <a class="tooltip-shop" href="https://www.youtube.com/channel/UCLZnBKwF4FpBd_1VNsX3xDA" target="view_window">&#xf16a;<span class="tooltip-content-shop"><span class="tooltip-text-shop"><span class="tooltip-inner-shop">+1 us</span></span></span></a>
                    </li>
                    <li class="contact-soc">
                        <a class="tooltip-shop" href="https://vimeo.com/medlabclinical" target="view_window">&#xf1ca;<span class="tooltip-content-shop"><span class="tooltip-text-shop"><span class="tooltip-inner-shop">+1 us</span></span></span></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

</section>

<!-- FOOTER SECTION
================================================== -->

<section class="section footer-1 section-padding-top-bottom">
    <div class="container">

        <div class="four columns" data-scroll-reveal="enter left move 200px over 0.5s after 0.8s">
            <h5><i class="icon-footer">&#xf041;</i>Corporate Offices</h5>
            <p>Unit 5-6, 11 Lord St<br/>Botany, NSW<br/>Australia 2019</p>
            <a href="{{ route('contact') }}">Regional and International offices</a>
        </div>
        <div class="four columns" data-scroll-reveal="enter left move 200px over 0.5s after 0.3s">
            <h5><i class="icon-footer">&#xf199;</i>Contact Details</h5>
            <p>Office Hours: 8:30am - 5pm EST</p>
            <a href="tel:1300 369 570">AUS: 1300 369 570</a><br/>
            <a href="tel:+61 2 8188 0311">AUS: +61 2 8188 0311</a><br/>
            <a href="tel:+1 947 771 8119">USA: +1 947 771 8119</a><br/>
            <a href="tel:+44 20 8138 0311">UK: +44 20 8138 0311</a><br/>
            <a href="fax:+61 2 9699 3347">FAX: +61 2 9699 3347</a><br/>
            <a href="mailto:hello@medlab.co">Email: hello@medlab.co</a>
        </div>
        <div class="four columns" data-scroll-reveal="enter right move 200px over 0.5s after 0.3s">
            <h5><i class="icon-footer">&#xf0c1;</i>Our Websites</h5>
            <a href="https://dev-www.medlab.co" style="color: white;">www.medlab.co</a><br/>
            <a href="https://shop.medlab.co" style="color: white;">shop.medlab.co</a><br/>
            <a href="https://clinic.medlab.co" style="color: white;">clinic.medlab.co</a>

        </div>
        <div class="four columns" data-scroll-reveal="enter right move 200px over 0.5s after 0.8s">
            <h5><i class="icon-footer">&#xf0f6;</i>Policies</h5>
            <a href="{{route('www.privacy_policy')}}" style="color: white;">Privacy Policy</a><br/>
            <a href="{{route('www.shipping_and_delivery')}}" style="color: white;">Shipping and Delivery</a><br/>
            <a href="{{route('www.returns')}}" style="color: white;">Returns</a><br/>
            <a href="{{route('www.sales_policy')}}" style="color: white;">Sales Policy</a>
        </div>
    </div>
</section>
<section class="section footer-bottom">
    <div class="container">
        <div class="sixteen columns">
            <p>All Contents Copyright © 2020 Medlab Clinical® All rights reserved</p>
        </div>
    </div>
</section>



