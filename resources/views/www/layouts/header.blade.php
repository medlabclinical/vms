@include('www.layouts.app')


<!-- MENU
    ================================================== -->

<div class="header-top">

    <header id="header" class="cd-main-header">
        <meta name="csrf-token" content="{{ csrf_token() }}"/>
        <a class="cd-logo animsition-link" href="/"><img src="{{ asset('/images/logo.png')}}" alt="Logo"></a>

        <ul class="cd-header-buttons">
            <li><a class="cd-search-trigger" href="#cd-search"><span></span></a></li>
            <li><a id="nav" class="cd-nav-trigger" href="#cd-primary-nav"><span></span></a></li>
        </ul>
    </header>
    <nav class="cd-nav">
        <ul id="cd-primary-nav" class="cd-primary-nav is-fixed">

            <li class="has-children">
                <a href="#" style="font-size: medium">About & Partnering</a>

                <ul class="cd-secondary-nav is-hidden">
                    <li class="go-back"><a href="#">@lang('trs.menu')</a></li>
                    <li class="see-all"><a href="/">@lang('trs.welcome to medlab')</a></li>
                    <li class="has-children">
                        <a href="#">About</a>

                        <ul class="is-hidden" style="padding: unset;">
                            <li class="go-back"><a href="#">About</a></li>
                            <li><a href="{{ route('about') }}" class="animsition-link">@lang('trs.about us')</a></li>
                            <li><a href="{{ route('contact') }}" class="animsition-link">@lang('trs.contact us')</a></li>
                        </ul>
                    </li>

                    <li class="has-children">
                        <a href="#">Organisation</a>

                        <ul class="is-hidden" style="padding: unset;">
                            <li class="go-back"><a href="#">Organisation</a></li>
                            <li><a href="{{ route('leadership_team') }}" class="animsition-link">Leadership Team</a></li>
                            <li><a href="{{ route('consulting_team') }}" class="animsition-link">Scientific Consulting Team</a></li>
                        </ul>
                    </li>

                    <li class="has-children">
                        <a href="#">Partnering</a>

                        <ul class="is-hidden" style="padding: unset;">
                            <li class="go-back"><a href="#">Partnering</a></li>
                            <li><a href="{{ route('commercial_partners') }}" class="animsition-link">@lang('trs.commercial partners')</a></li>
                            <li><a href="{{ route('collaborations') }}" class="animsition-link">Collaborations</a></li>
                            <li><a href="{{ route('industry_associations') }}" class="animsition-link">@lang('trs.industry associations')</a></li>
                            <li><a href="{{ route('partner_opportunities') }}" class="animsition-link">Partner Opportunities</a></li>

                        </ul>
                    </li>


                </ul>
            </li>
            <li class="has-children">
                <a href="#" style="font-size: medium">@lang('trs.research')</a>
                <ul class="cd-nav-icons is-hidden">
                    <li class="go-back"><a href="#">@lang('trs.menu')</a></li>
                    <li class="see-all"><a href="/" class="animsition-link">@lang('trs.welcome to medlab')</a></li>

                    <li>
                        <a class="cd-nav-item item-5 animsition-link" href="{{ route('nanocelle_technology') }}">
                            <h5>@lang('trs.nanocelle technology')</h5>
                        </a>
                    </li>

                    <li>
                        <a class="cd-nav-item item-6 animsition-link" href="{{ route('clinical_trials') }}">
                            <h5>@lang('trs.clinical trials')</h5>
                        </a>
                    </li>

                    <li>
                        <a class="cd-nav-item item-7 animsition-link" href="{{ route('pc2') }}">
                            <h5>@lang('trs.pc2')</h5>
                        </a>
                    </li>
                    <li>
                        <a class="cd-nav-item item-14 animsition-link" href="{{ route('publications') }}">
                            <h5>@lang('trs.publications')</h5>
                        </a>
                    </li>


                </ul>
            </li>
            <li class="has-children">
                <a href="#" style="font-size: medium">@lang('trs.education')</a>

                <ul class="cd-secondary-nav is-hidden">
                    <li class="go-back"><a href="#">@lang('trs.menu')</a></li>
                    <li class="see-all"><a href="/">@lang('trs.welcome to medlab')</a></li>
                    <li class="has-children">
                        <a href="#">Articles</a>

                        <ul class="is-hidden" style="padding: unset;">
                            <li class="go-back"><a href="#">Articles</a></li>
                            <li><a href="{{ route('www.articles') }}" class="animsition-link">Show All</a></li>
                            @foreach($header_article_category as $item)
                                <li><a href="{{ route('www.articles') }}?article_category={{ $item }}"
                                       class="animsition-link">{{ $item }}</a></li>
                            @endforeach

                        </ul>
                    </li>

                    <li class="has-children">
                        <a href="#">Flyers</a>

                        <ul class="is-hidden" style="padding: unset;">
                            <li class="go-back"><a href="#">Flyers</a></li>
                            <li><a href="{{ route('www.flyers') }}" class="animsition-link">Show All</a></li>
                            @foreach($header_pdf_list as $item)
                                <li><a href="{{ route('www.flyers') }}?pdf_type={{ $item->name }}"
                                       class="animsition-link">{{ $item->name }}</a></li>
                            @endforeach
                        </ul>
                    </li>


                </ul>
            </li>
            <li class="has-children">
                <a href="#" style="font-size: medium">@lang('trs.products')</a>

                <ul class="cd-nav-gallery is-hidden">
                    <li class="go-back"><a href="#">@lang('trs.menu')</a></li>
                    <li class="see-all"><a href="/"
                                           class="animsition-link">@lang('trs.welcome to medlab')</a>
                    </li>
                    <li>
                        <a class="cd-nav-item" onclick="redirectInfo(this.id)" href="javascript:;"
                           id="{{ \App\Models\Subweb::where('id',1)->first()->link }}">
                            <img
                                src="{{ config("app.image_url") }}/www/Images/{{ \App\Models\Subweb::where('id',1)->first()->image()->first()->name }}"
                                alt="Product Image" id="myPopup">
                            <h5 style="font-size: small;">Vitamins and supplements</h5>
                        </a>
                    </li>
                    <li>
                        <a class="cd-nav-item" onclick="redirectInfo(this.id)" href="javascript:;"
                           id="{{ \App\Models\Subweb::where('id',2)->first()->link }}">
                            <img
                                src="{{ config("app.image_url") }}/www/Images/{{ \App\Models\Subweb::where('id',2)->first()->image()->first()->name }}"
                                alt="Product Image" id="myPopup">
                            <h5 style="font-size: small;">Virtual Clinic</h5>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('www.products') }}" class="cd-nav-item">
                            <img
                                src="{{ config("app.image_url") }}/www/Images/pharma-main-1583399462CLvpr.jpg"
                                alt="Product Image" id="myPopup">
                            <h5 style="font-size: small;">Pharmaceuticals</h5>
                        </a>
                    </li>
{{--                    <li>--}}
{{--                        <a href="{{ route('products_in_development_page') }}" class="cd-nav-item">--}}
{{--                            <img--}}
{{--                                src="{{ config("app.image_url") }}/www/Images/pharma-main-1583399462CLvpr.jpg"--}}
{{--                                alt="Product Image" id="myPopup">--}}
{{--                            <h5 style="font-size: small;">Products In Development</h5>--}}
{{--                        </a>--}}
{{--                    </li>--}}
                </ul>
            </li>

            <li class="has-children">
                <a href="#" style="font-size: medium">@lang('trs.investors')</a>
                <ul class="cd-nav-icons is-hidden">
                    <li class="go-back"><a href="#">@lang('trs.menu')</a></li>
                    <li class="see-all"><a href="/" class="animsition-link">@lang('trs.welcome to medlab')</a></li>
                    <li>
                        <a class="cd-nav-item item-7 animsition-link" href="{{ route('share_price') }}">
                            <h5>@lang('trs.share price')</h5>
                        </a>
                    </li>

                    <li>
                        <a class="cd-nav-item item-5 animsition-link" href="{{ route('corporate_governance') }}">
                            <h5>@lang('trs.corporate governance')</h5>
                        </a>
                    </li>

                    <li>
                        <a class="cd-nav-item item-6 animsition-link" href="{{ route('asx_announcement') }}">
                            <h5>@lang('trs.asx announcement')</h5>
                        </a>
                    </li>


                </ul>
            </li>
            @if(Auth::guard('practitioner')->check())
                <li class="has-children">
                    <a href="#" style="font-size: medium"><i
                            class="fa fa-fw">&#xf090;</i>Hi&nbsp;{{ Auth::guard('practitioner')->user()->first_name }}
                    </a>
                    <ul class="cd-nav-icons is-hidden">
                        <li class="go-back"><a href="#">@lang('trs.menu')</a></li>
                        <li class="see-all"><a href="/" class="animsition-link">@lang('trs.welcome to medlab')</a>
                        </li>
{{--                        <li>--}}
{{--                            <a class="cd-nav-item item-7 animsition-link" href="#">--}}
{{--                                <h5>Order History</h5>--}}
{{--                            </a>--}}
{{--                        </li>--}}

                        <li>
                            <a class="cd-nav-item item-5 animsition-link" href="{{ route('practitioner-profile') }}">
                                <h5>My profile</h5>
                            </a>
                        </li>
                        <li>
                            <a class="cd-nav-item item-5 animsition-link" href="{{ route('practitioner-password-page') }}">
                                <h5>Change Password</h5>
                            </a>
                        </li>
                        <li>
                            <a class="cd-nav-item item-5 animsition-link" href="{{ route('change-referral-code') }}">
                                <h5>My Referral Code</h5>
                            </a>
                        </li>

                        <li>
                            <a class="cd-nav-item item-15 animsition-link" href="{{ route('logout') }}">
                                <h5>Logout</h5>
                            </a>
                        </li>
                    </ul>
                </li>
                @elseif(Auth::guard('patient')->check())
                <li class="has-children">
                    <a href="#" style="font-size: medium"><i
                            class="fa fa-fw">&#xf090;</i>Hi&nbsp;{{ Auth::guard('patient')->user()->first_name }}
                    </a>
                    <ul class="cd-nav-icons is-hidden">
                        <li class="go-back"><a href="#">@lang('trs.menu')</a></li>
                        <li class="see-all"><a href="/" class="animsition-link">@lang('trs.welcome to medlab')</a>
                        </li>
{{--                        <li>--}}
{{--                            <a class="cd-nav-item item-7 animsition-link" href="#">--}}
{{--                                <h5>Order History</h5>--}}
{{--                            </a>--}}
{{--                        </li>--}}

                        <li>
                            <a class="cd-nav-item item-5 animsition-link" href="{{ route('patient-profile') }}">
                                <h5>My profile</h5>
                            </a>
                        </li>
                        <li>
                            <a class="cd-nav-item item-5 animsition-link" href="{{ route('patient-password-page') }}">
                                <h5>Change Password</h5>
                            </a>
                        </li>

                        <li>
                            <a class="cd-nav-item item-15 animsition-link" href="{{ route('logout') }}">
                                <h5>Logout</h5>
                            </a>
                        </li>
                    </ul>
                </li>
            @else
                <li>
                    <a href="#" onClick="open_login()" style="font-size: medium"><i
                            class="fa fa-fw">&#xf090;</i>@lang('trs.login')</a>
                </li>
            @endif
        </ul>
    </nav>

    <div id="cd-search" class="cd-search">
        <form action="{{ route('www.search') }}" method="get">
            <input type='text' name='search' placeholder="Search Medlab"/>
        </form>
    </div>
    @yield('breadcrumbs')
    <style type="text/css">
        #loginbg {
            display: none;
            position: absolute;
            top: 0;
            left: 0;
            z-index: 200;
            height: 100%;
            width: 100%;
            background: #000000;
            filter: alpha(opacity=30);
            -moz-opacity: 0.3;
            opacity: 0.3;
            /*overflow:hidden;*/
        }
    </style>
    <div id="loginbg"></div>

    <a class="hiddenanchor" id="toregister"></a>
    <a class="hiddenanchor" id="tologin"></a>
    <div id="wrapper" style="display:none;z-index: 201;">
        <div id="login" class="animate form">
            <button id="cboxClose"
                    style="width: 1px;margin-left: 85%;padding:8px 20px 8px 10px;"
                    onClick="close_login()">X
            </button>
            <div class="login100-pic js-tilt" data-tilt
                 style="text-align: -webkit-left;float: left;margin-left: 6%;">
                <img src="https://cdn.medlab.co/www/Images/medlab%20logo%20port%20COL.png" alt="IMG">
            </div>
            <form method="post" id="loginForm" action="{{ route('login') }}">
                @csrf

                <h5 style="font-size: 25px;">Login</h5>
                @if (session('login_error'))
                    <div class="sixteen columns" id="login_error">
                        <div class="alert alert-red">
                            <p><span>&#xf00d;</span>{{ session('login_error') }}</p>
                        </div>
                    </div>
                @endif
                <p>
                    <label for="username" class="uname" data-icon="u"> Your email or username</label>
                    <input id="username" name="email" type="email" required="required"
                           placeholder="myusername or mymail@mail.com"/>

                </p>
                <p>
                    <label for="password" class="youpasswd" data-icon="p"> Your password </label>
                    <input id="password" name="password" required="required"
                           {{--                                   oninput="validatePassword(this)"--}}
                           type="password"
                           placeholder="eg. X8df!90EO"/>

                </p>
                <p class="keeplogin">
                    <input type="checkbox" name="remember" id="remember" value="on"/>
                    <label for="remember">Keep me logged in</label>
                </p>
                @if (Route::has('password.request'))
                    <a class="btn btn-link" href="{{ route('password.request') }}"
                       style="width: 300px;text-align: left;background: unset;">
                        {{ __('Forgot Your Password?') }}
                    </a>
                    <a class="btn btn-link" href="{{ route('register') }}"
                       style="width: 300px;text-align: left;background: unset;">Not a member ? Register here
                    </a>
                @endif
                {{--                        <input type="submit" value="submit" />--}}
                <div class="sixteen columns">
                    <div id="button-con" style="text-align: right;padding: 5%;">
                        <button name="submit" type="submit" id="submit_login">submit</button>
                    </div>
                </div>
                <p class="change_link" style="width: 100%;">
                </p>
            </form>


        </div>
    </div>
</div>

<script type="text/javascript" src="{{ asset('/js/jquery-2.1.1.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script type="text/javascript" src="{{ asset('/js/promise-polyfill.js') }}"></script>
<script language="javascript">
    jQuery(document).ready(function ($) {
        $("#loginForm").submit(function (e) {

            e.preventDefault(); // avoid to execute the actual submit of the form.

            var form = $(this);
            var url = form.attr('action');

            $.ajax({
                type: "POST",
                url: url,
                data: form.serialize(), // serializes the form's elements.
                success: function (data) {
                    if (data.msg == 'practitioner') {
                        var list = JSON.parse(data.practitioner);
                        var options = {};
                        $.map(list,
                            function (o) {
                                options[o.guid] = o.customer_name;
                            });
                        const {value: practitioner} = Swal.fire({
                            title: 'Select a business',
                            input: 'select',
                            inputOptions: options,    // inputOptions: {
                            inputPlaceholder: 'Select a business',
                            allowOutsideClick: false,
                            showLoaderOnConfirm: true,
                            inputValidator: (value) => {
                                return new Promise((resolve) => {
                                    $.ajax({
                                        type: "POST",
                                        url: '{{ url('submit_practitioner') }}',
                                        data: {
                                            _token: '{{ csrf_token() }}',
                                            practitioner: value,
                                        },
                                        success: function () {
                                            location.reload();
                                        }

                                    })
                                })
                            }
                        })
                    } else if (data.msg == 'patient') {
                        location.reload();
                    }  else if (data.msg == 'invalid account') {
                        Swal.fire({
                            icon: 'error',
                            title: 'Your account haven"t been processed yet, please try again later',
                        })
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'Invalid email or password',
                        })
                    }
                }
            });


        });
    });

    function open_login() {
        document.getElementById('loginbg').style.display = 'block';
        document.getElementById('wrapper').style.display = 'block';
        document.getElementById('cd-primary-nav').classList.remove('nav-is-visible');
        document.getElementById('header').classList.remove('nav-is-visible');
        document.getElementById('nav').classList.remove('nav-is-visible');
        document.getElementById('main').classList.remove('nav-is-visible');
        showloginbg();
        $('html, body').addClass('lock-back');
    }

    function close_login() {
        @php \Session::forget('login_error'); @endphp
        $("#login_error").remove();
        document.getElementById('loginbg').style.display = 'none';
        document.getElementById('wrapper').style.display = 'none';
        $('html, body').removeClass('lock-back');
    }

    function showloginbg() {
        var sWidth, sHeight;
        sWidth = screen.width;
        sWidth = document.body.offsetWidth;
        sHeight = document.body.offsetHeight;
        if (sHeight < screen.height) {
            sHeight = screen.height;
        }
        document.getElementById("loginbg").style.width = sWidth + "px";
        document.getElementById("loginbg").style.height = sHeight + "px";
        document.getElementById("loginbg").style.display = "block";
        document.getElementById("loginbg").style.display = "block";
        document.getElementById("loginbg").style.right = document.getElementById("wrapper").offsetLeft + "px";
    }

    function logo_in() {
        alert()
        close_login();
    };

    function redirectInfo(id) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You are now navigating off Medlab corporate Medlab site",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#dd3333',
            confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.value) {
                window.open(id, '_blank');
            }
        })


    }
    function changeLocale() {
        document.getElementById("myDropdown").classList.toggle("show");
    }

    window.onclick = function (event) {
        if (!event.target.matches('.dropbtn')) {
            var dropdowns = document.getElementsByClassName("dropdown-content");
            var i;
            for (i = 0; i < dropdowns.length; i++) {
                var openDropdown = dropdowns[i];
                if (openDropdown.classList.contains('show')) {
                    openDropdown.classList.remove('show');
                }
            }
        }
    }

</script>



