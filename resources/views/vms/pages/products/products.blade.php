@extends('vms.layouts.main')
@section('title')
    Medlab - {{ $content->title }}
@endsection
@section('description')
    @if($content->meta_description)
        {{ $content->meta_description }}
    @elseif(json_decode($content->content))
        @foreach(json_decode($content->content) as $item)
            @if($item->layout=='text')
                {{ substr(str_replace('&nbsp;',' ',strip_tags($item->attributes->content)),0,160) }}
            @endif
        @endforeach
    @endif
@endsection
@section('breadcrumbs')
    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset;background-color:#7AA43F;">
            <ol class="breadcrumb medlab_breadcrumbs" style="margin-bottom: unset;background-color:#7AA43F;">
            </ol>
        </div>
    </div>

    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset">
            <ol class="breadcrumb medlab_breadcrumbs">
                <li><a class="medlab_breadcrumbs_link" href="/">Home</a></li>
                @if(isset($condition['vegan_friendly']))
                    <li><a class="medlab_breadcrumbs_link" href="/products">Products</a></li>
                    <li class="active medlab_breadcrumbs_text">@lang('Vegan Friendly')</li>
                @elseif(isset($condition['vegetarian_friendly']))
                    <li><a class="medlab_breadcrumbs_link" href="/products">Products</a></li>
                    <li class="active medlab_breadcrumbs_text">@lang('Vegetarian Friendly')</li>
                @elseif(isset($condition['gluten_free']))
                    <li><a class="medlab_breadcrumbs_link" href="/products">Products</a></li>
                    <li class="active medlab_breadcrumbs_text">@lang('Gluten Free')</li>
                @elseif(isset($condition['pregnancy_safe']))
                    <li><a class="medlab_breadcrumbs_link" href="/products">Products</a></li>
                    <li class="active medlab_breadcrumbs_text">@lang('Pregnancy Safe')</li>
                @elseif(isset($condition['dairy_free']))
                    <li><a class="medlab_breadcrumbs_link" href="/products">Products</a></li>
                    <li class="active medlab_breadcrumbs_text">@lang('Dairy Free')</li>
                @elseif(isset($condition['group']))
                    <li><a class="medlab_breadcrumbs_link" href="/products">Products</a></li>
                    <li class="active medlab_breadcrumbs_text">{{$condition['group']}}</li>
                @else
                    <li class="active medlab_breadcrumbs_text">Products</li>
                @endif
            </ol>
        </div>
    </div>
@endsection

@section('content')



    <main class="cd-main-content" id="main" style="margin-top: 90px">

        <!-- HOME SECTION
        ================================================== -->

        @if(isset($content->image)&&$content->image!=''&&$content->image!='Null')
            @if(isset($content->page))
            <section class="home">

                <div class="slider-container">
                    <div class="tp-banner-container">
                        <div class="tp-banner">
                            <ul>
                                <li data-transition="fade" data-slotamount="1" data-masterspeed="500"
                                    data-saveperformance="on" data-title="Intro Slide">
                                    <img src="{{ $image_url }}/www/Images/{{ $content->image()->first()->name }}"
                                         data-lazyload="{{ $image_url }}/www/Images/{{ $content->image()->first()->name }}"
                                         data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                                    <a href="#">
                                        <div class="black-heavy-3">
                                            <div class="black-heavy-3-heading">
                                            <h1>{{ $content->title }}</h1></div>
                                            @if(isset($content->subtitle))
                                                <div class="black-heavy-3-subheading">{{ $content->subtitle }}</div>
                                            @endif
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

            </section>
                @else
                <section class="home">

                    <div class="slider-container">
                        <div class="tp-banner-container">
                            <div class="tp-banner">
                                <ul>
                                    <li data-transition="fade" data-slotamount="1" data-masterspeed="500"
                                        data-saveperformance="on" data-title="Intro Slide">
                                        <img src="{{ $image_url }}/www/Images/{{ $content->image()->first()->name }}"
                                             data-lazyload="{{ $image_url }}/www/Images/{{ $content->image()->first()->name }}"
                                             data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                                        <a href="#">
                                            <div class="black-heavy-3">
                                                <div class="black-heavy-3-heading">
                                                    <h1>{{ $content->name }}</h1></div>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </section>
                @endif
            @else
            <section class="section section-home-padding-top" style="padding-top: 60px;">

                <div class="container">
                    <div class="sixteen columns">
                        <div class="section-title left">
                            <h1>Products</h1>
                        </div>
                    </div>
                </div>
            </section>
        @endif

{{--        <br>--}}
        @include('vms.layouts.content_section')

    <!-- SECTION
        ================================================== -->


        <section class="section white-section section-padding-top-bottom">
            <div class="container">
{{--                <div id="portfolio-filter">--}}
{{--                    <ul id="filter">--}}
{{--                        <li><a href="#" class="current" data-filter="*" title="">Show All</a></li>--}}
{{--                        <li><a href="#" data-filter=".inDev" title="">In Development</a></li>--}}
{{--                    </ul>--}}
{{--                </div>--}}
                <div class="clear"></div>
                <div id="shop-grid">
                    @foreach($index as $item)
                        <div class="one-third column">
                            <a href={{ route('product_page', ['id' => preg_replace('/--+/','-',preg_replace('/[^A-Za-z0-9\-]/', '-', $item->product_name)).'-'.$item->id]) }}>
                                <div class="blog-box-1 grey-section"
                                     style="text-align: center">

                                    @if($item->product()->first()->unapproved_drug=='1' && (!(Auth::guard('practitioner')->check() && (Auth::guard('practitioner')->user()->ahpra_number || $if_au == 0) )))
{{--                                        <img--}}
{{--                                            src="https://cdn.medlab.co/www/Images/demo_drug.png"--}}
{{--                                            style="display: inline-block" alt=""/>--}}
                                        <img
                                            src="{{ $image_url }}/{{ $item->product()->first()->fake_bottle }}"
                                            style="display: inline-block" alt=""/>
                                    @else
                                        <img
                                            src="{{ $image_url }}/{{ $item->product()->first()->img_front_view_small }}"
                                            style="display: inline-block" alt=""/>
                                    @endif
                                    <div>
                                        <h2 class="one-row-max"
                                            style="padding: 5px;height: 40px;text-align: center">{{ $item->product_name }}</h2>
                                        <br>
                                        <div class="two-row-max"
                                             style="padding: 5px;height: 30px;">
                                            <p>{!! $item->product_heading !!}</p></div>
                                        <br>
                                        <div class="two-row-max"
                                             style="padding: 5px;height: 50px;">
                                            <p>{!! $item->product_byline !!}</p></div>
                                    </div>
                                </div>
                            </a>

                        </div>
                    @endforeach
{{--                    @foreach($productsInDev as $item)--}}
{{--                        <div class="one-third column inDev">--}}
{{--                            <a href="{{ route('products_in_development', ['id' => preg_replace('/--+/','-',preg_replace('/[^A-Za-z0-9\-]/', '-', $item->product_name)).'-'.$item->id]) }}">--}}
{{--                                <div class="blog-box-1 grey-section"--}}
{{--                                     style="text-align: center">--}}
{{--                                    <img--}}
{{--                                        src="https://cdn.medlab.co/www/Images/demo_drug.png"--}}
{{--                                        style="display: inline-block" alt=""/>--}}

{{--                                    <div>--}}
{{--                                        <h2 class="one-row-max"--}}
{{--                                            style="padding: 5px;height: 40px;text-align: center">{{ $item->product_name }}</h2>--}}
{{--                                        <br>--}}
{{--                                        <div class="two-row-max"--}}
{{--                                             style="padding: 5px;height: 30px;">--}}
{{--                                            <p>{!! $item->product_heading !!}</p></div>--}}
{{--                                        <br>--}}
{{--                                        <div class="two-row-max"--}}
{{--                                             style="padding: 5px;height: 50px;">--}}
{{--                                            <p>{!! $item->product_byline !!}</p></div>--}}
{{--                                    </div>--}}

{{--                                </div>--}}
{{--                            </a>--}}

{{--                        </div>--}}
{{--                    @endforeach--}}
                </div>

            </div>
        </section>
        <!-- SECTION
        ================================================== -->


        <section class="section grey-section section-padding-top-bottom">
            <div class="container">
                <div class="sixteen columns">
                    <div class="blog-left-right-links pagination">
                        @if($index->currentPage()-1!=0)
                            @if(isset($condition['vegan_friendly']))
                                <a href="{{ route('products') }}?vegan_friendly={{ $condition['vegan_friendly'] }}&page={{ $index->currentPage()-1 }}">
                                    <div class="blog-left-link"><p>PREVIOUS 25</p></div>
                                </a>
                            @elseif(isset($condition['vegetarian_friendly']))
                                <a href="{{ route('products') }}?vegetarian_friendly={{ $condition['vegetarian_friendly'] }}&page={{ $index->currentPage()-1 }}">
                                    <div class="blog-left-link"><p>PREVIOUS 25</p></div>
                                </a>
                            @elseif(isset($condition['gluten_free']))
                                <a href="{{ route('products') }}?gluten_free={{ $condition['gluten_free'] }}&page={{ $index->currentPage()-1 }}">
                                    <div class="blog-left-link"><p>PREVIOUS 25</p></div>
                                </a>
                            @elseif(isset($condition['pregnancy_safe']))
                                <a href="{{ route('products') }}?pregnancy_safe={{ $condition['pregnancy_safe'] }}&page={{ $index->currentPage()-1 }}">
                                    <div class="blog-left-link"><p>PREVIOUS 25</p></div>
                                </a>
                            @elseif(isset($condition['dairy_free']))
                                <a href="{{ route('products') }}?dairy_free={{ $condition['dairy_free'] }}&page={{ $index->currentPage()-1 }}">
                                    <div class="blog-left-link"><p>PREVIOUS 25</p></div>
                                </a>
                            @elseif(isset($condition['group']))
                                <a href="{{ route('products') }}?group={{ $condition['group'] }}&page={{ $index->currentPage()-1 }}">
                                    <div class="blog-left-link"><p>PREVIOUS 25</p></div>
                                </a>
                            @else
                                <a href="{{ route('products') }}?page={{ $index->currentPage()-1 }}">
                                    <div class="blog-left-link"><p>PREVIOUS 25</p></div>
                                </a>
                            @endif
                        @endif
                        {{--                                @for($i=1;$i<$allArticles->total()+1;$i++)--}}
                        {{--                                        <a href="{{ route('articles') }}?role={{ $role }}&article_category={{ $article_category }}&page={{ $i }}">--}}
                        {{--                                            <div class="blog-left-link"><p>{{ $i }}</p></div>--}}
                        {{--                                        </a>--}}
                        {{--                                    @endfor--}}

                        @if($index->currentPage()!=$index->lastPage())
                            @if(isset($condition['vegan_friendly']))
                                <a href="{{ route('products') }}?vegan_friendly={{ $condition['vegan_friendly'] }}&page={{ $index->currentPage()+1 }}">
                                    <div class="blog-right-link"><p>NEXT 25</p></div>
                                </a>
                            @elseif(isset($condition['vegetarian_friendly']))
                                <a href="{{ route('products') }}?vegetarian_friendly={{ $condition['vegetarian_friendly'] }}&page={{ $index->currentPage()+1 }}">
                                    <div class="blog-right-link"><p>NEXT 25</p></div>
                                </a>
                            @elseif(isset($condition['gluten_free']))
                                <a href="{{ route('products') }}?gluten_free={{ $condition['gluten_free'] }}&page={{ $index->currentPage()+1 }}">
                                    <div class="blog-right-link"><p>NEXT 25</p></div>
                                </a>
                            @elseif(isset($condition['pregnancy_safe']))
                                <a href="{{ route('products') }}?pregnancy_safe={{ $condition['pregnancy_safe'] }}&page={{ $index->currentPage()+1 }}">
                                    <div class="blog-right-link"><p>NEXT 25</p></div>
                                </a>
                            @elseif(isset($condition['dairy_free']))
                                <a href="{{ route('products') }}?dairy_free={{ $condition['dairy_free'] }}&page={{ $index->currentPage()+1 }}">
                                    <div class="blog-right-link"><p>NEXT 25</p></div>
                                </a>
                            @elseif(isset($condition['group']))
                                <a href="{{ route('products') }}?group={{ $condition['group'] }}&page={{ $index->currentPage()+1 }}">
                                    <div class="blog-right-link"><p>NEXT 25</p></div>
                                </a>
                            @else
                                <a href="{{ route('products') }}?page={{ $index->currentPage()+1 }}">
                                    <div class="blog-right-link"><p>NEXT 25</p></div>
                                </a>
                            @endif
                        @endif
                        {{--                                    <a>--}}
                        <div style="padding: 6px">
                            <select onchange="changePage()" id="page" style="float: right;margin-right: 5%;">
                                @for($i=1;$i<$index->lastPage()+1;$i++)
                                    <option
                                        value={{ $i }} @if($index->currentPage() == $i) selected @endif >{{ $i }}</option>
                                @endfor
                            </select>
                            <p style="float: right">Page No.</p>
                        </div>
                        {{--                                    </a>--}}


                    </div>
                </div>
            </div>

        </section>

        @include('vms.layouts.footer')
    </main>

    <div class="scroll-to-top">&#xf106;</div>





    <!-- JAVASCRIPT
    ================================================== -->
    <script type="text/javascript">


        function addone(id) {
            document.getElementById('amount_' + id).innerHTML = (parseInt(document.getElementById('amount_' + id).innerHTML) + 1 >= 0) ? parseInt(document.getElementById('amount_' + id).innerHTML) + 1 : 0;
        }

        function minusone(id) {
            document.getElementById('amount_' + id).innerHTML = (parseInt(document.getElementById('amount_' + id).innerHTML) - 1 >= 0) ? parseInt(document.getElementById('amount_' + id).innerHTML) - 1 : 0;
        }

        $(".num-itm").keypress(function (e) {
            if (isNaN(String.fromCharCode(e.which))) e.preventDefault();
        });

        function changePage() {
            var page = document.getElementById("page").value;
            console.log(page);

                @if(isset($condition['vegan_friendly'])){
                location.href = "{{ route('products') }}?vegan_friendly={{$condition['vegan_friendly']}}&page=" + page;
            }
                @elseif(isset($condition['vegetarian_friendly'])){
                location.href = "{{ route('products') }}?vegetarian_friendly={{$condition['vegetarian_friendly']}}&page=" + page;
            }
                @elseif(isset($condition['gluten_free'])){
                location.href = "{{ route('products') }}?gluten_free={{$condition['gluten_free']}}&page=" + page;
            }
                @elseif(isset($condition['pregnancy_safe'])){
                location.href = "{{ route('products') }}?pregnancy_safe={{$condition['pregnancy_safe']}}&page=" + page;
            }
                @elseif(isset($condition['dairy_free'])){
                location.href = "{{ route('products') }}?dairy_free={{$condition['dairy_free']}}&page=" + page;
            }
                @elseif(isset($condition['group'])){
                location.href = "{{ route('products') }}?group={{$condition['group']}}&page=" + page;
            }
                @else{
                location.href = "{{ route('products') }}?&page=" + page;
            }
            @endif



        }

        function isset(ref) {
            return typeof ref !== 'undefined'
        }
    </script>
    <script type="text/javascript" src="{{ asset('/js/medlab.js') }}"></script>

    <!-- End Document
    ================================================== -->
@endsection
