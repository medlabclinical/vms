@extends('vms.layouts.main')
@section('title')
    {{ $article->title }}
@endsection
@section('description')
    @if(($article->req_login && Auth::guard('practitioner')->check())|| !$article->req_login)
        @if(json_decode($article->content))
            @foreach(json_decode($article->content) as $item)
                @if($item->layout=='text')
                    {{ substr(str_replace('&nbsp;',' ',$article->subtitle.' - '.strip_tags($item->attributes->content)),0,160) }}
                @endif
            @endforeach
        @endif
    @else
        {{substr($article->subtitle.' - '.$article->preview_text,0,160)}}
    @endif
@endsection
@section('breadcrumbs')
    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset;background-color:#7AA43F;">
            <ol class="breadcrumb medlab_breadcrumbs" style="margin-bottom: unset;background-color:#7AA43F;">
            </ol>
        </div>
    </div>

    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset">
            <ol class="breadcrumb medlab_breadcrumbs">
                <li><a class="medlab_breadcrumbs_link" href="/">@lang('trs.home')</a></li>
                <li><a class="medlab_breadcrumbs_link" href="{{ route('education') }}">@lang('trs.education')</a></li>
                <li><a class="medlab_breadcrumbs_link" href="{{ route('articles') }}">Articles</a>
                </li>
                <li class="active medlab_breadcrumbs_text">{{ $article->title }}</li>
            </ol>
        </div>
    </div>
@endsection
@section('content')



    <main class="cd-main-content" id="main">


        <!-- TOP SECTION
        ================================================== -->

        <section class="section white-section section-home-padding-top">

            <div class="container">
                <div class="sixteen columns">
                    <div class="section-title left">
                        <h1>{{ $article->title }}</h1>
                        <div class="subtitle left big">{{ $article->subtitle }}</div>
                        @if(isset($article->date))
                            <div class="subtitle left">Date:&nbsp&nbsp{{ substr($article->date,0,10) }}</div>@endif
                        @if(count($article->member))
                            <div class="subtitle left">Author:&nbsp&nbsp
                                @if(count($article->member))
                                    {{$article->member[0]['name']}}
                                    @if(count($article->member)!=1)
                                        @for($i=1;$i<count($article->member);$i++)
                                            ,&nbsp{{ $article->member[$i]['name'] }}
                                        @endfor
                                    @endif
                                @endif
                            </div>@endif
                        @include('vms.layouts.article_content_section',['data'=>$article->content])
                    </div>
                </div>
            </div>

        </section>

        <!-- SECTION
        ================================================== -->

        <section class="section white-section section-padding-bottom">

            <div class="container">
                <div class="twelve columns">
                    @if(($article->req_login && Auth::guard('practitioner')->check())|| !$article->req_login)
                        @if($article->link_url)
                            <div class="blog-big-wrapper white-section"
                                 data-scroll-reveal="enter bottom move 200px over 1s after 0.3s" style="color:white;">
                                <div class="medlab_news_item">
                                    <div class="medlab_news_item_content_wrapper">
                                        <div class="medlab_news_item_body" style="height: 100vh">
                                            {{--                                        <p>--}}
{{--                                            <iframe src="{{ $image_url }}/{{ $article->link_url }}"--}}
{{--                                                    type="application/pdf" width="100%" height="100%"></iframe>--}}
                                            <div class="pdf-pro-plugin" data-pdf-url="{{ $image_url }}/{{ $article->link_url }}"></div>
                                            {{--                                        </p>--}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                            @if(!$article->disable_comments)
                                <div class="post-content-com-top grey-section"
                                     data-scroll-reveal="enter bottom move 200px over 1s after 0.3s">
                                    <p>COMMENTS <span>{{ count($comments) }}</span></p>
                                </div>
                                @foreach($comments as $comment)
                                    <div id="comment_{{$comment->id}}">
                                        <div class="post-content-comment grey-section"
                                             data-scroll-reveal="enter bottom move 200px over 1s after 0.3s">
                                            <h4>{{ $comment->user_name }}</h4>
                                            <p>{{ $comment->content }}</p>
                                            @if(Auth::guard('practitioner')->check()||Auth::guard('patient')->check())
                                                <a class="reply" id="reply_{{$comment->id}}"
                                                   onclick="addComment({{$comment->id}})">reply</a>
                                            @endif
                                        </div>
                                    </div>
                                    @if(count($comment->children))
                                        @foreach($comment->children as $children)
                                            <div class="post-content-comment reply-in grey-section"
                                                 data-scroll-reveal="enter bottom move 200px over 1s after 0.3s"
                                                 id="comment_{{$children['id']}}">
                                                <h4>{{ $children['user_name'] }}</h4>
                                                <p>{{ $children['content'] }}</p>
                                            </div>
                                        @endforeach
                                    @endif
                                @endforeach
                                @if(Auth::guard('practitioner')->check()||Auth::guard('patient')->check())

                                    <form action="{{ url('post_comment') }}" method="post" id="comment_form" target="_blank"
                                          style="width: 100%">
                                        {!! csrf_field() !!}
                                        <div class="leave-reply grey-section"
                                             data-scroll-reveal="enter bottom move 200px over 1s after 0.3s">
                                            <h4>LEAVE A COMMENT</h4>
                                            <input name="article" type="hidden" value="{{ $article->id }}"/>
                                            <input name="parent" type="hidden" value='0'/>
                                            <textarea name="content" placeholder="COMMENT"></textarea>
                                            <button class="post-comment sendForm" id="send">post comment</button>
                                        </div>
                                    </form>
                                @else
                                    <div class="leave-reply grey-section"
                                         data-scroll-reveal="enter bottom move 200px over 1s after 0.3s">
                                        <h4 style="padding-bottom: unset;">Tell us what you think <a onClick="open_login()"
                                                                                                     style="cursor: pointer;color: #6ba53a;">login</a>
                                            to share your thoughts.</h4>
                                    </div>
                                @endif

                            @endif
                    @else
                        <div class="grey-section" style="margin-top: 20px;">
                            <div class="articleClass padding">
                                {{$article->preview_text}}
                            </div>
                            <div style="height: 100px;float: right;">
                                <div style="margin-right: unset;margin-top: 10%;"
                                     class="button-shortcodes text-size-1 text-padding-1 version-1"
                                     onClick="open_login()"><span>&#xf18e;</span> Practitioner login to view
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="four columns" data-scroll-reveal="enter bottom move 200px over 1s after 0.3s">
                    <div class="post-sidebar">
                        @if(count($article->product_family))
                            <div class="separator-sidebar"></div>
                            <h3>Related Products</h3>
                            <ul class="link-recents">
                                @foreach($article->product_family as $a)
                                    <li><a style="color: #6ba53a;word-break: break-word;" href="{{ route('product_page', ['id' => preg_replace('/--+/','-',preg_replace('/[^A-Za-z0-9\-]/', '-', $a['product_name'])).'-'.$a['id']]) }}">{{ $a['product_name'] }}</a></li>
                                @endforeach
                            </ul>
                        @endif
                        @if(count($article->clinical_trial))
                            <div class="separator-sidebar"></div>
                            <h3>Related Clinical Trials</h3>
                            <ul class="link-recents">
                                @foreach($article->clinical_trial as $a)
                                    <li><a style="color: #6ba53a;word-break: break-word;" onclick="redirectInfo('{{  config('app.www_url').'/clinical_trials/'.preg_replace('/--+/','-',preg_replace('/[^A-Za-z0-9\-]/', '-', $a['name'])).'-'.$a['id'] }}')" href="javascript:">{{ $a['name'] }}</a></li>
                                @endforeach
                            </ul>
                        @endif
                        @if(count($article->publication))
                            <div class="separator-sidebar"></div>
                            <h3>Related Publications And Presentations</h3>
                            <ul class="link-recents">
                                @foreach($article->publication as $a)
                                    <li><a style="color: #6ba53a;word-break: break-word;" onclick="redirectInfo('{{  config('app.www_url').'/publications' }}')" href="javascript:">{{ $a['title'] }}</a></li>
                                @endforeach
                            </ul>
                        @endif
                        @if($article->article_category)
                            <div class="separator-sidebar"></div>
                            <h3>Categories</h3>
                            <ul class="link-recents">
                                @foreach($article->article_category as $a)
                                    <li><a style="color: #6ba53a;word-break: break-word;"
                                           href="{{ route('articles') }}?article_category={{ $a['name'] }}">{{ str_replace('_',' ',$a['name']) }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        @endif
                    </div>
                </div>
            </div>

        </section>


        <!-- SECTION
        ================================================== -->
        @if(count($article->member))
            <div class="container">
                <div class="sixteen columns">
                    <div class="section-title">
                        <h4 style="text-align: center">ABOUT THE AUTHOR</h4>
                    </div>
                </div>
                @foreach($article->member as $people)
                    <section class="section white-section section-padding-top-bottom">


                        <div class="sixteen columns">
                            <div class="blockquotes-box-1 grey-section blockquotes-float-content"
                                 style="max-width: 300px;padding: 10px;border: unset;">
                                @if($people['picture'])
                                    <img src="{{ $image_url }}/www/Images/{{ $people['picture'] }}"
                                         alt="{{ $people['alt'] }}">
                                @else
                                    <img src="{{ $image_url }}/www/Images/boy-1585447286t6EYT.jpg"
                                         alt="{{ $people['alt'] }}">
                                @endif
                            </div>
                            <div class="team-name-top mobile-inline-block"
                                 style="font-family: 'Playball',crusive;">{{ $people['position'] }}</div>
                            <h5 style="margin: 20px;font-size: x-large;text-align: unset;margin-left: unset;">{{ $people['name'] }}</h5>
                            {!! $people['bio'] !!}
                        </div>

                    </section>
            </div>
            @endforeach
        @endif
        @if(count($article->related_article))
            <section class="section grey-section section-padding-top-bottom" style="padding-top: unset;">
                <div class="container">
                    <div class="sixteen columns">
                        <div class="section-title">
                            <h4>MORE BY THIS AUTHOR</h4>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="blog-wrapper">
                        <div id="blog-grid-masonry">
                            @foreach($article->related_article as $item)
                                @if($item->article_type=='link')
                                    <a href={{ route('articles_content',['id' => preg_replace('/--+/','-',preg_replace('/[^A-Za-z0-9\-]/', '-', $item->title)).'-'.$item->id]) }}>
                                        <div class="blog-box-3">
                                            <div class="blog-box-1 link-post grey-section">
                                                <h4 class="two-row-max"
                                                    style="min-height: 96px;">{{ $item->title }}</h4>
                                                <p class="four-row-max"
                                                   style="padding-bottom: unset;min-height: 92px;">{{ $item->preview_text }}</p>
                                                <br>
                                                <p>{{ substr($item->date,0,10) }}</p>
                                                @if(isset($item->author))<p>Author: {{ $item->author }}</p>@endif
                                                <div class="link">&#xf178;</div>
                                            </div>
                                        </div>
                                    </a>
                                @elseif($item->article_type=='video')
                                    <a href={{ route('articles_content',['id' => preg_replace('/--+/','-',preg_replace('/[^A-Za-z0-9\-]/', '-', $item->title)).'-'.$item->id]) }}>
                                        <div class="blog-box-3">
                                            <div class="blog-box-1 grey-section">
                                                @if(($item->req_login && !(Auth::guard('practitioner')->check()||Auth::guard('patient')->check())))
                                                    <div
                                                        style="max-height: 270px; position:relative; padding-bottom: 56.25%; height: 0; opacity: 0.85;background: black; ">
                                                        <iframe
                                                            srcdoc="<p style='text-align:center;margin-top:25%;font-size:xx-large;color:white'>Please login to view this video</p>"
                                                            width="560" height="349" src="{{ $item->video_link }}"
                                                            style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"
                                                            allowfullscreen></iframe>
                                                    </div>
                                                @else
                                                    <div
                                                        style="max-height: 270px; position:relative; padding-bottom: 56.25%; height: 0;">
                                                        <iframe width="560" height="349" src="{{ $item->video_link }}"
                                                                style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"
                                                                allowfullscreen></iframe>
                                                    </div>
                                                @endif
                                                <h4 class="two-row-max"
                                                    style="min-height: 96px;">{{ $item->title }}</h4>
                                                <p class="four-row-max"
                                                   style="padding-bottom: unset;min-height: 92px;">{{ $item->preview_text }}</p>
                                                <br>
                                                <p>{{ substr($item->date,0,10) }}</p>
                                                @if(isset($item->author))<p>Author: {{ $item->author }}</p>@endif
                                                <div class="link">&#xf178;</div>
                                            </div>
                                        </div>
                                    </a>
                                @else
                                    <a href={{ route('articles_content',['id' => preg_replace('/--+/','-',preg_replace('/[^A-Za-z0-9\-]/', '-', $item->title)).'-'.$item->id]) }}>
                                        <div class="blog-box-3">
                                            <div class="blog-box-1 grey-section">
                                                <div
                                                    style="max-height: 270px; position:relative; padding-bottom: 56.25%; height: 0;">
                                                    <img src="{{ $image_url }}/www/Images/{{ $item->title_img }}"
                                                         alt="{{ $item->title_alt }}" style="margin-left: auto;
                                                 margin-right: auto; position: absolute; top: 0; left: 0; width: 100%; height: 100%;">
                                                </div>
                                                <h4 class="two-row-max"
                                                    style="min-height: 96px;">{{ $item->title }}</h4>
                                                <p class="four-row-max"
                                                   style="padding-bottom: unset;min-height: 92px;">{{ $item->preview_text }}</p>
                                                <br>
                                                <p>{{ substr($item->date,0,10) }}</p>
                                                @if(isset($item->author))<p>Author: {{ $item->author }}</p>@endif
                                                <div class="link">&#xf178;</div>
                                            </div>
                                        </div>
                                    </a>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </section>
        @endif


    <!-- FOOTER
        ================================================== -->


        @include('vms.layouts.footer')


    </main>

    <div class="scroll-to-top">&#xf106;</div>





    <!-- JAVASCRIPT
    ================================================== -->
    <script type="text/javascript" src="{{ asset('/js/pdfviewer/pdf.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/pdfviewer/pdf-viewer.min.js') }}"></script>
    <script type="text/javascript">
        (function ($) {

            "use strict";
            function redirectInfo(id) {
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You are now navigating off Medlab corporate Medlab site",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#dd3333',
                    confirmButtonText: 'Yes'
                }).then((result) => {
                    if (result.value) {
                        window.open(id, '_blank');
                    }
                })
            }
        })(jQuery);
    </script>

    <script type="text/javascript">
        var form = "<form class='form'></form>";

        function addComment(id) {
            if (document.getElementById('reply_' + id).innerText == 'CANCEL') {
                document.getElementById('comment_form_' + id).remove();
                document.getElementById('reply_' + id).innerText = 'reply';
            } else {
                document.getElementById('reply_' + id).innerText = 'cancel';
                $('#comment_' + id).append("<form action='{{ url('post_comment')}}' method='post' id='comment_form_" + id + "' style='width: 100%' target='_blank'>" +
                    "<input type='hidden' name='_token' value=" + document.getElementsByName('_token')[0].value + ">" + "<div class='leave-reply grey-section post-content-comment'><input name='parent' type='hidden' value=" + id
                    + "><textarea name='content'  placeholder='COMMENT'></textarea><input name='id' value='comment_" + id + "' type='hidden'></input><button class='post-comment sendForm' id=" + id + ">reply</button></div></form>");
            }
        }

        // $(document).on("click",".sendForm",function() {
        //     var fileId = $(this).attr("id");
        //     $('#send').click(function () { // when the button is clicked the code executes
        //
        //         var data_string = $('#comment_form').serialize(); // Collect data from form
        //
        //         console.log('send');
        //         $.ajax({
        //             type: "POST",
        //             url: $('#comment_form').attr('action'),
        //             data: data_string,
        //             error: function (data) {
        //                 console.log(data);
        //
        //             },
        //             success: function (data) {
        //                 console.log(data);
        //                 location.reload();
        //
        //             }
        //         });
        //
        //         return false; // stops user browser being directed to the php file
        //     });
        // });
        $(document).on("click", ".sendForm", function () {
            var fileId = $(this).attr("id");
            console.log(fileId)
            if (fileId == 'send') {
                console.log('basic work')
                var data_string = $('#comment_form').serialize();
                $.ajax({
                    type: "POST",
                    url: $('#comment_form').attr('action'),
                    data: data_string,
                    error: function (data) {
                        console.log(data);

                    },
                    success: function (data) {
                        console.log(data);
                        location.reload();

                    }
                });
                return false;
            } else {
                console.log('new work')
                var id = $(this).attr("id");
                console.log(id)
                var data_string = $('#comment_form_' + id).serialize();
                console.log(data_string)
                $.ajax({
                    type: "POST",
                    url: $('#comment_form_' + id).attr('action'),
                    data: data_string,
                    error: function (data) {
                        console.log(data);

                    },
                    success: function (data) {
                        console.log(data);
                        location.reload();

                    }
                });

                return false;
            }
        });
    </script>
    <script type="text/javascript" src="{{ asset('/js/medlab.js') }}"></script>

    <!-- End Document
    ================================================== -->
@endsection
