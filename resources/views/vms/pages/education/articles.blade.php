@extends('vms.layouts.main')
@section('title')
    {{ $article_category=='*'?$content->title:$content->name }}
@endsection
@section('description')
    @if($content->meta_description)
        {{ $content->meta_description }}
    @elseif($article_category=='*')
        @if(json_decode($content->content))
            @foreach(json_decode($content->content) as $item)
                @if($item->layout=='text')
                    {{ substr(str_replace('&nbsp;',' ',strip_tags($item->attributes->content)),0,160) }}
                @endif
            @endforeach
        @endif

    @else
        {{ substr(str_replace('&nbsp;',' ',strip_tags($content->content)),0,160) }}
    @endif
@endsection
@section('breadcrumbs')
    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset;background-color:#7AA43F;">
            <ol class="breadcrumb medlab_breadcrumbs" style="margin-bottom: unset;background-color:#7AA43F;">
            </ol>
        </div>
    </div>

    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset">
            <ol class="breadcrumb medlab_breadcrumbs">
                <li><a class="medlab_breadcrumbs_link" href="/">@lang('trs.home')</a></li>
                @if($article_category=='*')
                    <li><a class="medlab_breadcrumbs_link" href="{{ route('education') }}">@lang('trs.education')</a></li>
                    <li class="active medlab_breadcrumbs_text">Show All</li>
                @else
                    <li><a class="medlab_breadcrumbs_link" href="{{ route('education') }}">@lang('trs.education')</a></li>
                    <li class="active medlab_breadcrumbs_text">{{ $article_category }}</li>
                @endif
            </ol>
        </div>
    </div>
@endsection
@section('content')

    <main class="cd-main-content" id="main" style="margin-top:90px">

        <!-- TOP SECTION - SLIDER IMAGE
================================================== -->
        @if($article_category=='*')
            @if(json_decode($content->slider))
                <section class="home">
                    <div class="slider-container">
                        <div class="tp-banner-container">
                            <div class="tp-banner">
                                <ul>
                                    @foreach(json_decode($content->slider) as $slider)
                                        @if($slider->layout=='video')
                                            <li data-transition="fade" data-slotamount="1" data-masterspeed="500"
                                                data-saveperformance="on" data-title="Intro Slide">
                                                <iframe
                                                    src="https://player.vimeo.com/video/{{ $slider->attributes->video_link }}"
                                                    width="100%" height="100%" align="center" frameborder="0"
                                                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                                    allowfullscreen></iframe>
                                            </li>
                                        @elseif($slider->layout=='image')
                                            <li data-transition="fade" data-slotamount="1" data-masterspeed="500"
                                                data-saveperformance="on" data-title="Intro Slide">
                                                <img
                                                    src="{{ $image_url }}/www/Images/{{ \App\Models\MediaLibrary::where('id',$slider->attributes->slider_image)->first()->name }}"
                                                    alt="{{ $slider->attributes->slider_alt }}"
                                                    data-lazyload="{{ $image_url }}/www/Images/{{ \App\Models\MediaLibrary::where('id',$slider->attributes->slider_image)->first()->name }}"
                                                    data-bgposition="center top" data-bgfit="cover"
                                                    data-bgrepeat="no-repeat">
                                                <a href="#">
                                                    <div class="black-heavy-3">
                                                        <div class="black-heavy-3-heading">
                                                            <h1>{{ $slider->attributes->title }}</h1></div>
                                                        <div
                                                            class="black-heavy-3-subheading">{{ $slider->attributes->subtitle }}</div>
                                                    </div>
                                                </a>
                                            </li>
                                        @endif
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </section>
            @endif
            <br>
                @include('vms.layouts.content_section')
        @else
            <section class="home">
                <div class="slider-container">
                    <div class="tp-banner-container">
                        <div class="tp-banner">
                            <ul>
                                @foreach(json_decode($content->slider) as $slider)
                                    @if($slider->layout=='video')
                                        <li data-transition="fade" data-slotamount="1" data-masterspeed="500"
                                            data-saveperformance="on" data-title="Intro Slide">
                                            <iframe
                                                src="https://player.vimeo.com/video/{{ $slider->attributes->video_link }}"
                                                width="100%" height="100%" align="center" frameborder="0"
                                                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                                allowfullscreen></iframe>
                                        </li>
                                    @elseif($slider->layout=='image')
                                        <li data-transition="fade" data-slotamount="1" data-masterspeed="500"
                                            data-saveperformance="on" data-title="Intro Slide">
                                            <img
                                                src="{{ $image_url }}/www/Images/{{ \App\Models\MediaLibrary::where('id',$slider->attributes->slider_image)->first()->name }}"
                                                alt="{{ $slider->attributes->slider_alt }}"
                                                data-lazyload="{{ $image_url }}/www/Images/{{ \App\Models\MediaLibrary::where('id',$slider->attributes->slider_image)->first()->name }}"
                                                data-bgposition="center top" data-bgfit="cover"
                                                data-bgrepeat="no-repeat">
                                            <a href="#">
                                                <div class="black-heavy-3">
                                                    <div class="black-heavy-3-heading">
                                                        <h1>{{ $slider->attributes->title }}</h1></div>
                                                    <div
                                                        class="black-heavy-3-subheading">{{ $slider->attributes->subtitle }}</div>
                                                </div>
                                            </a>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
            @if($content->content)
                @include('vms.layouts.content_section')
            @endif
        @endif



        <!-- ARTICLE SECTION
            ================================================== -->

        <section class="section white-section section-padding-top-bottom" style="padding-top: unset;">

            @if(array_count_values($level_amount)[1]>2)
                <div class="container">
                    <div class="sixteen columns">
                        <div>
                            <ul id="filter">
                                @if(array_count_values($level_amount)[1]>2)
                                    <li>
                                        <a href="{{ route('articles') }}?article_category={{ $article_category }}&role=*"
                                           @if(!isset($role) || $role == '*') class="current" @endif>Show All</a></li>
                                @endif
                                @if($level_amount[1]==1)
                                    <li>
                                        <a href="{{ route('articles') }}?article_category={{ $article_category }}&role=1"
                                           @if($role == '1') class="current" @endif>Practitioner</a></li>
                                @endif
                                @if($level_amount[2]==1)
                                    <li>
                                        <a href="{{ route('articles') }}?article_category={{ $article_category }}&role=0"
                                           @if($role == '0') class="current" @endif>Consumer</a></li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            @endif

            <div class="clear"></div>
            <div class="blog-wrapper">
                <div id="blog-grid-masonry">
                    @foreach($allArticles as $item)
                        @if($item->article_type=='link')
                            <a href={{ route('articles_content',['id' => preg_replace('/--+/','-',preg_replace('/[^A-Za-z0-9\-]/', '-', $item->title)).'-'.$item->id]) }}>
                                <div class="blog-box-3">
                                    <div class="blog-box-1 link-post grey-section">
                                        <h4 class="two-row-max"
                                            style="min-height: 96px;">{{ $item->title }}</h4>
                                        <p class="four-row-max"
                                           style="padding-bottom: unset;min-height: 92px;">{{ $item->preview_text }}</p>
                                        <br>
                                        <p>{{ substr($item->date,0,10) }}</p>
                                        @if(isset($item->author))<p>Author: {{ $item->author }}</p>@endif
                                        <div class="link">&#xf178;</div>
                                    </div>
                                </div>
                            </a>
                        @elseif($item->article_type=='video')
                            <a href={{ route('articles_content',['id' => preg_replace('/--+/','-',preg_replace('/[^A-Za-z0-9\-]/', '-', $item->title)).'-'.$item->id]) }}>
                                <div class="blog-box-3">
                                    <div class="blog-box-1 grey-section">
                                        @if(($item->req_login && !(Auth::guard('practitioner')->check()||Auth::guard('patient')->check())))
                                            <div
                                                style="max-height: 270px; position:relative; padding-bottom: 56.25%; height: 0; opacity: 0.85;background: black; ">
                                                <iframe
                                                    srcdoc="<p style='text-align:center;margin-top:25%;font-size:xx-large;color:white'>Login as Practitioner to View</p>"
                                                    width="560" height="349" src="{{ $item->video_link }}"
                                                    style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"
                                                    allowfullscreen></iframe>
                                            </div>
                                        @else
                                            <div
                                                style="max-height: 270px; position:relative; padding-bottom: 56.25%; height: 0;">
                                                <iframe width="560" height="349" src="{{ $item->video_link }}"
                                                        style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"
                                                        allowfullscreen></iframe>
                                            </div>
                                        @endif
                                        <h4 class="two-row-max"
                                            style="min-height: 96px;">{{ $item->title }}</h4>
                                        <p class="four-row-max"
                                           style="padding-bottom: unset;min-height: 92px;">{{ $item->preview_text }}</p>
                                        <br>
                                        <p>{{ substr($item->date,0,10) }}</p>
                                        @if(isset($item->author))<p>Author: {{ $item->author }}</p>@endif
                                        <div class="link">&#xf178;</div>
                                    </div>
                                </div>
                            </a>
                        @else
                            <a href={{ route('articles_content',['id' => preg_replace('/--+/','-',preg_replace('/[^A-Za-z0-9\-]/', '-', $item->title)).'-'.$item->id]) }}>
                                <div class="blog-box-3">
                                    <div class="blog-box-1 grey-section">
                                        <div
                                            style="max-height: 270px; position:relative; padding-bottom: 56.25%; height: 0;">
                                            <img src="{{ $image_url }}/www/Images/{{ $item->title_img }}"
                                                 alt="{{ $item->title_alt }}" style="margin-left: auto;
                                                 margin-right: auto; position: absolute; top: 0; left: 0; width: 100%; height: 100%;">
                                        </div>
                                        <h4 class="two-row-max"
                                            style="min-height: 96px;">{{ $item->title }}</h4>
                                        <p class="four-row-max"
                                           style="padding-bottom: unset;min-height: 92px;">{{ $item->preview_text }}</p>
                                        <br>
                                        <p>{{ substr($item->date,0,10) }}</p>
                                        @if(isset($item->author))<p>Author: {{ $item->author }}</p>@endif
                                        <div class="link">&#xf178;</div>
                                    </div>
                                </div>
                            </a>
                        @endif
                    @endforeach
                </div>
            </div>
        </section>


        <!-- SECTION
        ================================================== -->

        <section class="section grey-section section-padding-top-bottom">
            <div class="container">
                <div class="sixteen columns">
                    <div class="blog-left-right-links pagination">
                        @if($allArticles->currentPage()-1!=0)
                            <a href="{{ route('articles') }}?role={{ $role }}&article_category={{ $article_category }}&page={{ $allArticles->currentPage()-1 }}">
                                <div class="blog-left-link"><p>PREVIOUS 24</p></div>
                            </a>
                        @endif
                        @if($allArticles->currentPage()!=$allArticles->lastPage())
                            <a href="{{ route('articles') }}?role={{ $role }}&article_category={{ $article_category }}&page={{ $allArticles->currentPage()+1 }}">
                                <div class="blog-right-link"><p>NEXT 24</p></div>
                            </a>
                        @endif
                        <div style="padding: 6px">
                            <select onchange="changePage()" id="page" style="float: right;margin-right: 5%;">
                                @for($i=1;$i<$allArticles->lastPage()+1;$i++)
                                    <option
                                        value={{ $i }} @if($allArticles->currentPage() == $i) selected @endif >{{ $i }}</option>
                                @endfor
                            </select>
                            <p style="float: right">Page No.</p>
                        </div>
                    </div>
                </div>
            </div>

        </section>

        @include('vms.layouts.footer')
    </main>

    <div class="scroll-to-top">&#xf106;</div>



    <!-- JAVASCRIPT
    ================================================== -->
    <script type="text/javascript">
        function submitForm() {
            var form = document.getElementById("filterForm");
            form.submit();
        }

        function changePage() {
            var page = document.getElementById("page").value;

            location.href = "{{ route('articles') }}?role={{ $role }}&article_category={{ $article_category }}&page=" + page;

        }
    </script>
    <script type="text/javascript" src="{{ asset('/js/custom-corporate-home-1.js') }}"></script>


    <!-- End Document
================================================== -->
@endsection
