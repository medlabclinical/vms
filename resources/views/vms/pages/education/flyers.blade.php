@extends('vms.layouts.main')
@section('title')
    {{ $pdf_type=='*'?$content->title:$content->name }}
@endsection
@section('description')
    @if($content->meta_description)
        {{ $content->meta_description }}
    @elseif($pdf_type=='*')
        @if(json_decode($content->content))
            @foreach(json_decode($content->content) as $item)
                @if($item->layout=='text')
                    {{ substr(str_replace('&nbsp;',' ',strip_tags($item->attributes->content)),0,160) }}
                @endif
            @endforeach
        @endif
    @else
        {{ substr(str_replace('&nbsp;',' ',strip_tags($content->content)),0,160) }}
    @endif
@endsection
@section('breadcrumbs')
    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset;background-color:#7AA43F;">
            <ol class="breadcrumb medlab_breadcrumbs" style="margin-bottom: unset;background-color:#7AA43F;">
            </ol>
        </div>
    </div>

    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset">
            <ol class="breadcrumb medlab_breadcrumbs">
                <li><a class="medlab_breadcrumbs_link" href="/">@lang('trs.home')</a></li>
                @if($pdf_type=='*')
                    <li><a class="medlab_breadcrumbs_link" href="{{ route('education') }}">@lang('trs.education')</a></li>
                    <li class="active medlab_breadcrumbs_text">Flyers</li>
                @else
                    <li><a class="medlab_breadcrumbs_link" href="{{ route('education') }}">@lang('trs.education')</a></li>
                    <li class="active medlab_breadcrumbs_text">{{ $pdf_type }}</li>
                @endif
            </ol>
        </div>
    </div>
@endsection
@section('content')

    <main class="cd-main-content" id="main" style="margin-top:90px">

        <!-- TOP SECTION - SLIDER IMAGE
================================================== -->

        @if(json_decode($content->slider))
            @if(json_decode($content->slider)[0]->layout=='video')
                <div class="containerHomepage">
                    <div class="textHomepage">
                        <iframe width=100% height=100%
                                src="https://player.vimeo.com/video/{{ json_decode($content->slider)[0]->attributes->video_link }}?muted=1&autoplay=1&loop=1&autopause=0"
                                allow="autoplay; fullscreen" allowfullscreen></iframe>
                    </div>
                </div>
            @else

                <section class="home">
                    <div class="slider-container">
                        <div class="tp-banner-container">
                            <div class="tp-banner">
                                <ul>
                                    @foreach(json_decode($content->slider) as $slider)


                                        @if($slider->layout=='image')
                                            <li data-transition="fade" data-slotamount="1" data-masterspeed="500"
                                                data-saveperformance="on" data-title="Intro Slide">
                                                <img
                                                    src="{{ $image_url }}/www/Images/{{ \App\Models\MediaLibrary::where('id',$slider->attributes->slider_image)->first()->name }}"
                                                    alt="{{ $slider->attributes->slider_alt }}"
                                                    data-lazyload="{{ $image_url }}/www/Images/{{ \App\Models\MediaLibrary::where('id',$slider->attributes->slider_image)->first()->name }}"
                                                    data-bgposition="center top" data-bgfit="cover"
                                                    data-bgrepeat="no-repeat">
                                                <a href="#">
                                                    <div class="black-heavy-3">
                                                        <div class="black-heavy-3-heading">
                                                            <h1>{{ $slider->attributes->title }}</h1></div>
                                                        <div
                                                            class="black-heavy-3-subheading">{{ $slider->attributes->subtitle }}</div>
                                                    </div>
                                                </a>
                                            </li>
                                        @endif
                                    @endforeach
                                </ul>

                            </div>
                        </div>
                    </div>
                </section>
            @endif

        @else
            <br>
        @endif

        <!-- CONTENT SECTION
    ================================================== -->
        <br>
            @include('vms.layouts.content_section')
        <section class="section white-section section-padding-top-bottom" style="padding-top: unset;">
            @if(array_count_values($level_amount)[1]>2)
                <div class="container">
                    <div class="sixteen columns">
                        <div>
                            <ul id="filter">
                                @if(array_count_values($level_amount)[1]>2)
                                    <li><a href="{{ route('flyers') }}?role=*&pdf_type={{ $pdf_type }}"
                                           @if(!isset($role) || $role == '*') class="current" @endif>Show All</a></li>
                                @endif
                                @if($level_amount[1]==1)
                                    <li><a href="{{ route('flyers') }}?role=1&pdf_type={{ $pdf_type }}"
                                           @if($role == '1') class="current" @endif>Practitioner</a></li>
                                @endif
                                @if($level_amount[2]==1)
                                    <li><a href="{{ route('flyers') }}?role=2&pdf_type={{ $pdf_type }}"
                                           @if($role == '2') class="current" @endif>Practitioner and Patient</a>
                                    </li>
                                @endif
                                @if($level_amount[3]==1)
                                    <li><a href="{{ route('flyers') }}?role=3&pdf_type={{ $pdf_type }}"
                                           @if($role == '3') class="current" @endif>Consumer</a></li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            @endif


            <div class="clear"></div>
            <div class="blog-wrapper">
                <div id="blog-grid-masonry">
                    @foreach($AllPDF as $item)
                        <a href={{ route('flyer_details',['id' => preg_replace('/--+/','-',preg_replace('/[^A-Za-z0-9\-]/', '-', $item->name)).'-'.$item->id]) }}>
                            <div class="blog-box-3">
                                <div class="blog-box-1 link-post grey-section">
                                    <div
                                        style="max-height: 270px; position:relative; padding-bottom: 56.25%; height: 0;">
                                        @if($item->preview_img)
                                            <img
                                                src="{{ $image_url }}/www/Images/{{ $item->image()->first()->name }}"
                                                style="margin-left: auto;
                                                 margin-right: auto; position: absolute; top: 0; left: 0; width: 100%; height: 100%;">
                                        @elseif($item->pdf_type()->first()->image()->first())
                                            <img
                                                src="{{ $image_url }}/www/Images/{{ $item->pdf_type()->first()->image()->first()->name }}"
                                                style="margin-left: auto;
                                                 margin-right: auto; position: absolute; top: 0; left: 0; width: 100%; height: 100%;">
                                        @else
                                        @endif
                                    </div>
                                    <h4 class="two-row-max"
                                        style="min-height: 96px;">{{ $item->name }}</h4>
                                    <p class="one-row-max"
                                       style="min-height: 30px;">
                                        Product:&nbsp{{ $item->product_family()->first()->product_name }}</p>
                                    <p class="one-row-max"
                                       style="min-height: 30px;">
                                        Type:&nbsp{{ $item->pdf_type()->first()->name }}</p>
                                    <p class="one-row-max"
                                       style="min-height: 30px;">Language:&nbsp{{ $item->language }}</p>
                                    <p class="one-row-max"
                                       style="min-height: 30px;">Version #&nbsp{{ $item->version_number }}</p>
                                    <br>
                                    <div class="link">&#xf178;</div>
                                </div>
                            </div>
                        </a>
{{--                        @if($item->access_level=='1'&&!Auth::guard('practitioner')->check())--}}

{{--                            <div class="blog-box-3">--}}
{{--                                <a href='#' onClick='open_login()'>--}}
{{--                                    <div class="blog-box-1 grey-section">--}}
{{--                                        <div--}}
{{--                                            style="max-height: 270px; position:relative; padding-bottom: 56.25%; height: 0; opacity: 0.85;background: black; ">--}}
{{--                                            <p style='text-align:center;font-family: initial;font-size:30px;color:white;padding-top: 20%'>--}}
{{--                                                Login as Practitioner to View</p>--}}
{{--                                            --}}{{--                                            <iframe--}}
{{--                                            --}}{{--                                                srcdoc="<p style='text-align:center;margin-top:23%;font-size:45px;color:white'>Please login to view</p>"--}}
{{--                                            --}}{{--                                                src=""--}}
{{--                                            --}}{{--                                                style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"--}}
{{--                                            --}}{{--                                                allowfullscreen></iframe>--}}
{{--                                        </div>--}}
{{--                                        <h4 class="two-row-max"--}}
{{--                                            style="min-height: 96px;">{{ $item->name }}</h4>--}}
{{--                                        <p class="one-row-max"--}}
{{--                                           style="min-height: 30px;">--}}
{{--                                            Product:&nbsp{{ $item->product_family()->first()->product_name }}</p>--}}
{{--                                        <p class="one-row-max"--}}
{{--                                           style="min-height: 30px;">--}}
{{--                                            Type:&nbsp{{ $item->pdf_type()->first()->name }}</p>--}}
{{--                                        <p class="one-row-max"--}}
{{--                                           style="min-height: 30px;">Language:&nbsp{{ $item->language }}</p>--}}
{{--                                        <p class="one-row-max"--}}
{{--                                           style="min-height: 30px;">Version #&nbsp{{ $item->version_number }}</p>--}}
{{--                                        <br>--}}
{{--                                        <div class="link">&#xf178;</div>--}}
{{--                                    </div>--}}
{{--                                </a>--}}
{{--                            </div>--}}
{{--                        @elseif($item->access_level=='4'&&!(Auth::guard('practitioner')->check() && Auth::guard('practitioner')->user()->ahpra_number))--}}
{{--                            <div class="blog-box-3">--}}
{{--                                <a href='#' onClick='open_login()'>--}}
{{--                                    <div class="blog-box-1 grey-section">--}}
{{--                                        <div--}}
{{--                                            style="max-height: 270px; position:relative; padding-bottom: 56.25%; height: 0; opacity: 0.85;background: black; ">--}}
{{--                                            <p style='text-align:center;font-family: initial;font-size:30px;color:white;padding-top: 20%'>--}}
{{--                                                Login as Doctor to View</p>--}}
{{--                                        </div>--}}
{{--                                        <h4 class="two-row-max"--}}
{{--                                            style="min-height: 96px;">{{ $item->name }}</h4>--}}
{{--                                        <p class="one-row-max"--}}
{{--                                           style="min-height: 30px;">--}}
{{--                                            Product:&nbsp{{ $item->product_family()->first()->product_name }}</p>--}}
{{--                                        <p class="one-row-max"--}}
{{--                                           style="min-height: 30px;">--}}
{{--                                            Type:&nbsp{{ $item->pdf_type()->first()->name }}</p>--}}
{{--                                        <p class="one-row-max"--}}
{{--                                           style="min-height: 30px;">Language:&nbsp{{ $item->language }}</p>--}}
{{--                                        <p class="one-row-max"--}}
{{--                                           style="min-height: 30px;">Version #&nbsp{{ $item->version_number }}</p>--}}
{{--                                        <br>--}}
{{--                                        <div class="link">&#xf178;</div>--}}
{{--                                    </div>--}}
{{--                                </a>--}}
{{--                            </div>--}}
{{--                        @elseif($item->access_level=='2'&&!(Auth::guard('practitioner')->check()||Auth::guard('patient')->check()))--}}

{{--                            <div class="blog-box-3">--}}
{{--                                <a href='#' onClick='open_login()'>--}}
{{--                                    <div class="blog-box-1 grey-section">--}}
{{--                                        <div--}}
{{--                                            style="max-height: 270px; position:relative; padding-bottom: 56.25%; height: 0; opacity: 0.85;background: black; ">--}}
{{--                                            <p style='text-align:center;font-family: initial;font-size:30px;color:white;padding-top: 20%'>--}}
{{--                                                Login as Practitioner or Patient to View</p>--}}
{{--                                            --}}{{--                                            <iframe--}}
{{--                                            --}}{{--                                                srcdoc="<p style='text-align:center;margin-top:23%;font-size:45px;color:white'>Please login to view</p>"--}}
{{--                                            --}}{{--                                                src=""--}}
{{--                                            --}}{{--                                                style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"--}}
{{--                                            --}}{{--                                                allowfullscreen></iframe>--}}
{{--                                        </div>--}}
{{--                                        <h4 class="two-row-max"--}}
{{--                                            style="min-height: 96px;">{{ $item->name }}</h4>--}}
{{--                                        <p class="one-row-max"--}}
{{--                                           style="min-height: 30px;">--}}
{{--                                            Product:&nbsp{{ $item->product_family()->first()->product_name }}</p>--}}
{{--                                        <p class="one-row-max"--}}
{{--                                           style="min-height: 30px;">--}}
{{--                                            Type:&nbsp{{ $item->pdf_type()->first()->name }}</p>--}}
{{--                                        <p class="one-row-max"--}}
{{--                                           style="min-height: 30px;">Language:&nbsp{{ $item->language }}</p>--}}
{{--                                        <p class="one-row-max"--}}
{{--                                           style="min-height: 30px;">Version #&nbsp{{ $item->version_number }}</p>--}}
{{--                                        <br>--}}
{{--                                        <div class="link">&#xf178;</div>--}}
{{--                                    </div>--}}
{{--                                </a>--}}
{{--                            </div>--}}
{{--                        @else--}}
{{--                            <a href="{{$item->file_name}}">--}}
{{--                                <div class="blog-box-3">--}}
{{--                                    <div class="blog-box-1 link-post grey-section">--}}
{{--                                        <div--}}
{{--                                            style="max-height: 270px; position:relative; padding-bottom: 56.25%; height: 0;">--}}
{{--                                            @if($item->preview_img)--}}
{{--                                                <img--}}
{{--                                                    src="{{ $image_url }}/www/Images/{{ $item->image()->first()->name }}"--}}
{{--                                                    style="margin-left: auto;--}}
{{--                                                 margin-right: auto; position: absolute; top: 0; left: 0; width: 100%; height: 100%;">--}}
{{--                                            @elseif($item->pdf_type()->first()->image()->first())--}}
{{--                                                <img--}}
{{--                                                    src="{{ $image_url }}/www/Images/{{ $item->pdf_type()->first()->image()->first()->name }}"--}}
{{--                                                    style="margin-left: auto;--}}
{{--                                                 margin-right: auto; position: absolute; top: 0; left: 0; width: 100%; height: 100%;">--}}
{{--                                            @else--}}
{{--                                            @endif--}}
{{--                                        </div>--}}
{{--                                        <h4 class="two-row-max"--}}
{{--                                            style="min-height: 96px;">{{ $item->name }}</h4>--}}
{{--                                        <p class="one-row-max"--}}
{{--                                           style="min-height: 30px;">--}}
{{--                                            Product:&nbsp{{ $item->product_family()->first()->product_name }}</p>--}}
{{--                                        <p class="one-row-max"--}}
{{--                                           style="min-height: 30px;">--}}
{{--                                            Type:&nbsp{{ $item->pdf_type()->first()->name }}</p>--}}
{{--                                        <p class="one-row-max"--}}
{{--                                           style="min-height: 30px;">Language:&nbsp{{ $item->language }}</p>--}}
{{--                                        <p class="one-row-max"--}}
{{--                                           style="min-height: 30px;">Version #&nbsp{{ $item->version_number }}</p>--}}
{{--                                        <br>--}}
{{--                                        <div class="link">&#xf178;</div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </a>--}}
{{--                        @endif--}}
                    @endforeach
                </div>
            </div>


        </section>

        <!-- SELECT PAGE SECTION
        ================================================== -->

        <section class="section grey-section section-padding-top-bottom">
            <div class="container">
                <div class="sixteen columns">
                    <div class="blog-left-right-links pagination">
                        @if($AllPDF->currentPage()-1!=0)
                            <a href="{{ route('flyers') }}?role={{ $role }}&pdf_type={{ $pdf_type }}&page={{ $AllPDF->currentPage()-1 }}">
                                <div class="blog-left-link"><p>PREVIOUS 24</p></div>
                            </a>
                        @endif

                        @if($AllPDF->currentPage()!=$AllPDF->lastPage())
                            <a href="{{ route('flyers') }}?role={{ $role }}&pdf_type={{ $pdf_type }}&page={{ $AllPDF->currentPage()+1 }}">
                                <div class="blog-right-link"><p>NEXT 24</p></div>
                            </a>
                        @endif
                        <a>
                            <div style="padding: 6px">
                                <select onchange="changePage()" id="page" style="float: right;margin-right: 5%;">
                                    @for($i=1;$i<$AllPDF->lastPage()+1;$i++)
                                        <option
                                            value={{ $i }} @if($AllPDF->currentPage() == $i) selected @endif >{{ $i }}</option>
                                    @endfor
                                </select>
                                <p style="float: right">Page No.</p>
                            </div>
                        </a>


                    </div>
                </div>
            </div>

        </section>

        @include('vms.layouts.footer')
    </main>

    <div class="scroll-to-top">&#xf106;</div>



    <!-- JAVASCRIPT
    ================================================== -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdf.js/2.2.228/pdf.min.js"></script>
    <script type="text/javascript">
        function changePage() {
            var page = document.getElementById("page").value;

            location.href = "{{ route('flyers') }}?role={{ $role }}&pdf_type={{ $pdf_type }}&page=" + page;

        }
    </script>
    <script type="text/javascript" src="{{ asset('/js/custom-corporate-home-1.js') }}"></script>



    <!-- End Document
================================================== -->
@endsection
