@extends('vms.layouts.main')
@section('title')
    {{ $pdf->name }}
@endsection
@section('description')
    {{ $pdf->description }}
@endsection
@section('breadcrumbs')
    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset;background-color:#7AA43F;">
            <ol class="breadcrumb medlab_breadcrumbs" style="margin-bottom: unset;background-color:#7AA43F;">
            </ol>
        </div>
    </div>

    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset">
            <ol class="breadcrumb medlab_breadcrumbs">
                <li><a class="medlab_breadcrumbs_link" href="/">@lang('trs.home')</a></li>
                <li><a class="medlab_breadcrumbs_link" href="{{ route('education') }}">@lang('trs.education')</a></li>
                <li><a class="medlab_breadcrumbs_link" href="{{ route('flyers') }}">Flyers</a>
                </li>
                <li class="active medlab_breadcrumbs_text">{{ $pdf->name }}</li>
            </ol>
        </div>
    </div>
@endsection
@section('content')

    <main class="cd-main-content" id="main">

        <!-- TOP SECTION
        ================================================== -->
        <section class="section white-section section-home-padding-top">
            <div class="container">
                <div class="sixteen columns">
                    <div class="section-title left">
                        <h1>{{ $pdf->name }}</h1>
                    </div>
                </div>
            </div>
        </section>

        <!-- ARTICLE CONTENT SECTION
        ================================================== -->

        <section class="section white-section section-padding-bottom">
            <div class="container">
                <div class="twelve columns">
                    <div class="blog-big-wrapper white-section"
                         style="color:white;">
                        @if($pdf->preview_img)
                            <img
                                src="{{ $image_url }}/www/Images/{{ $pdf->image()->first()->name }}"
                                alt="{{ $pdf->preview_img_alt }}">
                        @elseif($pdf->pdf_type()->first()->image()->first())
                            <img
                                src="{{ $image_url }}/www/Images/{{ $pdf->pdf_type()->first()->image()->first()->name }}"
                                alt="{{ $pdf->preview_img_alt }}">
                        @else
                        @endif
                    </div>

                        <div class="grey-section" style="margin-top: 20px;">
                            <div class="articleClass padding">
                                <h4 class="two-row-max"
                                    style="min-height: 96px;">{{ $pdf->name }}</h4>
                                <p class="one-row-max"
                                   style="min-height: 30px;">
                                    Type:&nbsp{{ $pdf->pdf_type()->first()->name }}</p>
                                <p class="one-row-max"
                                   style="min-height: 30px;">Language:&nbsp{{ $pdf->language }}</p>
                                <p class="one-row-max"
                                   style="min-height: 30px;">Version #&nbsp{{ $pdf->version_number }}</p>
                                <p class="one-row-max"
                                   style="min-height: 30px;">Description:&nbsp{!! $pdf->description !!}</p>
                                <br>
                            </div>
                            @if($pdf->access_level=='1'&&!Auth::guard('practitioner')->check())
                            <div style="height: 100px;float: right;">
                                <div style="margin-right: unset;margin-top: 10%;"
                                     class="button-shortcodes text-size-1 text-padding-1 version-1"
                                     onClick="open_login()"><span>&#xf18e;</span> Login as Practitioner to View
                                </div>
                            </div>
                            @elseif($pdf->access_level=='4'&&!(Auth::guard('practitioner')->check() && Auth::guard('practitioner')->user()->ahpra_number))
                                <div style="height: 100px;float: right;">
                                    <div style="margin-right: unset;margin-top: 10%;"
                                         class="button-shortcodes text-size-1 text-padding-1 version-1"
                                         onClick="open_login()"><span>&#xf18e;</span>  Login as Doctor to View
                                    </div>
                                </div>
                            @elseif($pdf->access_level=='2'&&!(Auth::guard('practitioner')->check()||Auth::guard('patient')->check()))
                                <div style="height: 100px;float: right;">
                                    <div style="margin-right: unset;margin-top: 10%;"
                                         class="button-shortcodes text-size-1 text-padding-1 version-1"
                                         onClick="open_login()"><span>&#xf18e;</span>  Login as Practitioner or Patient to View
                                    </div>
                                </div>
                                @else
                                <div style="height: 100px;float: right;">
                                    <a href="{{$pdf->file_name}}">
                                    <div style="margin-right: unset;margin-top: 10%;"
                                         class="button-shortcodes text-size-1 text-padding-1 version-1"
                                         ><span>&#xf18e;</span> Download
                                    </div></a>
                                </div>
                                @endif
                        </div>
                </div>
                <div class="four columns" data-scroll-reveal="enter bottom move 200px over 1s after 0.3s">
                    <div class="post-sidebar">
                        @if(count($product_family))
                            <div class="separator-sidebar"></div>
                            <h3>Related Products</h3>
                            <ul class="link-recents">
                                @foreach($product_family as $item)
                                    <li><a style="color: #6ba53a;word-break: break-word;" href={{ route('product_page', ['id' => preg_replace('/--+/','-',preg_replace('/[^A-Za-z0-9\-]/', '-', $item->product_name)).'-'.$item->id]) }}>{{ $item->product_name }}</a></li>
                                @endforeach
                            </ul>
                        @endif
                    </div>
                </div>
            </div>

        </section>


    <!-- FOOTER
        ================================================== -->

        @include('vms.layouts.footer')


    </main>

    <div class="scroll-to-top">&#xf106;</div>





    <!-- JAVASCRIPT
    ================================================== -->

    <script type="text/javascript" src="{{ asset('/js/medlab.js') }}"></script>


    <!-- End Document
    ================================================== -->
@endsection
