﻿@extends('vms.layouts.main')
@section('title')
    Education
@endsection

@section('breadcrumbs')
    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset;background-color:#7AA43F;">
            <ol class="breadcrumb medlab_breadcrumbs" style="margin-bottom: unset;background-color:#7AA43F;">
            </ol>
        </div>
    </div>

    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset">
            <ol class="breadcrumb medlab_breadcrumbs">
                <li><a class="medlab_breadcrumbs_link" href="/">@lang('trs.home')</a></li>
                <li class="active medlab_breadcrumbs_text">Education</li>
            </ol>
        </div>
    </div>
@endsection




@section('content')


    <main class="cd-main-content" id="main" style="margin-top:90px">


        <section class="section white-section section-padding-top-bottom">
            <div class="container">
                <h1>{{ $header }}</h1>
                <br>
                <h2>Articles</h2>
                <div class="container">

                    <div class="clear"></div>
                    <div class="four columns">
                        <a href="{{ route('articles') }}">
                            <div class="portfolio-box-2 grey-section">
                                @if(json_decode($articles->slider))
                                    @if(count(json_decode($articles->slider)))
                                        @if(json_decode($articles->slider)[0]->layout=='image')
                                            <img
                                                src="{{ $image_url }}/www/Images/{{ \App\Models\MediaLibrary::where('id',json_decode($articles->slider)[0]->attributes->slider_image)->first()->name }}"
                                                alt="{{ json_decode($articles->slider)[0]->attributes->slider_alt }}"
                                                style="display: inline-block;margin-top: unset; width: 100%;"/>
                                        @endif
                                    @endif
                                @endif
                                <div>
                                    <p class="two-row-max"
                                       style="height: 30px; text-align: center">All Articles</p>
                                </div>
                            </div>
                        </a>
                    </div>
                    @foreach($list['Articles'] as $item)
                        <div class="four columns">
                            <a href="{{ route('articles') }}?article_category={{ $item->name }}">
                                <div class="portfolio-box-2 grey-section">
                                    @if(json_decode($item->slider))
                                        @if(count(json_decode($item->slider)))
                                            @if(json_decode($item->slider)[0]->layout=='image')
                                                <img
                                                    src="{{ $image_url }}/www/Images/{{ \App\Models\MediaLibrary::where('id',json_decode($item->slider)[0]->attributes->slider_image)->first()->name }}"
                                                    alt="{{ json_decode($item->slider)[0]->attributes->slider_alt }}"
                                                    style="display: inline-block;margin-top: unset; width: 100%;"/>
                                            @endif
                                        @endif
                                    @endif
                                    <div>
                                        <p class="two-row-max"
                                           style="height: 30px; text-align: center">{{ $item->name }}</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach

                </div>
                <h2>Flyers</h2>
                <div class="container">

                    <div class="clear"></div>
                    <div class="four columns">
                        <a href="{{ route('flyers') }}">
                            <div class="portfolio-box-2 grey-section">
                                @if(json_decode($flyers->slider))
                                    @if(count(json_decode($flyers->slider)))
                                        @if(json_decode($flyers->slider)[0]->layout=='image')
                                            <img
                                                src="{{ $image_url }}/www/Images/{{ \App\Models\MediaLibrary::where('id',json_decode($flyers->slider)[0]->attributes->slider_image)->first()->name }}"
                                                alt="{{ json_decode($flyers->slider)[0]->attributes->slider_alt }}"
                                                style="display: inline-block;margin-top: unset; width: 100%;"/>
                                        @endif
                                    @endif
                                @endif
                                <div>
                                    <p class="two-row-max"
                                       style="height: 30px; text-align: center">All Flyers</p>
                                </div>
                            </div>
                        </a>
                    </div>
                    @foreach($list['Flyers'] as $item)
                        <div class="four columns">
                            <a href="{{ route('flyers') }}?pdf_type={{ $item->name }}">
                                <div class="portfolio-box-2 grey-section">
                                    @if(json_decode($item->slider))
                                        @if(count(json_decode($item->slider)))
                                            @if(json_decode($item->slider)[0]->layout=='image')
                                                <img
                                                    src="{{ $image_url }}/www/Images/{{ \App\Models\MediaLibrary::where('id',json_decode($item->slider)[0]->attributes->slider_image)->first()->name }}"
                                                    alt="{{ json_decode($item->slider)[0]->attributes->slider_alt }}"
                                                    style="display: inline-block;margin-top: unset; width: 100%;"/>
                                            @endif
                                        @endif
                                    @endif
                                    <div>
                                        <p class="two-row-max"
                                           style="height: 30px; text-align: center">{{ $item->name }}</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach

                </div>
            </div>
        </section>

        @include('vms.layouts.footer')


    </main>

    <div class="scroll-to-top">&#xf106;</div>





    <!-- JAVASCRIPT
    ================================================== -->
    <script type="text/javascript" src="{{ asset('/js/medlab.js') }}"></script>

    <!-- End Document
================================================== -->
@endsection
