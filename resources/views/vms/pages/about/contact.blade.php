﻿@extends('vms.layouts.main')
@section('title')
    Medlab - {{ $content->title }}
@endsection
@section('description')
    @if($content->meta_description)
        {{ $content->meta_description }}
    @elseif(json_decode($content->content))
        @foreach(json_decode($content->content) as $item)
            @if($item->layout=='text')
                {{ substr(str_replace('&nbsp;',' ',strip_tags($item->attributes->content)),0,160) }}
            @endif
        @endforeach
    @endif
@endsection
@section('breadcrumbs')
    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset;background-color:#7AA43F;">
            <ol class="breadcrumb medlab_breadcrumbs" style="margin-bottom: unset;background-color:#7AA43F;">
            </ol>
        </div>
    </div>

    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset">
            <ol class="breadcrumb medlab_breadcrumbs">
                <li><a class="medlab_breadcrumbs_link" href="/">@lang('trs.home')</a></li>
                <li><a class="medlab_breadcrumbs_link" href="{{ route('about') }}">About</a></li>
                <li class="active medlab_breadcrumbs_text">Contact</li>
            </ol>
        </div>
    </div>
@endsection


@section('content')


    <main class="cd-main-content" id="main" style="margin-top:90px">

        <!-- SLIDER IMAGE
================================================== -->

        @if(json_decode($content->slider))
            <section class="home">
                <div class="slider-container">
                    <div class="tp-banner-container">
                        <div class="tp-banner">
                            <ul>
                                @foreach(json_decode($content->slider) as $slider)
                                    @if($slider->layout=='video')
                                        <li data-transition="fade" data-slotamount="1" data-masterspeed="500"
                                            data-saveperformance="on" data-title="Intro Slide">
                                            <iframe
                                                src="https://player.vimeo.com/video/{{ $slider->attributes->video_link }}"
                                                width="100%" height="100%" align="center" frameborder="0"
                                                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                                allowfullscreen></iframe>
                                        </li>
                                    @elseif($slider->layout=='image')
                                        <li data-transition="fade" data-slotamount="1" data-masterspeed="500"
                                            data-saveperformance="on" data-title="Intro Slide">
                                            <img
                                                src="{{ $image_url }}/www/Images/{{ \App\Models\MediaLibrary::where('id',$slider->attributes->slider_image)->first()->name }}"
                                                alt="{{ $slider->attributes->slider_alt }}"
                                                data-lazyload="{{ $image_url }}/www/Images/{{ \App\Models\MediaLibrary::where('id',$slider->attributes->slider_image)->first()->name }}"
                                                data-bgposition="center top" data-bgfit="cover"
                                                data-bgrepeat="no-repeat">
                                            <a href="#">
                                                <div class="black-heavy-3">
                                                    <div class="black-heavy-3-heading">
                                                        <h1>{{ $slider->attributes->title }}</h1></div>
                                                    <div
                                                        class="black-heavy-3-subheading">{{ $slider->attributes->subtitle }}</div>
                                                </div>
                                            </a>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
        @else
            <br>
        @endif

        <!-- CONTENT SECTION
    ================================================== -->
        @include('vms.layouts.content_section')
        <!-- MAIN OFFICES SECTION
    ================================================== -->

        <section class="section grey-section">
            <div class="office-1">
                <div class="box-1">
                    <iframe width="100%" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3310.0236266270413!2d151.19620771614922!3d-33.94052043021803!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6b12b1108182753b%3A0x733239442e7fe37c!2s11-13%20Lord%20St%2C%20Botany%20NSW%202019!5e0!3m2!1sen!2sau!4v1612839386323!5m2!1sen!2sau"></iframe>
                </div>
                <div class="box-1">
                    <div class="text-in">
                        <div class="section-title left office-text">
                            <h2>Head Office</h2>
                            <div class="subtitle left">11-13 Lord St Botany<br>
                                Building A, Unit A5 – A6, Botany Quarter<br>
                                NSW, 2019, Australia</div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="section" id="scroll-link">
            <div class="call-to-action-1">
                <h5 style="text-transform: unset;margin-bottom: unset;">Toll Free: 1300 369 570<br>
                    Phone: 02 8188 0311<br>
                    Fax: 02 9699 3347<br>
                    Email: hello@medlab.co<br>
                    Hours: Monday - Friday. 8:30am to 5pm AEST</h5>
            </div>
        </section>
        <section class="section grey-section">
            <div class="office-1">

                <div class="box-1">
                    <div class="text-in">
                        <div class="section-title left office-text">
                            <h2>Research Office & Laboratory</h2>
                            <div class="subtitle left">66 McCauley St
                                Alexandria, NSW
                                Australia 2015</div>
                        </div>
                    </div>
                </div>
                <div class="box-1">
                    <iframe width="100%" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3311.427536944767!2d151.19707685120267!3d-33.90439442824433!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6b12b1c6327606f3%3A0x8dc3ac4dbd083421!2s66%20McCauley%20St%2C%20Alexandria%20NSW%202015!5e0!3m2!1sen!2sau!4v1588313705410!5m2!1sen!2sau"></iframe>
                </div>
            </div>
        </section>

        <!-- REGIONAL OFFICES SECTION
================================================== -->

        <section class="section white-section section-padding-top-bottom">
            <div class="container">
                <div class="sixteen columns">
                    <div class="section-title">
                        <h2>REGIONAL OFFICES</h2>
                    </div>
                </div>

                <div class="eight columns" data-scroll-reveal="enter left move 200px over 1s after 0.8s">
                    <div class="team-box-1 full-image-box">
                        <iframe width="100%" height="280" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3151.4138372901875!2d144.90482411557568!3d-37.8271965797499!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6ad6677213adc2a9%3A0x99a5cd8e4700a2c7!2sUnit%203%2F31%20Sabre%20Dr%2C%20Port%20Melbourne%20VIC%203207!5e0!3m2!1sen!2sau!4v1588311370554!5m2!1sen!2sau"></iframe>
                        <div class="team-box-1-text-in grey-section">
                            <div class="team-name-top">Victorian Office</div>
                            <h4>Medlab</h4>
                            <p>3/31 Sabre Dr, <br>
                                Port Melbourne <br>
                                VIC 3207, Australia
                            </p>
                        </div>
                    </div>
                </div>
                <div class="eight columns" data-scroll-reveal="enter right move 200px over 1s after 0.3s">
                    <div class="team-box-1 full-image-box">
                        <iframe width="100%" height="280" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3321.6768766535242!2d-117.60831664903175!3d33.63961924671368!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcebafbb3a3a35%3A0xca48fe32e91021b!2s30021%20Tomas%20%23150%2C%20Rancho%20Santa%20Margarita%2C%20CA%2092688%2C%20USA!5e0!3m2!1sen!2sau!4v1588314530041!5m2!1sen!2sau"></iframe>
                        <div class="team-box-1-text-in grey-section">
                            <div class="team-name-top">Californian Office</div>
                            <h4>Medlab Clinical US, Inc</h4>
                            <p>30021 Tomas,Suite 150 <br>
                                Rancho Santa Margarita, <br>
                                CA 92679, USA</p>
                        </div>
                    </div>
                </div>
                <div class="eight columns" data-scroll-reveal="enter left move 200px over 1s after 0.3s">
                    <div class="team-box-1 full-image-box">
                        <iframe width="100%" height="280" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3310.0744162553938!2d151.1946691152121!3d-33.939214080636866!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6b12b11aed8bbda9%3A0x5c47b5b8f6fbf51b!2s5%2F6+Lord+St%2C+Botany+NSW+2019!5e0!3m2!1sen!2sau!4v1562030785868!5m2!1sen!2sau"></iframe>
                        <div class="team-box-1-text-in grey-section">
                            <div class="team-name-top">Malta Office</div>
                            <h4>MDC Europe Limited</h4>
                            <p>Level 1, <br>
                                Casla Naxaro,
                                <br>Labour Ave
                                Naxxar<br>
                                NXR 9021, Malta
                            </p>
                        </div>
                    </div>
                </div>
                <div class="eight columns" data-scroll-reveal="enter right move 200px over 1s after 0.8s">
                    <div class="team-box-1 full-image-box">
                        <iframe width="100%" height="280" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2483.222858476714!2d-0.13326928402775012!3d51.509127279635464!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x487604d18e6d459b%3A0xab2023f233cb14d3!2s10%20Orange%20St%2C%20West%20End%2C%20London%20WC2H%207DQ%2C%20UK!5e0!3m2!1sen!2sau!4v1588311566061!5m2!1sen!2sau"></iframe>
                        <div class="team-box-1-text-in grey-section">
                            <div class="team-name-top">UK Office</div>
                            <h4>Medlab Research LTD</h4>
                            <p>10 Orange Street<br>
                                Haymarket<br>
                                London, WC2H 7DQ,<br>
                                United Kingdom</p>
                        </div>
                    </div>
                </div>

            </div>
        </section>

        <!-- GET IN TOUCH FORM
================================================== -->


        <section class="section grey-section section-padding-top-bottom" id="scroll-link-6">

            <div class="container">
                <div class="sixteen columns">
                    <div class="section-title">
                        <h2>Contact Us</h2>
                    </div>
                </div>
                <div class="alert alert-red  error" id="err-form">There was a problem validating the form please check!</div>
                <div class="alert alert-red  error" id="err-timedout">The connection to the server timed out!</div>
                <div class="alert alert-red error" id="err-state"></div>
                <div class="clear"></div>

                <form name="ajax-form" id="ajax-form" action="{{ url('/contact') }}" method="post">
                    {!! csrf_field() !!}
                    <div class="eight columns">
                        <label for="name" class="uname required"> Name
                            <span class="error" id="err-name" style="color: red">please enter name</span>
                        </label>
                        <input name="name" id="name" type="text" style="text-transform: unset;" placeholder="Your Name: *"/>
                    </div>
                    <div class="eight columns">
                        <label for="email" class="uname required"> E-Mail Address
                            <span class="error" id="err-email" style="color: red">please enter e-mail</span>
                            <span class="error" id="err-emailvld" style="color: red">e-mail is not a valid format</span>
                            <span class="error" id="err-notexist" style="color: red">e-mail verification failed</span>
                        </label>
                        <input name="email" id="email" type="text" style="text-transform: unset;" placeholder="E-Mail: *"/>
                    </div>
                    <div class="sixteen columns">
                        <label for="message">Message</label>
                        <textarea name="message" id="message" style="text-transform: unset;" placeholder="Tell Us Everything"></textarea>
                    </div>
                    <div class="sixteen columns">
                        <div class="g-recaptcha" data-callback="recaptcha_callback" data-sitekey="{{ config('app.recaptcha_site_key') }}" style="text-align: -webkit-center"></div>
                        <div id="button-con"><button class="send_message" id="send" disabled style="display: none">submit</button></div>
                    </div>
                    <div class="clear"></div>

                </form>

                <div class="clear"></div>

                <div id="ajaxsuccess"></div>

            </div>

            <div class="clear"></div>

        </section>

{{--        <!-- MORE PAGES SECTION ABOUT US--}}
{{--                ================================================== -->--}}
{{--        <section class="section white-section section-padding-top-bottom">--}}
{{--            <div class="container">--}}
{{--                <div class="sixteen columns">--}}
{{--                    <h2>More</h2>--}}
{{--                </div>--}}
{{--                <div class="container">--}}

{{--                    <div class="clear"></div>--}}
{{--                    @foreach($about_list as $item)--}}
{{--                        <div class="four columns">--}}
{{--                            <a href="{{ $item->page_url }}">--}}
{{--                                <div class="portfolio-box-2 grey-section">--}}
{{--                                    @if(json_decode($item->slider))--}}
{{--                                        @foreach(json_decode($item->slider) as $slider)--}}
{{--                                            @if($slider->layout=='image')--}}
{{--                                                <img--}}
{{--                                                    src="{{ $image_url }}/www/Images/{{ \App\Models\MediaLibrary::where('id',$slider->attributes->slider_image)->first()->name }}"--}}
{{--                                                    style="display: inline-block;margin-top: unset; width: 100%;"/>--}}
{{--                                            @endif--}}
{{--                                        @endforeach--}}
{{--                                    @endif--}}
{{--                                    <div>--}}
{{--                                        <p class="two-row-max"--}}
{{--                                           style="height: 30px; text-align: center">{{ $item->title }}</p>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </a>--}}
{{--                        </div>--}}
{{--                    @endforeach--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </section>--}}

        @include('vms.layouts.footer')


    </main>

    <div class="scroll-to-top">&#xf106;</div>





    <!-- JAVASCRIPT
    ================================================== -->
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script type="text/javascript" src="{{ asset('/js/medlab.js') }}"></script>
    <script type="text/javascript">
        function recaptcha_callback(){
            $('#send').prop("disabled", false);
            document.getElementById('send').style.display = 'initial';
        }
        jQuery(document).ready(function ($) { // wait until the document is ready
            $('#send').click(function(){ // when the button is clicked the code executes
                $('.error').fadeOut('slow'); // reset the error messages (hides them)

                var error = false; // we will set this true if the form isn't valid

                var name = $('input#name').val();// get the value of the input field
                if(name == "" || name == " ") {
                    $('#err-name').fadeIn('slow'); // show the error message
                    error = true; // change the error state to true
                }

                var email_compare = /^([a-z0-9_.-]+)@([da-z.-]+).([a-z.]{2,6})$/; // Syntax to compare against input
                var email = $('input#email').val(); // get the value of the input field
                if (email == "" || email == " ") { // check if the field is empty
                    $('#err-email').fadeIn('slow'); // error - empty
                    error = true;
                }else if (!email_compare.test(email)) { // if it's not empty check the format against our email_compare variable
                    $('#err-emailvld').fadeIn('slow'); // error - not right format
                    error = true;
                }

                if(error == true) {
                    $('#err-form').slideDown('slow');
                    return false;
                }

                var data_string = $('#ajax-form').serialize(); // Collect data from form

                console.log(data_string);
                $.ajax({
                    type: "POST",
                    url: "/contact",
                    data: data_string,
                    // timeout: 6000,
                    error: function(data) {
                        console.log(data);
                        if (error == "timeout") {
                            $('#err-timedout').slideDown('slow');
                        }
                        else {
                            $('#err-state').slideDown('slow');
                            $("#err-state").html('An error occurred: ' + data['responseJSON'] + '');
                        }
                    },
                    success: function(data) {
                        console.log(data);
                        $('#ajax-form').slideUp('slow');
                        document.getElementById('ajaxsuccess').innerHTML=data['success'];
                        $('#ajaxsuccess').slideDown('slow');
                    }
                });

                return false; // stops user browser being directed to the php file
            }); // end click function
        });
        document.getElementById('email').addEventListener('change', function validateEmail() {
            if(this.value){
                Swal.fire({
                    title: 'Checking ..',
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    showConfirmButton: false,
                    showCloseButton: false,
                    showCancelButton: false,
                    onOpen: () => {
                        Swal.showLoading();

                        $.ajax({
                            type: "POST",
                            url: '{{ url('email_validation') }}',
                            data: {
                                _token: '{{ csrf_token() }}',
                                email: this.value,
                            },
                            timeout: 6000,
                            success: function (data) {
                                if (data.code == '2') {
                                    Swal.close();
                                    $('#err-notexist').fadeIn('slow'); // error - empty
                                    document.getElementById('email').value = '';
                                } else if (data.code == '3') {
                                    Swal.close();
                                    $('#err-timedout').fadeIn('slow'); // error - empty
                                    document.getElementById('email').value = '';
                                }else{
                                    $('#err-notexist').fadeOut('slow');
                                    $('#err-timedout').fadeOut('slow');
                                    Swal.close();
                                }
                            }

                        });
                    }
                })
            }

        })
    </script>
    <!-- End Document
================================================== -->
@endsection
