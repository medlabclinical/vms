﻿@extends('vms.layouts.main')
@section('title')
    Medlab - About & Partnering
@endsection

@section('breadcrumbs')
    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset;background-color:#7AA43F;">
            <ol class="breadcrumb medlab_breadcrumbs" style="margin-bottom: unset;background-color:#7AA43F;">
            </ol>
        </div>
    </div>

    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset">
            <ol class="breadcrumb medlab_breadcrumbs">
                <li><a class="medlab_breadcrumbs_link" href="/">@lang('trs.home')</a></li>
                <li class="active medlab_breadcrumbs_text">About</li>
            </ol>
        </div>
    </div>
@endsection




@section('content')


    <main class="cd-main-content" id="main" style="margin-top:90px">


        <section class="section white-section section-padding-top-bottom">
            <div class="container">
                <h1>{{ $header }}</h1>
                <br>
                @foreach($list as $category => $category_list)
                    <div class="container">

                        <div class="clear"></div>

                        @foreach($category_list as $item)
                            <div class="four columns">
                                <a href="{{ $item->page_url }}">
                                    <div class="portfolio-box-2 grey-section">
                                        @if($item->bottom_img)
                                            <img
                                                src="{{ $image_url }}/www/Images/{{ \App\Models\MediaLibrary::where('id',$item->bottom_img)->first()->name }}"
                                                alt="{{ $item->bottom_img_alt }}"
                                                style="display: inline-block;margin-top: unset; width: 100%;"/>
                                        @elseif(count(json_decode($item->slider)))
                                            @if(json_decode($item->slider)[0]->layout=='image')
                                                <img
                                                    src="{{ $image_url }}/www/Images/{{ \App\Models\MediaLibrary::where('id',json_decode($item->slider)[0]->attributes->slider_image)->first()->name }}"
                                                    alt="{{ json_decode($item->slider)[0]->attributes->slider_alt }}"
                                                    style="display: inline-block;margin-top: unset; width: 100%;"/>
                                            @endif
                                        @endif
                                        <div>
                                            <p class="two-row-max"
                                               style="height: 30px; text-align: center">{{ $item->title }}</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach

                    </div>
                @endforeach
            </div>
        </section>

        @include('vms.layouts.footer')


    </main>

    <div class="scroll-to-top">&#xf106;</div>





    <!-- JAVASCRIPT
    ================================================== -->
    <script type="text/javascript" src="{{ asset('/js/medlab.js') }}"></script>

    <!-- End Document
================================================== -->
@endsection
