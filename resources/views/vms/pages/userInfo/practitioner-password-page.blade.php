@extends('vms.layouts.main')
@section('title')
    Practitioner Password
@endsection
@section('description')
    Practitioner Password Page
@endsection
@section('breadcrumbs')
    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset;background-color:#7AA43F;">
            <ol class="breadcrumb medlab_breadcrumbs" style="margin-bottom: unset;background-color:#7AA43F;">
            </ol>
        </div>
    </div>

    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset">
            <ol class="breadcrumb medlab_breadcrumbs">
                <li><a class="medlab_breadcrumbs_link" href="/">@lang('trs.home')</a></li>
                <li class="active medlab_breadcrumbs_text">Practitioner Password</li>
            </ol>
        </div>
    </div>
@endsection
@section('content')
    <main class="cd-main-content" id="main" style="margin-top:90px">

        <section class="section white-section section-padding-top-bottom" id="scroll-link-6">
            <div class="container">

                <div class="sixteen columns">
                    <div class="section-title">
                        <h2>RESET PASSWORD</h2>
                        {{--                        <div class="subtitle big">Medlab</div>--}}
                    </div>
                </div>


                {{--                <div class="sixteen columns" data-scroll-reveal="enter right move 200px over 1s after 0.3s">--}}
                {{--                </div>--}}
{{--                @if(Auth::guard('practitioner')->check())--}}
                    <form name="ajax-form" id="ajax-form" action="{{ route('update_practitioner_password') }}"
                          method="post">
                        @csrf
                        <div class="sixteen columns">
                            <div class="row">
                                <label for="practitioner_password" class="youpasswd"> Your current password</label>
                                <input id="practitioner_password" type="password" class="form-control"
                                       name="current_password"
                                       value="{{$role->practitioner()->first()->password}}" required
                                       autocomplete="email" autofocus style="text-transform: unset;">
                            </div>

                            <div class="row">
                                <label for="practitioner_password-confirm" class="youpasswd required"> Enter new password </label>
                                <input id="practitioner_password-confirm" type="password" class="form-control"
                                       name="new_password"
                                       value="{{$role->practitioner()->first()->password}}" required
                                       autofocus style="text-transform: unset;">
                            </div>

                            <div class="alert alert-blue">
                                <p style="text-transform: unset;">Password needs to be 8 or more characters</p>
                            </div>
                            <br>
                            <div class="row">
                                <label for="practitioner_password-confirmation" class="youpasswd required"> Confirm your password </label>
                                <input id="practitioner_password-confirmation" type="password"
                                       class="form-control"
                                       name="new_password_confirm" required
                                       autofocus style="text-transform: unset;" >
                            </div>

                            <div class="sixteen columns">
                                <div id="button-con">
                                    <button class="send_message" type="submit">update</button>
                                </div>
                            </div>
                        </div>
                    </form>
            </div>
        </section>
    </main>
{{--    @endif--}}
    @include('vms.layouts.footer')

    <!-- JAVASCRIPT
    ================================================== -->
    <script type="text/javascript" src="{{ asset('/js/medlab.js') }}"></script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            $(document).ready(function () {
                document.getElementById("practitioner_password-confirm").addEventListener('change', function () {
                    if (this.value.length < 8) {
                        document.getElementById('practitioner_password-confirm').value = "";
                        Swal.fire({
                            title: 'Password too weak',
                            text: "Password needs to be 8 or more characters",
                            icon: 'error',
                        })
                    }else {
                        Swal.fire({
                            title: 'Validating Password ..',
                            allowOutsideClick: false,
                            allowEscapeKey: false,
                            showConfirmButton: false,
                            showCloseButton: false,
                            showCancelButton: false,
                            onOpen: () => {
                                Swal.showLoading();
                                $.ajax({
                                    type: "POST",
                                    url: '{{ url('validate_password') }}',
                                    data: {
                                        _token: '{{ csrf_token() }}',
                                        password: this.value,
                                    },
                                    timeout: 6000,
                                    success: function (data) {
                                        if (data.number > 1000) {
                                            document.getElementById('practitioner_password-confirm').value = "";
                                            Swal.fire({
                                                icon: 'error',
                                                html: 'The password you have entered has been found ' + data.number + ' times in the public data breach registry. Passwords found over 1000 times cannot be accepted. Please choose a different password. For more information head to <a href="https://haveibeenpwned.com/Passwords" target="_blank" color="#6ba53a">https://haveibeenpwned.com/Passwords</a>.'
                                            })
                                        } else if (data.number > 100 && data.number <= 1000) {
                                            Swal.fire({
                                                icon: 'info',
                                                html: 'The password you have entered has been found ' + data.number + ' times in the public data breach registry. We recommend choosing a different password, but you can continue if you wish. For more information head to <a href="https://haveibeenpwned.com/Passwords" target="_blank" color="#6ba53a">https://haveibeenpwned.com/Passwords</a>.'
                                            })

                                        } else {
                                            Swal.close()
                                        }
                                    }

                                });
                            }
                        });
                    }
                });
                $("#ajax-form").submit(function (e) {

                    e.preventDefault(); // avoid to execute the actual submit of the form.
{{--                        @if(Auth::guard('practitioner')->check())--}}
//                     var passw = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/;
                    var newPassword = document.getElementById("practitioner_password-confirm").value;
                    var confirmPassword = document.getElementById("practitioner_password-confirmation").value;
                    // console.log(password);
                    // console.log(confirmPassword);
                    if (newPassword !== confirmPassword) {
                        Swal.fire({
                            title: 'Error',
                            text: "Passwords do not match, please try again",
                            icon: 'error',
                        })
                    }else if(newPassword.length < 8){
                        Swal.fire({
                            title: 'Password too weak',
                            text: "Password needs to be 8 or more characters",
                            icon: 'error',
                        })
                    }
                    else{
                        var form = $(this);
                        var url = form.attr('action');

                        $.ajax({
                            type: "POST",
                            url: url,
                            data: form.serialize(), // serializes the form's elements.
                            success: function (data) {
                                if (data.msg == 'success') {
                                    Swal.fire({
                                        icon: 'success',
                                        title: 'Password reset successfully ',
                                        confirmButtonText: 'OK',
                                        allowOutsideClick: false
                                    }).then((result) => {
                                        if (result.value) {
                                            window.location.href = "/";
                                        }
                                    });

                                }else if(data.msg == 'wrong password'){
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Wrong Password',
                                        confirmButtonText: 'OK',
                                        allowOutsideClick: false
                                    })
                                } else {
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Something is wrong, please try again',
                                        confirmButtonText: 'OK',
                                        allowOutsideClick: false
                                    })
                                }
                            }
                        });
                    }
                });

            });
        })(jQuery);

    </script>

@endsection
