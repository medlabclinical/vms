@extends('vms.layouts.main')
{{--@section('breadcrumbs')--}}
{{--    <div class="medlab_breadcrumbs_wrapper">--}}
{{--        <div class="container" style="width: unset;background-color:#7AA43F;">--}}
{{--            <ol class="breadcrumb medlab_breadcrumbs" style="margin-bottom: unset;background-color:#7AA43F;">--}}
{{--            </ol>--}}
{{--        </div>--}}
{{--    </div>--}}

{{--    <div class="medlab_breadcrumbs_wrapper">--}}
{{--        <div class="container" style="width: unset">--}}
{{--            <ol class="breadcrumb medlab_breadcrumbs">--}}
{{--                <li><a class="medlab_breadcrumbs_link" href="/">@lang('trs.home')</a></li>--}}
{{--                <li><a class="medlab_breadcrumbs_link" href="/research">@lang('trs.research')</a></li>--}}
{{--                <li class="active medlab_breadcrumbs_text">@lang('trs.patents')</li>--}}
{{--            </ol>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--@endsection--}}
@section('content')


    <main class="cd-main-content" id="main" style="margin-top:90px">

        <section class="section grey-section section-padding-top">
            <div class="container">
                <div class="sixteen columns" data-scroll-reveal="enter left move 200px over 1s after 0.3s">

                </div>
                <form name="ajax-form" id="ajax-form" action="{{ route('edit_patient_detail') }}"
                      method="post">
                    @csrf
                    <div id="patientForm" style="">
                        <div class="sixteen columns"
                             data-scroll-reveal="enter right move 200px over 1s after 0.3s">
                            <div class="services-boxes-1"
                                 style="cursor: pointer; width:100%">
                                <div class="icon-box">&#xf007;</div>
                                <h6>Patient Information</h6>
                            </div>
                                <div class="sixteen columns">


                                    <h4>My Address Details</h4><br>
                                    <div class="row">

                                        <label for="patient_international" class="uname" style="display:inline-block;">
                                            Show International</label>
                                        <input id="patient_international" type="checkbox"
                                               class="form-control"
                                               name="patient_international" style="display:inline-block;width: unset"/>

                                    </div>
                                    <input type="text" name="patient_address_id"
                                           value="{{ isset($patient_address)?$patient_address->id:null }}" hidden/>
                                    <div class="row">
                                        <label for="patient_street_address" class="uname required" style="width: 100%;">
                                            Street Address</label>

                                        <input id="patient_street_address" type="text"
                                               class="form-control" name="address_line_1"
                                               value="{{ isset($patient_address)?$patient_address->address_line_1:null }}"
                                               required/>
                                    </div>
                                    <div class="row">
                                        <label for="patient_second_address" class="uname" style="width: 100%;">
                                            A second line if you need (optional)</label>
                                        <input id="patient_second_address" type="text"
                                               class="form-control"
                                               name="address_line_2" value="{{ isset($patient_address)?$patient_address->address_line_2:null}}"
                                               autocomplete="googleignoreautofill"/>
                                    </div>
                                    <div class="row" style="margin-bottom: unset;">
                                        <label for="patient_locality" class="uname required"
                                               style="width: 33.3%;float: left">Suburb/Postcode</label>
                                        <label for="patient_administrative_area_level_1" class="uname"
                                               style="width: 33.3%;float: left">&nbsp;</label>
                                        <label for="patient_postal_code" class="uname" style="width: 33.3%;float: left">&nbsp;</label>
                                    </div>
                                    <div class="row" style="position:relative" id="patient_locality_row">

                                        <input id="patient_locality" type="text" class="form-control" name="suburb"
                                               value="{{isset($patient_address)?$patient_address->suburb:null}}"
                                               style="width:100%;float: left;position: absolute" required/>

                                        <input id="patient_administrative_area_level_1" type="text" class="form-control"
                                               name="state"
                                               value="{{isset($patient_address)?$patient_address->state:null}}" autocomplete="googleignoreautofill"
                                               style="width: 33.3%;float: left;margin-left: 33.3%" readonly>

                                        <input id="patient_postal_code" type="text" class="form-control" name="postcode"
                                               style="width: 33.3%;float: left"
                                               value="{{isset($patient_address)?$patient_address->postcode:null}}"
                                               autocomplete="googleignoreautofill" readonly>

                                    </div>

                                    <div class="row">
                                        <label for="patient_country" class="uname" style="width: 100%;">Country</label>
                                        <input id="patient_country" type="text" class="form-control" name="country"
                                               value="{{isset($patient_address)?$patient_address->country:null}}" autocomplete="googleignoreautofill"
                                               readonly/>
                                    </div>

                                    <br>
                                    <h4>My Practitioner Detail</h4> <br>
                                    <div class="row">
                                        <label for="referral_code" class="uname"
                                               style="width: 100%;float:left;"> Referral Code as provided by Referring Practitioner</label>
                                        <input id="referral_code" type="text"
                                               class="form-control"
                                               name="referral_code"
                                               value="{{ $role->guid?$role->practitioner()->first()->referral_code:null }}"
                                               style="width: 49%;float:left;"/>
                                    </div>

                                    <div class="alert alert-blue" id="referral_name">
                                        <p>
                                            <p style="display: inline"><span>&#xf129;</span>
                                            </p>Your practitioner is <p style="display: inline" id="practitioner_name">{{$role->guid?$role->practitioner()->first()->customer_name:null}}</p>
{{--                                        </p>--}}
                                    </div>
                                    <br>
                                    <h4>My Personal Details</h4><br>
                                    <div class="row">
                                        <label for="salutation" class="uname required">Title</label>
                                        <select id="title" type="text"
                                                class="form-control"
                                                name="salutation"
                                                style="text-transform: unset;width: 49%;">
                                            <option></option>
                                            <option @if($role->salutation=='Mr') selected @endif>Mr</option>
                                            <option @if($role->salutation=='Ms') selected @endif>Ms</option>
                                            <option @if($role->salutation=='Miss') selected @endif>Miss</option>
                                            <option @if($role->salutation=='Dr.') selected @endif>Dr.</option>
                                            <option @if($role->salutation=='Prof.') selected @endif>Prof.</option>
                                        </select>
                                    </div>

                                    <div class="row">
                                        <input type="text" name="id"
                                               value={{ Auth::guard('patient')->user()->id }} hidden/>
                                        <label for="patient_firstname" class="uname"> First Name</label>
                                        <input id="patient_firstname" type="text"
                                               class="form-control"
                                               name="first_name"
                                               value={{ $role->first_name }}
                                                   style="text-transform:unset">
                                    </div>

                                    <div class="row">
                                        <label for="patient_lastname" class="uname required"> Last Name</label>
                                        <input id="patient_lastname" type="text"
                                               class="form-control"
                                               name="last_name" required
                                               value={{ $role->last_name }}
                                                   style="text-transform:unset">
                                    </div>

                                    <div class="row">
                                        <label for="patient_email" class="uname required"> Your email</label>
                                        <input id="patient_email" type="email"
                                               class="form-control"
                                               name="email"
                                               value="{{ $role->email }}"
                                               autocomplete="email" autofocus style="text-transform:unset" required/>
                                    </div>

                                    <div class="row">
                                        <label for="patient_phone" class="uname" style="width: 100%;">
                                            Telephone</label>

                                        <input id="patient_phone" type="number"
                                               class="form-control"
                                               name="phone"
                                               value="{{$role->phone }}"
                                               style="text-transform: unset;">
                                    </div>

                                    <div class="row">
                                        <label for="patient_mobile" class="uname" style="width: 100%;">
                                            Mobile
                                            Phone</label>
                                        <input id="patient_mobile" type="number"
                                               class="form-control"
                                               name="mobile"
                                               value="{{$role->mobile }}"
                                               style="text-transform: unset;">
                                    </div>

                                    <div class="sixteen columns">
                                        <div id="button-con">
                                            <button class="send_message" type="submit">update</button>
                                        </div>

                                    </div>

                                </div>


                        </div>
                    </div>
                </form>
            </div>
        </section>
    </main>
    @include('vms.layouts.footer')

    <div class="scroll-to-top">&#xf106;</div>

    <!-- JAVASCRIPT
    ================================================== -->
    <script type="text/javascript" src="{{ asset('/build/js/intlTelInput.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/medlab.js') }}"></script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            $(document).ready(function () {
                $('#ajax-form').submit(function (e) {
                    e.preventDefault(); // avoid to execute the actual submit of the form.
                    var form = $(this);
                    var url = form.attr('action');
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: form.serialize(), // serializes the form's elements.
                        success: function (data) {
                            console.log(data);
                            if (data.msg=='success') {
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Your update was successful!',
                                    confirmButtonText: 'OK',
                                    allowOutsideClick: false
                                }).then((result) => {
                                    if (result.value) {
                                        // document.getElementById("ajax-form").submit();
                                        window.location.reload();
                                    }
                                })
                            } else {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Something is wrong, please try again later',
                                    // title: data.msg,
                                })
                            }
                        }
                    });


                })
            });
        })(jQuery);

    </script>

    <script type="text/javascript">

        // document.getElementById("practitioner_name")[0].removeAttribute("referral_code");


        document.getElementById('referral_code').addEventListener('change', function () {
            $.ajax({
                type: "POST",
                url: '{{ url('referral_check') }}',
                data: {
                    _token: '{{ csrf_token() }}',
                    code: this.value,
                },
                timeout: 6000,
                success: function (data) {
                    var origin_referral = "{!! $referral_code !!}";
                    var origin_referral_name = "{!! $referral_name !!}";
                    if (data.code == '1') {
                        Swal.fire({
                            icon: 'question',
                            title: 'Do you want to add ' + data.top + ' as your practitioner?',
                            showCancelButton: true,
                            allowOutsideClick: false,
                            confirmButtonText: 'Yes',

                        }).then((result) => {
                            if (!result.value) {
                                document.getElementById('referral_code').value = origin_referral;
                                document.getElementById('practitioner_name').textContent = origin_referral_name;
                                // document.getElementById('referral_name').style.display = 'none'
                            }else{
                                document.getElementById('referral_name').style.display = 'block';
                                document.getElementById('practitioner_name').textContent = data.top;
                                // origin_referral = this.value;
                            }
                        })

                    } else if (data.code == '0') {
                        Swal.fire({
                            icon: 'e' +
                                'rror',
                            title: 'The code you input doesn"t match any business, please contact your business to confirm',
                        })
                        document.getElementById('referral_name').style.display = 'none'
                    }
                }

            });
        });


        document.getElementById('patient_email').addEventListener('change', function () {
            $.ajax({
                type: "POST",
                url: '{{ url('email_validation') }}',
                data: {
                    _token: '{{ csrf_token() }}',
                    role: 'patient',
                    email: this.value,
                },
                timeout: 6000,
                success: function (data) {
                    if (data.code == '0') {
                        document.getElementById('patient_email').value = "";
                        Swal.fire({
                            icon: 'info',
                            html: data.email + ' already exists. Please <a href="#" onClick="open_login()" color="#6ba53a">login</a> or <a href="/password/reset" color="#6ba53a">reset your password.</a>'
                        })
                    }
                }

            });
        });


        function stopAutoFillPatient() {
            document.getElementById('patient_street_address').removeAttribute('autocomplete');
            document.getElementById('patient_street_address').setAttribute('autocomplete', 'googleignoreautofill');
            document.getElementById('patient_locality').removeAttribute('autocomplete');
            document.getElementById('patient_locality').setAttribute('autocomplete', 'googleignoreautofill');
        }

        document.getElementById('patient_street_address').addEventListener("beforeinput", function () {
            $("#patient_locality_error").remove();
            // document.getElementById('street_address').value = '';
            document.getElementById('patient_locality').value = '';
            document.getElementById('patient_administrative_area_level_1').value = '';
            document.getElementById('patient_country').value = '';
            document.getElementById('patient_postal_code').value = '';
        })
        document.getElementById('patient_locality').addEventListener("input", function () {
            $("#patient_locality_error").remove();
            document.getElementById('patient_administrative_area_level_1').value = '';
            document.getElementById('patient_country').value = '';
            document.getElementById('patient_postal_code').value = '';
        })
        google.maps.event.addDomListener(window, 'load', function () {

            var locality = new google.maps.places.Autocomplete((document
                .getElementById('patient_locality')), {
                types: ['(regions)'],
                componentRestrictions: {
                    country: "au",
                }
            });
            patient_international.addEventListener('change', function () {
                if (this.checked) {
                    locality.setComponentRestrictions({'country': []});
                } else {
                    locality.setComponentRestrictions({'country': 'au'});
                }
            });
            addGoogleListenerLocality('patient', locality);


        });
        var patient_international = document.querySelector("input[name=patient_international]");

        google.maps.event.addDomListener(window, 'load', function () {

            var places = new google.maps.places.Autocomplete(document
                .getElementById('patient_street_address'), {
                componentRestrictions: {
                    country: "au",
                }
            });
            patient_international.addEventListener('change', function () {
                if (this.checked) {
                    places.setComponentRestrictions({'country': []});
                } else {
                    places.setComponentRestrictions({'country': 'au'});
                }
            });
            addGoogleListenerTotal('patient', places);


        });
        google.maps.event.addDomListener(window, 'load', function () {

            var locality = new google.maps.places.Autocomplete((document
                .getElementById('patient_locality')), {
                types: ['(regions)'],
                componentRestrictions: {
                    country: "au",
                }
            });
            addGoogleListenerLocality('patient', locality);


        });

        function addGoogleListenerLocality(role, locality) {
            google.maps.event.addListener(locality, 'place_changed', function () {
                var place = locality.getPlace();
                // var value = place.address_components;

                // var  value = address.split(",");
                // console.log(place.address_components)
                console.log(place.address_components);
                var componentForm = {
                    // street_number: 'short_name',
                    // route: 'long_name',
                    locality: 'long_name',
                    administrative_area_level_1: 'short_name',
                    country: 'long_name',
                    postal_code: 'short_name'
                };
                // document.getElementById('street_address').value = '';
                // document.getElementById('administrative_area_level_1').value = '';
                // document.getElementById('country').value = '';
                // document.getElementById('postal_code').value = '';
                var a = 0;
                for (var i = 0; i < place.address_components.length; i++) {
                    var addressType = place.address_components[i].types[0];
                    if (componentForm[addressType]) {
                        var val = place.address_components[i][componentForm[addressType]];
                        if (addressType == 'street_number') {
                            document.getElementById(role + '_' + 'street_address').value = val;
                        } else if (addressType == 'route') {
                            document.getElementById(role + '_' + 'street_address').value += (' ' + val);
                        } else if (addressType == 'locality') {
                            document.getElementById(role + '_' + 'locality').value = val;
                            a = 1;
                        } else {
                            document.getElementById(role + '_' + addressType).value = val;
                        }
                    }
                }
                if (a == 0) {
                    document.getElementById(role + '_' + 'locality').value = '';
                }
                if (document.getElementById(role + '_' + 'locality').value == '' ||
                    document.getElementById(role + '_' + 'administrative_area_level_1').value == '' ||
                    document.getElementById(role + '_' + 'country').value == '') {
                    $('#' + role + '_locality_row').append('<div class="alert alert-red" style="margin-top: 40px" id=' + role + '_locality_error><p>PLEASE SELECT ANOTHER SUBURB WITH A POSTCODE</p></div>')
                }
            });
        }

        function addGoogleListenerTotal(role, places) {

            google.maps.event.addListener(places, 'place_changed', function () {
                var place = places.getPlace();
                // var value = place.address_components;

                // var  value = address.split(",");
                // console.log(place.address_components)
                var componentForm = {
                    street_number: 'short_name',
                    route: 'long_name',
                    locality: 'long_name',
                    administrative_area_level_1: 'short_name',
                    country: 'long_name',
                    postal_code: 'short_name'
                };

                for (var i = 0; i < place.address_components.length; i++) {
                    var addressType = place.address_components[i].types[0];
                    if (componentForm[addressType]) {
                        var val = place.address_components[i][componentForm[addressType]];
                        if (addressType == 'street_number') {
                            document.getElementById(role + '_' + 'street_address').value = val;
                        } else if (addressType == 'route') {
                            document.getElementById(role + '_' + 'street_address').value += (' ' + val);
                        } else {
                            //console.log(addressType);
                            document.getElementById(role + '_' + addressType).value = val;
                        }
                    }
                }
                if (document.getElementById(role + '_' + 'locality').value == '' ||
                    document.getElementById(role + '_' + 'administrative_area_level_1').value == '' ||
                    document.getElementById(role + '_' + 'country').value == '') {
                    $('#' + role + '_locality_row').append('<div class="alert alert-red" style="margin-top: 40px" id=' + role + '_locality_error><p>PLEASE SELECT ANOTHER SUBURB WITH A POSTCODE</p></div>')
                }
                if (role == 'practitioner') {
                    console.log(document.getElementById(role + '_' + 'street_address').value);
                    $.ajax({
                        type: "POST",
                        url: '{{ url('address_check') }}',
                        data: {
                            _token: '{{ csrf_token() }}',
                            address: document.getElementById(role + '_' + 'street_address').value,
                        },
                        timeout: 6000,

                        success: function (data) {
                            var business = JSON.parse(data.business);
                            var business_address = JSON.parse(data.business_address);
                            if (data.code == '1') {
                                Swal.fire({
                                    icon: 'info',
                                    title: 'The address you input is already linked to the Business ' + business['customer_name'] + ' and can only be updated by the account holder, Would you like to add your Practioner information to the Bsuiness to gain access ?',
                                    showCancelButton: true,
                                    allowOutsideClick: false,
                                    confirmButtonText: 'Yes',

                                }).then((result) => {
                                    if (result.value) {
                                        document.getElementById('practitionerBusinessInfo').style.display = 'none';
                                        document.getElementById('practitioner_contact').style.display = 'none';
                                        document.getElementById('business_name').value = business['customer_name'];
                                        document.getElementById('practitioner_street_address').value = business_address['address_line_1'];
                                        document.getElementById('practitioner_second_address').value = business_address['address_line_2'];
                                        document.getElementById('practitioner_locality').value = business_address['suburb'];
                                        document.getElementById('practitioner_administrative_area_level_1').value = business_address['state'];
                                        document.getElementById('practitioner_postal_code').value = business_address['postcode'];
                                        document.getElementById('practitioner_country').value = business_address['country'];
                                        document.getElementById("business_name").readOnly = true;
                                        document.getElementById("practitioner_street_address").readOnly = true;
                                        document.getElementById("practitioner_second_address").readOnly = true;
                                        document.getElementById("practitioner_locality").readOnly = true;
                                        document.getElementById("practitioner_administrative_area_level_1").readOnly = true;
                                        document.getElementById("practitioner_postal_code").readOnly = true;
                                        document.getElementById("practitioner_country").readOnly = true;

                                    }
                                })
                            }
                        }

                    });
                }
            });

        }

        var patient_phone = document.querySelector("#patient_phone");
        var patient_mobile = document.querySelector("#patient_mobile");

        @if(Auth::guard('patient')->check())
        window.intlTelInput(patient_phone, {
            initialCountry: "au",
            separateDialCode: true,
            utilsScript: "{{ asset('build/js/utils.js') }}",
        });
        window.intlTelInput(patient_mobile, {
            initialCountry: "au",
            separateDialCode: true,
            utilsScript: "{{ asset('build/js/utils.js') }}",
        });
        @endif

    </script>


    <!-- End Document
    ================================================== -->
@endsection





