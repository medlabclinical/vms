@extends('vms.layouts.main')
@section('content')
    <main class="cd-main-content" id="main" style="margin-top:90px">

        <section class="section white-section section-padding-top-bottom" id="scroll-link-6">
            <div class="container">

                <div class="sixteen columns">
                    <div class="section-title">
                        <h2>Reset your password</h2>
                    </div>
                </div>
                <form name="ajax-form" id="ajax-form" action="{{ route('update_patient_password') }}"
                      method="post">
                    @csrf
                    <div class="sixteen columns">
                        <div class="row">
                            <label for="patient_password" class="youpasswd required"> Your current password</label>
                            <input id="patient_password" type="password" class="form-control"
                                   name="current_password" required autofocus style="text-transform: unset;">
                        </div>

                        <div class="row">
                            <label for="new-password" class="youpasswd required"> Enter new password </label>
                            <input id="new-password" type="password" class="form-control"
                                   name="new-password"
                                   value="{{$role->new_password}}" required
                                   autofocus style="text-transform: unset;">
                        </div>

                        <div class="alert alert-blue">
                            <p style="text-transform: unset;">Password needs to be 6 to 20 characters,contain at least
                                one numeric digit,
                                one uppercase and one lowercase letter.</p>
                        </div>
                        <br>

                        <div class="row">
                            <label for="password-confirm" class="youpasswd required"> Confirm your password </label>
                            <input id="password-confirm" type="password"
                                   class="form-control"
                                   name="password-confirm" required
                                   autofocus style="text-transform: unset;">
                        </div>

                        <div class="sixteen columns">
                            <div id="button-con">
                                <button class="send_message" type="submit">update</button>
                            </div>
                        </div>


                        {{--                </div>--}}
                    </div>
                </form>
            </div>
        </section>
    </main>
    @include('vms.layouts.footer')

    <!-- JAVASCRIPT
    ================================================== -->
    <script type="text/javascript" src="{{ asset('/js/medlab.js') }}"></script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            $(document).ready(function () {
                $("#ajax-form").submit(function (e) {

                    e.preventDefault(); // avoid to execute the actual submit of the form.
                        {{--                    @if(Auth::guard('patient')->check())--}}
                    var passw = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/;
                    var currentPassword = document.getElementById("patient_password").value;
                    var newPassword = document.getElementById("new-password").value;
                    var confirmPassword = document.getElementById("password-confirm").value;
                    // console.log(newPassword);
                    // console.log(confirmPassword);
                    if (newPassword !== confirmPassword) {
                        Swal.fire({
                            title: 'Error',
                            text: "Password not match,please try again",
                            icon: 'error',
                        })
                    } else if (!newPassword.match(passw)) {
                        Swal.fire({
                            title: 'Password too weak',
                            text: "Password needs 6 to 20 characters which contain at least one numeric digit, one uppercase and one lowercase letter",
                            icon: 'error',
                        })
                    } else {
                        var form = $(this);
                        var url = form.attr('action');

                        $.ajax({
                            type: "POST",
                            url: url,
                            data: form.serialize(), // serializes the form's elements.
                            success: function (data) {
                                // console.log(data);
                                if (data.msg == 'success') {
                                    Swal.fire({
                                        icon: 'success',
                                        title: 'Password updated successfully ',
                                        confirmButtonText: 'OK',
                                        allowOutsideClick: false
                                    }).then((result) => {
                                        if (result.value) {
                                            window.location.href = "/";
                                        }
                                    })
                                } else if (data.msg == 'wrong password') {
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Wrong Password',
                                        confirmButtonText: 'OK',
                                        allowOutsideClick: false
                                    })
                                } else {
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Something is wrong. Please try again',
                                        // title: data.msg,
                                    })
                                }
                            }
                        });
                    }
                    {{--                @endif--}}

                });

            });
        })(jQuery);
    </script>

@endsection
