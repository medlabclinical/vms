@extends('vms.layouts.main')
@section('title')
    Medlab - {{ $content->title }}
@endsection
@section('description')
    @if($content->meta_description)
        {{ $content->meta_description }}
    @elseif(json_decode($content->content))
        @foreach(json_decode($content->content) as $item)
            @if($item->layout=='text')
                {{ substr(str_replace('&nbsp;',' ',strip_tags($item->attributes->content)),0,160) }}
            @endif
        @endforeach
    @endif
@endsection
@section('breadcrumbs')
    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset;background-color:#7AA43F;">
            <ol class="breadcrumb medlab_breadcrumbs" style="margin-bottom: unset;background-color:#7AA43F;">
            </ol>
        </div>
    </div>

    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset">
            <ol class="breadcrumb medlab_breadcrumbs">
                <li><a class="medlab_breadcrumbs_link" href="/">@lang('trs.home')</a></li>
                <li class="active medlab_breadcrumbs_text">Marketing Preferences</li>
            </ol>
        </div>
    </div>
@endsection
@section('content')
    <main class="cd-main-content" id="main" style="margin-top:90px">

        @if(json_decode($content->slider))
            @if(json_decode($content->slider)[0]->layout=='video')
                <div class="containerHomepage">
                    <div class="textHomepage">
                        <iframe width=100% height=100%
                                src="https://player.vimeo.com/video/{{ json_decode($content->slider)[0]->attributes->video_link }}?muted=1&autoplay=1&loop=1&autopause=0"
                                allow="autoplay; fullscreen" allowfullscreen></iframe>
                    </div>
                </div>
            @else

                <section class="home">
                    <div class="slider-container">
                        <div class="tp-banner-container">
                            <div class="tp-banner">
                                <ul>
                                    @foreach(json_decode($content->slider) as $slider)


                                        @if($slider->layout=='image')
                                            <li data-transition="fade" data-slotamount="1" data-masterspeed="500"
                                                data-saveperformance="on" data-title="Intro Slide">
                                                <img
                                                    src="{{ $image_url }}/www/Images/{{ \App\Models\MediaLibrary::where('id',$slider->attributes->slider_image)->first()->name }}"
                                                    alt="{{ $slider->attributes->slider_alt }}"
                                                    data-lazyload="{{ $image_url }}/www/Images/{{ \App\Models\MediaLibrary::where('id',$slider->attributes->slider_image)->first()->name }}"
                                                    data-bgposition="center top" data-bgfit="cover"
                                                    data-bgrepeat="no-repeat">
                                                <a href="#">
                                                    <div class="black-heavy-3">
                                                        <div class="black-heavy-3-heading">
                                                            <h1>{{ $slider->attributes->title }}</h1></div>
                                                        <div
                                                            class="black-heavy-3-subheading">{{ $slider->attributes->subtitle }}</div>
                                                    </div>
                                                </a>
                                            </li>
                                        @endif
                                    @endforeach
                                </ul>

                            </div>
                        </div>
                    </div>
                </section>
            @endif

        @else
            <br>
        @endif
        @include('vms.layouts.content_section')
        <section class="section white-section section-padding-top-bottom" id="scroll-link-6">

            <div class="container">

                {{--                <div class="sixteen columns">--}}
                {{--                    <div class="section-title">--}}
                {{--                        <h1>Marketing Preferences</h1>--}}
                {{--                        --}}{{--                        <div class="subtitle big">Medlab</div>--}}
                {{--                    </div>--}}
                {{--                </div>--}}


                {{--                <div class="sixteen columns" data-scroll-reveal="enter right move 200px over 1s after 0.3s">--}}
                {{--                </div>--}}
                {{--                @if(Auth::guard('practitioner')->check())--}}
                <form id="marketing-table" name="marketing-table" action="{{ route('update_marketing') }}"
                      method="post">
                    @csrf
                    <div class="sixteen columns">
                        <div class="row">
                            <p>Would you like to receive notifications when there is a reply to your comments?</p>
                            <input id="notification_yes" type="radio" class="form-control"
                                   name="notification"
                                   value="1" required style="text-transform: unset;" @if($notification) checked @endif>
                            <label for="notification_yes">Yes</label><br>
                            <input id="notification_no" type="radio" class="form-control"
                                   name="notification"
                                   value="0" required style="text-transform: unset;" @if(!$notification) checked @endif>
                            <label for="notification_no">No</label>
                        </div>

                        <div class="row">
                            <p>Would you like to receive notifications when we have deals and promotions?</p>
                            <input id="deals_yes" type="radio" class="form-control"
                                   name="deals_preference"
                                   value="1" required style="text-transform: unset;" @if($deals_preference) checked @endif>
                            <label for="deals_yes">Yes</label><br>
                            <input id="deals_no" type="radio" class="form-control"
                                   name="deals_preference"
                                   value="0" required style="text-transform: unset;" @if(!$deals_preference) checked @endif>
                            <label for="deals_no">No</label>
                        </div>
                        <div class="row">
                            <p>Would you like to subscribe to our nutraceuticals newsletter?</p>
                            <input id="vms_yes" type="radio" class="form-control"
                                   name="vms_preference"
                                   value="1" required style="text-transform: unset;" @if($vms_preference) checked @endif>
                            <label for="vms_yes">Yes</label><br>
                            <input id="vms_no" type="radio" class="form-control"
                                   name="vms_preference"
                                   value="0" required style="text-transform: unset;" @if(!$vms_preference) checked @endif>
                            <label for="vms_no">No</label>
                        </div>
                        <div class="row">
                            <p>Would you like to subscribe to our pharmaceuticals newsletter?</p>
                            <input id="pharma_yes" type="radio" class="form-control"
                                   name="pharma_preference"
                                   value="1" required style="text-transform: unset;" @if($pharma_preference) checked @endif>
                            <label for="pharma_yes">Yes</label><br>
                            <input id="pharma_no" type="radio" class="form-control"
                                   name="pharma_preference"
                                   value="0" required style="text-transform: unset;" @if(!$pharma_preference) checked @endif>
                            <label for="pharma_no">No</label>
                        </div>

                        <div class="sixteen columns">
                            <div id="button-con">
                                <button class="send_message" type="submit">update</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </main>
    {{--    @endif--}}
    @include('vms.layouts.footer')

    <!-- JAVASCRIPT
    ================================================== -->
    <script type="text/javascript" src="{{ asset('/js/medlab.js') }}"></script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            $(document).ready(function () {
                $("#marketing-table").submit(function (e) {

                    e.preventDefault(); // avoid to execute the actual submit of the form.
                    var form = $(this);
                    var url = form.attr('action');

                    $.ajax({
                        type: "POST",
                        url: url,
                        data: form.serialize(), // serializes the form's elements.
                        success: function (data) {
                            if(data.status=='success'){
                                Swal.fire({
                                    icon: 'success',
                                    title: data.msg,
                                }).then((result) => {
                                    if (result.isConfirmed) {
                                        location.reload();
                                    }
                                })
                            }else{
                                Swal.fire({
                                    icon: 'error',
                                    title: data.msg,
                                })
                            }

                        }
                    });
                });
            });
        })(jQuery);
    </script>

@endsection
