﻿@extends('vms.layouts.main')
@section('title')
    Medlab - Order History
@endsection

@section('breadcrumbs')
    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset;background-color:#7AA43F;">
            <ol class="breadcrumb medlab_breadcrumbs" style="margin-bottom: unset;background-color:#7AA43F;">
            </ol>
        </div>
    </div>

    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset">
            <ol class="breadcrumb medlab_breadcrumbs">
                <li><a class="medlab_breadcrumbs_link" href="/">@lang('trs.home')</a></li>
                <li class="active medlab_breadcrumbs_text">Orders List</li>
            </ol>
        </div>
    </div>
@endsection




@section('content')


    <main class="cd-main-content" id="main" style="margin-top:130px">
        <section class="section white-section section-padding-top-bottom">
            <div class="container">
                <h1>My Orders</h1>
            </div>
        </section>
        <section class="section grey-section">
            <div class="container" style="padding-top: 20px;padding-bottom: 20px">
                <form method="get" name="ajax-form" id="ajax-form"
                      action="{{ route('order-list') }}">
                    {{--                @csrf--}}
                    <div class="eight columns" style="width: 100%;">
                        <br>
                        <h4 style="text-align: left;">Search for an order</h4>
                        <p style="font-size: 1.2em;font-style: italic;line-height: 12px;">Search for a Product name,
                            Order number or Purchase order number</p>
                        <br>
                        <div class="row">
                            <input id="keyword" name="keyword"
                                   class="business-list"
                                   style="text-transform: unset;width: 73%;float:left;border: 1px solid #313131;line-height:32px;padding-bottom: unset;margin-bottom:10px;"
                                   value="{{ isset($keyword)?$keyword:null }}"/>
                        </div>
                    </div>
                </form>
            </div>
        </section>
        @foreach($order as $key => $item)
            <section class="section @if($key % 2 == 0) white-section @else grey-section @endif">
                <div class="container" style="padding-top: 20px;padding-bottom: 20px">
                    <div id="order-list-container">
                        <table style="width: 100%">
                            <thead>
                            <tr>
                                <th style="width: 20%"></th>
                                <th style="width: 20%"></th>
                                <th style="width: 15%"></th>
                                <th style="width: 10%"></th>
                                <th style="width: 35%"></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class='no-border u-align-middle'>
                                    ORDER: {{ $item->order_number }}@if($item->order_status=='Backordered')
                                        (Backorder)@endif</td>
                                <td class='no-border u-align-middle'>DATE: {{ $item->completed_date }}</td>
                                <td class='no-border u-align-middle'>PO: {{ $item->customer_ref }}</td>

                                <td class='no-border u-align-middle'>TOTAL: {{ $item->total }}</td>
                                <td class='no-border u-align-middle'>
                                    @if($item->has_valid)
                                        <div style="margin-right: 1%;margin-bottom: 3%;"
                                             class="button-shortcodes text-size-1 text-padding-1 version-1 action-button"
                                             onClick="reorder('{{ $item->order_number }}')"><span>&#xf07a;</span>Reorder
                                        </div>
                                    @endif
                                    <a href="{{ route('print_invoice',['order_number' => $item->order_number ]) }}">
                                        <div style="margin-right: 1%;margin-bottom: 3%;"
                                             class="button-shortcodes text-size-1 text-padding-1 version-1 action-button">
                                            <span>&#xf02f;</span>Invoice
                                        </div>
                                    </a>
                                    <div style="margin-right: 1%;margin-bottom: 3%;"
                                         id="{{ $item->order_number }}_button"
                                         class="button-shortcodes text-size-1 text-padding-1 version-1 action-button-full"
                                         onClick="showDetail('{{ $item->order_number }}')"><span>&#xf063;</span>Details
                                    </div>
                                </td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                    <div id="{{ $item->order_number }}_preview" style="display: block">
                        @foreach($item->orderlines()->get() as $product)
                            @if($product->product()->first())
                                @if($product->product()->first()->product_group == 'Finished goods')
                                    @if($product->product()->first()->sellable_in_unleashed && !$product->product()->first()->obsolete && $product->product()->first()->show_on_website)
                                        <img
                                            src="{{ Config::get('app.image_url') }}/{{ $product->product()->first()->img_front_view_small }}"
                                            width="100"/>
                                    @else
                                        <img
                                            src="{{ Config::get('app.image_url') }}/www/Images/Discontinued-product.png"
                                            width="100"/>
                                    @endif
                                @endif
                            @endif
                        @endforeach
                    </div>
                    <div id="{{ $item->order_number }}_detail" style="display: none">

                        <table id="{{ $item->order_number }}_table" class="display compact"
                               style="width:100%">
                            {{--                        <table style="width: 100%">--}}
                            <thead>
                            <tr style="background-color: lightgrey;">
                                <th style="width: 20%">Image</th>
                                <th style="width: 20%">Description</th>
                                <th style="width: 20%">Price</th>
                                <th style="width: 20%">Quantity</th>
                                <th style="width: 20%">Total</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($item->orderlines()->get()) > 0)
                                @foreach($item->orderlines()->get() as $id => $details)
                                    <tr>
                                        <td style="vertical-align: middle">
                                            @if($details->product()->first())
                                                @if($details->product()->first()->product_group == 'Finished goods')
                                                    @if($details->product()->first()->sellable_in_unleashed && !$details->product()->first()->obsolete && $details->product()->first()->show_on_website)
                                                        <img
                                                            src="{{ Config::get('app.image_url') }}/{{ $details->product()->first()->img_front_view_small }}"
                                                            width="100"/>
                                                    @else
                                                        <img
                                                            src="{{ Config::get('app.image_url') }}/www/Images/Discontinued-product.png"
                                                            width="100"/>
                                                    @endif
                                                @endif
                                            @endif
                                        </td>
                                        <td style="vertical-align: middle">
                                            <p>{{ $details['product_description'] }}</p>
                                            {{--                                            <p>Expiry: {{ $details['product_expiry'] }}</p>--}}
                                            {{--                                            <p>Batch #: {{ $details['product_batchnumber'] }}</p>--}}
                                            {{--                                            @if ($details["product_ship_cold"]==1) <p class='cold'>** Product ships--}}
                                            {{--                                                cold</p>@endif--}}
                                        </td>
                                        <td style="vertical-align: middle">
                                            ${{ number_format($details['unit_price'], 2, '.', ',') }}
                                        </td>
                                        <td style="vertical-align: middle">
                                            {{ $details->quantity }}
                                        </td>
                                        <td style="vertical-align: middle">
                                            ${{ number_format($details['line_total']) }}
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>

                        <table style="width: 50%;float:right">
                            <thead>
                            <tr>
                                <th style="width: 50%"></th>
                                <th style="width: 50%"></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class='no-border u-align-right' colspan='4'>Product Total exGST <br>
                                    <p style="font-size: 0.8em;font-style: italic;line-height: 12px;">Free Shipping
                                        on
                                        purchases over $165.00 exGST</p></td>
                                <td class='no-border u-align-right'>
                                    ${{ number_format($item->sub_total, 2, '.', ',') }} </td>
                            </tr>
                            {{--                        <tr>--}}
                            {{--                            <td class='no-border u-align-right' colspan='4'>Shipping exGST</td>--}}
                            {{--                            <td class='no-border u-align-right'>${{ number_format($item->, 2, '.', ',') }} </td>--}}
                            {{--                        </tr>--}}
                            {{--                        @if ($total_cold_items>0)--}}
                            {{--                            <tr>--}}
                            {{--                                <td class='no-border u-align-right' colspan='4'>ESKY exGST <p class="small">Free ESKY with 6+ cold shipped products. ({{ $total_cold_items }} items)</p></td>--}}
                            {{--                                <td class='no-border u-align-right'>${{ number_format($total_esky_exgst, 2, '.', ',') }} </td>--}}
                            {{--                            </tr>--}}
                            {{--                        @endif--}}
                            <tr>
                                <td class='no-border u-align-right' colspan='4'>Total GST</td>
                                <td class='u-align-right'>${{ number_format($item->tax_total, 2, '.', ',') }} </td>
                            </tr>
                            <tr>
                                <td class='no-border u-align-right' colspan='4'>Total Payable (inc GST)</td>
                                <td class='u-align-right'>${{ number_format($item->total, 2, '.', ',') }} </td>
                            </tr>
                            </tbody>

                            {{--                    </div>--}}
                        </table>
                    </div>
                </div>
            </section>
        @endforeach
        <section class="section white-section section-padding-top-bottom">
            <div class="container">
                <div class="sixteen columns">
                    <div class="blog-left-right-links pagination">
                        @if($order->currentPage()-1!=0)
                            @if(isset($keyword))
                                <a href="{{ route('order-list') }}?keyword={{ $keyword }}&page={{ $order->currentPage()-1 }}">
                                    <div class="blog-left-link"><p>PREVIOUS 10</p></div>
                                </a>
                            @else
                                <a href="{{ route('order-list') }}?page={{ $order->currentPage()-1 }}">
                                    <div class="blog-left-link"><p>PREVIOUS 10</p></div>
                                </a>
                            @endif
                        @endif
                        @if($order->currentPage()!=$order->lastPage())
                            @if(isset($keyword))
                                <a href="{{ route('order-list') }}?keyword={{ $keyword }}&page={{ $order->currentPage()+1 }}">
                                    <div class="blog-right-link"><p>NEXT 10</p></div>
                                </a>
                            @else
                                <a href="{{ route('order-list') }}?page={{ $order->currentPage()+1 }}">
                                    <div class="blog-right-link"><p>NEXT 10</p></div>
                                </a>
                            @endif
                        @endif
                        <div style="padding: 6px">
                            <select onchange="changePage()" id="page" style="float: right;margin-right: 5%;">
                                @for($i=1;$i<$order->lastPage()+1;$i++)
                                    <option
                                        value={{ $i }} @if($order->currentPage() == $i) selected @endif >{{ $i }}</option>
                                @endfor
                            </select>
                            <p style="float: right">Page No.</p>
                        </div>
                    </div>
                </div>
            </div>

        </section>

        @include('vms.layouts.footer')


    </main>

    <div class="scroll-to-top">&#xf106;</div>





    <!-- JAVASCRIPT
    ================================================== -->

    <script type="text/javascript" src="{{ asset('/js/medlab.js') }}"></script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            $(document).ready(function () {
                {{--                @foreach($clinical_trial as $item)--}}
                @foreach($order as $item)

                $('#{{$item->order_number}}_table').DataTable({
                    "lengthMenu": [150],
                    "bFilter": false,
                    "bInfo": false,
                    "bPaginate": false,
                    "ordering": false,
                });
                @endforeach
            });
        })(jQuery);

        function showDetail(order_number) {
            var preview = document.getElementById(order_number + '_preview');
            var detail = document.getElementById(order_number + '_detail');
            if (preview.style.display == 'block') {
                preview.style.display = 'none';
                detail.style.display = 'block';
                document.getElementById(order_number + '_button').innerHTML = '<span>&#xf062;</span>' + 'Detail';
            } else {
                preview.style.display = 'block';
                detail.style.display = 'none';
                document.getElementById(order_number + '_button').innerHTML = '<span>&#xf063;</span>' + 'Detail';
            }

        }

        function reorder(order_number) {
            Swal.fire({
                title: 'Products will be added to your existing cart, and pricing will be updated to reflect current pricing',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#dd3333',
                confirmButtonText: 'Yes'
            }).then((result) => {
                if (result.value) {
                    Swal.fire({
                        title: 'Processing ..',
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        showConfirmButton: false,
                        showCloseButton: false,
                        showCancelButton: false,
                        onOpen: () => {
                            Swal.showLoading();
                            $.ajax({
                                type: "POST",
                                url: '{{ url('reorder_cart') }}',
                                data: {
                                    _token: '{{ csrf_token() }}',
                                    order_number: order_number,
                                },
                                timeout: 6000,
                                success: function (response) {
                                    console.log(response)
                                    $("#current_amount").html(response.amount);
                                    if (response.status == 'success') {

                                        Swal.fire({
                                            icon: 'success',
                                            title: response.msg,
                                            text: ' Go to cart to checkout now?',
                                            showCancelButton: true,
                                            cancelButtonText: `Continue Shopping`,
                                            confirmButtonText: `Checkout`,
                                        }).then((result) => {

                                            if (result.isConfirmed) {
                                                {{--$.redirect('{{ url('cart') }}',--}}
                                                    {{--{--}}
                                                    {{--    discontinued: response.discontinued,--}}
                                                    {{--    out_of_stock: response.out_of_stock,--}}
                                                    {{--});--}}
                                                    window.location.href = "/cart?discontinued=" + response.discontinued + '&out_of_stock=' + response.out_of_stock;
                                                {{--$.ajax({--}}
                                                {{--    type: "GET",--}}
                                                {{--    url: '{{ url('cart') }}',--}}
                                                {{--    data: {--}}
                                                {{--        _token: '{{ csrf_token() }}',--}}
                                                {{--        discontinued: response.discontinued,--}}
                                                {{--        out_of_stock: response.out_of_stock,--}}
                                                {{--    },--}}
                                                {{--})--}}
                                            }
                                        });
                                    } else {
                                        Swal.fire({
                                            icon: 'error',
                                            title: response.msg,
                                        })
                                    }

                                }
                            })
                        }
                    })
                }
            })

        }

        function changePage() {
            var page = document.getElementById("page").value;

            @if(isset($keyword))
                location.href = "{{ route('order-list') }}?keyword={{ $keyword }}&page=" + page;
            @else
                location.href = "{{ route('order-list') }}?page=" + page;
            @endif

        }
    </script>
    <link rel="stylesheet" type="text/css" href="{{ asset('/DataTables/datatables.min.css') }}"/>
    <script type="text/javascript" src="{{ asset('/DataTables/datatables.min.js') }}"></script>

    <!-- End Document
================================================== -->
@endsection
