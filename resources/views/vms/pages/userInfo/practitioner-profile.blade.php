@extends('vms.layouts.main')
@section('title')
    Practitioner Profile
@endsection
@section('description')
    Practitioner Profile Page
@endsection
@section('breadcrumbs')
    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset;background-color:#7AA43F;">
            <ol class="breadcrumb medlab_breadcrumbs" style="margin-bottom: unset;background-color:#7AA43F;">
            </ol>
        </div>
    </div>

    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset">
            <ol class="breadcrumb medlab_breadcrumbs">
                <li><a class="medlab_breadcrumbs_link" href="/">@lang('trs.home')</a></li>
                <li class="active medlab_breadcrumbs_text">Practitioner Profile</li>
            </ol>
        </div>
    </div>
@endsection
@section('content')
    <style>
        select {
            background-color: transparent;
        }
    </style>

    <main class="cd-main-content" id="main" style="margin-top:90px">

        <section class="section grey-section section-padding-top">
            <div class="container">
                <div class="sixteen columns" data-scroll-reveal="enter left move 200px over 1s after 0.3s">

                </div>
                <form name="ajax-form" id="ajax-form" action="{{ route('update_practitioner_detail') }}"
                      method="post" novalidate enctype="multipart/form-data">
                    @csrf
                    <div class="sixteen columns"
                         data-scroll-reveal="enter right move 200px over 1s after 0.3s">
                        <div class="services-boxes-1" style="cursor: pointer; width:100%">
                            <div class="icon-box">&#xf007;</div>
                            <h6>PRACTITIONER</h6>
                            <br>
                        </div>
                    </div>

                    <div id="business_form" style="display: none;">
                        <h4 style="text-align: left;">My Business Information</h4>
                        <div class="row">

                            <label for="practitioner_international" class="uname"
                                   style="display:inline-block;">
                                Show International</label>
                            <input id="practitioner_international" type="checkbox"
                                   class="form-control"
                                   name="practitioner_international"
                                   style="display:inline-block;width: unset"/>
                        </div>

                        <div class="row">
                            {{--                            <input type="text" name="id" value={{ Auth::guard('practitioner')->user()->customer_code }} hidden/>--}}
                            <label for="practitioner_street_address" class="uname required"
                                   style="width: 100%;">
                                Street Address</label>
                            <input id="practitioner_street_address" type="text"
                                   class="form-control"
                                   name="address_line_1"
                                   value="{{$business->address()->first()->address_line_1 }}"
                                   onFocus="stopAutoFillPractitioner()" required/>
                            <small class="alert alert-red"></small>

                        </div>
                        <div class="row">
                            <label for="practitioner_second_address" class="uname" style="width: 100%;">
                                A second line if you need (optional)</label>
                            <input id="practitioner_second_address" type="text"
                                   class="form-control"
                                   name="address_line_2"
                                   value="{{ $business->address()->first()->address_line_2}}"
                                   autocomplete="googleignoreautofill"/>
                        </div>
                        <div class="row" style="margin-bottom: unset;">
                            <label for="practitioner_locality" class="uname required"
                                   style="width: 33.3%;float: left">Suburb/Postcode</label>
                            <label for="practitioner_administrative_area_level_1" class="uname"
                                   style="width: 33.3%;float: left">
                                &nbsp;</label>
                            <label for="practitioner_postal_code" class="uname"
                                   style="width: 33.3%;float: left">
                                &nbsp;</label>
                        </div>

                        <div class="row" style="position:relative" id="locality_row">

                            <input id="practitioner_locality" type="text"
                                   class="form-control"
                                   name="suburb" value="{{$business->address()->first()->suburb}}"
                                   style="width:100%;float: left;position: absolute"/>

                            <input id="practitioner_administrative_area_level_1" type="text"
                                   class="form-control"
                                   name="state"
                                   value="{{$business->address()->first()->state}}"
                                   autocomplete="googleignoreautofill"
                                   style="width: 33.3%;float: left;margin-left: 33.3%" readonly>

                            <input id="practitioner_postal_code" type="number"
                                   class="form-control"
                                   name="postcode" style="width: 33.3%;float: left"
                                   value="{{$business->address()->first()->postcode}}"
                                   autocomplete="googleignoreautofill" readonly>

                        </div>
                        <div class="row">

                            <label for="practitioner_country" class="uname" style="width: 100%;">
                                Country</label>
                            <input id="practitioner_country" type="text"
                                   class="form-control"
                                   name="country"
                                   value="{{$business->address()->first()->country}}"
                                   autocomplete="googleignoreautofill" readonly/>
                        </div>
                        <br>
                        <div class="alert alert-blue">
                            <p><span style="font-size: 15px;">To change business name or ABN please contact</span><a
                                    href="mailto:accounts@medlab.co">accounts@medlab.co</a></p>
                            <br>
                            <div class="row">
                                <label for="customer_name" class="uname" style="width: 100%;">
                                    Legal Entity Name</label>
                                <input id="customer_name" type="text"
                                       class="form-control"
                                       name="customer_name" value="{{$business->customer_name}}"
                                       autocomplete="googleignoreautofill" style="border-bottom:unset;" readonly/>
                            </div>
                            {{--                            <div class="row">--}}
                            {{--                                <label for="business_type" class="uname" id="business_type_label"--}}
                            {{--                                       style="width: 100%"> Business--}}
                            {{--                                    Type</label>--}}
                            {{--                                <input id="business_type" type="text"--}}
                            {{--                                        class="form-control"--}}
                            {{--                                        name="business_type"--}}
                            {{--                                        style="text-transform: unset;width: 100%;border-bottom:unset;" value="{{$business->customer_type}}" readonly/>--}}

                            {{--                            </div>--}}

                            <div class="row">
                                <label for="business_number" class="uname"
                                       style="width: 50%;float:left;margin-right: 1%"> ABN
                                </label>

                                <label for="acn" class="uname" style="width: 49%;float:right;">ACN</label>
                                <input id="business_number" type="number"
                                       class="form-control"
                                       name="abn_number"
                                       value="{{$business->gstvat_number}}"
                                       style="text-transform: unset;width: 50%;float:left;border-bottom:unset;"
                                       readonly/>
                                <input id="acn" type="text"
                                       class="form-control"
                                       name="acn"
                                       value="{{ $business->acn}}"
                                       style="text-transform: unset;width: 49%;float:right;border-bottom:unset;"
                                       readonly/>

                            </div>
                        </div>
                        <div class="row">
                            <input type="text" name="id" value={{ Auth::guard('practitioner')->user()->id }} hidden/>
                            <label for="business_name" class="required" style="width: 100%">
                                Business Name</label>
                            <input id="business_name" type="text"
                                   class="form-control"
                                   name="business_name"
                                   value="{{$business->trading_name}}"
                                   style="text-transform: unset;width: 100%"
                                   onFocus="stopAutoFillPractitioner()" required/>
                            <small class="alert alert-red"></small>

                        </div>


                        <div id="practitionerBusinessInfo">
                            <div class="row">
                                <label for="primary_channel" class="required">Business
                                    Primary Channel</label>

                                <select id="primary_channel" type="text"
                                        class="form-control"
                                        name="primary_channel"
                                        style="text-transform: unset;width: 100%" required>
                                    @foreach($primary_channel as $item)
                                        <option
                                            @if($business->channel_id==$item->id) selected @endif> {{ $item->name }}</option>
                                    @endforeach
                                </select>
                                <small class="alert alert-red"></small>

                            </div>
                            <div>
                                <label for="second_channel" class="required"> Business Second Channel</label>
                                <select id="second_channel" type="text"
                                        class="form-control"
                                        name="second_channel"
                                        style="text-transform: unset;width: 100%" required>
                                    @foreach($second_channel as $item)
                                        <option
                                            @if($business->secondary_channel_id==$item->id) selected @endif>{{ $item->name }}</option>
                                    @endforeach
                                </select>
                                <small class="alert alert-red"></small>
                            </div>

                            <br>
                            <br>


                            <br>
                            <br>
                            <h4>My Personal Details</h4>
                            <br>

                            <input type="text" name="id" value={{ Auth::guard('practitioner')->user()->id }} hidden/>
                            <div class="row">
                                <label for="practitioner_title" class="uname required">
                                    Title</label>
                                <select id="practitioner_title" type="text"
                                        class="form-control"
                                        name="salutation" required
                                        style="text-transform: unset;width: 49%;">
                                    <option></option>
                                    <option @if($role->salutation=='Mr') selected @endif>Mr</option>
                                    <option @if($role->salutation=='Ms') selected @endif>Ms</option>
                                    <option @if($role->salutation=='Miss') selected @endif>Miss</option>
                                    <option @if($role->salutation=='Dr.') selected @endif>Dr.</option>
                                    <option @if($role->salutation=='Prof.') selected @endif>Prof.</option>
                                </select>
                                <small class="alert alert-red"></small>
                            </div>
                            <div class="row">
                                <label for="practitioner_firstname" class="uname required"> First
                                    Name</label>
                                <input id="practitioner_firstname" type="text"
                                       class="form-control"
                                       name="first_name" required
                                       value="{{$role->first_name}}"
                                       style="text-transform: unset;" onFocus="stopAutoFillPractitioner()">
                                <small class="alert alert-red"></small>
                            </div>
                            <div class="row">
                                <label for="practitioner_lastname" class="uname required"> Last Name</label>
                                <input id="practitioner_lastname" type="text"
                                       class="form-control"
                                       name="last_name" required
                                       value="{{$role->last_name}}"
                                       style="text-transform: unset;">
                                <small class="alert alert-red"></small>
                            </div>
                            <div class="row">
                                <label for="practitioner_email" class="uname required"> Your email</label>
                                <input id="practitioner_email" type="email"
                                       class="form-control"
                                       name="email" required
                                       value="{{$role->email}}"
                                       autocomplete="email" autofocus style="text-transform: unset;">
                                <small class="alert alert-red"></small>
                            </div>

                            <div class="grey-section padding">
                                <h4 style="text-align: left" class="required">Your Direct Number (Enter at least one
                                    number)</h4>
                                <div class="row">
                                    <small class="alert alert-red"></small>
                                    <label for="practitioner_phone" style="width: 100%;">
                                        Landline</label>
                                    <input id="practitioner_phone" type="tel" data-status="true"
                                           class="form-control"
                                           name="phone"
                                           style="text-transform: unset;"
                                           value="{{$role->phone }}">

                                </div>
                                <div class="row">
                                    <label for="practitioner_mobile" style="width: 100%;"> Mobile
                                        Phone</label>
                                    <input id="practitioner_mobile" type="tel" data-status="true"
                                           class="form-control"
                                           name="mobile"
                                           style="text-transform: unset;"
                                           value="{{$role->mobile }}">
                                    <small class="alert alert-red"></small>
                                </div>
                            </div>


                            {{-- End Working create checkbox if user wants to receive email  --}}


                            <br>
                            <h4>My Professional Details</h4>
                            <br>

                            <div class="row">
                                <label for="primary_profession" class="uname required"> Primary
                                    Profession</label>

                                <select id="primary_profession" type="text"
                                        class="form-control"
                                        name="primary_profession"
                                        style="text-transform: unset;background-color:transparent;width: 100%" required>
                                    @foreach($modality as $item)
                                        <option
                                            @if($role->modalityperson()->exists()) @if ($role->modalityperson()->first()->name==$item->name) selected @endif @endif> {{$item->name}}</option>
                                    @endforeach
                                </select>
                                <small class="alert alert-red"></small>

                            </div>
                            <div class="row">
                                <label for="job_title" class="uname required"> Job Title</label>
                                <input id="job_title" type="text"
                                       class="form-control"
                                       name="job_title"
                                       value="{{$role->job_title}}" required
                                       style="text-transform: unset;">
                                <small class="alert alert-red"></small>
                            </div>
                            <div class="row" id="if_have_number">
                                <p>Do you have an AHPRA Number?</p>
                                <input type="radio" id="have_ahpra_yes" name="have_ahpra"
                                       onclick="haveAphraYes()"
                                       style="width:unset;-webkit-appearance: checkbox; margin-left: 10px; margin-top: 10px; font-size: x-large;"
                                       value="1">
                                <label for="have_ahpra_yes" style="display: inline-block"> &nbspYes</label>
                                <br>
                                <input type="radio" id="have_ahpra_no" name="have_ahpra"
                                       onclick="haveAphraNo()"
                                       style="width:unset;-webkit-appearance: checkbox;  margin-left: 10px; margin-top: 10px; margin-bottom: 10px;font-size: x-large;"
                                       value="0">
                                <label for="have_ahpra_no" style="display: inline-block"> &nbspNo</label>
                            </div>
                            {{-- /surya - ahpra number --}}
                            <div class="row" id="ahpra_number_area" style="display: none;">
                                <label for="ahpra_number" class="uname required"
                                       style="width: 100%;float:left;"> AHPRA Number</label>

                                <input id="ahpra_number" type="text"
                                       class="form-control"
                                       name="ahpra_number"
                                       value="{{$role->ahpra_number}}" required
                                       style="text-transform: unset;width: 100%;">
                                <small class="alert alert-red"></small>

                            </div>

                            <div id="association_area" style="display: none;">
                                <div class="row">
                                    <label for="association_name" class="uname required"> Association
                                        Name</label>

                                    <select id="association_name" type="text"
                                            class="form-control"
                                            name="association_name"
                                            style="text-transform: unset;background-color:transparent;width: 100%"
                                            required>
                                        @foreach($associations as $item)
                                            <option @if ($role->association_id == $item->id) selected
                                                    @endif value="{{ $item->id }}">{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                    <small class="alert alert-red"></small>

                                </div>
                                <div class="row">
                                    <label for="association_number" class="uname required"> Association Number</label>
                                    <input id="association_number" type="number"
                                           class="form-control"
                                           name="association_number"
                                           value="{{$role->association_number}}" required
                                           style="text-transform: unset;">
                                    <small class="alert alert-red"></small>
                                </div>
                                <div class="row">
                                    <label for="qualification" class="required"
                                           style="width: 100%">Please assist us in the approval process by uploading
                                        a scanned copy of Certification/Association relevant to your modality<br>(Only
                                        accept .pdf .jpg .png .jpeg file (20 MB Max))</label>

                                    @if($qualification)
                                        {{--                                        <br>--}}
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" color="red"
                                             viewBox="0 0 24 24" aria-labelledby="download" role="presentation"
                                             class="fill-current mr-2">
                                            <path
                                                d="M11 14.59V3a1 1 0 0 1 2 0v11.59l3.3-3.3a1 1 0 0 1 1.4 1.42l-5 5a1 1 0 0 1-1.4 0l-5-5a1 1 0 0 1 1.4-1.42l3.3 3.3zM3 17a1 1 0 0 1 2 0v3h14v-3a1 1 0 0 1 2 0v3a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2v-3z"></path>
                                        </svg>
                                        <a href="{{ $qualification }}" target="_blank" download
                                           style="vertical-align: bottom">Download</a>
                                        <br></br>
                                    @endif

                                    <input name="old_qualification" value="{{ $role->qualification }}" hidden/>
                                    <input id="qualification" type="file"
                                           class="form-control"
                                           name="qualification"
                                           style="text-transform: unset;">
                                    <small class="alert alert-red"></small>
                                </div>
                            </div>

                            <div class="row">
                                <label for="business_phone" class="uname required" style="width: 100%">
                                    Landline</label>

                                <input id="business_phone" type="tel" data-status="true"
                                       class="form-control"
                                       name="business_phone"
                                       value="{{$business->phone_number }}"
                                       style="text-transform: unset;" required>
                                <small class="alert alert-red"></small>
                            </div>

                            <div class="row">

                                <label for="fax" class="uname" style="width: 100%"> Fax</label>

                                <input id="fax" type="tel" data-status="true"
                                       class="form-control"
                                       name="fax_number"
                                       value="{{$business->fax_number}}"
                                       style="text-transform: unset;">
                                <small class="alert alert-red"></small>
                            </div>


                            <div class="sixteen columns">
                                <div id="button-con">
                                    <button type="button" onclick="window.location.href='{{ route('homepage') }}'" style="margin-right: 10%;">Cancel</button>
                                    <button class="send_message" name="practitioner_information"
                                            type="submit">update
                                    </button>

                                </div>

                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>

    </main>
    @include('vms.layouts.footer')

    <div class="scroll-to-top">&#xf106;</div>

    <!-- JAVASCRIPT
    ================================================== -->
    <script type="text/javascript" src="{{ asset('/build/js/intlTelInput.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/medlab.js') }}"></script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            $(document).ready(function () {

                @if($role->ahpra_number)
                document.getElementById('have_ahpra_yes').checked = true;
                haveAphraYes()
                @else
                document.getElementById('have_ahpra_no').checked = true;
                haveAphraNo()
                @endif

            });
        })(jQuery);

    </script>
    <script type="text/javascript">
        document.getElementById('ajax-form').addEventListener('submit', (e) => {
            const errorMessage = 'Required field';
            e.preventDefault(); // avoid to execute the actual submit of the form.
            let allAreFilled = true;
            document.getElementById("ajax-form").querySelectorAll("[required]").forEach(function (i) {
                console.log(i.id)
                if (i.id == 'business_phone') {
                    if (i.value == '') {
                        setErrorFor(i.parentElement, errorMessage);
                        allAreFilled = false
                    } else if ($('#business_phone').attr('data-status') == 'false') {
                        allAreFilled = false
                    } else {
                        setSuccessFor(i.parentElement);
                    }
                } else {
                    if (i.value == '') {
                        setErrorFor(i, errorMessage);
                        allAreFilled = false
                    } else {
                        setSuccessFor(i);
                    }
                }
            })
            console.log(allAreFilled)
            if (document.getElementById('practitioner_phone').value == '' && document.getElementById('practitioner_mobile').value == '') {
                allAreFilled = false;
                setErrorFor(document.getElementById('practitioner_phone').parentElement, errorMessage);
            } else if ($('#practitioner_phone').attr('data-status') == 'false') {
                allAreFilled = false
            } else if ($('#practitioner_mobile').attr('data-status') == 'false') {
                allAreFilled = false
            } else {
                setSuccessFor(document.getElementById('practitioner_phone').parentElement);
            }
            {{--if(document.getElementById('qualification').value == '' && '{{ $qualification }}'==0){--}}
            {{--    allAreFilled = false--}}
            {{--    setErrorFor(document.getElementById('qualification'),errorMessage);--}}
            {{--}else{--}}
            {{--    setSuccessFor(document.getElementById('qualification'));--}}
            {{--}--}}
            if ($('#fax').attr('data-status') == 'false') {
                allAreFilled = false
            } else {
                setSuccessFor(document.getElementById('fax').parentElement);
            }
            if (allAreFilled) {
                var form = $(this);
                var url = form.attr('action');
                $('#practitioner_phone').attr('type', 'text');
                $('#practitioner_mobile').attr('type', 'text');
                $('#business_phone').attr('type', 'text');
                $('#fax').attr('type', 'text');
                document.getElementById('practitioner_phone').value = int_practitioner_phone.getNumber();
                document.getElementById('practitioner_mobile').value = int_practitioner_mobile.getNumber();

                document.getElementById('business_phone').value = int_business_phone.getNumber();
                document.getElementById('fax').value = int_fax.getNumber();

                document.getElementById('fax').value = int_fax.getNumber();
                @if($business->status == 0)
                Swal.fire({
                    title: 'Your update was successful!',
                    text: 'Please give us some time to process your upgrade application, we will send you an email when your account is ready',
                    icon: 'success',
                    confirmButtonText: 'OK',
                    allowOutsideClick: false
                }).then((result) => {
                    if (result.value) {
                        document.getElementById("ajax-form").submit();
                    }
                })
                @else

                Swal.fire({
                    title: 'Your update was successful!',
                    icon: 'success',
                    confirmButtonText: 'OK',
                    allowOutsideClick: false
                }).then((result) => {
                    if (result.value) {
                        document.getElementById("ajax-form").submit();
                    }
                })
                @endif
                // $.ajax({
                //     method: "post",
                //     url: url,
                //     data: form.serialize(), // serializes the form's elements.
                //     success: function (data) {
                //         console.log(data);
                //         if (data.msg == 'success') {
                //             Swal.fire({
                //                 icon: 'success',
                //                 title: 'Your update was successful!',
                //                 confirmButtonText: 'OK',
                //                 allowOutsideClick: false
                //             }).then((result) => {
                //                 if (result.value) {
                //                     // document.getElementById("ajax-form").submit();
                //                     window.location.reload();
                //                 }
                //             })
                //         } else {
                //             Swal.fire({
                //                 icon: 'error',
                //                 title: 'Something is wrong, please try again!',
                //                 // title: data.msg,
                //             })
                //         }
                //     }
                // });
            } else {
                Swal.fire({
                    icon: 'error',
                    title: 'Please fill in all required field and submit again',
                })
            }

        })
        document.getElementById('ahpra_number').addEventListener('change', function () {
            $.ajax({
                type: "POST",
                url: '{{ url('ahpra_number_validation') }}',
                data: {
                    _token: '{{ csrf_token() }}',
                    number: this.value,
                },
                timeout: 6000,
                success: function (data) {
                    if (data.code == '0') {
                        document.getElementById('ahpra_number').value = "";
                        Swal.fire({
                            icon: 'info',
                            html: data.number + ' already exists and is linked to ' + data.name + '. Please <a href="#" onClick="open_login()" color="#6ba53a">login</a> or <a href="/password/reset" color="#6ba53a">reset your password.</a> If you need assistance with your login name please <a href="/contact" color="#6ba53a">contact us.</a>'
                        })
                    } else if (data.code == '2') {
                        Swal.fire({
                            icon: 'error',
                            title: 'AHPRA number is not valid, please check and try again',
                        })
                        document.getElementById('ahpra_number').value = '';
                    }
                }

            });
        });
        document.getElementById('practitioner_phone').addEventListener('change', function () {
            var number = int_practitioner_phone.getNumber();
            if (number) {
                $.ajax({
                    type: "POST",
                    url: '{{ url('phone_existence_validate') }}',
                    data: {
                        _token: '{{ csrf_token() }}',
                        type: 'landline',
                        phone: number,
                    },
                    timeout: 6000,
                    success: function (data) {
                        if (data.code == 2) {
                            Swal.fire({
                                icon: 'error',
                                title: 'Landline number is not valid',
                            })
                            setErrorFor(document.getElementById('practitioner_phone').parentElement, 'Landline number is not valid');
                            var new_status = 'false';
                            $('#practitioner_phone').attr('data-status', new_status);
                        } else {
                            var new_status = 'true';
                            $('#practitioner_phone').attr('data-status', new_status);
                            setSuccessFor(document.getElementById('practitioner_phone').parentElement);
                        }
                    }
                })
            } else {
                var new_status = 'true';
                $('#practitioner_phone').attr('data-status', new_status);
                setSuccessFor(document.getElementById('practitioner_phone').parentElement);
            }

        });
        document.getElementById('practitioner_mobile').addEventListener('change', function () {
            var number = int_practitioner_mobile.getNumber();
            if (number) {
                $.ajax({
                    type: "POST",
                    url: '{{ url('phone_existence_validate') }}',
                    data: {
                        _token: '{{ csrf_token() }}',
                        type: 'mobile',
                        phone: number,
                    },
                    timeout: 6000,
                    success: function (data) {
                        if (data.code == 2) {
                            Swal.fire({
                                icon: 'error',
                                title: 'Mobile number is not valid',
                            })
                            setErrorFor(document.getElementById('practitioner_phone').parentElement, 'Mobile number is not valid');
                            var new_status = 'false';
                            $('#practitioner_mobile').attr('data-status', new_status);
                        } else {
                            var new_status = 'true';
                            $('#practitioner_mobile').attr('data-status', new_status);
                            setSuccessFor(document.getElementById('practitioner_mobile').parentElement);
                        }
                    }
                })
            } else {
                var new_status = 'true';
                $('#practitioner_mobile').attr('data-status', new_status);
                setSuccessFor(document.getElementById('practitioner_phone').parentElement);
            }

        });
        document.getElementById('primary_channel').addEventListener('change', function () {

            $.ajax({
                type: "POST",
                url: '{{ url('get_channel') }}',
                data: {
                    _token: '{{ csrf_token() }}',
                    parent: this.value,
                },
                timeout: 6000,
                error: function (request, error) {
                    if (error == "timeout") {
                        $('#err-timedout').slideDown('slow');
                    } else {
                        $('#err-state').slideDown('slow');
                        $("#err-state").html('An error occurred: ' + error + '');
                    }
                },
                success: function (data) {
                    $('#second_channel').empty();
                    $.each(data.data, function () {
                        $("#second_channel").append('<option value="' + this + '">' + this + '</option>')
                    })
                }

            });
        });
        document.getElementById('practitioner_email').addEventListener('change', function validateEmail() {
            if (this.value != '{{$role->email}}') {
                if (!(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(this.value))) {
                    Swal.fire({
                        icon: 'error',
                        title: 'You have entered an invalid email address',
                    })
                    document.getElementById('practitioner_email').value = "";
                } else {
                    Swal.fire({
                        title: 'Checking ..',
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        showConfirmButton: false,
                        showCloseButton: false,
                        showCancelButton: false,
                        onOpen: () => {
                            Swal.showLoading();

                            $.ajax({
                                type: "POST",
                                url: '{{ url('email_validation') }}',
                                data: {
                                    _token: '{{ csrf_token() }}',
                                    role: 'practitioner',
                                    email: this.value,
                                },
                                timeout: 6000,
                                success: function (data) {
                                    if (data.code == '0') {
                                        document.getElementById('practitioner_email').value = "";
                                        Swal.fire({
                                            icon: 'info',
                                            html: data.email + ' already exists. Please <a href="#" onClick="open_login()" color="#6ba53a">login</a> or <a href="/password/reset" color="#6ba53a">reset your password.</a>'
                                        })

                                    } else if (data.code == '2') {
                                        document.getElementById('practitioner_email').value = "";
                                        Swal.fire({
                                            icon: 'info',
                                            text: 'Email verification failed, please enter a valid email address'
                                        })
                                    } else if (data.code == '3') {
                                        document.getElementById('practitioner_email').value = "";
                                        Swal.fire({
                                            icon: 'info',
                                            text: 'Timeout, please try again later'
                                        })
                                    } else {
                                        Swal.close();

                                    }
                                }

                            });
                        }
                    })

                }
            }

        });

        document.getElementById('business_phone').addEventListener('change', function () {
            var number = int_business_phone.getNumber();
            if (number) {
                $.ajax({
                    type: "POST",
                    url: '{{ url('phone_existence_validate') }}',
                    data: {
                        _token: '{{ csrf_token() }}',
                        type: 'landline',
                        phone: number,
                    },
                    timeout: 6000,
                    success: function (data) {
                        if (data.code == 2) {
                            Swal.fire({
                                icon: 'error',
                                title: 'Landline number is not valid',
                            })
                            setErrorFor(document.getElementById('business_phone').parentElement, 'Landline number is not valid');
                            var new_status = 'false';
                            $('#business_phone').attr('data-status', new_status);
                        } else {
                            var new_status = 'true';
                            $('#business_phone').attr('data-status', new_status);
                            setSuccessFor(document.getElementById('business_phone').parentElement);
                            $.ajax({
                                type: "POST",
                                url: '{{ url('phone_update_check') }}',
                                data: {
                                    _token: '{{ csrf_token() }}',
                                    phone: number,
                                },
                                timeout: 6000,
                                success: function (data) {
                                    var business = JSON.parse(data.business);
                                    var pre_phone = JSON.parse(data.pre_phone);
                                    if (data.code == '1') {
                                        Swal.fire({
                                            icon: 'info',
                                            text: 'The phone entered has an existing business "' + business['customer_name'] + '" in our database. Continue with this update? "',
                                            showCancelButton: true,
                                            allowOutsideClick: false,
                                            // confirmButtonText: 'Link to existing',
                                            // cancelButtonText: 'New Account',
                                            reverseButtons: true

                                        }).then((result) => {
                                            if (!result.value) {

                                                document.getElementById('business_phone').value = pre_phone['phone_number'];

                                            }
                                        })
                                    }
                                }

                            });
                        }
                    }
                });
            } else {
                var new_status = 'true';
                $('#business_phone').attr('data-status', new_status);
                setSuccessFor(document.getElementById('business_phone').parentElement);
            }
        });

        document.getElementById('fax').addEventListener('change', function () {
            var number = int_fax.getNumber();
            if (number) {
                $.ajax({
                    type: "POST",
                    url: '{{ url('phone_existence_validate') }}',
                    data: {
                        _token: '{{ csrf_token() }}',
                        type: 'landline',
                        phone: number,
                    },
                    timeout: 6000,
                    success: function (data) {
                        if (data.code == 2) {
                            Swal.fire({
                                icon: 'error',
                                title: 'Fax number is not valid',
                            })
                            setErrorFor(document.getElementById('fax').parentElement, 'Fax number is not valid');
                            var new_status = 'false';
                            $('#fax').attr('data-status', new_status);
                        } else {
                            var new_status = 'true';
                            $('#fax').attr('data-status', new_status);
                            setSuccessFor(document.getElementById('fax').parentElement);
                            $.ajax({
                                type: "POST",
                                url: '{{ url('update_fax_check') }}',

                                data: {
                                    _token: '{{ csrf_token() }}',
                                    fax: number,
                                },
                                timeout: 6000,
                                success: function (data) {
                                    var business = JSON.parse(data.business);
                                    var pre_fax = JSON.parse(data.pre_fax);
                                    console.log(pre_fax);
                                    if (data.code == '1') {
                                        Swal.fire({
                                            icon: 'info',
                                            text: 'The fax entered has an existing business "' + business['customer_name'] + '" in our database. Continue with this update?"',
                                            showCancelButton: true,
                                            allowOutsideClick: false,
                                            // confirmButtonText: 'Link to existing',
                                            // cancelButtonText: 'New Account',
                                            reverseButtons: true

                                        }).then((result) => {
                                            if (!result.value) {
                                                document.getElementById('fax').value = pre_fax['fax_number'];
                                            }
                                        })
                                    }
                                }

                            });
                        }
                    }
                });
            } else {
                var new_status = 'true';
                $('#fax').attr('data-status', new_status);
                setSuccessFor(document.getElementById('fax').parentElement);
            }
        });

        function haveAphraYes() {
            document.getElementById('business_form').style.display = 'block';
            document.getElementById('ahpra_number_area').style.display = 'block';
            document.getElementById('association_area').style.display = 'none';
            $("#ahpra_number").prop('required', true);
            $("#association_name").prop('required', false);
            $("#association_number").prop('required', false);
            document.getElementById('association_name').value = "";
            document.getElementById('association_number').value = "";
            document.getElementById('qualification').value = "";
        }

        function haveAphraNo() {
            document.getElementById('business_form').style.display = 'block';
            document.getElementById('ahpra_number_area').style.display = 'none';
            document.getElementById('association_area').style.display = 'block';
            $("#ahpra_number").prop('required', false);
            $("#association_name").prop('required', true);
            $("#association_number").prop('required', true);
            document.getElementById('ahpra_number').value = "";
        }

        document.getElementById('practitioner_street_address').addEventListener("beforeinput", function () {
            $("#practitioner_locality_error").remove();
            // document.getElementById('street_address').value = '';
            document.getElementById('practitioner_locality').value = '';
            document.getElementById('practitioner_administrative_area_level_1').value = '';
            document.getElementById('practitioner_country').value = '';
            document.getElementById('practitioner_postal_code').value = '';
        })
        document.getElementById('practitioner_locality').addEventListener("input", function () {
            $("#practitioner_locality_error").remove();
            document.getElementById('practitioner_administrative_area_level_1').value = '';
            document.getElementById('practitioner_country').value = '';
            document.getElementById('practitioner_postal_code').value = '';
        })

        var practitioner_international = document.querySelector("input[name=practitioner_international]");

        google.maps.event.addDomListener(window, 'load', function () {

            var places = new google.maps.places.Autocomplete(document
                .getElementById('practitioner_street_address'), {
                componentRestrictions: {
                    country: "au",
                }
            });
            practitioner_international.addEventListener('change', function () {
                if (this.checked) {
                    places.setComponentRestrictions({'country': []});
                } else {
                    places.setComponentRestrictions({'country': 'au'});
                }
            });
            addGoogleListenerTotal('practitioner', places);


        });
        google.maps.event.addDomListener(window, 'load', function () {

            var locality = new google.maps.places.Autocomplete((document
                .getElementById('practitioner_locality')), {
                types: ['(regions)'],
                componentRestrictions: {
                    country: "au",
                }
            });
            practitioner_international.addEventListener('change', function () {
                if (this.checked) {
                    locality.setComponentRestrictions({'country': []});
                } else {
                    locality.setComponentRestrictions({'country': 'au'});
                }
            });
            addGoogleListenerLocality('practitioner', locality);


        });

        function addGoogleListenerLocality(role, locality) {
            google.maps.event.addListener(locality, 'place_changed', function () {
                var place = locality.getPlace();
                // var value = place.address_components;

                // var  value = address.split(",");
                // console.log(place.address_components)
                console.log(place.address_components);
                var componentForm = {
                    // street_number: 'short_name',
                    // route: 'long_name',
                    locality: 'long_name',
                    sublocality_level_2: 'long_name',
                    administrative_area_level_1: 'short_name',
                    country: 'long_name',
                    postal_code: 'short_name'
                };
                // document.getElementById('street_address').value = '';

                // document.getElementById('administrative_area_level_1').value = '';
                // document.getElementById('country').value = '';
                // document.getElementById('postal_code').value = '';
                var a = 0;
                for (var i = 0; i < place.address_components.length; i++) {
                    var addressType = place.address_components[i].types[0];
                    if (componentForm[addressType]) {
                        var val = place.address_components[i][componentForm[addressType]];
                        if (addressType == 'street_number') {
                            document.getElementById(role + '_' + 'street_address').value = val;
                        } else if (addressType == 'route') {
                            document.getElementById(role + '_' + 'street_address').value += (' ' + val);
                        } else if (addressType == 'locality' || addressType == "sublocality_level_2") {
                            document.getElementById(role + '_' + 'locality').value = val;
                            a = 1;
                        } else {
                            document.getElementById(role + '_' + addressType).value = val;
                        }
                    }
                }
                if (a == 0) {
                    document.getElementById(role + '_' + 'locality').value = '';
                }
                if(!document.getElementById(role + '_' + 'international').checked) {
                    if (document.getElementById(role + '_' + 'locality').value == '' ||
                        document.getElementById(role + '_' + 'administrative_area_level_1').value == '' ||
                        document.getElementById(role + '_' + 'country').value == '') {
                        $('#' + role + '_locality_row').append('<div class="alert alert-red" style="margin-top: 40px" id=' + role + '_locality_error><p>PLEASE SELECT ANOTHER SUBURB WITH A POSTCODE</p></div>')
                    }
                }
            });
        }

        function addGoogleListenerTotal(role, places) {

            google.maps.event.addListener(places, 'place_changed', function () {
                var place = places.getPlace();
                // var value = place.address_components;

                // var  value = address.split(",");
                // console.log(place.address_components)
                var componentForm = {
                    street_number: 'short_name',
                    route: 'long_name',
                    locality: 'long_name',
                    sublocality_level_2: 'long_name',
                    administrative_area_level_1: 'short_name',
                    country: 'long_name',
                    postal_code: 'short_name'
                };

                for (var i = 0; i < place.address_components.length; i++) {
                    var addressType = place.address_components[i].types[0];
                    if (componentForm[addressType]) {
                        var val = place.address_components[i][componentForm[addressType]];
                        if (addressType == 'street_number') {
                            document.getElementById(role + '_' + 'street_address').value = val;
                        } else if (addressType == 'route') {
                            document.getElementById(role + '_' + 'street_address').value += (' ' + val);
                        } else if (addressType == 'locality' || addressType == "sublocality_level_2"){
                            document.getElementById(role + '_' + 'locality').value = val;
                        } else {
                            document.getElementById(role + '_' + addressType).value = val;
                        }
                    }
                }
                if(!document.getElementById(role + '_' + 'international').checked) {
                    if (document.getElementById(role + '_' + 'locality').value == '' ||
                        document.getElementById(role + '_' + 'administrative_area_level_1').value == '' ||
                        document.getElementById(role + '_' + 'country').value == '') {
                        $('#' + role + '_locality_row').append('<div class="alert alert-red" style="margin-top: 40px" id=' + role + '_locality_error><p>PLEASE SELECT ANOTHER SUBURB WITH A POSTCODE</p></div>')
                    }
                }
                if (role == 'practitioner') {
                    console.log(document.getElementById(role + '_' + 'street_address').value);
                    $.ajax({
                        type: "POST",
                        url: '{{ url('address_update_check') }}',
                        data: {
                            _token: '{{ csrf_token() }}',
                            address: document.getElementById(role + '_' + 'street_address').value,
                        },
                        timeout: 6000,

                        success: function (data) {
                            var business = JSON.parse(data.business);
                            var pre_address = JSON.parse(data.pre_address);
                            if (data.code == '1') {
                                Swal.fire({
                                    icon: 'info',
                                    text: 'The address entered has an existing business "' + business['customer_name'] + '" in our database. Continue with this update? "',
                                    showCancelButton: true,
                                    allowOutsideClick: false,
                                    // confirmButtonText: 'Cancel',
                                    // cancelButtonText: 'Yes',
                                    reverseButtons: true

                                }).then((result) => {
                                    if (!result.value) {
                                        document.getElementById('practitioner_street_address').value = pre_address['address_line_1'];
                                        document.getElementById('practitioner_second_address').value = pre_address['address_line_2'];
                                        document.getElementById('practitioner_locality').value = pre_address['suburb'];
                                        document.getElementById('practitioner_administrative_area_level_1').value = pre_address['state'];
                                        document.getElementById('practitioner_postal_code').value = pre_address['postcode'];
                                        document.getElementById('practitioner_country').value = pre_address['country'];

                                    }
                                })
                            }
                        }

                    });
                }
            });

        }

        // function submitPractitionerForm() {
        //     document.getElementById('practitioner_phone').value = int_practitioner_phone.getNumber();
        //     document.getElementById('fax').value = int_fax.getNumber();
        //     return true;

        // }
        function setErrorFor(input, message) {
            // x refers to each row
            const x = input.parentElement;
            const small = x.querySelector('small');
            // console.log(small);
            small.innerText = message;
            small.style.display = 'block';

        }

        function setSuccessFor(input) {
            // x refers to each row
            const x = input.parentElement;
            const small = x.querySelector('small');
            small.style.display = 'none';

        }

        var practitioner_phone = document.querySelector("#practitioner_phone");
        var practitioner_mobile = document.querySelector("#practitioner_mobile");
        var business_phone = document.querySelector("#business_phone");
        var fax = document.querySelector("#fax");


        var int_practitioner_phone = window.intlTelInput(practitioner_phone, {
            // allowDropdown: true,
            // autoHideDialCode: false,
            // autoPlaceholder: "off",
            // dropdownContainer: document.body,
            // excludeCountries: ["us"],
            formatOnDisplay: true,
            // geoIpLookup: function(callback) {
            //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
            //     var countryCode = (resp && resp.country) ? resp.country : "";
            //     callback(countryCode);
            //   });
            // },
            // hiddenInput: "full_number",
            initialCountry: "au",
            // localizedCountries: { 'de': 'Deutschland' },
            // nationalMode: false,
            // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
            // placeholderNumberType: "MOBILE",
            // preferredCountries: ['cn', 'jp'],
            separateDialCode: true,
            utilsScript: "{{ asset('build/js/utils.js') }}",
        });
        var int_practitioner_mobile = window.intlTelInput(practitioner_mobile, {
            initialCountry: "au",
            separateDialCode: true,
            utilsScript: "{{ asset('build/js/utils.js') }}",
        });
        var int_business_phone = window.intlTelInput(business_phone, {
            initialCountry: "au",
            separateDialCode: true,
            utilsScript: "{{ asset('build/js/utils.js') }}",
        });
        var int_fax = window.intlTelInput(fax, {
            initialCountry: "au",
            separateDialCode: true,
            utilsScript: "{{ asset('build/js/utils.js') }}",
        });

    </script>

    <!-- End Document
    ================================================== -->
@endsection


