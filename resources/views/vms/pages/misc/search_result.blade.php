@extends('vms.layouts.main')
@section('title')
    Search Result
@endsection
@section('breadcrumbs')
    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset;background-color:#7AA43F;">
            <ol class="breadcrumb medlab_breadcrumbs" style="margin-bottom: unset;background-color:#7AA43F;">
            </ol>
        </div>
    </div>

    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset">
            <ol class="breadcrumb medlab_breadcrumbs">
                <li><a class="medlab_breadcrumbs_link" href="/">@lang('trs.home')</a></li>
                <li class="active medlab_breadcrumbs_text">@lang('trs.search result')</li>
            </ol>
        </div>
    </div>
@endsection



@section('content')

    <main class="cd-main-content" id="main" style="margin-top:130px">


        <!-- TOP SECTION
        ================================================== -->

        <section class="section parallax-section" style="padding-bottom: unset;">
            <div class="container">
                <div class="sixteen columns">
                    <div class="section-title left">
                        <h1>Search Result</h1>
                        <div class="subtitle left big" style="text-transform: unset;font-family: unset;">
                            Found {{ $amount }} results for "{{ $search }}" across all content in all brand
                        </div>
                    </div>
                </div>
            </div>

        </section>


        <!-- SECTION
  ================================================== -->

        <section class="section white-section section-padding-top-bottom">
            <div class="shortcodes-carousel">

                <div id="sync-sortcodes-1" class="owl-carousel">

                    @if(count($article)!=0)

                        <div class="item white-section" id="article">
                            <div class="blog-wrapper">
                                <div id="blog-grid-masonry">
                                    @foreach($article as $item)
                                        @if($item->article_type=='link')
                                            <a href={{ route('articles_content',['id' => preg_replace('/--+/','-',preg_replace('/[^A-Za-z0-9\-]/', '-', $item->title)).'-'.$item->id]) }}>
                                                <div class="blog-box-3">
                                                    <div class="blog-box-1 link-post">
                                                        <h4>{{ $item->title }}</h4>
                                                        <p>{{ $item->preview_text }}</p>
                                                        <p>{{ substr($item->date,0,10) }}</p>
                                                        @if(isset($item->author))<p>
                                                            Author: {{ $item->author }}</p>@endif
                                                        <div class="link">&#xf178;</div>
                                                    </div>
                                                </div>
                                            </a>
                                        @elseif($item->article_type=='video')
                                            <a href={{ route('articles_content',['id' => preg_replace('/--+/','-',preg_replace('/[^A-Za-z0-9\-]/', '-', $item->title)).'-'.$item->id]) }}>
                                                <div class="blog-box-3">
                                                    <div class="blog-box-1 grey-section">
                                                        @if(($item->req_login && !(Auth::guard('practitioner')->check()||Auth::guard('patient')->check())))
                                                            <div style="opacity: 0.85;background: black;">
                                                                <iframe
                                                                    srcdoc="<p style='text-align:center;margin-top:23%;font-size:xxx-large;color:white'>Login as Practitioner to View</p>"
                                                                    src="{{ $item->video_link }}" width="100%"
                                                                    height="270"
                                                                    allowfullscreen></iframe>
                                                            </div>
                                                        @else
                                                            <iframe src="{{ $item->video_link }}" width="100%"
                                                                    height="220"
                                                                    allowfullscreen></iframe>
                                                        @endif
                                                        <h4>{{ $item->title }}</h4>
                                                        <p>{{ $item->preview_text }}</p>
                                                        <p>{{ substr($item->date,0,10) }}</p>
                                                        @if(isset($item->author))<p>
                                                            Author: {{ $item->author }}</p>@endif
                                                        <div class="link">&#xf178;</div>
                                                    </div>
                                                </div>
                                            </a>
                                        @else
                                            <a href={{ route('articles_content',['id' => preg_replace('/--+/','-',preg_replace('/[^A-Za-z0-9\-]/', '-', $item->title)).'-'.$item->id]) }}>
                                                <div class="blog-box-3">
                                                    <div class="blog-box-1 grey-section">
                                                        <img src="{{ $image_url }}/www/Images/{{ $item->title_img }}"
                                                             alt="{{ $item->title_alt }}">
                                                        <h4>{{ $item->title }}</h4>
                                                        <p>{{ $item->preview_text }}</p>
                                                        <p>{{ substr($item->date,0,10) }}</p>
                                                        @if(isset($item->author))<p>
                                                            Author: {{ $item->author }}</p>@endif
                                                        <div class="link">&#xf178;</div>
                                                    </div>
                                                </div>
                                            </a>
                                        @endif
                                    @endforeach
                                </div>
                            </div>


                        </div>
                    @endif
                        @if(count($flyers)!=0)

                            <div class="item white-section" id="flyers">
                                <div class="blog-wrapper">
                                    <div id="blog-grid-masonry">
                                        @foreach($flyers as $item)
                                            <a href={{ route('flyer_details',['id' => preg_replace('/--+/','-',preg_replace('/[^A-Za-z0-9\-]/', '-', $item->name)).'-'.$item->id]) }}>
                                                <div class="blog-box-3">
                                                    <div class="blog-box-1 link-post grey-section">
                                                        <div
                                                            style="max-height: 270px; position:relative; padding-bottom: 56.25%; height: 0;">
                                                            @if($item->preview_img)
                                                                <img
                                                                    src="{{ $image_url }}/www/Images/{{ $item->image()->first()->name }}"
                                                                    style="margin-left: auto;
                                                 margin-right: auto; position: absolute; top: 0; left: 0; width: 100%; height: 100%;">
                                                            @elseif($item->pdf_type()->first()->image()->first())
                                                                <img
                                                                    src="{{ $image_url }}/www/Images/{{ $item->pdf_type()->first()->image()->first()->name }}"
                                                                    style="margin-left: auto;
                                                 margin-right: auto; position: absolute; top: 0; left: 0; width: 100%; height: 100%;">
                                                            @else
                                                            @endif
                                                        </div>
                                                        <h4 class="two-row-max"
                                                            style="min-height: 96px;">{{ $item->name }}</h4>
                                                        <p class="one-row-max"
                                                           style="min-height: 30px;">
                                                            Product:&nbsp{{ $item->product_family()->first()->product_name }}</p>
                                                        <p class="one-row-max"
                                                           style="min-height: 30px;">
                                                            Type:&nbsp{{ $item->pdf_type()->first()->name }}</p>
                                                        <p class="one-row-max"
                                                           style="min-height: 30px;">Language:&nbsp{{ $item->language }}</p>
                                                        <p class="one-row-max"
                                                           style="min-height: 30px;">Version #&nbsp{{ $item->version_number }}</p>
                                                        <br>
                                                        <div class="link">&#xf178;</div>
                                                    </div>
                                                </div>
                                            </a>
                                        @endforeach
                                    </div>
                                </div>


                            </div>
                        @endif
                    @if(count($all_products)!=0)
                        <div class="item white-section" id="all_products">
                            <section class="section white-section">
                                <div class="container">
                                    <div id="shop-grid">
                                        @foreach($all_products as $item)
                                            <div class="one-third column">
                                                <a href={{ route('product_page', ['id' => preg_replace('/--+/','-',preg_replace('/[^A-Za-z0-9\-]/', '-', $item->product_name)).'-'.$item->id]) }}>
                                                    <div class="blog-box-1 grey-section"
                                                         style="text-align: center">

                                                        @if($item->product()->first()->unapproved_drug=='1' && (!(Auth::guard('practitioner')->check() && (Auth::guard('practitioner')->user()->ahpra_number || $if_au == 0) )))
                                                            {{--                                        <img--}}
                                                            {{--                                            src="https://cdn.medlab.co/www/Images/demo_drug.png"--}}
                                                            {{--                                            style="display: inline-block" alt=""/>--}}
                                                            <img
                                                                src="{{ $image_url }}/{{ $item->product()->first()->fake_bottle }}"
                                                                style="display: inline-block" alt=""/>
                                                        @else
                                                            <img
                                                                src="{{ $image_url }}/{{ $item->product()->first()->img_front_view_small }}"
                                                                style="display: inline-block" alt=""/>
                                                        @endif
                                                        <div>
                                                            <h2 class="one-row-max"
                                                                style="padding: 5px;height: 40px;text-align: center">{{ $item->product_name }}</h2>
                                                            <br>
                                                            <div class="two-row-max"
                                                                 style="padding: 5px;height: 30px;">
                                                                <p>{!! $item->product_heading !!}</p></div>
                                                            <br>
                                                            <div class="two-row-max"
                                                                 style="padding: 5px;height: 50px;">
                                                                <p>{!! $item->product_byline !!}</p></div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </section>
                        </div>
                    @endif
                    @if(count($product_ingredient)!=0)

                        <div class="item white-section" id="product_ingredient">
                            <section class="section white-section">
                                <div class="container">
                                    <div class="clear"></div>
                                    <div id="shop-grid">
                                        @foreach($product_ingredient as $item)
                                            <div class="one-third column" style="height: 500px;">
                                                <a href={{ route('product_page', ['id' => preg_replace('/--+/','-',preg_replace('/[^A-Za-z0-9\-]/', '-', $item->product_name)).'-'.$item->id]) }}>
                                                    <div class="blog-box-1 grey-section"
                                                         style="text-align: center">

                                                        @if($item->product()->first()->unapproved_drug=='1' && (!(Auth::guard('practitioner')->check() && (Auth::guard('practitioner')->user()->ahpra_number || $if_au == 0) )))
                                                            {{--                                        <img--}}
                                                            {{--                                            src="https://cdn.medlab.co/www/Images/demo_drug.png"--}}
                                                            {{--                                            style="display: inline-block" alt=""/>--}}
                                                            <img
                                                                src="{{ $image_url }}/{{ $item->product()->first()->fake_bottle }}"
                                                                style="display: inline-block" alt=""/>
                                                        @else
                                                            <img
                                                                src="{{ $image_url }}/{{ $item->product()->first()->img_front_view_small }}"
                                                                style="display: inline-block" alt=""/>
                                                        @endif
                                                        <div>
                                                            <h2 class="one-row-max"
                                                                style="padding: 5px;height: 40px;text-align: center">{{ $item->product_name }}</h2>
                                                            <br>
                                                            <div class="two-row-max"
                                                                 style="padding: 5px;height: 30px;">
                                                                <p>{!! $item->product_heading !!}</p></div>
                                                            <br>
                                                            <div class="two-row-max"
                                                                 style="padding: 5px;height: 50px;">
                                                                <p>{!! $item->product_byline !!}</p></div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </section>


                        </div>
                    @endif

                </div>
                <div id="sync-sortcodes-2" class="owl-carousel">

                    @if(count($article)!=0)
                        <div class="item" onclick="setArticle()">
                            <h3>Articles</h3>
                        </div>
                    @endif
                        @if(count($flyers)!=0)
                            <div class="item" onclick="setFlyers()">
                                <h3>Flyers</h3>
                            </div>
                        @endif
                    @if(count($all_products)!=0)
                        <div class="item" onclick="setResult()">
                            <h3>Products</h3>
                        </div>
                    @endif
                    @if(count($product_ingredient)!=0)
                        <div class="item" onclick="setIngredient()">
                            <h3>Ingredients</h3>
                        </div>
                    @endif

                </div>
            </div>

        </section>
        @include('vms.layouts.footer')


    </main>

    <div class="scroll-to-top">&#xf106;</div>





    <!-- JAVASCRIPT
   ================================================== -->
    <script type="text/javascript">

        //Tabs 1

        $(document).ready(function () {

            var sync1 = $("#sync-sortcodes-1");
            var sync2 = $("#sync-sortcodes-2");

            sync1.owlCarousel({
                singleItem: true,
                transitionStyle: "backSlide",
                slideSpeed: 1500,
                navigation: false,
                pagination: false,
                afterAction: syncPosition,
                responsiveRefreshRate: 200
            });


            sync2.owlCarousel({
                items: '{{ $tab_amount }}',
                itemsDesktop: [1199, {{ $tab_amount }}],
                itemsDesktopSmall: [979, {{ $tab_amount }}],
                itemsTablet: [768, {{ $tab_amount }}],
                itemsMobile: [479, {{ $tab_amount }}],
                pagination: false,
                responsiveRefreshRate: 100,
                afterInit: function (el) {
                    el.find(".owl-item").eq(0).addClass("synced");
                }
            });

            function syncPosition(el) {
                var current = this.currentItem;
                $("#sync-sortcodes-2")
                    .find(".owl-item")
                    .removeClass("synced")
                    .eq(current)
                    .addClass("synced")
                if ($("#sync-sortcodes-2").data("owlCarousel") !== undefined) {
                    center(current)
                }
            }

            $("#sync-sortcodes-2").on("click", ".owl-item", function (e) {
                e.preventDefault();
                var number = $(this).data("owlItem");
                sync1.trigger("owl.goTo", number);
            });

            function center(number) {
                var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
                var num = number;
                var found = false;
                for (var i in sync2visible) {
                    if (num === sync2visible[i]) {
                        var found = true;
                    }
                }

                if (found === false) {
                    if (num > sync2visible[sync2visible.length - 1]) {
                        sync2.trigger("owl.goTo", num - sync2visible.length + 2)
                    } else {
                        if (num - 1 === -1) {
                            num = 0;
                        }
                        sync2.trigger("owl.goTo", num);
                    }
                } else if (num === sync2visible[sync2visible.length - 1]) {
                    sync2.trigger("owl.goTo", sync2visible[1])
                } else if (num === sync2visible[0]) {
                    sync2.trigger("owl.goTo", num - 1)
                }

            }


                @if(count($article)!=0)
            var height = document.getElementById('article').offsetHeight + 60;
            document.getElementById('sync-sortcodes-1').style.minHight = height + 'px';
                @elseif(count($flyers)!=0)
            var height = document.getElementById('flyers').offsetHeight + 60;
            document.getElementById('sync-sortcodes-1').style.height = height + 'px';
                @elseif(count($all_products)!=0)
            var height = document.getElementById('all_products').offsetHeight + 60;
            document.getElementById('sync-sortcodes-1').style.height = height + 'px';
                @elseif(count($product_ingredient)!=0)
            var height = document.getElementById('product_ingredient').offsetHeight + 60;
            document.getElementById('sync-sortcodes-1').style.height = height + 'px';
                @else
            var height = 60;
            document.getElementById('sync-sortcodes-1').style.minHight = height + 'px';
            @endif

        });

        function setArticle() {
            var height = document.getElementById('article').offsetHeight + 60;
            document.getElementById('sync-sortcodes-1').style.height = null;
            document.getElementById('sync-sortcodes-1').style.minHeight = height + 'px';

        }
        function setFlyers() {
            var height = document.getElementById('flyers').offsetHeight + 60;
            document.getElementById('sync-sortcodes-1').style.height = null;
            document.getElementById('sync-sortcodes-1').style.minHeight = height + 'px';

        }

        function setResult() {
            var height = document.getElementById('all_products').offsetHeight + 60;
            document.getElementById('sync-sortcodes-1').style.minHeight = null;
            document.getElementById('sync-sortcodes-1').style.height = height + 'px';
        }

        function setIngredient() {
            var height = document.getElementById('product_ingredient').offsetHeight + 60;
            document.getElementById('sync-sortcodes-1').style.minHeight = null;
            document.getElementById('sync-sortcodes-1').style.height = height + 'px';
        }

    </script>
    <script type="text/javascript" src="{{ asset('/js/jquery.parallax-1.1.3.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/owl.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/masonry.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/isotope.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/custom-corporate-home-1.js') }}"></script>



    <!-- End Document
================================================== -->

@endsection
