@extends('vms.layouts.main')
@section('title')
    Medlab - Login
@endsection
@section('breadcrumbs')
    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset;background-color:#7AA43F;">
            <ol class="breadcrumb medlab_breadcrumbs" style="margin-bottom: unset;background-color:#7AA43F;">
            </ol>
        </div>
    </div>

    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset">
            <ol class="breadcrumb medlab_breadcrumbs">
                <li><a class="medlab_breadcrumbs_link" href="/">@lang('trs.home')</a></li>
                <li class="active medlab_breadcrumbs_text">Login</li>
            </ol>
        </div>
    </div>
@endsection




@section('content')


    <main class="cd-main-content" id="main" style="margin-top:90px">
                <section class="section section-padding-top-bottom" id="scroll-link-6">

                    <div class="container">
                        <div class="sixteen columns">
                            <div class="section-title">
                                <h1>Login</h1>
                            </div>
                        </div>

                        <form name="ajax-form" id="ajax-form" method="post" action="{{ route('login') }}">
                            {!! csrf_field() !!}
                            <div class="row">
                                <label for="username" class="required"> Your email or username</label>
                                <input id="username" name="email" type="email" required
                                       placeholder="myusername or mymail@mail.com"/>
                            </div>
                            <div class="row">
                                <label for="password" class="required"> Your password </label>
                                <input id="password" name="password" required
                                       type="password"
                                       placeholder="eg. X8df!90EO"/>
                            </div>
                            <div class="keeplogin">
                                <input type="checkbox" name="remember" id="remember" value="on" style="width: unset;"/>
                                <label for="remember" style="display: inline-block;">Keep me logged in</label>


                            </div>
                            @if (Route::has('password.request'))
                                <a class="btn btn-link" href="{{ route('password.request') }}"
                                   style="width: 100%;text-align: left;background: unset;color: rgb(95, 155, 198);text-decoration: underline">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                                <a class="btn btn-link" href="{{ route('register') }}"
                                   style="width: 100%;text-align: left;background: unset;color: rgb(95, 155, 198);text-decoration: underline">Not a member ? Register here
                                </a>
                            @endif
                            <div class="sixteen columns">
                                <div id="button-con"><button name="submit" type="submit" id="submit_login">submit</button></div>
                            </div>
                            <div class="clear"></div>

                        </form>

                        <div class="clear"></div>

                        <div id="ajaxsuccess"></div>

                    </div>

                    <div class="clear"></div>

                </section>


        @include('vms.layouts.footer')


    </main>

    <div class="scroll-to-top">&#xf106;</div>





    <!-- JAVASCRIPT
    ================================================== -->
    <script type="text/javascript">
        (function ($) {
            "use strict";
            $(document).ready(function () {
                $("#ajax-form").submit(function (e) {
                    e.preventDefault(); // avoid to execute the actual submit of the form.
                    var form = $(this);
                    var url = form.attr('action');

                    $.ajax({
                        type: "POST",
                        url: url,
                        data: form.serialize(), // serializes the form's elements.
                        success: function (data) {
                            if (data.msg == '2fa'){
                                location.href = "/2fa_verify";
                            }
                            else if (data.msg == 'practitioner') {
                                var list = JSON.parse(data.practitioner);
                                var options = {};
                                $.map(list,
                                    function (o) {
                                        if (o.customer_name == o.trading_name || o.trading_name==null) {
                                            options[o.guid] = o.customer_name;
                                        } else {
                                            options[o.guid] = o.customer_name + '|' + o.trading_name;
                                        }

                                    });
                                const {value: practitioner} = Swal.fire({
                                    title: 'Select a business',
                                    input: 'select',
                                    inputOptions: options,    // inputOptions: {
                                    inputPlaceholder: 'Select a business',
                                    allowOutsideClick: false,
                                    showLoaderOnConfirm: true,
                                    inputValidator: (value) => {
                                        return new Promise((resolve) => {
                                            if(value == ''){
                                                resolve('You need to select a business')
                                            }else{
                                                $.ajax({
                                                    type: "POST",
                                                    url: '{{ url('submit_practitioner') }}',
                                                    data: {
                                                        _token: '{{ csrf_token() }}',
                                                        practitioner: value,
                                                    },
                                                    success: function (data) {
                                                        if (data.msg == 'obsolete account') {
                                                            Swal.fire({
                                                                icon: 'error',
                                                                html: "Your account " + data.name + " has been closed. Please <a href='{{ route('contact') }}' style='font-size: unset'>contact us</a> if you would like to re-open the account",
                                                            })

                                                        } else {
                                                            // location.reload();
                                                            var redirect = parseURLParams(window.location.href);
                                                            if (redirect) {
                                                                window.location.href = '/' + redirect['redirectTo'];
                                                            } else {
                                                                window.history.back()
                                                            }
                                                        }
                                                    }

                                                })
                                            }

                                        })
                                    }
                                })
                            } else if (data.msg == 'patient') {
                                // location.reload();
                                var redirect = parseURLParams(window.location.href);
                                if (redirect) {
                                    window.location.href = '/' + redirect['redirectTo'];
                                } else {
                                    window.history.back()
                                }
                            } else if (data.msg == 'invalid account') {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Your account haven"t been processed yet, please try again later',
                                })
                            } else if (data.msg == 'obsolete account') {
                                Swal.fire({
                                    icon: 'error',
                                    html: "Your account " + data.name + " has been closed. Please <a href='{{ route('contact') }}' style='font-size: unset'>contact us</a> if you would like to re-open the account",
                                })
                            } else {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Invalid email or password',
                                })
                            }
                        }
                    });


                });
            });
            function parseURLParams(url) {
                var queryStart = url.indexOf("?") + 1,
                    queryEnd = url.indexOf("#") + 1 || url.length + 1,
                    query = url.slice(queryStart, queryEnd - 1),
                    pairs = query.replace(/\+/g, " ").split("&"),
                    parms = {}, i, n, v, nv;

                if (query === url || query === "") return;

                for (i = 0; i < pairs.length; i++) {
                    nv = pairs[i].split("=", 2);
                    n = decodeURIComponent(nv[0]);
                    v = decodeURIComponent(nv[1]);

                    if (!parms.hasOwnProperty(n)) parms[n] = [];
                    parms[n].push(nv.length === 2 ? v : null);
                }
                return parms;
            }
        })(jQuery);
    </script>
    <script type="text/javascript" src="{{ asset('/js/medlab.js') }}"></script>
    <!-- End Document
================================================== -->
@endsection
