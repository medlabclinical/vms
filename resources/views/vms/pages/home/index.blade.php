﻿@extends('vms.layouts.main')
@section('title')
    Medlab Home
@endsection
@section('description')
    @if($content->meta_description)
        {{ $content->meta_description }}
    @elseif(json_decode($content->content))
        @foreach(json_decode($content->content) as $item)
            @if($item->layout=='text')
                {{ substr(str_replace('&nbsp;',' ',strip_tags($item->attributes->content)),0,160) }}
            @endif
        @endforeach
    @endif
@endsection
@section('breadcrumbs')
    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset;background-color:#7AA43F;">
            <ol class="breadcrumb medlab_breadcrumbs" style="margin-bottom: unset;background-color:#7AA43F;">
            </ol>
        </div>
    </div>

    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset">
            <ol class="breadcrumb medlab_breadcrumbs">
                <li class="active medlab_breadcrumbs_text">Home</li>
            </ol>
        </div>
    </div>
@endsection
@section('content')
    <!-- HOME SECTION
    ================================================== -->

    <main class="cd-main-content" id="main" style="margin-top:45px">


        <!-- SLIDER IMAGE
================================================== -->
        @if(json_decode($content->slider))
            @foreach(json_decode($content->slider) as $slider)
                @if($slider->layout=='video')
                    <div class="containerHomepage">
                        <div class="textHomepage">
                            <iframe width=100% height=100%
                                    src="https://player.vimeo.com/video/{{ $slider->attributes->video_link }}?muted=1&autoplay=1&loop=1&autopause=0"
                                    allow="autoplay; fullscreen" allowfullscreen></iframe>
                        </div>
                    </div>

                @elseif($slider->layout=='image')

                    <section class="home">
                        <div class="slider-container">
                            <div class="tp-banner-container">
                                <div class="tp-banner">
                                    <ul>
                                        <li data-transition="fade" data-slotamount="1" data-masterspeed="500"
                                            data-saveperformance="on" data-title="Intro Slide">
                                            <img
                                                src="{{ $image_url }}/www/Images/{{ \App\Models\MediaLibrary::where('id',$slider->attributes->slider_image)->first()->name }}"
                                                alt="{{ $slider->attributes->slider_alt }}"
                                                data-lazyload="{{ $image_url }}/www/Images/{{ \App\Models\MediaLibrary::where('id',$slider->attributes->slider_image)->first()->name }}"
                                                data-bgposition="center top" data-bgfit="cover"
                                                data-bgrepeat="no-repeat">
                                            <a href="#">
                                                <div class="black-heavy-3">
                                                    <div class="black-heavy-3-heading">
                                                        <h1>{{ $slider->attributes->title }}</h1></div>
                                                    <div
                                                        class="black-heavy-3-subheading">{{ $slider->attributes->subtitle }}</div>
                                                </div>
                                            </a>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </section>
                @endif
            @endforeach
        @else
            <br>
        @endif

    <!-- CONTENT SECTION
    ================================================== -->
        @include('vms.layouts.content_section')
    <!-- SUBWEBS
        ================================================== -->

        @if(count($subweb))
        <section class="section grey-section section-padding-top-bottom">
            <div class="container">
                <div id="projects-grid">
                    @foreach($subweb as $item)

                        <div class="eight columns">
                            <a target="_blank" href="{{ $item->link }}" class="animsition-link">
                                <div class="portfolio-box-2 grey-section">
                                    <img src="{{ $image_url }}/www/Images/{{ $item->image()->first()->name }}" alt="{{ $item->alt }}" width = "100%">
                                    <h3>{{ $item->title }}</h3>
                                    <p>{{ $item->introduction }}</p>
                                    <br>
                                    <div class="link" style="font-family: 'FontAwesome';">
                                        &#xf178;
                                    </div>
                                </div>
                            </a>
                        </div>

                    @endforeach
                </div>

            </div>
        </section>
        @endif

        <!-- RECENT ARTICLES
            ================================================== -->

        @if(count($recent_articles))


            <section class="section white-section section-padding-top-bottom">
                <div class="container">

                    <h2 style="text-align: left;" class="padding">Recent Articles</h2>
                    <div class="blog-wrapper">
                        <div id="blog-grid-masonry">
                            @foreach($recent_articles as $item)
                                @if($item->article_type=='link')
                                    <a href={{ route('articles_content',['id' => preg_replace('/--+/','-',preg_replace('/[^A-Za-z0-9\-]/', '-', $item->title)).'-'.$item->id]) }}>
                                        <div class="blog-box-3 one-third column">
                                            <div class="blog-box-1 link-post grey-section">
                                                <h4 class="two-row-max"
                                                    style="min-height: 96px;">{{ $item->title }}</h4>
                                                <p class="four-row-max"
                                                   style="padding-bottom: unset;min-height: 92px;">{{ $item->preview_text }}</p>
                                                <br>
                                                <p>{{ substr($item->date,0,10) }}</p>
                                                @if(isset($item->author))<p>Author: {{ $item->author }}</p>@endif
                                                <div class="link">&#xf178;</div>
                                            </div>
                                        </div>
                                    </a>
                                @elseif($item->article_type=='video')
                                    <a href={{ route('articles_content',['id' => preg_replace('/--+/','-',preg_replace('/[^A-Za-z0-9\-]/', '-', $item->title)).'-'.$item->id]) }}>
                                        <div class="blog-box-3 one-third column">
                                            <div class="blog-box-1 grey-section">
                                                @if(($item->req_login && !(Auth::guard('practitioner')->check()||Auth::guard('patient')->check())))
                                                    <div style="max-height: 270px; position:relative; padding-bottom: 56.25%; height: 0; opacity: 0.85;background: black; ">
                                                        <iframe
                                                            srcdoc="<p style='text-align:center;margin-top:25%;font-size:xx-large;color:white'>Login as Practitioner to View</p>"
                                                            width="560" height="349" src="{{ $item->video_link }}" style = "position: absolute; top: 0; left: 0; width: 100%; height: 100%;"
                                                            allowfullscreen></iframe>
                                                    </div>
                                                @else
                                                    <div style="max-height: 270px; position:relative; padding-bottom: 56.25%; height: 0;">
                                                        <iframe width="560" height="349" src="{{ $item->video_link }}" style = "position: absolute; top: 0; left: 0; width: 100%; height: 100%;"
                                                                allowfullscreen></iframe>
                                                    </div>
                                                @endif
                                                <h4 class="two-row-max"
                                                    style="min-height: 96px;">{{ $item->title }}</h4>
                                                <p class="four-row-max"
                                                   style="padding-bottom: unset;min-height: 92px;">{{ $item->preview_text }}</p>
                                                <br>
                                                <p>{{ substr($item->date,0,10) }}</p>
                                                @if(isset($item->author))<p>Author: {{ $item->author }}</p>@endif
                                                <div class="link">&#xf178;</div>
                                            </div>
                                        </div>
                                    </a>
                                @else
                                    <a href={{ route('articles_content',['id' => preg_replace('/--+/','-',preg_replace('/[^A-Za-z0-9\-]/', '-', $item->title)).'-'.$item->id]) }}>
                                        <div class="blog-box-3 one-third column">
                                            <div class="blog-box-1 grey-section">
                                                <div style="max-height: 270px; position:relative; padding-bottom: 56.25%; height: 0;">
                                                    <img src="{{ $image_url }}/www/Images/{{ $item->title_img }}"
                                                         alt="{{ $item->title_alt }}" style="margin-left: auto;
                                                 margin-right: auto; position: absolute; top: 0; left: 0; width: 100%; height: 100%;">
                                                </div>
                                                <h4 class="two-row-max"
                                                    style="min-height: 96px;">{{ $item->title }}</h4>
                                                <p class="four-row-max"
                                                   style="padding-bottom: unset;min-height: 92px;">{{ $item->preview_text }}</p>
                                                <br>
                                                <p>{{ substr($item->date,0,10) }}</p>
                                                @if(isset($item->author))<p>Author: {{ $item->author }}</p>@endif
                                                <div class="link">&#xf178;</div>
                                            </div>
                                        </div>
                                    </a>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </section>

        @endif
        @if(count($highlight_article))


            <section class="section grey-section section-padding-top-bottom">
                <div class="container">

                    <h2 style="text-align: left;" class="padding">Highlight Articles</h2>
                    <div class="blog-wrapper">
                        <div id="blog-grid">
                            @foreach($highlight_article as $item)
                                @if($item->article_type=='link')
                                    <a href={{ route('articles_content',['id' => preg_replace('/--+/','-',preg_replace('/[^A-Za-z0-9\-]/', '-', $item->title)).'-'.$item->id]) }}>
                                        <div class="blog-box-3 one-third column">
                                            <div class="blog-box-1 link-post grey-section">
                                                <h4 class="two-row-max"
                                                    style="min-height: 96px;">{{ $item->title }}</h4>
                                                <p class="four-row-max"
                                                   style="padding-bottom: unset;min-height: 92px;">{{ $item->preview_text }}</p>
                                                <br>
                                                <p>{{ substr($item->date,0,10) }}</p>
                                                @if(isset($item->author))<p>Author: {{ $item->author }}</p>@endif
                                                <div class="link">&#xf178;</div>
                                            </div>
                                        </div>
                                    </a>
                                @elseif($item->article_type=='video')
                                    <a href={{ route('articles_content',['id' => preg_replace('/--+/','-',preg_replace('/[^A-Za-z0-9\-]/', '-', $item->title)).'-'.$item->id]) }}>
                                        <div class="blog-box-3 one-third column">
                                            <div class="blog-box-1 grey-section">
                                                @if(($item->req_login && !(Auth::guard('practitioner')->check()||Auth::guard('patient')->check())))
                                                    <div style="max-height: 270px; position:relative; padding-bottom: 56.25%; height: 0; opacity: 0.85;background: black; ">
                                                        <iframe
                                                            srcdoc="<p style='text-align:center;margin-top:25%;font-size:xx-large;color:white'>Login as Practitioner to View</p>"
                                                            width="560" height="349" src="{{ $item->video_link }}" style = "position: absolute; top: 0; left: 0; width: 100%; height: 100%;"
                                                            allowfullscreen></iframe>
                                                    </div>
                                                @else
                                                    <div style="max-height: 270px; position:relative; padding-bottom: 56.25%; height: 0;">
                                                        <iframe width="560" height="349" src="{{ $item->video_link }}" style = "position: absolute; top: 0; left: 0; width: 100%; height: 100%;"
                                                                allowfullscreen></iframe>
                                                    </div>
                                                @endif
                                                <h4 class="two-row-max"
                                                    style="min-height: 96px;">{{ $item->title }}</h4>
                                                <p class="four-row-max"
                                                   style="padding-bottom: unset;min-height: 92px;">{{ $item->preview_text }}</p>
                                                <br>
                                                <p>{{ substr($item->date,0,10) }}</p>
                                                @if(isset($item->author))<p>Author: {{ $item->author }}</p>@endif
                                                <div class="link">&#xf178;</div>
                                            </div>
                                        </div>
                                    </a>
                                @else
                                    <a href={{ route('articles_content',['id' => preg_replace('/--+/','-',preg_replace('/[^A-Za-z0-9\-]/', '-', $item->title)).'-'.$item->id]) }}>
                                        <div class="blog-box-3 one-third column">
                                            <div class="blog-box-1 grey-section">
                                                <div style="max-height: 270px; position:relative; padding-bottom: 56.25%; height: 0;">
                                                    <img src="{{ $image_url }}/www/Images/{{ $item->title_img }}"
                                                         alt="{{ $item->title_alt }}" style="margin-left: auto;
                                                 margin-right: auto; position: absolute; top: 0; left: 0; width: 100%; height: 100%;">
                                                </div>
                                                <h4 class="two-row-max"
                                                    style="min-height: 96px;">{{ $item->title }}</h4>
                                                <p class="four-row-max"
                                                   style="padding-bottom: unset;min-height: 92px;">{{ $item->preview_text }}</p>
                                                <br>
                                                <p>{{ substr($item->date,0,10) }}</p>
                                                @if(isset($item->author))<p>Author: {{ $item->author }}</p>@endif
                                                <div class="link">&#xf178;</div>
                                            </div>
                                        </div>
                                    </a>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </section>

        @endif
        @include('vms.layouts.footer')
    </main>

    <div class="scroll-to-top">&#xf106;</div>

    <!-- JAVASCRIPT
    ================================================== -->
    <script type="text/javascript" src="{{ asset('/js/medlab.js') }}"></script>


    <!-- End Document
    ================================================== -->
@endsection


