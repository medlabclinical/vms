﻿@extends('vms.layouts.main')
@section('title')
    Medlab - {{ $content->title }}
@endsection
@section('description')
    @if($content->meta_description)
        {{ $content->meta_description }}
    @elseif(json_decode($content->content))
        @foreach(json_decode($content->content) as $item)
            @if($item->layout=='text')
                {{ substr(str_replace('&nbsp;',' ',strip_tags($item->attributes->content)),0,160) }}
            @endif
        @endforeach
    @endif
@endsection
@section('breadcrumbs')
<div class="medlab_breadcrumbs_wrapper">
	<div class="container" style="width: unset;background-color:#7AA43F;">
		<ol class="breadcrumb medlab_breadcrumbs" style="margin-bottom: unset;background-color:#7AA43F;">
		</ol>
	</div>
</div>

<div class="medlab_breadcrumbs_wrapper">
	<div class="container" style="width: unset">
		<ol class="breadcrumb medlab_breadcrumbs">
			<li><a class="medlab_breadcrumbs_link" href="/">@lang('trs.home')</a></li>
			<li class="active medlab_breadcrumbs_text">Terms & Conditions</li>
		</ol>
	</div>
</div>
@endsection
@section('content')
    <main class="cd-main-content" id="main" style="margin-top:90px">

        <!-- SLIDER IMAGE
================================================== -->

        @if(json_decode($content->slider))
            @if(json_decode($content->slider)[0]->layout=='video')
                <div class="containerHomepage">
                    <div class="textHomepage">
                        <iframe width=100% height=100%
                                src="https://player.vimeo.com/video/{{ json_decode($content->slider)[0]->attributes->video_link }}?muted=1&autoplay=1&loop=1&autopause=0"
                                allow="autoplay; fullscreen" allowfullscreen></iframe>
                    </div>
                </div>
            @else

                <section class="home">
                    <div class="slider-container">
                        <div class="tp-banner-container">
                            <div class="tp-banner">
                                <ul>
                                    @foreach(json_decode($content->slider) as $slider)


                                        @if($slider->layout=='image')
                                            <li data-transition="fade" data-slotamount="1" data-masterspeed="500"
                                                data-saveperformance="on" data-title="Intro Slide">
                                                <img
                                                    src="{{ $image_url }}/www/Images/{{ \App\Models\MediaLibrary::where('id',$slider->attributes->slider_image)->first()->name }}"
                                                    alt="{{ $slider->attributes->slider_alt }}"
                                                    data-lazyload="{{ $image_url }}/www/Images/{{ \App\Models\MediaLibrary::where('id',$slider->attributes->slider_image)->first()->name }}"
                                                    data-bgposition="center top" data-bgfit="cover"
                                                    data-bgrepeat="no-repeat">
                                                <a href="#">
                                                    <div class="black-heavy-3">
                                                        <div class="black-heavy-3-heading">
                                                            <h1>{{ $slider->attributes->title }}</h1></div>
                                                        <div
                                                            class="black-heavy-3-subheading">{{ $slider->attributes->subtitle }}</div>
                                                    </div>
                                                </a>
                                            </li>
                                        @endif
                                    @endforeach
                                </ul>

                            </div>
                        </div>
                    </div>
                </section>
            @endif

        @else
            <br>
        @endif

        <!-- CONTENT SECTION
    ================================================== -->
        @include('vms.layouts.content_section')

        @include('vms.layouts.footer')


	</main>

	<div class="scroll-to-top">&#xf106;</div>





	<!-- JAVASCRIPT
    ================================================== -->
    <script type="text/javascript" src="{{ asset('/js/medlab.js') }}"></script>
	<!-- End Document
================================================== -->
	@endsection
