<table class='u-full-width'>
    <thead>
        <tr>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
            <th class='u-align-right'>Price</th>
            <th class='u-align-right'>Quantity</th>
            <th class='u-align-right'>Total</th>
        </tr>
    </thead>
    <tbody>
        @if(count($cart) > 0)
            @foreach($cart as $id => $details)
                <tr>
                    <td>
                        <img src="{{ Config::get('app.image_url') }}/{{ \App\Models\ProductModel::ProductImage($details['product_guid'])->img_front_view_small }}" width="100"/>
                    </td>
                    <td>
                        <p>{{ $details['product_description'] }}</p>
                        <p>Expiry: {{ $details['product_expiry'] }}</p>
                        <p>Batch #: {{ $details['product_batchnumber'] }}</p>
                        @if ($details["product_ship_cold"]==1) <p class='cold'>** Product ships cold</p><div class="clear"></div> @endif
                    </td>
                    <td class='u-align-right'>
                        ${{ number_format($details['product_cost'], 2, '.', ',') }}
                    </td>
                    <td class='u-align-right'>
                        <input type="number" value="{{ $details['cart_quantity'] }}" class="quantity update-cart" data-id="{{ $id }}"/>
                            <i class="fa fa-fw remove-cart" style="cursor: pointer;color: red" data-id="{{ $id }}">&#xf00d;</i>
                    </td>
                    <td class='u-align-right'>
                        ${{ number_format($details['product_cost'] * $details['cart_quantity'],2, '.', ',') }}
                    </td>
                </tr>
            @endforeach
        @endif
        <tr>
            <td class='no-border'>&nbsp;</td>
        </tr>
        <tr>
            <td class='no-border u-align-right' colspan='4'>Product Total exGST <br><p class="small">Free Shipping on purchases over $165.00 exGST</p></td>
            <td class='no-border u-align-right'>${{ number_format($total_exgst, 2, '.', ',') }} </td>
        </tr>
        <tr>
            <td class='no-border u-align-right' colspan='4'>Shipping exGST</td>
            <td class='no-border u-align-right'>${{ number_format($total_shipping_exgst, 2, '.', ',') }} </td>
        </tr>
        @if ($total_cold_items>0)
        <tr>
            <td class='no-border u-align-right' colspan='4'>ESKY exGST <p class="small">Free ESKY with 6+ cold shipped products. ({{ $total_cold_items }} items)</p></td>
            <td class='no-border u-align-right'>${{ number_format($total_esky_exgst, 2, '.', ',') }} </td>
        </tr>
        @endif
        <tr>
            <td class='no-border u-align-right' colspan='4'>Total GST</td>
            <td class='u-align-right'>${{ number_format($total_gst, 2, '.', ',') }} </td>
        </tr>
        <tr>
            <td class='no-border u-align-right' colspan='4'>Total Payable (inc GST)</td>
            <td class='u-align-right'>${{ number_format($total, 2, '.', ',') }} </td>
        </tr>
    </tbody>
</table>