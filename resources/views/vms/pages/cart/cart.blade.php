@extends('vms.layouts.main')
@section('title')
    Medlab - Cart
@endsection
@section('breadcrumbs')
    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset;background-color:#7AA43F;">
            <ol class="breadcrumb medlab_breadcrumbs" style="margin-bottom: unset;background-color:#7AA43F;">
            </ol>
        </div>
    </div>

    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset">
            <ol class="breadcrumb medlab_breadcrumbs">
                <li><a class="medlab_breadcrumbs_link" href="/">@lang('trs.home')</a></li>
                <li class="active medlab_breadcrumbs_text">Cart</li>
            </ol>
        </div>
    </div>
@endsection
@section('content')
    <main class="cd-main-content" id="main">
        <section class="section white-section section-home-padding-top">
            <div class="container">
                <div class="sixteen columns">
                    <div class="section-title left">
                        <h2>cart</h2>
                    </div>
                </div>
            </div>
        </section>


        @if ($cart)
            <section class="section white-section">
                <div id="cart-container" class='container'>
                    @if (!(Auth::guard('practitioner')->check() || Auth::guard('patient')->check()))
                        <div class="alert alert-blue" style="margin-bottom: 10px;">
                            <p>
                                Have a practitioner or patient account? <a href='#' onClick="open_login()"
                                                                           style="color: #6ba53a;cursor: pointer">Login
                                    to Continue</a>
                                <br>Don’t have an account? <a href="{{ route('register') }}"
                                                              style="color: #6ba53a;cursor: pointer">Create an
                                    Account</a>
                                <br>Or continue as a Guest Patient
                            </p>
                        </div>
                    @endif

                    <div class="sixteen columns">

                        @if(app('request')->has('discontinued'))
                        @if(count(json_decode(app('request')->input('discontinued'),true)))
                                <div class="alert alert-blue" style="margin-bottom: 10px;">
                                    @foreach(json_decode(app('request')->input('discontinued'),true) as $item)
                                        {{ $item }} have not been added to the cart as have been discontinued
                                        @endforeach
                                </div>
                            @endif
                        @endif
                            @if(app('request')->has('out_of_stock'))
                                @if(count(json_decode(app('request')->input('out_of_stock'),true)))
                                    <div class="alert alert-blue" style="margin-bottom: 10px;">
                                        @foreach(json_decode(app('request')->input('out_of_stock'),true) as $item)
                                            {{ $item }} have not been added to the cart as are currently out of stock
                                            @endforeach
                                    </div>
                            @endif
                            @endif
                        <h2>Items</h2>
                        <div id="cart_table">
                            @include('vms.pages.cart.cart_table')
                        </div>

                    </div>

                    <div class="row">
                        <a class="button-shortcodes text-size-1 text-padding-1 version-1" id='continue-shopping'>
                            Continue Shopping
                        </a>
                        <a class="button-shortcodes text-size-1 text-padding-1 version-1" id='clear-cart'> Clear Cart
                        </a>
                    </div>

                    <div class="row">
                        &nbsp;
                        <hr>
                    </div>

                    <form name="ajax-form" id="ajax-form" method="POST" action="{{ route('cart_checkout') }}">
                        @csrf
                        <input type="hidden" name="temp_cart_id" id="temp_cart_id" value={{ $temp_cart_id }}>
                        <meta name="_ctoken" content="{{ csrf_token() }}">
                        <meta name="_cupdate" content="{{ url('update-cart') }}">
                        <meta name="_cclear" content="{{ url('clear-cart') }}">
                        <meta name="_cremove" content="{{ url('remove-cart') }}">

                        <div class="row">
                            <h2>Address &amp; Contact Information</h2>
                            <br>

                            <br class='clear'>
                            <br class='clear'>

                            @if (Auth::guard('practitioner')->check() || isset($addressbook))
{{--                                                                @if (count($addressbook)>1)--}}
                                <div class="row">
                                    <label for="use_delivery" class="required"> Select delivery address</label>
                                    <select id="use_delivery" class="u-full-width" name="use_delivery"
                                            onchange="changeDelivery()" required data-printname="Delivery Address">
                                        <option value="" disabled selected>Please select a delivery address</option>
                                        @if (count($addressbook)>0)
                                            @foreach($addressbook as $item)
                                                <option
                                                    value={{ $item->id.'-'.$item->country }}>{{ $item->address_line_1.' '.$item->address_line_2.' '.$item->suburb.' '.$item->country.' '.$item->postcode }}</option>
                                            @endforeach
                                        @endif
                                        <option value="0">New Address</option>
                                    </select>
                                </div>
                                {{--                                @else--}}
                                {{--                                    <input type="hidden" name="use_delivery" id="use_delivery" value="0">--}}
                                {{--                                @endif--}}
                                <br>
                            @endif

                            <div id="shippingForm" style="display: none;">
                                <div class="row">
                                    <label for="street_address" class="required"> Street Address</label>
                                    <input id="street_address" type="text" data-printname="Address Line 1"
                                           name="address_line_1" onFocus="stopAutoFill()" required/>
                                </div>

                                <div class="row">
                                    <label for="second_address"> A second line if you need (Optional)</label>
                                    <input id="second_address" type="text" name="address_line_2"
                                           autocomplete="googleignoreautofill"/>
                                </div>

                                <div class="row">
                                    <label for="locality" class="required">Suburb</label>
                                    <input id="locality" type="text" data-printname="Suburb" name="suburb"
                                           onFocus="stopAutoFill()" required/>
                                </div>

                                <div class="row">
                                    <div class="one-third column" style='margin-left: 0; margin-right: 20px;'><label
                                            for="state">State</label><input class='readonly'
                                                                            id="administrative_area_level_1"
                                                                            data-printname="State" type="text" required
                                                                            name="state"
                                                                            autocomplete="googleignoreautofill"
                                                                            readonly></div>
                                    <div class="one-third column" style='margin-right: 20px;'><label for="postal_code">Postcode</label><input
                                            class='readonly' id="postal_code" type="text" data-printname="Postcode"
                                            name="postcode" autocomplete="googleignoreautofill" required readonly></div>
                                    <div class="one-third column" style='margin-right: 0;'><label for="country">
                                            Country</label><input class='readonly' id="country" type="text"
                                                                  data-printname="Country" name="country"
                                                                  autocomplete="googleignoreautofill" required
                                                                  readonly/></div>
                                </div>
                            </div>

                            <br>

                            <br class='clear'>

                            <div class="row">
                                <div class="one-third column" style='margin-left: 0; margin-right: 20px;'>
                                    <label for="firstname" class="required"> First Name</label>
                                    <input id="firstname" type="text" name="firstname" value='{{ $first_name }}'
                                           data-printname="First Name" onFocus="stopAutoFill()" required>
                                </div>
                                <div class="one-third column" style='margin-right: 20px;'>
                                    <label for="lastname" class="required"> Last Name</label>
                                    <input id="lastname" type="text" name="lastname" value='{{ $last_name }}'
                                           data-printname="Last Name" required>
                                </div>
                                <div class="one-third column" style='margin-right: 0;'></div>
                            </div>

                            <div class="row">
                                <label for="delivery_notes">
                                    Delivery Notes (To a max of 75 characters)</label>
                                <textarea id="delivery_notes" type="text" name="delivery_notes"></textarea>
                            </div>
                        </div>

                        <div class="row">
                            <h2>Order Reference</h2>
                            <br>
                            <label for="po_number"> Purchase Order Number (Optional)</label>
                            <input id="po_number" type="text" name="po_number">
                        </div>

                        <div class="row">
                            <h2>Payment Details</h2>
                            <br>

                            @if(Auth::guard('practitioner')->check())
                                <div class="row">
                                    <label for="pay_method"> Method of Payment</label>
                                    <select id="pay_method" name="pay_method" class="u-full-width" required>
                                        <option value="0" selected>Pay with account</option>
                                        <option value="1">Pay with Credit Card</option>
                                        @if(count($stored_cc_card)>0)
                                            <option value="2">Saved Credit Card</option>@endif
                                    </select>


                                </div>
                                <div id="stored_card_info">
                                    <label for="pay_with_account"> Select Stored Credit Card</label>
                                    @if(count($stored_cc_card)>0)
                                        <select id="stored_card_id" class="u-full-width" name="stored_card_id"
                                                data-printname="Stored Credit Card" required>
                                            <option value="" selected>Select One</option>
                                            @foreach($stored_cc_card as $item)
                                                <option
                                                    value={{ $item->id }}>{{ $item->card_number.','.$item->card_name.','.$item->card_expiry }}</option>
                                            @endforeach
                                        </select>
                                    @endif
                                </div>
                                <br>

                                <div id="card_info">
                                    <div class="alert alert-blue" style="margin-bottom: 10px;">
                                        <p>
                                            We accept Visa, Mastercard and Amex
                                        </p>
                                    </div>
                                    <div class="row">

                                        <label for="card_name" class="required"> Card Name</label>
                                        <input id="card_name" type="text" data-printname="Card Name" name="card_name"
                                               required/>
                                    </div>

                                    <div class="row">

                                        <label for="card_number" class="required"> Card Number</label>
                                        <input id="card_number" type="number" maxlength="16" value=""
                                               data-printname="Card Number" name="card_number"
                                               autocomplete="googleignoreautofill" required/>
                                    </div>

                                    <div class="row">
                                        <label for="card_expiry_month" class="required"> Expire Month</label>
                                        <select id="card_expiry_month" name="card_expiry_month"
                                                data-printname="Card Expiry Month" required>
                                            <option value="01">(01) January</option>
                                            <option value="02">(02) February</option>
                                            <option value="03">(03) March</option>
                                            <option value="04">(04) April</option>
                                            <option value="05">(05) May</option>
                                            <option value="06">(06) June</option>
                                            <option value="07">(07) July</option>
                                            <option value="08">(08) August</option>
                                            <option value="09">(09) September</option>
                                            <option value="10">(10) October</option>
                                            <option value="11">(11) November</option>
                                            <option value="12">(12) December</option>
                                        </select>
                                    </div>

                                    <div class="row">
                                        <label for="card_expiry_year" class="required"> Expire Year</label>
                                        <select id="card_expiry_year" name="card_expiry_year"
                                                data-printname="Card Expiry Year" required>
                                            @for ($i = date("Y"); $i <= date("Y")+10; $i++)
                                                <option value="{{ $i }}" selected>{{ $i }}</option>
                                            @endfor
                                        </select>
                                    </div>

                                    <div class="row">

                                        <label for="card_cvv" class="required"> Card CVV</label>
                                        <input id="card_cvv" type="number" value="" name="card_cvv" maxlength="3"
                                               data-printname="Card CVV" autocomplete="googleignoreautofill" required/>
                                    </div>

                                    <div class="row">
                                        <label for="save_card">
                                            <input id="save_card" type="checkbox" name="save_card" value="1"/>
                                            <span class="label-body">Save this card</span>
                                        </label>
                                    </div>
                                </div>

                            @endif
                        </div>
                    </form>


                    <div class="row">
                        <div style="float: right;cursor: pointer;font-family: 'Lato', sans-serif;"
                             class="button-shortcodes text-size-1 text-padding-1 version-1"
                             onClick="submitCart();" type="submit"> Checkout
                        </div>
                    </div>
                </div>
            </section>

        @else

            <section class="section grey-section section-padding-top-bottom">
                <div class="container">
                    <div class="sixteen columns">
                        <p style="font-size: larger">Your shopping cart is currently empty. <a
                                href="{{ route('products') }}" style="color: #6ba53a;font-size: large">Shop now</a></p>
                    </div>
                </div>
            </section>

        @endif
        @include('vms.layouts.footer')
    </main>

    <div class="scroll-to-top">&#xf106;</div>

    <link rel="stylesheet" type="text/css" href="{{ asset('css/medlab/cart.css') }}">

    <script type="text/javascript" src="{{ asset('/build/js/intlTelInput.js') }}"></script>
    <script type="text/javascript" src="js/modernizr.custom.js"></script>

    <script type="text/javascript" src="{{ asset('js/medlab/cart.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/custom-cart.js') }}"></script>
    <script>
        window.addEventListener("load", function () {
            if ('{{ $show_notification }}' == 1) {
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 5000,
                    // timerProgressBar: true,
                    didOpen: (toast) => {
                        toast.addEventListener('mouseenter', Swal.stopTimer)
                        toast.addEventListener('mouseleave', Swal.resumeTimer)
                    }
                })

                Toast.fire({
                    icon: 'success',
                    title: 'There are items in your cart from previous sessions'
                })
            }
        });
    </script>

@endsection
