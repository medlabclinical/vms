@if(json_decode($data))

    @foreach(json_decode($data) as $item)
        @if($item->layout=='text')
            <div
                class="@if(isset($item->attributes->grey_section)) @if($item->attributes->grey_section) grey-section @else white-section @endif @else white-section @endif"
                style="margin-top: 20px;">
                <div class="articleClass padding">
                    {!! $item->attributes->content !!}
                </div>
            </div>
        @elseif($item->layout=='image')
            <div
                class="@if(isset($item->attributes->grey_section)) @if($item->attributes->grey_section) grey-section @else white-section @endif @else white-section @endif"
                style="margin-top: 20px;">
                <div class="articleClass padding office-1">
                    <img
                        src="{{ $image_url }}/www/Images/{{ \App\Models\MediaLibrary::where('id',$item->attributes->image)->first()->name }}"
                        alt="{{ $item->attributes->image_alt }}"
                        style="width: {{ $item->attributes->width }}%; float: {{ $item->attributes->float }}; margin-left: auto; margin-right: auto;display: block">
                </div>
            </div>
        @elseif($item->layout=='video')
            <div
                class="@if(isset($item->attributes->grey_section)) @if($item->attributes->grey_section) grey-section @else white-section @endif @else white-section @endif"
                style="margin-top: 20px;">
                <div class="articleClass padding">
                    <div class="containerHomepage">
                        <div class="textHomepage">
                            <iframe width=100% height=100%
                                    src="https://player.vimeo.com/video/{{ $item->attributes->content }}"
                                    allow="autoplay; fullscreen" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
        @elseif($item->layout=='text_and_image')
            <div
                class="@if(isset($item->attributes->grey_section)) @if($item->attributes->grey_section) grey-section @else white-section @endif @else white-section @endif"
                style="margin-top: 20px;">
                <div class="office-1 office-middle">
                    @if($item->attributes->float=='right')
                        <div class="box-1 padding">
                            @if($item->attributes->type=='image')
                                <img
                                    src="{{ $image_url }}/www/Images/{{ \App\Models\MediaLibrary::where('id',$item->attributes->image)->first()->name }}"
                                    alt="{{ $item->attributes->image_alt }}">
                            @else
                                <div class="containerHomepage">
                                    <div class="textHomepage">
                                        <iframe width=100% height=100%
                                                src="https://player.vimeo.com/video/{{ $item->attributes->video }}"
                                                allow="autoplay; fullscreen"
                                                allowfullscreen></iframe>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div class="box-1 padding articleClass">
                            {!! $item->attributes->content !!}
                        </div>
                    @else
                        <div class="box-1 padding articleClass">
                            {!! $item->attributes->content !!}
                        </div>
                        <div class="box-1 padding">
                            @if($item->attributes->type=='image')
                                <img
                                    src="{{ $image_url }}/www/Images/{{ \App\Models\MediaLibrary::where('id',$item->attributes->image)->first()->name }}"
                                    alt="{{ $item->attributes->image_alt }}">
                            @else
                                <div class="containerHomepage">
                                    <div class="textHomepage">
                                        <iframe width=100% height=100%
                                                src="https://player.vimeo.com/video/{{ $item->attributes->video }}"
                                                allow="autoplay; fullscreen"
                                                allowfullscreen></iframe>
                                    </div>
                                </div>
                            @endif
                        </div>
                    @endif

                </div>
            </div>
        @elseif($item->layout=='two_image')
            <div
                class="@if(isset($item->attributes->grey_section)) @if($item->attributes->grey_section) grey-section @else white-section @endif @else white-section @endif"
                style="margin-top: 20px;">
                @if($item->attributes->title)
                    <h2 style="text-align: left;"
                        class="padding">{{ $item->attributes->title }}</h2>
                @endif
                <div class="office-1 office-middle">
                    <div class="box-1 padding">
                        @if($item->attributes->link1)
                            <a href="{{ $item->attributes->link1 }}">
                                @endif
                                <div class="portfolio-box-2">
                                    @if($item->attributes->type1=='video')
                                        <div class="containerHomepage">
                                            <div class="textHomepage">
                                                <iframe width=100% height=100%
                                                        src="https://player.vimeo.com/video/{{ $item->attributes->video1 }}"
                                                        allow="autoplay; fullscreen" allowfullscreen></iframe>
                                            </div>
                                        </div>
                                    @else
                                        <img
                                            src="{{ $image_url }}/www/Images/{{ \App\Models\MediaLibrary::where('id',$item->attributes->image1)->first()->name }}"
                                            alt="{{ $item->attributes->image_alt1 }}">
                                    @endif
                                    <div class="padding">
                                        <h3>{{ $item->attributes->title1 }}</h3>
                                        <p>{{ $item->attributes->introduction1 }}</p>
                                        @if($item->attributes->link1)
                                            @if($item->attributes->button1)
                                                <br>
                                                <div style="margin-right: unset;"
                                                     class="button-shortcodes text-size-1 text-padding-1 version-1">{{ $item->attributes->button1 }}
                                                </div>
                                            @else
                                                <br>
                                                <div class="link" style="font-family: 'FontAwesome';">
                                                    &#xf178;
                                                </div>
                                            @endif
                                        @endif
                                    </div>
                                </div>
                                @if($item->attributes->link1)
                            </a>
                        @endif
                    </div>
                    <div class="box-1 padding">
                        @if($item->attributes->link2)
                            <a href="{{ $item->attributes->link2 }}">
                                @endif
                                <div class="portfolio-box-2">
                                    @if($item->attributes->type2=='video')
                                        <div class="containerHomepage">
                                            <div class="textHomepage">
                                                <iframe width=100% height=100%
                                                        src="https://player.vimeo.com/video/{{ $item->attributes->video2 }}"
                                                        allow="autoplay; fullscreen" allowfullscreen></iframe>
                                            </div>
                                        </div>
                                    @else
                                        <img
                                            src="{{ $image_url }}/www/Images/{{ \App\Models\MediaLibrary::where('id',$item->attributes->image2)->first()->name }}"
                                            alt="{{ $item->attributes->image_alt2 }}">
                                    @endif
                                    <div class="padding">
                                        <h3>{{ $item->attributes->title2 }}</h3>
                                        <p>{{ $item->attributes->introduction2 }}</p>
                                        @if($item->attributes->link2)
                                            @if($item->attributes->button2)
                                                <br>
                                                <div style="margin-right: unset;"
                                                     class="button-shortcodes text-size-1 text-padding-1 version-1">{{ $item->attributes->button2 }}
                                                </div>
                                            @else
                                                <br>
                                                <div class="link" style="font-family: 'FontAwesome';">
                                                    &#xf178;
                                                </div>
                                            @endif
                                        @endif
                                    </div>
                                </div>
                                @if($item->attributes->link2)
                            </a>
                        @endif
                    </div>
                </div>
            </div>
        @elseif($item->layout=='three_image')
            <div
                class="@if(isset($item->attributes->grey_section)) @if($item->attributes->grey_section) grey-section @else white-section @endif @else white-section @endif"
                style="margin-top: 20px;">
                @if($item->attributes->title)
                    <h2 style="text-align: left;"
                        class="padding">{{ $item->attributes->title }}</h2>
                @endif
                <div class="office-1 office-middle">
                    <div class="article-one-third"
                         data-scroll-reveal="enter left move 200px over 1s after 0.3s">
                        @if($item->attributes->link1)
                            <a href="{{ $item->attributes->link1 }}">
                                @endif
                                <div class="portfolio-box-2">
                                    @if($item->attributes->type1=='video')

                                        <div class="containerHomepage">
                                            <div class="textHomepage">
                                                <iframe width=100% height=100%
                                                        src="https://player.vimeo.com/video/{{ $item->attributes->video1 }}"
                                                        allow="autoplay; fullscreen" allowfullscreen></iframe>
                                            </div>
                                        </div>
                                    @else
                                        <img
                                            src="{{ $image_url }}/www/Images/{{ \App\Models\MediaLibrary::where('id',$item->attributes->image1)->first()->name }}"
                                            alt="{{ $item->attributes->image_alt1 }}">
                                    @endif
                                    <div class="padding">
                                        <h3>{{ $item->attributes->title1 }}</h3>
                                        <p>{{ $item->attributes->introduction1 }}</p>
                                        @if($item->attributes->link1)
                                            @if($item->attributes->button1)
                                                <br>
                                                <div style="margin-right: unset;"
                                                     class="button-shortcodes text-size-1 text-padding-1 version-1">{{ $item->attributes->button1 }}
                                                </div>
                                            @else
                                                <br>
                                                <div class="link" style="font-family: 'FontAwesome';">
                                                    &#xf178;
                                                </div>
                                            @endif
                                        @endif
                                    </div>
                                </div>
                                @if($item->attributes->link1)
                            </a>
                        @endif
                    </div>
                    <div class="article-one-third"
                         data-scroll-reveal="enter left move 200px over 1s after 0.3s">
                        @if($item->attributes->link2)
                            <a href="{{ $item->attributes->link2 }}">
                                @endif
                                <div class="portfolio-box-2">
                                    @if($item->attributes->type2=='video')

                                        <div class="containerHomepage">
                                            <div class="textHomepage">
                                                <iframe width=100% height=100%
                                                        src="https://player.vimeo.com/video/{{ $item->attributes->video2 }}"
                                                        allow="autoplay; fullscreen" allowfullscreen></iframe>
                                            </div>
                                        </div>
                                    @else
                                        <img
                                            src="{{ $image_url }}/www/Images/{{ \App\Models\MediaLibrary::where('id',$item->attributes->image2)->first()->name }}"
                                            alt="{{ $item->attributes->image_alt2 }}">
                                    @endif
                                    <div class="padding">
                                        <h3>{{ $item->attributes->title2 }}</h3>
                                        <p>{{ $item->attributes->introduction2 }}</p>
                                        @if($item->attributes->link2)
                                            @if($item->attributes->button2)
                                                <br>
                                                <div style="margin-right: unset;"
                                                     class="button-shortcodes text-size-1 text-padding-1 version-1">{{ $item->attributes->button2 }}
                                                </div>
                                            @else
                                                <br>
                                                <div class="link" style="font-family: 'FontAwesome';">
                                                    &#xf178;
                                                </div>
                                            @endif
                                        @endif
                                    </div>
                                </div>
                                @if($item->attributes->link2)
                            </a>
                        @endif
                    </div>
                    <div class="article-one-third"
                         data-scroll-reveal="enter left move 200px over 1s after 0.3s">
                        @if($item->attributes->link3)
                            <a href="{{ $item->attributes->link3 }}">
                                @endif
                                <div class="portfolio-box-2">
                                    @if($item->attributes->type3=='video')

                                        <div class="containerHomepage">
                                            <div class="textHomepage">
                                                <iframe width=100% height=100%
                                                        src="https://player.vimeo.com/video/{{ $item->attributes->video3 }}"
                                                        allow="autoplay; fullscreen" allowfullscreen></iframe>
                                            </div>
                                        </div>
                                    @else
                                        <img
                                            src="{{ $image_url }}/www/Images/{{ \App\Models\MediaLibrary::where('id',$item->attributes->image3)->first()->name }}"
                                            alt="{{ $item->attributes->image_alt3 }}">
                                    @endif
                                    <div class="padding">
                                        <h3>{{ $item->attributes->title3 }}</h3>
                                        <p>{{ $item->attributes->introduction3 }}</p>
                                        @if($item->attributes->link3)
                                            @if($item->attributes->button3)
                                                <br>
                                                <div style="margin-right: unset;"
                                                     class="button-shortcodes text-size-1 text-padding-1 version-1">{{ $item->attributes->button3 }}
                                                </div>
                                            @else
                                                <br>
                                                <div class="link" style="font-family: 'FontAwesome';">
                                                    &#xf178;
                                                </div>
                                            @endif
                                        @endif
                                    </div>
                                </div>
                                @if($item->attributes->link3)
                            </a>
                        @endif
                    </div>
                </div>
            </div>

        @elseif($item->layout=='text_over_image')
            <div
                class="@if(isset($item->attributes->grey_section)) @if($item->attributes->grey_section) grey-section @else white-section @endif @else white-section @endif"
                style="margin-top: 20px;">
                @if($item->attributes->image_style=='parallax')
                    <div class="call-to-action-1"
                         style="background: url('{{ $image_url }}/www/Images/{{ \App\Models\MediaLibrary::where('id',$item->attributes->image)->first()->name }}') repeat fixed;background-size: cover;background-position: center"
                         aria-label={{ $item->attributes->image_alt }}>
                        <div class="articleClass padding"
                             style="width: {{ $item->attributes->width?:50 }}%;margin-left: {{ $item->attributes->margin_left?:25 }}%;background-color: {{ $item->attributes->background_color?:'unset' }};text-align: initial">
                            {!! $item->attributes->content !!}
                        </div>
                        @if($item->attributes->button)
                            <a href="{{ $item->attributes->button_link }}"
                               class="button-1 button-shortcodes text-size-1 text-padding-1 version-1"
                               style="color: black; background-color: white;float:unset">{{ $item->attributes->button }}</a>
                        @endif
                    </div>
                @else
                    <div class="call-to-action-1"
                         style="background: url('{{ $image_url }}/www/Images/{{ \App\Models\MediaLibrary::where('id',$item->attributes->image)->first()->name }}');background-size: cover;"
                         aria-label={{ $item->attributes->image_alt }}>
                        <div class="articleClass padding"
                             style="width: {{ $item->attributes->width?:50 }}%;margin-left: {{ $item->attributes->margin_left?:25 }}%;background-color: {{ $item->attributes->background_color?:'unset' }};text-align: initial">
                            {!! $item->attributes->content !!}
                        </div>
                        @if($item->attributes->button)
                            <a href="{{ $item->attributes->button_link }}"
                               class="button-1 button-shortcodes text-size-1 text-padding-1 version-1"
                               style="color: black; background-color: white;float:unset">{{ $item->attributes->button }}</a>
                        @endif
                    </div>
                @endif
            </div>

        @elseif($item->layout=='extend')
            <div
                class="@if(isset($item->attributes->grey_section)) @if($item->attributes->grey_section) grey-section @else white-section @endif @else white-section @endif"
                style="margin-top: 20px;padding: 10px 0px 10px 0px;">
                @if($item->attributes->header)
                    <h2 style="text-align: left;"
                        class="padding">{{ $item->attributes->header }}</h2>
                @endif

                <div class="office-1">
                    <div class="accordion" style="margin: 10px;">
                        <div
                            class="accordion_in @if($item->attributes->active) acc_active @endif"
                            data-scroll-reveal="enter left move 200px over 1s after 0.3s"
                            style="margin: unset">
                            <div
                                class="acc_head white-section">{{ $item->attributes->title }}</div>
                            <div class="acc_content white-section articleClass">
                                {!! $item->attributes->content !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @elseif($item->layout=='padding')
            <div
                class="@if(isset($item->attributes->grey_section)) @if($item->attributes->grey_section) grey-section @else white-section @endif @else white-section @endif"
                style="height: {{ $item->attributes->height }}px;">
            </div>
        @elseif($item->layout=='code')
            <div
                class="@if(isset($item->attributes->grey_section)) @if($item->attributes->grey_section) grey-section @else white-section @endif @else white-section @endif">
                @if($item->attributes->float=='right')
                    <div class="office-1 office-middle">
                        <div class="box-1 padding">
                            {!! $item->attributes->code !!}
                        </div>
                        <div class="box-1 padding articleClass">
                            {!! $item->attributes->content !!}
                        </div>
                    </div>
                @elseif($item->attributes->float=='left')
                    <div class="office-1 office-middle">
                        <div class="box-1 padding articleClass">
                            {!! $item->attributes->content !!}
                        </div>
                        <div class="box-1 padding">
                            {!! $item->attributes->code !!}
                        </div>
                    </div>
                @else
                    {!! $item->attributes->code !!}
                @endif
            </div>
        @elseif($item->layout=='file')
            <div
                class="@if(isset($item->attributes->grey_section)) @if($item->attributes->grey_section) grey-section @else white-section @endif @else white-section @endif">
                <div class="articleClass padding" style="min-height: 100px">
                    @if($item->attributes->float!='center')

                        <div class="button-shortcodes text-size-1 text-padding-1 version-1" style="float:{{ $item->attributes->float }}"
                             onClick="location.href = '{{ Storage::disk('s3private')->temporaryUrl($item->attributes->pdf_file, now()->addMinutes(5)) }}';"><span>&#xf18e;</span>
                            {{ $item->attributes->button_name }}
                        </div>
                    @else
                        <div class="button-shortcodes text-size-1 text-padding-1 version-1" style="left: 45%;position: inherit;margin-left: -65px;"
                             onClick="location.href = '{{ Storage::disk('s3private')->temporaryUrl($item->attributes->pdf_file, now()->addMinutes(5)) }}';"><span>&#xf18e;</span>
                            {{ $item->attributes->button_name }}
                        </div>
                    @endif
                </div>
            </div>
        @endif


    @endforeach
@endif
