<!DOCTYPE html>
<!--[if IE 8]>
<html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]>
<!-->
<html class="no-js" lang="en"><!--<![endif]-->
<head>

    <meta name="google-site-verification" content="E6Z2w_wPne5Z223-U298s1K1BEHyKl6oqq40JLYoxzU"/>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-TP3N8DM');</script>
    <!-- End Google Tag Manager -->
    <!-- Basic Page Needs
      ================================================== -->
    <meta charset="utf-8">
    <title>@yield('title')</title>
    <meta name="description" content="@yield('description')">
    <meta name="keywords" content="@yield('keywords')">

    <!-- Mobile Specific Metas
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">


    <!-- CSS
    ================================================== -->
    <link rel="stylesheet" href="{{ asset('/css/base.css') }}"/>
    <link rel="stylesheet" href="{{ asset('/css/skeleton.css') }}"/>
    <link rel="stylesheet" href="{{ asset('/css/layout.css') }}"/>
    <link rel="stylesheet" href="{{ asset('/css/settings.css') }}"/>
    <link rel="stylesheet" href="{{ asset('/css/font-awesome.css') }}"/>
    <link rel="stylesheet" href="{{ asset('/css/owl.carousel.css') }}"/>
    <link rel="stylesheet" href="{{ asset('/css/retina.css') }}"/>
    {{--<link rel="stylesheet" href="{{ asset('/css/animsition.min.css') }}"/>--}}

    <link rel="alternate stylesheet" type="text/css" href="{{ asset('/css/colors/color-gold.css') }}" title="1">
    <script type="text/javascript" src="{{ asset('/js/jquery-2.1.1.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script type="text/javascript" src="{{ asset('/js/promise-polyfill.js') }}"></script>
    {{--<link rel="alternate stylesheet" type="text/css" href="{{ asset('/css/colors/color-2.css') }}" title="2">--}}
    {{--<link rel="alternate stylesheet" type="text/css" href="{{ asset('/css/colors/color-3.css') }}" title="3">--}}
    {{--<link rel="alternate stylesheet" type="text/css" href="{{ asset('/css/colors/color-4.css') }}" title="4">--}}
    {{--<link rel="alternate stylesheet" type="text/css" href="{{ asset('/css/colors/color-5.css') }}" title="5">--}}
    {{--<link rel="alternate stylesheet" type="text/css" href="{{ asset('/css/colors/color-6.css') }}" title="6">--}}
    {{--<link rel="alternate stylesheet" type="text/css" href="{{ asset('/css/colors/color-7.css') }}" title="7">--}}


<!-- Favicons
================================================== -->
    {{--<link rel="shortcut icon" href="{{ asset('/images/favicon.png')}}">--}}
    {{--<link rel="apple-touch-icon" href="{{ asset('/images/apple-touch-icon.png')}}">--}}
    {{--<link rel="apple-touch-icon" sizes="72x72" href="{{ asset('/images/apple-touch-icon-72x72.png')}}">--}}
    {{--<link rel="apple-touch-icon" sizes="114x114" href="{{ asset('/images/apple-touch-icon-114x114.png')}}">--}}

    <link rel="stylesheet" type="text/css" href="{{ asset('/css/demo.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/style2.css')}}"/>
    <link href="{{ asset('/css/pdfviewer/pdf-viewer.css')}}" rel="stylesheet" type="text/css"/>
    {{--<link rel="stylesheet" type="text/css" href="{{ asset('/css/animate-custom.css')}}" />--}}

    {{--<link rel="stylesheet" type="text/css" href="{{ asset('/vendor/bootstrap/css/bootstrap.min.css')}}">--}}
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
    <!--===============================================================================================-->
    {{--<link rel="stylesheet" type="text/css" href="{{ asset('/vendor/animate/animate.css')}}">--}}
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/vendor/css-hamburgers/hamburgers.min.css')}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/vendor/select2/select2.min.css')}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/main.css')}}">
    <script type="text/javascript"
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAxApK8MkLkseusXp2Ctc2Ad6fL1zw8xNo&libraries=places"></script>
    <link rel="stylesheet" href="{{ asset('/build/css/intlTelInput.css') }}">


<script type="text/javascript" src="{{ asset('/js/jquery.themepunch.tools.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/jquery.themepunch.revolution.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/modernizr.custom.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/smk-accordion.js') }}"></script>

<script type="text/javascript">
    (function ($) {
        "use strict";
        $(document).ready(function () {
            var offset = 450;
            var duration = 500;
            jQuery(window).scroll(function () {
                if (jQuery(this).scrollTop() > offset) {
                    jQuery('.scroll-to-top').fadeIn(duration);
                } else {
                    jQuery('.scroll-to-top').fadeOut(duration);
                }
            });

            jQuery('.scroll-to-top').click(function (event) {
                event.preventDefault();
                jQuery('html, body').animate({scrollTop: 0}, duration);
                return false;
            })
            $(".accordion").smk_Accordion({
                closeAble: true, //boolean
            });
        });
    })(jQuery);
</script>
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TP3N8DM"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->