{{--<style>--}}
{{--    label {--}}
{{--        display: inline-block;--}}
{{--        font-size: 14px;--}}
{{--        line-height: 30px;--}}
{{--    }--}}
{{--    input {--}}
{{--        width: 40%;--}}
{{--        border: none;--}}
{{--        font-weight: 400;--}}
{{--        /*text-transform:uppercase;*/--}}
{{--        letter-spacing: 2px;--}}
{{--        font-size: 12px;--}}
{{--        line-height: 22px;--}}
{{--        padding-bottom: 10px;--}}
{{--        background: transparent;--}}
{{--        border-bottom: 2px solid #313131;--}}
{{--        color: #101010;--}}
{{--    }--}}
{{--</style>--}}
@extends('vms.layouts.main')
@section('title')
    Medlab - 2FA Verify
@endsection
@section('breadcrumbs')
    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset;background-color:#7AA43F;">
            <ol class="breadcrumb medlab_breadcrumbs" style="margin-bottom: unset;background-color:#7AA43F;">
            </ol>
        </div>
    </div>

    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset">
            <ol class="breadcrumb medlab_breadcrumbs">
                <li><a class="medlab_breadcrumbs_link" href="/">@lang('trs.home')</a></li>
                <li class="active medlab_breadcrumbs_text">2fa verify</li>
            </ol>
        </div>
    </div>
@endsection
@section('content')

    <main class="cd-main-content" id="main" style="margin-top:90px">
        <section class="section section-home-padding-top" style="padding-top: 60px;">

            <div class="container">
                <div class="sixteen columns">
                    <div class="section-title left">
                        <h1>Two Factor Authentication</h1>
                    </div>
                </div>
            </div>
        </section>
        <section class="section white-section section-padding-top-bottom">
            <div class="container">

                <div>
                    <p>Two factor authentication (2FA) strengthens access security by requiring two methods (also
                        referred to as factors) to verify your identity. Two factor authentication protects against
                        phishing, social engineering and password brute force attacks and secures your logins from
                        attackers exploiting weak or stolen credentials.</p>

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <p>Enter the pin from Google Authenticator app:</p><br/>
                    <form class="form-horizontal" action="{{ route('2faVerify') }}" method="POST" id="verifyForm" style="text-align: center;width: 100%;">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('one_time_password-code') ? ' has-error' : '' }}" style="margin-bottom:20px">
                            <label for="one_time_password" class="control-label">One Time Password</label>
                            <input id="one_time_password" name="one_time_password" class="form-control col-md-4 two-auth-input" style="width: 30%"
                                   type="text" required/>
                        </div>
                        <a onclick="useBackupCode()" style="cursor: pointer;text-decoration: underline;margin-top: 20px;">Use a Backup Code to Authenticate</a><br>
                        <a onclick="useRecoveryPhone()" style="cursor: pointer;text-decoration: underline;margin-top: 20px;">Reset 2 Factor Authentication</a>
                        <div id="button-con" style="margin-top: 30px">
                            <button class="send_message" type="submit">Authenticate
                            </button>
                        </div>
{{--                        <button class="btn btn-primary" type="submit">Authenticate</button>--}}
                    </form>
                </div>

            </div>
        </section>
        @include('vms.layouts.footer')
    </main>

    <!-- JAVASCRIPT
  ================================================== -->
    <script type="text/javascript" src="{{ asset('/js/medlab.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery-2.1.1.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/modernizr.custom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.animsition.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.themepunch.tools.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.themepunch.revolution.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.hidescroll.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/isotope.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.counterup.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/waypoints.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/scrollReveal.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/owl.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/smk-accordion.js') }}"></script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            $(document).ready(function () {
                function loginProcess(data) {
                    if (data.msg == '2fa'){
                        location.href = "/2fa_verify";
                    }
                    else if (data.msg == 'practitioner') {
                        var list = JSON.parse(data.practitioner);
                        var options = {};
                        $.map(list,
                            function (o) {
                                if (o.customer_name == o.trading_name || o.trading_name==null) {
                                    options[o.guid] = o.customer_name;
                                } else {
                                    options[o.guid] = o.customer_name + '|' + o.trading_name;
                                }

                            });
                        const {value: practitioner} = Swal.fire({
                            title: 'Select a business',
                            input: 'select',
                            inputOptions: options,    // inputOptions: {
                            inputPlaceholder: 'Select a business',
                            allowOutsideClick: false,
                            showLoaderOnConfirm: true,
                            inputValidator: (value) => {
                                return new Promise((resolve) => {
                                    if(value == ''){
                                        resolve('You need to select a business')
                                    }else{
                                        $.ajax({
                                            type: "POST",
                                            url: '{{ url('submit_practitioner') }}',
                                            data: {
                                                _token: '{{ csrf_token() }}',
                                                practitioner: value,
                                            },
                                            success: function (data) {
                                                if (data.msg == 'obsolete account') {
                                                    Swal.fire({
                                                        icon: 'error',
                                                        html: "Your account " + data.name + " has been closed. Please <a href='{{ route('contact') }}' style='font-size: unset'>contact us</a> if you would like to re-open the account",
                                                    })

                                                } else {
                                                    location.href = '/';
                                                }
                                            }

                                        })
                                    }

                                })
                            }
                        })
                    } else if (data.msg == 'patient') {
                        location.href = '/';
                    } else if (data.msg == 'invalid account') {
                        Swal.fire({
                            icon: 'error',
                            title: 'Your account haven"t been processed yet, please try again later',
                        })
                    } else if (data.msg == 'obsolete account') {
                        Swal.fire({
                            icon: 'error',
                            html: "Your account " + data.name + " has been closed. Please <a href='{{ route('contact') }}' style='font-size: unset'>contact us</a> if you would like to re-open the account",
                        })
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'Invalid email or password',
                        })
                    }
                }
                $("#verifyForm").submit(function (e) {
                    e.preventDefault(); // avoid to execute the actual submit of the form.
                    var form = $(this);
                    var url = form.attr('action');

                    $.ajax({
                        type: "POST",
                        url: url,
                        data: form.serialize(), // serializes the form's elements.
                        success: function (data) {
                            $.ajax({
                                type: "POST",
                                url: "{{ url('login') }}",
                                data: {
                                    _token: '{{ csrf_token() }}',
                                    email:data.email,
                                    password:data.password,
                                }, // serializes the form's elements.
                                success: function (data) {
                                    loginProcess(data);
                                }
                            });
                        },
                        error: function (data) {
                            console.log(data);
                            Swal.fire({
                                icon: 'error',
                                title: data.responseJSON.message,
                            })

                        }
                    })
                });
                $(".accordion").smk_Accordion({
                    closeAble: true, //boolean
                });
                $(".animsition").animsition({

                    inClass: 'zoom-in-sm',
                    outClass: 'zoom-out-sm',
                    inDuration: 1500,
                    outDuration: 800,
                    linkElement: '.animsition-link',
                    // e.g. linkElement   :   'a:not([target="_blank"]):not([href^=#])'
                    loading: true,
                    loadingParentElement: 'body', //animsition wrapper element
                    loadingClass: 'animsition-loading',
                    unSupportCss: ['animation-duration',
                        '-webkit-animation-duration',
                        '-o-animation-duration'
                    ],
                    //"unSupportCss" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
                    //The default setting is to disable the "animsition" in a browser that does not support "animation-duration".

                    overlay: false,

                    overlayClass: 'animsition-overlay-slide',
                    overlayParentElement: 'body'
                });
                $('.header-top').hidescroll();
                window.scrollReveal = new scrollReveal();
                var offset = 450;
                var duration = 500;
                jQuery(window).scroll(function () {
                    if (jQuery(this).scrollTop() > offset) {
                        jQuery('.scroll-to-top').fadeIn(duration);
                    } else {
                        jQuery('.scroll-to-top').fadeOut(duration);
                    }
                });

                jQuery('.scroll-to-top').click(function (event) {
                    event.preventDefault();
                    jQuery('html, body').animate({scrollTop: 0}, duration);
                    return false;
                })
            });
        })(jQuery);
        function loginProcess(data,path = '/') {
            if (data.msg == '2fa'){
                location.href = "/2fa_verify";
            }
            else if (data.msg == 'practitioner') {
                var list = JSON.parse(data.practitioner);
                var options = {};
                $.map(list,
                    function (o) {
                        if (o.customer_name == o.trading_name || o.trading_name==null) {
                            options[o.guid] = o.customer_name;
                        } else {
                            options[o.guid] = o.customer_name + '|' + o.trading_name;
                        }

                    });
                const {value: practitioner} = Swal.fire({
                    title: 'Select a business',
                    input: 'select',
                    inputOptions: options,    // inputOptions: {
                    inputPlaceholder: 'Select a business',
                    allowOutsideClick: false,
                    showLoaderOnConfirm: true,
                    inputValidator: (value) => {
                        return new Promise((resolve) => {
                            if(value == ''){
                                resolve('You need to select a business')
                            }else{
                                $.ajax({
                                    type: "POST",
                                    url: '{{ url('submit_practitioner') }}',
                                    data: {
                                        _token: '{{ csrf_token() }}',
                                        practitioner: value,
                                    },
                                    success: function (data) {
                                        if (data.msg == 'obsolete account') {
                                            Swal.fire({
                                                icon: 'error',
                                                html: "Your account " + data.name + " has been closed. Please <a href='{{ route('contact') }}' style='font-size: unset'>contact us</a> if you would like to re-open the account",
                                            })

                                        } else {
                                            location.href = path;
                                        }
                                    }

                                })
                            }

                        })
                    }
                })
            } else if (data.msg == 'patient') {
                location.href = '/';
            } else if (data.msg == 'invalid account') {
                Swal.fire({
                    icon: 'error',
                    title: 'Your account haven"t been processed yet, please try again later',
                })
            } else if (data.msg == 'obsolete account') {
                Swal.fire({
                    icon: 'error',
                    html: "Your account " + data.name + " has been closed. Please <a href='{{ route('contact') }}' style='font-size: unset'>contact us</a> if you would like to re-open the account",
                })
            } else {
                Swal.fire({
                    icon: 'error',
                    title: 'Invalid email or password',
                })
            }
        }
        async function useBackupCode() {
            const {value: backupCode} = await Swal.fire({
                title: 'Enter your Backup Code',
                input: 'text',
                showCancelButton: true,
                inputValidator: (value) => {
                    if (!value) {
                        return 'You need to write something!'
                    }
                }
            })

            $.ajax({
                type: "POST",
                url: '{{ url('backup_login') }}',
                data: {
                _token: '{{ csrf_token() }}',
                    code: backupCode,
            },
                success: function (data) {
                    $.ajax({
                        type: "POST",
                        url: "{{ url('login') }}",
                        data: {
                            _token: '{{ csrf_token() }}',
                            email:data.email,
                            password:data.password,
                            use_backup:'yes',
                        }, // serializes the form's elements.
                        success: function (data) {
                           loginProcess(data);
                        }
                    });
                }
            })

        }
        async function useRecoveryPhone() {
            const {value: lastThreeNum} = await Swal.fire({
                title: 'Enter the last three digits of your recovery phone number',
                input: 'text',
                showCancelButton: true,
                inputValidator: (value) => {
                    if (!value) {
                        return 'You need to write something!'
                    }
                }
            })
            Swal.fire({
                title: 'Checking ..',
                allowOutsideClick: false,
                allowEscapeKey: false,
                showConfirmButton: false,
                showCloseButton: false,
                showCancelButton: false,
                onOpen: () => {
                    Swal.showLoading();
                    $.ajax({
                        type: "POST",
                        url: '{{ url('verify_last_three_num') }}',
                        data: {
                            _token: '{{ csrf_token() }}',
                            number: lastThreeNum,
                        },
                        success: function (data) {

                            if (data.status == 'error') {
                                Swal.fire({
                                    icon: 'error',
                                    title: data.msg,
                                })
                            } else {
                                const {value: verification_code} = Swal.fire({
                                    title: 'Enter your reset code that was sent via SMS to your Recovery Phone Number',
                                    input: 'text',
                                    inputLabel: 'Your reset code',
                                    showCancelButton: true,
                                    inputValidator: (value) => {
                                        if (!value) {
                                            return 'You need to write something!'
                                        }else{
                                            Swal.fire({
                                                title: 'Checking ..',
                                                allowOutsideClick: false,
                                                allowEscapeKey: false,
                                                showConfirmButton: false,
                                                showCloseButton: false,
                                                showCancelButton: false,
                                                onOpen: () => {
                                                    Swal.showLoading();
                                                    $.ajax({
                                                        type: "POST",
                                                        url: '{{ url('verify_verification_code') }}',
                                                        data: {
                                                            _token: '{{ csrf_token() }}',
                                                            code: value,
                                                        },
                                                        success: function (data) {
                                                            if (data.status == 'error') {
                                                                Swal.fire({
                                                                    icon: 'error',
                                                                    title: data.msg,
                                                                })
                                                            } else {
                                                                Swal.fire({
                                                                    icon: 'success',
                                                                    title: data.msg,
                                                                }).then((result) => {
                                                                    if (result.isConfirmed) {
                                                                        Swal.fire({
                                                                            title: 'Logging in ..',
                                                                            allowOutsideClick: false,
                                                                            allowEscapeKey: false,
                                                                            showConfirmButton: false,
                                                                            showCloseButton: false,
                                                                            showCancelButton: false,
                                                                            onOpen: () => {
                                                                                Swal.showLoading();
                                                                                $.ajax({
                                                                                    type: "POST",
                                                                                    url: "{{ url('login') }}",
                                                                                    data: {
                                                                                        _token: '{{ csrf_token() }}',
                                                                                        email: data.email,
                                                                                        password: data.password,
                                                                                    }, // serializes the form's elements.
                                                                                    success: function (data) {
                                                                                        var path = '/2fa';
                                                                                        loginProcess(data, path);
                                                                                    }
                                                                                });
                                                                            }
                                                                        })
                                                                    }
                                                                })
                                                            }
                                                        }
                                                    })
                                                }
                                            })
                                        }
                                    }
                                })
                                if (verification_code) {
                                    $.ajax({
                                        type: "POST",
                                        url: '{{ url('verify_verification_code') }}',
                                        data: {
                                            _token: '{{ csrf_token() }}',
                                            code: verification_code,
                                        },
                                        success: function (data) {
                                            if (data.status == 'error') {
                                                Swal.fire({
                                                    icon: 'error',
                                                    title: data.msg,
                                                })
                                            } else {
                                                Swal.fire({
                                                    icon: 'success',
                                                    title: data.msg,
                                                }).then((result) => {
                                                    if (result.isConfirmed) {
                                                        $.ajax({
                                                            type: "POST",
                                                            url: "{{ url('login') }}",
                                                            data: {
                                                                _token: '{{ csrf_token() }}',
                                                                email: data.email,
                                                                password: data.password,
                                                            }, // serializes the form's elements.
                                                            success: function (data) {
                                                                var path = '/2fa';
                                                                loginProcess(data, path);
                                                            }
                                                        });
                                                    }
                                                })
                                            }
                                        }
                                    })
                                }
                                // Swal.fire({
                                //     icon: 'success',
                                //     title: data.msg,
                                // })
                            }

                        }
                    })
                }});



        }

    </script>

    <!-- End Document
================================================== -->
@endsection
