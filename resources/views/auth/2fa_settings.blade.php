{{--<style>--}}
{{--    label {--}}
{{--        display: block;--}}
{{--        font-size: 14px;--}}
{{--        line-height: 30px;--}}
{{--    }--}}
{{--    input {--}}
{{--        width: 100%;--}}
{{--        border: none;--}}
{{--        font-weight: 400;--}}
{{--        /*text-transform:uppercase;*/--}}
{{--        letter-spacing: 2px;--}}
{{--        font-size: 12px;--}}
{{--        line-height: 22px;--}}
{{--        padding-bottom: 10px;--}}
{{--        background: transparent;--}}
{{--        border-bottom: 2px solid #313131;--}}
{{--        color: #101010;--}}
{{--    }--}}
{{--    #update_recover_phone{--}}
{{--        font-size: 12px;--}}
{{--    }--}}
{{--</style>--}}
@extends('vms.layouts.main')
@section('title')
    Medlab - 2FA Setting
@endsection
@section('breadcrumbs')
    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset;background-color:#7AA43F;">
            <ol class="breadcrumb medlab_breadcrumbs" style="margin-bottom: unset;background-color:#7AA43F;">
            </ol>
        </div>
    </div>

    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset">
            <ol class="breadcrumb medlab_breadcrumbs">
                <li><a class="medlab_breadcrumbs_link" href="/">@lang('trs.home')</a></li>
                <li class="active medlab_breadcrumbs_text">2fa setting</li>
            </ol>
        </div>
    </div>
@endsection
@section('content')
    <main class="cd-main-content" id="main" style="margin-top:90px">
        <section class="section section-home-padding-top" style="padding-top: 60px;">

            <div class="container">
                <div class="sixteen columns">
                    <div class="section-title left">
                        <h1>Two Factor Authentication</h1>
                    </div>
                </div>
            </div>
        </section>
        <section class="section white-section section-padding-top-bottom">
            <div class="container">
                {{--                <div class="row justify-content-md-center">--}}
                {{--                    <div class="col-md-8">--}}
                {{--                        <div class="card">--}}
                {{--                <h1>Two Factor Authentication</h1>--}}
                <div>
                    <p style="margin-bottom: 20px">Two factor authentication (2FA) strengthens access security by requiring two methods
                        (also referred to as factors) to verify your identity. Two factor authentication
                        protects against phishing, social engineering and password brute force attacks and
                        secures your logins from attackers exploiting weak or stolen credentials.</p>

                    @if (session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                    @endif
                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif

                    @if($data['user']->loginSecurity == null)
                        <form class="form-horizontal" method="POST"
                              action="{{ route('generate2faSecret') }}" style="width: 100%;">
                            {{ csrf_field() }}
                            {{--                            <div class="form-group">--}}
                            <div id="button-con">
                                <button class="send_message" type="submit" style="width: unset;margin-top: 40px;">
                                    Generate Secret Key to Enable 2FA
                                </button>
                            </div>
                            {{--                                <button type="submit" class="btn btn-primary">--}}
                            {{--                                    Generate Secret Key to Enable 2FA--}}
                            {{--                                </button>--}}
                            {{--                            </div>--}}
                        </form>
                    @elseif(!$data['user']->loginSecurity->google2fa_enable)
                        1. Download the Microsoft Authenticator App from App store:<br>
                        <div style="margin-top: 20px;margin-bottom: 20px;">
                            <a href="https://apps.apple.com/au/app/microsoft-authenticator/id983156458">
                                <img src="https://cdn.medlab.co/www/Images/app_store.png" alt="app_store_icon"
                                >
                            </a>
                            <a href="https://play.google.com/store/apps/details?id=com.azure.authenticator">
                                <img src="https://cdn.medlab.co/www/Images/google_play.png" alt="google_play_icon"
                                >
                            </a><br>
                        </div>
                    <br>
                        2. Scan this QR code with your Microsoft Authenticator App. Alternatively, you can use
                        the code: <code>{{ $data['secret'] }}</code><br/>
                        <img src="{{$data['google2fa_url'] }}" alt="">
                        <br/><br/>
                        3. Enter the pin from Microsoft Authenticator app:<br/><br/>
                        <form class="form-horizontal" method="POST" action="{{ route('enable2fa') }}"
                              style="width: 100%;" id="enable2fa">
                            {{ csrf_field() }}
                            <div class="form-group{{ $errors->has('verify-code') ? ' has-error' : '' }}">
                                <label for="secret" class="control-label">Authenticator Code</label>
                                <input id="secret" type="password" class="form-control col-md-4 two-auth-input"
                                       name="secret" required>
                                @if ($errors->has('verify-code'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('verify-code') }}</strong>
                                        </span>
                                @endif
                            </div>
                            <div id="button-con">
                                <button class="send_message" type="submit">Enable 2FA
                                </button>
                            </div>
                            {{--                            <button type="submit" class="btn btn-primary">--}}
                            {{--                                Enable 2FA--}}
                            {{--                            </button>--}}
                        </form>
                    @elseif($data['user']->loginSecurity->google2fa_enable)

                        <div class="alert alert-blue" style="margin: 20px"><p>2FA is currently <strong>enabled</strong>
                                on your account.</p></div>

                        <div style="margin-bottom: 20px">
                            <h2>BackUp Codes</h2>
                            <br>
                            <p>8 single-use codes are active at this time, but you can generate more as needed.</p>
                            <div id="button-con" style="text-align: unset">
                                @if($data['user']->loginSecurity->backup_code)
                                <button class="send_message" onclick="showBackupCode()">Show Codes
                                </button>
                                @endif
                                <button class="send_message" style="width: unset;" onclick="generateBackupCode()">Get
                                    New Codes
                                </button>
                            </div>
                        </div>
                        <br>
                        <div style="margin-bottom: 20px">
                            <h2>Recovery Phone</h2>
                            <br>
                            <p style="margin-bottom: 20px;">If you have lost your 2 factor authentication, We will text you a code to disable 2 factor.</p>
                            <form style="width: 100%;" id="update_recover_phone" method="POST"
                                  action="{{ route('update_recover_phone') }}">
                                <input id="recovery_phone" type="tel" data-status="true"
                                       class="form-control two-auth-input"
                                       name="recovery_phone"
                                       style="text-transform: unset;font-size: 12px" required
                                       autocomplete="googleignoreautofill" value="{{ $data['phone'] }}">
                                <div id="button-con" style="text-align: unset">
                                    <button class="send_message" type="submit">Update
                                    </button>
                                </div>
                            </form>
                        </div>
                        <br>
                        <h2>Disable 2FA</h2>
                        <br>
                        <p>If you are looking to disable Two Factor Authentication. Please confirm your
                            password and Click Disable 2FA Button.</p>
                        <form class="form-horizontal" method="POST" action="{{ route('disable2fa') }}"
                              style="width: 100%;margin-top:20px;" id="disable2fa">
                            {{ csrf_field() }}
                            <div
                                class="form-group{{ $errors->has('current-password') ? ' has-error' : '' }}">
                                <label for="change-password" class="control-label">Current Password</label>
                                <input id="current-password" type="password" class="form-control col-md-4 two-auth-input"
                                       name="current-password" required>

                            </div>
                            <div id="button-con" style="text-align: unset">
                                <button class="send_message" type="submit">Disable 2FA
                                </button>
                            </div>
                            {{--                            <button type="submit" class="btn btn-primary ">Disable 2FA</button>--}}
                        </form>
                    @endif
                </div>
                {{--                        </div>--}}
                {{--                    </div>--}}
                {{--                </div>--}}
            </div>
        </section>
        @include('vms.layouts.footer')
    </main>
    <div class="scroll-to-top">&#xf106;</div>
    <!-- JAVASCRIPT
  ================================================== -->
    <script type="text/javascript" src="{{ asset('/build/js/intlTelInput.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/medlab.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery-2.1.1.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/modernizr.custom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.animsition.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.themepunch.tools.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.themepunch.revolution.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.hidescroll.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/isotope.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.counterup.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/waypoints.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/scrollReveal.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/owl.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/smk-accordion.js') }}"></script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            $(document).ready(function () {
                $("#enable2fa").submit(function (e) {
                    e.preventDefault(); // avoid to execute the actual submit of the form.
                    var form = $(this);
                    var url = form.attr('action');
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: form.serialize(), // serializes the form's elements.
                        success: function (data) {
                            if (data.status == 'success') {
                                Swal.fire({
                                    icon: 'success',
                                    title: data.msg,
                                }).then((result) => {
                                    location.reload();
                                })

                            } else {
                                Swal.fire({
                                    icon: 'error',
                                    title: data.msg,
                                })
                                document.getElementById('secret').value = '';
                            }

                        }
                    })
                });
                $("#disable2fa").submit(function (e) {
                    e.preventDefault(); // avoid to execute the actual submit of the form.
                    var form = $(this);
                    var url = form.attr('action');
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: form.serialize(), // serializes the form's elements.
                        success: function (data) {
                            if (data.status == 'success') {
                                Swal.fire({
                                    icon: 'success',
                                    title: data.msg,
                                }).then((result) => {
                                    location.reload();
                                })

                            } else {
                                Swal.fire({
                                    icon: 'error',
                                    title: data.msg,
                                })
                                document.getElementById('secret').value = '';
                            }

                        }
                    })
                });
                $(".accordion").smk_Accordion({
                    closeAble: true, //boolean
                });
                $(".animsition").animsition({

                    inClass: 'zoom-in-sm',
                    outClass: 'zoom-out-sm',
                    inDuration: 1500,
                    outDuration: 800,
                    linkElement: '.animsition-link',
                    // e.g. linkElement   :   'a:not([target="_blank"]):not([href^=#])'
                    loading: true,
                    loadingParentElement: 'body', //animsition wrapper element
                    loadingClass: 'animsition-loading',
                    unSupportCss: ['animation-duration',
                        '-webkit-animation-duration',
                        '-o-animation-duration'
                    ],
                    //"unSupportCss" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
                    //The default setting is to disable the "animsition" in a browser that does not support "animation-duration".

                    overlay: false,

                    overlayClass: 'animsition-overlay-slide',
                    overlayParentElement: 'body'
                });
                $('.header-top').hidescroll();
                window.scrollReveal = new scrollReveal();
                var offset = 450;
                var duration = 500;
                jQuery(window).scroll(function () {
                    if (jQuery(this).scrollTop() > offset) {
                        jQuery('.scroll-to-top').fadeIn(duration);
                    } else {
                        jQuery('.scroll-to-top').fadeOut(duration);
                    }
                });

                jQuery('.scroll-to-top').click(function (event) {
                    event.preventDefault();
                    jQuery('html, body').animate({scrollTop: 0}, duration);
                    return false;
                })
            });
        })(jQuery);

        function generateBackupCode() {
            $.ajax({
                type: "POST",
                url: '{{ url('generate_backup_code') }}',
                data: {
                    _token: '{{ csrf_token() }}',
                },
                success: function (data) {
                    if (data.status == 'success') {
                        var code = JSON.parse(data.code);
                        var content = '';
                        code.forEach(function (item) {
                            content = content.concat(item)
                            content = content.concat('<br>')
                        })
                        Swal.fire({
                            icon: 'success',
                            title: data.msg,
                            html: content,
                            footer: '<ul>' +
                                "<li style='list-style-type: disc;line-height:30px;'>You can only use each backup code once.</li>" +
                                "<li style='list-style-type: disc;line-height:30px;'>Keep these backup codes somewhere safe but accessible.</li>" +
                                '</ul>',
                        })

                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: data.msg,
                        })
                    }

                }
            })

        }

        function showBackupCode() {
            $.ajax({
                type: "POST",
                url: '{{ url('show_backup_code') }}',
                data: {
                    _token: '{{ csrf_token() }}',
                },
                success: function (data) {
                    if (data.status == 'success') {
                        var code = JSON.parse(data.code);
                        var content = '';
                        code.forEach(function (item) {
                            content = content.concat(item)
                            content = content.concat('<br>')
                        })
                        Swal.fire({
                            icon: 'success',
                            title: data.msg,
                            html: content,
                            footer: '<ul>' +
                                "<li style='list-style-type: disc;line-height:30px;'>You can only use each backup code once.</li>" +
                                "<li style='list-style-type: disc;line-height:30px;'>Keep these backup codes somewhere safe but accessible.</li>" +
                                '</ul>',
                        })

                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: data.msg,
                        })
                    }

                }
            })

        }
        document.getElementById('update_recover_phone').addEventListener('submit', (e) => {
            e.preventDefault();
            var number = int_recovery_phone.getNumber();
            if (number) {
                $.ajax({
                    type: "POST",
                    url: '{{ url('phone_existence_validate') }}',
                    data: {
                        _token: '{{ csrf_token() }}',
                        type: 'mobile',
                        phone: number,
                    },
                    timeout: 6000,
                    success: function (data) {
                        if (data.code == 2) {
                            Swal.fire({
                                icon: 'error',
                                title: 'Mobile number is not valid',
                            })
                        }else{
                            $.ajax({
                                type: "POST",
                                url: '{{ url('update_recover_phone') }}',
                                data: {
                                    _token: '{{ csrf_token() }}',
                                    phone: number,
                                },
                                timeout: 6000,
                                success: function (data) {
                                    if(data.msg = 'success'){
                                        Swal.fire({
                                            icon: 'success',
                                            title: 'Update Successfully',
                                        }).then((result) => {
                                            location.reload();
                                        })
                                    }

                                }
                            })
                        }

                    }
                })
            }

        })
        var recovery_phone = document.querySelector("#recovery_phone");
        var int_recovery_phone = window.intlTelInput(recovery_phone, {
            customPlaceholder: function (selectedCountryPlaceholder, selectedCountryData) {
                if (selectedCountryData['name'] == 'Australia') {
                    return '0402 000 000';
                } else {
                    return selectedCountryPlaceholder;
                }
            },
            initialCountry: "au",
            separateDialCode: true,
            utilsScript: "{{ asset('build/js/utils.js') }}",
        });
    </script>

    <!-- End Document
================================================== -->
@endsection

