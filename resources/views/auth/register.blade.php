@extends('vms.layouts.main')
@section('title')
    Register
@endsection
@section('content')

    <main class="cd-main-content" id="main" style="margin-top:90px">

        <section class="section white-section section-padding-top-bottom" id="scroll-link-6">

            <div class="container">
                <section class="section grey-section">
                    <div class="container">
                        <div data-scroll-reveal="enter bottom move 400px over 1s after 0.3s">
                            <div class="full-image">
                                @if($content->extra_image)
                                    <img
                                        src="{{ $image_url }}/www/Images/{{ \App\Models\MediaLibrary::where('id',$content->extra_image)->first()->name }}"
                                        alt="">
                                @endif
                            </div>
                        </div>
                    </div>
                </section>
                <div class="sixteen columns">
                    <div class="section-title">
                        <h1>Register</h1><br>
                    </div>
                </div>
                <section class="section grey-section section-padding-top">
                    <div class="container">
                        <a href="#pharmaForm" class="scroll">
                            <div class="eight columns" data-scroll-reveal="enter right move 200px over 1s after 0.3s"
                                 onClick="extendForm3()" style="cursor: pointer;">
                                <div class="portfolio-box-2">
                                    @if($content->title_img)
                                        <img
                                            src="{{ $image_url }}/www/Images/{{ \App\Models\MediaLibrary::where('id',$content->title_img)->first()->name }}"
                                            alt="{{ $content->title_alt }}">
                                    @endif
                                    <div class="padding">
                                        <h3>Pharmacist/Business</h3>
                                        {!! $content->subtitle !!}
                                    </div>
                                </div>
                                {{--                                <div class="services-boxes-1" onClick="extendForm2()" style="cursor: pointer;">--}}
                                {{--                                    <div class="icon-box">&#xf007;</div>--}}
                                {{--                                    <h6>Patient</h6>--}}
                                {{--                                    {!! $content->subtitle2 !!}--}}
                                {{--                                </div>--}}
                            </div>
                        </a>
                        <a href="#practitionerForm" class="scroll">
                            <div class="eight columns" data-scroll-reveal="enter right move 200px over 1s after 0.3s"
                                 onClick="extendForm1()" style="cursor: pointer;">
                                @if($content->second_img)
                                    <img
                                        src="{{ $image_url }}/www/Images/{{ \App\Models\MediaLibrary::where('id',$content->second_img)->first()->name }}"
                                        alt="{{ $content->second_alt }}">
                                @endif
                                <div class="padding">
                                    <h3>Doctor</h3>
                                    {!! $content->subtitle2 !!}
                                </div>
                            </div>
                            {{--                            <div class="services-boxes-1" onClick="extendForm1()" style="cursor: pointer;">--}}
                            {{--                                <div class="icon-box">&#xf007;</div>--}}
                            {{--                                <h6>Practitioner</h6>--}}
                            {{--                                {!! $content->subtitle !!}--}}
                            {{--                            </div>--}}
                            {{--                        </div>--}}
                        </a>
                        {{--                        <a href="#patientForm" class="scroll">--}}
                        {{--                            <div class="one-third column"--}}
                        {{--                                 data-scroll-reveal="enter right move 200px over 1s after 0.3s" onClick="extendForm2()"--}}
                        {{--                                 style="cursor: pointer;">--}}
                        {{--                                @if($content->third_img)--}}
                        {{--                                    <img--}}
                        {{--                                        src="{{ $image_url }}/www/Images/{{ \App\Models\MediaLibrary::where('id',$content->third_img)->first()->name }}"--}}
                        {{--                                        alt="{{ $content->third_alt }}">--}}
                        {{--                                @endif--}}
                        {{--                                <div class="padding">--}}
                        {{--                                    <h3>Patient</h3>--}}
                        {{--                                    {!! $content->subtitle3 !!}--}}
                        {{--                                </div>--}}
                        {{--                            </div>--}}
                        {{--                        </a>--}}
                    </div>
                </section>
                <form name="ajax-form" enctype="multipart/form-data" id="ajax-form" method="POST"
                      action="{{ route('register') }}"
                      autocomplete="googleignoreautofill">
                    @csrf
                    <div class="eight columns" style="width: 100%;">
                        <div id="pharmaForm" style="display: none">
                            <br>
                            <h4 style="text-align: left;">Register - Phamacist/Business</h4>
                            <br>
                            <div class="row" id="pharma_email_area">
                                <label for="pharma_email" class="required"> Your email</label>
                                <input id="pharma_email" type="email"
                                       class="form-control"
                                       name="pharma_email" required
                                       autocomplete="googleignoreautofill" style="text-transform: unset;"
                                       onFocus="stopAutoFillPharma()"
                                >
                                <small class="alert alert-red"> </small>
                            </div>
                            <a
                                class="button-shortcodes text-size-1 text-padding-1 version-1 scroll"
                                href="#pharmaBusinessInfo" id="pharma_email_check"
                                style="position:fixed;left:50%;margin-left: -65px;"><span>&#xf18e;</span>
                                Next
                            </a>
                            {{--                            <a href="#pharmaBusinessInfo" id="pharma_email_check"--}}
                            {{--                               class="arrow-down scroll"--}}
                            {{--                               style="position: fixed;bottom: unset;cursor: pointer">&#xf103;</a>--}}

                            <div id="pharmaBusinessInfo" style="display: none;">

                                <br>
                                <h4 style="text-align: left;">Your Business Details</h4>
                                <br>
                                <br>
                                <h4 style="text-align: left;">Business Address</h4>
                                <br>
                                <div class="row">

                                    <label for="pharma_international" style="display:inline-block;">
                                        Show International</label>
                                    <input id="pharma_international" type="checkbox"
                                           class="form-control"
                                           name="pharma_international" style="display:inline-block;width: unset"/>

                                </div>


                                <div class="row">

                                    <label for="pharma_street_address" class="required"
                                           style="width: 100%;">
                                        Street Address</label>
                                    <input id="pharma_street_address" type="text"
                                           class="form-control"
                                           name="pharma_street_address" required/>
                                    <small class="alert alert-red"> </small>

                                </div>
                                <div class="row">

                                    <label for="pharma_second_address" style="width: 100%;">
                                        A second line if you need (optional)</label>
                                    <input id="pharma_second_address" type="text"
                                           class="form-control"
                                           name="pharma_second_address" autocomplete="googleignoreautofill"/>
                                </div>
                                <div class="row" style="margin-bottom: unset;">
                                    <label for="pharma_locality" class="required"
                                           style="width: 33.3%;float: left">Suburb/Postcode</label>
                                    <label for="pharma_administrative_area_level_1"
                                           style="width: 33.3%;float: left">
                                        &nbsp;</label>
                                    <label for="pharma_postal_code"
                                           style="width: 33.3%;float: left">
                                        &nbsp;</label>
                                </div>
                                <div class="row" style="position:relative" id="pharma_locality_row">

                                    <input id="pharma_locality" type="text"
                                           class="form-control"
                                           name="pharma_locality"
                                           style="width:100%;float: left;position: absolute" required/>
                                    <input id="pharma_administrative_area_level_1" type="text"
                                           class="form-control"
                                           name="pharma_administrative_area_level_1"
                                           autocomplete="googleignoreautofill"
                                           style="width: 33.3%;float: left;margin-left: 33.3%" readonly>
                                    <input id="pharma_postal_code" type="text"
                                           class="form-control"
                                           name="pharma_postal_code" style="width: 33.3%;float: left"
                                           autocomplete="googleignoreautofill" readonly>
                                    <small class="alert alert-red"> </small>

                                </div>
                                <div class="row">

                                    <label for="pharma_country" style="width: 100%;">
                                        Country</label>
                                    <input id="pharma_country" type="text"
                                           class="form-control"
                                           name="pharma_country" readonly autocomplete="googleignoreautofill"
                                           style="border: unset;"/>
                                </div>
                                <input id="pharma_lat" name="pharma_lat" hidden/>
                                <input id="pharma_lng" name="pharma_lng" hidden/>

                                <div id="pharma_search_business_area" style="display: none">
                                    <div id="pharma_search_box">
                                        <br>
                                        <h4 style="text-align: left;" class="required">Search for a Business Name, ABN
                                            or ACN</h4>
                                        <br>
                                        <div class="row">
                                            <input list="pharma_business_list" id="pharma_business_search"
                                                   class="business-list"
                                                   style="text-transform: unset;width: 73%;float:left;border: 1px solid #313131;line-height:32px;padding-bottom: unset;margin-bottom:10px;"
                                                   required/>
                                            <small class="alert alert-red"> </small>
                                            {{--                                            <div--}}
                                            {{--                                                style="margin-left: 15px;padding: 8px 18px;background: #7AA43F;border: unset;"--}}
                                            {{--                                                class="button-shortcodes text-size-1 text-padding-1 version-1"--}}
                                            {{--                                                onClick="sendPharmaBusinessList()"><span--}}
                                            {{--                                                    style="font-size: x-large;padding: unset">&#xf002;</span>--}}
                                            {{--                                            </div>--}}
                                            <datalist id="pharma_business_list"></datalist>

                                        </div>
                                        {{--                                        <a href="#pharma_business_search" id="pharma_search_business_button"--}}
                                        {{--                                           onClick="sendPharmaBusinessList()" class="arrow-down scroll"--}}
                                        {{--                                           style="position: fixed;bottom: unset;cursor: pointer;">&#xf103;</a>--}}
                                        <a
                                            class="button-shortcodes text-size-1 text-padding-1 version-1 scroll"
                                            href="#pharma_business_search" id="pharma_search_business_button"
                                            onClick="sendPharmaBusinessList()"
                                            style="position:fixed;left:50%;margin-left: -65px;"><span>&#xf18e;</span>
                                            Next
                                        </a>
                                    </div>

                                    <div class="alert alert-blue" style="display: none;" id="pharma_business_detail">
                                        <div class="row">
                                            <label for="pharma_business_name" class="required">
                                                Legal Entity Name</label>
                                            {{--                                            <label for="pharma_business_type" id="pharma_business_type_label"--}}
                                            {{--                                                   style="width: 49%;float:right;"> Business--}}
                                            {{--                                                Type</label>--}}
                                            <textarea id="pharma_business_name" type="text"
                                                      class="form-control"
                                                      name="pharma_business_name"
                                                      style="text-transform: unset;border: unset;overflow:hidden;font-family:'Lato', sans-serif;"
                                                      readonly required></textarea>
                                            <small class="alert alert-red"> </small>
                                            <input id="pharma_business_type" type="text"
                                                   class="form-control"
                                                   name="pharma_business_type"
                                                   style="text-transform: unset;width: 49%;float:right; border: unset;"
                                                   readonly hidden>
                                        </div>
                                        <div class="row">
                                            <label for="pharma_business_number"
                                                   style="width: 50%;float:left;margin-right: 1%"
                                                   id="pharma_business_number_label"> Business
                                                Number (ABN/NZBN)
                                            </label>
                                            <label for="pharma_acn" style="width: 49%;float:right;"
                                                   id="pharma_acn_label">Company Number
                                                (ACN)</label>
                                            <input id="pharma_business_number" type="number"
                                                   class="form-control"
                                                   name="pharma_business_number"
                                                   style="text-transform: unset;width: 49%;float:left; border: unset;"
                                                   readonly>
                                            <input id="pharma_acn" type="number"
                                                   class="form-control"
                                                   name="pharma_acn"
                                                   style="text-transform: unset;width: 49%;float:right; border: unset;"
                                                   readonly>
                                        </div>
                                    </div>
                                    <div id="pharma_business_sub_detail" style="display: none">


                                        <div class="row">
                                            <label for="pharma_business_trading_name" class="required"
                                                   style="width: 100%">
                                                Business Name</label>
                                            <div class="alert alert-yelow">
                                                <p>Please enter your Human friendly business name. Example Big Mans
                                                    Chemist Chatswood</p>
                                            </div>
                                            <textarea id="pharma_business_trading_name" type="text"
                                                      class="form-control"
                                                      name="pharma_business_trading_name"
                                                      style="text-transform: unset;overflow:hidden;font-family:'Lato', sans-serif;"
                                                      required
                                                      autocomplete="googleignoreautofill"></textarea>
                                            <small class="alert alert-red"> </small>


                                        </div>
                                        <div class="row">
                                            <label for="pharma_primary_channel" class="required"
                                                   style="width: 100%">Business
                                                Primary Channel</label>
                                            <select id="pharma_primary_channel" type="text"
                                                    class="form-control"
                                                    name="pharma_primary_channel"
                                                    style="text-transform: unset;width: 100%;" required>
                                                <option disabled selected value> -- select an option --</option>
                                                @foreach($primary_channel as $item)
                                                    <option value="{{ $item->name }}">{{ $item->name }}</option>
                                                @endforeach
                                            </select>
                                            <small class="alert alert-red"> </small>

                                        </div>
                                        <div class="row" id="pharma_second_channel_area" style="display:none;">

                                            <label for="pharma_second_channel" class="required"
                                                   style="width: 100%"> Business Second Channel</label>
                                            <select id="pharma_second_channel" type="text"
                                                    class="form-control"
                                                    name="pharma_second_channel"
                                                    style="text-transform: unset;width: 100%;" required>
                                                <option disabled selected value> -- select an option --</option>
                                            </select>
                                            <small class="alert alert-red"> </small>
                                        </div>

                                        <div class="row">
                                            <label for="pharma_business_phone" class="required" style="width: 100%">
                                                Landline</label>

                                            <input id="pharma_business_phone" type="number" data-status="true"
                                                   class="form-control"
                                                   name="pharma_business_phone"
                                                   style="text-transform: unset;" required
                                                   autocomplete="googleignoreautofill">
                                            <small class="alert alert-red"> </small>


                                        </div>
                                        <div class="row">

                                            <label for="pharma_fax" style="width: 100%"> Fax</label>

                                            <input id="pharma_fax" type="number" data-status="true"
                                                   class="form-control"
                                                   name="pharma_fax"
                                                   style="text-transform: unset;" autocomplete="googleignoreautofill">
                                            <small class="alert alert-red"> </small>

                                        </div>
                                    </div>
                                </div>

                                {{--                                <a href="#pharma_personal_info" id="pharma_business_info_check"--}}
                                {{--                                   class="arrow-down scroll"--}}
                                {{--                                   style="position: fixed;bottom: unset;cursor: pointer;display:none;">&#xf103;</a>--}}
                                <a
                                    class="button-shortcodes text-size-1 text-padding-1 version-1 scroll"
                                    href="#pharma_personal_info" id="pharma_business_info_check"
                                    style="position:fixed;left:50%;margin-left: -65px;display:none;"><span>&#xf18e;</span>
                                    Next
                                </a>

                            </div>

                            <div class="row alert alert-blue" id="pharma_company_name_display_area"
                                 style="display: none">

                                <label for="pharma_company_name_display"> Business Name</label>
                                <input id="pharma_company_name_display" type="text"
                                       name="pharma_company_name_display"
                                       class="form-control"
                                       style="text-transform: unset;border: unset;"
                                       readonly>
                            </div>
                            <div id="pharma_personal_info" style="display: none;">
                                <br>
                                <h4 style="text-align: left;">Practitioner Information</h4>
                                <br>
                                <div class="row" id="if_have_number">
                                    <p>Do you have an AHPRA Number?</p>
                                    <input type="radio" id="pharma_have_ahpra_yes" name="pharma_have_ahpra"
                                           onclick="pharmaHaveAphraYes()"
                                           style="width:unset;-webkit-appearance: checkbox; margin-left: 10px; margin-top: 10px; font-size: x-large;"
                                           value="1">
                                    <label for="pharma_have_ahpra_yes" style="display: inline-block"> &nbspYes</label>
                                    <br>
                                    <input type="radio" id="pharma_have_ahpra_no" name="pharma_have_ahpra"
                                           onclick="pharmaHaveAphraNo()"
                                           style="width:unset;-webkit-appearance: checkbox;  margin-left: 10px; margin-top: 10px; margin-bottom: 10px;font-size: x-large;"
                                           value="0">
                                    <label for="pharma_have_ahpra_no" style="display: inline-block"> &nbspNo</label>
                                </div>
                                <div id="pharma_personal_info_detail" style="display: none">
                                    <div class="row">
                                        <label for="pharma_title" class="required">
                                            Title</label>
                                        <select id="pharma_title" type="text"
                                                class="form-control"
                                                name="pharma_title" required
                                                style="text-transform: unset;width: 49%;">
                                            <option>Mr</option>
                                            <option>Ms</option>
                                            <option>Miss</option>
                                            <option>Dr.</option>
                                            <option>Prof.</option>
                                        </select>
                                        <small class="alert alert-red"> </small>
                                    </div>
                                    <div class="row">
                                        <label for="pharma_firstname" class="required"> First Name</label>
                                        <input id="pharma_firstname" type="text"
                                               class="form-control"
                                               name="pharma_firstname" required
                                               style="text-transform: unset;"
                                               autocomplete="googleignoreautofill">
                                        <small class="alert alert-red"> </small>
                                    </div>
                                    <div class="row">
                                        <label for="pharma_lastname" class="required"> Last Name</label>
                                        <input id="pharma_lastname" type="text"
                                               class="form-control"
                                               name="pharma_lastname" required
                                               style="text-transform: unset;" autocomplete="googleignoreautofill">
                                        <small class="alert alert-red"> </small>
                                    </div>


                                    <div class="grey-section padding">
                                        <h4 style="text-align: left" class="required">Your Direct Number (Enter at least
                                            one number)</h4>
                                        <div class="row">
                                            <small class="alert alert-red"> </small>
                                            <label for="pharma_phone" style="width: 100%;">
                                                Landline</label>
                                            <input id="pharma_phone" type="number" data-status="true"
                                                   class="form-control"
                                                   name="pharma_phone"
                                                   style="text-transform: unset;"
                                                   autocomplete="googleignoreautofill">

                                        </div>
                                        <div class="row">
                                            <label for="pharma_mobile" style="width: 100%;"> Mobile
                                                Phone</label>
                                            <input id="pharma_mobile" type="number" data-status="true"
                                                   class="form-control"
                                                   name="pharma_mobile"
                                                   style="text-transform: unset;" autocomplete="googleignoreautofill">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <label for="pharma_primary_profession" class="required"
                                               style="width: 100%"> Primary
                                            Profession</label>
                                        <select id="pharma_primary_profession" type="text"
                                                class="form-control"
                                                name="pharma_primary_profession"
                                                style="text-transform: unset;width: 100%;" required>
                                            <option disabled selected value> -- select an option --</option>
                                            @foreach($modality as $item)
                                                <option value="{{ $item->name }}">{{ $item->name }}</option>
                                            @endforeach
                                        </select>
                                        <small class="alert alert-red"> </small>
                                    </div>
                                    <div class="row">
                                        <label for="pharma_job_title" class="required"
                                               style="width: 100%"> Job Title</label>
                                        <input id="pharma_job_title" type="text"
                                               class="form-control"
                                               name="pharma_job_title"
                                               style="text-transform: unset;" required>
                                        <small class="alert alert-red"> </small>
                                    </div>
                                    <div id="pharma_association_area" style="display: none;">
                                        <div class="row">
                                            <label for="pharma_association_name" class="required"
                                                   style="width: 100%"> Association
                                                Name</label>
                                            <select id="pharma_association_name" type="text"
                                                    class="form-control"
                                                    name="pharma_association_name"
                                                    style="text-transform: unset;width: 100%;">
                                                <option disabled selected value> -- select an option --</option>
                                                @foreach($association as $item)
                                                    <option value="{{ $item->name }}">{{ $item->name }}</option>
                                                @endforeach
                                            </select>
                                            <small class="alert alert-red"> </small>
                                        </div>
                                        <div class="row">
                                            <label for="pharma_association_number" class="required"
                                                   style="width: 100%"> Association Number</label>
                                            <input id="pharma_association_number" type="number"
                                                   class="form-control"
                                                   name="pharma_association_number"
                                                   style="text-transform: unset;">
                                            <small class="alert alert-red"> </small>
                                        </div>
                                        <div class="row">
                                            <label for="pharma_qualification"
                                                   style="width: 100%" class="required">Please assist us in the approval
                                                process by uploading a scanned copy of Certification/Association
                                                relevant to your modality<br>(Only accept .pdf .jpg .png .jpeg file (20
                                                MB Max))</label>
                                            <input id="pharma_qualification" type="file"
                                                   class="form-control"
                                                   name="pharma_qualification"
                                                   style="text-transform: unset;">
                                            <small class="alert alert-red"> </small>
                                        </div>
                                    </div>
                                    <div class="row" id="pharma_ahpra_area">
                                        <label for="pharma_ahpra_number" class="required">
                                            AHPRA Number</label>
                                        <input id="pharma_ahpra_number" type="text"
                                               class="form-control"
                                               name="pharma_ahpra_number"
                                               style="text-transform: unset;width: 49%;"
                                               autocomplete="googleignoreautofill">
                                        <small class="alert alert-red"> </small>
                                    </div>
                                </div>
                                {{--                                                                <div class="alert alert-blue">--}}
                                {{--                                                                    <p style="text-transform: unset;">AHPRA Number required for SAS Submissions</p>--}}
                                {{--                                                                </div>--}}
                                {{--                                <a href="#pharma_accountTermsForm" id="pharma_personal_info_check_account"--}}
                                {{--                                   class="arrow-down scroll"--}}
                                {{--                                   style="position: fixed;bottom: unset;cursor: pointer;display: none">&#xf103;</a>--}}
                                <a
                                    class="button-shortcodes text-size-1 text-padding-1 version-1 scroll"
                                    href="#pharma_accountTermsForm" id="pharma_personal_info_check_account"
                                    style="position:fixed;left:50%;margin-left: -65px;display: none"><span>&#xf18e;</span>
                                    Next
                                </a>
                                {{--                                <a href="#pharma_agree_to_term_info" id="pharma_personal_info_check_agree"--}}
                                {{--                                   class="arrow-down scroll"--}}
                                {{--                                   style="position: fixed;bottom: unset;cursor: pointer;display: none">&#xf103;</a>--}}
                                <a
                                    class="button-shortcodes text-size-1 text-padding-1 version-1 scroll"
                                    href="#pharma_agree_to_term_info" id="pharma_personal_info_check_agree"
                                    style="position:fixed;left:50%;margin-left: -65px;display:none"><span>&#xf18e;</span>
                                    Next
                                </a>
                            </div>
                            <div id="pharma_accountTermsForm" style="display: none">
                                <br>

                                <h4 style="text-align: left;">Credit Application</h4>
                                <br>
                                <label for="credit_application"> I/We would like to apply for a credit
                                    account: </label>


                                <input type="radio" name="pharma_credit_application" id="pharma_credit_application_yes"
                                       value="1"
                                       style="width:unset;-webkit-appearance: checkbox; margin-left: 10px; margin-top: 10px; font-size: x-large;"
                                       onClick="pharma_creditFormAppear()"> &nbspYes
                                <br>
                                <input type="radio" name="pharma_credit_application" id="pharma_credit_application_no"
                                       value="0"
                                       style="width:unset;-webkit-appearance: checkbox;  margin-left: 10px; margin-top: 10px; margin-bottom: 10px;font-size: x-large;"
                                       onClick="pharma_creditFormDisappear()" checked> &nbspNo
                                <div id="pharma_creditForm" style="display: none;margin-left: 10%;">

                                    <label for="pharma_is_register_owner"> Are you the registered owner of the
                                        business? </label>

                                    <input type="radio" name="pharma_is_register_owner"
                                           id="pharma_is_register_owner_yes" value="1"
                                           style="width:unset;-webkit-appearance: checkbox; margin-left: 10px; margin-top: 10px; font-size: x-large;">
                                    &nbspYes
                                    <br>
                                    <input type="radio" name="pharma_is_register_owner" id="pharma_is_register_owner_no"
                                           value="0"
                                           style="width:unset;-webkit-appearance: checkbox;  margin-left: 10px; margin-top: 10px; margin-bottom: 10px;font-size: x-large;">
                                    &nbspNo

                                    <label for="pharma_is_individuals_declare" class="uname"> Have any of the
                                        individuals above
                                        been declared
                                        Bankrupt? </label>

                                    <input type="radio" name="pharma_is_individuals_declare"
                                           id="pharma_is_individuals_declare_yes"
                                           value="1"
                                           style="width:unset;-webkit-appearance: checkbox; margin-left: 10px; margin-top: 10px; font-size: x-large;">
                                    &nbspYes
                                    <br>
                                    <input type="radio" name="pharma_is_individuals_declare"
                                           id="pharma_is_individuals_declare_no"
                                           value="0"
                                           style="width:unset;-webkit-appearance: checkbox;  margin-left: 10px; margin-top: 10px; margin-bottom: 10px;font-size: x-large;">
                                    &nbspNo
                                    <label for="pharma_is_ever_refuse"> Has the business ever been refused
                                        credit? </label>

                                    <input type="radio" name="pharma_is_ever_refuse" id="pharma_is_ever_refuse_yes"
                                           value="1"
                                           style="width:unset;-webkit-appearance: checkbox; margin-left: 10px; margin-top: 10px; font-size: x-large;">
                                    &nbspYes
                                    <br>
                                    <input type="radio" name="pharma_is_ever_refuse" id="pharma_is_ever_refuse_no"
                                           value="0"
                                           style="width:unset;-webkit-appearance: checkbox;  margin-left: 10px; margin-top: 10px; margin-bottom: 10px;font-size: x-large;">
                                    &nbspNo
                                    {{--                                <div class="sixteen columns">--}}
                                    <div class="alert alert-blue big-alerts">
                                        <p style="text-transform: unset;"><strong>Credit Application
                                                Declaration</strong></br>I/We hereby apply for credit with Medlab
                                            Clinical
                                            Limited and certify that the information supplied in this application is
                                            true
                                            and correct.
                                            I/We authorise Medlab Clinical Limited to conduct a Credit History Check
                                            with an
                                            external Credit Agency. I/We understand that all information will be treated
                                            as
                                            confidential and will not be supplied to any other organisation.</p>
                                    </div>
                                    {{--                                </div>--}}
                                </div>
                                {{--                                <a href="#pharma_agree_to_term_info" id="pharma_account_term_info_check"--}}
                                {{--                                   class="arrow-down scroll"--}}
                                {{--                                   style="position: fixed;bottom: unset;cursor: pointer;display: none">&#xf103;</a>--}}
                                <a
                                    class="button-shortcodes text-size-1 text-padding-1 version-1 scroll"
                                    href="#pharma_agree_to_term_info" id="pharma_account_term_info_check"
                                    style="position:fixed;left:50%;margin-left: -65px;display:none"><span>&#xf18e;</span>
                                    Next
                                </a>
                            </div>
                            <div id="pharma_agree_to_term_info" style="display: none">
                                <div class="row">
                                    <label for="pharma_password" class="required"> Your password</label>
                                    <input id="pharma_password" type="password"
                                           class="form-control check_manually validate_password"
                                           name="pharma_password"
                                           autocomplete="googleignoreautofill" style="text-transform: unset;">
                                    <small class="alert alert-red"> </small>
                                </div>
                                <div class="alert alert-blue">
                                    <p style="text-transform: unset;">Password needs to be 8 or more characters</p>
                                </div>
                                <br>
                                <div class="row">
                                    <label for="pharma_password-confirm" class="required"> Confirm your
                                        password </label>
                                    <input id="pharma_password-confirm" type="password"
                                           class="form-control check_manually"
                                           name="pharma_password_confirmation"
                                           autocomplete="googleignoreautofill" style="text-transform: unset;">
                                    <small class="alert alert-red"> </small>
                                </div>
                                <br>
                                <h4 style="text-align: left;">Terms For Use of Website</h4>
                                <br>
                                <div class="row">
                                    <div class="alert alert-blue big-alerts">
                                        <p style="text-transform: unset;">I / We have read and agree to the Terms and
                                            Conditions, Terms of Sale and the Medlab Privacy Policy displayed on the
                                            Medlab
                                            website. I / We agree to comply with and be bound by these policy documents
                                            (which may be amended or updated from time to time).
                                            <br>Should you have any enquires on Medlab Clinical Pty Ltd Terms and
                                            Conditions, Terms of Sale or Privacy Policy please contact: hello@medlab.co
                                            or
                                            call 1300 369 570.<br>By clicking on the the submit, you Agree to the Terms
                                            and
                                            Conditions, Terms of Sale & Medlab's Privacy Policy.</p>
                                    </div>
                                    <input type="checkbox" name="pharma_list_in_web" id="pharma_list_in_web"
                                           class="pharma-checkbox"
                                           value="1"
                                           style="width:unset;-webkit-appearance: checkbox; margin-left: 10px; margin-top: 10px; font-size: x-large;"
                                           checked>
                                    <label for="pharma_list_in_web" style="display: inline">List our
                                        Business in Medlab’s Find a Practitioner section of the Website.</label>
                                    <br>
                                    <input type="checkbox" name="pharma_newsletter" id="pharma_newsletter"
                                           class="pharma-checkbox"
                                           value="1"
                                           style="width:unset;-webkit-appearance: checkbox; margin-left: 10px; margin-top: 10px; font-size: x-large;"
                                           checked>
                                    <label for="pharma_newsletter" style="display: inline">Signup for Medlab
                                        Newsletter</label>
                                    <br>
                                    <input type="checkbox" name="pharma_agree_term" id="pharma_agree_term"
                                           class="check_manually pharma-checkbox"
                                           value="1"
                                           style="width:unset;-webkit-appearance: checkbox; margin-left: 10px; margin-top: 10px; font-size: x-large;">

                                    <label for="pharma_agree_term" style="display: inline">
                                        Please check to
                                        agree to our <a
                                            href='{{route('privacy_policy')}}'
                                            style="color:#6ba53a">Privacy Policy</a>
                                        and <a
                                            href='{{route('sales_policy')}}'
                                            style="color:#6ba53a">Sales Policy</a> and
                                        <a href='{{route('returns')}}'
                                           style="color:#6ba53a">Returns Policy</a>. </label>
                                    <small id="er_pharma_agree_term" class="alert alert-red"> </small>
                                    <br>
                                    <div id="business_is_defined_area" style="display: none">
                                        <input type="checkbox" name="pharma_business_certified"
                                               id="pharma_business_certified"
                                               value="1"
                                               style="width:unset;-webkit-appearance: checkbox; margin-left: 10px; margin-top: 10px; font-size: x-large;"
                                               class="pharma-checkbox"
                                        >
                                        <label for="pharma_business_certified" style="display: inline">The Business as
                                            defined above is a certified registered Pharmacy as defined by your
                                            governing body</label>
                                    </div>
                                </div>
                                <div id="button-con">
                                    <button id="pharma_submit" class="send_message" type="submit">submit
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div id="practitionerForm" style="display: none">
                            <div class="row">
                                <p>Are you from Australia?</p>
                                <input type="radio" name="if_from_au" id="if_from_au_yes"
                                       onclick="showAuDoc()"
                                       style="width:unset;-webkit-appearance: checkbox; margin-left: 10px; margin-top: 10px; font-size: x-large;"
                                       value="1">
                                <label for="if_from_au_yes" style="display: inline-block"> &nbspYes</label>
                                <br>
                                <input type="radio" name="if_from_au" id="if_from_au_no"
                                       onclick="showInterDoc()"
                                       style="width:unset;-webkit-appearance: checkbox;  margin-left: 10px; margin-top: 10px; margin-bottom: 10px;font-size: x-large;"
                                       value="0">
                                <label for="if_from_au_no" style="display: inline-block"> &nbspNo</label>
                            </div>

                            <div id="practitioner_personal_info" style="display: none;">
                                <br>
                                <h4 style="text-align: left;">Doctor Information</h4>
                                <br>

                                <div class="row" id="practitioner_ahpra_area">
                                    <label for="practitioner_ahpra_number" class="required">
                                        AHPRA Number</label>
                                    <input id="practitioner_ahpra_number" type="text"
                                           class="form-control"
                                           name="practitioner_ahpra_number"
                                           style="text-transform: unset;width: 49%;" required>

                                    <small class="alert alert-red"></small>
                                </div>

                                <div class="row">
                                    <label for="practitioner_email" class="required"> Your email</label>
                                    <input id="practitioner_email" type="email"
                                           class="form-control"
                                           name="practitioner_email" required
                                           autocomplete="googleignoreautofill" style="text-transform: unset;"
                                           onFocus="stopAutoFillPractitioner()">

                                    <small class="alert alert-red"></small>
                                </div>
                                <div class="row">
                                    <label for="practitioner_title" class="required">
                                        Title</label>
                                    <select id="practitioner_title" type="text"
                                            class="form-control"
                                            name="practitioner_title" required
                                            style="text-transform: unset;width: 49%;">
                                        <option>Mr</option>
                                        <option>Ms</option>
                                        <option>Miss</option>
                                        <option>Dr.</option>
                                        <option>Prof.</option>
                                    </select>
                                    <small class="alert alert-red"></small>
                                </div>
                                <div class="row">
                                    <label for="practitioner_firstname" class="required"> First Name</label>
                                    <input id="practitioner_firstname" type="text"
                                           class="form-control"
                                           name="practitioner_firstname" required
                                           style="text-transform: unset;" onFocus="stopAutoFillPractitioner()"
                                           autocomplete="googleignoreautofill">
                                    <small class="alert alert-red"></small>
                                </div>
                                <div class="row">
                                    <label for="practitioner_lastname" class="required"> Last Name</label>
                                    <input id="practitioner_lastname" type="text"
                                           class="form-control"
                                           name="practitioner_lastname" required
                                           style="text-transform: unset;" autocomplete="googleignoreautofill">
                                    <small class="alert alert-red"></small>
                                </div>

                                <div class="grey-section padding">
                                    <h4 style="text-align: left" class="required">Your Direct Number (Enter at least one
                                        number)</h4>
                                    <div class="row">
                                        <small class="alert alert-red"></small>
                                        <label for="practitioner_phone" style="width: 100%;">
                                            Landline</label>
                                        <input id="practitioner_phone" type="number" data-status="true"
                                               class="form-control"
                                               name="practitioner_phone"
                                               style="text-transform: unset;" onFocus="stopAutoFillPractitioner()"
                                               autocomplete="googleignoreautofill">

                                    </div>
                                    <div class="row">
                                        <label for="practitioner_mobile" style="width: 100%;"> Mobile
                                            Phone</label>
                                        <input id="practitioner_mobile" type="number" data-status="true"
                                               class="form-control"
                                               name="practitioner_mobile"
                                               style="text-transform: unset;" onFocus="stopAutoFillPractitioner()"
                                               autocomplete="googleignoreautofill">
                                        <small class="alert alert-red"></small>
                                    </div>
                                </div>
                                {{--                                <div class="row">--}}
                                {{--                                    <label for="primary_profession" class="required"--}}
                                {{--                                           style="width: 100%"> Primary--}}
                                {{--                                        Profession</label>--}}
                                {{--                                    <select id="primary_profession" type="text"--}}
                                {{--                                            class="form-control"--}}
                                {{--                                            name="primary_profession"--}}
                                {{--                                            style="text-transform: unset;width: 100%;" required>--}}
                                {{--                                        <option disabled selected value> -- select an option --</option>--}}
                                {{--                                        @foreach($modality as $item)--}}
                                {{--                                            <option value="{{ $item->name }}">{{ $item->name }}</option>--}}
                                {{--                                        @endforeach--}}
                                {{--                                    </select>--}}
                                <input name="primary_profession" id="primary_profession" value="Doctor" hidden/>
                                {{--                                </div>--}}
                                <div class="row">
                                    <label for="job_title" class="required"
                                           style="width: 100%"> Job Title</label>
                                    <input id="job_title" type="text"
                                           class="form-control"
                                           name="job_title"
                                           style="text-transform: unset;" required>
                                    <small class="alert alert-red"></small>
                                </div>
                                <div id="association_area" style="display: none">
                                    <div class="row">
                                        <label for="association_name" class="required"
                                               style="width: 100%"> Association
                                            Name</label>
                                        <select id="association_name" type="text"
                                                class="form-control"
                                                name="association_name"
                                                style="text-transform: unset;">
                                            <option disabled selected value> -- select an option --</option>
                                            @foreach($association as $item)
                                                <option value="{{ $item->name }}">{{ $item->name }}</option>
                                            @endforeach
                                        </select>
                                        <small class="alert alert-red"></small>

                                    </div>
                                    <div class="row">

                                        <label for="association_number" class="required"
                                               style="width: 100%"> Association Number</label>
                                        <input id="association_number" type="number"
                                               class="form-control"
                                               name="association_number"
                                               style="text-transform: unset;">
                                        <small class="alert alert-red"></small>

                                    </div>

                                    <div class="row">
                                        <label for="practitioner_qualification" class="required"
                                               style="width: 100%">Please assist us in the approval process by uploading
                                            a scanned copy of Certification/Association relevant to your modality<br>(Only
                                            accept .pdf .jpg .png .jpeg file (20 MB Max))</label>
                                        <input id="practitioner_qualification" type="file"
                                               class="form-control"
                                               name="practitioner_qualification"
                                               style="text-transform: unset;">
                                        <small class="alert alert-red"></small>
                                    </div>
                                </div>
                                {{--                                <div class="alert alert-blue">--}}
                                {{--                                    <p style="text-transform: unset;">AHPRA Number required for SAS Submissions</p>--}}
                                {{--                                </div>--}}
                                {{--                                <a href="#practitionerBusinessInfo" id="personal_info_check" class="arrow-down scroll"--}}
                                {{--                                   style="position: fixed;bottom: unset;cursor: pointer;">&#xf103;</a>--}}
                                <a
                                    class="button-shortcodes text-size-1 text-padding-1 version-1 scroll"
                                    href="#practitionerBusinessInfo" id="personal_info_check"
                                    style="position:fixed;left:50%;margin-left: -65px;"><span>&#xf18e;</span>
                                    Next
                                </a>
                                {{--                                <a href="#agree_to_term_info" id="personal_info_check_agree" class="arrow-down scroll"--}}
                                {{--                                   style="position: fixed;bottom: unset;cursor: pointer;display: none">&#xf103;</a>--}}
                            </div>


                            <div class="row alert alert-blue" id="practitioner_company_name_display_area"
                                 style="display: none">

                                <label for="practitioner_company_name_display"> Business Name</label>
                                <input id="practitioner_company_name_display" type="text"
                                       name="practitioner_company_name_display"
                                       class="form-control"
                                       style="text-transform: unset;border: unset;"
                                       readonly>
                            </div>
                            <div id="practitionerBusinessInfo" style="display: none;">

                                <br>
                                <h4 style="text-align: left;">Your Business Details</h4>
                                <br>
                                {{--                                <br>--}}
                                {{--                                <h4 style="text-align: left;">Business Address</h4>--}}
                                {{--                                <br>--}}
                                <div class="row" id="practitioner_choose_inter_area">

                                    <label for="practitioner_international" style="display:inline-block;">
                                        Show International</label>
                                    <input id="practitioner_international" type="checkbox"
                                           class="form-control"
                                           name="practitioner_international" style="display:inline-block;width: unset"/>

                                </div>


                                <div class="row">

                                    <label for="practitioner_street_address" class="required"
                                           style="width: 100%;">
                                        Street Address</label>
                                    <input id="practitioner_street_address" type="text"
                                           class="form-control"
                                           name="practitioner_street_address" required
                                           autocomplete="googleignoreautofill"/>
                                    <small class="alert alert-red"> </small>

                                </div>
                                <div class="row">

                                    <label for="practitioner_second_address" style="width: 100%;">
                                        A second line if you need (optional)</label>
                                    <input id="practitioner_second_address" type="text"
                                           class="form-control"
                                           name="practitioner_second_address" autocomplete="googleignoreautofill"/>
                                </div>
                                <div class="row" style="margin-bottom: unset;">
                                    <label for="practitioner_locality" class="required"
                                           style="width: 33.3%;float: left">Suburb/Postcode</label>
                                    <label for="practitioner_administrative_area_level_1"
                                           style="width: 33.3%;float: left">
                                        &nbsp;</label>
                                    <label for="practitioner_postal_code"
                                           style="width: 33.3%;float: left">
                                        &nbsp;</label>
                                </div>
                                <div class="row" style="position:relative" id="practitioner_locality_row">

                                    <input id="practitioner_locality" type="text"
                                           class="form-control"
                                           name="practitioner_locality"
                                           style="width:100%;float: left;position: absolute" required/>
                                    <input id="practitioner_administrative_area_level_1" type="text"
                                           class="form-control"
                                           name="practitioner_administrative_area_level_1"
                                           autocomplete="googleignoreautofill"
                                           style="width: 33.3%;float: left;margin-left: 33.3%" readonly>
                                    <input id="practitioner_postal_code" type="text"
                                           class="form-control"
                                           name="practitioner_postal_code" style="width: 33.3%;float: left"
                                           autocomplete="googleignoreautofill" readonly>
                                    <small class="alert alert-red"> </small>

                                </div>


                                <div class="row">

                                    <label for="practitioner_country" style="width: 100%;">
                                        Country</label>
                                    <input id="practitioner_country" type="text"
                                           class="form-control"
                                           name="practitioner_country" readonly autocomplete="googleignoreautofill"
                                           style="border: unset;"/>
                                </div>
                                <input id="practitioner_lat" name="practitioner_lat" hidden/>
                                <input id="practitioner_lng" name="practitioner_lng" hidden/>

                                <div id="practitioner_search_business_area" style="display: none">
                                    <div id="practitioner_search_box">
                                        <br>
                                        <h4 style="text-align: left;">Search for a Business Name, ABN or ACN of where
                                            you work</h4>
                                        <br>
                                        <div class="row">
                                            <input list="practitioner_business_list" id="practitioner_business_search"
                                                   class="business-list"
                                                   style="text-transform: unset;width: 73%;float:left;border: 1px solid #313131;line-height:32px;padding-bottom: unset;"/>
                                            {{--                                            <div--}}
                                            {{--                                                style="margin-left: 15px;padding: 8px 18px;background: #7AA43F;border: unset;"--}}
                                            {{--                                                class="button-shortcodes text-size-1 text-padding-1 version-1"--}}
                                            {{--                                                onClick="sendPractitionerBusinessList()"><span--}}
                                            {{--                                                    style="font-size: x-large;padding: unset">&#xf002;</span>--}}
                                            {{--                                            </div>--}}
                                            <datalist id="practitioner_business_list"></datalist>
                                        </div>
                                        {{--                                        <a href="#practitioner_business_search" id="practitioner_search_business_button"--}}
                                        {{--                                           onClick="sendPractitionerBusinessList()" class="arrow-down scroll"--}}
                                        {{--                                           style="position: fixed;bottom: unset;cursor: pointer;">&#xf103;</a>--}}
                                        <a
                                            class="button-shortcodes text-size-1 text-padding-1 version-1 scroll"
                                            href="#practitioner_business_search"
                                            id="practitioner_search_business_button"
                                            onClick="sendPractitionerBusinessList()"
                                            style="position:fixed;left:50%;margin-left: -65px;"><span>&#xf18e;</span>
                                            Next
                                        </a>
                                    </div>


                                    <div class="alert alert-blue" style="display: none;"
                                         id="practitioner_business_detail">
                                        <div class="row">
                                            <label for="business_name" class="required">
                                                Legal Entity Name</label>
                                            {{--                                            <label for="business_type" id="business_type_label"--}}
                                            {{--                                                   style="width: 49%;float:right;"> Business--}}
                                            {{--                                                Type</label>--}}
                                            <textarea id="business_name" type="text"
                                                      class="form-control"
                                                      name="business_name"
                                                      style="text-transform: unset;border: unset;overflow:hidden;font-family:'Lato', sans-serif;"
                                                      readonly required></textarea>
                                            <input id="business_type" type="text"
                                                   class="form-control"
                                                   name="business_type"
                                                   style="text-transform: unset;width: 49%;float:right; border: unset;"
                                                   readonly hidden>
                                            <small class="alert alert-red"> </small>
                                        </div>
                                        <div class="row">
                                            <label for="business_number"
                                                   style="width: 50%;float:left;margin-right: 1%"
                                                   id="business_number_label"> Business
                                                Number (ABN/NZBN)
                                            </label>
                                            <label for="acn" style="width: 49%;float:right;"
                                                   id="acn_label">Company Number
                                                (ACN)</label>
                                            <input id="business_number" type="number"
                                                   class="form-control"
                                                   name="business_number"
                                                   style="text-transform: unset;width: 49%;float:left; border: unset;"
                                                   readonly>
                                            <input id="acn" type="number"
                                                   class="form-control"
                                                   name="acn"
                                                   style="text-transform: unset;width: 49%;float:right; border: unset;"
                                                   readonly>
                                        </div>
                                    </div>
                                    <div id="practitioner_business_sub_detail" style="display: none">


                                        <div class="row">
                                            <label for="business_trading_name" class="required"
                                                   style="width: 100%">
                                                Business Name</label>
                                            <div class="alert alert-yelow">
                                                <p>Please enter your Human friendly business name. Example Big Mans
                                                    Chemist Chatswood</p>
                                            </div>
                                            <textarea id="business_trading_name" type="text"
                                                      class="form-control"
                                                      name="business_trading_name"
                                                      style="text-transform: unset;overflow:hidden;font-family:'Lato', sans-serif;"
                                                      required
                                                      autocomplete="googleignoreautofill"></textarea>

                                            <small class="alert alert-red"> </small>


                                        </div>
                                        <div class="row">
                                            <label for="primary_channel" class="required"
                                                   style="width: 100%">Business
                                                Primary Channel</label>
                                            <select id="primary_channel" type="text"
                                                    class="form-control"
                                                    name="primary_channel"
                                                    style="text-transform: unset;width: 100%;" required>
                                                <option disabled selected value> -- select an option --</option>
                                                @foreach($primary_channel as $item)
                                                    <option value="{{ $item->name }}">{{ $item->name }}</option>
                                                @endforeach
                                            </select>

                                            <small class="alert alert-red"> </small>


                                        </div>
                                        <div class="row" id="second_channel_area" style="display:none;">

                                            <label for="second_channel" class="required"
                                                   style="width: 100%"> Business Second Channel</label>
                                            <select id="second_channel" type="text"
                                                    class="form-control"
                                                    name="second_channel"
                                                    style="text-transform: unset;width: 100%;" required>
                                                <option disabled selected value> -- select an option --</option>
                                            </select>
                                            <small id="er_second_channel" class="alert alert-red"> </small>

                                        </div>

                                        <div class="row">
                                            <label for="business_phone" class="required" style="width: 100%">
                                                Landline</label>

                                            <input id="business_phone" type="number" data-status="true"
                                                   class="form-control"
                                                   name="business_phone"
                                                   style="text-transform: unset;" required
                                                   autocomplete="googleignoreautofill">

                                            <small class="alert alert-red"> </small>


                                        </div>
                                        {{--                                        <div class="row">--}}

                                        {{--                                            <label for="fax" style="width: 100%"> Fax</label>--}}

                                        {{--                                            <input id="fax" type="number"--}}
                                        {{--                                                   class="form-control"--}}
                                        {{--                                                   name="fax"--}}
                                        {{--                                                   style="text-transform: unset;" autocomplete="googleignoreautofill">--}}

                                        {{--                                        </div>--}}
                                    </div>
                                </div>

                                {{--                                <a href="#agree_to_term_info" id="business_info_check" class="arrow-down scroll"--}}
                                {{--                                   style="position: fixed;bottom: unset;cursor: pointer;display:none;">&#xf103;</a>--}}
                                <a
                                    class="button-shortcodes text-size-1 text-padding-1 version-1 scroll"
                                    href="#agree_to_term_info" id="business_info_check"
                                    style="position:fixed;left:50%;margin-left: -65px;"><span>&#xf18e;</span>
                                    Next
                                </a>
                            </div>


                            <div id="agree_to_term_info" style="display: none">
                                <div class="row">
                                    <label for="practitioner_password" class="required"> Your password</label>
                                    <input id="practitioner_password" type="password"
                                           class="form-control check_manually validate_password"
                                           name="practitioner_password"
                                           autocomplete="googleignoreautofill" style="text-transform: unset;"
                                           onFocus="stopAutoFillPractitioner()">
                                    <small class="alert alert-red"> </small>

                                </div>
                                <div class="alert alert-blue">
                                    <p style="text-transform: unset;">Password needs to be 8 or more characters</p>
                                </div>
                                <br>
                                <div class="row">
                                    <label for="practitioner_password-confirm" class="required"> Confirm your
                                        password </label>
                                    <input id="practitioner_password-confirm" type="password"
                                           class="form-control check_manually"
                                           name="practitioner_password_confirmation"
                                           autocomplete="googleignoreautofill" style="text-transform: unset;"
                                           onFocus="stopAutoFillPractitioner()">

                                    <small class="alert alert-red"> </small>
                                </div>
                                <br>
                                <h4 style="text-align: left;">Terms For Use of Website</h4>
                                <br>
                                <div class="row">

                                    <div class="alert alert-blue big-alerts">
                                        <p style="text-transform: unset;">I / We have read and agree to the Terms and
                                            Conditions, Terms of Sale and the Medlab Privacy Policy displayed on the
                                            Medlab
                                            website. I / We agree to comply with and be bound by these policy documents
                                            (which may be amended or updated from time to time).
                                            <br>Should you have any enquires on Medlab Clinical Pty Ltd Terms and
                                            Conditions, Terms of Sale or Privacy Policy please contact: hello@medlab.co
                                            or
                                            call 1300 369 570.<br>By clicking on the the submit, you Agree to the Terms
                                            and
                                            Conditions, Terms of Sale & Medlab's Privacy Policy.</p>
                                    </div>
                                    <input type="checkbox" name="practitioner_list_in_web" id="practitioner_list_in_web"
                                           class="practitioner-checkbox"
                                           value="1"
                                           style="width:unset;-webkit-appearance: checkbox; margin-left: 10px; margin-top: 10px; font-size: x-large;"
                                           checked>
                                    <label for="practitioner_list_in_web" style="display: inline">List our
                                        Business in Medlab’s Find a Practitioner section of the Website.</label>
                                    <br>
                                    <input type="checkbox" name="practitioner_newsletter" id="practitioner_newsletter"
                                           class="practitioner-checkbox"
                                           value="1"
                                           style="width:unset;-webkit-appearance: checkbox; margin-left: 10px; margin-top: 10px; font-size: x-large;"
                                           checked>
                                    <label for="practitioner_newsletter" style="display: inline">Signup for Medlab
                                        Newsletter</label>
                                    <br>
                                    <input type="checkbox" class="check_manually practitioner-checkbox"
                                           name="practitioner_agree_term"
                                           id="practitioner_agree_term"
                                           value="1"
                                           style="width:unset;-webkit-appearance: checkbox; margin-left: 10px; margin-top: 10px; font-size: x-large;"
                                    >
                                    <label for="practitioner_agree_term" style="display: inline">
                                        &nbspPlease check to
                                        agree to our <a
                                            href='{{route('privacy_policy')}}'
                                            style="color:#6ba53a">Privacy Policy</a>
                                        and <a
                                            href='{{route('sales_policy')}}'
                                            style="color:#6ba53a">Sales Policy</a> and
                                        <a href='{{route('returns')}}'
                                           style="color:#6ba53a">Returns Policy</a>. </label>

                                    <small id="er_practitioner_agree_term" class="alert alert-red"> </small>

                                </div>
                                <div id="button-con">
                                    <button id="practitioner_submit" class="send_message" type="submit">submit
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div id="patientForm" style="display: none">
                            <br>
                            <h4 style="text-align: left;">Address</h4>
                            <br>
                            <div class="row">

                                <label for="patient_international" style="display:inline-block;">
                                    Show International</label>
                                <input id="patient_international" type="checkbox"
                                       class="form-control"
                                       name="patient_international" style="display:inline-block;width: unset"/>

                            </div>
                            <div class="row">

                                <label for="patient_street_address" class="required" style="width: 100%;">
                                    Street Address</label>
                                <input id="patient_street_address" type="text"
                                       class="form-control"
                                       name="patient_street_address" onFocus="stopAutoFillPatient()" required/>

                            </div>
                            <div class="row">

                                <label for="patient_second_address" style="width: 100%;">
                                    A second line if you need (optional)</label>
                                <input id="patient_second_address" type="text"
                                       class="form-control"
                                       name="patient_second_address" autocomplete="googleignoreautofill"/>
                            </div>
                            <div class="row" style="margin-bottom: unset;">
                                <label for="patient_locality" class="required" style="width: 33.3%;float: left">Suburb/Postcode</label>
                                <label for="patient_administrative_area_level_1"
                                       style="width: 33.3%;float: left">
                                    &nbsp;</label>
                                <label for="patient_postal_code" style="width: 33.3%;float: left">
                                    &nbsp;</label>
                            </div>
                            <div class="row" style="position:relative" id="patient_locality_row">

                                <input id="patient_locality" type="text"
                                       class="form-control"
                                       name="patient_locality" style="width:100%;float: left;position: absolute"
                                       required/>

                                <input id="patient_administrative_area_level_1" type="text"
                                       class="form-control"
                                       name="patient_administrative_area_level_1" autocomplete="googleignoreautofill"
                                       style="width: 33.3%;float: left;margin-left: 33.3%" readonly>

                                <input id="patient_postal_code" type="text"
                                       class="form-control"
                                       name="patient_postal_code" style="width: 33.3%;float: left"
                                       autocomplete="googleignoreautofill" readonly>

                            </div>
                            <div class="row">

                                <label for="patient_country" style="width: 100%;">
                                    Country</label>
                                <input id="patient_country" type="text"
                                       class="form-control"
                                       name="patient_country" autocomplete="googleignoreautofill" readonly/>
                            </div>
                            <input id="patient_lat" name="patient_lat" hidden/>
                            <input id="patient_lng" name="patient_lng" hidden/>
                            <div class="row">
                                <label for="patient_phone" style="width: 100%;">
                                    Landline</label>

                                <input id="patient_phone" type="number"
                                       class="form-control"
                                       name="patient_phone"
                                       style="text-transform: unset;" onFocus="stopAutoFillPatient()"
                                       autocomplete="googleignoreautofill">


                            </div>
                            <div class="row">
                                <label for="patient_mobile" style="width: 100%;"> Mobile
                                    Phone</label>
                                <input id="patient_mobile" type="number"
                                       class="form-control"
                                       name="patient_mobile"
                                       style="text-transform: unset;" autocomplete="googleignoreautofill">
                            </div>
                            <div class="row" id="referral_code_row">
                                <label for="referral_code" style="width: 100%;float:left;"> Referral Code as provided by
                                    Referring Practitioner (Optional)</label>
                                <input id="referral_code" type="text"
                                       class="form-control"
                                       name="referral_code"
                                       style="text-transform: unset;width: 49%;float:left;margin-bottom:10px;">
                            </div>
                            <div class="row">
                                <label for="patient_title" class="required"
                                >
                                    Title</label>
                                <select id="patient_title" type="text"
                                        class="form-control"
                                        name="patient_title" required
                                        style="text-transform: unset;width: 49%;">
                                    <option></option>
                                    <option>Mr</option>
                                    <option>Ms</option>
                                    <option>Miss</option>
                                    <option>Dr.</option>
                                    <option>Prof.</option>
                                </select>
                            </div>
                            <div class="row">
                                <label for="patient_firstname" class="required"> First Name</label>
                                <input id="patient_firstname" type="text"
                                       class="form-control"
                                       name="patient_firstname" required
                                       style="text-transform: unset;" onFocus="stopAutoFillPatient()"
                                       autocomplete="googleignoreautofill">
                            </div>
                            <div class="row">
                                <label for="patient_lastname" class="required"> Last Name</label>

                                <input id="patient_lastname" type="text"
                                       class="form-control"
                                       name="patient_lastname" required
                                       style="text-transform: unset;" autocomplete="googleignoreautofill">
                            </div>
                            <div class="row">
                                <label for="patient_email" class="required"> Your email</label>
                                <input id="patient_email" type="email"
                                       class="form-control"
                                       name="patient_email" required
                                       autocomplete="googleignoreautofill" style="text-transform: unset;"
                                       onFocus="stopAutoFillPatient()">
                            </div>
                            <div class="row">
                                {{--                                <input name="email" id="email" type="text" style="text-transform: unset;" placeholder="E-Mail: *"/>--}}
                                <label for="patient_password" class="required"> Your password</label>
                                <input id="patient_password" type="password"
                                       class="form-control"
                                       name="patient_password" required
                                       autocomplete="googleignoreautofill" style="text-transform: unset;">
                            </div>
                            <div class="alert alert-blue">
                                <p style="text-transform: unset;">Password needs to be 8 or more characters</p>
                            </div>
                            <div class="row">
                                <label for="patient_password-confirm" class="required"> Confirm your
                                    password </label>
                                <input id="patient_password-confirm" type="password"
                                       class="form-control"
                                       name="patient_password_confirmation" required
                                       autocomplete="googleignoreautofill" style="text-transform: unset;">
                            </div>

                            <br>
                            <h4 style="text-align: left;">Terms For Use of Website</h4>
                            <br>
                            <div class="row">


                                <div class="alert alert-blue big-alerts">
                                    <p style="text-transform: unset;">I / We have read and agree to the Terms and
                                        Conditions, Terms of Sale and the Medlab Privacy Policy displayed on the Medlab
                                        website. I / We agree to comply with and be bound by these policy documents
                                        (which may be amended or updated from time to time).
                                        <br>Should you have any enquires on Medlab Clinical Pty Ltd Terms and
                                        Conditions, Terms of Sale or Privacy Policy please contact: hello@medlab.co or
                                        call 1300 369 570.<br>By clicking on the the submit, you Agree to the Terms and
                                        Conditions, Terms of Sale & Medlab's Privacy Policy.</p>
                                </div>
                                <input type="checkbox" name="patient_newsletter" id="patient_newsletter"
                                       value="1"
                                       style="width:unset;-webkit-appearance: checkbox; margin-left: 10px; margin-top: 10px; font-size: x-large;"
                                       checked>
                                <label for="patient_newsletter" style="display: inline">Signup for Medlab
                                    Newsletter</label>
                                <br>
                                <input type="checkbox" name="patient_agree_term" id="patient_agree_term" value="1"
                                       style="width:unset;-webkit-appearance: checkbox; margin-left: 10px; margin-top: 10px; font-size: x-large;">
                                <label for="patient_agree_term" style="display: inline"> &nbspPlease check
                                    to
                                    agree to our <a
                                        href='{{route('privacy_policy')}}'
                                        style="color:#6ba53a">Privacy Policy</a>
                                    and <a
                                        href='{{route('sales_policy')}}'
                                        style="color:#6ba53a">Sales Policy</a> and
                                    <a href='{{route('returns')}}'
                                       style="color:#6ba53a">Returns Policy</a>. </label>

                            </div>
                            <div id="button-con">
                                <button class="send_message" type="submit" id="patient_submit">submit
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

        </section>
    </main>
    @include('vms.layouts.footer')

    <div class="scroll-to-top">&#xf106;</div>





    <!-- JAVASCRIPT
    ================================================== -->
    <script type="text/javascript" src="{{ asset('/build/js/intlTelInput.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/medlab.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/form-validate.js') }}"></script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            $(document).ready(function () {

                setHeight($('#pharma_business_trading_name'));
                setHeight($('#pharma_business_name'));
                setHeight($('#business_trading_name'));
                setHeight($('#business_name'));
                // document.getElementById('practitioner_email').removeAttribute('autocomplete');
                // document.getElementById('practitioner_email').setAttribute('autocomplete', 'googleignoreautofill');
                // document.getElementById('practitioner_password').removeAttribute('autocomplete');
                // document.getElementById('practitioner_password').setAttribute('autocomplete', 'googleignoreautofill');
                // document.getElementById('practitioner_password_confirmation').removeAttribute('autocomplete');
                // document.getElementById('practitioner_password_confirmation').setAttribute('autocomplete', 'googleignoreautofill');

            });
        })(jQuery);

    </script>
    <script language="javascript">
        function setHeight(jq_in) {
            jq_in.each(function (index, elem) {
                if (elem.scrollHeight < 34) {
                    elem.style.height = '34px';
                } else {
                    elem.style.height = elem.scrollHeight + 'px';
                }
            });
        }

        document.getElementById('business_info_check').onclick = function () {
            let allAreFilled = true;

            document.getElementById("practitionerBusinessInfo").querySelectorAll("[required]").forEach(function (i) {
                if (i.id == 'business_phone') {
                    if (i.value == '') {
                        setErrorFor(i.parentElement, errorMessage);
                        allAreFilled = false
                    } else if ($('#business_phone').attr('data-status') == 'false') {
                        allAreFilled = false
                    } else {
                        setSuccessFor(i.parentElement);
                    }
                } else {
                    if (i.value == '') {
                        setErrorFor(i, errorMessage);
                        allAreFilled = false
                    } else {
                        setSuccessFor(i);
                    }
                }

                // if (!allAreFilled) return;
                // if (!i.value) allAreFilled = false;
            })
            if (allAreFilled) {
                //     checkInputsB();
                // } else {
                document.getElementById('business_info_check').style.display = 'none';
                document.getElementById('agree_to_term_info').style.display = 'block';
                document.getElementById("practitioner_password").addEventListener('change', function () {
                    if (this.value.length < 8) {
                        document.getElementById('practitioner_password').value = "";
                        Swal.fire({
                            title: 'Password too weak',
                            text: "Password needs to be 8 or more characters",
                            icon: 'error',
                        })
                    } else {
                        Swal.fire({
                            title: 'Validating Password ..',
                            allowOutsideClick: false,
                            allowEscapeKey: false,
                            showConfirmButton: false,
                            showCloseButton: false,
                            showCancelButton: false,
                            onOpen: () => {
                                Swal.showLoading();
                                $.ajax({
                                    type: "POST",
                                    url: '{{ url('validate_password') }}',
                                    data: {
                                        _token: '{{ csrf_token() }}',
                                        password: this.value,
                                    },
                                    timeout: 6000,
                                    success: function (data) {
                                        if (data.number > 1000) {
                                            document.getElementById('practitioner_password').value = "";
                                            Swal.fire({
                                                icon: 'error',
                                                html: 'The password you have entered has been found ' + data.number + ' times in the public data breach registry. Passwords found over 1000 times cannot be accepted. Please choose a different password. For more information head to <a href="https://haveibeenpwned.com/Passwords" target="_blank" color="#6ba53a">https://haveibeenpwned.com/Passwords</a>.'
                                            })
                                        } else if (data.number > 100 && data.number <= 1000) {
                                            Swal.fire({
                                                icon: 'info',
                                                html: 'The password you have entered has been found ' + data.number + ' times in the public data breach registry. We recommend choosing a different password, but you can continue if you wish. For more information head to <a href="https://haveibeenpwned.com/Passwords" target="_blank" color="#6ba53a">https://haveibeenpwned.com/Passwords</a>.'
                                            })

                                        } else {
                                            Swal.close()
                                        }
                                    }

                                });
                            }
                        });
                    }

                });
                document.getElementById("practitionerForm").querySelectorAll("input:not(.practitioner-checkbox)").forEach(function (i) {
                    i.addEventListener('focusin', function () {
                        document.getElementById('practitioner_submit').style.display = 'none';
                    })
                })
                document.getElementById("practitionerForm").querySelectorAll("input:not(.practitioner-checkbox)").forEach(function (i) {
                    i.addEventListener('focusout', function () {
                        document.getElementById('practitioner_submit').style.display = 'initial';
                    })
                })
                // document.getElementById('er_business_trading_name').style.display = 'none';
                // document.getElementById('er_primary_channel').style.display = 'none';
                // document.getElementById('er_second_channel').style.display = 'none';
                // document.getElementById('er_business_phone').style.display = 'none';
                // console.log('test removing class sucess');
            }
        };
        document.getElementById("pharma_personal_info_check_agree").onclick = function () {

            let allAreFilled = true;
            document.getElementById("pharma_personal_info").querySelectorAll("[required]").forEach(function (i) {
                if (i.value == '') {
                    setErrorFor(i, errorMessage);
                    allAreFilled = false
                } else {
                    setSuccessFor(i);
                }
            })
            if (document.getElementById('pharma_phone').value == '' && document.getElementById('pharma_mobile').value == '') {
                allAreFilled = false;
                setErrorFor(document.getElementById('pharma_phone').parentElement, errorMessage);
            } else {
                setSuccessFor(document.getElementById('pharma_phone').parentElement);
            }
            console.log(allAreFilled)
            if (allAreFilled) {
                //     Swal.fire({
                //         icon: 'error',
                //         title: 'Please fill all the fields before next step',
                //     })
                // } else {
                document.getElementById("pharma_password").addEventListener('change', function () {
                    if (this.value.length < 8) {
                        document.getElementById('pharma_password').value = "";
                        Swal.fire({
                            title: 'Password too weak',
                            text: "Password needs to be 8 or more characters",
                            icon: 'error',
                        })
                    } else {
                        Swal.fire({
                            title: 'Validating Password ..',
                            allowOutsideClick: false,
                            allowEscapeKey: false,
                            showConfirmButton: false,
                            showCloseButton: false,
                            showCancelButton: false,
                            onOpen: () => {
                                Swal.showLoading();
                                $.ajax({
                                    type: "POST",
                                    url: '{{ url('validate_password') }}',
                                    data: {
                                        _token: '{{ csrf_token() }}',
                                        password: this.value,
                                    },
                                    timeout: 6000,
                                    success: function (data) {
                                        if (data.number > 1000) {
                                            document.getElementById('pharma_password').value = "";
                                            Swal.fire({
                                                icon: 'error',
                                                html: 'The password you have entered has been found ' + data.number + ' times in the public data breach registry. Passwords found over 1000 times cannot be accepted. Please choose a different password. For more information head to <a href="https://haveibeenpwned.com/Passwords" target="_blank" color="#6ba53a">https://haveibeenpwned.com/Passwords</a>.'
                                            })
                                        } else if (data.number > 100 && data.number <= 1000) {
                                            Swal.fire({
                                                icon: 'info',
                                                html: 'The password you have entered has been found ' + data.number + ' times in the public data breach registry. We recommend choosing a different password, but you can continue if you wish. For more information head to <a href="https://haveibeenpwned.com/Passwords" target="_blank" color="#6ba53a">https://haveibeenpwned.com/Passwords</a>.'
                                            })

                                        } else {
                                            Swal.close()
                                        }
                                    }

                                });
                            }
                        });
                    }
                });
                document.getElementById('pharma_personal_info_check_agree').style.display = 'none';
                document.getElementById('pharma_agree_to_term_info').style.display = 'block';


            }
        };
        document.getElementById("pharma_account_term_info_check").onclick = function () {
            let allAreFilled = true;
            document.getElementById("pharma_accountTermsForm").querySelectorAll("[required]").forEach(function (i) {
                if (!allAreFilled) return;
                if (!i.value) allAreFilled = false;
            })
            if (!allAreFilled) {
                Swal.fire({
                    icon: 'error',
                    title: 'Please fill all the fields before next step',
                })
            } else {
                document.getElementById('pharma_account_term_info_check').style.display = 'none';
                document.getElementById('pharma_agree_to_term_info').style.display = 'block';
                document.getElementById('pharma_password').focus();
                document.getElementById("pharma_password").addEventListener('change', function () {
                    if (this.value.length < 8) {
                        document.getElementById('pharma_password').value = "";
                        Swal.fire({
                            title: 'Password too weak',
                            text: "Password needs to be 8 or more characters",
                            icon: 'error',
                        })
                    } else {
                        Swal.fire({
                            title: 'Validating Password ..',
                            allowOutsideClick: false,
                            allowEscapeKey: false,
                            showConfirmButton: false,
                            showCloseButton: false,
                            showCancelButton: false,
                            onOpen: () => {
                                Swal.showLoading();
                                $.ajax({
                                    type: "POST",
                                    url: '{{ url('validate_password') }}',
                                    data: {
                                        _token: '{{ csrf_token() }}',
                                        password: this.value,
                                    },
                                    timeout: 6000,
                                    success: function (data) {
                                        if (data.number > 1000) {
                                            document.getElementById('pharma_password').value = "";
                                            Swal.fire({
                                                icon: 'error',
                                                html: 'The password you have entered has been found ' + data.number + ' times in the public data breach registry. Passwords found over 1000 times cannot be accepted. Please choose a different password. For more information head to <a href="https://haveibeenpwned.com/Passwords" target="_blank" color="#6ba53a">https://haveibeenpwned.com/Passwords</a>.'
                                            })
                                        } else if (data.number > 100 && data.number <= 1000) {
                                            Swal.fire({
                                                icon: 'info',
                                                html: 'The password you have entered has been found ' + data.number + ' times in the public data breach registry. We recommend choosing a different password, but you can continue if you wish. For more information head to <a href="https://haveibeenpwned.com/Passwords" target="_blank" color="#6ba53a">https://haveibeenpwned.com/Passwords</a>.'
                                            })

                                        } else {
                                            Swal.close()
                                        }
                                    }

                                });
                            }
                        });
                    }
                });
                document.getElementById("pharmaForm").querySelectorAll("input:not(.pharma-checkbox)").forEach(function (i) {
                    i.addEventListener('focusin', function () {
                        document.getElementById('pharma_submit').style.display = 'none';
                    })
                })
                document.getElementById("pharmaForm").querySelectorAll("input:not(.pharma-checkbox").forEach(function (i) {
                    i.addEventListener('focusout', function () {
                        document.getElementById('pharma_submit').style.display = 'initial';
                    })
                })
            }
        };

        // async
        function extendForm1() {
            document.getElementById('pharma_business_detail').style.display = 'none';
            document.getElementById('practitionerForm').style.display = 'block';
            document.getElementById('patientForm').style.display = 'none';
            document.getElementById('pharmaForm').style.display = 'none';
            $("#patientForm input").val('');
            $("#patientForm select").val('');
            $("#patientForm input").prop('checked', false);
            $("#patientForm input").prop('required', false);
            $("#patientForm select").prop('required', false);
            $("#pharmaForm input").val('');
            $("#pharmaForm select").val('');
            $("#pharmaForm textarea").val('');
            $("#pharmaForm input").prop('checked', false);
            $("#pharmaForm input").prop('required', false);
            $("#pharmaForm textarea").prop('required', false);
            $("#pharmaForm select").prop('required', false);
            $("#practitioner_street_address").prop('required', true);
            $("#practitioner_locality").prop('required', true);
            $("#practitioner_title").prop('required', true);
            $("#practitioner_firstname").prop('required', true);
            $("#practitioner_lastname").prop('required', true);
            $("#practitioner_email").prop('required', true);

            $("#practitioner_password").prop('required', false);
            $("#practitioner_password-confirm").prop('required', false);

            $("#practitioner_agree_term").prop('required', false);
            $("#business_trading_name").prop('required', true);
            $("#creditForm input").prop('required', false);
            document.getElementById("practitioner_list_in_web").checked = true;
            document.getElementById("practitioner_newsletter").checked = true;

        }

        function extendForm2() {
            document.getElementById('pharma_business_detail').style.display = 'none';
            document.getElementById('practitionerForm').style.display = 'none';
            document.getElementById('patientForm').style.display = 'block';
            document.getElementById('pharmaForm').style.display = 'none';
            $("#practitionerForm input").val('');
            $("#practitionerForm select").val('');
            $("#practitionerForm textarea").val('');
            $("#practitionerForm input").prop('checked', false);
            $("#practitionerForm input").prop('required', false);
            $("#practitionerForm select").prop('required', false);
            $("#practitionerForm textarea").prop('required', false);
            $("#pharmaForm input").val('');
            $("#pharmaForm select").val('');
            $("#pharmaForm textarea").val('');
            $("#pharmaForm input").prop('checked', false);
            $("#pharmaForm input").prop('required', false);
            $("#pharmaForm select").prop('required', false);
            $("#pharmaForm textarea").prop('required', false);
            $("#patient_street_address").prop('required', true);
            $("#patient_locality").prop('required', true);
            $("#patient_title").prop('required', true);
            $("#patient_firstname").prop('required', true);
            $("#patient_lastname").prop('required', true);
            $("#patient_email").prop('required', true);
            $("#patient_password").prop('required', true);
            $("#patient_password-confirm").prop('required', true);
            $("#patient_agree_term").prop('required', true);
            document.getElementById("patient_newsletter").checked = true;
            document.getElementById('patient_email').addEventListener('change', function () {
                $.ajax({
                    type: "POST",
                    url: '{{ url('email_validation') }}',
                    data: {
                        _token: '{{ csrf_token() }}',
                        role: 'patient',
                        email: this.value,
                    },
                    timeout: 6000,
                    success: function (data) {
                        if (data.code == '0') {
                            document.getElementById('patient_email').value = "";
                            Swal.fire({
                                icon: 'info',
                                html: data.email + ' already exists. Please <a href="#" onClick="open_login()" color="#6ba53a">login</a> or <a href="/password/reset" color="#6ba53a">reset your password.</a>'
                            })
                        }
                    }

                });
            });
        }

        function extendForm3() {

            document.getElementById('pharmaForm').style.display = 'block';
            document.getElementById('patientForm').style.display = 'none';
            document.getElementById('practitionerForm').style.display = 'none';
            $("#patientForm input").val('');
            $("#patientForm select").val('');
            $("#patientForm input").prop('checked', false);
            $("#patientForm input").prop('required', false);
            $("#patientForm select").prop('required', false);
            $("#practitionerForm input").val('');
            $("#practitionerForm select").val('');
            $("#practitionerForm textarea").val('');
            $("#practitionerForm input").prop('checked', false);
            $("#practitionerForm input").prop('required', false);
            $("#practitionerForm select").prop('required', false);
            $("#practitionerForm textarea").prop('required', false);
            $("#pharma_street_address").prop('required', true);
            $("#pharma_locality").prop('required', true);
            $("#pharma_title").prop('required', true);
            $("#pharma_firstname").prop('required', true);
            $("#pharma_lastname").prop('required', true);
            $("#pharma_email").prop('required', true);
            // $("#pharma_password").prop('required', true);
            // $("#pharma_password-confirm").prop('required', true);
            // $("#pharma_agree_term").prop('required', true);
            $("#pharma_creditForm input").prop('required', false);
            $("#pharma_business_trading_name").prop('required', true);
            document.getElementById("pharma_list_in_web").checked = true;
            document.getElementById("pharma_newsletter").checked = true;

            document.getElementById('pharma_email').addEventListener('change', function () {
                if (!(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(this.value))) {
                    Swal.fire({
                        icon: 'error',
                        title: 'You have entered an invalid email address',
                    })
                    document.getElementById('pharma_email').value = '';
                }
            });
            document.getElementById('pharma_email').focus();
        }

        document.getElementById("pharma_email_check").onclick = function () {
            {{--Swal.fire({--}}
            {{--    title: 'Checking ..',--}}
            {{--    allowOutsideClick: false,--}}
            {{--    allowEscapeKey: false,--}}
            {{--    showConfirmButton: false,--}}
            {{--    showCloseButton: false,--}}
            {{--    showCancelButton: false,--}}
            {{--    onOpen: () => {--}}
            {{--        Swal.showLoading();--}}
            {{--        $.ajax({--}}
            {{--            type: "POST",--}}
            {{--            url: '{{ url('email_validation') }}',--}}
            {{--            data: {--}}
            {{--                _token: '{{ csrf_token() }}',--}}
            {{--                role: 'practitioner',--}}
            {{--                email: document.getElementById('pharma_email').value,--}}
            {{--            },--}}
            {{--            timeout: 6000,--}}
            {{--            success: function (data) {--}}

            {{--                if (data.code == '0') {--}}
            {{--                    document.getElementById('pharma_email').value = "";--}}
            {{--                    Swal.fire({--}}
            {{--                        icon: 'info',--}}
            {{--                        html: data.email + ' already exists. Please <a href="#" onClick="open_login()" color="#6ba53a">login</a> or <a href="/password/reset" color="#6ba53a">reset your password.</a>'--}}
            {{--                    })--}}

            {{--                } else if (data.code == '2') {--}}
            {{--                    document.getElementById('pharma_email').value = "";--}}
            {{--                    Swal.fire({--}}
            {{--                        icon: 'info',--}}
            {{--                        text: 'Email verification failed, please enter a valid email address'--}}
            {{--                    })--}}
            {{--                } else if (data.code == '3') {--}}
            {{--                    document.getElementById('pharma_email').value = "";--}}
            {{--                    Swal.fire({--}}
            {{--                        icon: 'info',--}}
            {{--                        text: 'Timeout, please try again later'--}}
            {{--                    })--}}
            {{--                } else {--}}
            {{--                    Swal.close();--}}
                                let allAreFilled = true;
                                document.getElementById("pharma_email_area").querySelectorAll("[required]").forEach(function (i) {
                                    if (i.value == '') {
                                        setErrorFor(i, errorMessage);
                                        allAreFilled = false
                                    } else {
                                        setSuccessFor(i);
                                    }
                                })
                                if (allAreFilled) {
                                    document.getElementById('pharma_email_check').style.display = 'none';
                                    document.getElementById('pharmaBusinessInfo').style.display = 'block';
                                    location.href = '#pharmaBusinessInfo';
                                    document.getElementById('pharma_street_address').focus();
                                    document.getElementById('pharma_email').removeEventListener("change", function () {
                                        if (!(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(this.value))) {
                                            Swal.fire({
                                                icon: 'error',
                                                title: 'You have entered an invalid email address',
                                            })
                                            document.getElementById('pharma_email').value = '';
                                        }
                                    });
                                    document.getElementById('pharma_email').addEventListener('change', function () {
                                        if (!(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(this.value))) {
                                            Swal.fire({
                                                icon: 'error',
                                                title: 'You have entered an invalid email address',
                                            })
                                            document.getElementById('pharma_email').value = '';
                                        }
                                        else {
                                            Swal.fire({
                                                title: 'Checking ..',
                                                allowOutsideClick: false,
                                                allowEscapeKey: false,
                                                showConfirmButton: false,
                                                showCloseButton: false,
                                                showCancelButton: false,
                                                onOpen: () => {
                                                    Swal.showLoading();
                                                    $.ajax({
                                                        type: "POST",
                                                        url: '{{ url('email_validation') }}',
                                                        data: {
                                                            _token: '{{ csrf_token() }}',
                                                            role: 'practitioner',
                                                            email: document.getElementById('pharma_email').value,
                                                        },
                                                        timeout: 6000,
                                                        success: function (data) {

                                                            if (data.code == '0') {
                                                                document.getElementById('pharma_email').value = "";
                                                                Swal.fire({
                                                                    icon: 'info',
                                                                    html: data.email + ' already exists. Please <a href="#" onClick="open_login()" color="#6ba53a">login</a> or <a href="/password/reset" color="#6ba53a">reset your password.</a>'
                                                                })

                                                            } else if (data.code == '2') {
                                                                document.getElementById('pharma_email').value = "";
                                                                Swal.fire({
                                                                    icon: 'info',
                                                                    text: 'Email verification failed, please enter a valid email address'
                                                                })
                                                            } else if (data.code == '3') {
                                                                document.getElementById('pharma_email').value = "";
                                                                Swal.fire({
                                                                    icon: 'info',
                                                                    text: 'Timeout, please try again later'
                                                                })
                                                            } else {
                                                                Swal.close();
                                                            }
                                                        }
                                                    })
                                                }
                                            })
                                        }
                                    });
                                }
            //                 }
            //             }
            //
            //         });
            //     }
            // })

        };

        function pharmaHaveAphraYes() {
            $("#pharma_ahpra_number").prop('required', true);
            $("#pharma_association_number").prop('required', false);
            $("#pharma_qualification").prop('required', false);
            $("#pharma_association_name").prop('required', false);
            document.getElementById('pharma_ahpra_area').style.display = 'block';
            document.getElementById('pharma_association_area').style.display = 'none';
            document.getElementById('pharma_personal_info_detail').style.display = 'block';
            document.getElementById('pharma_ahpra_number').addEventListener('change', function () {
                $.ajax({
                    type: "POST",
                    url: '{{ url('ahpra_number_validation') }}',
                    data: {
                        _token: '{{ csrf_token() }}',
                        number: this.value,
                    },
                    timeout: 6000,
                    success: function (data) {
                        if (data.code == '0') {
                            document.getElementById('pharma_ahpra_number').value = "";
                            document.getElementById('business_is_defined_area').style.display = 'none';
                            Swal.fire({
                                icon: 'info',
                                html: data.number + ' already exists and is linked to ' + data.name + '. Please <a href="#" onClick="open_login()" color="#6ba53a">login</a> or <a href="/password/reset" color="#6ba53a">reset your password.</a> If you need assistance with your login name please <a href="/contact" color="#6ba53a">contact us.</a>'
                            })
                        } else if (data.code == '2') {
                            Swal.fire({
                                icon: 'error',
                                title: 'AHPRA number is not valid, please check and try again',
                            })
                            document.getElementById('pharma_ahpra_number').value = '';
                        } else {
                            document.getElementById('business_is_defined_area').style.display = 'block';
                        }
                    }

                });
            });
        }

        function pharmaHaveAphraNo() {
            $("#pharma_ahpra_number").prop('required', false);
            $("#pharma_association_number").prop('required', true);
            $("#pharma_qualification").prop('required', true);
            $("#pharma_association_name").prop('required', true);
            document.getElementById('pharma_ahpra_area').style.display = 'none';
            document.getElementById('pharma_association_area').style.display = 'block';
            document.getElementById('pharma_personal_info_detail').style.display = 'block';
        }

        function showAuDoc() {
            $("#association_number").prop('required', false);
            $("#practitioner_qualification").prop('required', false);
            $("#association_name").prop('required', false);
            $("#practitioner_ahpra_number").prop('required', true);
            document.getElementById('association_area').style.display = 'none';
            document.getElementById('practitioner_ahpra_area').style.display = 'block';
            document.getElementById('practitioner_personal_info').style.display = 'block';
            document.getElementById('practitioner_choose_inter_area').style.display = 'none';
            document.getElementById("practitioner_international").checked = false;
            document.getElementById('practitioner_choose_inter_area').style.display = 'none';
            document.getElementById('practitioner_international').dispatchEvent(new Event('change'));
            $("#practitioner_email").val('');
            $("#practitioner_password").val('');
            $("#practitioner_password-confirm").val('');

            document.getElementById('practitioner_ahpra_number').addEventListener('change', function () {
                $.ajax({
                    type: "POST",
                    url: '{{ url('ahpra_number_validation') }}',
                    data: {
                        _token: '{{ csrf_token() }}',
                        number: this.value,
                    },
                    timeout: 6000,
                    success: function (data) {
                        if (data.code == '0') {
                            document.getElementById('practitioner_ahpra_number').value = "";
                            Swal.fire({
                                icon: 'info',
                                html: data.number + ' already exists and is linked to ' + data.name + '. Please <a href="#" onClick="open_login()" color="#6ba53a">login</a> or <a href="/password/reset" color="#6ba53a">reset your password.</a> If you need assistance with your login name please <a href="/contact" color="#6ba53a">contact us.</a>'
                            })
                        } else if (data.code == '2') {
                            Swal.fire({
                                icon: 'error',
                                title: 'AHPRA number is not valid, please check and try again',
                            })
                            document.getElementById('practitioner_ahpra_number').value = '';
                        }
                    }

                });
            });
            location.href = '#practitionerForm';
            document.getElementById('practitioner_ahpra_number').focus();
        };

        function showInterDoc() {
            $("#association_number").prop('required', true);
            $("#practitioner_qualification").prop('required', true);
            $("#association_name").prop('required', true);
            document.getElementById('association_area').style.display = 'block';
            $("#practitioner_ahpra_number").prop('required', false);
            document.getElementById('practitioner_ahpra_area').style.display = 'none';
            document.getElementById('practitioner_personal_info').style.display = 'block';
            document.getElementById('practitioner_choose_inter_area').style.display = 'block';
            document.getElementById("practitioner_international").checked = true;
            document.getElementById('practitioner_choose_inter_area').style.display = 'none';
            document.getElementById('practitioner_international').dispatchEvent(new Event('change'));
            $("#practitioner_email").val('');
            $("#practitioner_password").val('');
            $("#practitioner_password-confirm").val('');
            location.href = '#practitionerForm';
            document.getElementById('practitioner_email').focus();
        };
        document.getElementById('practitioner_email').addEventListener('change', function validateEmail() {
            if (!(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(this.value))) {
                Swal.fire({
                    icon: 'error',
                    title: 'You have entered an invalid email address',
                })
                document.getElementById('practitioner_email').value = "";
            }
            else {
                Swal.fire({
                    title: 'Checking ..',
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    showConfirmButton: false,
                    showCloseButton: false,
                    showCancelButton: false,
                    onOpen: () => {
                        Swal.showLoading();

                        $.ajax({
                            type: "POST",
                            url: '{{ url('email_validation') }}',
                            data: {
                                _token: '{{ csrf_token() }}',
                                role: 'practitioner',
                                email: this.value,
                            },
                            timeout: 6000,
                            success: function (data) {document.getElementById('practitioner_email').addEventListener('change', function validateEmail() {
                                if (!(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(this.value))) {
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'You have entered an invalid email address',
                                    })
                                    document.getElementById('practitioner_email').value = "";
                                }
                                else {
                                    Swal.fire({
                                        title: 'Checking ..',
                                        allowOutsideClick: false,
                                        allowEscapeKey: false,
                                        showConfirmButton: false,
                                        showCloseButton: false,
                                        showCancelButton: false,
                                        onOpen: () => {
                                            Swal.showLoading();

                                            $.ajax({
                                                type: "POST",
                                                url: '{{ url('email_validation') }}',
                                                data: {
                                                    _token: '{{ csrf_token() }}',
                                                    role: 'practitioner',
                                                    email: this.value,
                                                },
                                                timeout: 6000,
                                                success: function (data) {
                                                    if (data.code == '0') {
                                                        document.getElementById('practitioner_email').value = "";
                                                        Swal.fire({
                                                            icon: 'info',
                                                            html: data.email + ' already exists. Please <a href="#" onClick="open_login()" color="#6ba53a">login</a> or <a href="/password/reset" color="#6ba53a">reset your password.</a>'
                                                        })

                                                    } else if (data.code == '2') {
                                                        document.getElementById('practitioner_email').value = "";
                                                        Swal.fire({
                                                            icon: 'info',
                                                            text: 'Email verification failed, please enter a valid email address'
                                                        })
                                                    } else if (data.code == '3') {
                                                        document.getElementById('practitioner_email').value = "";
                                                        Swal.fire({
                                                            icon: 'info',
                                                            text: 'Timeout, please try again later'
                                                        })
                                                    } else {
                                                        Swal.close();

                                                    }
                                                }

                                            });
                                        }
                                    })

                                }
                            });
                                if (data.code == '0') {
                                    document.getElementById('practitioner_email').value = "";
                                    Swal.fire({
                                        icon: 'info',
                                        html: data.email + ' already exists. Please <a href="#" onClick="open_login()" color="#6ba53a">login</a> or <a href="/password/reset" color="#6ba53a">reset your password.</a>'
                                    })

                                } else if (data.code == '2') {
                                    document.getElementById('practitioner_email').value = "";
                                    Swal.fire({
                                        icon: 'info',
                                        text: 'Email verification failed, please enter a valid email address'
                                    })
                                } else if (data.code == '3') {
                                    document.getElementById('practitioner_email').value = "";
                                    Swal.fire({
                                        icon: 'info',
                                        text: 'Timeout, please try again later'
                                    })
                                } else {
                                    Swal.close();

                                }
                            }

                        });
                    }
                })

            }
        });
        document.getElementById('pharma_business_trading_name').addEventListener('change', function () {
            $.ajax({
                type: "POST",
                url: '{{ url('trading_name_validation') }}',
                data: {
                    _token: '{{ csrf_token() }}',
                    name: this.value,
                },
                timeout: 6000,
                success: function (data) {
                    if (data.code == '1') {
                        document.getElementById('pharma_business_trading_name').value = "";
                        Swal.fire({
                            icon: 'info',
                            html: data.name + ' already exists in our database. Please <a href="#" onClick="open_login()" color="#6ba53a">login</a> or <a href="/password/reset" color="#6ba53a">reset your password.</a> If you need assistance with your login name please <a href="/contact" color="#6ba53a">contact us.</a>'
                        })
                    }
                }

            });
        });
        document.getElementById('business_trading_name').addEventListener('change', function () {
            $.ajax({
                type: "POST",
                url: '{{ url('trading_name_validation') }}',
                data: {
                    _token: '{{ csrf_token() }}',
                    name: this.value,
                },
                timeout: 6000,
                success: function (data) {
                    if (data.code == '1') {
                        document.getElementById('business_trading_name').value = "";
                        Swal.fire({
                            icon: 'info',
                            html: data.name + ' already exists in our database. Please <a href="#" onClick="open_login()" color="#6ba53a">login</a> or <a href="/password/reset" color="#6ba53a">reset your password.</a> If you need assistance with your login name please <a href="/contact" color="#6ba53a">contact us.</a>'
                        })
                    }
                }

            });
        });

        document.getElementById('pharma_business_search').addEventListener('change', function () {
            if (this.value.match(/\,/g)) {
                if ((this.value.match(/\,/g)).length >= 2) {
                    var sendValue = this.value.split(',');
                    $.ajax({
                        type: "POST",
                        url: '{{ url('select_business') }}',
                        data: {
                            _token: '{{ csrf_token() }}',
                            value: sendValue[sendValue.length - 1],
                        },
                        timeout: 6000,
                        success: function (data) {
                            if(data.data !=null){
                                var list = JSON.parse(data.data)
                                $('#pharma_business_list').empty();
                                document.getElementById('pharma_business_name').value = list['name'];
                                document.getElementById('pharma_business_trading_name').value = list['name'];
                                document.getElementById('pharma_business_trading_name').dispatchEvent(new Event('change'));
                                document.getElementById('pharma_business_type').value = list['business_type'];
                                document.getElementById('pharma_business_number').value = list['abn'];
                                document.getElementById('pharma_acn').value = list['acn'];
                                document.getElementById('pharma_business_detail').style.display = 'block';
                                document.getElementById('pharma_business_sub_detail').style.display = 'block';
                                setHeight($('#pharma_business_trading_name'));
                                setHeight($('#pharma_business_name'));
                                document.getElementById('pharma_business_number').dispatchEvent(new Event('change'));
                                document.getElementById('pharma_acn').dispatchEvent(new Event('change'));
                                location.href = '#pharma_business_search';
                                // document.getElementById('pharma_business_trading_name').focus();
                                document.getElementById('pharma_search_business_button').style.display = 'none';
                                document.getElementById('pharma_business_info_check').style.display = 'block';
                            }

                        }

                    });
                }
            } else if (this.value == '') {
                document.getElementById('pharma_business_detail').style.display = 'none';
                document.getElementById('pharma_company_name_display_area').style.display = 'none';
                document.getElementById('pharma_business_sub_detail').style.display = 'none';
                document.getElementById('pharma_business_name').value = '';
                document.getElementById('pharma_business_trading_name').value = '';
                document.getElementById('pharma_business_type').value = '';
                document.getElementById('pharma_business_number').value = '';
                document.getElementById('pharma_acn').value = '';
                document.getElementById('pharma_search_business_button').style.display = 'block';
                document.getElementById('pharma_business_info_check').style.display = 'none';
            }
        });

        function sendPharmaBusinessList() {
            if (document.getElementById('pharma_business_search').value == '') {
                setErrorFor(document.getElementById('pharma_business_search').parentElement, errorMessage);
            } else {
                setSuccessFor(document.getElementById('pharma_business_search').parentElement);
                var value = document.getElementById('pharma_business_search').value;
                if (value != '') {
                    Swal.fire({
                        title: 'Checking ..',
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        showConfirmButton: false,
                        showCloseButton: false,
                        showCancelButton: false,
                        onOpen: () => {
                            Swal.showLoading();
                            return $.ajax({
                                type: "POST",
                                url: '{{ url('search_business') }}',
                                data: {
                                    _token: '{{ csrf_token() }}',
                                    value: value,
                                },
                                success: function (data) {
                                    var list = JSON.parse(data.data)
                                    if (list.hasOwnProperty('business_type')) {
                                        $('#pharma_business_list').empty();
                                        document.getElementById('pharma_business_name').value = list['name'];
                                        document.getElementById('pharma_business_trading_name').value = list['name'];
                                        document.getElementById('pharma_business_trading_name').dispatchEvent(new Event('change'));
                                        document.getElementById('pharma_business_type').value = list['business_type'];
                                        document.getElementById('pharma_business_number').value = list['abn'];
                                        document.getElementById('pharma_acn').value = list['acn'];
                                        document.getElementById('pharma_business_detail').style.display = 'block';
                                        document.getElementById('pharma_business_sub_detail').style.display = 'block';
                                        setHeight($('#pharma_business_trading_name'));
                                        setHeight($('#pharma_business_name'));
                                        document.getElementById('pharma_business_number').dispatchEvent(new Event('change'));
                                        document.getElementById('pharma_acn').dispatchEvent(new Event('change'));
                                        location.href = '#pharma_business_search';
                                        document.getElementById('pharma_search_business_button').style.display = 'none';
                                        document.getElementById('pharma_business_info_check').style.display = 'block';
                                        // document.getElementById('pharma_business_trading_name').focus();
                                        Swal.close();
                                        return false;

                                    }
                                }

                            }).then((result) => {
                                Swal.hideLoading();
                                if (result.data) {
                                    var list = JSON.parse(result.data)
                                    if(typeof list['Names'] !== 'undefined'){
                                        var data = list['Names'];
                                        var options = data.map(function (data) {
                                            var option = document.createElement('option');
                                            option.value = data.Name + ', ' + data.Postcode + ', ' + data.Abn;
                                            return option;
                                        });
                                        $("#pharma_business_list")
                                            .empty()
                                            .append(list['Names']);
                                        var options_business = {};
                                        $.map(list['Names'],
                                            function (o) {
                                                var content = o.Name + ', ' + o.Postcode + ', ' + o.Abn;
                                                options_business[content] = content;
                                            });
                                        Swal.fire({
                                            icon: 'info',
                                            input: 'select',
                                            title: 'Please select a business from the list below',
                                            inputOptions: options_business,    // inputOptions: {
                                            inputPlaceholder: 'Select a business',
                                            allowOutsideClick: false,
                                            confirmButtonText: 'OK',
                                            showCancelButton: true,
                                            reverseButtons: true,
                                            inputValidator: (value) => {
                                                if (!value) {
                                                    return 'Please select your Business'
                                                }
                                                return new Promise((resolve) => {
                                                    document.getElementById('pharma_business_search').value = value;
                                                    document.getElementById('pharma_business_search').dispatchEvent(new Event('change'));
                                                    resolve();
                                                })
                                            }
                                        })
                                    }

                                    // document.getElementById('pharma_search_business_button').style.display = 'none';
                                    // document.getElementById('pharma_business_info_check').style.display = 'block';
                                }
                            });
                        }
                    })

                } else if (value == '') {
                    document.getElementById('pharma_business_detail').style.display = 'none';
                    document.getElementById('pharma_business_sub_detail').style.display = 'none';
                    document.getElementById('pharma_company_name_display_area').style.display = 'none';
                    document.getElementById('pharma_business_name').value = '';
                    document.getElementById('pharma_business_trading_name').value = '';
                    document.getElementById('pharma_business_type').value = '';
                    document.getElementById('pharma_business_number').value = '';
                    document.getElementById('pharma_acn').value = '';
                }
            }
        };
        document.getElementById('practitioner_business_search').addEventListener('change', function () {
            if (this.value.match(/\,/g)) {
                if ((this.value.match(/\,/g)).length >= 2) {
                    var sendValue = this.value.split(',');
                    $.ajax({
                        type: "POST",
                        url: '{{ url('select_business') }}',
                        data: {
                            _token: '{{ csrf_token() }}',
                            value: sendValue[sendValue.length - 1],
                        },
                        timeout: 6000,
                        success: function (data) {
                            var list = JSON.parse(data.data)
                            console.log(list);
                            $('#practitioner_business_list').empty();
                            document.getElementById('business_name').value = list['name'];
                            document.getElementById('business_trading_name').value = list['name'];
                            document.getElementById('business_trading_name').dispatchEvent(new Event('change'));
                            document.getElementById('business_type').value = list['business_type'];
                            document.getElementById('business_number').value = list['abn'];
                            document.getElementById('acn').value = list['acn'];
                            document.getElementById('practitioner_business_detail').style.display = 'block';
                            document.getElementById('practitioner_business_sub_detail').style.display = 'block';
                            setHeight($('#business_trading_name'));
                            setHeight($('#business_name'));
                            document.getElementById('business_number').dispatchEvent(new Event('change'));
                            document.getElementById('acn').dispatchEvent(new Event('change'));
                            location.href = '#practitioner_business_search';
                            document.getElementById('business_trading_name').focus();
                            document.getElementById('practitioner_search_business_button').style.display = 'none';
                            document.getElementById('business_info_check').style.display = 'block';
                        }

                    });
                }
            } else if (this.value == '') {
                document.getElementById('practitioner_business_detail').style.display = 'none';
                document.getElementById('practitioner_company_name_display_area').style.display = 'none';
                document.getElementById('practitioner_business_sub_detail').style.display = 'none';
                document.getElementById('business_name').value = '';
                document.getElementById('business_trading_name').value = '';
                document.getElementById('business_type').value = '';
                document.getElementById('business_number').value = '';
                document.getElementById('acn').value = '';
                document.getElementById('practitioner_search_business_button').style.display = 'block';
                document.getElementById('business_info_check').style.display = 'none';
            }
        });

        function sendPractitionerBusinessList() {
            let allAreFilled = true;
            if (!document.getElementById('practitioner_street_address').value) {
                allAreFilled = false;
                setErrorFor(document.getElementById('practitioner_street_address'), errorMessage);
            } else {
                setSuccessFor(document.getElementById('practitioner_street_address'));
            }
            if (!document.getElementById('practitioner_locality').value) {
                allAreFilled = false;
                setErrorFor(document.getElementById('practitioner_locality'), errorMessage);
            } else {
                setSuccessFor(document.getElementById('practitioner_locality'));
            }
            var value = document.getElementById('practitioner_business_search').value;
            console.log(value)

            if (value != '' && allAreFilled) {
                Swal.fire({
                    title: 'Checking ..',
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    showConfirmButton: false,
                    showCloseButton: false,
                    showCancelButton: false,
                    onOpen: () => {
                        Swal.showLoading();
                        return $.ajax({
                            type: "POST",
                            url: '{{ url('search_business') }}',
                            data: {
                                _token: '{{ csrf_token() }}',
                                value: value,
                            },
                            success: function (data) {
                                var list = JSON.parse(data.data)
                                if (list.hasOwnProperty('business_type')) {
                                    $('#practitioner_business_list').empty();
                                    document.getElementById('business_name').value = list['name'];
                                    document.getElementById('business_trading_name').value = list['name'];
                                    document.getElementById('business_trading_name').dispatchEvent(new Event('change'));
                                    document.getElementById('business_type').value = list['business_type'];
                                    document.getElementById('business_number').value = list['abn'];
                                    document.getElementById('acn').value = list['acn'];
                                    document.getElementById('practitioner_business_detail').style.display = 'block';
                                    document.getElementById('practitioner_business_sub_detail').style.display = 'block';
                                    setHeight($('#business_trading_name'));
                                    setHeight($('#business_name'));
                                    document.getElementById('business_number').dispatchEvent(new Event('change'));
                                    document.getElementById('acn').dispatchEvent(new Event('change'));
                                    location.href = '#practitioner_business_search';
                                    document.getElementById('business_trading_name').focus();
                                    document.getElementById('practitioner_search_business_button').style.display = 'none';
                                    document.getElementById('business_info_check').style.display = 'block';
                                    Swal.close();
                                    return false;

                                }
                            }

                        }).then((result) => {
                            Swal.hideLoading();
                            if (result.data) {
                                var list = JSON.parse(result.data)
                                var data = list['Names'];
                                if(typeof list['Names'] !== 'undefined'){
                                    var options = data.map(function (data) {
                                        var option = document.createElement('option');
                                        option.value = data.Name + ', ' + data.Postcode + ', ' + data.Abn;
                                        return option;
                                    });
                                    $("#practitioner_business_list")
                                        .empty()
                                        .append(list['Names']);
                                    var options_business = {};
                                    $.map(list['Names'],
                                        function (o) {
                                            var content = o.Name + ', ' + o.Postcode + ', ' + o.Abn;
                                            options_business[content] = content;
                                        });
                                    Swal.fire({
                                        icon: 'info',
                                        input: 'select',
                                        title: 'Please select a business from the list below',
                                        inputOptions: options_business,    // inputOptions: {
                                        inputPlaceholder: 'Select a business',
                                        allowOutsideClick: false,
                                        confirmButtonText: 'OK',
                                        showCancelButton: true,
                                        reverseButtons: true,
                                        inputValidator: (value) => {
                                            if (!value) {
                                                return 'Please select your Business'
                                            }
                                            return new Promise((resolve) => {
                                                document.getElementById('practitioner_business_search').value = value;
                                                document.getElementById('practitioner_business_search').dispatchEvent(new Event('change'));
                                                resolve();
                                            })
                                        }
                                    })
                                }

                                // document.getElementById('practitioner_search_business_button').style.display = 'none';
                                // document.getElementById('business_info_check').style.display = 'block';

                            }
                        });
                    }
                })

            } else if (value == '') {
                document.getElementById('practitioner_business_detail').style.display = 'none';
                document.getElementById('practitioner_business_sub_detail').style.display = 'none';
                document.getElementById('practitioner_company_name_display_area').style.display = 'none';
                document.getElementById('business_name').value = '';
                document.getElementById('business_trading_name').value = '';
                document.getElementById('business_type').value = '';
                document.getElementById('business_number').value = '';
                document.getElementById('acn').value = '';
            }

        };

        function creditFormAppear() {
            document.getElementById('creditForm').style.display = 'block';
        }

        function creditFormDisappear() {
            document.getElementById('creditForm').style.display = 'none';
            $("#creditForm input").val('');
        }

        function pharma_creditFormAppear() {
            document.getElementById('pharma_creditForm').style.display = 'block';
        }

        function pharma_creditFormDisappear() {
            document.getElementById('pharma_creditForm').style.display = 'none';
            $("#pharma_creditForm input").val('');
        }
    </script>
    <script type="text/javascript">
        document.getElementById('pharma_primary_channel').addEventListener('change', function () {

            $.ajax({
                type: "POST",
                url: '{{ url('get_channel') }}',
                data: {
                    _token: '{{ csrf_token() }}',
                    parent: this.value,
                },
                timeout: 6000,
                success: function (data) {
                    $('#pharma_second_channel').empty();
                    $("#pharma_second_channel").append('<option disabled selected value> -- select an option --</option>')
                    $.each(data.data, function () {
                        $("#pharma_second_channel").append('<option value="' + this + '">' + this + '</option>')
                    })
                    document.getElementById('pharma_second_channel_area').style.display = 'block';
                }

            });
        });
        document.getElementById('primary_channel').addEventListener('change', function () {

            $.ajax({
                type: "POST",
                url: '{{ url('get_channel') }}',
                data: {
                    _token: '{{ csrf_token() }}',
                    parent: this.value,
                },
                timeout: 6000,
                error: function (request, error) {
                    if (error == "timeout") {
                        $('#err-timedout').slideDown('slow');
                    } else {
                        $('#err-state').slideDown('slow');
                        $("#err-state").html('An error occurred: ' + error + '');
                    }
                },
                success: function (data) {
                    $('#second_channel').empty();
                    $("#second_channel").append('<option disabled selected value> -- select an option --</option>')
                    $.each(data.data, function () {
                        $("#second_channel").append('<option value="' + this + '">' + this + '</option>')
                    })
                    document.getElementById('second_channel_area').style.display = 'block';
                }

            });
        });

        document.getElementById('referral_code').addEventListener('change', function () {
            $.ajax({
                type: "POST",
                url: '{{ url('referral_check') }}',
                data: {
                    _token: '{{ csrf_token() }}',
                    code: this.value,
                },
                timeout: 6000,
                success: function (data) {
                    if (data.code == '1') {
                        Swal.fire({
                            icon: 'question',
                            text: 'Please confirm that your Practitioner is "' + data.top + '"?',
                            showCancelButton: true,
                            allowOutsideClick: false,
                            confirmButtonText: 'Yes',
                            reverseButtons: true,

                        }).then((result) => {
                            if (!result.value) {
                                document.getElementById('referral_code').value = "";
                                $('#referral_code_name').remove();
                            } else {
                                $('#referral_code_row').append('<div class="alert alert-blue" id="referral_code_name"><p>Your practitioner is ' + data.top + ' </p></div>');
                            }
                        })
                    } else if (data.code == '0') {
                        Swal.fire({
                            icon: 'error',
                            title: 'The code you have entered does not match our records. Please contact your Practitioner to confirm',
                        })
                        document.getElementById('referral_code').value = "";
                        $('#referral_code_name').remove();
                    }
                }

            });
        });
        document.getElementById('pharma_phone').addEventListener('change', function () {
            var number = int_pharma_phone.getNumber();
            if (number) {
                $.ajax({
                    type: "POST",
                    url: '{{ url('phone_existence_validate') }}',
                    data: {
                        _token: '{{ csrf_token() }}',
                        type: 'landline',
                        phone: number,
                    },
                    timeout: 6000,
                    success: function (data) {
                        if (data.code == 2) {
                            Swal.fire({
                                icon: 'error',
                                title: 'Landline number is not valid',
                            })
                            setErrorFor(document.getElementById('pharma_phone').parentElement, 'Landline number is not valid');
                            var new_status = 'false';
                            $('#pharma_phone').attr('data-status', new_status);
                        } else {
                            var new_status = 'true';
                            $('#pharma_phone').attr('data-status', new_status);
                            setSuccessFor(document.getElementById('pharma_phone').parentElement);
                        }
                    }
                })
            } else {
                var new_status = 'true';
                $('#pharma_phone').attr('data-status', new_status);
                setSuccessFor(document.getElementById('pharma_phone').parentElement);
            }
        });
        document.getElementById('pharma_mobile').addEventListener('change', function () {
            var number = int_pharma_mobile.getNumber();
            if (number) {
                $.ajax({
                    type: "POST",
                    url: '{{ url('phone_existence_validate') }}',
                    data: {
                        _token: '{{ csrf_token() }}',
                        type: 'mobile',
                        phone: number,
                    },
                    timeout: 6000,
                    success: function (data) {
                        if (data.code == 2) {
                            Swal.fire({
                                icon: 'error',
                                title: 'Mobile number is not valid',
                            })
                            setErrorFor(document.getElementById('pharma_phone').parentElement, 'Mobile number is not valid');
                            var new_status = 'false';
                            $('#pharma_mobile').attr('data-status', new_status);
                        } else {
                            var new_status = 'true';
                            $('#pharma_mobile').attr('data-status', new_status);
                            setSuccessFor(document.getElementById('pharma_phone').parentElement);
                        }
                    }
                })
            } else {
                var new_status = 'true';
                $('#pharma_mobile').attr('data-status', new_status);
                setSuccessFor(document.getElementById('pharma_phone').parentElement);
            }
        });
        document.getElementById('practitioner_phone').addEventListener('change', function () {
            var number = int_practitioner_phone.getNumber();
            if (number) {
                $.ajax({
                    type: "POST",
                    url: '{{ url('phone_existence_validate') }}',
                    data: {
                        _token: '{{ csrf_token() }}',
                        type: 'landline',
                        phone: number,
                    },
                    timeout: 6000,
                    success: function (data) {
                        if (data.code == 2) {
                            Swal.fire({
                                icon: 'error',
                                title: 'Landline number is not valid',
                            })
                            setErrorFor(document.getElementById('practitioner_phone').parentElement, 'Landline number is not valid');
                            var new_status = 'false';
                            $('#practitioner_phone').attr('data-status', new_status);
                        } else {
                            var new_status = 'true';
                            $('#practitioner_phone').attr('data-status', new_status);
                            setSuccessFor(document.getElementById('practitioner_phone').parentElement);
                        }
                    }
                })
            } else {
                var new_status = 'true';
                $('#practitioner_phone').attr('data-status', new_status);
                setSuccessFor(document.getElementById('practitioner_phone').parentElement);
            }

        });
        document.getElementById('practitioner_mobile').addEventListener('change', function () {
            var number = int_practitioner_mobile.getNumber();
            if (number) {
                $.ajax({
                    type: "POST",
                    url: '{{ url('phone_existence_validate') }}',
                    data: {
                        _token: '{{ csrf_token() }}',
                        type: 'mobile',
                        phone: number,
                    },
                    timeout: 6000,
                    success: function (data) {
                        if (data.code == 2) {
                            Swal.fire({
                                icon: 'error',
                                title: 'Mobile number is not valid',
                            })
                            setErrorFor(document.getElementById('practitioner_phone').parentElement, 'Mobile number is not valid');
                            var new_status = 'false';
                            $('#practitioner_mobile').attr('data-status', new_status);
                        } else {
                            var new_status = 'true';
                            $('#practitioner_mobile').attr('data-status', new_status);
                            setSuccessFor(document.getElementById('practitioner_mobile').parentElement);
                        }
                    }
                })
            } else {
                var new_status = 'true';
                $('#practitioner_mobile').attr('data-status', new_status);
                setSuccessFor(document.getElementById('practitioner_phone').parentElement);
            }

        });
        document.getElementById('pharma_business_phone').addEventListener('change', function () {
            // if (this.value.length < 6) {
            //     Swal.fire({
            //         icon: 'error',
            //         title: 'Landline number is not valid',
            //     })
            //     document.getElementById('pharma_business_phone').value = '';
            // }
            var number = int_pharma_business_phone.getNumber();
            if (number) {
                $.ajax({
                    type: "POST",
                    url: '{{ url('phone_existence_validate') }}',
                    data: {
                        _token: '{{ csrf_token() }}',
                        type: 'landline',
                        phone: number,
                    },
                    timeout: 6000,
                    success: function (data) {
                        if (data.code == 2) {
                            Swal.fire({
                                icon: 'error',
                                title: 'Landline number is not valid',
                            })
                            setErrorFor(document.getElementById('pharma_business_phone').parentElement, 'Landline number is not valid');
                            var new_status = 'false';
                            $('#pharma_business_phone').attr('data-status', new_status);
                        } else {
                            var new_status = 'true';
                            $('#pharma_business_phone').attr('data-status', new_status);
                            setSuccessFor(document.getElementById('pharma_business_phone').parentElement);
                            var number = int_pharma_business_phone.getNumber();
                            $.ajax({
                                type: "POST",
                                url: '{{ url('phone_check') }}',
                                data: {
                                    _token: '{{ csrf_token() }}',
                                    phone: number,
                                },
                                timeout: 6000,
                                success: function (data) {
                                    var business = JSON.parse(data.business);
                                    console.log(business);
                                    if (data.code == '1') {
                                        if (business.length == 1) {
                                            Swal.fire({
                                                icon: 'info',
                                                text: 'The phone entered has an existing business "' + business[0]['trading_name'] + '" in our database. Continue with a new Account application for "' + document.getElementById('pharma_business_name').value + '" or link your practitioner details to "' + business[0]['trading_name'] + '"',
                                                showCancelButton: true,
                                                allowOutsideClick: false,
                                                confirmButtonText: 'Link to existing',
                                                cancelButtonText: 'New Account',
                                                reverseButtons: true

                                            }).then((result) => {
                                                if (result.value) {
                                                    document.getElementById('pharmaBusinessInfo').style.display = 'none';
                                                    document.getElementById('pharma_accountTermsForm').style.display = 'none';
                                                    $("#pharmaBusinessInfo input").prop('required', false);
                                                    $("#pharmaBusinessInfo select").prop('required', false);
                                                    $("#pharmaBusinessInfo textarea").prop('required', false);
                                                    document.getElementById('pharma_business_name').value = business[0]['customer_name'];
                                                    document.getElementById('pharma_company_name_display').value = business[0]['trading_name'];
                                                    document.getElementById("pharma_business_name").readOnly = true;
                                                    document.getElementById('pharma_street_address').value = business[0]['address']['address_line_1'];
                                                    document.getElementById('pharma_second_address').value = business[0]['address']['address_line_2'];
                                                    document.getElementById('pharma_locality').value = business[0]['address']['suburb'];
                                                    document.getElementById('pharma_administrative_area_level_1').value = business[0]['address']['state'];
                                                    document.getElementById('pharma_postal_code').value = business[0]['address']['postcode'];
                                                    document.getElementById('pharma_country').value = business[0]['address']['country'];
                                                    document.getElementById('pharma_business_info_check').style.display = 'none';
                                                    document.getElementById('pharma_personal_info').style.display = 'block';
                                                    if (document.getElementById('pharmaBusinessInfo').style.display == 'none') {
                                                        document.getElementById('pharma_personal_info_check_agree').style.display = 'block';
                                                    } else {
                                                        document.getElementById('pharma_personal_info_check_account').style.display = 'block';
                                                    }
                                                    $("#pharma_password").val('');
                                                    $("#pharma_password-confirm").val('');
                                                    location.href = '#pharma_company_name_display_area';
                                                    document.getElementById('pharma_firstname').focus();
                                                    document.getElementById("pharma_company_name_display_area").style.display = 'block';

                                                }
                                            })
                                        } else {
                                            var options = {};
                                            $.map(business,
                                                function (o) {
                                                    options[o.guid] = o.trading_name;
                                                });
                                            Swal.fire({
                                                icon: 'info',
                                                text: 'The phone entered has several existing business in our database. Continue with a new Account application for "' + document.getElementById('pharma_business_name').value + '" or choose a existing business to link your practitioner details to',
                                                input: 'select',
                                                inputOptions: options,    // inputOptions: {
                                                inputPlaceholder: 'Select a business',
                                                allowOutsideClick: false,
                                                showCancelButton: true,
                                                confirmButtonText: 'Link to existing',
                                                cancelButtonText: 'New Account',
                                                reverseButtons: true,
                                                // showLoaderOnConfirm: true,
                                                inputValidator: (value) => {
                                                    return new Promise((resolve) => {
                                                        business.forEach(updateAddress);

                                                        function updateAddress(item) {
                                                            if (item['guid'] == value) {
                                                                document.getElementById('pharmaBusinessInfo').style.display = 'none';
                                                                document.getElementById('pharma_accountTermsForm').style.display = 'none';
                                                                $("#pharmaBusinessInfo input").prop('required', false);
                                                                $("#pharmaBusinessInfo select").prop('required', false);
                                                                $("#pharmaBusinessInfo textarea").prop('required', false);
                                                                document.getElementById('pharma_business_name').value = item['customer_name'];
                                                                document.getElementById('pharma_company_name_display').value = item['trading_name'];
                                                                document.getElementById("pharma_business_name").readOnly = true;
                                                                if (item['address']) {
                                                                    document.getElementById('pharma_street_address').value = item['address']['address_line_1'];
                                                                    document.getElementById('pharma_second_address').value = item['address']['address_line_2'];
                                                                    document.getElementById('pharma_locality').value = item['address']['suburb'];
                                                                    document.getElementById('pharma_administrative_area_level_1').value = item['address']['state'];
                                                                    document.getElementById('pharma_postal_code').value = item['address']['postcode'];
                                                                    document.getElementById('pharma_country').value = item['address']['country'];
                                                                }
                                                                document.getElementById('pharma_business_info_check').style.display = 'none';
                                                                document.getElementById('pharma_personal_info').style.display = 'block';
                                                                if (document.getElementById('pharmaBusinessInfo').style.display == 'none') {
                                                                    document.getElementById('pharma_personal_info_check_agree').style.display = 'block';
                                                                } else {
                                                                    document.getElementById('pharma_personal_info_check_account').style.display = 'block';
                                                                }
                                                                $("#pharma_password").val('');
                                                                $("#pharma_password-confirm").val('');
                                                                location.href = '#pharma_company_name_display_area';
                                                                document.getElementById('pharma_firstname').focus();
                                                                document.getElementById("pharma_company_name_display_area").style.display = 'block';
                                                                resolve();
                                                            }
                                                        }

                                                    })
                                                }
                                            })
                                        }

                                    }
                                }

                            });
                        }
                    }
                })
            } else {
                var new_status = 'true';
                $('#pharma_business_phone').attr('data-status', new_status);
                setSuccessFor(document.getElementById('pharma_business_phone').parentElement);
            }
        });
        document.getElementById('business_phone').addEventListener('change', function () {
            var number = int_business_phone.getNumber();
            if (number) {
                $.ajax({
                    type: "POST",
                    url: '{{ url('phone_existence_validate') }}',
                    data: {
                        _token: '{{ csrf_token() }}',
                        type: 'landline',
                        phone: number,
                    },
                    timeout: 6000,
                    success: function (data) {
                        if (data.code == 2) {
                            Swal.fire({
                                icon: 'error',
                                title: 'Landline number is not valid',
                            })
                            setErrorFor(document.getElementById('business_phone').parentElement, 'Landline number is not valid');
                            var new_status = 'false';
                            $('#business_phone').attr('data-status', new_status);
                        } else {
                            var new_status = 'true';
                            $('#business_phone').attr('data-status', new_status);
                            setSuccessFor(document.getElementById('business_phone').parentElement);
                            var number = int_business_phone.getNumber();
                            $.ajax({
                                type: "POST",
                                url: '{{ url('phone_check') }}',
                                data: {
                                    _token: '{{ csrf_token() }}',
                                    phone: number,
                                },
                                timeout: 6000,
                                success: function (data) {
                                    var business = JSON.parse(data.business);
                                    console.log(business);
                                    if (data.code == '1') {
                                        if (business.length == 1) {
                                            Swal.fire({
                                                icon: 'info',
                                                text: 'The phone entered has an existing business "' + business[0]['trading_name'] + '" in our database. Continue with a new Account application for "' + document.getElementById('business_name').value + '" or link your pharma details to "' + business[0]['trading_name'] + '"',
                                                showCancelButton: true,
                                                allowOutsideClick: false,
                                                confirmButtonText: 'Link to existing',
                                                cancelButtonText: 'New Account',
                                                reverseButtons: true

                                            }).then((result) => {
                                                if (result.value) {
                                                    document.getElementById('practitionerBusinessInfo').style.display = 'none';
                                                    $("#practitionerBusinessInfo input").prop('required', false);
                                                    $("#practitionerBusinessInfo select").prop('required', false);
                                                    $("#practitionerBusinessInfo textarea").prop('required', false);
                                                    document.getElementById('business_name').value = business[0]['customer_name'];
                                                    document.getElementById('practitioner_company_name_display').value = business[0]['trading_name'];
                                                    document.getElementById("business_name").readOnly = true;
                                                    document.getElementById('practitioner_street_address').value = business[0]['address']['address_line_1'];
                                                    document.getElementById('practitioner_second_address').value = business[0]['address']['address_line_2'];
                                                    document.getElementById('practitioner_locality').value = business[0]['address']['suburb'];
                                                    document.getElementById('practitioner_administrative_area_level_1').value = business[0]['address']['state'];
                                                    document.getElementById('practitioner_postal_code').value = business[0]['address']['postcode'];
                                                    document.getElementById('practitioner_country').value = business[0]['address']['country'];
                                                    document.getElementById('agree_to_term_info').style.display = 'block';
                                                    $("#practitioner_password").val('');
                                                    $("#practitioner_password-confirm").val('');
                                                    location.href = '#practitoner_password';
                                                    document.getElementById('practitioner_password').focus();
                                                    document.getElementById("practitioner_company_name_display_area").style.display = 'block';
                                                }
                                            })
                                        } else {
                                            var options = {};
                                            $.map(business,
                                                function (o) {
                                                    options[o.guid] = o.trading_name;
                                                });
                                            Swal.fire({
                                                icon: 'info',
                                                text: 'The phone entered has several existing business in our database. Continue with a new Account application for "' + document.getElementById('business_name').value + '" or choose a existing business to link your practitioner details to',
                                                input: 'select',
                                                inputOptions: options,    // inputOptions: {
                                                inputPlaceholder: 'Select a business',
                                                allowOutsideClick: false,
                                                showCancelButton: true,
                                                confirmButtonText: 'Link to existing',
                                                cancelButtonText: 'New Account',
                                                reverseButtons: true,
                                                // showLoaderOnConfirm: true,
                                                inputValidator: (value) => {
                                                    return new Promise((resolve) => {
                                                        business.forEach(updateAddress);

                                                        function updateAddress(item) {
                                                            if (item['guid'] == value) {
                                                                document.getElementById('practitionerBusinessInfo').style.display = 'none';
                                                                // document.getElementById('accountTermsForm').style.display = 'none';
                                                                $("#practitionerBusinessInfo input").prop('required', false);
                                                                $("#practitionerBusinessInfo select").prop('required', false);
                                                                $("#practitionerBusinessInfo textarea").prop('required', false);
                                                                document.getElementById('business_name').value = business[0]['customer_name'];
                                                                document.getElementById('practitioner_company_name_display').value = business[0]['trading_name'];
                                                                document.getElementById("business_name").readOnly = true;
                                                                if (item['address']) {
                                                                    document.getElementById('practitioner_street_address').value = item['address']['address_line_1'];
                                                                    document.getElementById('practitioner_second_address').value = item['address']['address_line_2'];
                                                                    document.getElementById('practitioner_locality').value = item['address']['suburb'];
                                                                    document.getElementById('practitioner_administrative_area_level_1').value = item['address']['state'];
                                                                    document.getElementById('practitioner_postal_code').value = item['address']['postcode'];
                                                                    document.getElementById('practitioner_country').value = item['address']['country'];
                                                                }
                                                                document.getElementById('agree_to_term_info').style.display = 'block';
                                                                $("#practitioner_password").val('');
                                                                $("#practitioner_password-confirm").val('');
                                                                location.href = '#practitoner_password';
                                                                document.getElementById('practitoner_password').focus();
                                                                document.getElementById("practitoner_company_name_display_area").style.display = 'block';
                                                                resolve();
                                                            }
                                                        }

                                                    })
                                                }
                                            })
                                        }

                                    }
                                }

                            });
                        }
                    }
                })
            } else {
                var new_status = 'true';
                $('#business_phone').attr('data-status', new_status);
                setSuccessFor(document.getElementById('business_phone').parentElement);
            }
        });
        document.getElementById('pharma_fax').addEventListener('change', function () {
            // if (this.value.length < 6) {
            //     Swal.fire({
            //         icon: 'error',
            //         title: 'Fax number is not valid',
            //     })
            //     document.getElementById('pharma_fax').value = '';
            // }
            var number = int_pharma_fax.getNumber();
            if (number) {
                $.ajax({
                    type: "POST",
                    url: '{{ url('phone_existence_validate') }}',
                    data: {
                        _token: '{{ csrf_token() }}',
                        type: 'landline',
                        phone: number,
                    },
                    timeout: 6000,
                    success: function (data) {
                        if (data.code == 2) {
                            Swal.fire({
                                icon: 'error',
                                title: 'Fax number is not valid',
                            })
                            setErrorFor(document.getElementById('pharma_fax').parentElement, 'Fax number is not valid');
                            var new_status = 'false';
                            $('#pharma_fax').attr('data-status', new_status);
                        } else {
                            var new_status = 'true';
                            $('#pharma_fax').attr('data-status', new_status);
                            setSuccessFor(document.getElementById('pharma_fax').parentElement);
                            var fax = int_pharma_fax.getNumber();
                            $.ajax({
                                type: "POST",
                                url: '{{ url('fax_check') }}',
                                data: {
                                    _token: '{{ csrf_token() }}',
                                    fax: fax,
                                },
                                timeout: 6000,
                                success: function (data) {
                                    var business = JSON.parse(data.business);
                                    if (data.code == '1') {
                                        if (business.length == 1) {
                                            Swal.fire({
                                                icon: 'info',
                                                text: 'The fax entered has an existing business "' + business[0]['trading_name'] + '" in our database. Continue with a new Account application for "' + document.getElementById('pharma_business_name').value + '" or link your practitioner details to "' + business[0]['trading_name'] + '"',
                                                showCancelButton: true,
                                                allowOutsideClick: false,
                                                confirmButtonText: 'Link to existing',
                                                cancelButtonText: 'New Account',
                                                reverseButtons: true

                                            }).then((result) => {
                                                if (result.value) {
                                                    document.getElementById('pharmaBusinessInfo').style.display = 'none';
                                                    document.getElementById('pharma_accountTermsForm').style.display = 'none';
                                                    $("#pharmaBusinessInfo input").prop('required', false);
                                                    $("#pharmaBusinessInfo select").prop('required', false);
                                                    $("#pharmaBusinessInfo textarea").prop('required', false);
                                                    document.getElementById('pharma_business_name').value = business[0]['customer_name'];
                                                    document.getElementById('pharma_company_name_display').value = business[0]['trading_name'];
                                                    document.getElementById("pharma_business_name").readOnly = true;
                                                    document.getElementById('pharma_street_address').value = business[0]['address']['address_line_1'];
                                                    document.getElementById('pharma_second_address').value = business[0]['address']['address_line_2'];
                                                    document.getElementById('pharma_locality').value = business[0]['address']['suburb'];
                                                    document.getElementById('pharma_administrative_area_level_1').value = business[0]['address']['state'];
                                                    document.getElementById('pharma_postal_code').value = business[0]['address']['postcode'];
                                                    document.getElementById('pharma_country').value = business[0]['address']['country'];
                                                }
                                                document.getElementById('pharma_business_info_check').style.display = 'none';
                                                document.getElementById('pharma_personal_info').style.display = 'block';
                                                if (document.getElementById('pharmaBusinessInfo').style.display == 'none') {
                                                    document.getElementById('pharma_personal_info_check_agree').style.display = 'block';
                                                } else {
                                                    document.getElementById('pharma_personal_info_check_account').style.display = 'block';
                                                }
                                                $("#pharma_password").val('');
                                                $("#pharma_password-confirm").val('');
                                                location.href = '#pharma_company_name_display_area';
                                                document.getElementById('pharma_firstname').focus();
                                                document.getElementById("pharma_company_name_display_area").style.display = 'block';
                                            })
                                        } else {
                                            var options = {};
                                            $.map(business,
                                                function (o) {
                                                    options[o.guid] = o.trading_name;
                                                });
                                            Swal.fire({
                                                icon: 'info',
                                                text: 'The fax entered has several existing business in our database. Continue with a new Account application for "' + document.getElementById('pharma_business_name').value + '" or choose a existing business to link your practitioner details to',
                                                input: 'select',
                                                inputOptions: options,    // inputOptions: {
                                                inputPlaceholder: 'Select a business',
                                                allowOutsideClick: false,
                                                showCancelButton: true,
                                                confirmButtonText: 'Link to existing',
                                                cancelButtonText: 'New Account',
                                                reverseButtons: true,
                                                // showLoaderOnConfirm: true,
                                                inputValidator: (value) => {
                                                    return new Promise((resolve) => {
                                                        business.forEach(updateAddress);

                                                        function updateAddress(item) {
                                                            if (item['guid'] == value) {
                                                                document.getElementById('pharmaBusinessInfo').style.display = 'none';
                                                                document.getElementById('pharma_accountTermsForm').style.display = 'none';
                                                                $("#pharmaBusinessInfo input").prop('required', false);
                                                                $("#pharmaBusinessInfo select").prop('required', false);
                                                                $("#pharmaBusinessInfo textarea").prop('required', false);
                                                                document.getElementById('pharma_business_name').value = item['trading_name'];
                                                                document.getElementById('pharma_company_name_display').value = item['trading_name'];
                                                                document.getElementById("pharma_business_name").readOnly = true;
                                                                if (item['address']) {
                                                                    document.getElementById('pharma_street_address').value = item['address']['address_line_1'];
                                                                    document.getElementById('pharma_second_address').value = item['address']['address_line_2'];
                                                                    document.getElementById('pharma_locality').value = item['address']['suburb'];
                                                                    document.getElementById('pharma_administrative_area_level_1').value = item['address']['state'];
                                                                    document.getElementById('pharma_postal_code').value = item['address']['postcode'];
                                                                    document.getElementById('pharma_country').value = item['address']['country'];
                                                                }
                                                                document.getElementById('pharma_business_info_check').style.display = 'none';
                                                                document.getElementById('pharma_personal_info').style.display = 'block';
                                                                if (document.getElementById('pharmaBusinessInfo').style.display == 'none') {
                                                                    document.getElementById('pharma_personal_info_check_agree').style.display = 'block';
                                                                } else {
                                                                    document.getElementById('pharma_personal_info_check_account').style.display = 'block';
                                                                }
                                                                $("#pharma_password").val('');
                                                                $("#pharma_password-confirm").val('');
                                                                location.href = '#pharma_company_name_display_area';
                                                                document.getElementById('pharma_firstname').focus();
                                                                document.getElementById("pharma_company_name_display_area").style.display = 'block';
                                                                resolve();
                                                            }
                                                        }

                                                    })
                                                }
                                            })
                                        }
                                    }
                                }

                            });
                        }
                    }
                })
            } else {
                var new_status = 'true';
                $('#pharma_fax').attr('data-status', new_status);
                setSuccessFor(document.getElementById('pharma_fax').parentElement);
            }
        });

        document.getElementById('pharma_acn').addEventListener('change', function () {
            $.ajax({
                type: "POST",
                url: '{{ url('acn_check') }}',
                data: {
                    _token: '{{ csrf_token() }}',
                    acn: this.value,
                },
                timeout: 6000,
                success: function (data) {
                    var business = JSON.parse(data.business);
                    if (data.code == '1') {
                        if (business.length == 1) {

                            Swal.fire({
                                icon: 'info',
                                text: 'The ACN entered has an existing business "' + business[0]['trading_name'] + '" in our database. Continue with a new Account application for "' + document.getElementById('pharma_business_name').value + '" or link your practitioner details to "' + business[0]['trading_name'] + '"',
                                showCancelButton: true,
                                allowOutsideClick: false,
                                confirmButtonText: 'Link to existing',
                                cancelButtonText: 'New Account',
                                reverseButtons: true

                            }).then((result) => {
                                if (result.value) {
                                    document.getElementById('pharmaBusinessInfo').style.display = 'none';
                                    document.getElementById('pharma_accountTermsForm').style.display = 'none';
                                    $("#pharmaBusinessInfo input").prop('required', false);
                                    $("#pharmaBusinessInfo select").prop('required', false);
                                    $("#pharmaBusinessInfo textarea").prop('required', false);
                                    document.getElementById('pharma_business_name').value = business[0]['customer_name'];
                                    document.getElementById('pharma_company_name_display').value = business[0]['trading_name'];
                                    document.getElementById("pharma_business_name").readOnly = true;
                                    document.getElementById('pharma_street_address').value = business[0]['address']['address_line_1'];
                                    document.getElementById('pharma_second_address').value = business[0]['address']['address_line_2'];
                                    document.getElementById('pharma_locality').value = business[0]['address']['suburb'];
                                    document.getElementById('pharma_administrative_area_level_1').value = business[0]['address']['state'];
                                    document.getElementById('pharma_postal_code').value = business[0]['address']['postcode'];
                                    document.getElementById('pharma_country').value = business[0]['address']['country'];
                                    document.getElementById('pharma_business_info_check').style.display = 'none';
                                    document.getElementById('pharma_personal_info').style.display = 'block';
                                    if (document.getElementById('pharmaBusinessInfo').style.display == 'none') {
                                        document.getElementById('pharma_personal_info_check_agree').style.display = 'block';
                                    } else {
                                        document.getElementById('pharma_personal_info_check_account').style.display = 'block';
                                    }
                                    $("#pharma_password").val('');
                                    $("#pharma_password-confirm").val('');
                                    location.href = '#pharma_company_name_display_area';
                                    document.getElementById('pharma_firstname').focus();
                                    document.getElementById("pharma_company_name_display_area").style.display = 'block';
                                }

                            })
                        } else {
                            var options = {};
                            $.map(business,
                                function (o) {
                                    options[o.guid] = o.trading_name;
                                });
                            Swal.fire({
                                icon: 'info',
                                text: 'The ACN entered has several existing business in our database. Continue with a new Account application for "' + document.getElementById('pharma_business_name').value + '" or choose a existing business to link your practitioner details to',
                                input: 'select',
                                inputOptions: options,    // inputOptions: {
                                inputPlaceholder: 'Select a business',
                                allowOutsideClick: false,
                                showCancelButton: true,
                                confirmButtonText: 'Link to existing',
                                cancelButtonText: 'New Account',
                                reverseButtons: true,
                                // showLoaderOnConfirm: true,
                                inputValidator: (value) => {
                                    return new Promise((resolve) => {
                                        business.forEach(updateAddress);

                                        function updateAddress(item) {
                                            if (item['guid'] == value) {
                                                document.getElementById('pharmaBusinessInfo').style.display = 'none';
                                                document.getElementById('pharma_accountTermsForm').style.display = 'none';
                                                $("#pharmaBusinessInfo input").prop('required', false);
                                                $("#pharmaBusinessInfo select").prop('required', false);
                                                $("#pharmaBusinessInfo textarea").prop('required', false);
                                                document.getElementById('pharma_business_name').value = item['customer_name'];
                                                document.getElementById('pharma_company_name_display').value = item['trading_name'];
                                                document.getElementById("pharma_business_name").readOnly = true;
                                                if (item['address']) {
                                                    document.getElementById('pharma_street_address').value = item['address']['address_line_1'];
                                                    document.getElementById('pharma_second_address').value = item['address']['address_line_2'];
                                                    document.getElementById('pharma_locality').value = item['address']['suburb'];
                                                    document.getElementById('pharma_administrative_area_level_1').value = item['address']['state'];
                                                    document.getElementById('pharma_postal_code').value = item['address']['postcode'];
                                                    document.getElementById('pharma_country').value = item['address']['country'];
                                                }
                                                document.getElementById('pharma_business_info_check').style.display = 'none';
                                                document.getElementById('pharma_personal_info').style.display = 'block';
                                                if (document.getElementById('pharmaBusinessInfo').style.display == 'none') {
                                                    document.getElementById('pharma_personal_info_check_agree').style.display = 'block';
                                                } else {
                                                    document.getElementById('pharma_personal_info_check_account').style.display = 'block';
                                                }
                                                $("#pharma_password").val('');
                                                $("#pharma_password-confirm").val('');
                                                location.href = '#pharma_company_name_display_area';
                                                document.getElementById('pharma_firstname').focus();
                                                document.getElementById("pharma_company_name_display_area").style.display = 'block';
                                                resolve();
                                            }
                                        }

                                    })
                                }
                            })
                        }
                    }
                }

            });
        });
        document.getElementById('pharma_business_number').addEventListener('change', function () {
            $.ajax({
                type: "POST",
                url: '{{ url('abn_check') }}',
                data: {
                    _token: '{{ csrf_token() }}',
                    abn: this.value,
                },
                timeout: 6000,
                success: function (data) {
                    var business = JSON.parse(data.business);
                    if (data.code == '1') {
                        if (business.length == 1) {
                            Swal.fire({
                                icon: 'info',
                                text: 'The business number entered has an existing business "' + business[0]['trading_name'] + '" in our database. Continue with a new Account application for "' + document.getElementById('pharma_business_name').value + '" or link your practitioner details to "' + business[0]['trading_name'] + '"',
                                showCancelButton: true,
                                allowOutsideClick: false,
                                confirmButtonText: 'Link to existing',
                                cancelButtonText: 'New Account',
                                reverseButtons: true

                            }).then((result) => {
                                if (result.value) {
                                    document.getElementById('pharmaBusinessInfo').style.display = 'none';
                                    document.getElementById('pharma_accountTermsForm').style.display = 'none';
                                    $("#pharmaBusinessInfo input").prop('required', false);
                                    $("#pharmaBusinessInfo select").prop('required', false);
                                    $("#pharmaBusinessInfo textarea").prop('required', false);
                                    document.getElementById('pharma_business_name').value = business[0]['customer_name'];
                                    document.getElementById('pharma_company_name_display').value = business[0]['trading_name'];
                                    document.getElementById("pharma_business_name").readOnly = true;
                                    document.getElementById('pharma_street_address').value = business[0]['address']['address_line_1'];
                                    document.getElementById('pharma_second_address').value = business[0]['address']['address_line_2'];
                                    document.getElementById('pharma_locality').value = business[0]['address']['suburb'];
                                    document.getElementById('pharma_administrative_area_level_1').value = business[0]['address']['state'];
                                    document.getElementById('pharma_postal_code').value = business[0]['address']['postcode'];
                                    document.getElementById('pharma_country').value = business[0]['address']['country'];
                                    document.getElementById('pharma_business_info_check').style.display = 'none';
                                    document.getElementById('pharma_personal_info').style.display = 'block';
                                    if (document.getElementById('pharmaBusinessInfo').style.display == 'none') {
                                        document.getElementById('pharma_personal_info_check_agree').style.display = 'block';
                                    } else {
                                        document.getElementById('pharma_personal_info_check_account').style.display = 'block';
                                    }
                                    $("#pharma_password").val('');
                                    $("#pharma_password-confirm").val('');
                                    location.href = '#pharma_company_name_display_area';
                                    document.getElementById('pharma_firstname').focus();
                                    document.getElementById("pharma_company_name_display_area").style.display = 'block';
                                }

                            })
                        } else {
                            var options = {};
                            $.map(business,
                                function (o) {
                                    options[o.guid] = o.trading_name;
                                });
                            Swal.fire({
                                icon: 'info',
                                text: 'The business number entered has several existing business in our database. Continue with a new Account application for "' + document.getElementById('pharma_business_name').value + '" or choose a existing business to link your practitioner details to',
                                input: 'select',
                                inputOptions: options,    // inputOptions: {
                                inputPlaceholder: 'Select a business',
                                allowOutsideClick: false,
                                showCancelButton: true,
                                confirmButtonText: 'Link to existing',
                                cancelButtonText: 'New Account',
                                reverseButtons: true,
                                // showLoaderOnConfirm: true,
                                inputValidator: (value) => {
                                    return new Promise((resolve) => {
                                        business.forEach(updateAddress);

                                        function updateAddress(item) {
                                            if (item['guid'] == value) {
                                                document.getElementById('pharmaBusinessInfo').style.display = 'none';
                                                document.getElementById('pharma_accountTermsForm').style.display = 'none';
                                                $("#pharmaBusinessInfo input").prop('required', false);
                                                $("#pharmaBusinessInfo select").prop('required', false);
                                                $("#pharmaBusinessInfo textarea").prop('required', false);
                                                document.getElementById('pharma_business_name').value = item['customer_name'];
                                                document.getElementById('pharma_company_name_display').value = item['trading_name'];
                                                document.getElementById("pharma_business_name").readOnly = true;
                                                if (item['address']) {
                                                    document.getElementById('pharma_street_address').value = item['address']['address_line_1'];
                                                    document.getElementById('pharma_second_address').value = item['address']['address_line_2'];
                                                    document.getElementById('pharma_locality').value = item['address']['suburb'];
                                                    document.getElementById('pharma_administrative_area_level_1').value = item['address']['state'];
                                                    document.getElementById('pharma_postal_code').value = item['address']['postcode'];
                                                    document.getElementById('pharma_country').value = item['address']['country'];
                                                }
                                                document.getElementById('pharma_business_info_check').style.display = 'none';
                                                document.getElementById('pharma_personal_info').style.display = 'block';
                                                if (document.getElementById('pharmaBusinessInfo').style.display == 'none') {
                                                    document.getElementById('pharma_personal_info_check_agree').style.display = 'block';
                                                } else {
                                                    document.getElementById('pharma_personal_info_check_account').style.display = 'block';
                                                }
                                                $("#pharma_password").val('');
                                                $("#pharma_password-confirm").val('');
                                                location.href = '#pharma_company_name_display_area';
                                                document.getElementById('pharma_firstname').focus();
                                                document.getElementById("pharma_company_name_display_area").style.display = 'block';
                                                resolve();
                                            }
                                        }

                                    })
                                }
                            })
                        }
                    }
                }

            });
        });
        document.getElementById('acn').addEventListener('change', function () {
            $.ajax({
                type: "POST",
                url: '{{ url('acn_check') }}',
                data: {
                    _token: '{{ csrf_token() }}',
                    acn: this.value,
                },
                timeout: 6000,
                success: function (data) {
                    var business = JSON.parse(data.business);
                    if (data.code == '1') {
                        if (business.length == 1) {
                            Swal.fire({
                                icon: 'info',
                                text: 'The ACN entered has an existing business "' + business[0]['trading_name'] + '" in our database. Continue with a new Account application for "' + document.getElementById('business_name').value + '" or link your practitioner details to "' + business[0]['trading_name'] + '"',
                                showCancelButton: true,
                                allowOutsideClick: false,
                                confirmButtonText: 'Link to existing',
                                cancelButtonText: 'New Account',
                                reverseButtons: true

                            }).then((result) => {
                                if (result.value) {
                                    document.getElementById('practitionerBusinessInfo').style.display = 'none';
                                    $("#practitionerBusinessInfo input").prop('required', false);
                                    $("#practitionerBusinessInfo select").prop('required', false);
                                    $("#practitionerBusinessInfo textarea").prop('required', false);
                                    document.getElementById('business_name').value = business[0]['customer_name'];
                                    document.getElementById('practitioner_company_name_display').value = business[0]['trading_name'];
                                    document.getElementById("business_name").readOnly = true;
                                    document.getElementById('practitioner_street_address').value = business[0]['address']['address_line_1'];
                                    document.getElementById('practitioner_second_address').value = business[0]['address']['address_line_2'];
                                    document.getElementById('practitioner_locality').value = business[0]['address']['suburb'];
                                    document.getElementById('practitioner_administrative_area_level_1').value = business[0]['address']['state'];
                                    document.getElementById('practitioner_postal_code').value = business[0]['address']['postcode'];
                                    document.getElementById('practitioner_country').value = business[0]['address']['country'];
                                    document.getElementById('agree_to_term_info').style.display = 'block';
                                    $("#practitioner_password").val('');
                                    $("#practitioner_password-confirm").val('');
                                    location.href = '#practitoner_password';
                                    document.getElementById('practitioner_password').focus();
                                    document.getElementById("practitioner_company_name_display_area").style.display = 'block';
                                }

                            })
                        } else {
                            var options = {};
                            $.map(business,
                                function (o) {
                                    options[o.guid] = o.trading_name;
                                });
                            Swal.fire({
                                icon: 'info',
                                text: 'The ACN entered has several existing business in our database. Continue with a new Account application for "' + document.getElementById('business_name').value + '" or choose a existing business to link your practitioner details to',
                                input: 'select',
                                inputOptions: options,    // inputOptions: {
                                inputPlaceholder: 'Select a business',
                                allowOutsideClick: false,
                                showCancelButton: true,
                                confirmButtonText: 'Link to existing',
                                cancelButtonText: 'New Account',
                                reverseButtons: true,
                                // showLoaderOnConfirm: true,
                                inputValidator: (value) => {
                                    return new Promise((resolve) => {
                                        business.forEach(updateAddress);

                                        function updateAddress(item) {
                                            if (item['guid'] == value) {
                                                document.getElementById('practitionerBusinessInfo').style.display = 'none';
                                                // document.getElementById('practitioner_accountTermsForm').style.display = 'none';
                                                $("#practitionerBusinessInfo input").prop('required', false);
                                                $("#practitionerBusinessInfo select").prop('required', false);
                                                $("#practitionerBusinessInfo textarea").prop('required', false);
                                                document.getElementById('business_name').value = item['customer_name'];
                                                document.getElementById('practitioner_company_name_display').value = item['trading_name'];
                                                document.getElementById("business_name").readOnly = true;
                                                if (item['address']) {
                                                    document.getElementById('practitioner_street_address').value = item['address']['address_line_1'];
                                                    document.getElementById('practitioner_second_address').value = item['address']['address_line_2'];
                                                    document.getElementById('practitioner_locality').value = item['address']['suburb'];
                                                    document.getElementById('practitioner_administrative_area_level_1').value = item['address']['state'];
                                                    document.getElementById('practitioner_postal_code').value = item['address']['postcode'];
                                                    document.getElementById('practitioner_country').value = item['address']['country'];
                                                }
                                                document.getElementById('agree_to_term_info').style.display = 'block';
                                                $("#practitioner_password").val('');
                                                $("#practitioner_password-confirm").val('');
                                                location.href = '#practitoner_password';
                                                document.getElementById('practitioner_password').focus();
                                                document.getElementById("practitioner_company_name_display_area").style.display = 'block';
                                                resolve();
                                            }
                                        }

                                    })
                                }
                            })
                        }
                    }
                }

            });
        });
        document.getElementById('business_number').addEventListener('change', function () {
            $.ajax({
                type: "POST",
                url: '{{ url('abn_check') }}',
                data: {
                    _token: '{{ csrf_token() }}',
                    abn: this.value,
                },
                timeout: 6000,
                success: function (data) {
                    var business = JSON.parse(data.business);
                    if (data.code == '1') {
                        if (business.length == 1) {
                            Swal.fire({
                                icon: 'info',
                                text: 'The business number entered has an existing business "' + business[0]['trading_name'] + '" in our database. Continue with a new Account application for "' + document.getElementById('business_name').value + '" or link your practitioner details to "' + business[0]['trading_name'] + '"',
                                showCancelButton: true,
                                allowOutsideClick: false,
                                confirmButtonText: 'Link to existing',
                                cancelButtonText: 'New Account',
                                reverseButtons: true

                            }).then((result) => {
                                console.log('it works');
                                if (result.value) {
                                    document.getElementById('practitionerBusinessInfo').style.display = 'none';
                                    $("#practitionerBusinessInfo input").prop('required', false);
                                    $("#practitionerBusinessInfo select").prop('required', false);
                                    $("#practitionerBusinessInfo textarea").prop('required', false);
                                    document.getElementById('business_name').value = business[0]['customer_name'];
                                    document.getElementById('practitioner_company_name_display').value = business[0]['trading_name'];
                                    document.getElementById("business_name").readOnly = true;
                                    document.getElementById('practitioner_street_address').value = business[0]['address']['address_line_1'];
                                    document.getElementById('practitioner_second_address').value = business[0]['address']['address_line_2'];
                                    document.getElementById('practitioner_locality').value = business[0]['address']['suburb'];
                                    document.getElementById('practitioner_administrative_area_level_1').value = business[0]['address']['state'];
                                    document.getElementById('practitioner_postal_code').value = business[0]['address']['postcode'];
                                    document.getElementById('practitioner_country').value = business[0]['address']['country'];
                                    document.getElementById('agree_to_term_info').style.display = 'block';
                                    $("#practitioner_password").val('');
                                    $("#practitioner_password-confirm").val('');
                                    location.href = '#practitoner_password';
                                    document.getElementById('practitioner_password').focus();
                                    document.getElementById("practitioner_company_name_display_area").style.display = 'block';
                                }

                            })
                        } else {
                            var options = {};
                            $.map(business,
                                function (o) {
                                    options[o.guid] = o.trading_name;
                                });
                            Swal.fire({
                                icon: 'info',
                                text: 'The business number entered has several existing business in our database. Continue with a new Account application for "' + document.getElementById('business_name').value + '" or choose a existing business to link your practitioner details to',
                                input: 'select',
                                inputOptions: options,    // inputOptions: {
                                inputPlaceholder: 'Select a business',
                                allowOutsideClick: false,
                                showCancelButton: true,
                                confirmButtonText: 'Link to existing',
                                cancelButtonText: 'New Account',
                                reverseButtons: true,
                                // showLoaderOnConfirm: true,
                                inputValidator: (value) => {
                                    return new Promise((resolve) => {
                                        business.forEach(updateAddress);

                                        function updateAddress(item) {
                                            if (item['guid'] == value) {
                                                document.getElementById('practitionerBusinessInfo').style.display = 'none';
                                                // document.getElementById('practitioner_accountTermsForm').style.display = 'none';
                                                $("#practitionerBusinessInfo input").prop('required', false);
                                                $("#practitionerBusinessInfo select").prop('required', false);
                                                $("#practitionerBusinessInfo textarea").prop('required', false);
                                                document.getElementById('business_name').value = item['customer_name'];
                                                document.getElementById('practitioner_company_name_display').value = item['trading_name'];
                                                document.getElementById("business_name").readOnly = true;
                                                if (item['address']) {
                                                    document.getElementById('practitioner_street_address').value = item['address']['address_line_1'];
                                                    document.getElementById('practitioner_second_address').value = item['address']['address_line_2'];
                                                    document.getElementById('practitioner_locality').value = item['address']['suburb'];
                                                    document.getElementById('practitioner_administrative_area_level_1').value = item['address']['state'];
                                                    document.getElementById('practitioner_postal_code').value = item['address']['postcode'];
                                                    document.getElementById('practitioner_country').value = item['address']['country'];
                                                }
                                                document.getElementById('agree_to_term_info').style.display = 'block';
                                                $("#practitioner_password").val('');
                                                $("#practitioner_password-confirm").val('');
                                                location.href = '#practitoner_password';
                                                document.getElementById('practitioner_password').focus();
                                                document.getElementById("practitioner_company_name_display_area").style.display = 'block';
                                                resolve();
                                            }
                                        }

                                    })
                                }
                            })
                        }
                    }
                }

            });
        });

        function stopAutoFillPractitioner() {
            document.getElementById('practitioner_street_address').removeAttribute('autocomplete');
            document.getElementById('practitioner_street_address').setAttribute('autocomplete', 'googleignoreautofill');
            document.getElementById('practitioner_locality').removeAttribute('autocomplete');
            document.getElementById('practitioner_locality').setAttribute('autocomplete', 'googleignoreautofill');
        }

        function stopAutoFillPharma() {
            document.getElementById('pharma_street_address').removeAttribute('autocomplete');
            document.getElementById('pharma_street_address').setAttribute('autocomplete', 'googleignoreautofill');
            document.getElementById('pharma_locality').removeAttribute('autocomplete');
            document.getElementById('pharma_locality').setAttribute('autocomplete', 'googleignoreautofill');
        }

        function stopAutoFillPatient() {
            document.getElementById('patient_street_address').removeAttribute('autocomplete');
            document.getElementById('patient_street_address').setAttribute('autocomplete', 'googleignoreautofill');
            document.getElementById('patient_locality').removeAttribute('autocomplete');
            document.getElementById('patient_locality').setAttribute('autocomplete', 'googleignoreautofill');
        }

        document.getElementById('practitioner_street_address').addEventListener("beforeinput", function () {
            $("#practitioner_locality_error").remove();
            document.getElementById('practitioner_locality').value = '';
            document.getElementById('practitioner_administrative_area_level_1').value = '';
            document.getElementById('practitioner_country').value = '';
            document.getElementById('practitioner_postal_code').value = '';
        })
        document.getElementById('practitioner_locality').addEventListener("input", function () {
            $("#practitioner_locality_error").remove();
            document.getElementById('practitioner_administrative_area_level_1').value = '';
            document.getElementById('practitioner_country').value = '';
            document.getElementById('practitioner_postal_code').value = '';
        })

        document.getElementById('patient_street_address').addEventListener("beforeinput", function () {
            $("#patient_locality_error").remove();
            document.getElementById('patient_locality').value = '';
            document.getElementById('patient_administrative_area_level_1').value = '';
            document.getElementById('patient_country').value = '';
            document.getElementById('patient_postal_code').value = '';
        })
        document.getElementById('patient_locality').addEventListener("input", function () {
            $("#patient_locality_error").remove();
            document.getElementById('patient_administrative_area_level_1').value = '';
            document.getElementById('patient_country').value = '';
            document.getElementById('patient_postal_code').value = '';
        })
        var pharma_international = document.querySelector("input[name=pharma_international]");

        google.maps.event.addDomListener(window, 'load', function () {

            var places = new google.maps.places.Autocomplete(document
                .getElementById('pharma_street_address'), {
                componentRestrictions: {
                    country: "au",
                }
            });
            pharma_international.addEventListener('change', function () {
                if (this.checked) {
                    // pharma_business_info_check
                    document.getElementById('pharma_search_box').style.display = 'none';
                    $("#pharma_business_search").prop('required', false);
                    document.getElementById('pharma_business_detail').style.display = 'block';
                    document.getElementById('pharma_business_sub_detail').style.display = 'block';
                    document.getElementById('pharma_business_info_check').style.display = 'block';
                    document.getElementById('pharma_business_detail').classList.remove('alert');
                    document.getElementById('pharma_business_detail').classList.remove('alert-blue');
                    document.getElementById('pharma_business_name').style.removeProperty('border');
                    document.getElementById('pharma_business_type').style.removeProperty('border');
                    document.getElementById('pharma_business_number').style.removeProperty('border');
                    document.getElementById('pharma_acn').style.removeProperty('border');
                    $('#pharma_business_name').prop('readonly', false);
                    $('#pharma_business_type').prop('readonly', false);
                    $('#pharma_business_number').prop('readonly', false);
                    $('#pharma_acn').prop('readonly', false);
                    document.getElementById('pharma_business_number_label').innerHTML = 'Business Number';
                    document.getElementById('pharma_acn_label').innerHTML = 'Company Number';
                    places.setComponentRestrictions({'country': []});
                    document.getElementById('pharma_association_area').style.display = 'block';
                    document.getElementById('pharma_ahpra_area').style.display = 'none';
                    $("#pharma_association_name").prop('required', true);
                    $("#pharma_association_number").prop('required', true);
                    $("#pharma_qualification").prop('required', true);
                } else {
                    document.getElementById('pharma_search_box').style.display = 'block';
                    $("#pharma_business_search").prop('required', true);
                    document.getElementById('pharma_business_detail').style.display = 'none';
                    document.getElementById('pharma_business_sub_detail').style.display = 'none';
                    document.getElementById('pharma_business_info_check').style.display = 'none';
                    document.getElementById('pharma_business_detail').classList.add('alert');
                    document.getElementById('pharma_business_detail').classList.add('alert-blue');
                    document.getElementById('pharma_business_name').style.border = 'unset';
                    document.getElementById('pharma_business_type').style.border = 'unset';
                    document.getElementById('pharma_business_number').style.border = 'unset';
                    document.getElementById('pharma_acn').style.border = 'unset';
                    $('#pharma_business_name').prop('readonly', true);
                    $('#pharma_business_type').prop('readonly', true);
                    $('#pharma_business_number').prop('readonly', true);
                    $('#pharma_acn').prop('readonly', true);
                    document.getElementById('pharma_business_number_label').innerHTML = 'Business Number (ABN/NZBN)';
                    document.getElementById('pharma_acn_label').innerHTML = 'Company Number (ACN)';
                    places.setComponentRestrictions({'country': 'au'});
                    document.getElementById('pharma_association_area').style.display = 'none';
                    document.getElementById('pharma_ahpra_area').style.display = 'block';
                    $("#pharma_association_name").prop('required', false);
                    $("#pharma_association_number").prop('required', false);
                    $("#pharma_qualification").prop('required', false);
                }
            });
            addGoogleListenerTotal('pharma', places);


        });
        google.maps.event.addDomListener(window, 'load', function () {

            var locality = new google.maps.places.Autocomplete((document
                .getElementById('pharma_locality')), {
                types: ['(regions)'],
                componentRestrictions: {
                    country: "au",
                }
            });
            pharma_international.addEventListener('change', function () {
                if (this.checked) {
                    // document.getElementById('pharma_search_business_area').style.display = 'none';
                    locality.setComponentRestrictions({'country': []});
                } else {
                    // document.getElementById('pharma_search_business_area').style.display = 'block';
                    locality.setComponentRestrictions({'country': 'au'});
                }
            });
            addGoogleListenerLocality('pharma', locality);


        });
        var practitioner_international = document.querySelector("input[name=practitioner_international]");

        google.maps.event.addDomListener(window, 'load', function () {

            var places = new google.maps.places.Autocomplete(document
                .getElementById('practitioner_street_address'), {
                componentRestrictions: {
                    country: "au",
                }
            });
            practitioner_international.addEventListener('change', function () {
                if (this.checked) {
                    document.getElementById('practitioner_search_box').style.display = 'none';
                    document.getElementById('practitioner_business_detail').style.display = 'block';
                    document.getElementById('practitioner_business_sub_detail').style.display = 'block';
                    document.getElementById('practitioner_business_detail').classList.remove('alert');
                    document.getElementById('practitioner_business_detail').classList.remove('alert-blue');
                    document.getElementById('business_name').style.removeProperty('border');
                    document.getElementById('business_type').style.removeProperty('border');
                    document.getElementById('business_number').style.removeProperty('border');
                    document.getElementById('acn').style.removeProperty('border');
                    $('#business_name').prop('readonly', false);
                    $('#business_type').prop('readonly', false);
                    $('#business_number').prop('readonly', false);
                    $('#acn').prop('readonly', false);
                    document.getElementById('business_number_label').innerHTML = 'Business Number';
                    document.getElementById('acn_label').innerHTML = 'Company Number';
                    places.setComponentRestrictions({'country': []});
                    document.getElementById('association_area').style.display = 'block';
                    document.getElementById('practitioner_ahpra_area').style.display = 'none';
                    $("#association_name").prop('required', true);
                    $("#association_number").prop('required', true);
                    $("#practitioner_qualification").prop('required', true);
                    document.getElementById('business_info_check').style.display = 'block';
                } else {
                    document.getElementById('practitioner_search_box').style.display = 'block';
                    document.getElementById('practitioner_business_detail').style.display = 'none';
                    document.getElementById('practitioner_business_sub_detail').style.display = 'none';
                    document.getElementById('practitioner_business_detail').classList.add('alert');
                    document.getElementById('practitioner_business_detail').classList.add('alert-blue');
                    document.getElementById('business_name').style.border = 'unset';
                    document.getElementById('business_type').style.border = 'unset';
                    document.getElementById('business_number').style.border = 'unset';
                    document.getElementById('acn').style.border = 'unset';
                    $('#business_name').prop('readonly', true);
                    $('#business_type').prop('readonly', true);
                    $('#business_number').prop('readonly', true);
                    $('#acn').prop('readonly', true);
                    document.getElementById('business_number_label').innerHTML = 'Business Number (ABN/NZBN)';
                    document.getElementById('acn_label').innerHTML = 'Company Number (ACN)';
                    places.setComponentRestrictions({'country': 'au'});
                    document.getElementById('association_area').style.display = 'none';
                    document.getElementById('practitioner_ahpra_area').style.display = 'block';
                    $("#association_name").prop('required', false);
                    $("#association_number").prop('required', false);
                    $("#practitioner_qualification").prop('required', false);
                    document.getElementById('business_info_check').style.display = 'none';
                }
            });
            addGoogleListenerTotal('practitioner', places);


        });
        google.maps.event.addDomListener(window, 'load', function () {

            var locality = new google.maps.places.Autocomplete((document
                .getElementById('practitioner_locality')), {
                types: ['(regions)'],
                componentRestrictions: {
                    country: "au",
                }
            });
            practitioner_international.addEventListener('change', function () {
                if (this.checked) {
                    locality.setComponentRestrictions({'country': []});
                } else {
                    locality.setComponentRestrictions({'country': 'au'});
                }
            });
            addGoogleListenerLocality('practitioner', locality);


        });


        var patient_international = document.querySelector("input[name=patient_international]");

        google.maps.event.addDomListener(window, 'load', function () {

            var places = new google.maps.places.Autocomplete(document
                .getElementById('patient_street_address'), {
                componentRestrictions: {
                    country: "au",
                }
            });
            patient_international.addEventListener('change', function () {
                if (this.checked) {
                    places.setComponentRestrictions({'country': []});
                } else {
                    places.setComponentRestrictions({'country': 'au'});
                }
            });
            addGoogleListenerTotal('patient', places);


        });
        google.maps.event.addDomListener(window, 'load', function () {

            var locality = new google.maps.places.Autocomplete((document
                .getElementById('patient_locality')), {
                types: ['(regions)'],
                componentRestrictions: {
                    country: "au",
                }
            });
            practitioner_international.addEventListener('change', function () {
                if (this.checked) {
                    locality.setComponentRestrictions({'country': []});
                } else {
                    locality.setComponentRestrictions({'country': 'au'});
                }
            });
            addGoogleListenerLocality('patient', locality);


        });

        function addGoogleListenerLocality(role, locality) {
            google.maps.event.addListener(locality, 'place_changed', function () {
                var place = locality.getPlace();
                // var value = place.address_components;

                // var  value = address.split(",");
                // console.log(place.address_components)
                // console.log(place.address_components);
                var componentForm = {
                    // street_number: 'short_name',
                    // route: 'long_name',
                    locality: 'long_name',
                    sublocality_level_2: 'long_name',
                    administrative_area_level_1: 'short_name',
                    country: 'long_name',
                    postal_code: 'short_name'
                };

                // document.getElementById('street_address').value = '';

                // document.getElementById('administrative_area_level_1').value = '';
                // document.getElementById('country').value = '';
                // document.getElementById('postal_code').value = '';
                var a = 0;
                for (var i = 0; i < place.address_components.length; i++) {
                    var addressType = place.address_components[i].types[0];
                    if (componentForm[addressType]) {
                        var val = place.address_components[i][componentForm[addressType]];
                        if (addressType == 'street_number' || addressType == 'route') {
                            // document.getElementById(role + '_' + 'street_address').value = val;
                        }
                            // else if (addressType == 'route') {
                            //     document.getElementById(role + '_' + 'street_address').value += (' ' + val);
                        // }
                        else if (addressType == 'locality' || addressType == "sublocality_level_2") {
                            document.getElementById(role + '_' + 'locality').value = val;
                            a = 1;
                        } else {
                            document.getElementById(role + '_' + addressType).value = val;
                        }
                    }
                }
                document.getElementById(role + '_' + 'lat').value = place.geometry.location.lat();
                document.getElementById(role + '_' + 'lng').value = place.geometry.location.lng();
                if (a == 0) {
                    document.getElementById(role + '_' + 'locality').value = '';
                }
                if (!document.getElementById(role + '_' + 'international').checked) {
                    if (document.getElementById(role + '_' + 'locality').value == '' ||
                        document.getElementById(role + '_' + 'administrative_area_level_1').value == '' ||
                        document.getElementById(role + '_' + 'country').value == '') {
                        $('#' + role + '_locality_row').append('<div class="alert alert-red" style="margin-top: 40px" id=' + role + '_locality_error><p>PLEASE SELECT ANOTHER SUBURB WITH A POSTCODE</p></div>')
                    }
                }
                if (role == 'practitioner') {
                    $.ajax({
                        type: "POST",
                        url: '{{ url('get_association') }}',
                        data: {
                            _token: '{{ csrf_token() }}',
                            country: document.getElementById(role + '_' + 'country').value,
                        },
                        timeout: 6000,
                        error: function (request, error) {
                            if (error == "timeout") {
                                $('#err-timedout').slideDown('slow');
                            } else {
                                $('#err-state').slideDown('slow');
                                $("#err-state").html('An error occurred: ' + error + '');
                            }
                        },
                        success: function (data) {
                            $('#association_name').empty();
                            // $("#association_name").append('<option disabled value> -- select an option --</option>')
                            $.each(data.data, function () {
                                $("#association_name").append('<option value="' + this + '">' + this + '</option>')

                            })
                            $("#association_name").append('<option selected value="Other">Other</option>')
                        }

                    });
                }
                if (role == 'pharma') {
                    $.ajax({
                        type: "POST",
                        url: '{{ url('get_association') }}',
                        data: {
                            _token: '{{ csrf_token() }}',
                            country: document.getElementById(role + '_' + 'country').value,
                        },
                        timeout: 6000,
                        error: function (request, error) {
                            if (error == "timeout") {
                                $('#err-timedout').slideDown('slow');
                            } else {
                                $('#err-state').slideDown('slow');
                                $("#err-state").html('An error occurred: ' + error + '');
                            }
                        },
                        success: function (data) {
                            $('#pharma_association_name').empty();
                            $("#pharma_association_name").append('<option disabled selected value> -- select an option --</option>')
                            $.each(data.data, function () {
                                $("#pharma_association_name").append('<option value="' + this + '">' + this + '</option>')

                            })


                            $("#pharma_association_name").append('<option value="Other">Other</option>')
                        }

                    });
                }
            });

        }

        function addGoogleListenerTotal(role, places) {

            google.maps.event.addListener(places, 'place_changed', function () {
                var place = places.getPlace();
                console.log(place.address_components);

                // var  value = address.split(",");
                var componentForm = {
                    street_number: 'short_name',
                    route: 'long_name',
                    locality: 'long_name',
                    sublocality_level_2: 'long_name',
                    administrative_area_level_1: 'short_name',
                    country: 'long_name',
                    postal_code: 'short_name'
                };

                for (var i = 0; i < place.address_components.length; i++) {
                    var addressType = place.address_components[i].types[0];
                    if (componentForm[addressType]) {
                        var val = place.address_components[i][componentForm[addressType]];
                        if (addressType == 'street_number') {
                            document.getElementById(role + '_' + 'street_address').value = val;
                        } else if (addressType == 'route') {
                            document.getElementById(role + '_' + 'street_address').value += (' ' + val);
                        } else if (addressType == 'locality' || addressType == "sublocality_level_2") {
                            document.getElementById(role + '_' + 'locality').value = val;
                        } else {
                            document.getElementById(role + '_' + addressType).value = val;
                        }
                    }
                }
                document.getElementById(role + '_' + 'lat').value = place.geometry.location.lat();
                document.getElementById(role + '_' + 'lng').value = place.geometry.location.lng();
                // document.getElementById(role + '_' + 'search_business_area').style.display = 'block';
                if (!document.getElementById(role + '_' + 'international').checked) {
                    if (document.getElementById(role + '_' + 'locality').value == '' ||
                        document.getElementById(role + '_' + 'administrative_area_level_1').value == '' ||
                        document.getElementById(role + '_' + 'country').value == '') {
                        $('#' + role + '_locality_row').append('<div class="alert alert-red" style="margin-top: 40px" id=' + role + '_locality_error><p>PLEASE SELECT ANOTHER SUBURB WITH A POSTCODE</p></div>')
                    }
                }
                if (role == 'practitioner') {
                    document.getElementById(role + '_' + 'search_business_area').style.display = 'block';
                    location.href = '#practitioner_business_search';
                    document.getElementById('practitioner_business_search').focus();
                    $.ajax({
                        type: "POST",
                        url: '{{ url('get_association') }}',
                        data: {
                            _token: '{{ csrf_token() }}',
                            country: document.getElementById(role + '_' + 'country').value,
                        },
                        timeout: 6000,
                        error: function (request, error) {
                            if (error == "timeout") {
                                $('#err-timedout').slideDown('slow');
                            } else {
                                $('#err-state').slideDown('slow');
                                $("#err-state").html('An error occurred: ' + error + '');
                            }
                        },
                        success: function (data) {
                            $('#association_name').empty();
                            // $("#association_name").append('<option disabled selected value> -- select an option --</option>')
                            $.each(data.data, function () {
                                $("#association_name").append('<option value="' + this + '">' + this + '</option>')

                            })

                            $("#association_name").append('<option selected value="Other">Other</option>')
                        }

                    });
                    $.ajax({
                        type: "POST",
                        url: '{{ url('address_check') }}',
                        data: {
                            _token: '{{ csrf_token() }}',
                            address: document.getElementById(role + '_' + 'street_address').value,
                        },
                        timeout: 6000,

                        success: function (data) {
                            if (data.code == 1) {
                                var business = JSON.parse(data.business);
                                if (business.length == 1) {
                                    Swal.fire({
                                        icon: 'info',
                                        text: 'The address entered has an existing business "' + business[0]['trading_name'] + '" in our database. Continue with a new Account application for "' + document.getElementById('business_name').value + '" or link your practitioner details to "' + business[0]['trading_name'] + '"',
                                        showCancelButton: true,
                                        allowOutsideClick: false,
                                        confirmButtonText: 'Link to existing',
                                        cancelButtonText: 'New Account',
                                        reverseButtons: true

                                    }).then((result) => {
                                        if (result.value) {
                                            document.getElementById('practitionerBusinessInfo').style.display = 'none';
                                            $("#practitionerBusinessInfo input").prop('required', false);
                                            $("#practitionerBusinessInfo select").prop('required', false);
                                            $("#practitionerBusinessInfo textarea").prop('required', false);
                                            document.getElementById('business_name').value = business[0]['customer_name'];
                                            document.getElementById('practitioner_company_name_display').value = business[0]['trading_name'];
                                            document.getElementById("business_name").readOnly = true;
                                            document.getElementById('practitioner_street_address').value = business[0]['address']['address_line_1'];
                                            document.getElementById('practitioner_second_address').value = business[0]['address']['address_line_2'];
                                            document.getElementById('practitioner_locality').value = business[0]['address']['suburb'];
                                            document.getElementById('practitioner_administrative_area_level_1').value = business[0]['address']['state'];
                                            document.getElementById('practitioner_postal_code').value = business[0]['address']['postcode'];
                                            document.getElementById('practitioner_country').value = business[0]['address']['country'];
                                            document.getElementById('agree_to_term_info').style.display = 'block';
                                            $("#practitioner_password").val('');
                                            $("#practitioner_password-confirm").val('');
                                            location.href = '#practitoner_password';
                                            document.getElementById('practitioner_password').focus();
                                            document.getElementById("practitioner_company_name_display_area").style.display = 'block';

                                        }
                                    })
                                } else {
                                    var options = {};
                                    $.map(business,
                                        function (o) {
                                            options[o.guid] = o.trading_name;
                                        });
                                    Swal.fire({
                                        icon: 'info',
                                        text: 'The address entered has several existing business in our database. Continue with a new Account application for "' + document.getElementById('business_name').value + '" or choose a existing business to link your practitioner details to',
                                        input: 'select',
                                        inputOptions: options,    // inputOptions: {
                                        inputPlaceholder: 'Select a business',
                                        allowOutsideClick: false,
                                        showCancelButton: true,
                                        confirmButtonText: 'Link to existing',
                                        cancelButtonText: 'New Account',
                                        reverseButtons: true,
                                        inputValidator: (value) => {
                                            return new Promise((resolve) => {
                                                business.forEach(updateAddress);

                                                function updateAddress(item) {
                                                    if (item['guid'] == value) {
                                                        document.getElementById('practitionerBusinessInfo').style.display = 'none';
                                                        // document.getElementById('practitioner_accountTermsForm').style.display = 'none';
                                                        $("#practitionerBusinessInfo input").prop('required', false);
                                                        $("#practitionerBusinessInfo select").prop('required', false);
                                                        $("#practitionerBusinessInfo textarea").prop('required', false);
                                                        document.getElementById('business_name').value = item['customer_name'];
                                                        document.getElementById('practitioner_company_name_display').value = item['trading_name'];
                                                        document.getElementById("business_name").readOnly = true;
                                                        if (item['address']) {
                                                            document.getElementById('practitioner_street_address').value = item['address']['address_line_1'];
                                                            document.getElementById('practitioner_second_address').value = item['address']['address_line_2'];
                                                            document.getElementById('practitioner_locality').value = item['address']['suburb'];
                                                            document.getElementById('practitioner_administrative_area_level_1').value = item['address']['state'];
                                                            document.getElementById('practitioner_postal_code').value = item['address']['postcode'];
                                                            document.getElementById('practitioner_country').value = item['address']['country'];
                                                        }
                                                        document.getElementById('agree_to_term_info').style.display = 'block';
                                                        $("#practitioner_password").val('');
                                                        $("#practitioner_password-confirm").val('');
                                                        location.href = '#practitoner_password';
                                                        document.getElementById('practitioner_password').focus();
                                                        document.getElementById("practitioner_company_name_display_area").style.display = 'block';
                                                        resolve();
                                                    }
                                                }

                                            })
                                        }
                                    })
                                }
                            }
                        }

                    });
                }

                if (role == 'pharma') {
                    document.getElementById(role + '_' + 'search_business_area').style.display = 'block';
                    location.href = '#pharma_business_search';
                    document.getElementById('pharma_business_search').focus();
                    $.ajax({
                        type: "POST",
                        url: '{{ url('get_association') }}',
                        data: {
                            _token: '{{ csrf_token() }}',
                            country: document.getElementById(role + '_' + 'country').value,
                        },
                        timeout: 6000,
                        error: function (request, error) {
                            if (error == "timeout") {
                                $('#err-timedout').slideDown('slow');
                            } else {
                                $('#err-state').slideDown('slow');
                                $("#err-state").html('An error occurred: ' + error + '');
                            }
                        },
                        success: function (data) {
                            $('#pharma_association_name').empty();
                            $("#pharma_association_name").append('<option disabled selected value> -- select an option --</option>')
                            $.each(data.data, function () {
                                $("#pharma_association_name").append('<option value="' + this + '">' + this + '</option>')

                            })

                            $("#pharma_association_name").append('<option value="Other">Other</option>')
                        }

                    });
                    $.ajax({
                        type: "POST",
                        url: '{{ url('address_check') }}',
                        data: {
                            _token: '{{ csrf_token() }}',
                            address: document.getElementById(role + '_' + 'street_address').value,
                        },
                        timeout: 6000,

                        success: function (data) {
                            if (data.code == 1) {
                                var business = JSON.parse(data.business);
                                if (business.length == 1) {
                                    Swal.fire({
                                        icon: 'info',
                                        text: 'The address entered has an existing business "' + business[0]['trading_name'] + '" in our database. Continue with a new Account application for "' + document.getElementById('pharma_business_name').value + '" or link your practitioner details to "' + business[0]['trading_name'] + '"',
                                        showCancelButton: true,
                                        allowOutsideClick: false,
                                        confirmButtonText: 'Link to existing',
                                        cancelButtonText: 'New Account',
                                        reverseButtons: true


                                    }).then((result) => {
                                        if (result.value) {
                                            document.getElementById('pharmaBusinessInfo').style.display = 'none';
                                            document.getElementById('pharma_accountTermsForm').style.display = 'none';
                                            $("#pharmaBusinessInfo input").prop('required', false);
                                            $("#pharmaBusinessInfo textarea").prop('required', false);
                                            $("#pharmaBusinessInfo select").prop('required', false);
                                            document.getElementById('pharma_business_name').value = business[0]['customer_name'];
                                            document.getElementById('pharma_company_name_display').value = business[0]['trading_name'];
                                            document.getElementById("pharma_business_name").readOnly = true;
                                            document.getElementById('pharma_street_address').value = business[0]['address']['address_line_1'];
                                            document.getElementById('pharma_second_address').value = business[0]['address']['address_line_2'];
                                            document.getElementById('pharma_locality').value = business[0]['address']['suburb'];
                                            document.getElementById('pharma_administrative_area_level_1').value = business[0]['address']['state'];
                                            document.getElementById('pharma_postal_code').value = business[0]['address']['postcode'];
                                            document.getElementById('pharma_country').value = business[0]['address']['country'];
                                            document.getElementById('pharma_business_info_check').style.display = 'none';
                                            document.getElementById('pharma_personal_info').style.display = 'block';
                                            if (document.getElementById('pharmaBusinessInfo').style.display == 'none') {
                                                document.getElementById('pharma_personal_info_check_agree').style.display = 'block';
                                            } else {
                                                document.getElementById('pharma_personal_info_check_account').style.display = 'block';
                                            }
                                            $("#pharma_password").val('');
                                            $("#pharma_password-confirm").val('');
                                            location.href = '#pharma_company_name_display_area';
                                            document.getElementById('pharma_firstname').focus();
                                            document.getElementById("pharma_company_name_display_area").style.display = 'block';

                                        }
                                    })
                                } else {
                                    var options = {};
                                    $.map(business,
                                        function (o) {
                                            options[o.guid] = o.trading_name;
                                        });
                                    Swal.fire({
                                        icon: 'info',
                                        text: 'The address entered has several existing business in our database. Continue with a new Account application for "' + document.getElementById('pharma_business_name').value + '" or choose a existing business to link your practitioner details to',
                                        input: 'select',
                                        inputOptions: options,    // inputOptions: {
                                        inputPlaceholder: 'Select a business',
                                        allowOutsideClick: false,
                                        showCancelButton: true,
                                        confirmButtonText: 'Link to existing',
                                        cancelButtonText: 'New Account',
                                        reverseButtons: true,
                                        inputValidator: (value) => {
                                            return new Promise((resolve) => {
                                                business.forEach(updateAddress);
                                                function updateAddress(item) {
                                                    if (item['guid'] == value) {
                                                        document.getElementById('pharmaBusinessInfo').style.display = 'none';
                                                        document.getElementById('pharma_accountTermsForm').style.display = 'none';
                                                        $("#pharmaBusinessInfo input").prop('required', false);
                                                        $("#pharmaBusinessInfo select").prop('required', false);
                                                        $("#pharmaBusinessInfo textarea").prop('required', false);
                                                        document.getElementById('pharma_business_name').value = item['customer_name'];
                                                        document.getElementById('pharma_company_name_display').value = item['trading_name'];
                                                        document.getElementById("pharma_business_name").readOnly = true;
                                                        if (item['address']) {
                                                            document.getElementById('pharma_street_address').value = item['address']['address_line_1'];
                                                            document.getElementById('pharma_second_address').value = item['address']['address_line_2'];
                                                            document.getElementById('pharma_locality').value = item['address']['suburb'];
                                                            document.getElementById('pharma_administrative_area_level_1').value = item['address']['state'];
                                                            document.getElementById('pharma_postal_code').value = item['address']['postcode'];
                                                            document.getElementById('pharma_country').value = item['address']['country'];
                                                        }
                                                        document.getElementById('pharma_business_info_check').style.display = 'none';
                                                        document.getElementById('pharma_personal_info').style.display = 'block';
                                                        if (document.getElementById('pharmaBusinessInfo').style.display == 'none') {
                                                            document.getElementById('pharma_personal_info_check_agree').style.display = 'block';
                                                        } else {
                                                            document.getElementById('pharma_personal_info_check_account').style.display = 'block';
                                                        }
                                                        $("#pharma_password").val('');
                                                        $("#pharma_password-confirm").val('');
                                                        location.href = '#pharma_company_name_display_area';
                                                        document.getElementById('pharma_firstname').focus();
                                                        document.getElementById("pharma_company_name_display_area").style.display = 'block';
                                                        resolve();
                                                    }
                                                }

                                            })
                                        }
                                    })
                                }
                            }
                        }

                    });
                }
            });

        }


        // on submit

        function submitRegisterForm() {
            // // call function from form-valdate.js
            // validateInputs();
            // console.log('1');

            event.preventDefault();
            $('#pharma_phone').attr('type', 'text');
            $('#pharma_mobile').attr('type', 'text');
            $('#pharma_business_phone').attr('type', 'text');
            $('#pharma_fax').attr('type', 'text');
            $('#practitioner_phone').attr('type', 'text');
            $('#practitioner_mobile').attr('type', 'text');
            $('#business_phone').attr('type', 'text');
            // $('#fax').attr('type', 'text');
            $('#patient_phone').attr('type', 'text');
            $('#patient_mobile').attr('type', 'text');

            document.getElementById('pharma_phone').value = int_pharma_phone.getNumber();
            document.getElementById('pharma_mobile').value = int_pharma_mobile.getNumber();

            document.getElementById('pharma_business_phone').value = int_pharma_business_phone.getNumber();
            document.getElementById('pharma_fax').value = int_pharma_fax.getNumber();
            document.getElementById('practitioner_phone').value = int_practitioner_phone.getNumber();
            document.getElementById('practitioner_mobile').value = int_practitioner_mobile.getNumber();
            document.getElementById('business_phone').value = int_business_phone.getNumber();
            // document.getElementById('fax').value = int_fax.getNumber();
            document.getElementById('patient_phone').value = int_patient_phone.getNumber();
            document.getElementById('patient_mobile').value = int_patient_mobile.getNumber();
            if (document.getElementById('practitioner_firstname').value) {
                // var passw = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,20}$/;
                var password = document.getElementById("practitioner_password").value;
                var confirmPassword = document.getElementById("practitioner_password-confirm").value;
                if (password != confirmPassword) {
                    Swal.fire({
                        title: 'Error',
                        text: "Passwords do not match, please try again",
                        icon: 'error',
                    })
                } else if (password.length < 8) {
                    Swal.fire({
                        title: 'Password too weak',
                        text: "Password needs to be 8 or more characters",
                        icon: 'error',
                    })
                } else {
                    if (document.getElementById('if_from_au_yes').checked) {
                        // document.getElementById('association_name').selectedIndex = 0;
                        document.getElementById('association_number').value = '';
                    } else {
                        document.getElementById('practitioner_ahpra_number').value = '';
                    }

                    Swal.fire({
                        title: 'Processing ..',
                        text: "Please give us some time to verify your application, we'll send you an email when your account is ready",
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        showConfirmButton: false,
                        showCloseButton: false,
                        showCancelButton: false,
                        onOpen: () => {
                            Swal.showLoading();
                            return document.getElementById("ajax-form").submit()
                        }
                    })
                }

            } else if (document.getElementById('pharma_firstname').value) {
                var passw = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/;
                var password = document.getElementById("pharma_password").value;
                var confirmPassword = document.getElementById("pharma_password-confirm").value;
                if (password != confirmPassword) {
                    Swal.fire({
                        title: 'Error',
                        text: "Passwords do not match, please try again",
                        icon: 'error',
                    })
                } else if (password.length < 8) {
                    Swal.fire({
                        title: 'Password too weak',
                        text: "Password needs to be 8 or more characters",
                        icon: 'error',
                    })
                } else {
                    Swal.fire({
                        title: 'Processing ..',
                        text: "Please give us some time to verify your application, we'll send you an email when your account is ready",
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        showConfirmButton: false,
                        showCloseButton: false,
                        showCancelButton: false,
                        onOpen: () => {
                            Swal.showLoading();
                            return document.getElementById("ajax-form").submit()
                        }
                    })


                }

            } else {
                var passw = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/;
                var password = document.getElementById("patient_password").value;
                var confirmPassword = document.getElementById("patient_password-confirm").value;
                if (password != confirmPassword) {
                    Swal.fire({
                        title: 'Error',
                        text: "Passwords do not match, please try again",
                        icon: 'error',
                    })
                } else if (password.length < 8) {
                    Swal.fire({
                        title: 'Password too weak',
                        text: "Password needs to be 8 or more characters",
                        icon: 'error',
                    })
                } else {
                    Swal.fire({
                        title: 'Thanks for your registration',
                        icon: 'success',
                        confirmButtonText: 'OK',
                        allowOutsideClick: false
                    }).then((result) => {
                        if (result.value) {
                            document.getElementById("ajax-form").submit();
                        }
                    })
                }
            }
            // } else {
            //     alert(document.getElementById('example').validationMessage);
            // }


        }

        var pharma_phone = document.querySelector("#pharma_phone");
        var pharma_mobile = document.querySelector("#pharma_mobile");
        var pharma_business_phone = document.querySelector("#pharma_business_phone");
        var pharma_fax = document.querySelector("#pharma_fax");
        var practitioner_phone = document.querySelector("#practitioner_phone");
        var practitioner_mobile = document.querySelector("#practitioner_mobile");
        var business_phone = document.querySelector("#business_phone");
        // var fax = document.querySelector("#fax");
        var patient_phone = document.querySelector("#patient_phone");
        var patient_mobile = document.querySelector("#patient_mobile");
        var int_practitioner_phone = window.intlTelInput(practitioner_phone, {
            customPlaceholder: function (selectedCountryPlaceholder, selectedCountryData) {
                if (selectedCountryData['name'] == 'Australia') {
                    return '1300 369 570';
                } else {
                    return selectedCountryPlaceholder;
                }
            },
            // allowDropdown: true,
            // autoHideDialCode: false,
            // autoPlaceholder: "aggressive",
            // dropdownContainer: document.body,
            // excludeCountries: ["us"],
            // formatOnDisplay: false,
            // geoIpLookup: function(callback) {
            //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
            //     var countryCode = (resp && resp.country) ? resp.country : "";
            //     callback(countryCode);
            //   });
            // },
            // hiddenInput: "full_number",
            initialCountry: "au",
            // localizedCountries: { 'de': 'Deutschland' },
            // nationalMode: false,
            // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
            // placeholderNumberType: "MOBILE",
            // preferredCountries: ['cn', 'jp'],
            separateDialCode: true,
            utilsScript: "{{ asset('build/js/utils.js') }}",
        });
        var int_pharma_phone = window.intlTelInput(pharma_phone, {
            customPlaceholder: function (selectedCountryPlaceholder, selectedCountryData) {
                if (selectedCountryData['name'] == 'Australia') {
                    return '1300 369 570';
                } else {
                    return selectedCountryPlaceholder;
                }
            },
            initialCountry: "au",
            separateDialCode: true,
            utilsScript: "{{ asset('build/js/utils.js') }}",
        });
            {{--var int_fax = window.intlTelInput(fax, {--}}
            {{--    initialCountry: "au",--}}
            {{--    separateDialCode: true,--}}
            {{--    utilsScript: "{{ asset('build/js/utils.js') }}",--}}
            {{--});--}}
        var int_pharma_fax = window.intlTelInput(pharma_fax, {
                customPlaceholder: function (selectedCountryPlaceholder, selectedCountryData) {
                    if (selectedCountryData['name'] == 'Australia') {
                        return '02 9699 3347';
                    } else {
                        return selectedCountryPlaceholder;
                    }
                },
                initialCountry: "au",
                separateDialCode: true,
                utilsScript: "{{ asset('build/js/utils.js') }}",
            });
        var int_patient_phone = window.intlTelInput(patient_phone, {
            customPlaceholder: function (selectedCountryPlaceholder, selectedCountryData) {
                if (selectedCountryData['name'] == 'Australia') {
                    return '1300 369 570';
                } else {
                    return selectedCountryPlaceholder;
                }
            },
            initialCountry: "au",
            separateDialCode: true,
            utilsScript: "{{ asset('build/js/utils.js') }}",
        });
        var int_patient_mobile = window.intlTelInput(patient_mobile, {
            customPlaceholder: function (selectedCountryPlaceholder, selectedCountryData) {
                if (selectedCountryData['name'] == 'Australia') {
                    return '0402 000 000';
                } else {
                    return selectedCountryPlaceholder;
                }
            },
            initialCountry: "au",
            separateDialCode: true,
            utilsScript: "{{ asset('build/js/utils.js') }}",
        });
        var int_practitioner_mobile = window.intlTelInput(practitioner_mobile, {
            customPlaceholder: function (selectedCountryPlaceholder, selectedCountryData) {
                if (selectedCountryData['name'] == 'Australia') {
                    return '0402 000 000';
                } else {
                    return selectedCountryPlaceholder;
                }
            },
            initialCountry: "au",
            separateDialCode: true,
            utilsScript: "{{ asset('build/js/utils.js') }}",
        });
        var int_business_phone = window.intlTelInput(business_phone, {
            customPlaceholder: function (selectedCountryPlaceholder, selectedCountryData) {
                if (selectedCountryData['name'] == 'Australia') {
                    return '1300 369 570';
                } else {
                    return selectedCountryPlaceholder;
                }
            },
            initialCountry: "au",
            separateDialCode: true,
            utilsScript: "{{ asset('build/js/utils.js') }}",
        });
        var int_pharma_mobile = window.intlTelInput(pharma_mobile, {
            customPlaceholder: function (selectedCountryPlaceholder, selectedCountryData) {
                if (selectedCountryData['name'] == 'Australia') {
                    return '0402 000 000';
                } else {
                    return selectedCountryPlaceholder;
                }
            },
            initialCountry: "au",
            separateDialCode: true,
            utilsScript: "{{ asset('build/js/utils.js') }}",
        });
        var int_pharma_business_phone = window.intlTelInput(pharma_business_phone, {
            customPlaceholder: function (selectedCountryPlaceholder, selectedCountryData) {
                if (selectedCountryData['name'] == 'Australia') {
                    return '1300 369 570';
                } else {
                    return selectedCountryPlaceholder;
                }
            },
            initialCountry: "au",
            separateDialCode: true,
            utilsScript: "{{ asset('build/js/utils.js') }}",
        });
    </script>


    <!-- End Document
    ================================================== -->
@endsection


