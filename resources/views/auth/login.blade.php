@extends('vms.layouts.main')
@section('title')
    Login
@endsection
@section('content')


    <main class="cd-main-content" id="main" style="margin-top:90px">

        <section class="section white-section section-padding-top-bottom" id="scroll-link-6">


            <div class="container">
                <div class="sixteen columns">
                    <div class="section-title">
                        <h1>Login</h1>
                    </div>
                </div>

                <form name="ajax-form" id="ajax-form" method="POST" action="{{ route('login') }}">
                    @csrf

                    <div class="sixteen columns">
                        <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                    </div>

                    <div class="sixteen columns">
                        <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                    </div>

                    <div class="sixteen columns">
                        <input style='width: 20px; float: left;' type="checkbox" name="remember" id="rememberme" {{ old('remember') ? 'checked' : '' }}>
                        <label class="form-check-label" style='line-height: 20px;' for="rememberme"> {{ __('Remember Me') }} </label>
                    </div>

                    <div class="sixteen columns">
                        <div id="button-con">
                            <button type="submit" class="send_message">
                                {{ __('Login') }}
                            </button>

                            @if (Route::has('password.request'))
                            <br><br>
                                <a href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                            @endif
                        </div>
                    </div>
                </form>
            </div>
        </section>
        @include('vms.layouts.footer')
    </main>

    <div class="scroll-to-top">&#xf106;</div>





    <!-- JAVASCRIPT
    ================================================== -->
    <script type="text/javascript" src="{{ asset('/js/jquery-2.1.1.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/modernizr.custom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.mobile.custom.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/retina-1.1.0.min.js') }}"></script>

    <!-- End Document
    ================================================== -->
@endsection