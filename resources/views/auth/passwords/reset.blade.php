@extends('vms.layouts.main')
@section('title')
    Reset Password
@endsection

@section('content')
    <main class="cd-main-content" id="main" style="margin-top:90px">

        <section class="section white-section section-padding-top-bottom" id="scroll-link-6">
            <div class="container">

                <div class="sixteen columns">
                    <div class="section-title">
                        <h1>{{ __('Reset Password') }}</h1>
                        {{--                        <div class="subtitle big">Medlab</div>--}}
                    </div>
                </div>
                <form name="ajax-form" id="ajax-form" method="POST" action="{{ route('password.update') }}">
                    @csrf

                    <input type="hidden" name="token" value="{{ $token }}">

                    <div class="eight columns" style="width: 100%;">
                        <label for="email"
                               class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>


                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                               name="email" value="{{ $email ?? old('email') }}" required autocomplete="email"
                               autofocus readonly>

                        @error('email')
                        {{--                                    <span class="invalid-feedback" role="alert">--}}
                        {{--                                        <strong>{{ $message }}</strong>--}}
                        {{--                                    </span>--}}
                        <div class="alert alert-red">
                            <p>{{ $message }}</p>
                        </div>
                        @enderror

                    </div>

                    <div class="eight columns" style="width: 100%;">
                        <label for="new-password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>


                        <input id="new-password" type="password"
                               class="form-control @error('password') is-invalid @enderror" name="password" required
                               autocomplete="new-password">

                        @error('password')
                        {{--                                    <span class="invalid-feedback" role="alert">--}}
                        {{--                                        <strong>{{ $message }}</strong>--}}
                        {{--                                    </span>--}}
                        <div class="alert alert-red">
                            <p>{{ $message }}</p>
                        </div>
                        @enderror

                    </div>
                    <div class="alert alert-blue">
                        <p style="text-transform: unset;">Password needs to be 8 or more characters</p>
                    </div>
                    <br>
                    <div class="eight columns" style="width: 100%;">
                        <label for="password-confirm"
                               class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>


                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation"
                               required autocomplete="new-password">

                    </div>

                    {{--                    <div class="eight columns" style="width: 100%;">--}}

                    {{--                        <button type="submit" class="btn btn-primary">--}}
                    {{--                            {{ __('Reset Password') }}--}}
                    {{--                        </button>--}}
                    {{--                    </div>--}}
                    <div class="sixteen columns">
                        <div id="button-con">
                            <button class="send_message" type="submit">Reset</button>
                        </div>
                    </div>
                </form>
            </div>
        </section>
        @include('vms.layouts.footer')
    </main>
    <div class="scroll-to-top">&#xf106;</div>





    <!-- JAVASCRIPT
    ================================================== -->
    <script type="text/javascript" src="{{ asset('/js/jquery-2.1.1.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/modernizr.custom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.mobile.custom.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/retina-1.1.0.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.animsition.min.js') }}"></script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            $(document).ready(function () {
                document.getElementById("new-password").addEventListener('change', function () {
                    if (this.value.length < 8) {
                        document.getElementById('new-password').value = "";
                        Swal.fire({
                            title: 'Password too weak',
                            text: "Password needs to be 8 or more characters",
                            icon: 'error',
                        })
                    }else {
                        Swal.fire({
                            title: 'Validating Password ..',
                            allowOutsideClick: false,
                            allowEscapeKey: false,
                            showConfirmButton: false,
                            showCloseButton: false,
                            showCancelButton: false,
                            onOpen: () => {
                                Swal.showLoading();
                                $.ajax({
                                    type: "POST",
                                    url: '{{ url('validate_password') }}',
                                    data: {
                                        _token: '{{ csrf_token() }}',
                                        password: this.value,
                                    },
                                    timeout: 6000,
                                    success: function (data) {
                                        if (data.number > 1000) {
                                            document.getElementById('new-password').value = "";
                                            Swal.fire({
                                                icon: 'error',
                                                html: 'The password you have entered has been found ' + data.number + ' times in the public data breach registry. Passwords found over 1000 times cannot be accepted. Please choose a different password. For more information head to <a href="https://haveibeenpwned.com/Passwords" target="_blank" color="#6ba53a">https://haveibeenpwned.com/Passwords</a>.'
                                            })
                                        } else if (data.number > 100 && data.number <= 1000) {
                                            Swal.fire({
                                                icon: 'info',
                                                html: 'The password you have entered has been found ' + data.number + ' times in the public data breach registry. We recommend choosing a different password, but you can continue if you wish. For more information head to <a href="https://haveibeenpwned.com/Passwords" target="_blank" color="#6ba53a">https://haveibeenpwned.com/Passwords</a>.'
                                            })

                                        } else {
                                            Swal.close()
                                        }
                                    }

                                });
                            }
                        });
                    }
                });
                $("#ajax-form").submit(function (e) {

                    e.preventDefault(); // avoid to execute the actual submit of the form.
                    // var passw = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/;
                    var password = document.getElementById("new-password").value;
                    var confirmPassword = document.getElementById("password-confirm").value;
                    console.log(password);
                    console.log(confirmPassword);
                    if (password != confirmPassword) {
                        Swal.fire({
                            title: 'Error',
                            text: "Passwords do not match, please try again",
                            icon: 'error',
                        })
                    } else if (password.length < 8) {
                        Swal.fire({
                            title: 'Password too weak',
                            text: "Password needs to be 8 or more characters",
                            icon: 'error',
                        })
                    }
                    else{
                        var form = $(this);
                        var url = form.attr('action');

                        $.ajax({
                            type: "POST",
                            url: url,
                            data: form.serialize(), // serializes the form's elements.
                            success: function (data) {
                                console.log(data);
                                if (data.msg == 'success') {
                                    Swal.fire({
                                        icon: 'success',
                                        title: 'Password reset successfully ',
                                        confirmButtonText: 'OK',
                                        allowOutsideClick: false
                                    }).then((result) => {
                                        if (result.value) {
                                            if(data.path=='/2fa_verify'){
                                                window.location.href = data.path;
                                            }else{
                                                $.ajax({
                                                    type: "POST",
                                                    url: "{{ url('login') }}",
                                                    data: {
                                                        _token: '{{ csrf_token() }}',
                                                        email:data.email,
                                                        password:data.password,
                                                    }, // serializes the form's elements.
                                                    success: function (data) {
                                                        if (data.msg == '2fa'){
                                                            location.href = "/2fa_verify";
                                                        }
                                                        else if (data.msg == 'practitioner') {
                                                            var list = JSON.parse(data.practitioner);
                                                            var options = {};
                                                            $.map(list,
                                                                function (o) {
                                                                    if (o.customer_name == o.trading_name || o.trading_name==null) {
                                                                        options[o.guid] = o.customer_name;
                                                                    } else {
                                                                        options[o.guid] = o.customer_name + '|' + o.trading_name;
                                                                    }

                                                                });
                                                            const {value: practitioner} = Swal.fire({
                                                                title: 'Select a business',
                                                                input: 'select',
                                                                inputOptions: options,    // inputOptions: {
                                                                inputPlaceholder: 'Select a business',
                                                                allowOutsideClick: false,
                                                                showLoaderOnConfirm: true,
                                                                inputValidator: (value) => {
                                                                    return new Promise((resolve) => {
                                                                        if(value == ''){
                                                                            resolve('You need to select a business')
                                                                        }else{
                                                                            $.ajax({
                                                                                type: "POST",
                                                                                url: '{{ url('submit_practitioner') }}',
                                                                                data: {
                                                                                    _token: '{{ csrf_token() }}',
                                                                                    practitioner: value,
                                                                                },
                                                                                success: function (data) {
                                                                                    if (data.msg == 'obsolete account') {
                                                                                        Swal.fire({
                                                                                            icon: 'error',
                                                                                            html: "Your account " + data.name + " has been closed. Please <a href='{{ route('contact') }}' style='font-size: unset'>contact us</a> if you would like to re-open the account",
                                                                                        })

                                                                                    } else {
                                                                                        location.reload();
                                                                                    }
                                                                                }

                                                                            })
                                                                        }

                                                                    })
                                                                }
                                                            })
                                                        } else if (data.msg == 'patient') {
                                                            location.reload();
                                                        } else if (data.msg == 'invalid account') {
                                                            Swal.fire({
                                                                icon: 'error',
                                                                title: 'Your account haven"t been processed yet, please try again later',
                                                            })
                                                        } else if (data.msg == 'obsolete account') {
                                                            Swal.fire({
                                                                icon: 'error',
                                                                html: "Your account " + data.name + " has been closed. Please <a href='{{ route('contact') }}' style='font-size: unset'>contact us</a> if you would like to re-open the account",
                                                            })
                                                        } else {
                                                            Swal.fire({
                                                                icon: 'error',
                                                                title: 'Invalid email or password',
                                                            })
                                                        }
                                                    }
                                                });
                                            }

                                        }
                                    })

                                } else {
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'This link is expired, please try again later',
                                        // title: data.msg,
                                    })
                                }
                            }
                        });
                    }




                });

                $(".animsition").animsition({

                    inClass: 'zoom-in-sm',
                    outClass: 'zoom-out-sm',
                    inDuration: 1500,
                    outDuration: 800,
                    linkElement: '.animsition-link',
                    // e.g. linkElement   :   'a:not([target="_blank"]):not([href^=#])'
                    loading: true,
                    loadingParentElement: 'body', //animsition wrapper element
                    loadingClass: 'animsition-loading',
                    unSupportCss: ['animation-duration',
                        '-webkit-animation-duration',
                        '-o-animation-duration'
                    ],
                    //"unSupportCss" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
                    //The default setting is to disable the "animsition" in a browser that does not support "animation-duration".

                    overlay: false,

                    overlayClass: 'animsition-overlay-slide',
                    overlayParentElement: 'body'
                });
            });
        })(jQuery);


        $(document).ready(function () {
            $('#example').DataTable({
                "lengthMenu": [25, 50, 75, 100],
            });

        });
    </script>
    <script type="text/javascript" src="{{ asset('/js/jquery.easing.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.hidescroll.min.js') }}"></script>
    <script type="text/javascript">
        $('.header-top').hidescroll();
    </script>
    <script type="text/javascript" src="{{ asset('/js/smoothScroll.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.parallax-1.1.3.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/owl.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/scrollReveal.js') }}"></script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            window.scrollReveal = new scrollReveal();
        })(jQuery);
    </script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            jQuery(document).ready(function () {
                var offset = 450;
                var duration = 500;
                jQuery(window).scroll(function () {
                    if (jQuery(this).scrollTop() > offset) {
                        jQuery('.scroll-to-top').fadeIn(duration);
                    } else {
                        jQuery('.scroll-to-top').fadeOut(duration);
                    }
                });

                jQuery('.scroll-to-top').click(function (event) {
                    event.preventDefault();
                    jQuery('html, body').animate({scrollTop: 0}, duration);
                    return false;
                })
            });
        })(jQuery);
    </script>
    <script type="text/javascript" src="{{ asset('/js/visible.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/pro-bars.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.counterup.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/waypoints.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/smk-accordion.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/styleswitcher.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/custom-about-1.js') }}"></script>

    <link rel="stylesheet" type="text/css" href="{{ asset('/DataTables/datatables.min.css') }}"/>

    <script type="text/javascript" src="{{ asset('/DataTables/datatables.min.js') }}"></script>
@endsection
