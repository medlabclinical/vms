@extends('vms.layouts.main')
@section('title')
    Send Email
@endsection
{{--@section('breadcrumbs')--}}
{{--    <div class="medlab_breadcrumbs_wrapper">--}}
{{--        <div class="container" style="width: unset;background-color:#7AA43F;">--}}
{{--            <ol class="breadcrumb medlab_breadcrumbs" style="margin-bottom: unset;background-color:#7AA43F;">--}}
{{--            </ol>--}}
{{--        </div>--}}
{{--    </div>--}}

{{--    <div class="medlab_breadcrumbs_wrapper">--}}
{{--        <div class="container" style="width: unset">--}}
{{--            <ol class="breadcrumb medlab_breadcrumbs">--}}
{{--                <li><a class="medlab_breadcrumbs_link" href="/">@lang('trs.home')</a></li>--}}
{{--                <li><a class="medlab_breadcrumbs_link" href="/research">@lang('trs.research')</a></li>--}}
{{--                <li class="active medlab_breadcrumbs_text">@lang('trs.patents')</li>--}}
{{--            </ol>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--@endsection--}}
@section('content')


    <main class="cd-main-content" id="main" style="margin-top:90px">

        <section class="section white-section section-padding-top-bottom" id="scroll-link-6">


            <div class="container">
                <div class="sixteen columns">
                    <div class="section-title">
                        <h1>Reset Password</h1>
                    </div>
                </div>
                {{--                    <div class="row justify-content-center">--}}
                {{--                        <div class="col-md-8">--}}
                {{--                            <div class="card">--}}
                {{--                                <div class="card-header">{{ __('Reset Password') }}</div>--}}

                {{--                                <div class="card-body">--}}
                {{--                                    @if (session('status'))--}}
                {{--                                        <div class="alert alert-success" role="alert">--}}
                {{--                                            {{ session('status') }}--}}
                {{--                                        </div>--}}
                {{--                                    @endif--}}

                <form name="ajax-form" id="ajax-form" method="POST" action="{{ route('password.email') }}">
                    @csrf

                    {{--                        <div class="eight columns">--}}
                    {{--                                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>--}}

                    {{--                                                                        <div class="col-md-6">--}}
                    {{--                                                                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>--}}

                    {{--                                                                            @error('email')--}}
                    {{--                                                                            <span class="invalid-feedback" role="alert">--}}
                    {{--                                                                    <strong>{{ $message }}</strong>--}}
                    {{--                                                                </span>--}}
                    {{--                                                                            @enderror--}}
                    {{--                                                                        </div>--}}
                    <div class="eight columns" style="width: 100%;">
                        @if (session('status'))
                            <div class="alert alert-blue">
                                <p>{{ session('status') }}</p>
                            </div>
                        @endif
                        @error('email')
                        <div class="alert alert-red">
                            <p>{{ $message }}</p>
                        </div>
                        @enderror
                        <label for="username" class="uname"> Your email or username</label>
                        <input id="email" type="email"
                               class="form-control @error('email') is-invalid @enderror"
                               name="email" value="{{ old('email') }}" required
                               autocomplete="email" autofocus style="text-transform: unset;">
                        {{--                                <input name="email" id="email" type="text" style="text-transform: unset;" placeholder="E-Mail: *"/>--}}


                    </div>
                    {{--                        </div>--}}

                    {{--                                        <div class="form-group row mb-0">--}}
                    {{--                                            <div class="col-md-6 offset-md-4">--}}
                    {{--                                                <button type="submit" class="btn btn-primary">--}}
                    {{--                                                    {{ __('Send Password Reset Link') }}--}}
                    {{--                                                </button>--}}
                    {{--                                            </div>--}}
                    {{--                                        </div>--}}
                    <div class="sixteen columns">
                        <div id="button-con">
                            <button class="send_message" type="submit">submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </section>
        @include('vms.layouts.footer')
    </main>

    <div class="scroll-to-top">&#xf106;</div>





    <!-- JAVASCRIPT
    ================================================== -->
    <script type="text/javascript" src="{{ asset('/js/jquery-2.1.1.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/modernizr.custom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.mobile.custom.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/retina-1.1.0.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.animsition.min.js') }}"></script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            $(document).ready(function () {
                $("#ajax-form").submit(function (e) {

                    e.preventDefault(); // avoid to execute the actual submit of the form.

                    var form = $(this);
                    var url = form.attr('action');

                    $.ajax({
                        type: "POST",
                        url: url,
                        data: form.serialize(), // serializes the form's elements.
                        success: function (data) {
                            console.log(data);
                            if (data.msg == 'success') {
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Password reset email sent ',
                                    text: 'Please check your email for instructions on how to reset your password. If you did not receive this email. Please check your spam folder for an email from Do-not-reply@mg.medlab.co',
                                    confirmButtonText: 'OK',
                                    allowOutsideClick: false
                                    // title: data.msg,
                                }).then((result) => {
                                    if (result.value) {
                                        window.location.href = "/";
                                    }
                                })

                            } else {
                                Swal.fire({
                                    icon: 'error',
                                    html: 'We don’t have an account registered with the email address you provided. Please check your email address was entered correctly. If you don’t have an account, you can <a href="/register" color="#6ba53a">create an account.</a>.',
                                    // title: data.msg,
                                })
                            }
                        }
                    });


                });

                $(".animsition").animsition({

                    inClass: 'zoom-in-sm',
                    outClass: 'zoom-out-sm',
                    inDuration: 1500,
                    outDuration: 800,
                    linkElement: '.animsition-link',
                    // e.g. linkElement   :   'a:not([target="_blank"]):not([href^=#])'
                    loading: true,
                    loadingParentElement: 'body', //animsition wrapper element
                    loadingClass: 'animsition-loading',
                    unSupportCss: ['animation-duration',
                        '-webkit-animation-duration',
                        '-o-animation-duration'
                    ],
                    //"unSupportCss" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
                    //The default setting is to disable the "animsition" in a browser that does not support "animation-duration".

                    overlay: false,

                    overlayClass: 'animsition-overlay-slide',
                    overlayParentElement: 'body'
                });
            });
        })(jQuery);


        $(document).ready(function () {
            $('#example').DataTable({
                "lengthMenu": [25, 50, 75, 100],
            });

        });
    </script>
    <script type="text/javascript" src="{{ asset('/js/jquery.easing.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.hidescroll.min.js') }}"></script>
    <script type="text/javascript">
        $('.header-top').hidescroll();
    </script>
    <script type="text/javascript" src="{{ asset('/js/smoothScroll.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.parallax-1.1.3.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/owl.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/scrollReveal.js') }}"></script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            window.scrollReveal = new scrollReveal();
        })(jQuery);
    </script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            jQuery(document).ready(function () {
                var offset = 450;
                var duration = 500;
                jQuery(window).scroll(function () {
                    if (jQuery(this).scrollTop() > offset) {
                        jQuery('.scroll-to-top').fadeIn(duration);
                    } else {
                        jQuery('.scroll-to-top').fadeOut(duration);
                    }
                });

                jQuery('.scroll-to-top').click(function (event) {
                    event.preventDefault();
                    jQuery('html, body').animate({scrollTop: 0}, duration);
                    return false;
                })
            });
        })(jQuery);
    </script>
    <script type="text/javascript" src="{{ asset('/js/visible.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/pro-bars.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.counterup.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/waypoints.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/smk-accordion.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/styleswitcher.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/custom-about-1.js') }}"></script>

    <link rel="stylesheet" type="text/css" href="{{ asset('/DataTables/datatables.min.css') }}"/>

    <script type="text/javascript" src="{{ asset('/DataTables/datatables.min.js') }}"></script>

    <!-- End Document
    ================================================== -->
@endsection

