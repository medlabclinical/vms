@extends('beautymail::templates.minty')

@section('content')

    @include('beautymail::templates.minty.contentStart')
    <tr>
        <td class="title">
            {{ $headers }}
        </td>
    </tr>
    <tr>
        <td width="100%" height="10"></td>
    </tr>
    <tr>
        <td width="100%" height="10"></td>
    </tr>
    <tr>
        <td class="paragraph">
            <br>
            A Practitioner <strong>{{ $application->first_name.' '.$application->last_name }}</strong> has requested to open up an account for the business <strong>{{ $application->business_name }}</strong><br>
{{--            <br>--}}
{{--            Practitioner Details:<br>--}}
{{--            Name: {{ $application->salutation }}&nbsp;{{ $application->first_name.' '.$application->last_name }}<br>--}}
{{--            Job title: {{ $application->job_title }}<br>--}}
{{--            Email: {{ $application->email }}<br>--}}
{{--            Phone: {{ $application->phone }}<br>--}}
{{--            Mobile: {{ $application->mobile }}<br>--}}
{{--            Modality: Practitioner<br>--}}
{{--            Association: {{ $application->association()->first()->name }}<br>--}}
{{--            Association Number: {{ $application->association_number }}<br>--}}
{{--            <br>--}}
{{--            Business to be created:<br>--}}
{{--            Name: {{ $application->business_name }}<br>--}}
{{--            @if($application->address_line_1)--}}
{{--                Address: {{ $application->address_line_1 }}<br>--}}
{{--            @endif--}}
{{--            @if($application->primary_channel()->first() && $application->secondary_channel()->first())--}}
{{--                Classification: {{ $application->primary_channel()->first()->name }}/{{ $application->secondary_channel()->first()->name }}<br>--}}
{{--            @elseif($application->primary_channel()->first())--}}
{{--                Classification: {{ $application->primary_channel()->first()->name }}<br>--}}
{{--            @endif--}}
{{--            Phone: {{ $application->business_phone }}<br>--}}
            <br>
            Please Action Request to Approve or Decline.

        </td>
    </tr>
    <tr>
        <td width="100%" height="25"></td>
    </tr>

    <tr>
        <td width="100%" height="10"></td>
    </tr>

    <tr>
        <td width="100%" height="25"></td>
    </tr>
    <tr>
        <td>
            @include('beautymail::templates.minty.button', ['text' => 'Action Request', 'link' => $url ])
        </td>
    </tr>
    <tr>
        <td width="100%" height="25"></td>
    </tr>
    @include('beautymail::templates.minty.contentEnd')

@stop
