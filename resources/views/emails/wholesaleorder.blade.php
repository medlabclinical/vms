@component('mail::message')

<p>Hi {{ $selected_wholesaler }} </p><br>

<p>Please confirm receipt of this email by replying back</p><br><br>


<p>Attached is a turn over form for:</p><br><br>


<p>Pharmacist Name: {{ $person_name }}</p>
<p>Pharmacy Name: {{ $business_name }}</p>
<p>Pharmacy Account No.: {{ $ws_rego_number }} </p>
<p>In the state of: {{ $business_secondary_classification }}</p><br><br>


<p>If there are any questions please contact {{ $rep_email }} </p><br>

Medlab<br><br>

Tollfree: 1300 369 570<br>
Ph: +61 2 8188 0311<br>
Fax: +61 2 9699 3347<br><br><br>


------<br><br>

Notes for the Wholesaler<br><br>

------<br><br>

<table style='width: 100%; border: 1px solid;' cellpadding="2" cellspacing="2">
  <thead>
    <tr>
      <th style='width: 30%; border: 1px solid;'>Product</th>
      <th style='border: 1px solid;'>Size</th>
      <th style='border: 1px solid;'>APICode</th>
      <th style='border: 1px solid;'>Discount</th>
      <th style='border: 1px solid;'>RRPInc</th>
    </tr>
  </thead>
  <tbody>
	@foreach ($cart_items as $item)
	    <tr>
	      <td style='border: 1px solid;'>{{ $item["Product"] }}</td>
	      <td style='border: 1px solid;'>{{ $item["Size"] }}</td>
	      <td style='border: 1px solid;'>{{ $item["APICode"] }}</td>
	      <td style='border: 1px solid;'>{{ $item["Discount"] }}</td>
	      <td style='border: 1px solid;'>{{ $item["RRPInc"] }}</td>
	    </tr>
	@endforeach
  </tbody>
</table>

<br><br>
This email was sent to {{ $distributor_email }}
@endcomponent