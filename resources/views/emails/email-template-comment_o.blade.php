@component('mail::message')
# Comment Record


@component('mail::panel')
<p>This is to inform you that your comment has been responded</p>
@endcomponent

<p>  {{ $header}} </p>

<p> <strong>Message:</strong> {{ $message }} </p>
<p> <strong>Time:</strong> {{ $time }} </p>

@endcomponent
