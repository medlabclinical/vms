@extends('beautymail::templates.minty')

@section('content')

    @include('beautymail::templates.minty.contentStart')
    <tr>
        <td class="title">
            {{ $headers }}
        </td>
    </tr>
    <tr>
        <td width="100%" height="10"></td>
    </tr>
    <tr>
        <td width="100%" height="10"></td>
    </tr>
    <tr>
        <td class="paragraph">
            {{ $time }}
        </td>
    </tr>
    <tr>
        <td class="paragraph">
            <br>
            {{ $name }}
            <br>
            {{ $messages }}
            <br>
            {{ $details }}

            <br>
        </td>
    </tr>
    <tr>
        <td width="100%" height="25"></td>
    </tr>

    <tr>
        <td width="100%" height="10"></td>
    </tr>

    <tr>
        <td width="100%" height="25"></td>
    </tr>
{{--    <tr>--}}
{{--        <td>--}}
{{--            @include('beautymail::templates.minty.button', ['text' => 'Action Request', 'link' => $url ])--}}
{{--        </td>--}}
{{--    </tr>--}}
    <tr>
        <td width="100%" height="25"></td>
    </tr>
    @include('beautymail::templates.minty.contentEnd')

@stop
