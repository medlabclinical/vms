@component('mail::message')
# Interaction Record


@component('mail::panel')
<p>This is to inform you that the following interaction has been placed by {{ $staff->first_name }} {{ $staff->last_name }} for <i>{{ $person->first_name }} {{ $person->last_name }}</i> of <strong>{{ $business->customer_name }}</strong></p>
@endcomponent

<p> <strong>Type:</strong> {{ $type }} </p>

<p> <strong>Date:</strong> {{ $date }} </p>
<p> <strong>Time:</strong> {{ $time }} </p>
@if ($duration!='')
	<p> <strong>Duration:</strong> {{ $duration }} minutes </p>
@endif

<p> <strong>Notes:</strong> {{ $notes }} </p>

@if ($order_placed!='')
<p> <strong>Order Placed:</strong> {{ $order_placed }} </p>
@endif



@endcomponent