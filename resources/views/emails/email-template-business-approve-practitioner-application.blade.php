@extends('beautymail::templates.minty')

@section('content')

    @include('beautymail::templates.minty.contentStart')
    <tr>
        <td class="title">
            {{ $headers }}
        </td>
    </tr>
    <tr>
        <td width="100%" height="10"></td>
    </tr>
    <tr>
        <td width="100%" height="10"></td>
    </tr>
    <tr>
        <td class="paragraph">
            <br>
            A Practitioner <strong>{{ $messages->first_name.' '.$messages->last_name }}</strong> has requested access to the Business <strong>{{ $business->customer_name }}</strong><br>
{{--            <br>--}}
{{--            Practitioner Details:<br>--}}
{{--            Name: {{ $messages->salutation }}&nbsp;{{ $messages->first_name.' '.$messages->last_name }}<br>--}}
{{--            Job title: {{ $messages->job_title }}<br>--}}
{{--            Email: {{ $messages->email }}<br>--}}
{{--            Phone: {{ $messages->phone }}<br>--}}
{{--            Mobile: {{ $messages->mobile }}<br>--}}
{{--            Modality: Practitioner<br>--}}
{{--            Association: {{ $messages->association()->first()->name }}<br>--}}
{{--            Association Number: {{ $messages->association_number }}<br>--}}
{{--            <br>--}}
{{--            Business details to be linked to:<br>--}}
{{--            Name: {{ $business->customer_name }}<br>--}}
{{--            @if($business->address()->first())--}}
{{--            Address: {{ $business->address()->first()->address_line_1 }}<br>--}}
{{--            @endif--}}
{{--            @if($business->primary_channel()->first() && $business->secondary_channel()->first())--}}
{{--                Classification: {{ $business->primary_channel()->first()->name }}/{{ $business->secondary_channel()->first()->name }}<br>--}}
{{--            @elseif($business->primary_channel()->first())--}}
{{--                Classification: {{ $business->primary_channel()->first()->name }}<br>--}}
{{--            @endif--}}
{{--            Phone: {{ $business->phone_number }}<br>--}}
            <br>
            Please Action Request to Approve or Decline.

        </td>
    </tr>
    <tr>
        <td width="100%" height="25"></td>
    </tr>

    <tr>
        <td width="100%" height="10"></td>
    </tr>

    <tr>
        <td width="100%" height="25"></td>
    </tr>
    <tr>
        <td>
            @include('beautymail::templates.minty.button', ['text' => 'Action Request', 'link' => $url ])
        </td>
    </tr>
    <tr>
        <td width="100%" height="25"></td>
    </tr>
    @include('beautymail::templates.minty.contentEnd')

@stop
