@extends('beautymail::templates.minty')

@section('content')

    @include('beautymail::templates.minty.contentStart')
{{--    <tr>--}}
{{--        <td class="title">--}}
{{--            {{ $headers }}--}}
{{--        </td>--}}
{{--    </tr>--}}
{{--    <tr>--}}
{{--        <td width="100%" height="10"></td>--}}
{{--    </tr>--}}
    <tr>
        <td class="title">
            Hello {{ $name }}
        </td>
    </tr>
    <tr>
        <td width="100%" height="10"></td>
    </tr>
    <tr>
        <td class="paragraph">
            {{ $headers }}
        </td>
    </tr>
    <tr>
        <td width="100%" height="10"></td>
    </tr>
    <tr>
        <td class="paragraph">
            <i>"{{ $messages }}"</i>
        </td>
    </tr>
    <tr>
        <td width="100%" height="20"></td>
    </tr>
    <tr>
        <td>
            @include('beautymail::templates.minty.button', ['text' => 'View your comment', 'link' => $url ])
        </td>
    </tr>
    <tr>
        <td width="100%" height="20"></td>
    </tr>
    <tr>
        <td class="paragraph">
            To change your notification preferences, update your profile <a href="{{ $login_url }}">here</a>
        </td>
    </tr>
    <tr>
        <td width="100%" height="10"></td>
    </tr>
    <tr>
        <td width="100%" height="25"></td>
    </tr>
    <tr>
        <td width="100%" height="10"></td>
    </tr>
    @include('beautymail::templates.minty.contentEnd')

@stop
