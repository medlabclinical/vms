@component('mail::message')
# Sales Record


<p>A new sales order for {{ $clientname }} has been created</p>
<p>Order number {{ $ordernumber }}</p>
@if($drug_ordernumber!='')
<p>Drug Order number {{ $drug_ordernumber }}</p>
@endif
<p>Receipt: {{ $receipt_number }} for a total of ${{ $total_paid }}</p>



@endcomponent