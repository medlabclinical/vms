@extends('beautymail::templates.minty')

@section('content')

    @include('beautymail::templates.minty.contentStart')
    <tr>
        <td class="title">
            {{ $headers }}
        </td>
    </tr>
    <tr>
        <td width="100%" height="10"></td>
    </tr>
    <tr>
        <td width="100%" height="10"></td>
    </tr>
    <tr>
        <td class="paragraph">
            {{ $time }}
        </td>
    </tr>
    <tr>
        <td class="paragraph">
            <br>
            {{ $name }}
            <br>
            {{ $phone }}
            <br>
            {{ $email }}
            <br>
            {{ $company }}
            <br>
            {{ $country }}
            <br>
            {{ $messages }}
            <br>
            <hr>
            {{ $details }}
            <br>
        </td>
    </tr>

    @include('beautymail::templates.minty.contentEnd')

@stop
