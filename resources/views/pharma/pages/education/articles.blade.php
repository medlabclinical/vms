@extends('pharma.layouts.main')
@section('breadcrumbs')
    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset;background-color:#7AA43F;">
            <ol class="breadcrumb medlab_breadcrumbs" style="margin-bottom: unset;background-color:#7AA43F;">
            </ol>
        </div>
    </div>

    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset">
            <ol class="breadcrumb medlab_breadcrumbs">
                <li><a class="medlab_breadcrumbs_link" href="/">@lang('trs.home')</a></li>
                <li class="active medlab_breadcrumbs_text">@lang('trs.articles')</li>
            </ol>
        </div>
    </div>
@endsection
@section('content')


    <main class="cd-main-content" id="main" style="margin-top:90px">

        <!-- TOP SECTION
       ================================================== -->

        <section class="section parallax-section parallax-section-padding-top-bottom-pagetop section-page-top-title">

            <div class="parallax-blog-3"></div>

            <div class="container">
                <div class="eight columns">
                    <h1>articles</h1>
                </div>
                <div class="eight columns">
                    <div id="owl-top-page-slider" class="owl-carousel owl-theme">
                        <div class="item">
                            <div class="page-top-icon">&#xf0a1;</div>
                            <div class="page-top-text">77 posts</div>
                        </div>
                        <div class="item">
                            <div class="page-top-icon">&#xf007;</div>
                            <div class="page-top-text">7 autors</div>
                        </div>
                        <div class="item">
                            <div class="page-top-icon">&#xf086;</div>
                            <div class="page-top-text">2472 comments</div>
                        </div>
                    </div>
                </div>
            </div>

        </section>

        <!-- SECTION
        ================================================== -->

        <section class="section white-section section-padding-top-bottom">

            <div class="container">
                <div class="sixteen columns">
                    <div id="portfolio-filter">
                        <ul id="filter">
                            <li><a href="#" class="current" data-filter="*" title="">Show All</a></li>
                            <li><a href="#" data-filter=".photo" title="">Photo</a></li>
                            <li><a href="#" data-filter=".media" title="">Media</a></li>
                            <li><a href="#" data-filter=".news" title="">News</a></li>
                            <li><a href="#" data-filter=".links" title="">Links</a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="clear"></div>

            <div class="container">
                <div class="four columns">
                    <div class="post-sidebar">
{{--                        <input name="search" type="text" placeholder="type to search and hit enter"/>--}}
                        <div class="separator-sidebar"></div>
                        <h6>Categories</h6>
                        <ul class="link-recents">
                            <div id="portfolio-filter">
                                <ul id="filter">
                                    @foreach($list as $item)

                                    <li style="width:100%;text-align: left;"><a
                                                style="margin:unset;letter-spacing: unset" href="#"
                                                data-filter=".{{ $item[0]->category }}" title="">{{ $item[0]->category }} ({{ count($item) }})</a></li>
                                    @endforeach
{{--                                    <li style="width:100%;text-align: left;"><a--}}
{{--                                                style="margin:unset;letter-spacing: unset " href="#"--}}
{{--                                                data-filter=".advertisement" title="">Design (3)</a></li>--}}
{{--                                    <li style="width:100%;text-align: left;"><a--}}
{{--                                                style="margin:unset;letter-spacing: unset " href="#"--}}
{{--                                                data-filter=".origin" title="">Image (1)</a></li>--}}
{{--                                    <li style="width:100%;text-align: left;"><a--}}
{{--                                                style="margin:unset;letter-spacing: unset " href="#"--}}
{{--                                                data-filter=".photo" title="">Videos (4)</a></li>--}}
{{--                                    <li style="width:100%;text-align: left;"><a--}}
{{--                                                style="margin:unset;letter-spacing: unset " href="#"--}}
{{--                                                data-filter=".photo" title="">Word (5)</a></li>--}}
                                </ul>
                            </div>
                        </ul>

                        <div class="separator-sidebar"></div>
                        <h6>recent posts</h6>
                        <ul class="link-recents">
                            @if(count($allArticles)<=5)
                                @foreach ($allArticles as $item)
                                    <li><a href={{ route('pharma.articles_content',['id' => $item->id]) }}>{{ $item->title }}</a></li>
                                @endforeach
                            @else
                                @for($i=0;$i<5;$i++)
                                    <li><a href={{ route('pharma.articles_content',['id' => $allArticles[$i]->id]) }}>{{ $allArticles[$i]->title }}</a></li>
                                @endfor
                            @endif
                        </ul>
{{--                        <div class="separator-sidebar"></div>--}}
{{--                        <h6>LATEST PROJECTS</h6>--}}
{{--                        <ul class="lat-pro">--}}
{{--                            <li><a href="#">--}}
{{--                                    <div class="lat-pro-img"><img src="images/portfolio/1.jpg" alt=""/></div>--}}
{{--                                </a></li>--}}
{{--                            <li><a href="#">--}}
{{--                                    <div class="lat-pro-img"><img src="images/portfolio/2.jpg" alt=""/></div>--}}
{{--                                </a></li>--}}
{{--                            <li><a href="#">--}}
{{--                                    <div class="lat-pro-img"><img src="images/portfolio/3.jpg" alt=""/></div>--}}
{{--                                </a></li>--}}
{{--                            <li><a href="#">--}}
{{--                                    <div class="lat-pro-img"><img src="images/portfolio/4.jpg" alt=""/></div>--}}
{{--                                </a></li>--}}
{{--                        </ul>--}}
                        <div class="separator-sidebar"></div>
                        <h6>LATEST video</h6>
                            @if(count($videoArticles)<=2)
                                @foreach ($videoArticles as $item)
                                <iframe src="{{ $item->video_link }}?title=0&amp;byline=0&amp;portrait=0&amp;color=000000"
                                        width="940" height="450"></iframe>
                                    <br>
                                @endforeach
                            @else
                                @for($i=0;$i<2;$i++)
                                <iframe src="{{ $allArticles[$i]->video_link }}?title=0&amp;byline=0&amp;portrait=0&amp;color=000000"
                                        width="940" height="450"></iframe>
                                <br>
                                @endfor
                            @endif
{{--                        <iframe src="http://player.vimeo.com/video/96696089?title=0&amp;byline=0&amp;portrait=0&amp;color=000000"--}}
{{--                                width="940" height="450"></iframe>--}}
{{--                        <div class="separator-sidebar"></div>--}}
{{--                        <h6>tags</h6>--}}
{{--                        <ul class="link-tag">--}}
{{--                            <li><a href="#">Analysis</a></li>--}}
{{--                            <li><a href="#">Art</a></li>--}}
{{--                            <li><a href="#">Articles</a></li>--}}
{{--                            <li><a href="#">Audio</a></li>--}}
{{--                            <li><a href="#">Business</a></li>--}}
{{--                            <li><a href="#">Culture</a></li>--}}
{{--                            <li><a href="#">Development</a></li>--}}
{{--                            <li><a href="#">Ecology</a></li>--}}
{{--                            <li><a href="#">Events</a></li>--}}
{{--                            <li><a href="#">Information</a></li>--}}
{{--                            <li><a href="#">Inspiration</a></li>--}}
{{--                            <li><a href="#">Nature</a></li>--}}
{{--                            <li><a href="#">Opportunities</a></li>--}}
{{--                            <li><a href="#">Science</a></li>--}}
{{--                            <li><a href="#">Trends</a></li>--}}
{{--                            <li><a href="#">Video</a></li>--}}
{{--                        </ul>--}}
                    </div>
                </div>
                <div class="twelve columns remove-top">
                    <div class="blog-wrapper">
                        <div id="blog-grid-masonry">
                            @foreach($allArticles as $item)
                                @if($item->article_type=='image')
                                    <a href={{ route('pharma.articles_content',['id' => $item->id]) }} class="animsition-link">
                                        <div class="blog-box-3 half-blog-width photo {{ $item['category'] }}">
                                            <div class="blog-box-1 grey-section">
                                                <img src="{{ $image_url }}/storage/{{ $item->title_img }}" alt="">
                                                <div class="blog-date-1">{{ substr($item->date,0,10) }}</div>
                                                <div class="blog-comm-1">{{ $item->comment_amount }}
                                                    <span>&#xf086;</span></div>
                                                <h6>{{ $item->title }}</h6>
                                                <p>{{ $item->preview_text }}</p>
                                                <div class="link">&#xf178;</div>
                                            </div>
                                        </div>
                                    </a>
                                @elseif($item->article_type=='link')
                                    <a href={{ route('pharma.articles_content',['id' => $item->id]) }} class="animsition-link">
                                        <div class="blog-box-3 half-blog-width links {{ $item['category'] }}">
                                            <div class="blog-box-1 link-post">
                                                <div class="blog-date-1">{{ substr($item->date,0,10) }}</div>
                                                <div class="blog-comm-1">{{ $item->comment_amount }}
                                                    <span>&#xf086;</span></div>
                                                <h6>{{ $item->title }}</h6>
                                                <p>{{ $item->preview_text }}</p>
                                                <div class="link">&#xf178;</div>
                                            </div>
                                        </div>
                                    </a>
                                @elseif($item->article_type=='slider')
                                    <a href={{ route('pharma.articles_content',['id' => $item->id]) }}class="animsition-link">
                                        <div class="blog-box-3 half-blog-width media {{ $item['category'] }}">
                                            <div class="blog-box-1 grey-section">
                                                <img src="{{ $image_url }}/{{ $item->title_img }}" alt="">
                                                <div class="blog-date-1">{{ substr($item->date,0,10) }}</div>
                                                <div class="blog-comm-1">{{ $item->comment_amount }}
                                                    <span>&#xf086;</span></div>
                                                <h6>{{ $item->title }}</h6>
                                                <p>{{ $item->preview_text }}</p>
                                                <div class="link">&#xf178;</div>
                                            </div>
                                        </div>
                                    </a>
                                @else
                                    <a href={{ route('pharma.articles_content',['id' => $item->id]) }} class="animsition-link">
                                        <div class="blog-box-3 half-blog-width news {{ $item['category'] }}">
                                            <div class="blog-box-1 grey-section">
                                                <img src="{{ $image_url }}/{{ $item->title_img }}" alt="">
                                                <div class="blog-date-1">{{ substr($item->date,0,10) }}</div>
                                                <div class="blog-comm-1">{{ $item->comment_amount }}
                                                    <span>&#xf086;</span></div>
                                                <h6>{{ $item->title }}</h6>
                                                <p>{{ $item->preview_text }}</p>
                                                <div class="link">&#xf178;</div>
                                            </div>
                                        </div>
                                    </a>
                                @endif
                            @endforeach






                            {{--                            <a href="image-post.html" class="animsition-link">--}}
                            {{--                                <div class="blog-box-3 half-blog-width photo">--}}
                            {{--                                    <div class="blog-box-1 grey-section">--}}
                            {{--                                        <img src="images/blog/1.jpg"  alt="">--}}
                            {{--                                        <div class="blog-date-1">21.11.'14</div>--}}
                            {{--                                        <div class="blog-comm-1">3 <span>&#xf086;</span></div>--}}
                            {{--                                        <h6>Latest post</h6>--}}
                            {{--                                        <p>Lorem ipsum dolor sit consectetur amet, adipisicing elit.</p>--}}
                            {{--                                        <div class="link">&#xf178;</div>--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                            </a>--}}
                            {{--                            <a href="link-post.html" class="animsition-link">--}}
                            {{--                                <div class="blog-box-3 half-blog-width links">--}}
                            {{--                                    <div class="blog-box-1 link-post">--}}
                            {{--                                        <div class="blog-date-1">21.11.'14</div>--}}
                            {{--                                        <div class="blog-comm-1">3 <span>&#xf086;</span></div>--}}
                            {{--                                        <h6>When you are courting a nice girl an hour seems like a second.</h6>--}}
                            {{--                                        <div class="link">&#xf178;</div>--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                            </a>--}}
                            {{--                                                                <a href="slider-post.html" class="animsition-link">--}}
                            {{--                                                                    <div class="blog-box-3 half-blog-width media">--}}
                            {{--                                                                        <div class="blog-box-1 grey-section">--}}
                            {{--                                                                            <img src="images/blog/2.jpg"  alt="">--}}
                            {{--                                                                            <div class="blog-date-1">17.11.'14</div>--}}
                            {{--                                                                            <div class="blog-comm-1">2 <span>&#xf086;</span></div>--}}
                            {{--                                                                            <h6>Latest post</h6>--}}
                            {{--                                                                            <p>Lorem ipsum dolor sit consectetur amet, adipisicing elit.</p>--}}
                            {{--                                                                            <div class="link">&#xf178;</div>--}}
                            {{--                                                                        </div>--}}
                            {{--                                                                    </div>--}}
                            {{--                                                                </a>--}}
                            {{--                                                                <a href="video-post.html" class="animsition-link">--}}
                            {{--                                                                    <div class="blog-box-3 half-blog-width news">--}}
                            {{--                                                                        <div class="blog-box-1 grey-section">--}}
                            {{--                                                                            <img src="images/blog/4.jpg"  alt="">--}}
                            {{--                                                                            <div class="blog-date-1">13.11.'14</div>--}}
                            {{--                                                                            <div class="blog-comm-1">5 <span>&#xf086;</span></div>--}}
                            {{--                                                                            <h6>Latest post</h6>--}}
                            {{--                                                                            <p>Lorem ipsum dolor sit consectetur amet, adipisicing elit.</p>--}}
                            {{--                                                                            <div class="link">&#xf178;</div>--}}
                            {{--                                                                        </div>--}}
                            {{--                                                                    </div>--}}
                            {{--                                                                </a>--}}
                            {{--                            <a href="image-post.html" class="animsition-link">--}}
                            {{--                                <div class="blog-box-3 half-blog-width news">--}}
                            {{--                                    <div class="blog-box-1 grey-section">--}}
                            {{--                                        <img src="images/blog/3.jpg"  alt="">--}}
                            {{--                                        <div class="blog-date-1">05.11.'14</div>--}}
                            {{--                                        <div class="blog-comm-1">7 <span>&#xf086;</span></div>--}}
                            {{--                                        <h6>Latest post</h6>--}}
                            {{--                                        <p>Lorem ipsum dolor sit consectetur amet, adipisicing elit.</p>--}}
                            {{--                                        <div class="link">&#xf178;</div>--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                            </a>--}}
                            {{--                            <a href="link-post.html" class="animsition-link">--}}
                            {{--                                <div class="blog-box-3 half-blog-width links">--}}
                            {{--                                    <div class="blog-box-1 link-post">--}}
                            {{--                                        <div class="blog-date-1">23.10.'14</div>--}}
                            {{--                                        <div class="blog-comm-1">3 <span>&#xf086;</span></div>--}}
                            {{--                                        <h6>When you sit on a red-hot cinder a second seems like an hour. That's relativity.</h6>--}}
                            {{--                                        <div class="link">&#xf178;</div>--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                            </a>--}}
                            {{--                            <a href="image-post.html" class="animsition-link">--}}
                            {{--                                <div class="blog-box-3 half-blog-width photo">--}}
                            {{--                                    <div class="blog-box-1 grey-section">--}}
                            {{--                                        <img src="images/blog/6.jpg"  alt="">--}}
                            {{--                                        <div class="blog-date-1">25.10.'14</div>--}}
                            {{--                                        <div class="blog-comm-1">2 <span>&#xf086;</span></div>--}}
                            {{--                                        <h6>Latest post</h6>--}}
                            {{--                                        <p>Lorem ipsum dolor sit consectetur amet, adipisicing elit.</p>--}}
                            {{--                                        <div class="link">&#xf178;</div>--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                            </a>--}}
                            {{--                            <a href="video-post.html" class="animsition-link">--}}
                            {{--                                <div class="blog-box-3 half-blog-width news">--}}
                            {{--                                    <div class="blog-box-1 grey-section">--}}
                            {{--                                        <img src="images/blog/5.jpg"  alt="">--}}
                            {{--                                        <div class="blog-date-1">16.10.'14</div>--}}
                            {{--                                        <div class="blog-comm-1">5 <span>&#xf086;</span></div>--}}
                            {{--                                        <h6>Latest post</h6>--}}
                            {{--                                        <p>Lorem ipsum dolor sit consectetur amet, adipisicing elit.</p>--}}
                            {{--                                        <div class="link">&#xf178;</div>--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                            </a>--}}
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <!-- SECTION
        ================================================== -->

{{--        <section class="section grey-section section-padding-top-bottom">--}}

{{--            <div class="container">--}}
{{--                <div class="sixteen columns">--}}
{{--                    <div class="blog-left-right-links">--}}
{{--                        <a href="#">--}}
{{--                            <div class="blog-left-link"><p>older</p></div>--}}
{{--                        </a>--}}
{{--                        <a href="#">--}}
{{--                            <div class="blog-right-link"><p>newer</p></div>--}}
{{--                        </a>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}

{{--        </section>--}}

        @include('pharma.layouts.footer')
    </main>

    <div class="scroll-to-top">&#xf106;</div>







    <!-- JAVASCRIPT
    ================================================== -->
    <script type="text/javascript" src="{{ asset('/js/jquery-2.1.1.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/modernizr.custom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.mobile.custom.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/retina-1.1.0.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.animsition.min.js') }}"></script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            $(document).ready(function () {
                @if (session('login_error'))
                open_login();
                @endif
                $(".animsition").animsition({

                    inClass: 'zoom-in-sm',
                    outClass: 'zoom-out-sm',
                    inDuration: 1500,
                    outDuration: 800,
                    linkElement: '.animsition-link',
                    // e.g. linkElement   :   'a:not([target="_blank"]):not([href^=#])'
                    loading: true,
                    loadingParentElement: 'body', //animsition wrapper element
                    loadingClass: 'animsition-loading',
                    unSupportCss: ['animation-duration',
                        '-webkit-animation-duration',
                        '-o-animation-duration'
                    ],
                    //"unSupportCss" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
                    //The default setting is to disable the "animsition" in a browser that does not support "animation-duration".

                    overlay: false,

                    overlayClass: 'animsition-overlay-slide',
                    overlayParentElement: 'body'
                });
            });

            $(document).ready(function () {
                $('#example').DataTable({
                    'searching': false,
                    'lengthChange': false,
                });

            });
        })(jQuery);
    </script>
    <script type="text/javascript" src="{{ asset('/js/jquery.easing.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.hidescroll.min.js') }}"></script>
    <script type="text/javascript">
        $('.header-top').hidescroll();
    </script>
    <script type="text/javascript" src="{{ asset('/js/smoothScroll.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.parallax-1.1.3.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/owl.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/scrollReveal.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/masonry.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/isotope.js') }}"></script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            window.scrollReveal = new scrollReveal();
        })(jQuery);
    </script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            jQuery(document).ready(function () {
                var offset = 450;
                var duration = 500;
                jQuery(window).scroll(function () {
                    if (jQuery(this).scrollTop() > offset) {
                        jQuery('.scroll-to-top').fadeIn(duration);
                    } else {
                        jQuery('.scroll-to-top').fadeOut(duration);
                    }
                });

                jQuery('.scroll-to-top').click(function (event) {
                    event.preventDefault();
                    jQuery('html, body').animate({scrollTop: 0}, duration);
                    return false;
                })
            });
        })(jQuery);
    </script>
    <script type="text/javascript" src="{{ asset('/js/jquery.fitvids.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/styleswitcher.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/custom-blog-grid-left.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('/DataTables/datatables.min.css') }}"/>

    <script type="text/javascript" src="{{ asset('/DataTables/datatables.min.js') }}"></script>


    <iframe src=”https://player.vimeo.com/video/894794478?api=1&player_id=894794478″ id=”894794478″
            width=”640″ height=”390″ frameborder=”0″ data-progress=”true” data-seek=”true”
            data-bounce=”false” webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>

    <script src=”/js/jquery-1.12.1.min.js”></script>

    <script src=”/js/vimeo.ga.min.js”></script>
    <!-- End Document
================================================== -->
@endsection
