@extends('pharma.layouts.main')
@section('breadcrumbs')
    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset;background-color:#7AA43F;">
            <ol class="breadcrumb medlab_breadcrumbs" style="margin-bottom: unset;background-color:#7AA43F;">
            </ol>
        </div>
    </div>

    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset">
            <ol class="breadcrumb medlab_breadcrumbs">
                <li><a class="medlab_breadcrumbs_link" href="/">@lang('trs.home')</a></li>
                <li><a class="medlab_breadcrumbs_link" href="{{ route('pharma.articles') }}">@lang('trs.education')</a></li>
                <li class="active medlab_breadcrumbs_text">{{ $article->title }}</li>
            </ol>
        </div>
    </div>
@endsection
@section('content')


    <main class="cd-main-content" id="main" style="margin-top:90px">



        <!-- TOP SECTION
        ================================================== -->

        <section class="section white-section section-home-padding-top">

            <div class="container">
                <div class="sixteen columns">
                    <div class="section-title left">
                        <h1>{{ $article->title }}</h1>
                        <div class="subtitle left big">{{ $article->subtitle }}</div>
                    </div>
                </div>
            </div>

        </section>

        <!-- SECTION
        ================================================== -->

        <section class="section white-section section-padding-bottom">

            <div class="container">
                <div class="twelve columns">

                    <div class="blog-big-wrapper grey-section" data-scroll-reveal="enter bottom move 200px over 1s after 0.3s" style="color:white;">
                        <div class="big-post-date"><span>&#xf073;</span> {{ substr($article->date,0,10) }}</div>
                        <div id="owl-blog-big-slider" class="owl-carousel owl-theme">
                            <div class="item">
                                <img src="{{ $image_url }}/www/Images/{{ $article->title_img }}" alt="{{ $article->title_alt }}">
                            </div>
{{--                            @if()--}}
                            @if(isset($article->second_img))
                            <div class="item">
                                <img src="{{ $image_url }}/www/Images/{{ $article->second_img }}" alt="{{ $article->second_alt }}">
                            </div>
                            @endif
                            @if(isset($article->third_img))
                            <div class="item">
                                <img src="{{ $image_url }}/www/Images/{{ $article->third_img }}" alt="{{ $article->third_alt }}">
                            </div>
                            @endif
                        </div>
{{--                        <img src="/storage/{{ $article->title_img }}" alt="">--}}
                        <div class="articleClass padding">
                            {!! $article->content !!}
                        </div>

{{--                        <p class="text-border-left">Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem.</p>--}}
{{--                        <p>Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.</p>--}}
{{--                        <pre>--}}
{{--.post-content h5{--}}
{{--	text-align: left;--}}
{{--	line-height: 30px;--}}
{{--	padding-top: 30px;--}}
{{--	padding-bottom: 30px;--}}
{{--}--}}
{{--</pre>--}}
                    </div>
{{--                    <div class="post-tags-categ grey-section" data-scroll-reveal="enter bottom move 200px over 1s after 0.3s">--}}
{{--                        <p>Categories: <a href="#">Image</a>, <a href="#">Photography</a>, <a href="#">Wordpress</a><span>|</span>Tags: <a href="#">Gallery</a></p>--}}
{{--                    </div>--}}
{{--                    <div class="post-content-share grey-section" data-scroll-reveal="enter bottom move 200px over 1s after 0.3s">--}}
{{--                        <div class="social-share">--}}
{{--                            <span>share:</span>--}}
{{--                            <ul class="list-social-share">--}}
{{--                                <li class="icon-soc-share">--}}
{{--                                    <a href="#">&#xf099;</a>--}}
{{--                                </li>--}}
{{--                                <li class="icon-soc-share">--}}
{{--                                    <a href="#">&#xf09a;</a>--}}
{{--                                </li>--}}
{{--                                <li class="icon-soc-share">--}}
{{--                                    <a href="#">&#xf09b;</a>--}}
{{--                                </li>--}}
{{--                                <li class="icon-soc-share">--}}
{{--                                    <a href="#">&#xf0d5;</a>--}}
{{--                                </li>--}}
{{--                            </ul>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="post-content-com-top grey-section" data-scroll-reveal="enter bottom move 200px over 1s after 0.3s">--}}
{{--                        <p>COMMENTS <span>{{ $article->comment_amount }}</span></p>--}}
{{--                    </div>--}}

{{--                    <div class="post-content-comment grey-section" data-scroll-reveal="enter bottom move 200px over 1s after 0.3s">--}}
{{--                        <img  src="images/team11.jpg" alt="" />--}}
{{--                        <h6>ALEX ANDREWS</h6>--}}
{{--                        <p>Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>--}}
{{--                        <a href="#"><div class="reply">reply</div></a>--}}
{{--                    </div>--}}

{{--                    <div class="post-content-comment reply-in grey-section" data-scroll-reveal="enter bottom move 200px over 1s after 0.3s">--}}
{{--                        <img  src="images/team22.jpg" alt="" />--}}
{{--                        <h6>KARA KULIS</h6>--}}
{{--                        <p>Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>--}}
{{--                        <a href="#"><div class="reply">reply</div></a>--}}
{{--                    </div>--}}

{{--                    <div class="post-content-comment grey-section" data-scroll-reveal="enter bottom move 200px over 1s after 0.3s">--}}
{{--                        <img  src="images/team33.jpg" alt="" />--}}
{{--                        <h6>FRANK FURIOUS</h6>--}}
{{--                        <p>Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>--}}
{{--                        <a href="#"><div class="reply">reply</div></a>--}}
{{--                    </div>--}}

{{--                    <div class="leave-reply grey-section" data-scroll-reveal="enter bottom move 200px over 1s after 0.3s">--}}
{{--                        <h6>LEAVE A REPLY</h6>--}}
{{--                        <p>Your email address will not be published. Required fields are marked *</p>--}}
{{--                        <input name="name" type="text"   placeholder="NAME *"/>--}}
{{--                        <input name="email" type="text"   placeholder="EMAIL *"/>--}}
{{--                        <input name="website" type="text"   placeholder="website"/>--}}
{{--                        <textarea name="website"  placeholder="COMMENT"></textarea>--}}
{{--                        <button class="post-comment">post comment</button>--}}
{{--                    </div> --}}

                </div>
{{--                <div class="four columns" data-scroll-reveal="enter bottom move 200px over 1s after 0.3s">--}}
{{--                    <div class="post-sidebar">--}}
{{--                        <input name="search" type="text"   placeholder="type to search and hit enter"/>--}}
{{--                        <div class="separator-sidebar"></div>--}}
{{--                        <h6>Categories</h6>--}}
{{--                        <ul class="link-recents">--}}
{{--                            <li><a href="#">Creative (2)</a></li>--}}
{{--                            <li><a href="#">Design (3)</a></li>--}}
{{--                            <li><a href="#">Image (1)</a></li>--}}
{{--                            <li><a href="#">Videos (4)</a></li>--}}
{{--                            <li><a href="#">WordPress (5)</a></li>--}}
{{--                        </ul>--}}
{{--                        <div class="separator-sidebar"></div>--}}
{{--                        <h6>recent posts</h6>--}}
{{--                        <ul class="link-recents">--}}
{{--                            <li><a href="#">Quisque rutrum.</a></li>--}}
{{--                            <li><a href="#">Fringilla vel, aliquet nec.</a></li>--}}
{{--                            <li><a href="#">Ligula leo, porttitor eu.</a></li>--}}
{{--                            <li><a href="#">Aenean imperdiet.</a></li>--}}
{{--                            <li><a href="#">Nulla consequat massa.</a></li>--}}
{{--                            <li><a href="#">Vivamus elementum semper.</a></li>--}}
{{--                        </ul>--}}
{{--                        <div class="separator-sidebar"></div>--}}
{{--                        <h6>LATEST PROJECTS</h6>--}}
{{--                        <ul class="lat-pro">--}}
{{--                            <li><a href="#"><div class="lat-pro-img"><img  src="images/portfolio/1.jpg" alt="" /></div></a></li>--}}
{{--                            <li><a href="#"><div class="lat-pro-img"><img  src="images/portfolio/2.jpg" alt="" /></div></a></li>--}}
{{--                            <li><a href="#"><div class="lat-pro-img"><img  src="images/portfolio/3.jpg" alt="" /></div></a></li>--}}
{{--                            <li><a href="#"><div class="lat-pro-img"><img  src="images/portfolio/4.jpg" alt="" /></div></a></li>--}}
{{--                        </ul>--}}
{{--                        <div class="separator-sidebar"></div>--}}
{{--                        <h6>LATEST video</h6>--}}
{{--                        <iframe src="http://player.vimeo.com/video/96696089?title=0&amp;byline=0&amp;portrait=0&amp;color=cfa144" width="940" height="450" ></iframe>--}}
{{--                        <div class="separator-sidebar"></div>--}}
{{--                        <h6>tags</h6>--}}
{{--                        <ul class="link-tag">--}}
{{--                            <li><a href="#">Analysis</a></li>--}}
{{--                            <li><a href="#">Art</a></li>--}}
{{--                            <li><a href="#">Articles</a></li>--}}
{{--                            <li><a href="#">Audio</a></li>--}}
{{--                            <li><a href="#">Business</a></li>--}}
{{--                            <li><a href="#">Culture</a> </li>--}}
{{--                            <li><a href="#">Development</a> </li>--}}
{{--                            <li><a href="#">Ecology</a></li>--}}
{{--                            <li><a href="#">Events</a> </li>--}}
{{--                            <li><a href="#">Information</a> </li>--}}
{{--                            <li><a href="#">Inspiration</a></li>--}}
{{--                            <li><a href="#">Nature</a> </li>--}}
{{--                            <li><a href="#">Opportunities</a> </li>--}}
{{--                            <li><a href="#">Science</a> </li>--}}
{{--                            <li><a href="#">Trends</a> </li>--}}
{{--                            <li><a href="#">Video</a></li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}

        </section>


        <!-- SECTION
        ================================================== -->

{{--        <section class="section grey-section section-padding-top-bottom">--}}
{{--            <div class="container">--}}
{{--                <div class="sixteen columns">--}}
{{--                    <div class="section-title">--}}
{{--                        <h3>Related Posts</h3>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="one-third column" data-scroll-reveal="enter left move 200px over 1s after 0.3s">--}}
{{--                    <a href="image-post.html" class="animsition-link">--}}
{{--                        <div class="blog-box-1 white-section">--}}
{{--                            <img src="images/blog/1.jpg"  alt="">--}}
{{--                            <div class="blog-date-1">21.11.'14</div>--}}
{{--                            <div class="blog-comm-1">3 <span>&#xf086;</span></div>--}}
{{--                            <h6>Latest post</h6>--}}
{{--                            <p>Lorem ipsum dolor sit consectetur amet, adipisicing elit.</p>--}}
{{--                            <div class="link">&#xf178;</div>--}}
{{--                        </div>--}}
{{--                    </a>--}}
{{--                </div>--}}
{{--                <div class="one-third column" data-scroll-reveal="enter bottom move 200px over 1s after 0.3s">--}}
{{--                    <a href="slider-post.html" class="animsition-link">--}}
{{--                        <div class="blog-box-1 white-section">--}}
{{--                            <img src="images/blog/2.jpg"  alt="">--}}
{{--                            <div class="blog-date-1">17.11.'14</div>--}}
{{--                            <div class="blog-comm-1">2 <span>&#xf086;</span></div>--}}
{{--                            <h6>Latest post</h6>--}}
{{--                            <p>Lorem ipsum dolor sit consectetur amet, adipisicing elit.</p>--}}
{{--                            <div class="link">&#xf178;</div>--}}
{{--                        </div>--}}
{{--                    </a>--}}
{{--                </div>--}}
{{--                <div class="one-third column" data-scroll-reveal="enter right move 200px over 1s after 0.3s">--}}
{{--                    <a href="video-post.html" class="animsition-link">--}}
{{--                        <div class="blog-box-1 white-section">--}}
{{--                            <img src="images/blog/3.jpg"  alt="">--}}
{{--                            <div class="blog-date-1">14.11.'14</div>--}}
{{--                            <div class="blog-comm-1">7 <span>&#xf086;</span></div>--}}
{{--                            <h6>Latest post</h6>--}}
{{--                            <p>Lorem ipsum dolor sit consectetur amet, adipisicing elit.</p>--}}
{{--                            <div class="link">&#xf178;</div>--}}
{{--                        </div>--}}
{{--                    </a>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </section>--}}




        <!-- FOOTER
        ================================================== -->


        @include('pharma.layouts.footer')



    </main>

    <div class="scroll-to-top">&#xf106;</div>





<!-- JAVASCRIPT
================================================== -->
    <script type="text/javascript" src="{{ asset('/js/jquery-2.1.1.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/modernizr.custom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.mobile.custom.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/retina-1.1.0.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.animsition.min.js') }}"></script>
    <script type="text/javascript">
        (function ($) {
            {{--var aImgs=document.getElementsByTagName('img')--}}
            {{--for(var i=0; i<aImgs.length;i++){--}}
            {{--    if(i>1)--}}
            {{--    {--}}
            {{--        console.log(i);--}}
            {{--        aImgs[i].src = "{{ $image_url }}" + aImgs[i].getAttribute('src');--}}
            {{--    }--}}
            {{--};--}}

            "use strict";
            $(document).ready(function () {
                @if (session('login_error'))
                open_login();
                @endif
                $(".animsition").animsition({

                    inClass: 'zoom-in-sm',
                    outClass: 'zoom-out-sm',
                    inDuration: 1500,
                    outDuration: 800,
                    linkElement: '.animsition-link',
                    // e.g. linkElement   :   'a:not([target="_blank"]):not([href^=#])'
                    loading: true,
                    loadingParentElement: 'body', //animsition wrapper element
                    loadingClass: 'animsition-loading',
                    unSupportCss: ['animation-duration',
                        '-webkit-animation-duration',
                        '-o-animation-duration'
                    ],
                    //"unSupportCss" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
                    //The default setting is to disable the "animsition" in a browser that does not support "animation-duration".

                    overlay: false,

                    overlayClass: 'animsition-overlay-slide',
                    overlayParentElement: 'body'
                });
            });

        })(jQuery);
    </script>
    <script type="text/javascript" src="{{ asset('/js/jquery.easing.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.hidescroll.min.js') }}"></script>
    <script type="text/javascript">
        $('.header-top').hidescroll();
    </script>
    <script type="text/javascript" src="{{ asset('/js/smoothScroll.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.parallax-1.1.3.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/owl.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/scrollReveal.js') }}"></script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            window.scrollReveal = new scrollReveal();
        })(jQuery);
    </script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            jQuery(document).ready(function () {
                var offset = 450;
                var duration = 500;
                jQuery(window).scroll(function () {
                    if (jQuery(this).scrollTop() > offset) {
                        jQuery('.scroll-to-top').fadeIn(duration);
                    } else {
                        jQuery('.scroll-to-top').fadeOut(duration);
                    }
                });

                jQuery('.scroll-to-top').click(function (event) {
                    event.preventDefault();
                    jQuery('html, body').animate({scrollTop: 0}, duration);
                    return false;
                })
            });
        })(jQuery);
    </script>
    <script type="text/javascript" src="{{ asset('/js/jquery.fitvids.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/styleswitcher.js') }}"></script>
{{--    <script type="text/javascript" src="{{ asset('/js/custom-blog-grid-left.js') }}"></script>--}}
{{--    <link rel="stylesheet" type="text/css" href="{{ asset('/DataTables/datatables.min.css') }}"/>--}}

{{--    <script type="text/javascript" src="{{ asset('/DataTables/datatables.min.js') }}"></script>--}}
    <script type="text/javascript" src="{{ asset('/js/custom-slider-post.js') }}"></script>
<!-- End Document
================================================== -->
@endsection
