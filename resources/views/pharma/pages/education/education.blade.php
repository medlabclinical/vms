@extends('pharma.layouts.main')
@section('breadcrumbs')
    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset;background-color:#7AA43F;">
            <ol class="breadcrumb medlab_breadcrumbs" style="margin-bottom: unset;background-color:#7AA43F;">
            </ol>
        </div>
    </div>

    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset">
            <ol class="breadcrumb medlab_breadcrumbs">
                <li><a class="medlab_breadcrumbs_link" href="/">@lang('trs.home')</a></li>
                <li class="active medlab_breadcrumbs_text">@lang('trs.education')</li>
            </ol>
        </div>
    </div>
@endsection
@section('content')




    <main class="cd-main-content" id="main" style="margin-top:90px">

        <!-- TOP SECTION
       ================================================== -->

        <section class="home">
            <div class="slider-container">
                <div class="tp-banner-container">
                    <div class="tp-banner">
                        <ul>
                            <li data-transition="fade" data-slotamount="1" data-masterspeed="500"
                                data-saveperformance="on" data-title="Intro Slide">
                                <img src="{{ $image_url }}/www/Images/{{ $content->title_img }}"
                                     data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                            </li>
                            @if(isset($content->second_img))
                                <li data-transition="fade" data-slotamount="1" data-masterspeed="500"
                                    data-saveperformance="on" data-title="Intro Slide">
                                    <img src="{{ $image_url }}/www/Images/{{ $content->second_img }}"
                                         data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                                </li>
                            @endif
                            @if(isset($content->third_img))
                                <li data-transition="fade" data-slotamount="1" data-masterspeed="500"
                                    data-saveperformance="on" data-title="Intro Slide">
                                    <img src="{{ $image_url }}/www/Images/{{ $content->third_img }}"
                                         data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                                </li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <section class="section grey-section section-home-padding-top" style="padding-top: 10px;">

            <div class="container">
                <div class="sixteen columns">
                    <div class="section-title left">
                        <h1>{{ $content->title }}</h1>
                        @if(isset($content->subtitle))
                            <div class="subtitle left">{{ $content->subtitle }}</div>
                        @endif
                    </div>
                </div>
            </div>

        </section>
        @if($content->content)
            <section class="section white-section section-padding-top" id="scroll-link">
                <div class="container">
                    <div class="sixteen columns remove-bottom"
                    >
                        <div class="full-image">
                            <div class="articleClass">
                                {{--                            111--}}
                                {!! $content->content !!}
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        @endif

    <!-- SECTION
        ================================================== -->

        <section class="section white-section section-padding-top-bottom" style="padding-top: unset;">


            <div class="container">
                <div class="sixteen columns">
                    <form method="get" action="{{ route('pharma.articles') }}" id="filterForm" style="text-align: left;width: 100%">
{{--                    <div id="portfolio-filter">--}}
{{--                        <ul id="filter_user">--}}
                        <label for="role">Access&nbsp&nbsp&nbsp&nbsp</label>
                            <select onchange="submitForm()" name="role" id="role" class="form-control">
                                    <option value="*" @if(!isset($role) || $role == '*') selected @endif>Show All</option>
                                <option value="1" @if($role == '1') selected @endif >Show Practioner only</option>
                                <option value="0" @if($role == '0') selected @endif >Show Consumer only</option>
                            </select>
                        <br><br>
{{--                            <li><a href="#" class="current" data-filter="*" title="">Show All</a></li>--}}
{{--                            <li><a href="#" data-filter=".1" title="">Show Practioner only</a></li>--}}
{{--                            <li><a href="#" data-filter=".0" title="">Show Consumer only</a></li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                    <div id="portfolio-filter">--}}
{{--                        <ul id="filter">--}}
{{--                            @if($hasFilter)--}}
{{--                                <select onchange="submitForm()" name="category" id="category" class="form-control">--}}
{{--                                    <option value={{ $category[0] }} selected >{{ $category[0] }}</option>--}}
{{--                                    <option value="1"  >Show Practioner only</option>--}}
{{--                                    <option value="0"  >Show Consumer only</option>--}}
{{--                                </select>--}}
{{--                            @else--}}
                        <label for="article_category">Category</label>
                                <select onchange="submitForm()" name="article_category" id="article_category" class="form-control">
                                    <option value="*" @if(!isset($article_category) || $article_category == '*') selected @endif >Show All</option>
                                    @foreach($category as $item)
                                        <option value={{ $item }} @if($article_category == $item) selected @endif>{{ str_replace('_',' ',$item) }}</option>
                                    @endforeach
{{--                                    <option value="1"  >Show Practioner only</option>--}}
{{--                                    <option value="0"  >Show Consumer only</option>--}}
                                </select>

{{--                        @endif--}}
{{--                                --}}
{{--                            <li><a href="#" class="current" data-filter="*" title="">Show All</a></li>--}}
{{--                            @foreach($category as $item)--}}
{{--                                <li><a href="#" data-filter=".{{ str_replace(' ','_',$item) }}" title="">{{ $item }}</a></li>--}}
{{--                            @endforeach--}}
{{--                                @endif--}}

{{--                        </ul>--}}
{{--                    </div>--}}
                    </form>
                </div>
            </div>

            <div class="clear"></div>

{{--            <div class="container">--}}
{{--                <div class="four columns">--}}
{{--                    <div class="post-sidebar">--}}
{{--                                                <input name="search" type="text" placeholder="type to search and hit enter"/>--}}
{{--                        <div class="separator-sidebar"></div>--}}
{{--                        <h6>Categories</h6>--}}
{{--                        <ul class="link-recents">--}}
{{--                            <div id="portfolio-filter">--}}
{{--                                <ul id="filter">--}}
{{--                                    @foreach($list as $item)--}}

{{--                                        <li style="width:100%;text-align: left;"><a--}}
{{--                                                style="margin:unset;letter-spacing: unset" href="#"--}}
{{--                                                data-filter=".{{ $item[0]->category }}"--}}
{{--                                                title="">{{ $item[0]->category }} ({{ count($item) }})</a></li>--}}
{{--                                    @endforeach--}}
                                    {{--                                    <li style="width:100%;text-align: left;"><a--}}
                                    {{--                                                style="margin:unset;letter-spacing: unset " href="#"--}}
                                    {{--                                                data-filter=".advertisement" title="">Design (3)</a></li>--}}
                                    {{--                                    <li style="width:100%;text-align: left;"><a--}}
                                    {{--                                                style="margin:unset;letter-spacing: unset " href="#"--}}
                                    {{--                                                data-filter=".origin" title="">Image (1)</a></li>--}}
                                    {{--                                    <li style="width:100%;text-align: left;"><a--}}
                                    {{--                                                style="margin:unset;letter-spacing: unset " href="#"--}}
                                    {{--                                                data-filter=".photo" title="">Videos (4)</a></li>--}}
                                    {{--                                    <li style="width:100%;text-align: left;"><a--}}
                                    {{--                                                style="margin:unset;letter-spacing: unset " href="#"--}}
                                    {{--                                                data-filter=".photo" title="">Word (5)</a></li>--}}
{{--                                </ul>--}}
{{--                            </div>--}}
{{--                        </ul>--}}

{{--                        <div class="separator-sidebar"></div>--}}
{{--                        <h6>recent posts</h6>--}}
{{--                        <ul class="link-recents">--}}
{{--                            @if(count($allArticles)<=5)--}}
{{--                                @foreach ($allArticles as $item)--}}
{{--                                    <li>--}}
{{--                                        <a href={{ route('pharma.articles_content',['id' => $item->id]) }}>{{ $item->title }}</a>--}}
{{--                                    </li>--}}
{{--                                @endforeach--}}
{{--                            @else--}}
{{--                                @for($i=0;$i<5;$i++)--}}
{{--                                    <li>--}}
{{--                                        <a href={{ route('pharma.articles_content',['id' => $allArticles[$i]->id]) }}>{{ $allArticles[$i]->title }}</a>--}}
{{--                                    </li>--}}
{{--                                @endfor--}}
{{--                            @endif--}}
{{--                        </ul>--}}
                        {{--                        <div class="separator-sidebar"></div>--}}
                        {{--                        <h6>LATEST PROJECTS</h6>--}}
                        {{--                        <ul class="lat-pro">--}}
                        {{--                            <li><a href="#">--}}
                        {{--                                    <div class="lat-pro-img"><img src="images/portfolio/1.jpg" alt=""/></div>--}}
                        {{--                                </a></li>--}}
                        {{--                            <li><a href="#">--}}
                        {{--                                    <div class="lat-pro-img"><img src="images/portfolio/2.jpg" alt=""/></div>--}}
                        {{--                                </a></li>--}}
                        {{--                            <li><a href="#">--}}
                        {{--                                    <div class="lat-pro-img"><img src="images/portfolio/3.jpg" alt=""/></div>--}}
                        {{--                                </a></li>--}}
                        {{--                            <li><a href="#">--}}
                        {{--                                    <div class="lat-pro-img"><img src="images/portfolio/4.jpg" alt=""/></div>--}}
                        {{--                                </a></li>--}}
                        {{--                        </ul>--}}
                        {{--                        <div class="separator-sidebar"></div>--}}
                        {{--                        <h6>LATEST video</h6>--}}
                        {{--                            @if(count($videoArticles)<=2)--}}
                        {{--                                @foreach ($videoArticles as $item)--}}
                        {{--                                <iframe src="{{ $item->video_link }}?title=0&amp;byline=0&amp;portrait=0&amp;color=000000"--}}
                        {{--                                        width="940" height="450"></iframe>--}}
                        {{--                                    <br>--}}
                        {{--                                @endforeach--}}
                        {{--                            @else--}}
                        {{--                                @for($i=0;$i<2;$i++)--}}
                        {{--                                <iframe src="{{ $allArticles[$i]->video_link }}?title=0&amp;byline=0&amp;portrait=0&amp;color=000000"--}}
                        {{--                                        width="940" height="450"></iframe>--}}
                        {{--                                <br>--}}
                        {{--                                @endfor--}}
                        {{--                            @endif--}}
                        {{--                        <iframe src="http://player.vimeo.com/video/96696089?title=0&amp;byline=0&amp;portrait=0&amp;color=000000"--}}
                        {{--                                width="940" height="450"></iframe>--}}
                        {{--                        <div class="separator-sidebar"></div>--}}
                        {{--                        <h6>tags</h6>--}}
                        {{--                        <ul class="link-tag">--}}
                        {{--                            <li><a href="#">Analysis</a></li>--}}
                        {{--                            <li><a href="#">Art</a></li>--}}
                        {{--                            <li><a href="#">Articles</a></li>--}}
                        {{--                            <li><a href="#">Audio</a></li>--}}
                        {{--                            <li><a href="#">Business</a></li>--}}
                        {{--                            <li><a href="#">Culture</a></li>--}}
                        {{--                            <li><a href="#">Development</a></li>--}}
                        {{--                            <li><a href="#">Ecology</a></li>--}}
                        {{--                            <li><a href="#">Events</a></li>--}}
                        {{--                            <li><a href="#">Information</a></li>--}}
                        {{--                            <li><a href="#">Inspiration</a></li>--}}
                        {{--                            <li><a href="#">Nature</a></li>--}}
                        {{--                            <li><a href="#">Opportunities</a></li>--}}
                        {{--                            <li><a href="#">Science</a></li>--}}
                        {{--                            <li><a href="#">Trends</a></li>--}}
                        {{--                            <li><a href="#">Video</a></li>--}}
                        {{--                        </ul>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="twelve columns remove-top">--}}
                    <div class="blog-wrapper">
                        <div id="blog-grid-masonry">
                            @foreach($allArticles as $item)
                                @if($item->article_type=='link')
                                    <a href={{ route('pharma.articles_content',['id' => $item->id]) }} class="animsition-link">
                                        <div class="blog-box-3 {{ str_replace(' ','_',$item['category']) }} {{ $item['article_type'] }} {{ $item['req_login'] }}">
                                            <div class="blog-box-1 link-post">
                                                <h4>{{ $item->title }}</h4>
                                                <p>{{ $item->preview_text }}</p>
                                                <p>{{ substr($item->date,0,10) }}</p>
                                                @if(isset($item->author))<p>Author: {{ $item->author }}</p>@endif
                                                <div class="link">&#xf178;</div>
                                            </div>
                                        </div>
                                    </a>
                                @elseif($item->article_type=='video')
                                    <a href={{ route('pharma.articles_content',['id' => $item->id]) }}class="animsition-link">
                                        <div class="blog-box-3  {{ str_replace(' ','_',$item['category']) }} {{ $item['article_type'] }} {{ $item['req_login'] }}">
                                            <div class="blog-box-1 grey-section">
                                                @if(($item->req_login && !(Auth::guard('practitioner')->check()||Auth::guard('patient')->check())))
                                                    <div style="opacity: 0.85;background: black;">
                                                        <iframe srcdoc="<p style='text-align:center;margin-top:23%;font-size:xxx-large;color:white'>Please login to view this video</p>" src="{{ $item->video_link }}" width="100%" height="270"
                                                                allowfullscreen></iframe>
                                                    </div>
                                                @else
{{--                                                    <iframe src="{{ $item->video_link }}" width="100%" height="220"--}}
{{--                                                            allowfullscreen></iframe>--}}
                                                    <iframe src="{{ $item->video_link }}" width="100%" height="220" id={{ substr($item->video_link,31,9) }}
                                                             frameborder=”0″ data-progress=”true” data-seek=”true”
                                                            data-bounce=”false” webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>



                                                @endif
{{--                                                <img src="{{ $image_url }}/www/Images/{{ $item->title_img }}"--}}
{{--                                                     alt="{{ $item->title_alt }}">--}}
                                                <h4>{{ $item->title }}</h4>
                                                <p>{{ $item->preview_text }}</p>
                                                <p>{{ substr($item->date,0,10) }}</p>
                                                @if(isset($item->author))<p>Author: {{ $item->author }}</p>@endif
                                                <div class="link">&#xf178;</div>
                                            </div>
                                        </div>
                                    </a>
                                @else
                                    <a href={{ route('pharma.articles_content',['id' => $item->id]) }} class="animsition-link">
                                        <div class="blog-box-3  {{ str_replace(' ','_',$item['category']) }} {{ $item['article_type'] }} {{ $item['req_login'] }}">
                                            <div class="blog-box-1 grey-section">
                                                <img src="{{ $image_url }}/www/Images/{{ $item->title_img }}"
                                                     alt="{{ $item->title_alt }}">
                                                <h4>{{ $item->title }}</h4>
                                                <p>{{ $item->preview_text }}</p>
                                                <p>{{ substr($item->date,0,10) }}</p>
                                                @if(isset($item->author))<p>Author: {{ $item->author }}</p>@endif
                                                <div class="link">&#xf178;</div>
                                            </div>
                                        </div>
                                    </a>
                                @endif
                            @endforeach
                        </div>
                    </div>
{{--                            <a href="image-post.html" class="animsition-link">--}}
{{--                                <div class="blog-box-3 photo">--}}
{{--                                    <div class="blog-box-1 grey-section">--}}
{{--                                        <img src="images/blog/1.jpg"  alt="">--}}
{{--                                        <div class="blog-date-1">21.11.'14</div>--}}
{{--                                        <div class="blog-comm-1">3 <span>&#xf086;</span></div>--}}
{{--                                        <h6>Latest post</h6>--}}
{{--                                        <p>Lorem ipsum dolor sit consectetur amet, adipisicing elit.</p>--}}
{{--                                        <div class="link">&#xf178;</div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </a>--}}
{{--                            <a href="link-post.html" class="animsition-link">--}}
{{--                                <div class="blog-box-3 links">--}}
{{--                                    <div class="blog-box-1 link-post">--}}
{{--                                        <div class="blog-date-1">21.11.'14</div>--}}
{{--                                        <div class="blog-comm-1">3 <span>&#xf086;</span></div>--}}
{{--                                        <h6>When you are courting a nice girl an hour seems like a second.</h6>--}}
{{--                                        <div class="link">&#xf178;</div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </a>--}}
{{--                            <a href="slider-post.html" class="animsition-link">--}}
{{--                                <div class="blog-box-3 media">--}}
{{--                                    <div class="blog-box-1 grey-section">--}}
{{--                                        <img src="images/blog/2.jpg"  alt="">--}}
{{--                                        <div class="blog-date-1">17.11.'14</div>--}}
{{--                                        <div class="blog-comm-1">2 <span>&#xf086;</span></div>--}}
{{--                                        <h6>Latest post</h6>--}}
{{--                                        <p>Lorem ipsum dolor sit consectetur amet, adipisicing elit.</p>--}}
{{--                                        <div class="link">&#xf178;</div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </a>--}}
{{--                            <a href="video-post.html" class="animsition-link">--}}
{{--                                <div class="blog-box-3 news">--}}
{{--                                    <div class="blog-box-1 grey-section">--}}
{{--                                        <img src="images/blog/4.jpg"  alt="">--}}
{{--                                        <div class="blog-date-1">13.11.'14</div>--}}
{{--                                        <div class="blog-comm-1">5 <span>&#xf086;</span></div>--}}
{{--                                        <h6>Latest post</h6>--}}
{{--                                        <p>Lorem ipsum dolor sit consectetur amet, adipisicing elit.</p>--}}
{{--                                        <div class="link">&#xf178;</div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </a>--}}
{{--                            <a href="image-post.html" class="animsition-link">--}}
{{--                                <div class="blog-box-3 news">--}}
{{--                                    <div class="blog-box-1 grey-section">--}}
{{--                                        <img src="images/blog/3.jpg"  alt="">--}}
{{--                                        <div class="blog-date-1">05.11.'14</div>--}}
{{--                                        <div class="blog-comm-1">7 <span>&#xf086;</span></div>--}}
{{--                                        <h6>Latest post</h6>--}}
{{--                                        <p>Lorem ipsum dolor sit consectetur amet, adipisicing elit.</p>--}}
{{--                                        <div class="link">&#xf178;</div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </a>--}}
{{--                            <a href="link-post.html" class="animsition-link">--}}
{{--                                <div class="blog-box-3 links">--}}
{{--                                    <div class="blog-box-1 link-post">--}}
{{--                                        <div class="blog-date-1">23.10.'14</div>--}}
{{--                                        <div class="blog-comm-1">3 <span>&#xf086;</span></div>--}}
{{--                                        <h6>When you sit on a red-hot cinder a second seems like an hour. That's relativity.</h6>--}}
{{--                                        <div class="link">&#xf178;</div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </a>--}}
{{--                            <a href="image-post.html" class="animsition-link">--}}
{{--                                <div class="blog-box-3 photo">--}}
{{--                                    <div class="blog-box-1 grey-section">--}}
{{--                                        <img src="images/blog/6.jpg"  alt="">--}}
{{--                                        <div class="blog-date-1">25.10.'14</div>--}}
{{--                                        <div class="blog-comm-1">2 <span>&#xf086;</span></div>--}}
{{--                                        <h6>Latest post</h6>--}}
{{--                                        <p>Lorem ipsum dolor sit consectetur amet, adipisicing elit.</p>--}}
{{--                                        <div class="link">&#xf178;</div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </a>--}}
{{--                            <a href="video-post.html" class="animsition-link">--}}
{{--                                <div class="blog-box-3 news">--}}
{{--                                    <div class="blog-box-1 grey-section">--}}
{{--                                        <img src="images/blog/5.jpg"  alt="">--}}
{{--                                        <div class="blog-date-1">16.10.'14</div>--}}
{{--                                        <div class="blog-comm-1">5 <span>&#xf086;</span></div>--}}
{{--                                        <h6>Latest post</h6>--}}
{{--                                        <p>Lorem ipsum dolor sit consectetur amet, adipisicing elit.</p>--}}
{{--                                        <div class="link">&#xf178;</div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </a>--}}
{{--                            <a href="slider-post.html" class="animsition-link">--}}
{{--                                <div class="blog-box-3 media">--}}
{{--                                    <div class="blog-box-1 grey-section">--}}
{{--                                        <img src="images/blog/7.jpg"  alt="">--}}
{{--                                        <div class="blog-date-1">29.10.'14</div>--}}
{{--                                        <div class="blog-comm-1">3 <span>&#xf086;</span></div>--}}
{{--                                        <h6>Latest post</h6>--}}
{{--                                        <p>Lorem ipsum dolor sit consectetur amet, adipisicing elit.</p>--}}
{{--                                        <div class="link">&#xf178;</div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--            {{ $allArticles->links() }}--}}
        </section>


        <!-- SECTION
        ================================================== -->

                <section class="section grey-section section-padding-top-bottom">
                    <div class="container">
                        <div class="sixteen columns">
                            <div class="blog-left-right-links pagination">
                                @if($allArticles->currentPage()-1!=0)
                                <a href="{{ route('pharma.articles') }}?role={{ $role }}&article_category={{ $article_category }}&page={{ $allArticles->currentPage()-1 }}">
                                    <div class="blog-left-link"><p>PREVIOUS 12</p></div>
                                </a>
                                @endif
{{--                                @for($i=1;$i<$allArticles->total()+1;$i++)--}}
{{--                                        <a href="{{ route('pharma.articles') }}?role={{ $role }}&article_category={{ $article_category }}&page={{ $i }}">--}}
{{--                                            <div class="blog-left-link"><p>{{ $i }}</p></div>--}}
{{--                                        </a>--}}
{{--                                    @endfor--}}

                                @if($allArticles->currentPage()!=$allArticles->lastPage())
                                <a href="{{ route('pharma.articles') }}?role={{ $role }}&article_category={{ $article_category }}&page={{ $allArticles->currentPage()+1 }}">
                                    <div class="blog-right-link"><p>NEXT 12</p></div>
                                </a>
                                    @endif
{{--                                    <a>--}}
                                        <div style="padding: 6px">
                                            <select onchange="changePage()" id="page" style="float: right;margin-right: 5%;">
                                                @for($i=1;$i<$allArticles->lastPage()+1;$i++)
                                                    <option value={{ $i }} @if($allArticles->currentPage() == $i) selected @endif >{{ $i }}</option>
                                                @endfor
                                            </select><p style="float: right">Page No.</p>
                                        </div>
{{--                                    </a>--}}


                            </div>
                        </div>
                    </div>

                </section>

        @include('pharma.layouts.footer')
    </main>

    <div class="scroll-to-top">&#xf106;</div>



    <!-- JAVASCRIPT
    ================================================== -->
    <script type="text/javascript" src="{{ asset('/js/jquery-2.1.1.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/modernizr.custom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.mobile.custom.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/retina-1.1.0.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.animsition.min.js') }}"></script>
    <script type="text/javascript">
        function submitForm() {
            var form = document.getElementById("filterForm");
            form.submit();
        }
        function changePage() {
            var page=document.getElementById("page").value;

            location.href = "{{ route('pharma.articles') }}?role={{ $role }}&article_category={{ $article_category }}&page="+page;

        }


        (function ($) {
            "use strict";
            $(document).ready(function () {
                @if (session('login_error'))
                    open_login();
                @endif

                $(".animsition").animsition({

                    inClass: 'zoom-in-sm',
                    outClass: 'zoom-out-sm',
                    inDuration: 1500,
                    outDuration: 800,
                    linkElement: '.animsition-link',
                    // e.g. linkElement   :   'a:not([target="_blank"]):not([href^=#])'
                    loading: true,
                    loadingParentElement: 'body', //animsition wrapper element
                    loadingClass: 'animsition-loading',
                    unSupportCss: ['animation-duration',
                        '-webkit-animation-duration',
                        '-o-animation-duration'
                    ],
                    //"unSupportCss" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
                    //The default setting is to disable the "animsition" in a browser that does not support "animation-duration".

                    overlay: false,

                    overlayClass: 'animsition-overlay-slide',
                    overlayParentElement: 'body'
                });
            });

            $(document).ready(function () {
                $('#example').DataTable({
                    'searching': false,
                    'lengthChange': false,
                });

            });
        })(jQuery);
    </script>
    <script type="text/javascript" src="{{ asset('/js/jquery.easing.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.hidescroll.min.js') }}"></script>
    <script type="text/javascript">
        $('.header-top').hidescroll();
    </script>
{{--    <script type="text/javascript" src="{{ asset('/js/smoothScroll.js') }}"></script>--}}
    <script type="text/javascript" src="{{ asset('/js/jquery.parallax-1.1.3.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/owl.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/scrollReveal.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/masonry.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/isotope.js') }}"></script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            window.scrollReveal = new scrollReveal();
        })(jQuery);
    </script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            jQuery(document).ready(function () {
                var offset = 450;
                var duration = 500;
                jQuery(window).scroll(function () {
                    if (jQuery(this).scrollTop() > offset) {
                        jQuery('.scroll-to-top').fadeIn(duration);
                    } else {
                        jQuery('.scroll-to-top').fadeOut(duration);
                    }
                });

                jQuery('.scroll-to-top').click(function (event) {
                    event.preventDefault();
                    jQuery('html, body').animate({scrollTop: 0}, duration);
                    return false;
                })
            });
        // })(jQuery);
        /* Portfolio Sorting */


        // (function ($) {
        // //
        // //
        //     var container = $('#blog-grid-masonry');
        // //
        // //
        //     function getNumbColumns() {
        //
        //         var winWidth = $(window).width(),
        //             columnNumb = 1;
        //
        //
        //         if (winWidth > 1500) {
        //             columnNumb = 3;
        //         } else if (winWidth > 1200) {
        //             columnNumb = 3;
        //         } else if (winWidth > 900) {
        //             columnNumb = 2;
        //         } else if (winWidth > 600) {
        //             columnNumb = 2;
        //         } else if (winWidth > 300) {
        //             columnNumb = 1;
        //         }
        //         console.log(columnNumb)
        //
        //         return columnNumb;
        //     }
        //
        //
        //     function setColumnWidth() {
        //         var winWidth = $(window).width(),
        //             columnNumb = getNumbColumns(),
        //
        //             postWidth = Math.floor(winWidth / columnNumb);
        //
        //     }
        // //
        //     $('#portfolio-filter #filter a').click(function () {
        //         var selector_1 = $(this).attr('data-filter');
        //         var selector_2 = document.getElementsByClassName('current')[0].getAttribute('data-filter');
        //         var selector = selector_1+selector_2;
        //         if(selector=='**'){
        //             selector='*';
        //         }
        //         console.log(selector)
        //
        //         $(this).parent().parent().find('a').removeClass('current');
        //         $(this).addClass('current');
        //
        //         container.isotope({
        //             filter: selector,
        //         });
        //
        //         setTimeout(function () {
        //             reArrangeProjects();
        //         }, 300);
        //
        //
        //         return false;
        //     });
        //     $('#portfolio-filter #filter_user a').click(function () {
        //         var selector_1 = $(this).attr('data-filter');
        //         var selector_2 = document.getElementsByClassName('current')[1].getAttribute('data-filter');
        //         var selector = selector_2+selector_1;
        //         if(selector=='**'){
        //             selector='*';
        //         }
        //         console.log(selector)
        //
        //         $(this).parent().parent().find('a').removeClass('current');
        //         $(this).addClass('current');
        //
        //         container.isotope({
        //             filter: selector
        //         });
        //
        //         setTimeout(function () {
        //             reArrangeProjects();
        //         }, 300);
        //
        //
        //         return false;
        //     });
        //
        //     function reArrangeProjects() {
        //         setColumnWidth();
        //         container.isotope('reLayout');
        //     }
        //
        //
        //     container.imagesLoaded(function () {
        //         setColumnWidth();
        //
        //
        //         container.isotope({
        //             itemSelector: '.blog-box-3',
        //             layoutMode: 'masonry',
        //             resizable: false
        //         });
        //     });


            // $(window).on('debouncedresize', function () {
            //     reArrangeProjects();
            //
            // });
        //
        //
        // })(jQuery);
        //
        // /* DebouncedResize Function */
        // (function ($) {
        //     var $event = $.event,
        //         $special,
        //         resizeTimeout;
        //
        //
        //     $special = $event.special.debouncedresize = {
        //         setup : function () {
        //             $(this).on('resize', $special.handler);
        //         },
        //         teardown : function () {
        //             $(this).off('resize', $special.handler);
        //         },
        //         handler : function (event, execAsap) {
        //             var context = this,
        //                 args = arguments,
        //                 dispatch = function () {
        //                     event.type = 'debouncedresize';
        //
        //                     $event.dispatch.apply(context, args);
        //                 };
        //
        //
        //             if (resizeTimeout) {
        //                 clearTimeout(resizeTimeout);
        //             }
        //
        //
        //             execAsap ? dispatch() : resizeTimeout = setTimeout(dispatch, $special.threshold);
        //         },
        //         threshold : 150
        //     };
        // } )(jQuery);
        })(jQuery);

    </script>



    <script src=”/js/jquery-1.12.1.min.js”></script>

    <script src=”/js/vimeo.ga.min.js”></script>





    {{--    <script type="text/javascript" src="{{ asset('/js/jquery.fitvids.js') }}"></script>--}}
    <script type="text/javascript" src="{{ asset('/js/styleswitcher.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.themepunch.tools.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.themepunch.revolution.min.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('/DataTables/datatables.min.css') }}"/>

    <script type="text/javascript" src="{{ asset('/DataTables/datatables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/custom-corporate-home-1.js') }}"></script>
{{--    <script type="text/javascript" src={{ asset('/js/custom-blog-grid-full.js') }}></script>--}}


    <!-- End Document
================================================== -->
@endsection
