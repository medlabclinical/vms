@extends('pharma.layouts.main')
@section('breadcrumbs')
    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset;background-color:#7AA43F;">
            <ol class="breadcrumb medlab_breadcrumbs" style="margin-bottom: unset;background-color:#7AA43F;">
            </ol>
        </div>
    </div>

    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset">
            <ol class="breadcrumb medlab_breadcrumbs">
                <li><a class="medlab_breadcrumbs_link" href="/">@lang('trs.home')</a></li>
                <li><a class="medlab_breadcrumbs_link" href="{{ route('pharma.articles') }}">@lang('trs.education')</a></li>
                <li class="active medlab_breadcrumbs_text">{{ $article->title }}</li>
            </ol>
        </div>
    </div>
@endsection
@section('content')


    <main class="cd-main-content" id="main">



        <!-- TOP SECTION
        ================================================== -->

        <section class="section white-section section-home-padding-top">

            <div class="container">
                <div class="sixteen columns">
                    <div class="section-title left">
                        <h1>{{ $article->title }}</h1>
                        <div class="subtitle left big">{{ $article->subtitle }}</div>
                    </div>
                </div>
            </div>

        </section>

        <!-- SECTION
        ================================================== -->

        <section class="section white-section section-padding-bottom">

            <div class="container">

                {{--                <div class="col-md-12 col-sm-12 col-xs-12">--}}

                <div class="medlab_news_item">
                    <div class="medlab_news_item_content_wrapper">
                        {{--                            <div class="medlab_news_item_title">--}}
                        {{--                                Current Medlab Efficacy Magazine--}}
                        {{--                            </div>--}}
                        <div class="medlab_news_item_body">

                            <p>


                                <embed src="{{ $image_url }}/article-pdf/{{ $article->link_url }}" type="application/pdf" width="100%" height="100%" />

                            </p>

                        </div>
                    </div>
                </div>

            </div>

        </section>


        <!-- SECTION
        ================================================== -->

{{--        <section class="section grey-section section-padding-top-bottom">--}}
{{--            <div class="container">--}}
{{--                <div class="sixteen columns">--}}
{{--                    <div class="section-title">--}}
{{--                        <h3>Related Posts</h3>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="one-third column" data-scroll-reveal="enter left move 200px over 1s after 0.3s">--}}
{{--                    <a href="image-post.html" class="animsition-link">--}}
{{--                        <div class="blog-box-1 white-section">--}}
{{--                            <img src="images/blog/1.jpg"  alt="">--}}
{{--                            <div class="blog-date-1">21.11.'14</div>--}}
{{--                            <div class="blog-comm-1">3 <span>&#xf086;</span></div>--}}
{{--                            <h6>Latest post</h6>--}}
{{--                            <p>Lorem ipsum dolor sit consectetur amet, adipisicing elit.</p>--}}
{{--                            <div class="link">&#xf178;</div>--}}
{{--                        </div>--}}
{{--                    </a>--}}
{{--                </div>--}}
{{--                <div class="one-third column" data-scroll-reveal="enter bottom move 200px over 1s after 0.3s">--}}
{{--                    <a href="slider-post.html" class="animsition-link">--}}
{{--                        <div class="blog-box-1 white-section">--}}
{{--                            <img src="images/blog/2.jpg"  alt="">--}}
{{--                            <div class="blog-date-1">17.11.'14</div>--}}
{{--                            <div class="blog-comm-1">2 <span>&#xf086;</span></div>--}}
{{--                            <h6>Latest post</h6>--}}
{{--                            <p>Lorem ipsum dolor sit consectetur amet, adipisicing elit.</p>--}}
{{--                            <div class="link">&#xf178;</div>--}}
{{--                        </div>--}}
{{--                    </a>--}}
{{--                </div>--}}
{{--                <div class="one-third column" data-scroll-reveal="enter right move 200px over 1s after 0.3s">--}}
{{--                    <a href="video-post.html" class="animsition-link">--}}
{{--                        <div class="blog-box-1 white-section">--}}
{{--                            <img src="images/blog/3.jpg"  alt="">--}}
{{--                            <div class="blog-date-1">14.11.'14</div>--}}
{{--                            <div class="blog-comm-1">7 <span>&#xf086;</span></div>--}}
{{--                            <h6>Latest post</h6>--}}
{{--                            <p>Lorem ipsum dolor sit consectetur amet, adipisicing elit.</p>--}}
{{--                            <div class="link">&#xf178;</div>--}}
{{--                        </div>--}}
{{--                    </a>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </section>--}}




        <!-- FOOTER
        ================================================== -->



        @include('pharma.layouts.footer')


    </main>

    <div class="scroll-to-top">&#xf106;</div>





<!-- JAVASCRIPT
================================================== -->
    <script type="text/javascript" src="{{ asset('/js/jquery-2.1.1.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/modernizr.custom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.mobile.custom.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/retina-1.1.0.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.animsition.min.js') }}"></script>
    <script type="text/javascript">
        (function ($) {
            {{--var aImgs=document.getElementsByTagName('img')--}}
            {{--for(var i=0; i<aImgs.length;i++){--}}
            {{--    if(i>1)--}}
            {{--    {--}}
            {{--        console.log(i);--}}
            {{--        aImgs[i].src = "{{ $image_url }}" + aImgs[i].getAttribute('src');--}}
            {{--    }--}}
            {{--};--}}

            "use strict";
            $(document).ready(function () {
                @if (session('login_error'))
                open_login();
                @endif
                $(".animsition").animsition({

                    inClass: 'zoom-in-sm',
                    outClass: 'zoom-out-sm',
                    inDuration: 1500,
                    outDuration: 800,
                    linkElement: '.animsition-link',
                    // e.g. linkElement   :   'a:not([target="_blank"]):not([href^=#])'
                    loading: true,
                    loadingParentElement: 'body', //animsition wrapper element
                    loadingClass: 'animsition-loading',
                    unSupportCss: ['animation-duration',
                        '-webkit-animation-duration',
                        '-o-animation-duration'
                    ],
                    //"unSupportCss" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
                    //The default setting is to disable the "animsition" in a browser that does not support "animation-duration".

                    overlay: false,

                    overlayClass: 'animsition-overlay-slide',
                    overlayParentElement: 'body'
                });
            });

            $(document).ready(function () {
                $('#example').DataTable({
                    'searching': false,
                    'lengthChange': false,
                });

            });
        })(jQuery);
    </script>
    <script type="text/javascript" src="{{ asset('/js/jquery.easing.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.hidescroll.min.js') }}"></script>
    <script type="text/javascript">
        $('.header-top').hidescroll();
    </script>
    <script type="text/javascript" src="{{ asset('/js/smoothScroll.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.parallax-1.1.3.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/owl.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/scrollReveal.js') }}"></script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            window.scrollReveal = new scrollReveal();
        })(jQuery);
    </script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            jQuery(document).ready(function () {
                var offset = 450;
                var duration = 500;
                jQuery(window).scroll(function () {
                    if (jQuery(this).scrollTop() > offset) {
                        jQuery('.scroll-to-top').fadeIn(duration);
                    } else {
                        jQuery('.scroll-to-top').fadeOut(duration);
                    }
                });

                jQuery('.scroll-to-top').click(function (event) {
                    event.preventDefault();
                    jQuery('html, body').animate({scrollTop: 0}, duration);
                    return false;
                })
            });
        })(jQuery);
    </script>
    <script type="text/javascript" src="{{ asset('/js/jquery.fitvids.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/styleswitcher.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/custom-blog-grid-left.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('/DataTables/datatables.min.css') }}"/>

    <script type="text/javascript" src="{{ asset('/DataTables/datatables.min.js') }}"></script>
<!-- End Document
================================================== -->
@endsection
