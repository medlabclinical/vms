@extends('pharma.layouts.main')
@section('breadcrumbs')
    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset;background-color:#7AA43F;">
            <ol class="breadcrumb medlab_breadcrumbs" style="margin-bottom: unset;background-color:#7AA43F;">
            </ol>
        </div>
    </div>

    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset">
            <ol class="breadcrumb medlab_breadcrumbs">
                <li><a class="medlab_breadcrumbs_link" href="/">@lang('trs.home')</a></li>
                <li><a class="medlab_breadcrumbs_link" href="{{ route('pharma.articles') }}">@lang('trs.articles')</a></li>
                <li class="active medlab_breadcrumbs_text">{{ $article->title }}</li>
            </ol>
        </div>
    </div>
@endsection
@section('content')


    <main class="cd-main-content" id="main">

        <!-- HOME SECTION
        ================================================== -->

        {{--        <section class="section grey-section section-padding-top-bottom" style="padding-top: 200px;">--}}

        {{--                <div class="sixteen columns">--}}
        {{--                    <div class="section-title">--}}
        {{--                        <h1>{{ $article->title }}</h1>--}}
        {{--                    </div>--}}
        {{--                </div>--}}
        {{--                <div class="eleven columns">--}}
        {{--                    <iframe src="{{ $article->video_link }}?title=0&amp;byline=0&amp;portrait=0&amp;color=cfa144" width="940" height="450" allowfullscreen></iframe>--}}
        {{--                </div>--}}
        {{--            <div class="container">--}}
        {{--                <div class="sixteen columns">--}}
        {{--                    <iframe src="{{ $article->video_link }}" width="940" height="450"--}}
        {{--                            allowfullscreen></iframe>--}}
        {{--                </div>--}}
        {{--            </div>--}}
        {{--                <div class="five columns">--}}
        {{--                    <div class="ajax-project-content">--}}
        {{--                        <div class="articleClass padding">--}}
        {{--                            {!! $article->content !!}--}}
        {{--                        </div>--}}
        {{--                        <div class="ajax-project-info">--}}
        {{--                            <div class="ajax-info"><span>&#xf1d9;</span><strong>Created by:</strong> Medlab</div>--}}
        {{--                            <div class="ajax-info"><span>&#xf017;</span><strong>Completed:</strong>{{ substr($article->date,0,10) }}</div>--}}
        {{--                        </div>--}}
        {{--                        <a href="#"><div class="ajax-link">pharma.medlab.co</div></a>--}}
        {{--                    </div>--}}
        {{--                </div>--}}
        {{--        </section>--}}
        {{--            <section class="section white-section section-padding-bottom">--}}
        {{--                <div class="container">--}}
        {{--                    <div class="twelve columns" style="width: unset">--}}
        {{--                        <div class="blog-big-wrapper" data-scroll-reveal="enter bottom move 200px over 1s after 0.3s" style="color:white;">--}}
        {{--                        <h3 style="text-align: unset"><strong>{{ $article->title }}</strong></h3>--}}
        {{--                            <br>--}}
        {{--                            <h4 style="text-align: unset">{{ substr($article->date,0,10) }}</h4>--}}
        {{--                        <div class="articleClass padding">--}}
        {{--                            {!! $article->content !!}--}}
        {{--                        </div>--}}

        {{--                        </div>--}}
        {{--                    </div>--}}
        {{--                </div>--}}
        {{--            </section>--}}


        <section class="section white-section section-home-padding-top">

            <div class="container">
                <div class="sixteen columns">
                    <div class="section-title left">
                        <h1>{{ $article->title }}</h1>
                        <div class="subtitle left big">{{ $article->subtitle }}</div>
                        @if(isset($article->date))
                            <div class="subtitle left">Date:&nbsp&nbsp{{ substr($article->date,0,10) }}</div>@endif
                        @if(count($article->member))
                            <div class="subtitle left">Author:&nbsp&nbsp
                                @if(count($article->member))
                                    {{$article->member[0]['name']}}
                                    @if(count($article->member)!=1)
                                        @for($i=1;$i<count($article->member);$i++)
                                            ,&nbsp{{ $article->member[$i]['name'] }}
                                        @endfor
                                    @endif
                                @endif
                            </div>@endif
                    </div>
                </div>
            </div>

        </section>

        <!-- SECTION
        ================================================== -->

        <section class="section white-section section-padding-bottom">

            <div class="container">
                <div class="twelve columns">

                    <div class="blog-big-wrapper white-section"
                         data-scroll-reveal="enter bottom move 200px over 1s after 0.3s"
                         style="color:white;padding:unset;">
                        {{--                        <div class="big-post-date"><span>&#xf073;</span> {{ substr($article->date,0,10) }}</div>--}}
                        {{--                        <img src="{{ $image_url }}/www/Images/{{ $article->title_img }}" alt="{{ $article->title_alt }}">--}}

                        {{--                        <div class="container">--}}
                        {{--                            <div class="sixteen columns">--}}
                        @if(($article->req_login && !(Auth::guard('practitioner')->check()||Auth::guard('patient')->check())))
                        <div style="opacity: 0.85;background: black;">
                        <iframe srcdoc="<p style='text-align:center;margin-top:25%;font-size:xxx-large;color:white'>Please login to view this video</p>" src="{{ $article->video_link }}" width="100" height="56"
                                allowfullscreen></iframe>
                        </div>
                        @else
                            <iframe src="{{ $article->video_link }}" width="100" height="56"
                                    allowfullscreen></iframe>
                        @endif
{{--                        <iframe srcdoc="<p>Hello world!</p>" src="demo_iframe_srcdoc.htm"></iframe>--}}
                    </div>
                    @if(($article->req_login && (Auth::guard('practitioner')->check()||Auth::guard('patient')->check()))|| !$article->req_login)
                        <div class="grey-section" style="margin-top: 20px;">
                            <div class="articleClass padding">
                                {!! $article->content !!}
                            </div>
                        </div>

                        @if(!$article->disable_comments)
                            <div class="post-content-com-top grey-section"
                                 data-scroll-reveal="enter bottom move 200px over 1s after 0.3s">
                                <p>COMMENTS <span>{{ count($comments) }}</span></p>
                            </div>
                            @if(Auth::guard('practitioner')->check()||Auth::guard('patient')->check())

                                @foreach($comments as $comment)
                                    <div id="comment_{{$comment->id}}">
                                        <div class="post-content-comment grey-section"
                                             data-scroll-reveal="enter bottom move 200px over 1s after 0.3s">
                                            {{--                        <img  src="images/team11.jpg" alt="" />--}}
                                            <h4>{{ $comment->user_name }}</h4>
                                            <p>{{ $comment->content }}</p>
                                            <a class="reply" id="reply_{{$comment->id}}"
                                               onclick="addComment({{$comment->id}})">reply</a>
                                        </div>
                                        {{--                            <form method="get" id="reply_form_{{$comment['id']}}" style="visibility: hidden">--}}
                                        {{--                                {!! csrf_field() !!}--}}
                                        {{--                                <div class='leave-reply grey-section post-content-comment'><h4>LEAVE A REPLY</h4><input name='parent' type='hidden' value={{$comment['id']}}>--}}
                                        {{--                                    <input name='name' type='text'   placeholder='NAME *' required/>--}}
                                        {{--                                    <textarea name='content'  placeholder='COMMENT'></textarea>--}}
                                        {{--                                    <button class='post-comment' value='{{$comment['id']}}' id='send_2'>reply</button>--}}
                                        {{--                                </div>--}}
                                        {{--                            </form>--}}
                                    </div>
                                    @if(count($comment->children))
                                        @foreach($comment->children as $children)
                                            <div class="post-content-comment reply-in grey-section"
                                                 data-scroll-reveal="enter bottom move 200px over 1s after 0.3s"
                                                 id="comment_{{$children['id']}}">
                                                {{--                                    <img  src="images/team22.jpg" alt="" />--}}
                                                <h4>{{ $children['user_name'] }}</h4>
                                                <p>{{ $children['content'] }}</p>
                                                {{--                                    <a href="#"><div class="reply" id="reply_{{$children['id']}}">reply</div></a>--}}
                                            </div>
                                        @endforeach
                                    @endif
                                @endforeach
                            <form action="{{ url('post_comment') }}" method="post" id="comment_form"
                                  style="width: 100%">
                                {!! csrf_field() !!}
                                <div class="leave-reply grey-section"
                                     data-scroll-reveal="enter bottom move 200px over 1s after 0.3s">
                                    <h4>LEAVE A COMMENT</h4>

                                    <input name="article" type="hidden" value="{{ $article->id }}"/>
                                    <input name="parent" type="hidden" value='0'/>
                                    {{--                        <input name="email" type="text"   placeholder="EMAIL *" required/>--}}
                                    {{--                        <input name="website" type="text"   placeholder="website"/>--}}
                                    <textarea name="content" placeholder="COMMENT"></textarea>
                                    <button class="post-comment sendForm" id="send">post comment</button>

                                </div>
                            </form>
                            @else
                                <div class="leave-reply grey-section"
                                     data-scroll-reveal="enter bottom move 200px over 1s after 0.3s">
                                    <h4 style="padding-bottom: unset;">Tell us what you think <a onClick="open_login()"
                                                                                                 style="cursor: pointer;color: #6ba53a;">login</a>
                                        to share your thoughts.</h4>
                                </div>
                            @endif
                        @endif
                    @else
                        <div class="grey-section" style="margin-top: 20px;">
                            <div class="articleClass padding">
                                {{$article->preview_text}}
                            </div>
                            <div style="height: 100px;float: right;">
                                <div style="margin-right: unset;margin-top: 10%;"
                                     class="button-shortcodes text-size-1 text-padding-1 version-1"
                                     onClick="open_login()"><span>&#xf18e;</span> Please login to view
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="four columns" data-scroll-reveal="enter bottom move 200px over 1s after 0.3s">
                    <div class="post-sidebar">
                        {{--                        <input name="search" type="text"   placeholder="type to search and hit enter"/>--}}
                        @if(count($article->product_family))
                            <div class="separator-sidebar"></div>
                            <h4>Related Products</h4>
                            <ul class="link-recents">
                                @foreach($article->product_family as $a)
                                    <li><a href="#">{{ $a['product_name'] }}</a></li>
                                @endforeach
                            </ul>
                        @endif
                        @if(count($article->clinical_trial))
                            <div class="separator-sidebar"></div>
                            <h4>Related Clinical Trials</h4>
                            <ul class="link-recents">
                                @foreach($article->clinical_trial as $a)
                                    <li><a style="color: #6ba53a;word-break: break-word;"
                                           href="https://dev-www.medlab.co/clinical_trials">{{ $a['name'] }}</a></li>
                                @endforeach
                            </ul>
                        @endif
                        @if(count($article->publication))
                            <div class="separator-sidebar"></div>
                            <h4>Related Publications And Presentations</h4>
                            <ul class="link-recents">
                                @foreach($article->publication as $a)
                                    <li><a style="color: #6ba53a;word-break: break-word;"
                                           href="https://dev-www.medlab.co/publications">{{ $a['title'] }}</a></li>
                                @endforeach
                            </ul>
                        @endif
                        @if($article->article_category)
                            <div class="separator-sidebar"></div>
                            <h4>Categories</h4>
                            <ul class="link-recents">
                                @foreach($article->article_category as $a)
                                    <li><a style="color: #6ba53a;word-break: break-word;"
                                           href="https://dev-pharma.medlab.co/education?article_category={{ $a['name'] }}">{{ str_replace('_',' ',$a['name']) }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        @endif
                        {{--                        <div class="separator-sidebar"></div>--}}
                        {{--                        <h4>LATEST video</h4>--}}
                        {{--                        <iframe src="http://player.vimeo.com/video/96696089?title=0&amp;byline=0&amp;portrait=0&amp;color=cfa144" width="940" height="450" ></iframe>--}}
                        {{--                        <div class="separator-sidebar"></div>--}}
                        {{--                        <h4>tags</h4>--}}
                        {{--                        <ul class="link-tag">--}}
                        {{--                            <li><a href="#">Analysis</a></li>--}}
                        {{--                            <li><a href="#">Art</a></li>--}}
                        {{--                            <li><a href="#">Articles</a></li>--}}
                        {{--                            <li><a href="#">Audio</a></li>--}}
                        {{--                            <li><a href="#">Business</a></li>--}}
                        {{--                            <li><a href="#">Culture</a> </li>--}}
                        {{--                            <li><a href="#">Development</a> </li>--}}
                        {{--                            <li><a href="#">Ecology</a></li>--}}
                        {{--                            <li><a href="#">Events</a> </li>--}}
                        {{--                            <li><a href="#">Information</a> </li>--}}
                        {{--                            <li><a href="#">Inspiration</a></li>--}}
                        {{--                            <li><a href="#">Nature</a> </li>--}}
                        {{--                            <li><a href="#">Opportunities</a> </li>--}}
                        {{--                            <li><a href="#">Science</a> </li>--}}
                        {{--                            <li><a href="#">Trends</a> </li>--}}
                        {{--                            <li><a href="#">Video</a></li>--}}
                        {{--                        </ul>--}}
                    </div>
                </div>
            </div>

        </section>


        <!-- SECTION
        ================================================== -->

        @if(count($article->member))
            <div class="container">
                <div class="sixteen columns">
                    <div class="section-title">
                        <h2>ABOUT THE AUTHOR</h2>
                    </div>
                </div>
            @foreach($article->member as $people)
                <section class="section white-section section-padding-top-bottom">


                        <div class="sixteen columns">
                            <div class="blockquotes-box-1 grey-section blockquotes-float-content"
                                 style="max-width: 300px;padding: 10px;border: unset;">
                                <img src="{{ $image_url }}/www/Images/{{ $people['picture'] }}"
                                     alt="{{ $people['alt'] }}">
                            </div>
                            <div class="team-name-top"
                                 style="font-family: 'Playball',crusive;display: inline-block;">{{ $people['position'] }}</div>
                            <h4 style="margin: 20px;font-size: x-large;text-align: unset;margin-left: unset;">{{ $people['name'] }}</h4>
                            {!! $people['bio'] !!}
                        </div>
                </section>
            @endforeach
            </div>
        @endif
        @if(count($article->related_article))
            <section class="section grey-section section-padding-top-bottom">
                <div class="container">
                    <div class="sixteen columns">
                        <div class="section-title">
                            <h2>MORE BY THIS AUTHOR</h2>
                        </div>
                    </div>

                    @foreach($article->related_article as $item)
                        <div class="one-third column" data-scroll-reveal="enter bottom move 200px over 1s after 0.3s">
                            {{--                    <a href={{ route('pharma.articles_content',['id' => $item->id]) }} class="animsition-link">--}}
                            {{--                        <div class="blog-box-1 white-section">--}}
                            {{--                            <img src="{{ $image_url }}/www/Images/{{ $item->title_img }}"--}}
                            {{--                                 alt="{{ $item->title_alt }}">--}}
                            {{--                            <div class="blog-date-1">{{ substr($item->date,0,10) }}</div>--}}
                            {{--                            <div class="blog-comm-1">3 <span>&#xf086;</span></div>--}}
                            {{--                            <h4>{{ $item->title }}</h4>--}}
                            {{--                            <p>{{ $item->preview_text }}</p>--}}
                            {{--                            <div class="link">&#xf178;</div>--}}
                            {{--                        </div>--}}
                            {{--                    </a>--}}
                            @if($item->article_type=='link')
                                <a href={{ route('pharma.articles_content',['id' => $item->id]) }} class="animsition-link">
                                    <div class="blog-box-1 link-post">
                                        <h4>{{ $item->title }}</h4>
                                        <p>{{ $item->preview_text }}</p>
                                        <p>{{ substr($item->date,0,10) }}</p>
                                        @if(isset($item->author))<p>Author: {{ $item->author }}</p>@endif
                                        <div class="link">&#xf178;</div>
                                    </div>
                                </a>
                            @elseif($item->article_type=='video')
                                <a href={{ route('pharma.articles_content',['id' => $item->id]) }}class="animsition-link">
                                    <div class="blog-box-1 white-section">
                                        @if(($item->req_login && !(Auth::guard('practitioner')->check()||Auth::guard('patient')->check())))
                                            <div style="opacity: 0.85;background: black;">
                                                <iframe srcdoc="<p style='text-align:center;margin-top:15%;font-size:xx-large;color:white'>Please login to view this video</p>" src="{{ $item->video_link }}" width="100" height="56"
                                                        allowfullscreen></iframe>
                                            </div>
                                        @else
                                            <iframe src="{{ $item->video_link }}" width="100" height="56"
                                                    allowfullscreen></iframe>
                                        @endif
                                        {{--                                                <img src="{{ $image_url }}/www/Images/{{ $item->title_img }}"--}}
                                        {{--                                                     alt="{{ $item->title_alt }}">--}}
                                        <h4>{{ $item->title }}</h4>
                                        <p>{{ $item->preview_text }}</p>
                                        <p>{{ substr($item->date,0,10) }}</p>
                                        @if(isset($item->author))<p>Author: {{ $item->author }}</p>@endif
                                        <div class="link">&#xf178;</div>
                                    </div>
                                </a>
                            @else
                                <a href={{ route('pharma.articles_content',['id' => $item->id]) }} class="animsition-link">
                                    <div class="blog-box-1 white-section">
                                        <img src="{{ $image_url }}/www/Images/{{ $item->title_img }}"
                                             alt="{{ $item->title_alt }}">
                                        <h4>{{ $item->title }}</h4>
                                        <p>{{ $item->preview_text }}</p>
                                        <p>{{ substr($item->date,0,10) }}</p>
                                        @if(isset($item->author))<p>Author: {{ $item->author }}</p>@endif
                                        <div class="link">&#xf178;</div>
                                    </div>
                                </a>
                            @endif
                        </div>
                    @endforeach
                </div>
            </section>
        @endif


        @include('pharma.layouts.footer')


    </main>

    <div class="scroll-to-top">&#xf106;</div>





    <!-- JAVASCRIPT
    ================================================== -->
    <script type="text/javascript" src="{{ asset('/js/jquery-2.1.1.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/modernizr.custom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.mobile.custom.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/retina-1.1.0.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.animsition.min.js') }}"></script>
    <script type="text/javascript">
        (function ($) {

            "use strict";
            $(document).ready(function () {
                @if (session('login_error'))
                open_login();
                @endif
                $(".animsition").animsition({

                    inClass: 'zoom-in-sm',
                    outClass: 'zoom-out-sm',
                    inDuration: 1500,
                    outDuration: 800,
                    linkElement: '.animsition-link',
                    // e.g. linkElement   :   'a:not([target="_blank"]):not([href^=#])'
                    loading: true,
                    loadingParentElement: 'body', //animsition wrapper element
                    loadingClass: 'animsition-loading',
                    unSupportCss: ['animation-duration',
                        '-webkit-animation-duration',
                        '-o-animation-duration'
                    ],
                    //"unSupportCss" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
                    //The default setting is to disable the "animsition" in a browser that does not support "animation-duration".

                    overlay: false,

                    overlayClass: 'animsition-overlay-slide',
                    overlayParentElement: 'body'
                });
            });
        })(jQuery);
    </script>
    <script type="text/javascript" src="{{ asset('/js/jquery.easing.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.hidescroll.min.js') }}"></script>
    <script type="text/javascript">
        $('.header-top').hidescroll();
    </script>
    <script type="text/javascript" src="{{ asset('/js/smoothScroll.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.counterup.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/waypoints.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/scrollReveal.js') }}"></script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            window.scrollReveal = new scrollReveal();
        })(jQuery);
    </script>
    <script type="text/javascript" src="{{ asset('/js/jquery.colorbox.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/owl.carousel.min.js') }}"></script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            jQuery(document).ready(function () {
                var offset = 450;
                var duration = 500;
                jQuery(window).scroll(function () {
                    if (jQuery(this).scrollTop() > offset) {
                        jQuery('.scroll-to-top').fadeIn(duration);
                    } else {
                        jQuery('.scroll-to-top').fadeOut(duration);
                    }
                });

                jQuery('.scroll-to-top').click(function (event) {
                    event.preventDefault();
                    jQuery('html, body').animate({scrollTop: 0}, duration);
                    return false;
                })
            });
        })(jQuery);
        var form = "<form class='form'></form>";

        function addComment(id) {
            console.log(document.getElementById('reply_' + id).innerText);
            if (document.getElementById('reply_' + id).innerText == 'CANCEL') {
                document.getElementById('comment_form_' + id).remove();
                document.getElementById('reply_' + id).innerText = 'reply';
            } else {
                document.getElementById('reply_' + id).innerText = 'cancel';
                $('#comment_' + id).append("<form action='{{ url('post_comment')}}' method='post' id='comment_form_" + id + "' style='width: 100%' target='_blank'>" + "<input type='hidden' name='_token' value=" + document.getElementsByName('_token')[0].value + ">" + "<div class='leave-reply grey-section post-content-comment'><input name='parent' type='hidden' value=" + id + "><textarea name='content'  placeholder='COMMENT'></textarea><button class='post-comment sendForm' id="+id+">reply</button></div></form>");
            }
        }
        $(document).on("click", ".sendForm", function () {
            var fileId = $(this).attr("id");
            console.log(fileId)
            if(fileId=='send'){
                console.log('basic work')
                var data_string = $('#comment_form').serialize();
                $.ajax({
                    type: "POST",
                    url: $('#comment_form').attr('action'),
                    data: data_string,
                    error: function (data) {
                        console.log(data);

                    },
                    success: function (data) {
                        console.log(data);
                        location.reload();

                    }
                });
                return false;
            }
            else{
                console.log('new work')
                var id = $(this).attr("id");
                console.log(id)
                var data_string = $('#comment_form_'+id).serialize();
                console.log(data_string)
                $.ajax({
                    type: "POST",
                    url: $('#comment_form_'+id).attr('action'),
                    data: data_string,
                    error: function (data) {
                        console.log(data);

                    },
                    success: function (data) {
                        console.log(data);
                        location.reload();

                    }
                });

                return false;
            }
        });
    </script>
    <script type="text/javascript" src="{{ asset('/js/jquery.fitvids.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/styleswitcher.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/custom-video-project.js') }}"></script>

    <iframe src=”https://player.vimeo.com/video/894794478?api=1&player_id=894794478″ id=”player_id″
            width=”640″ height=”390″ frameborder=”0″ data-progress=”true” data-seek=”true”
            data-bounce=”false” webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>

    <script src=”/js/jquery-1.12.1.min.js”></script>

    <script src=”/js/vimeo.ga.min.js”></script>

    <!-- End Document
    ================================================== -->
@endsection
