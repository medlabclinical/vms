﻿@extends('pharma.layouts.main')
@section('breadcrumbs')
<div class="medlab_breadcrumbs_wrapper">
	<div class="container" style="width: unset;background-color:#7AA43F;">
		<ol class="breadcrumb medlab_breadcrumbs" style="margin-bottom: unset;background-color:#7AA43F;">
		</ol>
	</div>
</div>

<div class="medlab_breadcrumbs_wrapper">
	<div class="container" style="width: unset">
		<ol class="breadcrumb medlab_breadcrumbs">
			<li><a class="medlab_breadcrumbs_link" href="/">@lang('trs.home')</a></li>
			<li class="active medlab_breadcrumbs_text">@lang('trs.shipping and delivery')</li>
		</ol>
	</div>
</div>
@endsection
@section('content')




	<main class="cd-main-content" id="main" style="margin-top:90px">

		<!-- TOP SECTION
    ================================================== -->

        <section class="home">
            <div class="slider-container">
                <div class="tp-banner-container">
                    <div class="tp-banner">
                        <ul>
                            <li data-transition="fade" data-slotamount="1" data-masterspeed="500"
                                data-saveperformance="on" data-title="Intro Slide">
                                <img src="{{ $image_url }}/www/Images/{{ $content->title_img }}" alt="{{ $content->title_alt }}"
                                     data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                            </li>
                            @if(isset($content->second_img))
                                <li data-transition="fade" data-slotamount="1" data-masterspeed="500"
                                    data-saveperformance="on" data-title="Intro Slide">
                                    <img src="{{ $image_url }}/www/Images/{{ $content->second_img }}" alt="{{ $content->second_alt }}"
                                         data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                                </li>
                            @endif
                            @if(isset($content->third_img))
                                <li data-transition="fade" data-slotamount="1" data-masterspeed="500"
                                    data-saveperformance="on" data-title="Intro Slide">
                                    <img src="{{ $image_url }}/www/Images/{{ $content->third_img }}" alt="{{ $content->third_alt }}"
                                         data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                                </li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <section class="section grey-section section-home-padding-top" style="padding-top: 10px;">

            <div class="container">
                <div class="sixteen columns">
                    <div class="section-title left">
                        <h1>{{ $content->title }}</h1>
                        @if(isset($content->subtitle))
                            <div class="subtitle big left">{{ $content->subtitle }}</div>
                        @endif
                    </div>
                </div>
            </div>

        </section>
        <section class="section white-section section-padding-top" id="scroll-link">
            <div class="container">
                <div class="sixteen columns remove-bottom"
                     >
                    <div class="full-image">
                        <div class="articleClass padding">
                            {{--                            111--}}
                            {!! $content->content !!}
                        </div>
                    </div>
                </div>
            </div>
        </section>



        @include('pharma.layouts.footer')




	</main>

	<div class="scroll-to-top">&#xf106;</div>





	<!-- JAVASCRIPT
    ================================================== -->
    <script type="text/javascript" src="{{ asset('/js/jquery-2.1.1.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/modernizr.custom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.mobile.custom.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/retina-1.1.0.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.animsition.min.js') }}"></script>
    <script type="text/javascript">
        (function($) { "use strict";
            $(document).ready(function() {
                @if (session('login_error'))
                open_login();
                @endif
                $(".animsition").animsition({

                    inClass               :   'zoom-in-sm',
                    outClass              :   'zoom-out-sm',
                    inDuration            :    1500,
                    outDuration           :    800,
                    linkElement           :   '.animsition-link',
                    // e.g. linkElement   :   'a:not([target="_blank"]):not([href^=#])'
                    loading               :    true,
                    loadingParentElement  :   'body', //animsition wrapper element
                    loadingClass          :   'animsition-loading',
                    unSupportCss          : [ 'animation-duration',
                        '-webkit-animation-duration',
                        '-o-animation-duration'
                    ],
                    //"unSupportCss" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
                    //The default setting is to disable the "animsition" in a browser that does not support "animation-duration".

                    overlay               :   false,

                    overlayClass          :   'animsition-overlay-slide',
                    overlayParentElement  :   'body'
                });
            });
        })(jQuery);
    </script>
    <script type="text/javascript" src="{{ asset('/js/jquery.themepunch.tools.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.themepunch.revolution.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.easing.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.hidescroll.min.js') }}"></script>
    <script type="text/javascript">
        $('.header-top').hidescroll();
    </script>
    <script type="text/javascript" src="{{ asset('/js/smoothScroll.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/isotope.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.counterup.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/waypoints.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/scrollReveal.js') }}"></script>
    <script type="text/javascript">
        (function($) { "use strict";
            window.scrollReveal = new scrollReveal();
        })(jQuery);
    </script>
    <script type="text/javascript" src="{{ asset('/js/jquery.colorbox.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/owl.carousel.min.js') }}"></script>
    <script type="text/javascript">
        (function($) { "use strict";
            jQuery(document).ready(function() {
                var offset = 450;
                var duration = 500;
                jQuery(window).scroll(function() {
                    if (jQuery(this).scrollTop() > offset) {
                        jQuery('.scroll-to-top').fadeIn(duration);
                    } else {
                        jQuery('.scroll-to-top').fadeOut(duration);
                    }
                });

                jQuery('.scroll-to-top').click(function(event) {
                    event.preventDefault();
                    jQuery('html, body').animate({scrollTop: 0}, duration);
                    return false;
                })
            });
        })(jQuery);
    </script>
    <script type="text/javascript" src="{{ asset('/js/styleswitcher.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/custom-shop-home-1.js') }}"></script>
	<!-- End Document
================================================== -->
	@endsection
