﻿@extends('pharma.layouts.main')

@section('content')

	<!-- HOME SECTION
    ================================================== -->

    <style>
        @media only screen and (max-width: 1419px){
            .home{
                margin-top: 50px;
            }
        }
        @media only screen and (min-width: 1420px){
            .home{
                margin-top: 80px;
            }
        }
    </style>
	<main class="cd-main-content" id="main">
        <section class="home">

            <div class="slider-container">
                <div class="tp-banner-container">
                    <div class="tp-banner" >
                        <ul>
                            <li data-transition="fade" data-slotamount="1" data-masterspeed="500"  data-saveperformance="on"  data-title="Intro Slide">
                                <img src="{{ $image_url }}/www/Images/{{ $content->title_img }}" alt="{{ $content->title_alt }}" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat"/>

                                <!-- THE CAPTIONS IN THIS SLIDE -->
                                {{--								<div class="tp-caption tp-fade fadeout fullscreenvideo"--}}
                                {{--									 data-x="0"--}}
                                {{--									 data-y="0"--}}
                                {{--									 data-speed="1000"--}}
                                {{--									 data-start="1100"--}}
                                {{--									 data-easing="Power4.easeOut"--}}
                                {{--									 data-elementdelay="0.01"--}}
                                {{--									 data-endelementdelay="0.1"--}}
                                {{--									 data-endspeed="1500"--}}
                                {{--									 data-endeasing="Power4.easeIn"--}}
                                {{--									 data-autoplay="true"--}}
                                {{--									 data-autoplayonlyfirsttime="false"--}}
                                {{--									 data-nextslideatend="true"--}}
                                {{--									 data-volume="mute" data-forceCover="1"--}}
                                {{--									 data-aspectratio="16:9" data-forcerewind="on"--}}
                                {{--									 style="z-index: 2;">--}}
                                {{--									<video class="" preload="none">--}}
                                {{--										<source src="{{ $image_url }}/www/Images/{{ $content->video }}" type="video/mp4">--}}
                                {{--                                        src="{{ $image_url }}/www/Images/{{ $content->video }}"--}}
                                {{--										<source src="{{ asset('/videos/video.webm') }}" type="video/webm">--}}
                                {{--										<source src="{{ asset('/videos/video.mp4') }}" type="video/mp4">--}}
                                {{--									</video>--}}
                                {{--								</div>--}}

                                <div class="tp-caption black-heavy randomrotate tp-resizeme"
                                     data-speed="200"
                                     data-start="300"
                                     data-easing="Power3.easeInOut"
                                     data-elementdelay="0.1"
                                     data-endelementdelay="0.1"
                                     style="z-index: 500; max-width: auto; max-height: auto; white-space: nowrap;">
                                    <span class="border-text">{{ $content->title }}</span>
                                </div>
                                <div class="tp-caption color-small randomrotate tp-resizeme"
                                     data-speed="200"
                                     data-start="300"
                                     data-easing="Power3.easeInOut"
                                     data-splitin="chars"
                                     data-splitout="none"
                                     data-elementdelay="0.1"
                                     data-endelementdelay="0.1"
                                     style="z-index: 500; max-width: auto; max-height: auto; white-space: nowrap;">
                                    {{ $content->subtitle }}
                                </div>

                            </li>
                            @if(isset($content->second_img))
                                <li data-transition="fade" data-slotamount="1" data-masterspeed="500"  data-saveperformance="on"  data-title="Intro Slide">

                                    <img src="{{ $image_url }}/www/Images/{{ $content->second_img }}"  alt="{{ $content->second_alt }}" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">

                                    <div class="tp-caption black-heavy randomrotate tp-resizeme"
                                         data-speed="200"
                                         data-start="300"
                                         data-easing="Power3.easeInOut"
                                         data-elementdelay="0.1"
                                         data-endelementdelay="0.1"
                                         style="z-index: 500; max-width: auto; max-height: auto; white-space: nowrap;">
                                        <span class="border-text">{{ $content->title2 }}</span>
                                    </div>
                                    <div class="tp-caption color-small randomrotate tp-resizeme"
                                         data-speed="200"
                                         data-start="300"
                                         data-easing="Power3.easeInOut"
                                         data-splitin="chars"
                                         data-splitout="none"
                                         data-elementdelay="0.1"
                                         data-endelementdelay="0.1"
                                         style="z-index: 500; max-width: auto; max-height: auto; white-space: nowrap;">
                                        {{ $content->subtitle2 }}
                                    </div>

                                </li>
                            @endif
                            @if(isset($content->third_img))
                                <li data-transition="fade" data-slotamount="1" data-masterspeed="500"  data-saveperformance="on"  data-title="Intro Slide">

                                    <img src="{{ $image_url }}/www/Images/{{ $content->third_img }}"  alt="{{ $content->third_alt }}" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">

                                                                        <div class="tp-caption black-heavy randomrotate tp-resizeme"
                                                                             data-speed="200"
                                                                             data-start="300"
                                                                             data-easing="Power3.easeInOut"
                                                                             data-elementdelay="0.1"
                                                                             data-endelementdelay="0.1"
                                                                             style="z-index: 500; max-width: auto; max-height: auto; white-space: nowrap;">
                                                                            <span class="border-text">{{ $content->title3 }}</span>
                                                                        </div>
                                                                        <div class="tp-caption color-small randomrotate tp-resizeme"
                                                                             data-speed="200"
                                                                             data-start="300"
                                                                             data-easing="Power3.easeInOut"
                                                                             data-splitin="chars"
                                                                             data-splitout="none"
                                                                             data-elementdelay="0.1"
                                                                             data-endelementdelay="0.1"
                                                                             style="z-index: 500; max-width: auto; max-height: auto; white-space: nowrap;">
                                                                            {{ $content->subtitle3 }}
                                                                        </div>

                                </li>
                            @endif

                        </ul>
                    </div>
                </div>
            </div>

        </section>

	<!-- SECTION
    ================================================== -->

        @if($content->content)
		<section class="section white-section section-padding-top" id="scroll-link">
			<div class="container">
				<div class="sixteen columns remove-bottom">
					<div class="full-image">
                        <div class="articleClass padding">
                            {!! $content->content !!}
                        </div>
					</div>
				</div>
			</div>
		</section>
        @endif

{{--        <section class="section grey-section section-padding-top-bottom">--}}
{{--            <div class="container">--}}
{{--                <div class="one-third column" data-scroll-reveal="enter left move 200px over 1s after 0.3s">--}}
{{--                    <div class="services-boxes-1">--}}
{{--                        <div class="icon-box">&#xf085;</div>--}}
{{--                        <h6>FULLY CUSTOMIZABLE</h6>--}}
{{--                        <p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="one-third column" data-scroll-reveal="enter bottom move 200px over 1s after 0.3s">--}}
{{--                    <div class="services-boxes-1">--}}
{{--                        <div class="icon-box">&#xf121;</div>--}}
{{--                        <h6>UNLIMITED OPTIONS</h6>--}}
{{--                        <p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="one-third column" data-scroll-reveal="enter right move 200px over 1s after 0.3s">--}}
{{--                    <div class="services-boxes-1">--}}
{{--                        <div class="icon-box">&#xf06e;</div>--}}
{{--                        <h6>RETINA READY</h6>--}}
{{--                        <p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </section>--}}


{{--        <section class="section grey-section section-padding-top-bottom">--}}
{{--            <div class="container">--}}
{{--                <div class="sixteen columns">--}}
{{--                    <div class="section-title">--}}
{{--                        <h3>RECENT EDUCATION ARTICLES</h3>--}}
{{--                        <div class="subtitle">WE ARE <span class="subtitle-written">Medlab</span></div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                @foreach($articles as $item)--}}
{{--                    @if($item->article_type == 'video')--}}
{{--                <div class="eight columns" data-scroll-reveal="enter left move 200px over 1s after 0.3s">--}}
{{--                    <a href={{ route('www.articles_content',['id' => preg_replace('/--+/','-',preg_replace('/[^A-Za-z0-9\-]/', '-', $item->title)).'-'.$item->id]) }} class="animsition-link">--}}
{{--                        <div class="portfolio-box-2 grey-section">--}}
{{--                            <iframe src="{{ $item->video_link }}" width="100%" height="305"></iframe>--}}
{{--                            <h6>{{ $item->title }}</h6>--}}
{{--                            <p>{{ $item->subtitle }}</p>--}}
{{--                        </div>--}}
{{--                    </a>--}}
{{--                </div>--}}
{{--                    @else--}}
{{--                        <div class="eight columns" data-scroll-reveal="enter left move 200px over 1s after 0.3s">--}}
{{--                            <a href={{ route('www.articles_content',['id' => preg_replace('/--+/','-',preg_replace('/[^A-Za-z0-9\-]/', '-', $item->title)).'-'.$item->id]) }} class="animsition-link">--}}
{{--                                <div class="portfolio-box-2 grey-section">--}}
{{--                                    <img src="{{ $image_url }}/www/Images/{{ $item->title_img }}" alt="{{ $item->title_alt }}">--}}
{{--                                    <h6>{{ $item->title }}</h6>--}}
{{--                                    <p>{{ $item->subtitle }}</p>--}}
{{--                                </div>--}}
{{--                            </a>--}}
{{--                        </div>--}}
{{--                        @endif--}}
{{--                @endforeach--}}
{{--                <div class="eight columns" data-scroll-reveal="enter right move 200px over 1s after 0.3s">--}}
{{--                    <a href="https://pharmaceuticals.medlab.co" class="animsition-link">--}}
{{--                        <div class="portfolio-box-2 grey-section">--}}
{{--                            <img src="images/portfolio/4.jpg" alt="">--}}
{{--                            <h6>pharmaceuticals</h6>--}}
{{--                            <p>Introduction</p>--}}
{{--                        </div>--}}
{{--                    </a>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </section>--}}

	<!-- SECTION
    ================================================== -->


	<!-- SECTION
    ================================================== -->

{{--		<section class="section white-section section-padding-top-bottom">--}}
{{--			<div class="container">--}}
{{--				<div class="sixteen columns">--}}
{{--					<div class="section-title">--}}
{{--						<h3>QODE PROCESS</h3>--}}
{{--						<div class="subtitle">Carefully <span class="subtitle-written">crafted elements</span> come together into one amazing design.</div>--}}
{{--					</div>--}}
{{--				</div>--}}
{{--				<div class="sixteen columns">--}}
{{--					<div id="cd-timeline" class="cd-container">--}}
{{--						<div class="cd-timeline-block">--}}
{{--							<div class="cd-timeline-img">&#xf0eb;</div> <!-- cd-timeline-img -->--}}

{{--							<div class="cd-timeline-content">--}}
{{--								<h6>IDEA</h6>--}}
{{--								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, optio, dolorum provident rerum.</p>--}}
{{--								<a href="/" class="cd-read-more">Read more</a>--}}
{{--								<span class="cd-date">20%</span>--}}
{{--							</div> <!-- cd-timeline-content -->--}}
{{--						</div> <!-- cd-timeline-block -->--}}

{{--						<div class="cd-timeline-block">--}}
{{--							<div class="cd-timeline-img">&#xf200;</div> <!-- cd-timeline-img -->--}}

{{--							<div class="cd-timeline-content">--}}
{{--								<h6>CONCEPT</h6>--}}
{{--								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, optio, dolorum provident rerum.</p>--}}
{{--								<a href="/" class="cd-read-more">Read more</a>--}}
{{--								<span class="cd-date">40%</span>--}}
{{--							</div> <!-- cd-timeline-content -->--}}
{{--						</div> <!-- cd-timeline-block -->--}}

{{--						<div class="cd-timeline-block">--}}
{{--							<div class="cd-timeline-img">&#xf0d0;</div> <!-- cd-timeline-img -->--}}

{{--							<div class="cd-timeline-content">--}}
{{--								<h6>DESIGN</h6>--}}
{{--								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, optio, dolorum provident rerum.</p>--}}
{{--								<a href="/" class="cd-read-more">Read more</a>--}}
{{--								<span class="cd-date">60%</span>--}}
{{--							</div> <!-- cd-timeline-content -->--}}
{{--						</div> <!-- cd-timeline-block -->--}}

{{--						<div class="cd-timeline-block">--}}
{{--							<div class="cd-timeline-img">&#xf085;</div> <!-- cd-timeline-img -->--}}

{{--							<div class="cd-timeline-content">--}}
{{--								<h6>DEVELOP</h6>--}}
{{--								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, optio, dolorum provident rerum.</p>--}}
{{--								<a href="/" class="cd-read-more">Read more</a>--}}
{{--								<span class="cd-date">80%</span>--}}
{{--							</div> <!-- cd-timeline-content -->--}}
{{--						</div> <!-- cd-timeline-block -->--}}

{{--						<div class="cd-timeline-block">--}}
{{--							<div class="cd-timeline-img">&#xf201;</div> <!-- cd-timeline-img -->--}}

{{--							<div class="cd-timeline-content">--}}
{{--								<h6>TEST</h6>--}}
{{--								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, optio, dolorum provident rerum.</p>--}}
{{--								<a href="/" class="cd-read-more">Read more</a>--}}
{{--								<span class="cd-date">100%</span>--}}
{{--							</div> <!-- cd-timeline-content -->--}}
{{--						</div> <!-- cd-timeline-block -->--}}
{{--					</div>--}}
{{--				</div>--}}
{{--			</div>--}}
{{--		</section>--}}

{{--	<!-- SECTION--}}
{{--    ================================================== -->--}}

{{--		<section class="section parallax-section parallax-section-padding-top-bottom">--}}

{{--			<div class="parallax-1"></div>--}}

{{--			<div class="container">--}}
{{--				<div class="container">--}}
{{--					<div class="four columns">--}}
{{--						<div class="facts-box-1">--}}
{{--							<div class="facts-box-1-num"><span class="counter">82</span><div class="line"></div></div>--}}
{{--							<h6>FINISHED PROJECTS</h6>--}}
{{--						</div>--}}
{{--					</div>--}}
{{--					<div class="four columns">--}}
{{--						<div class="facts-box-1">--}}
{{--							<div class="facts-box-1-num"><span class="counter">8723</span><div class="line"></div></div>--}}
{{--							<h6>LINES OF CODE</h6>--}}
{{--						</div>--}}
{{--					</div>--}}
{{--					<div class="four columns">--}}
{{--						<div class="facts-box-1">--}}
{{--							<div class="facts-box-1-num"><span class="counter">513</span><div class="line"></div></div>--}}
{{--							<h6>CUPS OF COFFEE</h6>--}}
{{--						</div>--}}
{{--					</div>--}}
{{--					<div class="four columns">--}}
{{--						<div class="facts-box-1">--}}
{{--							<div class="facts-box-1-num"><span class="counter">3419</span><div class="line"></div></div>--}}
{{--							<h6>CUSTOM COUNTERS</h6>--}}
{{--						</div>--}}
{{--					</div>--}}
{{--				</div>--}}
{{--			</div>--}}
{{--		</section>--}}

{{--	<!-- SECTION--}}
{{--    ================================================== -->--}}

{{--		<section class="section white-section section-padding-top-bottom">--}}
{{--			<div class="container">--}}
{{--				<div class="five columns">--}}
{{--					<div class="cd-product cd-container">--}}
{{--						<div class="cd-product-wrapper">--}}
{{--							<img src="{{ asset('/images/iphone.png') }}" alt="Preview image">--}}
{{--							<ul>--}}
{{--								<li class="cd-single-point">--}}
{{--									<a class="cd-img-replace" href="/">More</a>--}}
{{--									<div class="cd-more-info cd-top">--}}
{{--										<h6>Title</h6>--}}
{{--										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae distinctio esse placeat minus fugit, voluptate, quos, ea, nisi temporibus repudiandae perspiciatis natus.</p>--}}
{{--										<a href="/" class="cd-close-info cd-img-replace">Close</a>--}}
{{--									</div>--}}
{{--								</li> <!-- .cd-single-point -->--}}

{{--								<li class="cd-single-point">--}}
{{--									<a class="cd-img-replace" href="/">More</a>--}}
{{--									<div class="cd-more-info cd-top">--}}
{{--										<h6>Title</h6>--}}
{{--										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae distinctio esse placeat minus fugit, voluptate, quos, ea, nisi temporibus repudiandae perspiciatis natus.</p>--}}
{{--										<a href="/" class="cd-close-info cd-img-replace">Close</a>--}}
{{--									</div>--}}
{{--								</li> <!-- .cd-single-point -->--}}

{{--								<li class="cd-single-point">--}}
{{--									<a class="cd-img-replace" href="/">More</a>--}}
{{--									<div class="cd-more-info cd-right">--}}
{{--										<h6>Title</h6>--}}
{{--										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae distinctio esse placeat minus fugit, voluptate, quos, ea, nisi temporibus repudiandae perspiciatis natus.</p>--}}
{{--										<a href="/" class="cd-close-info cd-img-replace">Close</a>--}}
{{--									</div>--}}
{{--								</li> <!-- .cd-single-point -->--}}

{{--								<li class="cd-single-point">--}}
{{--									<a class="cd-img-replace" href="/">More</a>--}}
{{--									<div class="cd-more-info cd-left">--}}
{{--										<h6>Title</h6>--}}
{{--										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae distinctio esse placeat minus fugit, voluptate, quos, ea, nisi temporibus repudiandae perspiciatis natus.</p>--}}
{{--										<a href="/" class="cd-close-info cd-img-replace">Close</a>--}}
{{--									</div>--}}
{{--								</li> <!-- .cd-single-point -->--}}
{{--							</ul>--}}
{{--						</div> <!-- .cd-product-wrapper -->--}}
{{--					</div> <!-- .cd-product -->--}}
{{--				</div>--}}
{{--				<div class="eleven columns">--}}
{{--					<div class="section-title left">--}}
{{--						<h3>Points of Interest</h3>--}}
{{--						<div class="subtitle left">Use this to <span class="subtitle-written">highlight the points of interest</span> of your products. Just a click to open a brief description of each point.</div>--}}
{{--					</div>--}}
{{--					<div class="services-boxes-1 interest-padding" data-scroll-reveal="enter right move 200px over 1s after 0.3s">--}}
{{--						<div class="icon-box">&#xf10a;</div>--}}
{{--						<h6>100% RESPONSIVE</h6>--}}
{{--						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>--}}
{{--					</div>--}}
{{--					<div class="services-boxes-1 interest-padding" data-scroll-reveal="enter right move 200px over 1s after 0.3s">--}}
{{--						<div class="icon-box">&#xf06e;</div>--}}
{{--						<h6>RETINA READY</h6>--}}
{{--						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>--}}
{{--					</div>--}}
{{--					<div class="services-boxes-1 interest-padding" data-scroll-reveal="enter right move 200px over 1s after 0.3s">--}}
{{--						<div class="icon-box">&#xf121;</div>--}}
{{--						<h6>INTERACTIVE ELEMENTS</h6>--}}
{{--						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>--}}
{{--					</div>--}}
{{--					<div class="services-boxes-1 interest-padding" data-scroll-reveal="enter right move 200px over 1s after 0.3s">--}}
{{--						<div class="icon-box">&#xf121;</div>--}}
{{--						<h6>UNLIMITED OPTIONS</h6>--}}
{{--						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>--}}
{{--					</div>--}}
{{--				</div>--}}
{{--			</div>--}}
{{--		</section>--}}

{{--	<!-- SECTION--}}
{{--    ================================================== -->--}}

{{--		<section class="section grey-section section-padding-top">--}}
{{--			<div class="container">--}}
{{--				<div class="sixteen columns">--}}
{{--					<div class="section-title">--}}
{{--						<h3>OUR LATEST WORKS</h3>--}}
{{--						<div class="subtitle">WE ARE <span class="subtitle-written">Medlab</span> CREATIVE AGENCY.</div>--}}
{{--					</div>--}}
{{--				</div>--}}
{{--			</div>--}}
{{--			<div class="portfolio-wrap-1">--}}
{{--				<a href="slider-project.html" class="animsition-link">--}}
{{--					<div class="portfolio-box-1">--}}
{{--						<div class="mask-1"></div>--}}
{{--						<img src="{{ asset('/images/portfolio/1.jpg') }}" alt="">--}}
{{--						<h6>holy sadie</h6>--}}
{{--					</div>--}}
{{--				</a>--}}
{{--				<a href="video-project.html" class="animsition-link">--}}
{{--					<div class="portfolio-box-1">--}}
{{--						<div class="mask-1"></div>--}}
{{--						<img src="{{ asset('/images/portfolio/2.jpg') }}" alt="">--}}
{{--						<h6>dreamy honey</h6>--}}
{{--					</div>--}}
{{--				</a>--}}
{{--				<a href="gallery-project.html" class="animsition-link">--}}
{{--					<div class="portfolio-box-1">--}}
{{--						<div class="mask-1"></div>--}}
{{--						<img src="{{ asset('/images/portfolio/3.jpg') }}" alt="">--}}
{{--						<h6>crazy layla</h6>--}}
{{--					</div>--}}
{{--				</a>--}}
{{--				<a href="parallax-project.html" class="animsition-link">--}}
{{--					<div class="portfolio-box-1">--}}
{{--						<div class="mask-1"></div>--}}
{{--						<img src="{{ asset('/images/portfolio/4.jpg') }}" alt="">--}}
{{--						<h6>creative zoe</h6>--}}
{{--					</div>--}}
{{--				</a>--}}
{{--			</div>--}}
{{--			<div class="call-to-action-1">--}}
{{--				<div class="action-top-1">over 80 finished projects</div>--}}
{{--				<h5>ALL WORKS</h5>--}}
{{--				<a href="grid-2col.html" class="button-1 animsition-link">Discover</a>--}}
{{--			</div>--}}
{{--		</section>--}}

{{--	<!-- SECTION--}}
{{--    ================================================== -->--}}

{{--		<section class="section white-section section-padding-top-bottom">--}}
{{--			<div class="container">--}}
{{--				<div class="sixteen columns">--}}
{{--					<div class="section-title">--}}
{{--						<h3>WHAT OUR CLIENTS SAY</h3>--}}
{{--					</div>--}}
{{--				</div>--}}
{{--				<div class="sixteen columns">--}}
{{--					<div id="owl-blockquotes" class="owl-carousel owl-theme">--}}
{{--						<div class="item blockquotes-1">--}}
{{--							<div class="arrow-right"></div>--}}
{{--							<img src="{{ asset('/images/team11.jpg') }}" alt="">--}}
{{--							<h6>JAMES BEAN</h6>--}}
{{--							<p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. A small river named Duden flows by their place and supplies it with the necessary regelialia. Lorem ipsum dolor sit amet, consectetur adipisicing elit."</p>--}}
{{--							<div class="company-name">CEO, CompanyName</div>--}}
{{--						</div>--}}
{{--						<div class="item blockquotes-1">--}}
{{--							<div class="arrow-right"></div>--}}
{{--							<img src="{{ asset('/images/team22.jpg') }}" alt="">--}}
{{--							<h6>ISABELLA MORO</h6>--}}
{{--							<p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. A small river named Duden flows by their place and supplies it with the necessary regelialia. Lorem ipsum dolor sit amet, consectetur adipisicing elit."</p>--}}
{{--							<div class="company-name">CEO, CompanyName</div>--}}
{{--						</div>--}}
{{--						<div class="item blockquotes-1">--}}
{{--							<div class="arrow-right"></div>--}}
{{--							<img src="{{ asset('/images/team33.jpg') }}" alt="">--}}
{{--							<h6>DAVID IVE</h6>--}}
{{--							<p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. A small river named Duden flows by their place and supplies it with the necessary regelialia. Lorem ipsum dolor sit amet, consectetur adipisicing elit."</p>--}}
{{--							<div class="company-name">CEO, CompanyName</div>--}}
{{--						</div>--}}
{{--						<div class="item blockquotes-1">--}}
{{--							<div class="arrow-right"></div>--}}
{{--							<img src="{{ asset('/images/team44.jpg') }}" alt="">--}}
{{--							<h6>MARIA BEAN</h6>--}}
{{--							<p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. A small river named Duden flows by their place and supplies it with the necessary regelialia. Lorem ipsum dolor sit amet, consectetur adipisicing elit."</p>--}}
{{--							<div class="company-name">CEO, CompanyName</div>--}}
{{--						</div>--}}
{{--					</div>--}}
{{--				</div>--}}
{{--			</div>--}}
{{--		</section>--}}

	<!-- SECTION
    ================================================== -->

{{--		<section class="section grey-section section-padding-top-bottom">--}}
{{--			<div class="container">--}}
{{--				<div class="sixteen columns">--}}
{{--					<ul id="owl-logos" class="owl-carousel owl-theme">--}}
{{--						<li><img  src="{{ asset('/images/logos/1.png') }}" alt="" /></li>--}}
{{--						<li><img  src="{{ asset('/images/logos/2.png') }}" alt="" /></li>--}}
{{--						<li><img  src="{{ asset('/images/logos/3.png') }}" alt="" /></li>--}}
{{--						<li><img  src="{{ asset('/images/logos/4.png') }}" alt="" /></li>--}}
{{--						<li><img  src="{{ asset('/images/logos/1.png') }}" alt="" /></li>--}}
{{--						<li><img  src="{{ asset('/images/logos/2.png') }}" alt="" /></li>--}}
{{--						<li><img  src="{{ asset('/images/logos/3.png') }}" alt="" /></li>--}}
{{--						<li><img  src="{{ asset('/images/logos/4.png') }}" alt="" /></li>--}}
{{--					</ul>--}}
{{--				</div>--}}
{{--			</div>--}}
{{--		</section>--}}
        @include('pharma.layouts.footer')
	</main>

	<div class="scroll-to-top">&#xf106;</div>



	<!-- JAVASCRIPT
    ================================================== -->
<script type="text/javascript" src="{{ asset('/js/jquery-2.1.1.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/modernizr.custom.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/jquery.mobile.custom.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/retina-1.1.0.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/jquery.animsition.min.js') }}"></script>
<script type="text/javascript">

(function($) { "use strict";
	$(document).ready(function() {
        @if (session('login_error'))
        open_login();
        @endif
	  $(".animsition").animsition({

		inClass               :   'zoom-in-sm',
		outClass              :   'zoom-out-sm',
		inDuration            :    1500,
		outDuration           :    800,
		linkElement           :   '.animsition-link',
		// e.g. linkElement   :   'a:not([target="_blank"]):not([href^=#])'
		loading               :    true,
		loadingParentElement  :   'body', //animsition wrapper element
		loadingClass          :   'animsition-loading',
		unSupportCss          : [ 'animation-duration',
								  '-webkit-animation-duration',
								  '-o-animation-duration'
								],
		//"unSupportCss" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
		//The default setting is to disable the "animsition" in a browser that does not support "animation-duration".

		overlay               :   false,

		overlayClass          :   'animsition-overlay-slide',
		overlayParentElement  :   'body'
	  });
	});
})(jQuery);
</script>

<script type="text/javascript" src="{{ asset('/js/jquery.themepunch.tools.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/jquery.themepunch.revolution.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/jquery.easing.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/jquery.hidescroll.min.js') }}"></script>
<script type="text/javascript">
	$('.header-top').hidescroll();
</script>
<script type="text/javascript" src="{{ asset('/js/smoothScroll.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/jquery.parallax-1.1.3.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/jquery.counterup.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/waypoints.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/scrollReveal.js') }}"></script>
<script type="text/javascript">
(function($) { "use strict";
      window.scrollReveal = new scrollReveal();
})(jQuery);
</script>
<script type="text/javascript" src="{{ asset('/js/owl.carousel.min.js') }}"></script>
<script type="text/javascript">
(function($) { "use strict";
			jQuery(document).ready(function() {
				var offset = 450;
				var duration = 500;
				jQuery(window).scroll(function() {
					if (jQuery(this).scrollTop() > offset) {
						jQuery('.scroll-to-top').fadeIn(duration);
					} else {
						jQuery('.scroll-to-top').fadeOut(duration);
					}
				});

				jQuery('.scroll-to-top').click(function(event) {
					event.preventDefault();
					jQuery('html, body').animate({scrollTop: 0}, duration);
					return false;
				})
			});
})(jQuery);
</script>
<script type="text/javascript" src="{{ asset('/js/styleswitcher.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/custom-shop-home-1.js') }}"></script>
{{--<script type="text/javascript" src="{{ asset('/js/custom-index.js') }}"></script>--}}
<!-- End Document

================================================== -->
@endsection


