@extends('pharma.layouts.main')
@section('breadcrumbs')
    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset;background-color:#7AA43F;">
            <ol class="breadcrumb medlab_breadcrumbs" style="margin-bottom: unset;background-color:#7AA43F;">
            </ol>
        </div>
    </div>

    <div class="medlab_breadcrumbs_wrapper">
        <div class="container" style="width: unset">
            <ol class="breadcrumb medlab_breadcrumbs">
                <li><a class="medlab_breadcrumbs_link" href="/">Home</a></li>
                <li><a class="medlab_breadcrumbs_link" href="{{ route('pharma.products') }}">Products</a></li>
                <li class="active medlab_breadcrumbs_text">{{ $productfamily->product_name }}</li>
            </ol>
        </div>
    </div>
@endsection
@section('content')


    <script>
        dataLayer = [];
    </script>


    <main class="cd-main-content" id="main">


    {{--    <!-- TOP SECTION--}}
    {{--================================================== -->--}}

    {{--    <section class="section white-section section-home-padding-top">--}}
    {{--        <div class="container">--}}
    {{--            <div class="sixteen columns">--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </section>--}}

    <!-- MAIN SECTION
     ================================================== -->


        @if(!$this_product['unapproved_drug']=='1')
        <section class="section white-section section-padding-bottom" style="margin-top: 120px;padding-bottom: unset">
            <div class="container">
                <!-- Product Images -->
                <div class="twelve columns white-section">
                    <div class="shop-carousel-wrap grey-section" style="position: relative">

                        <div id="sync1" class="owl-carousel"
                             style="width: 70%;display: inline-block;position: absolute">

                            {{--                            @foreach ($products as $mainproductpicture)--}}
                            @if($this_product['img_front_view']!=''&&$this_product['img_front_view']!='Null')
                                <div class="item">
                                    <img src="https://cdn.medlab.co/{{ $this_product['img_front_view']}}" alt=""/>
                                </div>
                            @else
                                <div class="item">
                                    <img src="{{ asset('/images/default_image.png') }}" alt=""/>
                                </div>
                            @endif
                            @if($this_product['img_left_view']!=''&&$this_product['img_left_view']!='Null')
                                <div class="item">
                                    <img src="https://cdn.medlab.co/{{ $this_product['img_left_view']}}" alt=""/>
                                </div>
                            @else
                                <div class="item">
                                    <img src="{{ asset('/images/default_image.png') }}" alt=""/>
                                </div>
                            @endif
                            @if($this_product['img_right_view']!=''&&$this_product['img_right_view']!='Null')
                                <div class="item">
                                    <img src="https://cdn.medlab.co/{{ $this_product['img_right_view']}}" alt=""/>
                                </div>
                            @else
                                <div class="item">
                                    <img src="{{ asset('/images/default_image.png') }}" alt=""/>
                                </div>
                            @endif
                            {{--                            @endforeach--}}
                        </div>
                        <div id="sync2" class="owl-carousel"
                             style="display: inline-block;writing-mode: vertical-rl;width: 75%;float: right;">
                            {{--                            @foreach ($products as $smallproductpictures)--}}
                            @if($this_product['img_front_view_small']!=''&&$this_product['img_front_view_small']!='Null')
                                <div class="item">
                                    <img src="https://cdn.medlab.co/{{ $this_product['img_front_view_small']}}" alt=""/>
                                </div>
                            @else
                                <div class="item">
                                    <img src="{{ asset('/images/default_small_image.png') }}" alt=""/>
                                </div>
                            @endif
                            @if($this_product['img_left_view_small']!=''&&$this_product['img_left_view_small']!='Null')
                                <div class="item">
                                    <img src="https://cdn.medlab.co/{{ $this_product['img_left_view_small']}}" alt=""/>
                                </div>
                            @else
                                <div class="item">
                                    <img src="{{ asset('/images/default_small_image.png') }}" alt=""/>
                                </div>
                            @endif
                            @if($this_product['img_right_view_small']!=''&&$this_product['img_right_view_small']!='Null')
                                <div class="item">
                                    <img src="https://cdn.medlab.co/{{ $this_product['img_right_view_small']}}" alt=""/>
                                </div>
                            @else
                                <div class="item">
                                    <img src="{{ asset('/images/default_small_image.png') }}" alt=""/>
                                </div>
                            @endif
                            {{--                            @endforeach--}}
                        </div>
                    </div>
                    <div class="grey-section" style="margin-top: 20px;">
                        <div class="articleClass padding">
                            <h1 style="text-align: left;">{{ $productfamily->product_name }}</h1><br>
                            <p>{{ $productfamily->product_heading }}</p><br>
                            <p>{{ $productfamily->product_byline }}</p>
                            {!! $productfamily->product_description !!}
                        </div>
                    </div>
                    @if($productfamily->directions_use)
                        <div class="grey-section" style="margin-top: 20px;">
                            <div class="articleClass padding">
                                <h1 style="text-align: left;">Directions of Use</h1><br>
                                {!! $productfamily->directions_use !!}
                            </div>
                        </div>
                    @endif
                    @if($productfamily->reg_permissableindications)
                        <div class="grey-section" style="margin-top: 20px;">
                            <div class="articleClass padding">
                                <h1 style="text-align: left;">Indications</h1><br>
                                {!! $productfamily->reg_permissableindications !!}
                            </div>
                        </div>
                    @endif
                    @if($productfamily->warnings)
                        <div class="grey-section" style="margin-top: 20px;">
                            <div class="articleClass padding">
                                <h1 style="text-align: left;">Warning</h1><br>
                                {!! $productfamily->warnings !!}
                            </div>
                        </div>
                    @endif
                    @if($productfamily->side_effects)
                        <div class="grey-section" style="margin-top: 20px;">
                            <div class="articleClass padding">
                                <h1 style="text-align: left;">Side Effects</h1><br>
                                {!! $productfamily->side_effects !!}
                            </div>
                        </div>
                    @endif
                    @if($productfamily->product_interactions)
                        <div class="grey-section" style="margin-top: 20px;">
                            <div class="articleClass padding">
                                <h1 style="text-align: left;">Interactions</h1><br>
                                {!! $productfamily->product_interactions !!}
                            </div>
                        </div>
                    @endif
                </div>

                <div class="four columns">
                    <div class="post-sidebar">

                        {{--                    <div class="separator-sidebar"></div>--}}
                        <h2>Size</h2>
                        <ul class="link-recents" style="padding: unset">
                            @foreach($products as $a)
                                @if($a->show_on_website=='1'&&($a->obsolete==0||$a->obsolete==null))
                                <li><a style="color: #6ba53a;word-break: break-word;" id="{{ $a->product_description }}"
                                       onclick="updateImage(this.id)"
                                       data-token="{{ csrf_token() }}">{{ $a->product_description }}</a></li>
                                @endif
                            @endforeach
                        </ul>
                        {{--                        <div class="separator-sidebar"></div>--}}
                        {{--                        <hr style="margin: unset;margin-bottom: 20px;margin-top: 20px;">--}}
                        <h2 style="margin-top: 40px">Price RRP INC</h2>
                        @foreach($products as $a)
                            {{--                    <div class="separator-sidebar"></div>--}}
                            {{--                        <hr style="margin: unset;margin-bottom: 20px;margin-top: 20px;">--}}
                            @if($a->show_on_website=='1'&&($a->obsolete==0||$a->obsolete==null))
                            <h7 style="font-weight: bold;">{{ $a->product_description }}</h7>
                            <ul class="link-recents" style="line-height: 2em; padding: unset">
                                {{--                        @foreach($article->product_family as $a)--}}
                                Code: {{ $a->product_code }} <br>
                                GS1: {{ $a->barcode }}<br>
                                Size: {{ $a->atr_unit_count }}{{ $a->atr_unit_count_type }}<br>
                                <p style="font-weight: bold;font-size: x-large;display: inline-block">${{ round($a->sell_price_10*(1+$a->tax_rate),2) }}</p>&nbsp&nbsp<p style="display: inline-block;font-weight: bold">INC GST</p>
                                <br>
                                {{--                        @endforeach--}}
                            </ul>
                                <div class="shop-item-details">
                                    <div class="num-itm-ic" onclick="minusone();">&#xf068;</div>
                                    <div class="num-itm" id="amount" contentEditable="true">1</div>
                                    <div class="num-itm-ic" style="margin-right: 10px;" onclick="addone();">&#xf067;
                                    </div>
                                    <a href="#">
                                        <div class="button-shop">add to cart</div>
                                    </a>
                                </div>
                            @endif
                        @endforeach
                        {{--                        <div class="separator-sidebar"></div>--}}
                        <hr style="margin: unset;margin-bottom: 20px;margin-top: 20px;">
                        <h2>Facts</h2>
                        @if($productfamily->ing_patent_information)
                            <div class="alert alert-green">
                                <p><span>&#xf087;</span>{!! $productfamily->ing_patent_information !!}</p>
                            </div><br>
                        @endif
                        @if($productfamily->store_refrigerated)
                            <div class="alert alert-blue">
                                <p><span>&#xf129;</span>PRODUCT REQUIRES REFRIGERATION</p>
                            </div><br>
                        @endif
                        <ul class="link-recents" style="line-height: 2em;padding: unset;">
                            Age Suitable: {{ $productfamily->reg_suitable_for_ages_above }}<br>
                            Serving Type: {{ $productfamily->unit_type }}<br>
                            AUSTL: {{ $productfamily->austl_number }}<br>
                        </ul>
                        {{--                        <div class="separator-sidebar"></div>--}}
                        <hr style="margin: unset;margin-bottom: 20px;margin-top: 20px;">
                        <h2>Dietary types</h2>
                        @if($productfamily->vegan_friendly)
                            <div class="alert" style="padding: unset;">
                                <a href={{ route('pharma.products') }}?vegan_friendly=1><p style="color: #6ba53a;"><span
                                            style="color: #6ba53a;">&#xf00c;</span>Vegan
                                        Friendly</p></a>
                            </div>
                        @else
                            <div class="alert" style="padding: unset;">
                                <a href={{ route('pharma.products') }}?vegan_friendly=1><p style="color: #6ba53a;"><span
                                            style="color: coral;">&#xf00d;</span>Vegan
                                        Friendly</p></a>
                            </div>
                        @endif
                        @if($productfamily->vegetarian_friendly)
                            <div class="alert" style="padding: unset;">
                                <a href={{ route('pharma.products') }}?vegetarian_friendly=1><p style="color: #6ba53a;"><span
                                            style="color: #6ba53a;">&#xf00c;</span>Vegetarian
                                        Friendly</p></a>
                            </div>
                        @else
                            <div class="alert" style="padding: unset;">
                                <a href={{ route('pharma.products') }}?vegetarian_friendly=1><p style="color: #6ba53a;"><span
                                            style="color: coral;">&#xf00d;</span>Vegetarian
                                        Friendly</p></a>
                            </div>
                        @endif
                        @if($productfamily->gluten_free)
                            <div class="alert" style="padding: unset;">
                                <a href={{ route('pharma.products') }}?gluten_free=1><p style="color: #6ba53a;"><span
                                            style="color: #6ba53a;">&#xf00c;</span>Gluten
                                        Free</p></a>
                            </div>
                        @else
                            <div class="alert" style="padding: unset;">
                                <a href={{ route('pharma.products') }}?gluten_free=1><p style="color: #6ba53a;"><span
                                            style="color: coral;">&#xf00d;</span>Gluten
                                        Free</p></a>
                            </div>
                        @endif
                        @if($productfamily->pregnancy_safe)
                            <div class="alert" style="padding: unset;">
                                <a href={{ route('pharma.products') }}?pregnancy_safe=1><p style="color: #6ba53a;"><span
                                            style="color: #6ba53a;">&#xf00c;</span>Pregnancy
                                        Safe</p></a>
                            </div>
                        @else
                            <div class="alert" style="padding: unset;">
                                <a href={{ route('pharma.products') }}?pregnancy_safe=1><p style="color: #6ba53a;"><span
                                            style="color: coral;">&#xf00d;</span>Pregnancy
                                        Safe</p></a>
                            </div>
                        @endif
                        @if($productfamily->dairy_free)
                            <div class="alert" style="padding: unset;">
                                <a href={{ route('pharma.products') }}?dairy_free=1><p style="color: #6ba53a;"><span
                                            style="color: #6ba53a;">&#xf00c;</span>Dairy
                                        Free</p></a>
                            </div>
                        @else
                            <div class="alert" style="padding: unset;">
                                <a href={{ route('pharma.products') }}?dairy_free=1><p style="color: #6ba53a;"><span
                                            style="color: coral;">&#xf00d;</span>Dairy
                                        Free</p></a>
                            </div>
                        @endif
                        {{--                        <div class="separator-sidebar"></div>--}}
                        <hr style="margin: unset;margin-bottom: 20px;margin-top: 20px;">
                        <h2>Categories</h2>
                        <ul class="link-recents" style="padding: unset">
                            @foreach($product_group as $a)
                                <li><a style="color: #6ba53a;word-break: break-word;"
                                       href={{ route('pharma.products') }}?group={{ $a->name }}>{{ $a->name }}</a></li>
                            @endforeach
                        </ul>

                        {{--                            </ul>--}}


                    </div>
                </div>
            </div>
        </section>


        <section class="section white-section">
            <div class="container">
                <div class="grey-section" style="margin-top: 20px;">
                    <div class="articleClass padding">
                        <h1 style="text-align: left;">Ingredients</h1><br>
                        {!! $productfamily->ing_first_msg !!}<br>
                        @php
                            session_start();
                            $_SESSION['a']=0;
                            $_SESSION['b']=0;
                        @endphp
                        @foreach($product_family_ingredient as $a)
                            @if(!$a->is_excipient)
                                @if(($a->product_ingredient()->first()->practitioner_ingredient==''||$a->product_ingredient()->first()->practitioner_ingredient==null)
                                  &&($a->product_ingredient()->first()->consumer_ingredient==''||$a->product_ingredient()->first()->consumer_ingredient==null))
                                    @if($_SESSION['a']==0)
                                        <div class="accordion_in"
                                             data-scroll-reveal="enter left move 200px over 1s after 0.3s">
                                            @else
                                                <div class="accordion_in"
                                                     data-scroll-reveal="enter right move 200px over 1s after 0.3s">
                                                    @endif
                                                    <div class="acc_head white-section"
                                                         style="font-weight: unset;padding: 20px;padding-right: 20%">
                                                        {!! $a->product_ingredient()->first()->scientific_name !!}<p
                                                            class="hover-white">
                                                            ({{ $a->product_ingredient()->first()->name }})</p>
                                                        <p class="float-move"
                                                           style="display: block;margin: 0px">{{ $a->quantity }}</p>
                                                        @if($a->equiv_to1)
                                                            <br/><p class="mobile-indent">
                                                                Equiv. {{ $a->equiv_to1 }}</p>
                                                            <p class="float-move hover-white"
                                                               style="display: block;margin: 0px">{{ $a->equiv_to1_size }}</p>
                                                        @endif
                                                        @if($a->equiv_to2)
                                                            <br/><p class="mobile-indent">
                                                                Equiv. {{ $a->equiv_to2 }}</p>
                                                            <p class="float-move hover-white"
                                                               style="display: block;margin: 0px">{{ $a->equiv_to2_size }}</p>
                                                        @endif
                                                        @if($a->equiv_to3)
                                                            <br/><p class="mobile-indent">
                                                                Equiv. {{ $a->equiv_to3 }}</p>
                                                            <p class="float-move hover-white"
                                                               style="display: block;margin: 0px">{{ $a->equiv_to2_size }}</p>
                                                        @endif

                                                    </div>
                                                </div><br>
                                                @else
                                                    <div class="accordion">
                                                        @if($_SESSION['a']==0)
                                                            <div class="accordion_in"
                                                                 data-scroll-reveal="enter left move 200px over 1s after 0.3s">
                                                                @else
                                                                    <div class="accordion_in"
                                                                         data-scroll-reveal="enter right move 200px over 1s after 0.3s">
                                                                        @endif
                                                                        <div class="acc_head white-section hover-white"
                                                                             style="font-weight: unset;padding-right: 20%">
                                                                     {!! $a->product_ingredient()->first()->scientific_name !!}
                                                                            <p>
                                                                                ({{ $a->product_ingredient()->first()->name }}
                                                                                )</p>
                                                                            <p class="float-move"
                                                                               style="display: block;margin: 0px">{{ $a->quantity }}</p>
                                                                            @if($a->equiv_to1)
                                                                                <br/><p
                                                                                    class="mobile-indent">
                                                                                    Equiv. {{ $a->equiv_to1 }}</p>
                                                                                <p class="float-move hover-white"
                                                                                   style="display: block;margin: 0px">{{ $a->equiv_to1_size }}</p>
                                                                            @endif
                                                                            @if($a->equiv_to2)
                                                                                <br/><p
                                                                                    class="mobile-indent">
                                                                                    Equiv. {{ $a->equiv_to2 }}</p>
                                                                                <p class="float-move hover-white"
                                                                                   style="display: block;margin: 0px">{{ $a->equiv_to2_size }}</p>
                                                                            @endif
                                                                            @if($a->equiv_to3)
                                                                                <br/><p
                                                                                    class="mobile-indent">
                                                                                    Equiv. {{ $a->equiv_to3 }}</p>
                                                                                <p class="float-move hover-white"
                                                                                   style="display: block;margin: 0px">{{ $a->equiv_to2_size }}</p>
                                                                            @endif
                                                                        </div>
                                                                        <div class="acc_content white-section"
                                                                             style="overflow: hidden">
                                                                            <h4 style="text-align: left;">Consumer
                                                                                Information</h4>
                                                                            {!! $a->product_ingredient()->first()->consumer_ingredient !!}
                                                                            <br>
                                                                            <h4 style="text-align:left;padding-top: 20px;">
                                                                                Practitioner Information</h4>
                                                                            @if(Auth()->check())
                                                                                {!! $a->product_ingredient()->first()->practitioner_ingredient !!}
                                                                            @else
                                                                                <div
                                                                                    class="button-shortcodes text-size-1 text-padding-1 version-1"
                                                                                    onClick="open_login()"><span>&#xf18e;</span>
                                                                                    Please login to view
                                                                                </div>
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                            </div>
                                                        @endif
                                                        @endif
                                                        @php
                                                            if($_SESSION['a']==0){$_SESSION['a']=1;} else{$_SESSION['a']=0;};@endphp
                                                        @endforeach

                                                        <br>{!! $productfamily->ing_last_msg !!}<br>
                                                    </div>
                                        </div>

                                        <div class="grey-section" style="margin-top: 20px;">
                                            <div class="articleClass padding">
                                                <h1 style="text-align: left;">Excipents</h1><br>


                                                @foreach($product_family_ingredient as $a)
                                                    @if($a->is_excipient)
                                                        @if(($a->product_ingredient()->first()->practitioner_ingredient==''||$a->product_ingredient()->first()->practitioner_ingredient==null)
                                                          &&($a->product_ingredient()->first()->consumer_ingredient==''||$a->product_ingredient()->first()->consumer_ingredient==null))
                                                            @if($_SESSION['b']==0)
                                                                <div class="accordion_in"
                                                                     data-scroll-reveal="enter left move 200px over 1s after 0.3s">
                                                                    @else
                                                                        <div class="accordion_in"
                                                                             data-scroll-reveal="enter right move 200px over 1s after 0.3s">
                                                                            @endif
                                                                <div class="acc_head white-section"
                                                                     style="font-weight: unset;padding: 20px;padding-right: 20%">
                                                                    {!! $a->product_ingredient()->first()->scientific_name !!}
                                                                    <p>
                                                                        ({{ $a->product_ingredient()->first()->name }}
                                                                        )</p>
                                                                    <p class="float-move"
                                                                       style="display: block;margin: 0px;">{{ $a->quantity }}</p>
                                                                    @if($a->equiv_to1)
                                                                        <br/><p class="mobile-indent">
                                                                            Equiv. {{ $a->equiv_to1 }}</p>
                                                                        <p class="float-move hover-white"
                                                                           style="display: block;margin: 0px">{{ $a->equiv_to1_size }}</p>
                                                                    @endif
                                                                    @if($a->equiv_to2)
                                                                        <br/><p class="mobile-indent">
                                                                            Equiv. {{ $a->equiv_to2 }}</p>
                                                                        <p class="float-move hover-white"
                                                                           style="display: block;margin: 0px">{{ $a->equiv_to2_size }}</p>
                                                                    @endif
                                                                    @if($a->equiv_to3)
                                                                        <br/><p class="mobile-indent">
                                                                            Equiv. {{ $a->equiv_to3 }}</p>
                                                                        <p class="float-move hover-white"
                                                                           style="display: block;margin: 0px">{{ $a->equiv_to2_size }}</p>
                                                                    @endif
                                                                </div>
                                                            </div><br>
                                                        @else
                                                            <div class="accordion">
                                                                @if($_SESSION['b']==0)
                                                                    <div class="accordion_in"
                                                                         data-scroll-reveal="enter left move 200px over 1s after 0.3s">
                                                                        @else
                                                                            <div class="accordion_in"
                                                                                 data-scroll-reveal="enter right move 200px over 1s after 0.3s">
                                                                                @endif
                                                                    <div class="acc_head white-section hover-white"
                                                                         style="font-weight: unset;padding-right: 20%">
                                                                        {!! $a->product_ingredient()->first()->scientific_name !!}
                                                                        <p>
                                                                            ({{ $a->product_ingredient()->first()->name }}
                                                                            )</p>
                                                                        <p class="float-move"
                                                                           style="display: block;margin: 0px">{{ $a->quantity }}</p>
                                                                        @if($a->equiv_to1)
                                                                            <br/><p class="mobile-indent">
                                                                                Equiv. {{ $a->equiv_to1 }}</p>
                                                                            <p class="float-move"
                                                                               style="display: block;margin: 0px">{{ $a->equiv_to1_size }}</p>
                                                                        @endif
                                                                        @if($a->equiv_to2)
                                                                            <br/><p class="mobile-indent">
                                                                                Equiv. {{ $a->equiv_to2 }}</p>
                                                                            <p class="float-move"
                                                                               style="display: block;margin: 0px">{{ $a->equiv_to2_size }}</p>
                                                                        @endif
                                                                        @if($a->equiv_to3)
                                                                            <br/><p class="mobile-indent">
                                                                                Equiv. {{ $a->equiv_to3 }}</p>
                                                                            <p class="float-move"
                                                                               style="display: block;margin: 0px">{{ $a->equiv_to2_size }}</p>
                                                                        @endif
                                                                    </div>
                                                                    <div class="acc_content white-section"
                                                                         style="overflow: hidden">
                                                                        <h4 style="text-align: left;">Consumer
                                                                            Information</h4>
                                                                        {!! $a->product_ingredient()->first()->consumer_ingredient !!}
                                                                        <br>
                                                                        <h4 style="text-align:left;padding-top: 20px;">
                                                                            Practitioner Information</h4>
                                                                        @if(Auth()->check())
                                                                            {!! $a->product_ingredient()->first()->practitioner_ingredient !!}
                                                                        @else
                                                                            <div
                                                                                class="button-shortcodes text-size-1 text-padding-1 version-1"
                                                                                onClick="open_login()">
                                                                                <span>&#xf18e;</span> Please login to
                                                                                view
                                                                            </div>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endif
                                                    @endif
                                                                        @php
                                                                            if($_SESSION['b']==0){$_SESSION['b']=1;} else{$_SESSION['b']=0;};@endphp
                                                @endforeach
                                            </div>
                                        </div>

                    </div>
        </section>
        @if($productfamily->consumer_video)
            <div class="container">
                <hr>
            </div>
            <section class="section grey-section">
                <div class="office-1">
                    <div class="box-1">
                        {{--                    <ul id="owl-office" class="owl-carousel owl-theme">--}}
                        <iframe width="100%" height="350" frameborder="0" scrolling="no" marginheight="0"
                                marginwidth="0" allowfullscreen
                                src="https://player.vimeo.com/video/{{ $productfamily->consumer_video }}"></iframe>
                        {{--                        <li><img  src="images/office2.jpg" alt="" /></li>--}}
                        {{--                        <li><img  src="images/office3.jpg" alt="" /></li>--}}
                        {{--                    </ul>--}}
                    </div>
                    <div class="box-1">
                        <div class="text-in">
                            <div class="section-title left office-text">
                                <div class="subtitle left">
                                    About
                                </div>
                                <h3>{{ $productfamily->product_name }}</h3>
                                <div class="subtitle left">
                                    Consumer Video
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        @endif

        @if($productfamily->practitioner_video)
            <div class="container">
                <hr>
            </div>
            <section class="section grey-section">

                <div class="office-1">

                    <div class="box-1">
                        <div class="text-in">
                            <div class="section-title right office-text">
                                <div class="subtitle right">
                                    About
                                </div>
                                <h3>{{ $productfamily->product_name }}</h3>
                                <div class="subtitle right">
                                    Practitioner Video
                                </div>
                                @if(!Auth::check())
                                    <div style="margin-right: unset;margin-bottom: 3%;"
                                         class="button-shortcodes text-size-1 text-padding-1 version-1"
                                         onClick="open_login()"><span>&#xf18e;</span> Please login to view
                                    </div>
                                @endif

                            </div>
                        </div>
                    </div>
                    @if(Auth::check()&&Auth::user()->role_id==1)
                        <div class="box-1">
                            <iframe width="100%" height="350" frameborder="0" scrolling="no" marginheight="0"
                                    marginwidth="0" allowfullscreen
                                    src="https://player.vimeo.com/video/{{ $productfamily->practitioner_video }}"></iframe>
                        </div>
                    @elseif(Auth::check()&&Auth::user()->role_id==2)
                        <div class="box-1">
                            <iframe
                                srcdoc="<p style='text-align:center;font-size:xxx-large;color:white;padding:5%;margin-top:15%;'>Restricted to Practitioner accounts</p>"
                                width="100%" height="350"
                                allowfullscreen style="background-color:black;"></iframe>
                        </div>
                    @else
                        <div class="box-1">
                            <iframe
                                srcdoc="<p style='text-align:center;font-size:xxx-large;color:white;padding:5%;margin-top:15%;'>Please login to view this video</p>"
                                width="100%" height="350"
                                allowfullscreen style="background-color:black;"></iframe>
                        </div>
                    @endif

                </div>
            </section>
        @endif

        @if(count($product_pdf_cmi))
            <div class="container">
                <hr>
            </div>
            <section class="section white-section section-padding-top-bottom">
                <div class="container">
                    <h1 style="text-align: left;">Consumer Medicines Information (CMI)</h1><br>
                    @foreach($product_pdf_cmi as $a)
                        <div class="one-third column" data-scroll-reveal="enter left move 200px over 1s after 0.3s">
                            <a href="{{ $image_url }}/{{ $a['file_name'] }}" class="animsition-link">
                                <div class="blog-box-1 white-section" style="padding: unset;margin:unset;">
                                    <img src="{{ $image_url }}/www/Images/large-flag/{{ $a['language'] }}.jpg"
                                         alt="{{ $a['language'] }}">
                                    <div class="link"
                                         style="color: #6ba53a;text-align: center">{{ $a['language'] }}</div>
                                </div>
                            </a>
                        </div>
                    @endforeach

                </div>
            </section>
        @endif

        @if(count($publication))
            <div class="container">
                <hr>
            </div>
            <section class="section white-section section-padding-top-bottom">
                <div class="container">
                    <h1 style="text-align: left;">Related Publications</h1><br>
                    @for($i=0;$i<count($publication);$i++)
                        {{--                @foreach($publication as $a)--}}
                        @if($i%2==0)
                            <div class="accordion">
                                <div class="accordion_in"
                                     data-scroll-reveal="enter left move 200px over 1s after 0.3s">
                                    <div class="acc_head grey-section">{{$publication[$i]['title']}}</div>
                                    <div class="acc_content grey-section" style="padding: 5px;">
                                        <table id="publication_table_{{ $publication[$i]['id'] }}"
                                               class="display compact"
                                               style="width:100%">
                                            <thead>
                                            <tr style="background-color: lightgrey;">
                                                <th style="width: 35%">Title</th>
                                                <th style="width: 65%">Content</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if($publication[$i]['publication_location'])
                                                <tr>
                                                    <td>Location</td>
                                                    <td>{{ $publication[$i]['publication_location'] }}</td>
                                                </tr>
                                            @endif
                                            @if($publication[$i]['date'])
                                                <tr>
                                                    <td>Date</td>
                                                    <td>{{ substr($publication[$i]['date'],0,10) }}</td>
                                                </tr>
                                            @endif

                                            @if(count($publication[$i]['member']))

                                                <tr>
                                                    <td>Authors</td>
                                                    <td>
                                                        @if(count($publication[$i]['member']))
                                                            {{$publication[$i]['member'][0]['name']}}
                                                            @if(count($publication[$i]['member'])!=1)
                                                                @for($j=1;$j<count($publication[$i]['member']);$j++)
                                                                    ,&nbsp{{ $publication[$i]['member'][$j]['name'] }}
                                                                @endfor
                                                            @endif
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endif
                                            @if($publication[$i]['link'])
                                                <tr>
                                                    <td>Link</td>
                                                    <td>
                                                        <a style="color: #6ba53a;word-break: break-word;"
                                                           href="{{ $publication[$i]['link'] }}"
                                                           target="_blank">{{ $publication[$i]['link'] }}</a>
                                                    </td>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="accordion">
                                <div class="accordion_in"
                                     data-scroll-reveal="enter right move 200px over 1s after 0.3s">
                                    <div class="acc_head grey-section">{{$publication[$i]['title']}}</div>
                                    <div class="acc_content grey-section" style="padding: 5px;">
                                        <table id="publication_table_{{ $publication[$i]['id'] }}"
                                               class="display compact"
                                               style="width:100%">
                                            <thead>
                                            <tr style="background-color: lightgrey;">
                                                <th style="width: 35%">Title</th>
                                                <th style="width: 65%">Content</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if($publication[$i]['publication_location'])
                                                <tr>
                                                    <td>Location</td>
                                                    <td>{{ $publication[$i]['publication_location'] }}</td>
                                                </tr>
                                            @endif
                                            @if($publication[$i]['date'])
                                                <tr>
                                                    <td>Date</td>
                                                    <td>{{ substr($publication[$i]['date'],0,10) }}</td>
                                                </tr>
                                            @endif

                                            @if(count($publication[$i]['member']))

                                                <tr>
                                                    <td>Authors</td>
                                                    <td>
                                                        @if(count($publication[$i]['member']))
                                                            {{$publication[$i]['member'][0]['name']}}
                                                            @if(count($publication[$i]['member'])!=1)
                                                                @for($j=1;$j<count($publication[$i]['member']);$j++)
                                                                    ,&nbsp{{ $publication[$i]['member'][$j]['name'] }}
                                                                @endfor
                                                            @endif
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endif
                                            @if($publication[$i]['link'])
                                                <tr>
                                                    <td>Link</td>
                                                    <td>
                                                        <a style="color: #6ba53a;word-break: break-word;"
                                                           href="{{ $publication[$i]['link'] }}"
                                                           target="_blank">{{ $publication[$i]['link'] }}</a>
                                                    </td>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endfor
                </div>
            </section>
        @endif


        @if(count($clinical_trial->toArray()))
            <div class="container">
                <hr>
            </div>
            <section class="section white-section section-padding-top-bottom">
                <div class="container">
                    <h1 style="text-align: left;">Related Clinical Trials</h1><br>

                    @php
                        $_SESSION['i']=0;
                    @endphp
                    @foreach($clinical_trial as $a)
                        {{--                @foreach($publication as $a)--}}
                        @if($_SESSION['i']==0)
                            <div class="accordion">
                                <div class="accordion_in"
                                     data-scroll-reveal="enter left move 200px over 1s after 0.3s">
                                    @else
                                        <div class="accordion">
                                            <div class="accordion_in"
                                                 data-scroll-reveal="enter right move 200px over 1s after 0.3s">
                                                @endif
                                                <div class="acc_head grey-section">{{$a->name}}</div>
                                                <div class="acc_content grey-section" style="padding: 5px;">
                                                    <table id="clinical_table_{{ $a->id }}" class="display compact"
                                                           style="width:100%">
                                                        <thead>
                                                        <tr style="background-color: lightgrey;">
                                                            <th style="width: 35%">Title</th>
                                                            <th style="width: 65%">Content</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @if($a->name)
                                                            <tr>
                                                                <td>Name</td>
                                                                <td>{{ $a->name }}</td>
                                                            </tr>
                                                        @endif
                                                        @if($a->type)
                                                            <tr>
                                                                <td>Type</td>
                                                                <td>{{ $a->type }}</td>
                                                            </tr>
                                                        @endif
                                                        @if($a->about)
                                                            <tr>
                                                                <td>About</td>
                                                                <td>{!! $a->about !!}</td>
                                                            </tr>
                                                        @endif
                                                        @if($a->formulation)
                                                            <tr>
                                                                <td>Formulation</td>
                                                                <td>{!! $a->formulation !!}</td>
                                                            </tr>
                                                        @endif
                                                        @if($a->date_started)
                                                            <tr>
                                                                <td>Date started</td>
                                                                <td>{{ substr($a->date_started,0,10) }}</td>
                                                            </tr>
                                                        @endif
                                                        @if($a->date_hrec)
                                                            <tr>
                                                                <td>Date of HREC approval</td>
                                                                <td>{{ substr($a->date_hrec,0,10) }}</td>
                                                            </tr>
                                                        @endif
                                                        @if($a->hrec_name)
                                                            <tr>
                                                                <td>HREC name</td>
                                                                <td>{{ $a->hrec_name }}</td>
                                                            </tr>
                                                        @endif
                                                        @if($a->hrec_id)
                                                            <tr>
                                                                <td>HREC approval ID</td>
                                                                <td>{{ $a->hrec_id }}</td>
                                                            </tr>
                                                        @endif
                                                        @if($a->ctn)
                                                            <tr>
                                                                <td>CTN</td>
                                                                <td>{{ $a->ctn }}</td>
                                                            </tr>
                                                        @endif
                                                        @if($a->anzctr)
                                                            <tr>
                                                                <td>ANZCTR</td>
                                                                <td>{{ $a->anzctr }}</td>
                                                            </tr>
                                                        @endif
                                                        @if($a->anzctr_link)
                                                            <tr>
                                                                <td>ANZCTR URL Link</td>
                                                                <td><a href="{{ $a->anzctr_link }}"
                                                                       style="color: #6ba53a;word-break: break-word;">{{ $a->anzctr_link }}</a>
                                                                </td>
                                                            </tr>
                                                        @endif
                                                        @if($a->study_site)
                                                            <tr>
                                                                <td>Study Site</td>
                                                                <td>{{ $a->study_site }}</td>
                                                            </tr>
                                                        @endif
                                                        @if($a->status)
                                                            <tr>
                                                                <td>Status</td>
                                                                <td>{{ $a->status }}</td>
                                                            </tr>
                                                        @endif
                                                        @if($a->progression)
                                                            <tr>
                                                                <td>Progression</td>
                                                                <td>{{ $a->progression }}</td>
                                                            </tr>
                                                        @endif
                                                        @if(count($a->brand()->get()->toArray()))
                                                            <tr>
                                                                <td>Brand</td>
                                                                <td>
                                                                    @for($i=0;$i<count($a->brand()->get()->toArray());$i++)
                                                                        @if($i==count($a->brand()->get()->toArray())-1)
                                                                            {{ $a->brand()->get()->toArray()[$i]['name'] }}
                                                                        @else
                                                                            {{ $a->brand()->get()->toArray()[$i]['name'] }}
                                                                            ,
                                                                        @endif
                                                                    @endfor
                                                                </td>
                                                            </tr>
                                                        @endif
                                                        @if(count($a->product_family()->get()->toArray()))
                                                            <tr>
                                                                <td>Product Family</td>
                                                                <td>
                                                                    @for($i=0;$i<count($a->product_family()->get()->toArray());$i++)
                                                                        @if($i==count($a->product_family()->get()->toArray())-1)
                                                                            {{ $a->product_family()->get()->toArray()[$i]['product_name'] }}
                                                                        @else
                                                                            {{ $a->product_family()->get()->toArray()[$i]['product_name'] }}
                                                                            ,
                                                                        @endif
                                                                    @endfor
                                                                </td>
                                                            </tr>
                                                        @endif
                                                        @if(count($a->member()->get()->toArray()))
                                                            <tr>
                                                                <td>Member</td>
                                                                <td>@for($i=0;$i<count($a->member()->get()->toArray());$i++)
                                                                        @if($i==count($a->member()->get()->toArray())-1)
                                                                            {{ $a->member()->get()->toArray()[$i]['name'] }}
                                                                        @else
                                                                            {{ $a->member()->get()->toArray()[$i]['name'] }}
                                                                            ,
                                                                        @endif
                                                                    @endfor
                                                                </td>
                                                            </tr>
                                                        @endif
                                                        @if(count($a->partner()->get()->toArray()))
                                                            <tr>
                                                                <td>Partner</td>
                                                                <td>
                                                                    @for($i=0;$i<count($a->partner()->get()->toArray());$i++)
                                                                        @if($i==count($a->partner()->get()->toArray())-1)
                                                                            {{ $a->partner()->get()->toArray()[$i]['name'] }}
                                                                        @else
                                                                            {{ $a->partner()->get()->toArray()[$i]['name'] }}
                                                                            ,
                                                                        @endif
                                                                    @endfor
                                                                </td>
                                                            </tr>
                                                        @endif
                                                        @if(count($a->product_ingredient()->get()->toArray()))
                                                            <tr>
                                                                <td>Product Ingredient</td>
                                                                <td>
                                                                    @for($i=0;$i<count($a->product_ingredient()->get()->toArray());$i++)
                                                                        @if($i==count($a->product_ingredient()->get()->toArray())-1)
                                                                            {!! $a->product_ingredient()->get()->toArray()[$i]['scientific_name'] !!}
                                                                        @else
                                                                            {!! $a->product_ingredient()->get()->toArray()[$i]['scientific_name'] !!}
                                                                            ,
                                                                        @endif
                                                                    @endfor
                                                                </td>
                                                            </tr>
                                                        @endif
                                                        @if(count($a->product_group()->get()->toArray()))
                                                            <tr>
                                                                <td>Product Group</td>
                                                                <td>
                                                                    @for($i=0;$i<count($a->product_group()->get()->toArray());$i++)
                                                                        @if($i==count($a->product_group()->get()->toArray())-1)
                                                                            {{ $a->product_group()->get()->toArray()[$i]['name'] }}
                                                                        @else
                                                                            {{ $a->product_group()->get()->toArray()[$i]['name'] }}
                                                                            ,
                                                                        @endif
                                                                    @endfor
                                                                </td>
                                                            </tr>
                                                        @endif
                                                        @if(count($a->article()->get()->toArray()))
                                                            <tr>
                                                                <td>Article</td>
                                                                <td>
                                                                    @for($i=0;$i<count($a->article()->get()->toArray());$i++)
                                                                        @if($i==count($a->article()->get()->toArray())-1)
                                                                            {{ $a->article()->get()->toArray()[$i]['title'] }}
                                                                        @else
                                                                            {{ $a->article()->get()->toArray()[$i]['title'] }}
                                                                            ,
                                                                        @endif
                                                                    @endfor
                                                                </td>
                                                            </tr>
                                                        @endif
                                                        @if(count($a->publicationsandpresentation()->get()->toArray()))
                                                            <tr>
                                                                <td>Publications and presentations</td>
                                                                <td>
                                                                    @for($i=0;$i<count($a->publicationsandpresentation()->get()->toArray());$i++)
                                                                        @if($i==count($a->publicationsandpresentation()->get()->toArray())-1)
                                                                            {{ $a->publicationsandpresentation()->get()->toArray()[$i]['title'] }}
                                                                        @else
                                                                            {{ $a->publicationsandpresentation()->get()->toArray()[$i]['title'] }}
                                                                            ,
                                                                        @endif
                                                                    @endfor
                                                                </td>
                                                            </tr>
                                                        @endif
                                                        @php
                                                            if($_SESSION['i']==0){$_SESSION['i']=1;} else{$_SESSION['i']=0;};@endphp
                                                        </tbody>
                                                    </table>

                                                </div>
                                            </div>
                                        </div>
                                        @endforeach

                                </div>
                            </div>
                </div>
            </section>
        @endif
        @if(count($not_pdf))
            <div class="container">
                <hr>
            </div>
            <section class="section white-section section-padding-top-bottom">
                <div class="container">
                    <h1 style="text-align: left;">Practitioner Downloads</h1>

                    @if(Auth::check()&&Auth()->user()->role_id==1)
                        @foreach($not_pdf as $type)
                            <h2>{{ $type[0]['pdf_type_id'] }}</h2>

                            @foreach($type as $a)
                                <div class="grey-section" style="margin-bottom: 20px;min-height: 80px;">
                                    <div class="articleClass padding">
                                        {{--                                            <h4 style="float: left;width: 90%;color:#6ba53a;" href="{{ $image_url }}/{{ $a['file_name']}}">{{ $a['name'] }}</h4>--}}
                                        <p style="margin: 0px;float: left;width: 94%;"><a
                                                href="{{ $image_url }}/{{ $a['file_name']}}"
                                                style="color:#6ba53a;font-size: larger;font-weight: 900;">{{ $a['name'] }}</a>
                                        </p>
                                        <img src="{{ $image_url }}/www/Images/large-flag/{{ $a['language'] }}.jpg"
                                             alt="{{ $a['language'] }}" style="width: 6%;float: right;">

                                        @if($a['description'])
                                            <p style="margin: 0px;">{!! $a['description'] !!}</p>
                                        @endif

                                    </div>
                                </div>
                            @endforeach

                        @endforeach
                    @elseif(Auth::check()&&Auth()->user()->role_id==2)
                        <div class="grey-section" style="margin-top: 20px;">
                            <div class="articleClass padding">
                                <p style="margin: 0px;">Practitioner Only!</p>
                            </div>
                        </div>
                    @else
                        <div class="grey-section" style="margin-top: 20px;">
                            <div class="button-shortcodes text-size-1 text-padding-1 version-1"
                                 onClick="open_login()"><span>&#xf18e;</span> Please login to view
                            </div>
                        </div>
                    @endif

                </div>
            </section>
        @endif

        @if(count($article->toArray()))
            <div class="container">
                <hr>
            </div>
            <section class="section white-section section-padding-top-bottom">
                <div class="container">

                    <h1 style="text-align: left;">Related Articles</h1>
                    <div class="blog-wrapper">
                        <div id="blog-grid-masonry">
                            @foreach($article as $item)

                                {{--                    <a href={{ route('pharma.articles_content',['id' => $item->id]) }} class="animsition-link">--}}
                                {{--                        <div class="blog-box-1 white-section">--}}
                                {{--                            <img src="{{ $image_url }}/www/Images/{{ $item->title_img }}"--}}
                                {{--                                 alt="{{ $item->title_alt }}">--}}
                                {{--                            <div class="blog-date-1">{{ substr($item->date,0,10) }}</div>--}}
                                {{--                            <div class="blog-comm-1">3 <span>&#xf086;</span></div>--}}
                                {{--                            <h4>{{ $item->title }}</h4>--}}
                                {{--                            <p>{{ $item->preview_text }}</p>--}}
                                {{--                            <div class="link">&#xf178;</div>--}}
                                {{--                        </div>--}}
                                {{--                    </a>--}}
                                @if($item->article_type=='link')
                                    <a href="https://dev-pharma.medlab.co/articles_content/{{ $item->id }}"
                                       class="animsition-link">
                                        <div class="blog-box-3">
                                            <div class="blog-box-1 link-post grey-section">
                                                <h4>{{ $item->title }}</h4>
                                                <p>{{ $item->preview_text }}</p>
                                                <p>{{ substr($item->date,0,10) }}</p>
                                                @if(isset($item->author))<p>Author: {{ $item->author }}</p>@endif
                                                <div class="link">&#xf178;</div>
                                            </div>
                                        </div>
                                    </a>
                                @elseif($item->article_type=='video')
                                    <a href="https://dev-pharma.medlab.co/articles_content/{{ $item->id }}"
                                       class="animsition-link">
                                        <div class="blog-box-3">
                                            <div class="blog-box-1 grey-section">
                                                {{--                                        @if(($item->req_login && !Auth()->check()))--}}
                                                {{--                                            <div style="opacity: 0.85;background: black;">--}}
                                                {{--                                                <iframe srcdoc="<p style='text-align:center;margin-top:15%;font-size:xx-large;color:white'>Please login to view this video</p>" src="{{ $item->video_link }}" width="100" height="56"--}}
                                                {{--                                                        allowfullscreen></iframe>--}}
                                                {{--                                            </div>--}}
                                                {{--                                        @else--}}
                                                <iframe src="{{ $item->video_link }}" width="100%" height="auto"
                                                        allowfullscreen></iframe>
                                                {{--                                        @endif--}}
                                                {{--                                                <img src="{{ $image_url }}/www/Images/{{ $item->title_img }}"--}}
                                                {{--                                                     alt="{{ $item->title_alt }}">--}}
                                                <h4>{{ $item->title }}</h4>
                                                <p>{{ $item->preview_text }}</p>
                                                <p>{{ substr($item->date,0,10) }}</p>
                                                @if(isset($item->author))<p>Author: {{ $item->author }}</p>@endif
                                                <div class="link">&#xf178;</div>
                                            </div>
                                        </div>
                                    </a>
                                @else
                                    <a href="https://dev-pharma.medlab.co/articles_content/{{ $item->id }}"
                                       class="animsition-link">
                                        <div class="blog-box-3">
                                            <div class="blog-box-1 grey-section">
                                                <img src="{{ $image_url }}/www/Images/{{ $item->title_img }}"
                                                     alt="{{ $item->title_alt }}">
                                                <h4>{{ $item->title }}</h4>
                                                <p>{{ $item->preview_text }}</p>
                                                <p>{{ substr($item->date,0,10) }}</p>
                                                @if(isset($item->author))<p>Author: {{ $item->author }}</p>@endif
                                                <div class="link">&#xf178;</div>
                                            </div>
                                        </div>
                                    </a>
                                @endif

                            @endforeach
                        </div>
                    </div>
                </div>
            </section>
            <section class="section white-section section-padding-top-bottom">
                <div class="container">
                    <div class="sixteen columns">
                        <div class="blog-left-right-links pagination">
                            <a onclick="changePage(0)" id="pre" style="display: none;cursor:pointer">
                                <div class="blog-left-link"><p>PREVIOUS 12</p></div>
                            </a>
                            <a onclick="changePage(1)" id="next" style="display: unset;cursor:pointer">
                                <div class="blog-right-link"><p>NEXT 12</p></div>
                            </a>
                            {{--                                    <a>--}}
                            {{--                            <div style="padding: 6px">--}}
                            {{--                                <select onchange="changePage()" id="page" style="float: right;margin-right: 5%;">--}}
                            {{--                                    @for($i=1;$i<$allArticles->lastPage()+1;$i++)--}}
                            {{--                                        <option value={{ $i }} @if($allArticles->currentPage() == $i) selected @endif >{{ $i }}</option>--}}
                            {{--                                    @endfor--}}
                            {{--                                </select><p style="float: right">Page No.</p>--}}
                            {{--                            </div>--}}
                            {{--                                    </a>--}}


                        </div>
                    </div>
                </div>

            </section>
        @endif
        @else
            <section class="section white-section section-padding-bottom" style="margin-top: 120px;padding-bottom: unset">
                <div class="container">
                    <div class="grey-section" style="margin-top: 20px;">
                        <div class="articleClass padding">
                            <h1 style="text-align: left;">{{ $productfamily->product_name }}</h1><br>
                            <p>{{ $productfamily->product_heading }}</p><br>
                            <p>{{ $productfamily->product_byline }}</p>
                            {!! $productfamily->unlisted_drug_description !!}
                        </div>
                        <div
                            class="button-shortcodes text-size-1 text-padding-1 version-1"
                            onClick="open_login()"><span>&#xf18e;</span>
                            Please login to view
                        </div>
                    </div>

                </div>
            </section>

        @endif


    <!-- SECTION
        ================================================== -->


        @include('pharma.layouts.footer')


    </main>




    <div class="scroll-to-top">&#xf106;</div>





    <!-- JAVASCRIPT
    ================================================== -->
    <script type="text/javascript" src="{{ asset('/js/jquery-2.1.1.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/modernizr.custom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.mobile.custom.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/retina-1.1.0.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.animsition.min.js') }}"></script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            $(document).ready(function () {
                @if (session('login_error'))
                open_login();
                @endif
                @if(!$this_product['unapproved_drug']=='1')
                sessionStorage.setItem('current_page', 1);
                sessionStorage.setItem('last_page',{{ $totalPage }});
                if (sessionStorage.getItem('last_page') == '1') {
                    document.getElementById('next').style.display = 'none';
                }
                @endif

                $(".animsition").animsition({

                    inClass: 'zoom-in-sm',
                    outClass: 'zoom-out-sm',
                    inDuration: 1500,
                    outDuration: 800,
                    linkElement: '.animsition-link',
                    // e.g. linkElement   :   'a:not([target="_blank"]):not([href^=#])'
                    loading: true,
                    loadingParentElement: 'body', //animsition wrapper element
                    loadingClass: 'animsition-loading',
                    unSupportCss: ['animation-duration',
                        '-webkit-animation-duration',
                        '-o-animation-duration'
                    ],
                    //"unSupportCss" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
                    //The default setting is to disable the "animsition" in a browser that does not support "animation-duration".

                    overlay: false,

                    overlayClass: 'animsition-overlay-slide',
                    overlayParentElement: 'body'
                });
            });
        })(jQuery);
        // var cw = $('#sync2').width();
        // $('#sync2').css({'height': cw + 'px'});

        function updateImage(id) {
            var token = document.getElementById(id).getAttribute('data-token');
            var data = 'id=' + id;
            $.ajax({
                type: "POST",
                url: '{{ url('pharma.updateImage')}}',
                data: {
                    _token: token,
                    'id': id,
                },
                dataType: 'json',
                success: function (data) {
                    console.log(document.getElementsByClassName('item'));
                    var result_1 = '';
                    var result_2 = '';
                    var maxwidth = document.getElementsByClassName('owl-item')[0].style.width;
                    var minwidth = document.getElementsByClassName('owl-item')[document.getElementsByClassName('owl-item').length - 1].style.width;
                    if (data['img_front_view_small'] == '' || data['img_front_view_small'] == 'Null') {
                        document.getElementsByClassName('item')[0].innerHTML = "<img src='{{ asset('/images/default_image.png') }}' alt=''/>";
                        document.getElementsByClassName('item')[3].innerHTML = "<img src='{{ asset('/images/default_small_image.png') }}' alt=''/>";
                    } else {
                        // result_1+=("<div class='owl-item' style='width:"+maxwidth+"'>"+"<div class='item'>"+
                        //     "<img src='https://cdn.medlab.co/"+ data['img_front_view']+"' alt=''/>"+
                        //     "</div>"+"</div>");
                        // result_2+=("<div class='owl-item synced' style='width:"+minwidth+"'>"+"<div class='item'>"+
                        //     "<img src='https://cdn.medlab.co/"+ data['img_front_view']+"' alt=''/>"+
                        //     "</div>"+"</div>");
                        document.getElementsByClassName('item')[0].innerHTML = "<img src='https://cdn.medlab.co/" + data['img_front_view'] + "' alt=''/>";
                        document.getElementsByClassName('item')[3].innerHTML = "<img src='https://cdn.medlab.co/" + data['img_front_view_small'] + "' alt=''/>";
                    }
                    if (data['img_left_view_small'] == '' || data['img_left_view_small'] == 'Null') {
                        document.getElementsByClassName('item')[1].innerHTML = "<img src='{{ asset('/images/default_image.png') }}' alt=''/>";
                        document.getElementsByClassName('item')[4].innerHTML = "<img src='{{ asset('/images/default_small_image.png') }}' alt=''/>";
                    } else {
                        // result_1+=("<div class='owl-item' style='width:"+maxwidth+"'>"+"<div class='item'>"+
                        //     "<img src='https://cdn.medlab.co/"+ data['img_left_view']+"' alt=''/>"+
                        //     "</div>"+"</div>");
                        // result_2+=("<div class='owl-item' style='width:"+minwidth+"'>"+"<div class='item'>"+
                        //     "<img src='https://cdn.medlab.co/"+ data['img_left_view']+"' alt=''/>"+
                        //     "</div>"+"</div>");
                        document.getElementsByClassName('item')[1].innerHTML = "<img src='https://cdn.medlab.co/" + data['img_left_view'] + "' alt=''/>";
                        document.getElementsByClassName('item')[4].innerHTML = "<img src='https://cdn.medlab.co/" + data['img_left_view_small'] + "' alt=''/>";
                    }
                    if (data['img_right_view_small'] == '' || data['img_right_view_small'] == 'Null') {
                        document.getElementsByClassName('item')[2].innerHTML = "<img src='{{ asset('/images/default_image.png') }}' alt=''/>";
                        document.getElementsByClassName('item')[5].innerHTML = "<img src='{{ asset('/images/default_small_image.png') }}' alt=''/>";
                    } else {
                        // result_1+=("<div class='owl-item' style='width:"+maxwidth+"'>"+"<div class='item'>"+
                        //     "<img src='https://cdn.medlab.co/"+ data['img_right_view']+"' alt=''/>"+
                        //     "</div>"+"</div>");
                        // result_2+=("<div class='owl-item' style='width:"+minwidth+"'>"+"<div class='item'>"+
                        //     "<img src='https://cdn.medlab.co/"+ data['img_right_view']+"' alt=''/>"+
                        //     "</div>"+"</div>");
                        document.getElementsByClassName('item')[2].innerHTML = "<img src='https://cdn.medlab.co/" + data['img_right_view'] + "' alt=''/>";
                        document.getElementsByClassName('item')[5].innerHTML = "<img src='https://cdn.medlab.co/" + data['img_right_view_small'] + "' alt=''/>";
                    }
                    // document.getElementsByClassName('owl-wrapper')[0].innerHTML=result_1;
                    // document.getElementsByClassName('owl-wrapper')[1].innerHTML=result_2;
                    // location.reload();

                },
                error: function (data) {
                    console.log(data);

                },
            })

        }

        function addone() {
            document.getElementById('amount').innerHTML = (parseInt(document.getElementById('amount').innerHTML) + 1 >= 0) ? parseInt(document.getElementById('amount').innerHTML) + 1 : 0;
        }

        function minusone() {
            document.getElementById('amount').innerHTML = (parseInt(document.getElementById('amount').innerHTML) - 1 >= 0) ? parseInt(document.getElementById('amount').innerHTML) - 1 : 0;
        }

        $("#amount").keypress(function (e) {
            if (isNaN(String.fromCharCode(e.which))) e.preventDefault();
        });
        $(document).ready(function () {
            @foreach($clinical_trial as $item)

            $('#clinical_table_{{$item['id']}}').DataTable({
                "lengthMenu": [150],
                "bFilter": false,
                "bInfo": false,
                "bPaginate": false,
                "ordering": false,
            });
            @endforeach

            @foreach($publication as $item)

            $('#publication_table_{{$item['id']}}').DataTable({
                "lengthMenu": [150],
                "bFilter": false,
                "bInfo": false,
                "bPaginate": false,
                "ordering": false,
            });
            @endforeach

        });

        function changePage(tag) {
            console.log(sessionStorage.getItem('current_page'));
            if (tag == '0') {
                var page = sessionStorage.getItem('current_page') - 1;
            } else {
                var page = Number(sessionStorage.getItem('current_page')) + 1;
            }
            console.log(page);
            $.ajax({
                headers: {
                    'X-CSRF-Token': '{{ csrf_token() }}',
                },
                url: "{{ url('pharma.articles_page') }}",
                data: {
                    page: page,
                    id: '{{ $productfamily->id }}',
                },
                type: "POST",
                dataType: "JSON",
                success: function (data) {

                    var str = '';
                    for (var k in data['data']) {
                        if (data['data'][k]['article_type'] == 'video') {
                            str += "<a href = 'https://dev-pharma.medlab.co/articles_content/" + data['data'][k]['id'] +
                                "' class='animsition-link'>" + "<div class='blog-box-3'>" +
                                "<div class='blog-box-1 grey-section'>" +
                                "<iframe src='" + data['data'][k]['video_link'] + "' width='100%' height='auto' allowfullscreen></iframe>" +
                                "<h4>" + data['data'][k]['title'] + "</h4>" +
                                "<p>" + data['data'][k]['preview_text'] + "</p>" +
                                "<p>" + data['data'][k]['date'] + "</p>" +
                                "<div class='link'>&#xf178;</div>" +
                                "</div>" +
                                "</div>" +
                                "</a>";


                        } else {
                            str += "<a href = 'https://dev-pharma.medlab.co/articles_content/" + data['data'][k]['id'] +
                                "' class='animsition-link'>" +
                                "<div class='blog-box-3'>" +
                                "<div class='blog-box-1 grey-section'>" +
                                "<img src='https://cdn.medlab.co/www/Images/" + data['data'][k]['title_img'] + "'" + ' ' +
                                "alt=" + data['data'][k]['title_alt'] + ">" +
                                "<h4>" + data['data'][k]['title'] + "</h4>" +
                                "<p>" + data['data'][k]['preview_text'] + "</p>" +
                                "<p>" + data['data'][k]['date'] + "</p>" +
                                "<div class='link'>&#xf178;</div>" +
                                "</div>" +
                                "</div>" +
                                "</a>";
                        }
                        $('#blog-grid-masonry').empty().html(str);

                    }
                    sessionStorage.setItem('current_page', data['current_page'])
                    sessionStorage.setItem('last_page', data['last_page'])
                    if (data['current_page'] == 1) {
                        document.getElementById('pre').style.display = 'none';
                    } else {
                        document.getElementById('pre').style.display = 'unset';
                    }
                    if (data['current_page'] == data['last_page']) {
                        document.getElementById('next').style.display = 'none';
                    } else {
                        document.getElementById('next').style.display = 'unset';
                    }
                }
            })
        }

    </script>
    <script type="text/javascript" src="{{ asset('/js/jquery.themepunch.tools.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.themepunch.revolution.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.easing.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.hidescroll.min.js') }}"></script>
    <script type="text/javascript">
        $('.header-top').hidescroll();
    </script>
    <script type="text/javascript" src="{{ asset('/js/smoothScroll.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/isotope.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.counterup.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/waypoints.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/scrollReveal.js') }}"></script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            window.scrollReveal = new scrollReveal();
        })(jQuery);
    </script>
    <script type="text/javascript" src="{{ asset('/js/jquery.colorbox.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/owl.carousel.min.js') }}"></script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            jQuery(document).ready(function () {
                var offset = 450;
                var duration = 500;
                jQuery(window).scroll(function () {
                    if (jQuery(this).scrollTop() > offset) {
                        jQuery('.scroll-to-top').fadeIn(duration);
                    } else {
                        jQuery('.scroll-to-top').fadeOut(duration);
                    }
                });

                jQuery('.scroll-to-top').click(function (event) {
                    event.preventDefault();
                    jQuery('html, body').animate({scrollTop: 0}, duration);
                    return false;
                })
            });
        })(jQuery);
    </script>
    <script type="text/javascript" src="{{ asset('/js/styleswitcher.js') }}"></script>
    {{--    <script type="text/javascript" src="{{ asset('/js/custom-shop-home-1.js') }}"></script>--}}
    <script type="text/javascript" src="{{ asset('js/custom-shop-item.js') }}"></script>
    {{--    <script type="text/javascript" src="{{ asset('js/custom-tabs.js') }}"></script>--}}
    <script type="text/javascript" src="{{ asset('js/smk-accordion.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('/DataTables/datatables.min.css') }}"/>

    <script type="text/javascript" src="{{ asset('/DataTables/datatables.min.js') }}"></script>
    {{--    <script type="text/javascript" src="{{ asset('/js/custom-blog-grid-left.js') }}"></script>--}}






    <!-- End Document
================================================== -->
@endsection
