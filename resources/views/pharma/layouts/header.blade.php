@include('pharma.layouts.app1')


{{--</head>--}}
{{--<body>--}}
<!-- Primary Page Layout
================================================== -->

<div class="animsition">

    <!-- Switch Panel -->
{{--    <div id="switch">--}}
{{--        <div class="content-switcher">--}}
{{--            <p>Color Options:</p>--}}
{{--            <ul class="header">--}}
{{--                <li><a href="#" onClick="setActiveStyleSheet('1'); return false;" class="button color switch"--}}
{{--                       style="background-color:#124a89"></a></li>--}}
{{--                <li><a href="#" onClick="setActiveStyleSheet('2'); return false;" class="button color switch"--}}
{{--                       style="background-color:#9b59b6"></a></li>--}}
{{--                <li><a href="#" onClick="setActiveStyleSheet('3'); return false;" class="button color switch"--}}
{{--                       style="background-color:#2ecc71"></a></li>--}}
{{--                <li><a href="#" onClick="setActiveStyleSheet('4'); return false;" class="button color switch"--}}
{{--                       style="background-color:#e74c3c"></a></li>--}}
{{--                <li><a href="#" onClick="setActiveStyleSheet('5'); return false;" class="button color switch"--}}
{{--                       style="background-color:#34495e"></a></li>--}}
{{--                <li><a href="#" onClick="setActiveStyleSheet('6'); return false;" class="button color switch"--}}
{{--                       style="background-color:#f1c40f"></a></li>--}}
{{--                <li><a href="#" onClick="setActiveStyleSheet('7'); return false;" class="button color switch"--}}
{{--                       style="background-color:#124a89"></a></li>--}}
{{--            </ul>--}}
{{--            <div class="clear"></div>--}}
{{--            <div id="hide">--}}
{{--                <img src="{{ asset('/images/close.png')}}" alt=""/>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <div id="show" style="display: block;">--}}
{{--        <div id="setting"></div>--}}
{{--    </div>--}}
<!-- Switch Panel -->


    <!-- MENU
    ================================================== -->

    <div class="header-top">

        <header id="header" class="cd-main-header">
            <a class="cd-logo animsition-link" href="/"><img src="{{ asset('/images/logo.png')}}" alt="Logo"></a>

            <ul class="cd-header-buttons">
                <li><a class="cd-search-trigger" href="#cd-search"><span></span></a></li>
                <li><a id="nav" class="cd-nav-trigger" href="#cd-primary-nav"><span></span></a></li>
            </ul> <!-- cd-header-buttons -->
        </header>
        <nav class="cd-nav">
            <ul id="cd-primary-nav" class="cd-primary-nav is-fixed" style="z-index: 2;">
                {{--                --}}{{--                                <li>--}}
                {{--                --}}{{--                                    <a href="/" class="animsition-link" style="font-size: large">@lang('trs.home')</a>--}}
                {{--                --}}{{--                                </li>--}}
                {{--                <li class="has-children">--}}
                {{--                    <a href="/" style="font-size: large">@lang('trs.about medlab')</a>--}}

                {{--                    <ul class="cd-secondary-nav is-hidden">--}}
                {{--                        <li class="go-back"><a href="#">@lang('trs.menu')</a></li>--}}
                {{--                        <li class="see-all"><a href="/">@lang('trs.welcome to medlab')</a></li>--}}
                {{--                        <li class="has-children">--}}
                {{--                            <a href="/">@lang('trs.about us')</a>--}}

                {{--                            <ul class="is-hidden">--}}
                {{--                                <li class="go-back"><a href="/">@lang('trs.about us')</a></li>--}}
                {{--                                @if(Auth::check() &&(Auth::user()->is_admin || array_key_exists('about-1',array_column(Auth::user()->routers()->get()->toArray(),NULL,'router'))))--}}


                {{--                                    <li class="has-children">--}}
                {{--                                        <a href="#0">@lang('trs.about medlab')</a>--}}

                {{--                                        <ul class="is-hidden">--}}
                {{--                                            <li class="go-back"><a href="#">Home</a></li>--}}
                {{--                                            <li><a href="corporate-home-1.html" class="animsition-link">Corporate Layout--}}
                {{--                                                    1</a></li>--}}
                {{--                                            <li><a href="corporate-home-2.html" class="animsition-link">Corporate Layout--}}
                {{--                                                    2</a></li>--}}
                {{--                                            <li><a href="corporate-home-3.html" class="animsition-link">Corporate Layout--}}
                {{--                                                    3</a></li>--}}
                {{--                                        </ul>--}}
                {{--                                    </li>--}}
                {{--                                    <li><a href="{{ route('about-1') }}"--}}
                {{--                                           class="animsition-link">@lang('trs.about us')</a>--}}
                {{--                                    </li>--}}
                {{--                                @endif--}}
                {{--                                <li><a href="{{ route('executive_team') }}"--}}
                {{--                                       class="animsition-link">@lang('trs.executive team')</a>--}}
                {{--                                </li>--}}
                {{--                                <li><a href="{{ route('consulting_team') }}"--}}
                {{--                                       class="animsition-link">@lang('trs.consulting team')</a></li>--}}
                {{--                                <li><a href="{{ route('scientific_team') }}"--}}
                {{--                                       class="animsition-link">@lang('trs.scientific team')</a></li>--}}
                {{--                                <li><a href="{{ route('board_of_directors') }}"--}}
                {{--                                       class="animsition-link">@lang('trs.board of directors')</a></li>--}}
                {{--                                <li><a href="{{ route('careers') }}" class="animsition-link">@lang('trs.careers')</a>--}}
                {{--                                </li>--}}
                {{--                                <li><a href="parallax-home.html" class="animsition-link">@lang('trs.contact us')</a>--}}
                {{--                                </li>--}}
                {{--                            </ul>--}}

                {{--                        </li>--}}
                {{--                        <li class="has-children">--}}
                {{--                            <a href="/">@lang('trs.research')</a>--}}
                {{--                            <ul class="is-hidden">--}}
                {{--                                <li class="has-children">--}}
                {{--                                    <a href="#0">@lang('trs.about medlab')</a>--}}

                {{--                                    <ul class="is-hidden">--}}
                {{--                                        <li class="go-back"><a href="#">Home</a></li>--}}
                {{--                                        <li><a href="corporate-home-1.html" class="animsition-link">Corporate Layout--}}
                {{--                                                1</a></li>--}}
                {{--                                        <li><a href="corporate-home-2.html" class="animsition-link">Corporate Layout--}}
                {{--                                                2</a></li>--}}
                {{--                                        <li><a href="corporate-home-3.html" class="animsition-link">Corporate Layout--}}
                {{--                                                3</a></li>--}}
                {{--                                    </ul>--}}
                {{--                                </li>--}}
                {{--                                <li><a href="parallax-home.html" class="animsition-link">@lang('trs.our laboratory')</a>--}}
                {{--                                </li>--}}
                {{--                                <li><a href="{{ route('patents') }}" class="animsition-link">@lang('trs.patents')</a>--}}
                {{--                                </li>--}}
                {{--                                <li><a href="{{ route('clinical_trials') }}"--}}
                {{--                                       class="animsition-link">@lang('trs.clinical trials')</a></li>--}}
                {{--                                <li><a href="{{ route('delivery_platforms') }}"--}}
                {{--                                       class="animsition-link">@lang('trs.delivery platforms')</a></li>--}}
                {{--                                <li><a href="{{ route('research') }}" class="animsition-link">@lang('trs.research')</a>--}}
                {{--                                </li>--}}
                {{--                            </ul>--}}
                {{--                        </li>--}}
                {{--                        <li class="has-children">--}}
                {{--                            <a href="/">@lang('trs.education')</a>--}}
                {{--                            <ul class="is-hidden">--}}
                {{--                                <li class="has-children">--}}
                {{--                                    <a href="#0">@lang('trs.about medlab')</a>--}}

                {{--                                    <ul class="is-hidden">--}}
                {{--                                        <li class="go-back"><a href="#">Home</a></li>--}}
                {{--                                        <li><a href="corporate-home-1.html" class="animsition-link">Corporate Layout--}}
                {{--                                                1</a></li>--}}
                {{--                                        <li><a href="corporate-home-2.html" class="animsition-link">Corporate Layout--}}
                {{--                                                2</a></li>--}}
                {{--                                        <li><a href="corporate-home-3.html" class="animsition-link">Corporate Layout--}}
                {{--                                                3</a></li>--}}
                {{--                                    </ul>--}}
                {{--                                </li>--}}
                {{--                                <li><a href="{{ route('efficacy_magazine') }}"--}}
                {{--                                       class="animsition-link">@lang('trs.efficacy magazine')</a></li>--}}
                {{--                                <li><a href="{{ route('www.articles') }}" class="animsition-link">@lang('trs.articles')</a>--}}
                {{--                                </li>--}}
                {{--                                <li><a href="{{ route('webinars') }}" class="animsition-link">@lang('trs.webinars')</a>--}}
                {{--                                </li>--}}
                {{--                                <li><a href="{{ route('publications') }}"--}}
                {{--                                       class="animsition-link">@lang('trs.our publications')</a></li>--}}
                {{--                                <li><a href="{{ route('product_manual') }}"--}}
                {{--                                       class="animsition-link">@lang('trs.product manual')</a>--}}
                {{--                                </li>--}}
                {{--                                <li><a href="{{ route('children_dosage') }}"--}}
                {{--                                       class="animsition-link">@lang('trs.children dosage')</a></li>--}}
                {{--                                <li><a href="{{ route('marketing_material') }}"--}}
                {{--                                       class="animsition-link">@lang('trs.marketing material')</a></li>--}}
                {{--                            </ul>--}}
                {{--                        </li>--}}

                {{--                        <li class="has-children">--}}
                {{--                            <a href="/">@lang('trs.investors')</a>--}}
                {{--                            <ul class="is-hidden">--}}
                {{--                                <li class="has-children">--}}
                {{--                                    <a href="#0">@lang('trs.about medlab')</a>--}}

                {{--                                    <ul class="is-hidden">--}}
                {{--                                        <li class="go-back"><a href="#">Home</a></li>--}}
                {{--                                        <li><a href="corporate-home-1.html" class="animsition-link">Corporate Layout--}}
                {{--                                                1</a></li>--}}
                {{--                                        <li><a href="corporate-home-2.html" class="animsition-link">Corporate Layout--}}
                {{--                                                2</a></li>--}}
                {{--                                        <li><a href="corporate-home-3.html" class="animsition-link">Corporate Layout--}}
                {{--                                                3</a></li>--}}
                {{--                                    </ul>--}}
                {{--                                </li>--}}
                {{--                                <li><a href="{{ route('share_price') }}"--}}
                {{--                                       class="animsition-link">@lang('trs.share price')</a></li>--}}
                {{--                                <li><a href="{{ route('corporate_governance') }}"--}}
                {{--                                       class="animsition-link">@lang('trs.corporate governance')</a></li>--}}
                {{--                                <li><a href="{{ route('asx_announcement') }}"--}}
                {{--                                       class="animsition-link">@lang('trs.asx announcement')</a></li>--}}
                {{--                                <li><a href="{{ route('asx_announcement') }}"--}}
                {{--                                       class="animsition-link">@lang('trs.asx announcements')</a></li>--}}
                {{--                                <li><a href="{{ route('corporate_governance') }}"--}}
                {{--                                       class="animsition-link">@lang('trs.corporate governance')</a></li>--}}
                {{--                                <li><a href="{{ route('share_price') }}"--}}
                {{--                                       class="animsition-link">@lang('trs.share price')</a></li>--}}
                {{--                            </ul>--}}
                {{--                        </li>--}}


                {{--                                <li class="has-children">--}}
                {{--                                    <a href="#0">Home Portfolio</a>--}}

                {{--                                    <ul class="is-hidden">--}}
                {{--                                        <li class="go-back"><a href="#">Home</a></li>--}}
                {{--                                        <li><a href="portfolio-home-1.html" class="animsition-link">Portfolio Layout--}}
                {{--                                                1</a></li>--}}
                {{--                                        <li><a href="portfolio-home-2.html" class="animsition-link">Portfolio Layout--}}
                {{--                                                2</a></li>--}}
                {{--                                        <li><a href="masonry-home.html" class="animsition-link">Masonry</a></li>--}}
                {{--                                        <li><a href="ajax-home.html" class="animsition-link">Ajax</a></li>--}}
                {{--                                    </ul>--}}
                {{--                                </li>--}}
                {{--                                <li class="has-children">--}}
                {{--                                    <a href="#0">Home Blog</a>--}}

                {{--                                    <ul class="is-hidden">--}}
                {{--                                        <li class="go-back"><a href="#">Home</a></li>--}}
                {{--                                        <li><a href="blog-home-1.html" class="animsition-link">Blog Layout 1</a></li>--}}
                {{--                                        <li><a href="blog-home-2.html" class="animsition-link">Blog Layout 2</a></li>--}}
                {{--                                        <li><a href="blog-home-3.html" class="animsition-link">Blog Layout 3</a></li>--}}
                {{--                                    </ul>--}}
                {{--                                </li>--}}
                {{--                                <li class="has-children">--}}
                {{--                                    <a href="#0">Home Shop</a>--}}

                {{--                                    <ul class="is-hidden">--}}
                {{--                                        <li class="go-back"><a href="#">Home</a></li>--}}
                {{--                                        <li><a href="shop-home-1.html" class="animsition-link">Shop Layout 1</a></li>--}}
                {{--                                        <li><a href="shop-home-2.html" class="animsition-link">Shop Layout 2</a></li>--}}
                {{--                                        <li><a href="shop-home-3.html" class="animsition-link">Shop Layout 3</a></li>--}}
                {{--                                    </ul>--}}
                {{--                                </li>--}}
                {{--                                <li class="has-children">--}}
                {{--                                    <a href="#0">Home Fullscreen</a>--}}

                {{--                                    <ul class="is-hidden">--}}
                {{--                                        <li class="go-back"><a href="#">Home</a></li>--}}
                {{--                                        <li><a href="image-home.html" class="animsition-link">Fullscreen Image</a></li>--}}
                {{--                                        <li><a href="slider-home.html" class="animsition-link">Fullscreen Slider</a>--}}
                {{--                                        </li>--}}
                {{--                                        <li><a href="video-home.html" class="animsition-link">Fullscreen Video</a></li>--}}
                {{--                                        <li><a href="youtube-video-home.html" class="animsition-link">Fullscreen YouTube--}}
                {{--                                                Video</a></li>--}}
                {{--                                    </ul>--}}
                {{--                                </li>--}}
                {{--                                <li><a href="parallax-home.html" class="animsition-link">Home Parallax</a></li>--}}
                {{--                                <li class="has-children">--}}
                {{--                                    <a href="#0">One Page</a>--}}

                {{--                                    <ul class="is-hidden">--}}
                {{--                                        <li class="go-back"><a href="#">Home</a></li>--}}
                {{--                                        <li><a href="one-page-home.html" class="animsition-link">One Page</a></li>--}}
                {{--                                        <li><a href="one-page-home-2.html" class="animsition-link">One Page 3D menu</a>--}}
                {{--                                        </li>--}}
                {{--                                    </ul>--}}
                {{--                                </li>--}}
                {{--                                <li><a href="showcase-home-1.html" class="animsition-link">Home App Showcase</a></li>--}}


                {{--                        <li class="has-children">--}}
                {{--                            <a href="#">About and Services</a>--}}

                {{--                            <ul class="is-hidden">--}}
                {{--                                <li class="go-back"><a href="/">Pages</a></li>--}}
                {{--                                <li><a href="{{route('about-1')}}" class="animsition-link">About Layout 1</a></li>--}}
                {{--                                <li><a href="about-2.html" class="animsition-link">About Layout 2</a></li>--}}
                {{--                                <li><a href="about-me.html" class="animsition-link">About Me</a></li>--}}
                {{--                                <li><a href="services-1.html" class="animsition-link">Services Layout 1</a></li>--}}
                {{--                                <li><a href="services-2.html" class="animsition-link">Services Layout 2</a></li>--}}
                {{--                                <li><a href="team.html" class="animsition-link">Meet The Team</a></li>--}}
                {{--                                <li><a href="office.html" class="animsition-link">Our Office</a></li>--}}
                {{--                                <li><a href="faq.html" class="animsition-link">FAQ</a></li>--}}
                {{--                            </ul>--}}
                {{--                        </li>--}}

                {{--                        <li class="has-children">--}}
                {{--                            <a href="#">Blog</a>--}}

                {{--                            <ul class="is-hidden">--}}
                {{--                                <li class="go-back"><a href="/">Pages</a></li>--}}
                {{--                                <li class="has-children">--}}
                {{--                                    <a href="/">Grid Layouts</a>--}}

                {{--                                    <ul class="is-hidden">--}}
                {{--                                        <li class="go-back"><a href="/">Blog</a></li>--}}
                {{--                                        <li><a href="blog-grid-full.html" class="animsition-link">Grid Full Width</a>--}}
                {{--                                        </li>--}}
                {{--                                        <li><a href="blog-grid-left.html" class="animsition-link">Grid Left Sidebar</a>--}}
                {{--                                        </li>--}}
                {{--                                        <li><a href="blog-grid-right.html" class="animsition-link">Grid Right--}}
                {{--                                                Sidebar</a></li>--}}
                {{--                                    </ul>--}}
                {{--                                </li>--}}
                {{--                                <li class="has-children">--}}
                {{--                                    <a href="/">Large Image Layouts</a>--}}

                {{--                                    <ul class="is-hidden">--}}
                {{--                                        <li class="go-back"><a href="/">Blog</a></li>--}}
                {{--                                        <li><a href="blog-large-full.html" class="animsition-link">Large Image Full--}}
                {{--                                                Width</a></li>--}}
                {{--                                        <li><a href="blog-large-left.html" class="animsition-link">Large Image Left--}}
                {{--                                                Sidebar</a></li>--}}
                {{--                                        <li><a href="blog-large-right.html" class="animsition-link">Large Image Right--}}
                {{--                                                Sidebar</a></li>--}}
                {{--                                    </ul>--}}
                {{--                                </li>--}}
                {{--                                <li><a href="image-post.html" class="animsition-link">Image Post</a></li>--}}
                {{--                                <li><a href="slider-post.html" class="animsition-link">Slider Post</a></li>--}}
                {{--                                <li><a href="video-post.html" class="animsition-link">Video Post</a></li>--}}
                {{--                                <li><a href="audio-post.html" class="animsition-link">Audio Post</a></li>--}}
                {{--                                <li><a href="link-post.html" class="animsition-link">Link Post</a></li>--}}
                {{--                                <li><a href="quote-post.html" class="animsition-link">Quote Post</a></li>--}}
                {{--                            </ul>--}}
                {{--                        </li>--}}

                {{--                        <li class="has-children">--}}
                {{--                            <a href="#">Portfolio</a>--}}

                {{--                            <ul class="is-hidden">--}}
                {{--                                <li class="go-back"><a href="/">Pages</a></li>--}}
                {{--                                <li class="has-children">--}}
                {{--                                    <a href="/">Grid Layouts</a>--}}

                {{--                                    <ul class="is-hidden">--}}
                {{--                                        <li class="go-back"><a href="/">Portfolio</a></li>--}}
                {{--                                        <li><a href="grid-2col.html" class="animsition-link">2 Columns</a></li>--}}
                {{--                                        <li><a href="grid-3col.html" class="animsition-link">3 Columns</a></li>--}}
                {{--                                        <li><a href="grid-4col.html" class="animsition-link">4 Columns</a></li>--}}
                {{--                                    </ul>--}}
                {{--                                </li>--}}
                {{--                                <li class="has-children">--}}
                {{--                                    <a href="/">Masonry Layouts</a>--}}

                {{--                                    <ul class="is-hidden">--}}
                {{--                                        <li class="go-back"><a href="/">Portfolio</a></li>--}}
                {{--                                        <li><a href="masonry-mixed.html" class="animsition-link">Mixed Columns</a></li>--}}
                {{--                                        <li><a href="masonry-2col.html" class="animsition-link">2 Columns</a></li>--}}
                {{--                                        <li><a href="masonry-3col.html" class="animsition-link">3 Columns</a></li>--}}
                {{--                                        <li><a href="masonry-4col.html" class="animsition-link">4 Columns</a></li>--}}
                {{--                                    </ul>--}}
                {{--                                </li>--}}
                {{--                                <li class="has-children">--}}
                {{--                                    <a href="/">Ajax Portfolio</a>--}}

                {{--                                    <ul class="is-hidden">--}}
                {{--                                        <li class="go-back"><a href="/">Portfolio</a></li>--}}
                {{--                                        <li><a href="ajax-masonry-mixed.html" class="animsition-link">Mixed Columns</a>--}}
                {{--                                        </li>--}}
                {{--                                        <li><a href="ajax-masonry-2col.html" class="animsition-link">2 Columns--}}
                {{--                                                Masonry</a></li>--}}
                {{--                                        <li><a href="ajax-masonry-3col.html" class="animsition-link">3 Columns--}}
                {{--                                                Masonry</a></li>--}}
                {{--                                        <li><a href="ajax-masonry-4col.html" class="animsition-link">4 Columns--}}
                {{--                                                Masonry</a></li>--}}
                {{--                                        <li><a href="ajax-grid-2col.html" class="animsition-link">2 Columns Grid</a>--}}
                {{--                                        </li>--}}
                {{--                                        <li><a href="ajax-grid-3col.html" class="animsition-link">3 Columns Grid</a>--}}
                {{--                                        </li>--}}
                {{--                                        <li><a href="ajax-grid-4col.html" class="animsition-link">4 Columns Grid</a>--}}
                {{--                                        </li>--}}
                {{--                                    </ul>--}}
                {{--                                </li>--}}
                {{--                                <li><a href="slider-project.html" class="animsition-link">Slider Project</a></li>--}}
                {{--                                <li><a href="video-project.html" class="animsition-link">Video Project</a></li>--}}
                {{--                                <li><a href="gallery-project.html" class="animsition-link">Gallery Project</a></li>--}}
                {{--                                <li><a href="parallax-project.html" class="animsition-link">Parallax Project</a></li>--}}
                {{--                                <li><a href="fullscreen-project.html" class="animsition-link">Fullscreen Project</a>--}}
                {{--                                </li>--}}
                {{--                            </ul>--}}
                {{--                        </li>--}}
                {{--                    </ul>--}}
                {{--                </li>--}}
                {{--                <li class="has-children">--}}
                {{--                    <a href="#">@lang('trs.products')</a>--}}

                {{--                    <ul class="cd-nav-gallery is-hidden">--}}
                {{--                        <li class="go-back"><a href="#">@lang('trs.menu')</a></li>--}}
                {{--                        <li class="see-all"><a href="shop-all.html" class="animsition-link">Browse All</a></li>--}}
                {{--                        <li>--}}
                {{--                            <a class="cd-nav-item animsition-link" href="shop-1.html">--}}
                {{--                                <img src="{{ asset('/images/shop1.jpg') }}" alt="Product Image">--}}
                {{--                                <h3>@lang('trs.nutraceuticals')</h3>--}}
                {{--                            </a>--}}
                {{--                        </li>--}}

                {{--                        <li>--}}
                {{--                            <a class="cd-nav-item animsition-link" href="shop-2.html">--}}
                {{--                                <img src="{{ asset('/images/shop2.jpg') }}" alt="Product Image">--}}
                {{--                                <h3>@lang('trs.product name')</h3>--}}
                {{--                            </a>--}}
                {{--                        </li>--}}

                {{--                        <li>--}}
                {{--                            <a class="cd-nav-item animsition-link" href="shop-3.html">--}}
                {{--                                <img src="{{ asset('/images/shop3.jpg') }}" alt="Product Image">--}}
                {{--                                <h3>@lang('trs.category')</h3>--}}
                {{--                            </a>--}}
                {{--                        </li>--}}

                {{--                        <li>--}}
                {{--                            <a class="cd-nav-item animsition-link" href="shop-4.html">--}}
                {{--                                <img src="{{ asset('/images/shop4.jpg') }}" alt="Product Image">--}}
                {{--                                <h3>@lang('trs.ingredients')</h3>--}}
                {{--                            </a>--}}
                {{--                        </li>--}}

                {{--                        <li>--}}
                {{--                            <a class="cd-nav-item animsition-link" href="shop-4.html">--}}
                {{--                                <img src="{{ asset('/images/shop4.jpg') }}" alt="Product Image">--}}
                {{--                                <h3>@lang('trs.listed medicine')</h3>--}}
                {{--                            </a>--}}
                {{--                        </li>--}}

                {{--                        <li>--}}
                {{--                            <a class="cd-nav-item animsition-link" href="shop-4.html">--}}
                {{--                                <img src="{{ asset('/images/shop4.jpg') }}" alt="Product Image">--}}
                {{--                                <h3>@lang('trs.nanabidial')</h3>--}}
                {{--                            </a>--}}
                {{--                        </li>--}}
                {{--                        --}}
                {{--                        <li>--}}
                {{--                            <a class="cd-nav-item animsition-link" href="shop-4.html">--}}
                {{--                                <img src="{{ asset('/images/shop4.jpg') }}" alt="Product Image">--}}
                {{--                                <h3>@lang('trs.nanabidial')</h3>--}}
                {{--                            </a>--}}
                {{--                        </li>--}}
                {{--                    </ul>--}}
                {{--                </li>--}}

                {{--                <li class="has-children">--}}
                {{--                    <a href="#" style="font-size: large">@lang('trs.products')</a>--}}

                {{--                    <ul class="cd-nav-gallery is-hidden">--}}
                {{--                        <li class="go-back"><a href="#">@lang('trs.menu')</a></li>--}}
                {{--                        <li class="see-all"><a href="shop-all.html" class="animsition-link">@lang('trs.browse all')</a>--}}
                {{--                        </li>--}}
                {{--                        <li>--}}
                {{--                            <a class="cd-nav-item animsition-link">--}}
                {{--                                <img src="{{ asset('/images/shop1.jpg') }}" alt="Product Image">--}}
                {{--                                <h3 style="margin: 70px">@lang('trs.nutraceuticals')</h3>--}}
                {{--                            </a>--}}
                {{--                        </li>--}}

                {{--                        <li>--}}
                {{--                            <a class="cd-nav-item animsition-link" href="{{ route('shop_name') }}">--}}
                {{--                                <img src="{{ asset('/images/shop2.jpg') }}" alt="Product Image">--}}
                {{--                                <h3>@lang('trs.product name')</h3>--}}
                {{--                            </a>--}}
                {{--                        </li>--}}

                {{--                        <li>--}}
                {{--                            <a class="cd-nav-item animsition-link" href="shop-3.html">--}}
                {{--                                <img src="{{ asset('/images/shop3.jpg') }}" alt="Product Image">--}}
                {{--                                <h3>@lang('trs.category')</h3>--}}
                {{--                            </a>--}}
                {{--                        </li>--}}

                {{--                        <li>--}}
                {{--                            <a class="cd-nav-item animsition-link" href="shop-4.html">--}}
                {{--                                <img src="{{ asset('/images/shop4.jpg') }}" alt="Product Image">--}}
                {{--                                <h3>@lang('trs.ingredients')</h3>--}}
                {{--                            </a>--}}
                {{--                        </li>--}}
                {{--                        <li>--}}
                {{--                            <a class="cd-nav-item animsition-link" href="shop-2.html">--}}
                {{--                                <img src="{{ asset('/images/shop2.jpg') }}" alt="Product Image">--}}
                {{--                                <h3 style="margin: 70px">@lang('trs.listed medicine')</h3>--}}
                {{--                            </a>--}}
                {{--                        </li>--}}

                {{--                        <li>--}}
                {{--                            <a class="cd-nav-item animsition-link" href="shop-3.html">--}}
                {{--                                <img src="{{ asset('/images/shop3.jpg') }}" alt="Product Image">--}}
                {{--                                <h3>@lang('trs.nanabis')</h3>--}}
                {{--                            </a>--}}
                {{--                        </li>--}}

                {{--                        <li>--}}
                {{--                            <a class="cd-nav-item animsition-link" href="shop-4.html">--}}
                {{--                                <img src="{{ asset('/images/shop4.jpg') }}" alt="Product Image">--}}
                {{--                                <h3>@lang('trs.nanabidial')</h3>--}}
                {{--                            </a>--}}
                {{--                        </li>--}}
                {{--                    </ul>--}}
                {{--                </li>--}}
{{--                <li class="has-children">--}}
{{--                    <a href="#" style="font-size: large">@lang('trs.about')</a>--}}
{{--                    <ul class="cd-nav-icons is-hidden">--}}
{{--                        <li class="go-back"><a href="#">@lang('trs.menu')</a></li>--}}
{{--                        <li class="see-all"><a href="/" class="animsition-link">@lang('trs.welcome to medlab')</a></li>--}}
{{--                        <li>--}}
{{--                            <a class="cd-nav-item item-3 animsition-link" href="{{ route('about') }}">--}}
{{--                                <h3>@lang('trs.about us')</h3>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a class="cd-nav-item item-12 animsition-link" href="{{ route('scientific_team') }}">--}}
{{--                                <h3>@lang('trs.scientific team')</h3>--}}
{{--                            </a>--}}
{{--                        </li>--}}

{{--                        <li>--}}
{{--                            <a class="cd-nav-item item-7 animsition-link" href="{{ route('consulting_team') }}">--}}
{{--                                <h3>@lang('trs.scientific consulting team')</h3>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a class="cd-nav-item item-6 animsition-link" href="{{ route('contact') }}">--}}
{{--                                <h3>@lang('trs.contact us')</h3>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                </li>--}}
                <li>
                    <a href="{{ route('pharma.products') }}" class="animsition-link" style="font-size: large">@lang('trs.products')</a>
                </li>
                <li>
                    <a href="{{ route('pharma.articles') }}" class="animsition-link"
                       style="font-size: large">@lang('trs.education')</a>
                </li>
{{--                <li class="has-children">--}}
{{--                    <a href="#" style="font-size: large">@lang('trs.products')</a>--}}

{{--                    <ul class="cd-nav-icons is-hidden">--}}
{{--                        <li class="go-back"><a href="#">@lang('trs.menu')</a></li>--}}
{{--                        <li class="see-all"><a href="more-shortcodes.html"--}}
{{--                                               class="animsition-link">@lang('trs.welcome to medlab')</a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a class="cd-nav-item item-10 animsition-link" href="{{ route('pharma.products') }}">--}}
{{--                                <h3>@lang('products')</h3>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a class="cd-nav-item item-12 animsition-link" href="{{ route('pharma.products_by_category') }}">--}}
{{--                                <h3>Products By Category</h3>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a class="cd-nav-item item-13 animsition-link" href="{{ route('pharma.products_by_dietary') }}">--}}
{{--                                <h3>Products By Dietary</h3>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                </li>--}}

{{--                <li class="has-children">--}}
{{--                    <a href="#" style="font-size: large">@lang('trs.research')</a>--}}
{{--                    <ul class="cd-nav-icons is-hidden">--}}
{{--                        <li class="go-back"><a href="#">@lang('trs.menu')</a></li>--}}
{{--                        <li class="see-all"><a href="/" class="animsition-link">@lang('trs.welcome to medlab')</a></li>--}}

{{--                        <li>--}}
{{--                            <a class="cd-nav-item item-4 animsition-link" href="{{ route('delivery_platforms') }}">--}}
{{--                                <h3>@lang('trs.delivery platforms')</h3>--}}
{{--                            </a>--}}
{{--                        </li>--}}

{{--                        <li>--}}
{{--                            <a class="cd-nav-item item-5 animsition-link" href="{{ route('nanacelle_technology') }}">--}}
{{--                                <h3>@lang('trs.nanacelle technology')</h3>--}}
{{--                            </a>--}}
{{--                        </li>--}}

{{--                        <li>--}}
{{--                            <a class="cd-nav-item item-6 animsition-link" href="{{ route('clinical_trials') }}">--}}
{{--                                <h3>@lang('trs.clinical trials')</h3>--}}
{{--                            </a>--}}
{{--                        </li>--}}

{{--                        <li>--}}
{{--                            <a class="cd-nav-item item-7 animsition-link" href="{{ route('pc2') }}">--}}
{{--                                <h3>@lang('trs.pc2')</h3>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a class="cd-nav-item item-8 animsition-link" href="{{ route('publications') }}">--}}
{{--                                <h3>@lang('trs.publications')</h3>--}}
{{--                            </a>--}}
{{--                        </li>--}}


{{--                    </ul>--}}
{{--                </li>--}}

{{--                <li>--}}
{{--                    <a href="{{ route('www.articles') }}" class="animsition-link" style="font-size: large">@lang('trs.education')</a>--}}
{{--                </li>--}}


                @if(Auth::guard('practitioner')->check())
                    <li class="has-children">
                        <a href="#" style="font-size: large"><i
                                class="fa fa-fw">&#xf090;</i>Hi&nbsp;{{ Auth::guard('practitioner')->user()->first_name }}
                        </a>
                        <ul class="cd-nav-icons is-hidden">
                            <li class="go-back"><a href="#">@lang('trs.menu')</a></li>
                            <li class="see-all"><a href="/" class="animsition-link">@lang('trs.welcome to medlab')</a>
                            </li>
                            <li>
                                <a class="cd-nav-item item-7 animsition-link" href="#">
                                    <h3>Order History</h3>
                                </a>
                            </li>

                            <li>
                                <a class="cd-nav-item item-5 animsition-link" href="#">
                                    <h3>My profile</h3>
                                </a>
                            </li>

                            <li>
                                <a class="cd-nav-item item-6 animsition-link" href="{{ route('logout') }}">
                                    <h3>Logout</h3>
                                </a>
                            </li>
                        </ul>
                    </li>
                @elseif(Auth::guard('patient')->check())
                    <li class="has-children">
                        <a href="#" style="font-size: large"><i
                                class="fa fa-fw">&#xf090;</i>Hi&nbsp;{{ Auth::guard('patient')->user()->first_name }}
                        </a>
                        <ul class="cd-nav-icons is-hidden">
                            <li class="go-back"><a href="#">@lang('trs.menu')</a></li>
                            <li class="see-all"><a href="/" class="animsition-link">@lang('trs.welcome to medlab')</a>
                            </li>
                            <li>
                                <a class="cd-nav-item item-7 animsition-link" href="#">
                                    <h3>Order History</h3>
                                </a>
                            </li>

                            <li>
                                <a class="cd-nav-item item-5 animsition-link" href="#">
                                    <h3>My profile</h3>
                                </a>
                            </li>

                            <li>
                                <a class="cd-nav-item item-6 animsition-link" href="{{ route('logout') }}">
                                    <h3>Logout</h3>
                                </a>
                            </li>
                        </ul>
                    </li>

                @else
                    <li>
                        <a href="#" onClick="open_login()" style="font-size: large"><i
                                class="fa fa-fw">&#xf090;</i>@lang('trs.login')</a>
                    </li>
                @endif
                {{--                                <li class="has-children">--}}
                {{--                                    @if (Session::get('locale', 'en') == 'en')--}}
                {{--                                        <a href="#" style="font-size: large">@lang('trs.en')</a>--}}


                {{--                                        <ul class="cd-nav-gallery is-hidden" style="padding: unset;">--}}
                {{--                                            <li class="go-back"><a href="#">@lang('trs.menu')</a></li>--}}

                {{--                                            <li style="width: 100%">--}}
                {{--                                                <a class="cd-nav-item animsition-link" href="{{ url('locale/cn') }}">--}}
                {{--                                                    <img src="{{ asset('/images/flags-png/chinese.png') }}" alt="Product Image">--}}
                {{--                                                    <h3>@lang('trs.cn')</h3>--}}
                {{--                                                </a>--}}
                {{--                                            </li>--}}

                {{--                                            <li style="width: 100%">--}}
                {{--                                                <a class="cd-nav-item animsition-link" href="{{ url('locale/en') }}">--}}
                {{--                                                    <img src="{{ asset('/images/flags-png/america.png') }}" alt="Product Image">--}}
                {{--                                                    <h3>@lang('trs.en')</h3>--}}
                {{--                                                </a>--}}
                {{--                                            </li>--}}
                {{--                                        </ul>--}}
                {{--                                    @else--}}
                {{--                                        <a href="#" style="font-size: large">@lang('trs.cn')</a>--}}


                {{--                                        <ul class="cd-nav-gallery is-hidden" style="padding: unset;">--}}
                {{--                                            <li class="go-back"><a href="#">@lang('trs.menu')</a></li>--}}

                {{--                                            <li style="width: 100%">--}}
                {{--                                                <a class="cd-nav-item animsition-link" href="{{ url('locale/cn') }}">--}}
                {{--                                                    <img src="{{ asset('/images/flags-png/chinese.png') }}" alt="Product Image">--}}
                {{--                                                    <h3>@lang('trs.cn')</h3>--}}
                {{--                                                </a>--}}
                {{--                                            </li>--}}

                {{--                                            <li style="width: 100%">--}}
                {{--                                                <a class="cd-nav-item animsition-link" href="{{ url('locale/en') }}">--}}
                {{--                                                    <img src="{{ asset('/images/flags-png/america.png') }}" alt="Product Image">--}}
                {{--                                                    <h3>@lang('trs.en')</h3>--}}
                {{--                                                </a>--}}
                {{--                                            </li>--}}
                {{--                                        </ul>--}}
                {{--                                    @endif--}}
                {{--                                </li>--}}


                {{--                <li class="has-children">--}}
                {{--                    @if (Session::get('locale', 'en') == 'en')--}}
                {{--                        <a href="#" style="font-size: large">@lang('trs.en')</a>--}}
                {{--                    @else--}}
                {{--                        <a href="#" style="font-size: large">@lang('trs.cn')</a>--}}
                {{--                    @endif--}}
                {{--                    <ul class="cd-nav-icons is-hidden">--}}
                {{--                        <li class="go-back"><a href="#">@lang('trs.menu')</a></li>--}}
                {{--                        <li class="see-all"><a href="more-shortcodes.html"--}}
                {{--                                               class="animsition-link">@lang('trs.welcome to medlab')</a>--}}
                {{--                        </li>--}}
                {{--                        <li>--}}
                {{--                            <a class="cd-nav-item item-2 animsition-link" href="{{ url('locale/en') }}">--}}
                {{--                                <h3>@lang('trs.en')</h3>--}}
                {{--                                <p>This is the page description</p>--}}
                {{--                            </a>--}}
                {{--                        </li>--}}

                {{--                        <li>--}}
                {{--                            <a class="cd-nav-item item-1 animsition-link" href="{{ url('locale/cn') }}">--}}
                {{--                                <h3>@lang('trs.cn')</h3>--}}
                {{--                                <p>This is the page description</p>--}}
                {{--                            </a>--}}
                {{--                        </li>--}}
                {{--                    </ul>--}}
                {{--                </li>--}}


                {{--                                        <li>--}}
                {{--                                            <a class="cd-nav-item item-3 animsition-link" href="counters.html">--}}
                {{--                                                <h3>Counters</h3>--}}
                {{--                                                <p>This is the page description</p>--}}
                {{--                                            </a>--}}
                {{--                                        </li>--}}

                {{--                                        <li>--}}
                {{--                                            <a class="cd-nav-item item-4 animsition-link" href="carousels.html">--}}
                {{--                                                <h3>Carousels</h3>--}}
                {{--                                                <p>This is the page description</p>--}}
                {{--                                            </a>--}}
                {{--                                        </li>--}}

                {{--                                        <li>--}}
                {{--                                            <a class="cd-nav-item item-5 animsition-link" href="media.html">--}}
                {{--                                                <h3>Media</h3>--}}
                {{--                                                <p>This is the page description</p>--}}
                {{--                                            </a>--}}
                {{--                                        </li>--}}

                {{--                                        <li>--}}
                {{--                                            <a class="cd-nav-item item-6 animsition-link" href="blockquotes.html">--}}
                {{--                                                <h3>Blockquotes</h3>--}}
                {{--                                                <p>This is the page description</p>--}}
                {{--                                            </a>--}}
                {{--                                        </li>--}}

                {{--                                        <li>--}}
                {{--                                            <a class="cd-nav-item item-7 animsition-link" href="process.html">--}}
                {{--                                                <h3>Process Steps</h3>--}}
                {{--                                                <p>This is the page description</p>--}}
                {{--                                            </a>--}}
                {{--                                        </li>--}}

                {{--                                        <li>--}}
                {{--                                            <a class="cd-nav-item item-8 animsition-link" href="alert.html">--}}
                {{--                                                <h3>Alert Boxes</h3>--}}
                {{--                                                <p>This is the page description</p>--}}
                {{--                                            </a>--}}
                {{--                                        </li>--}}

                {{--                                        <li>--}}
                {{--                                            <a class="cd-nav-item item-9 animsition-link" href="tabs.html">--}}
                {{--                                                <h3>Tabs</h3>--}}
                {{--                                                <p>This is the page description</p>--}}
                {{--                                            </a>--}}
                {{--                                        </li>--}}

                {{--                                        <li>--}}
                {{--                                            <a class="cd-nav-item item-10 animsition-link" href="pricing.html">--}}
                {{--                                                <h3>Pricing Boxes</h3>--}}
                {{--                                                <p>This is the page description</p>--}}
                {{--                                            </a>--}}
                {{--                                        </li>--}}

                {{--                                        <li>--}}
                {{--                                            <a class="cd-nav-item item-11 animsition-link" href="clients.html">--}}
                {{--                                                <h3>Clients</h3>--}}
                {{--                                                <p>This is the page description</p>--}}
                {{--                                            </a>--}}
                {{--                                        </li>--}}

                {{--                                        <li>--}}
                {{--                                            <a class="cd-nav-item item-12 animsition-link" href="lightbox.html">--}}
                {{--                                                <h3>Lightbox</h3>--}}
                {{--                                                <p>This is the page description</p>--}}
                {{--                                            </a>--}}
                {{--                                        </li>--}}
                {{--                    </ul>--}}
                {{--                </li>--}}

                {{--                                <li>--}}
                {{--                                    @if (Session::get('locale', 'en') == 'en')--}}
                {{--                                        <div class="dropdown">--}}
                {{--                                            <button onclick="changeLocale()" class="dropbtn" style="font-size: large">En</button>--}}
                {{--                                            <div id="myDropdown" class="dropdown-content">--}}
                {{--                                                <a href="{{ url('locale/en') }}">@lang('trs.en')</a>--}}
                {{--                                                <a href="{{ url('locale/cn') }}">@lang('trs.cn')</a>--}}
                {{--                                            </div>--}}
                {{--                                        </div>--}}
                {{--                                    @else--}}
                {{--                                        <div class="dropdown">--}}
                {{--                                            <button onclick="changeLocale()" class="dropbtn" style="font-size: large">中文</button>--}}
                {{--                                            <div id="myDropdown" class="dropdown-content">--}}
                {{--                                                <a href="{{ url('locale/en') }}">@lang('trs.en')</a>--}}
                {{--                                                <a href="{{ url('locale/cn') }}">@lang('trs.cn')</a>--}}
                {{--                                            </div>--}}
                {{--                                        </div>--}}
                {{--                                    @endif--}}
                {{--                                </li>--}}
                {{--                                <li>--}}
                {{--                                    @if (Session::get('locale', 'en') == 'en')--}}
                {{--                                        <a href="{{ url('locale/cn') }}" >--}}
                {{--                                            <img width="20px" height="20px" src="{{ asset('images/flags-png/chinese.png') }}"--}}
                {{--                                                 alt="america">--}}
                {{--                                        </a>--}}
                {{--                                    @else--}}
                {{--                                        <a href="{{ url('locale/en') }}" >--}}
                {{--                                            <img width="20px" height="20px" src="{{ asset('images/flags-png/america.png') }}"--}}
                {{--                                                 alt="chinese">--}}
                {{--                                        </a>--}}
                {{--                                    @endif--}}
                {{--                                </li>--}}

            </ul> <!-- primary-nav -->
            {{--                    </ul>--}}
        </nav> <!-- cd-nav -->


        <div id="cd-search" class="cd-search">
            <form>
                <input type="search" id="search" placeholder="Search...">
                <button onclick="search_page(document.getElementById('search').value)" style="display: none">Search</button>
            </form>
        </div>
        @yield('breadcrumbs')
        <style type="text/css">
            #loginbg {
                display: none;
                position: absolute;
                top: 0;
                left: 0;
                z-index: 200;
                height: 100%;
                width: 100%;
                background: #000000;
                filter: alpha(opacity=30);
                -moz-opacity: 0.3;
                opacity: 0.3;
                /*overflow:hidden;*/
            }

            /*#login {*/
            /*    !*position:absolute;*!*/
            /*    top: 10%;*/
            /*    !*left:10%;*!*/
            /*    !*width:293px;*!*/
            /*    !*z-index:201;*!*/
            /*    !*background:#FFFFFF;*!*/
            /*}*/

        </style>
        <div id="loginbg"></div>

        <a class="hiddenanchor" id="toregister"></a>
        <a class="hiddenanchor" id="tologin"></a>

        {{--        @error('email'|| 'name' || 'password')--}}
        {{--        @if($errors->has('email') || $errors->has('name') || $errors->has('password'))--}}
        {{--        <div id="wrapper" style="display:block;z-index: 201;">--}}
        {{--            <div id="login" class="animate form">--}}

        {{--                <button id="cboxClose"--}}
        {{--                        style="width: 1px;margin-left: 90%;padding-right: 20px;padding-left: 15px;padding-bottom: 12px;"--}}
        {{--                        onClick="close_login()">X</button>--}}
        {{--                --}}{{--                    <div id="button-con" style="text-align: right;padding: 5%;">--}}
        {{--                --}}{{--                        <button id="close_login" class="send_message" type="button" onClick="close_login()">x</button>--}}
        {{--                --}}{{--                    </div>--}}
        {{--                <div class="login100-pic js-tilt" data-tilt style="text-align: -webkit-left;float: left;margin-top: 10%;margin-left: 6%;">--}}
        {{--                    <img src="{{ asset('/images/medlab logo port COL.png')}}" alt="IMG">--}}
        {{--                </div>--}}
        {{--                --}}{{--                    <div id="login" class="wrap-login100 animate form" style="z-index: 205;">--}}
        {{--                --}}{{--                        <input id="close_login" type="button" style="width: 1px;margin-left: 90%;padding-right: 20px;padding-left: 15px;padding-bottom: 12px;" value="x" onClick="close_login()" />--}}
        {{--                <form method="POST" action="{{ route('login') }}">--}}
        {{--                    @csrf--}}

        {{--                    <h1>Login</h1>--}}
        {{--                    @error('name')--}}
        {{--                    --}}{{--                    <span class="invalid-feedback" role="alert">--}}
        {{--                    <strong style="color:#dc3545">{{ $message }}</strong>--}}
        {{--                    --}}{{--                                    </span>--}}
        {{--                    @enderror--}}
        {{--                    @error('email')--}}
        {{--                    --}}{{--                    <span class="invalid-feedback" role="alert">--}}
        {{--                    <strong style="color:#dc3545">{{ $message }}</strong>--}}
        {{--                    --}}{{--                                    </span>--}}
        {{--                    @enderror--}}
        {{--                    @error('password')--}}
        {{--                    --}}{{--                    <span class="invalid-feedback" role="alert">--}}
        {{--                    <strong style="color:#dc3545">{{ $message }}</strong>--}}
        {{--                    --}}{{--                                    </span>--}}
        {{--                    @enderror--}}
        {{--                    <p>--}}
        {{--                        <label for="username" class="uname" data-icon="u"> Your email or username</label>--}}
        {{--                        <input id="username" name="email" required="required" type="text"--}}
        {{--                               placeholder="myusername or mymail@mail.com"/>--}}

        {{--                    </p>--}}
        {{--                    <p>--}}
        {{--                        <label for="password" class="youpasswd" data-icon="p"> Your password </label>--}}
        {{--                        <input id="password" name="password" required="required" type="password"--}}
        {{--                               placeholder="eg. X8df!90EO"/>--}}

        {{--                    </p>--}}
        {{--                    <p class="keeplogin">--}}
        {{--                        <input type="checkbox" name="remember" id="remember" value="on"/>--}}
        {{--                        <label for="remember">Keep me logged in</label>--}}
        {{--                    </p>--}}
        {{--                    @if (Route::has('password.request'))--}}
        {{--                        <a class="btn btn-link" href="{{ route('password.request') }}"--}}
        {{--                           style="width: 300px;text-align: left;background: unset;">--}}
        {{--                            {{ __('Forgot Your Password?') }}--}}
        {{--                        </a>--}}
        {{--                        <a class="btn btn-link" href="{{ route('register') }}"--}}
        {{--                           style="width: 300px;text-align: left;background: unset;">Not a member ? Register here--}}
        {{--                        </a>--}}
        {{--                    @endif--}}
        {{--                    <div class="sixteen columns">--}}
        {{--                        <div id="button-con" style="text-align: right;padding: 5%;">--}}
        {{--                            <button class="send_message" type="submit">submit</button>--}}
        {{--                        </div>--}}
        {{--                    </div>--}}
        {{--                    <p class="change_link" style="width: 100%;">--}}
        {{--                    </p>--}}
        {{--                </form>--}}


        {{--            </div>--}}
        {{--        </div>--}}
        <div id="wrapper" style="display:none;z-index: 201;">
            <div id="login" class="animate form">
                {{--                    @if(count($errors->all())!=0)--}}
                {{--                                                    @php dd($errors->all()); @endphp--}}
                {{--                        <div class="sixteen columns">--}}
                {{--                            <div class="alert alert-red">--}}
                {{--                                <p><span>&#xf00d;</span>{{ $errors->all()[0] }}</p>--}}
                {{--                            </div>--}}
                {{--                        </div>--}}
                {{--                    @endif--}}

                <button id="cboxClose"
                        style="width: 1px;margin-left: 90%;padding:8px 20px 8px 10px;"
                        onClick="close_login()">X
                </button>
                {{--                    <div id="button-con" style="text-align: right;padding: 5%;">--}}
                {{--                        <button id="close_login" class="send_message" type="button" onClick="close_login()">x</button>--}}
                {{--                    </div>--}}
                <div class="login100-pic js-tilt" data-tilt
                     style="text-align: -webkit-left;float: left;margin-top: 10%;margin-left: 6%;">
                    <img src="{{ asset('/images/medlab logo port COL.png')}}" alt="IMG">
                </div>
                {{--                    <div id="login" class="wrap-login100 animate form" style="z-index: 205;">--}}
                {{--                        <input id="close_login" type="button" style="width: 1px;margin-left: 90%;padding-right: 20px;padding-left: 15px;padding-bottom: 12px;" value="x" onClick="close_login()" />--}}
                <form method="post" id="loginForm" action="{{ route('login') }}">
                    @csrf

                    <h1>Login</h1>
                    @if (session('login_error'))
                        <div class="sixteen columns" id="login_error">
                            <div class="alert alert-red">
                                <p><span>&#xf00d;</span>{{ session('login_error') }}</p>
                            </div>
                        </div>
                    @endif
                    {{--                        @error('name')--}}
                    {{--                        --}}{{--                    <span class="invalid-feedback" role="alert">--}}
                    {{--                        <strong style="color:#dc3545">{{ $message }}</strong>--}}
                    {{--                        --}}{{--                                    </span>--}}
                    {{--                        @enderror--}}
                    {{--                        @error('email')--}}
                    {{--                        --}}{{--                    <span class="invalid-feedback" role="alert">--}}
                    {{--                        <strong style="color:#dc3545">{{ $message }}</strong>--}}
                    {{--                        --}}{{--                                    </span>--}}
                    {{--                        @enderror--}}
                    {{--                        @error('password')--}}
                    {{--                        --}}{{--                    <span class="invalid-feedback" role="alert">--}}
                    {{--                        <strong style="color:#dc3545">{{ $message }}</strong>--}}
                    {{--                        --}}{{--                                    </span>--}}
                    {{--                        @enderror--}}
                    <p>
                        <label for="username" class="uname" data-icon="u"> Your email or username</label>
                        <input id="username" name="email" type="email" required="required"
                               placeholder="myusername or mymail@mail.com"/>

                    </p>
                    <p>
                        <label for="password" class="youpasswd" data-icon="p"> Your password </label>
                        <input id="password" name="password" required="required"
{{--                               oninput="validatePassword(this)"--}}
                               type="password"
                               placeholder="eg. X8df!90EO"/>

                    </p>
                    <p class="keeplogin">
                        <input type="checkbox" name="remember" id="remember" value="on"/>
                        <label for="remember">Keep me logged in</label>
                    </p>
                    @if (Route::has('password.request'))
                        <a class="btn btn-link" href="{{ route('password.request') }}"
                           style="width: 300px;text-align: left;background: unset;">
                            {{ __('Forgot Your Password?') }}
                        </a>
                        <a class="btn btn-link" href="{{ route('register') }}"
                           style="width: 300px;text-align: left;background: unset;">Not a member ? Register here
                        </a>
                    @endif
                    {{--                        <input type="submit" value="submit" />--}}
                    <div class="sixteen columns">
                        <div id="button-con" style="text-align: right;padding: 5%;">
                            <button name="submit" type="submit">submit</button>
                        </div>
                    </div>
                    <p class="change_link" style="width: 100%;">
                    </p>
                </form>


            </div>
        </div>

        {{--        <div class="medlab_breadcrumbs_wrapper">--}}
        {{--            <div class="container" style="width: unset;background-color:#7AA43F;">--}}
        {{--                <ol class="breadcrumb medlab_breadcrumbs" style="margin-bottom: unset;background-color:#7AA43F;">--}}
        {{--                </ol>--}}
        {{--            </div>--}}
        {{--        </div>--}}

        {{--        <div class="medlab_breadcrumbs_wrapper">--}}
        {{--            <div class="container" style="width: unset">--}}
        {{--                <ol class="breadcrumb medlab_breadcrumbs">--}}
        {{--                    <li><a class="medlab_breadcrumbs_link" href="/">Home</a></li>--}}
        {{--                    <li><a class="medlab_breadcrumbs_link" href="/about">About Us</a></li>--}}
        {{--                    <li class="active medlab_breadcrumbs_text">Careers</li>--}}
        {{--                </ol>--}}
        {{--            </div>--}}
        {{--        </div>--}}
        {{--        <div class="cd-search">--}}
        {{--            @if (Session::get('locale', 'en') == 'en')--}}
        {{--                <a href="{{ url('locale/cn') }}" class="nav-full icon m-auto bg-white">--}}
        {{--                    <img width="20px" height="20px" src="{{ asset('images/flags-png/america.png') }}"--}}
        {{--                         alt="america">--}}
        {{--                </a>--}}
        {{--            @else--}}
        {{--                <a href="{{ url('locale/en') }}" class="nav-full icon m-auto bg-white">--}}
        {{--                    <img width="20px" height="20px" src="{{ asset('images/flags-png/chinese.png') }}"--}}
        {{--                         alt="chinese">--}}
        {{--                </a>--}}
        {{--            @endif--}}
        {{--        </div>--}}

    </div>


    {{--</div>--}}
    <script type="text/javascript" src="{{ asset('/js/jquery-2.1.1.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script src="sweetalert2.all.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>

    <script language="javascript">

        jQuery(document).ready(function ($) {
            $("#loginForm").submit(function(e) {

                console.log('123');
                e.preventDefault(); // avoid to execute the actual submit of the form.

                var form = $(this);
                var url = form.attr('action');

                $.ajax({
                    type: "POST",
                    url: url,
                    data: form.serialize(), // serializes the form's elements.
                    success: function(data)
                    {
                        console.log(data);
                        if(data.msg=='practitioner') {
                            var list = JSON.parse(data.practitioner);
                            var options = {};
                            $.map(list,
                                function (o) {
                                    options[o.guid] = o.customer_name;
                                });
                            const {value: practitioner} = Swal.fire({
                                title: 'Select a business',
                                input: 'select',
                                inputOptions: options,    // inputOptions: {
                                //     'practitioner': {
                                //
                                //
                                //         // apples: 'Apples',
                                //         // bananas: 'Bananas',
                                //         // grapes: 'Grapes',
                                //         // oranges: 'Oranges'
                                //     },
                                // },
                                inputPlaceholder: 'Select a business',
                                allowOutsideClick: false,
                                showLoaderOnConfirm: true,
                                inputValidator: (value) => {
                                    return new Promise((resolve) => {
                                        // if (value === 'oranges') {
                                        //     resolve()
                                        // } else {
                                        //     resolve('You need to select oranges :)')
                                        // }
                                        console.log(value);
                                        $.ajax({
                                            type: "POST",
                                            url: '{{ url('submit_practitioner') }}',
                                            data: {
                                                _token: '{{ csrf_token() }}',
                                                practitioner: value,
                                            },
                                            success: function () {
                                                location.reload();
                                            }

                                        })
                                    })
                                }
                            })
                        }else if(data.msg=='patient'){
                            location.reload();
                        }else{
                            Swal.fire({
                                icon: 'error',
                                title: 'Invalid email or password',
                            })
                        }
                    }
                });


            });
        });
        function search_page(string){
            window.find(string);
        }
        function open_login() {
            document.getElementById('loginbg').style.display = 'block';
            document.getElementById('wrapper').style.display = 'block';
            document.getElementById('cd-primary-nav').classList.remove('nav-is-visible');
            document.getElementById('header').classList.remove('nav-is-visible');
            document.getElementById('nav').classList.remove('nav-is-visible');
            document.getElementById('main').classList.remove('nav-is-visible');
            // document.getElementsByClassName('cd-main-content').classList.remove('nav-is-visible');
            showloginbg();
            $('html, body').addClass('lock-back');
        }

        function close_login() {
            @php \Session::forget('login_error'); @endphp
            $("#login_error").remove();
            document.getElementById('loginbg').style.display = 'none';
            document.getElementById('wrapper').style.display = 'none';
            // if(screen.width < '1420px'){
            //     document.getElementById('cd-primary-nav').classList.add('nav-is-visible');
            //     document.getElementById('header').classList.add('nav-is-visible');
            //     document.getElementById('nav').classList.add('nav-is-visible');
            //     document.getElementById('main').classList.add('nav-is-visible');
            // }

            $('html, body').removeClass('lock-back');
        }

        function showloginbg() {
            var sWidth, sHeight;
            sWidth = screen.width;
            sWidth = document.body.offsetWidth;
            sHeight = document.body.offsetHeight;
            if (sHeight < screen.height) {
                sHeight = screen.height;
            }
            document.getElementById("loginbg").style.width = sWidth + "px";
            document.getElementById("loginbg").style.height = sHeight + "px";
            document.getElementById("loginbg").style.display = "block";
            document.getElementById("loginbg").style.display = "block";
            document.getElementById("loginbg").style.right = document.getElementById("wrapper").offsetLeft + "px";
        }

        function logo_in() {
            alert()
            //myform.action=""
            //myform.submit()
            close_login();
        };

        // function validatePassword(input) {
        //     var regex = /^[0-9a-zA-Z]{8,16}$/;
        //     if (!(input.value.match(regex))) {
        //         input.setCustomValidity("Password must be between 8 and 16 digits without space");
        //     } else {
        //         input.setCustomValidity("");
        //     }
        // }

        // window.onload = function() {
        //
        //     var username = document.getElementById("username");
        //     console.log('username');
        //     var password = document.getElementById("password");
        //     username.onkeyup = function() {
        //         inprove("username");
        //     }
        //     password.onkeyup = function() {
        //         inprove("password");
        //     }
        // };
        // function inprove(attr) {
        //     var flag = document.getElementById(attr);
        //
        //     if (attr == "username") {
        //         var Regex = /^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/;
        //         if (!(flag.value.match(Regex))) {
        //             document.getElementById("checkUsername").innerText = "格式不对";
        //             document.getElementById("checkUsername").style.color = "red";
        //         } else {
        //             document.getElementById("checkUsername").innerText = "格式正确";
        //             document.getElementById("checkUsername").style.color = "red";
        //         }
        //     } else {
        //         var regex = /^[0-9a-zA-Z]{8,16}$/g; // 匹配用户输入的正则
        //         if (!(flag.value.match(regex))) {
        //             document.getElementById("checkPassword").innerText = "格式不对";
        //             document.getElementById("checkPassword").style.color = "red";
        //         } else {
        //             document.getElementById("checkPassword").innerText = "格式正确";
        //             document.getElementById("checkPassword").style.color = "red";
        //         }
        //     }
        // }


        {{--            $().ready(function() {--}}
        {{--                console.log('1');--}}
        {{--                $("#loginform").validate({--}}
        {{--                    onsubmit:true,// 是否在提交是验证--}}
        {{--                    onfocusout:false,// 是否在获取焦点时验证--}}
        {{--                    onkeyup :false,// 是否在敲击键盘时验证--}}

        {{--                    rules: {　　　　//规则--}}
        {{--                        username: {　　//要对应相对应的input中的name属性--}}
        {{--                            required: true--}}
        {{--                        },--}}
        {{--                        password: {--}}
        {{--                            required: true--}}
        {{--                        }--}}
        {{--                    },--}}
        {{--                    messages:{　　　　//验证错误信息--}}
        {{--                        username: {--}}
        {{--                            required: "请输入用户名"--}}
        {{--                        },--}}
        {{--                        password: {--}}
        {{--                            required: "请输入密码"--}}
        {{--                        }--}}
        {{--                    },--}}
        {{--                    submitHandler: function(form) { //通过之后回调--}}
        {{--//进行ajax传值--}}
        {{--                        $.ajax({--}}
        {{--                            url : "{{ route('login') }}",--}}
        {{--                            type : "post",--}}
        {{--                            dataType : "json",--}}
        {{--                            data: {--}}
        {{--                                email: $("#username").val(),--}}
        {{--                                password: $("#password").val()--}}
        {{--                            },--}}
        {{--                            success : function(msg) {--}}
        {{--                                console.log('success');--}}
        {{--                            }--}}
        {{--                        });--}}
        {{--                    },--}}
        {{--                    invalidHandler: function(form, validator) {return false;}--}}
        {{--                });--}}
        {{--            });--}}
    </script>
    {{--<script>--}}
    {{--    $(function() {--}}
    {{--        $("#loginForm").submit(function (event) {--}}
    {{--            console.log('1');--}}
    {{--            event.preventDefault(); //prevent default action--}}
    {{--            // var post_url = $(this).attr("action"); //get form action url--}}
    {{--            // var request_method = $(this).attr("method"); //get form GET/POST method--}}
    {{--            // var form_data = $(this).serialize(); //Encode form elements for submission--}}

    {{--            $.ajax({--}}
    {{--                url: "{{ route('login') }}",--}}
    {{--                type: "post",--}}
    {{--                dataType: "json",--}}
    {{--                data: {--}}
    {{--                    email: $("#username").val(),--}}
    {{--                    password: $("#password").val()--}}
    {{--                },--}}
    {{--                success: function (msg) {--}}
    {{--                    console.log('success');--}}
    {{--                }--}}
    {{--            });--}}
    {{--        });--}}
    {{--    });--}}


    {{--        </script>--}}
    <script src="{{ asset('/vendor/jquery/jquery-3.2.1.min.js')}}"></script>
    <!--===============================================================================================-->
    <script src="{{ asset('/vendor/bootstrap/js/popper.js')}}"></script>
    <script src="{{ asset('/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
    <!--===============================================================================================-->
    <script src="{{ asset('/vendor/select2/select2.min.js')}}"></script>
    <script>
        $('.js-tilt').tilt({
            scale: 5.1
        })
    </script>

    <script src="{{ asset('/vendor/tilt/tilt.jquery.min.js')}}"></script>
    <script src="{{ asset('/js/main.js')}}"></script>
    <script>

        /* 点击按钮，下拉菜单在 显示/隐藏 之间切换 */
        function changeLocale() {
            document.getElementById("myDropdown").classList.toggle("show");
        }

        // 点击下拉菜单意外区域隐藏
        window.onclick = function (event) {
            if (!event.target.matches('.dropbtn')) {

                var dropdowns = document.getElementsByClassName("dropdown-content");
                var i;
                for (i = 0; i < dropdowns.length; i++) {
                    var openDropdown = dropdowns[i];
                    if (openDropdown.classList.contains('show')) {
                        openDropdown.classList.remove('show');
                    }
                }
            }
        }
    </script>

