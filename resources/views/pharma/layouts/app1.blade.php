<!DOCTYPE html>
<!--[if IE 8]>
<html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]>
<!-->
<html class="no-js" lang="en"><!--<![endif]-->
{{--<head>--}}

<!-- Basic Page Needs
  ================================================== -->
<meta charset="utf-8">
<title>Medlab</title>
<meta name="description" content="">
<meta name="author" content="">
<meta name="csrf-token" content="{{ csrf_token() }}">

<!-- Mobile Specific Metas
================================================== -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">


<!-- CSS
================================================== -->
<link rel="stylesheet" type="text/css" href="{{ asset('/css/base.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('/css/skeleton.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('/css/layout.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('/css/settings.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('/css/font-awesome.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('/css/owl.carousel.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('/css/retina.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('/css/animsition.min.css') }}"/>

<link rel="alternate stylesheet" type="text/css" href="{{ asset('/css/colors/color-gold.css') }}" title="1">
{{--<link rel="alternate stylesheet" type="text/css" href="{{ asset('/css/colors/color-2.css') }}" title="2">--}}
{{--<link rel="alternate stylesheet" type="text/css" href="{{ asset('/css/colors/color-3.css') }}" title="3">--}}
{{--<link rel="alternate stylesheet" type="text/css" href="{{ asset('/css/colors/color-4.css') }}" title="4">--}}
{{--<link rel="alternate stylesheet" type="text/css" href="{{ asset('/css/colors/color-5.css') }}" title="5">--}}
{{--<link rel="alternate stylesheet" type="text/css" href="{{ asset('/css/colors/color-6.css') }}" title="6">--}}
{{--<link rel="alternate stylesheet" type="text/css" href="{{ asset('/css/colors/color-7.css') }}" title="7">--}}


<!-- Favicons
================================================== -->
<link rel="shortcut icon" href="{{ asset('/images/favicon.png')}}">
<link rel="apple-touch-icon" href="{{ asset('/images/apple-touch-icon.png')}}">
<link rel="apple-touch-icon" sizes="72x72" href="{{ asset('/images/apple-touch-icon-72x72.png')}}">
<link rel="apple-touch-icon" sizes="114x114" href="{{ asset('/images/apple-touch-icon-114x114.png')}}">

<link rel="stylesheet" type="text/css" href="{{ asset('/css/demo.css')}}" />
<link rel="stylesheet" type="text/css" href="{{ asset('/css/style2.css')}}" />
<link rel="stylesheet" type="text/css" href="{{ asset('/css/animate-custom.css')}}" />

{{--<link rel="stylesheet" type="text/css" href="{{ asset('/vendor/bootstrap/css/bootstrap.min.css')}}">--}}
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="{{ asset('/fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="{{ asset('/vendor/animate/animate.css')}}">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="{{ asset('/vendor/css-hamburgers/hamburgers.min.css')}}">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="{{ asset('/vendor/select2/select2.min.css')}}">
<!--===============================================================================================-->
{{--<link rel="stylesheet" type="text/css" href="{{ asset('/css/util.css"')}}">--}}
<link rel="stylesheet" type="text/css" href="{{ asset('/css/main.css')}}">
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA806a5QISlttwcTojqi6lfSp1OuBk8UWU&libraries=places"></script>
<link rel="stylesheet" href="{{ asset('/build/css/intlTelInput.css') }}">



{{--<script type="text/javascript" src="{{ asset('/js/jquery-2.1.1.js') }}"></script>--}}
{{--<script type="text/javascript" src="{{ asset('js/modernizr.custom.js') }}"></script>--}}
{{--<script type="text/javascript" src="{{ asset('/js/jquery.mobile.custom.min.js') }}"></script>--}}
{{--<script type="text/javascript" src="{{ asset('/js/retina-1.1.0.min.js') }}"></script>--}}
{{--<script type="text/javascript" src="{{ asset('/js/jquery.animsition.min.js') }}"></script>--}}
{{--<script type="text/javascript">--}}

{{--    (function($) { "use strict";--}}
{{--        $(document).ready(function() {--}}

{{--            $(".animsition").animsition({--}}

{{--                inClass               :   'zoom-in-sm',--}}
{{--                outClass              :   'zoom-out-sm',--}}
{{--                inDuration            :    1500,--}}
{{--                outDuration           :    800,--}}
{{--                linkElement           :   '.animsition-link',--}}
{{--                // e.g. linkElement   :   'a:not([target="_blank"]):not([href^=#])'--}}
{{--                loading               :    true,--}}
{{--                loadingParentElement  :   'body', //animsition wrapper element--}}
{{--                loadingClass          :   'animsition-loading',--}}
{{--                unSupportCss          : [ 'animation-duration',--}}
{{--                    '-webkit-animation-duration',--}}
{{--                    '-o-animation-duration'--}}
{{--                ],--}}
{{--                //"unSupportCss" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.--}}
{{--                //The default setting is to disable the "animsition" in a browser that does not support "animation-duration".--}}

{{--                overlay               :   false,--}}

{{--                overlayClass          :   'animsition-overlay-slide',--}}
{{--                overlayParentElement  :   'body'--}}
{{--            });--}}
{{--        });--}}
{{--    })(jQuery);--}}
{{--</script>--}}


