<!-- FOOTER
    ================================================== -->

{{--<section class="section grey-section section-padding-top-bottom" id="scroll-link-6">--}}

{{--    <div class="container">--}}
{{--        <div class="sixteen columns">--}}
{{--            <div class="section-title">--}}
{{--                <h2>contact</h2>--}}
{{--                <div class="subtitle">Get in touch.</div>--}}
{{--            </div>--}}
{{--        </div>--}}

{{--        <div class="clear"></div>--}}

{{--        <form name="ajax-form" id="ajax-form" action="{{ url('/') }}" method="post">--}}
{{--            {!! csrf_field() !!}--}}
{{--            <div class="eight columns">--}}
{{--                <label for="name">--}}
{{--                    <span class="error" id="err-name">please enter name</span>--}}
{{--                </label>--}}
{{--                <input name="name" id="name" type="text" style="text-transform: unset;" placeholder="Your Name: *"/>--}}
{{--            </div>--}}
{{--            <div class="eight columns">--}}
{{--                <label for="email">--}}
{{--                    <span class="error" id="err-email">please enter e-mail</span>--}}
{{--                    <span class="error" id="err-emailvld">e-mail is not a valid format</span>--}}
{{--                </label>--}}
{{--                <input name="email" id="email" type="text" style="text-transform: unset;" placeholder="E-Mail: *"/>--}}
{{--            </div>--}}
{{--            <div class="sixteen columns">--}}
{{--                <label for="message"></label>--}}
{{--                <textarea name="message" id="message" style="text-transform: unset;" placeholder="Tell Us Everything"></textarea>--}}
{{--            </div>--}}
{{--            <div class="sixteen columns">--}}
{{--                <div id="button-con"><button class="send_message" id="send">submit</button></div>--}}
{{--            </div>--}}
{{--            <div class="clear"></div>--}}
{{--            <div class="error text-align-center" id="err-form">There was a problem validating the form please check!</div>--}}
{{--            <div class="error text-align-center" id="err-timedout">The connection to the server timed out!</div>--}}
{{--            <div class="error" id="err-state"></div>--}}
{{--        </form>--}}

{{--        <div class="clear"></div>--}}

{{--        <div id="ajaxsuccess">Successfully sent!!</div>--}}

{{--    </div>--}}

{{--    <div class="clear"></div>--}}

{{--</section>--}}

<!-- SECTION
================================================== -->

<section class="section white-section section-padding-top-bottom" style="height:160px;padding-top:50px;padding-bottom:50px;z-index: -1">

    <div class="container">
        <div class="sixteen columns">
            <div class="social-contact">
                <ul class="list-contact" style="padding: 0px;">
                    <li class="contact-soc">
                        <a class="tooltip-shop" href="https://www.facebook.com/medlabAUS" target="view_window">&#xf09a;<span class="tooltip-content-shop"><span class="tooltip-text-shop"><span class="tooltip-inner-shop">follow us</span></span></span></a>
                    </li>
                    <li class="contact-soc">
                        <a class="tooltip-shop" href="https://twitter.com/medlabclinical" target="view_window">&#xf099;<span class="tooltip-content-shop"><span class="tooltip-text-shop"><span class="tooltip-inner-shop">like us</span></span></span></a>
                    </li>
                    <li class="contact-soc">
                        <a class="tooltip-shop" href="https://www.instagram.com/medlab_clinical/" target="view_window">&#xf16d;<span class="tooltip-content-shop"><span class="tooltip-text-shop"><span class="tooltip-inner-shop">stay up to date</span></span></span></a>
                    </li>
                    <li class="contact-soc">
                        <a class="tooltip-shop" href="https://www.youtube.com/channel/UCLZnBKwF4FpBd_1VNsX3xDA" target="view_window">&#xf16a;<span class="tooltip-content-shop"><span class="tooltip-text-shop"><span class="tooltip-inner-shop">+1 us</span></span></span></a>
                    </li>
                    <li class="contact-soc">
                        <a class="tooltip-shop" href="https://vimeo.com/medlabclinical" target="view_window">&#xf1ca;<span class="tooltip-content-shop"><span class="tooltip-text-shop"><span class="tooltip-inner-shop">+1 us</span></span></span></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

</section>


{{--<section class="section">--}}

{{--    <a class="button-map close-map"><span>Locate Us on Map</span></a>--}}
{{--    <div id="google_map"></div>--}}

{{--</section>--}}


<!-- SECTION
================================================== -->





<section class="section footer-1 section-padding-top-bottom">
    <div class="container">

        <div class="four columns" data-scroll-reveal="enter left move 200px over 0.5s after 0.8s">
            <h4><i class="icon-footer">&#xf041;</i>Corporate Offices</h4>
            <p>Medlab Clinical LTD<br/>66 McCauley Street<br/>Alexandria, NSW<br/>Australia 2015</p>
        </div>
        <div class="four columns" data-scroll-reveal="enter left move 200px over 0.5s after 0.3s">
            <h4><i class="icon-footer">&#xf199;</i>Contact Details</h4>
            <p>Office Hours: 8:30am - 5pm EST<br/>Toll Free: 1300 369 570<br/>Main Phone: 02 8188 0311<br/>Fax: 02 9699 3347<br/>Email: hello@medlab.co</p>
        </div>
        <div class="four columns" data-scroll-reveal="enter right move 200px over 0.5s after 0.3s">
            <h4><i class="icon-footer">&#xf0c1;</i>Our Websites</h4>
            <a href="https://dev-www.medlab.co" style="color: white;">www.medlab.co</a><br/>
            <a href="https://dev-pharma.medlab.co" style="color: white;">pharma.medlab.co</a><br/>
            <a href="https://dev-vms.medlab.co" style="color: white;">vms.medlab.co</a><br/>
            <a href="https://shop.medlab.co" style="color: white;">shop.medlab.co</a>
        </div>
        <div class="four columns" data-scroll-reveal="enter right move 200px over 0.5s after 0.8s">
            <h4><i class="icon-footer">&#xf0f6;</i>Policies</h4>
            <a href="{{route('pharma.privacy_policy')}}" style="color: white;">Privacy Policy</a><br/>
            <a href="{{route('pharma.shipping_and_delivery')}}" style="color: white;">Shipping and Delivery</a><br/>
            <a href="{{route('pharma.returns')}}" style="color: white;">Returns</a><br/>
            <a href="{{route('pharma.sales_policy')}}" style="color: white;">Sales Policy</a>
        </div>
    </div>
</section>
<section class="section footer-bottom">
    <div class="container">
        <div class="sixteen columns">
            <p>All Contents Copyright © 2020 Medlab Clinical® All rights reserved</p>
        </div>
    </div>
</section>


{{--<script type="text/javascript" src="js/jquery.themepunch.tools.min.js"></script>--}}
{{--<script type="text/javascript" src="js/jquery.themepunch.revolution.min.js"></script>--}}
{{--<script type="text/javascript" src="js/imagesloaded.pkgd.min.js"></script>--}}
{{--<script type="text/javascript" src="js/masonry.js"></script>--}}
{{--<script type="text/javascript" src="js/isotope.js"></script>--}}
{{--<script type="text/javascript" src="js/jquery.fitvids.js"></script>--}}
{{--<script type="text/javascript" src="js/contact.js"></script>--}}
{{--<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>--}}
{{--<script type="text/javascript" src="js/custom-one-page-home.js"></script>--}}







