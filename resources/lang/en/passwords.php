<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Your password has been reset!',
    'sent' => 'Please check your email address for your unique password reset link.',
    'token' => 'This password reset token is invalid.',
    'user' => "No email or username found, please re-enter.",
    'throttled' => 'Please wait before retrying.',

];
